import MotherCreator from './mother-creator';

export default class AddressMother {
  static city() {
    return MotherCreator.faker.address.city();
  }

  static coordinate() {
    return {
      latitude: MotherCreator.faker.address.latitude(),
      longitude: MotherCreator.faker.address.longitude(),
    };
  }

  static zipCode() {
    return MotherCreator.faker.address.zipCode();
  }

  static random() {
    return MotherCreator.faker.address.streetAddress();
  }

  static countryCode() {
    return MotherCreator.faker.address.countryCode();
  }
}
