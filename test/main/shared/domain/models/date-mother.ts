import MotherCreator from './mother-creator';

export default class DateMother {
  static between(from: string | number | Date, to: string | Date) {
    return MotherCreator.faker.date.between(from, to);
  }

  static past(years: number) {
    return MotherCreator.faker.date.past(years);
  }
}
