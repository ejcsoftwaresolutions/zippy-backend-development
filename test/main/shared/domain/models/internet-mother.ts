import MotherCreator from './mother-creator';

export default class InternetMother {
  static email() {
    return MotherCreator.faker.internet.email();
  }

  static password() {
    return MotherCreator.faker.internet.password();
  }
}
