import MotherCreator from './mother-creator';

export default class AlphanumericMother {
  static random(count?: number) {
    return MotherCreator.faker.random.alphaNumeric(count);
  }

  static number(min?: number, max?: number, precision?: number) {
    return MotherCreator.faker.random.number({
      min: min,
      max: max,
      precision: precision ?? 1,
    });
  }
}
