import { Gender } from '@shared/domain/models/gender';

export default class GenderMother {
  static random(): Gender {
    const types: Array<Gender> = ['male', 'female'];
    return types[Math.floor(Math.random() * types.length)];
  }
}
