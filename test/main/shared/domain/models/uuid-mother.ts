import MotherCreator from './mother-creator';

export default class UuidMother {
  static random() {
    return MotherCreator.faker.random.uuid();
  }
}
