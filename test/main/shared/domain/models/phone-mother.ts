import MotherCreator from './mother-creator';

export default class PhoneMother {
  static random(format?: string) {
    return MotherCreator.faker.phone.phoneNumber(format);
  }
}
