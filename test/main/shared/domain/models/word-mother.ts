import MotherCreator from './mother-creator';

export default class WordMother {
  static random() {
    return MotherCreator.faker.random.word();
  }

  static randomWords(count?: number) {
    return MotherCreator.faker.random.words(count);
  }
}
