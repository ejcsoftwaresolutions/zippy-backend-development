import Id from '@shared/domain/id/id';

export default class IdMother {
  static create(id?: string): Id {
    return new Id(id);
  }

  static random() {
    return IdMother.create();
  }
}
