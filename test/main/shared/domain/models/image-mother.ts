import MotherCreator from './mother-creator';

export default class ImageMother {
  static random() {
    return MotherCreator.faker.image.imageUrl();
  }
}
