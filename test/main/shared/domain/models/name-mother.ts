import MotherCreator from './mother-creator';

export default class NameMother {
  public static random() {
    return MotherCreator.faker.name.findName();
  }

  public static firstName() {
    return MotherCreator.faker.name.firstName();
  }

  public static lastName() {
    return MotherCreator.faker.name.lastName();
  }
}
