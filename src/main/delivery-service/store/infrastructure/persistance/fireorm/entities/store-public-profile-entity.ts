import { Collection } from 'fireorm';

@Collection('users')
export class StorePublicProfileEntity {
  id: string;
  firstName?: string;
  lastName?: string;
  email: string;
  phone?: string;
}
