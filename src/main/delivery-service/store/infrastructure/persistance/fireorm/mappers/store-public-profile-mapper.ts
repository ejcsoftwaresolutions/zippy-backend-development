import StorePublicProfile from '@deliveryService/store/domain/models/store-public-profile-model';

export default class StorePublicProfileMapper {
    static toDomain(plainValues: {
        id: string; title: string; address: any; location: {
            latitude: number; longitude: number;
        }; deliveryRadius: string; phone?: string; pushToken?: string; logoImage?: string; configurations: {
            referralPromotions: boolean; receiveEmails: boolean; receivePushNotifications: boolean; receiveSMS: boolean;
        };
    }): StorePublicProfile {
        return StorePublicProfile.fromPrimitives({
            id: plainValues.id,
            name: plainValues.title,
            location: {
                address: `${plainValues.address.line1} ${plainValues.address.line2 ?? ""} ${plainValues.address.city ?? ""} ${plainValues.address.state ?? ""}`,
                geoLocation: [parseFloat(plainValues.location.latitude as any), parseFloat(plainValues.location.longitude as any),],
            },
            phone: plainValues.phone,
            serviceConfigurations: {
                deliveryRadius: parseFloat(plainValues.deliveryRadius),
            },
            pushToken: plainValues.pushToken,
            logoUrl: plainValues.logoImage,
            configurations: plainValues.configurations,
        });
    }

    static toPersistence(user: StorePublicProfile): any {
        const primitives = user.toPrimitives();

        return primitives;
    }
}
