import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import firebase from 'firebase-admin';
import {
    PUSH_TOKENS,
    RESTAURANT_ORDERS,
    VENDORS
} from '@deliveryService/delivery/infrastructure/persistance/fireorm/constants';
import StoreOrderQueryRepository from '@deliveryService/store/domain/repositories/store-order-query-repository';
import StoreOrder from '@deliveryService/store/domain/models/store-order-model';
import FirebaseUtils from '../../../../../../../../functions/src/utils/firebase-utils';
import StorePublicProfile from '@deliveryService/store/domain/models/store-public-profile-model';
import StorePublicProfileMapper
    from '@deliveryService/store/infrastructure/persistance/fireorm/mappers/store-public-profile-mapper';

export default class StoreOrderInfrastructureQueryRepository
    extends FireOrmRepository<any>
    implements StoreOrderQueryRepository {
    static readonly bindingKey = 'StoreOrderQueryRepository';

    getEntityClass(): EntityConstructorOrPath<any> {
        return null;
    }

    async findById(id: string): Promise<StoreOrder | null> {

        const vendorOrder = (
            await firebase.firestore().collection(RESTAURANT_ORDERS).doc(id).get()
        ).data();

        if (!vendorOrder) return null;

        const profile = await this.findVendorProfileById(vendorOrder.vendorID);

        if (!profile) return null;
        if (vendorOrder.deliveryMethod === 'PICK_UP') return null;

        return StoreOrder.fromPrimitives({
            id: vendorOrder.id,
            items: vendorOrder.products.map((p: any) => ({
                id: p.id,
                name: p.name,
                quantity: p.quantity,
                unitPrice: p.price
            })),
            paymentMethod: 'CASH',
            subtotal: vendorOrder.subtotal,
            total: vendorOrder.total,
            customerId: vendorOrder.customerID,
            customerLocation: {
                address: vendorOrder.shipAddr.formattedAddress,
                geoLocation: [vendorOrder.shipAddr.location.latitude, vendorOrder.shipAddr.location.longitude]
            },
            deliveryFee:
                vendorOrder?.fees?.delivery ??
                vendorOrder.total - vendorOrder.subtotal,
            date: FirebaseUtils.getDate(vendorOrder.createdAt),
            code: vendorOrder.code ?? vendorOrder.id,
            store: profile.toPrimitives()
        });
    }


    private async findVendorProfileById(id: string): Promise<StorePublicProfile | null> {

        const userAccount = (await firebase.firestore().collection("users")
        .doc(id)
        .get()).data();

        if (!userAccount) return null;

        const profile = (
            await firebase.firestore().collection(VENDORS).doc(userAccount.id).get()
        ).data();

        if (!profile) return null;

        const pushTokens = (
            await firebase
            .firestore()
            .collection(PUSH_TOKENS)
            .doc(userAccount.id)
            .get()
        ).data();


        return StorePublicProfileMapper.toDomain({
            id: userAccount.id,
            address: profile.address,
            location: {
                latitude: profile.location.latitude,
                longitude: profile.location.longitude
            },
            phone: userAccount.phone,
            deliveryRadius: profile.deliveryRadius,
            logoImage: profile.logoInage,
            title: profile.title,
            pushToken: pushTokens?.vendor,
            configurations: profile.configurations ?? {
                receiveEmails: false,
                receiveSMS: false,
                receivePushNotifications: false,
                referralPromotions: false
            }
        });
    }
}
