import StorePublicProfile from '@deliveryService/store/domain/models/store-public-profile-model';
import StorePublicProfileQueryRepository from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { StorePublicProfileEntity } from '../entities/store-public-profile-entity';
import StorePublicProfileMapper from '../mappers/store-public-profile-mapper';
import firebase from 'firebase-admin';
import {
  PUSH_TOKENS,
  VENDORS,
} from '@deliveryService/delivery/infrastructure/persistance/fireorm/constants';

export default class StorePublicProfileQueryInfrastructureRepository
  extends FireOrmRepository<StorePublicProfileEntity>
  implements StorePublicProfileQueryRepository
{
  static readonly bindingKey = 'StorePublicProfileQueryRepository';

  getEntityClass(): EntityConstructorOrPath<StorePublicProfileEntity> {
    return StorePublicProfileEntity;
  }

  async findProfileById(id: string): Promise<StorePublicProfile | null> {
    const userAccount = await this.repository()
      .whereEqualTo('id', id)
      .findOne();

    if (!userAccount) return null;

    const profile = (
      await firebase.firestore().collection(VENDORS).doc(userAccount.id).get()
    ).data();

    if (!profile) return null;

    const pushTokens = (
      await firebase
        .firestore()
        .collection(PUSH_TOKENS)
        .doc(userAccount.id)
        .get()
    ).data();

    return StorePublicProfileMapper.toDomain({
      id: userAccount.id,
      address: profile.address,
      location: {
        latitude: profile.location.latitude,
        longitude: profile.location.longitude,
      },
      phone: userAccount.phone,
      deliveryRadius: profile.deliveryRadius,
      logoImage: profile.logoInage,
      title: profile.title,
      pushToken: pushTokens?.vendor,
      configurations: profile.configurations ?? {
        receiveEmails: false,
        receiveSMS: false,
        receivePushNotifications: false,
        referralPromotions: false,
      },
    });
  }
}
