import StorePublicProfile from '../models/store-public-profile-model';

export default interface StorePublicProfileQueryRepository {
  findProfileById(id: string): Promise<StorePublicProfile | null>;
}
