import StoreOrder from '@deliveryService/store/domain/models/store-order-model';

export default interface StoreOrderQueryRepository {

    findById(id: string): Promise<StoreOrder | null>;

}