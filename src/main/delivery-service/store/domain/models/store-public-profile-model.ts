import { RideGeoPoint } from '@deliveryService/delivery/domain/models/delivery-request-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { ObjectUtils } from '@shared/domain/utils';

interface StorePublicProfileProps {
  id: string;
  name: string;
  location: RideGeoPoint;
  phone?: string;
  serviceConfigurations: {
    deliveryRadius: number;
  };
  pushToken?: string;
  logoUrl?: string;
  configurations: {
    referralPromotions: boolean;
    receiveEmails: boolean;
    receivePushNotifications: boolean;
    receiveSMS: boolean;
  };
}

export type StorePublicProfilePrimitiveProps = StorePublicProfileProps;

const DefaultProps = {};

export default class StorePublicProfile extends AggregateRoot<StorePublicProfileProps> {
  constructor(props: StorePublicProfileProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get location() {
    return this.props.location;
  }

  get phone() {
    return this.props.phone;
  }

  get pushToken() {
    return this.props.pushToken;
  }

  get logoUrl() {
    return this.props.logoUrl;
  }

  get deliveryRadius() {
    return this.props.serviceConfigurations.deliveryRadius;
  }

  get wantsToReceivePushNotifications() {
    return this.props.configurations.receivePushNotifications;
  }

  static create(plainData: StorePublicProfileProps): StorePublicProfile {
    return new StorePublicProfile(plainData);
  }

  static fromPrimitives({
    ...plainData
  }: StorePublicProfilePrimitiveProps): StorePublicProfile {
    return new StorePublicProfile(plainData);
  }

  toPrimitives(): StorePublicProfilePrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
