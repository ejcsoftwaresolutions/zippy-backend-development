import { PaymentMethod, RideGeoPoint } from '@deliveryService/delivery/domain/models/delivery-request-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { ObjectUtils } from '@shared/domain/utils';
import StorePublicProfile, { StorePublicProfilePrimitiveProps } from './store-public-profile-model';

export type StoreOrderItem = {
    id: string;
    name: string;
    quantity: number;
    unitPrice: number;
};

interface StoreOrderProps {
    id: string;
    items: StoreOrderItem[];
    store: StorePublicProfile;
    customerId: string;
    subtotal: number;
    total: number;
    date: Date;
    paymentMethod: PaymentMethod;
    deliveryFee: number;
    code: string;
    customerLocation: RideGeoPoint;
}

export interface StoreOrderPrimitiveProps {
    id: string;
    items: StoreOrderItem[];
    store: StorePublicProfilePrimitiveProps;
    customerId: string;
    subtotal: number;
    total: number;
    date: Date;
    paymentMethod: PaymentMethod;
    deliveryFee: number;
    code: string;
    customerLocation: RideGeoPoint;
}

const DefaultProps = {};

export default class StoreOrder extends AggregateRoot<StoreOrderProps> {
    constructor(props: StoreOrderProps) {
        super({
            ...DefaultProps,
            ...props
        });
    }

    get paymentMethod() {
        return this.props.paymentMethod;
    }

    get customerLocation() {
        return this.props.customerLocation;
    }

    get storeLocation() {
        return this.props.store.location;
    }

    get store() {
        return this.props.store;
    }
    
    get storeId() {
        return this.props.store.id;
    }

    get customerId() {
        return this.props.customerId;
    }

    get deliveryFee() {
        return this.props.deliveryFee;
    }

    get id() {
        return this.props.id;
    }

    get code() {
        return this.props.code;
    }

    static create({ ...plainData }: StoreOrderProps): StoreOrder {
        return new StoreOrder(plainData);
    }

    static fromPrimitives({
        ...plainData
    }: StoreOrderPrimitiveProps): StoreOrder {
        return new StoreOrder({
            id: plainData.id,
            date: plainData.date,
            items: plainData.items,
            store: StorePublicProfile.fromPrimitives(plainData.store),
            subtotal: plainData.subtotal,
            total: plainData.total,
            paymentMethod: plainData.paymentMethod,
            deliveryFee: plainData.deliveryFee,
            code: plainData.code,
            customerId: plainData.customerId,
            customerLocation: plainData.customerLocation
        });
    }

    toPrimitives(): StoreOrderPrimitiveProps {
        const json = super.toJson();

        return ObjectUtils.omitUnknown(json);
    }
}
