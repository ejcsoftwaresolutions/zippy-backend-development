import TripGeolocationDescriptor, {
  TripGeoInformation,
} from '@deliveryService/delivery/domain/contracts/trip-geolocation-descriptor';
import {
  RideGeoPoint,
  RideRoute,
} from '@deliveryService/delivery/domain/models/delivery-request-model';
import TimeHumanizer from '@shared/domain/utils/time-humanizer';

export default class RequestInformationBuilder {
  constructor(
    private tripGeolocationDescriptor: TripGeolocationDescriptor,
    private timeHumanizer: TimeHumanizer,
  ) {}

  async getTripGeoInformation(
    routes: RideRoute[],
  ): Promise<TripGeoInformation> {
    const totalDistance = parseFloat(
      routes
        .map((route) => route.distance.value)
        .reduce((a, b) => a + b, 0)
        .toFixed(2),
    );

    const totalDuration = parseFloat(
      routes
        .map((route) => route.duration.value)
        .reduce((a, b) => a + b, 0)
        .toFixed(2),
    );

    return {
      distance: `${totalDistance} km`,
      distanceValue: totalDistance,
      duration: this.timeHumanizer.humanize(totalDuration * 60000),
      durationValue: totalDuration,
    };
  }

  async buildNewRoute(
    origin: RideGeoPoint,
    destination: RideGeoPoint,
  ): Promise<RideRoute> {
    const routeGeoInformation = await this.tripGeolocationDescriptor.basicInfo(
      origin.geoLocation.join(','),
      destination.geoLocation.join(','),
    );

    if (!routeGeoInformation) {
      throw new Error('not_found');
    }

    return {
      distance: {
        text: routeGeoInformation.distance,
        value: routeGeoInformation.distanceValue,
      },
      duration: {
        text: routeGeoInformation.duration,
        value: routeGeoInformation.durationValue,
      },
      origin: { ...origin, address: routeGeoInformation.originAddress },
      destination: {
        ...destination,
        address: routeGeoInformation.destinationAddress,
      },
    };
  }

  async buildRoutes(stops: RideGeoPoint[]): Promise<RideRoute[]> {
    const points = stops.filter((stop, index) => index !== stops.length - 1);

    const promises: Promise<RideRoute>[] = points.map((point, index) => {
      const isLastPoint = index === points.length - 1;

      return new Promise(async (resolve) => {
        const routeOrigin = point;

        const routeDestination = isLastPoint
          ? stops[stops.length - 1]
          : points[index + 1];

        const routeGeoInformation = await this.buildNewRoute(
          routeOrigin,
          routeDestination,
        );

        resolve(routeGeoInformation);
      });
    });

    return await Promise.all(promises);
  }
}
