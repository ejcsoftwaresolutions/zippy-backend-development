import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import CancelDeliveryRequestCommand from './cancel-delivery-request-command';
import CancelDeliveryRequestCommandCase from './cancel-delivery-request-use-case';

@service()
export default class CancelDeliveryRequestCommandHandler
  implements CommandHandler
{
  constructor(
    @inject('CancelDeliveryRequestCommandCase')
    private useCase: CancelDeliveryRequestCommandCase,
    @inject('DeliveryRequestCommandRepository')
    private repository: DeliveryRequestCommandRepository,
  ) {}

  getCommandName(): string {
    return CancelDeliveryRequestCommand.name;
  }

  async handle(command: CancelDeliveryRequestCommand) {
    const currentRequest = await this.repository.getRequest(command.requestId);

    if (!currentRequest) {
      throw new Error('request_not_found');
    }

    await this.useCase.execute(command.requestId);
  }
}
