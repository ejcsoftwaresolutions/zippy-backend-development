import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import EventBus from '@shared/domain/bus/event/event-bus';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Collection from '@shared/domain/value-object/collection';

@service()
export default class CancelDeliveryRequestCommandCase {
  constructor(
    @inject('DeliveryRequestCommandRepository')
    private deliveryRequestRepository: DeliveryRequestCommandRepository,
    @inject('services.request.attempt.countdown.timer')
    private requestTimerService: RequestAttemptCountdownTimer,
    @inject('event.bus')
    private eventBus: EventBus,
  ) {}

  async execute(requestId: string) {
    const currentRequest = await this.deliveryRequestRepository.getRequest(
      requestId,
    );

    try {
      this.requestTimerService.stop(currentRequest.id.value);
      this.requestTimerService.destroy(currentRequest.id.value);
    } catch (error) {}

    currentRequest.markAsCanceled();

    await this.resetCurrentCandidate(currentRequest);
    await this.deliveryRequestRepository.deleteRequest(currentRequest);

    this.eventBus.publish(currentRequest.pullDomainEvents());
  }

  private async resetCurrentCandidate(currentRequest: DeliveryRequest) {
    if (!currentRequest.currentAlertedCandidate) {
      return;
    }

    currentRequest.setCurrentAttemptAlertedCandidateNotification(null);
    await this.deliveryRequestRepository.updateDriverNotification(
      currentRequest.currentAlertedCandidate,
      currentRequest.mode,
    );

    await this.deliveryRequestRepository.toggleLockedCandidates(
      new Collection([currentRequest.currentAlertedCandidate]),
      currentRequest.mode,
      false,
    );

    currentRequest.setCurrentAttemptAlertedCandidate(null);
  }
}
