import Command from '@shared/domain/bus/command/command';

export default class CancelDeliveryRequestCommand extends Command {
  constructor(public readonly requestId: string) {
    super();
  }

  name(): string {
    return CancelDeliveryRequestCommand.name;
  }
}
