import StartRequestAttemptUseCase
    from '@deliveryService/delivery/application/start-request-attempt/start-request-attempt-use-case';
import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import TripGeolocationDescriptor from '@deliveryService/delivery/domain/contracts/trip-geolocation-descriptor';
import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import DeliveryRequestCommandRepository
    from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import StoreOrder from '@deliveryService/store/domain/models/store-order-model';
import { Scope } from '@nestjs/common';
import EventBus from '@shared/domain/bus/event/event-bus';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import TimeHumanizer from '@shared/domain/utils/time-humanizer';
import Collection from '@shared/domain/value-object/collection';
import CustomerPublicProfile from '../../../customer/domain/models/customer-public-profile-model';
import DeliveryRequest, { RideGeoPoint } from '../../../delivery/domain/models/delivery-request-model';
import RequestInformationBuilder from '../../domain/services/request-information-builder';
import DeliveryRequestRiderCandidate
    from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';

interface Props {
    id: Id;
    customer: CustomerPublicProfile;
    order: StoreOrder;
    serviceConfigurations: ServiceConfigurations;
    mode: 'live' | 'test';
    customerLocation: RideGeoPoint;
    serviceType: string;
    preCandidates?: string[];
    candidate?: DeliveryRequestRiderCandidate;
}

@service(Scope.REQUEST)
export default class RequestDeliveryUseCase {
    private requestInformationBuilder: RequestInformationBuilder;
    private startAttemptService: StartRequestAttemptUseCase;

    constructor(
        @inject('DeliveryRequestCommandRepository')
        private tripRequestRepository: DeliveryRequestCommandRepository,
        @inject('services.request.geolocation.descriptor')
            tripGeolocationDescriptor: TripGeolocationDescriptor,
        @inject('time.humanizer') timeHumanizer: TimeHumanizer,
        @inject('services.request.attempt.countdown.timer')
            requestTimerService: RequestAttemptCountdownTimer,
        @inject('event.bus') private eventBus: EventBus
    ) {
        this.requestInformationBuilder = new RequestInformationBuilder(
            tripGeolocationDescriptor,
            timeHumanizer
        );

        this.startAttemptService = new StartRequestAttemptUseCase(
            requestTimerService,
            tripRequestRepository,
            this.requestInformationBuilder,
            this.eventBus
        );
    }

    async execute(props: Props) {

        const { id, preCandidates } = props;

        let currentRequest: DeliveryRequest | null = null;

        try {
            currentRequest = await this.tripRequestRepository.getRequest(id.value);
        } catch (e) {
        }

        if (currentRequest) {
            currentRequest.setPreCandidates(preCandidates);
            await this.retryRequest(currentRequest);
            return;
        }

        const newRequest = await this.saveRequest(props);

        this.startAttemptService.execute(newRequest);
    }

    async saveRequest(props: Props) {
        const {
            id,
            customer,
            serviceConfigurations,
            mode,
            customerLocation,
            order,
            preCandidates,
            serviceType
        } = props;

        const route = await this.requestInformationBuilder.buildNewRoute(
            order.storeLocation,
            customerLocation
        );

        const tripRoutes = [
            {
                ...route,
                origin: { ...route.origin, address: order.storeLocation.address },
                destination: {
                    ...route.destination,
                    address: customerLocation.address
                }
            }
        ];

        const tripGeoInformation =
            await this.requestInformationBuilder.getTripGeoInformation(tripRoutes);

        const newRequest = new DeliveryRequest({
            id: id,
            customer,
            requestInfo: order,
            serviceConfig: serviceConfigurations,
            preCandidates: preCandidates,
            tripDistance: {
                value: tripGeoInformation.distanceValue,
                text: tripGeoInformation.distance
            },
            tripDuration: {
                value: tripGeoInformation.durationValue,
                text: tripGeoInformation.duration
            },
            date: new Date(),
            mode,
            routes: tripRoutes ? new Collection(tripRoutes) : undefined,
            serviceType: serviceType as any
        });

        newRequest.registerNewAttempt();

        if (props.candidate) {
            newRequest.setCurrentAttemptAlertedCandidate(props.candidate);
        }

        await this.tripRequestRepository.saveRequest(newRequest);

        return newRequest;
    }

    private async retryRequest(currentRequest: any) {
        currentRequest.registerNewAttempt();

        await this.tripRequestRepository.saveRequest(currentRequest);

        this.startAttemptService.execute(currentRequest);
    }
}
