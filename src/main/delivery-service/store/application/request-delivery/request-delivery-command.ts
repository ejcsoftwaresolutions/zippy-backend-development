import { RideGeoPoint } from '@deliveryService/delivery/domain/models/delivery-request-model';
import Command from '@shared/domain/bus/command/command';

export interface RequestDeliveryData {
  customerId: string;
  mode: 'live' | 'test';
  order: {
    id: string;
    items: any[];
    storeId: string;
    subtotal: number;
    total: number;
    date: Date;
    paymentMethod: string;
    deliveryFee: number;
    code: string;
  };
  preCandidates?: string[];
  customerLocation: RideGeoPoint;
  serviceType: string;
}

export default class RequestDeliveryCommand extends Command {
  constructor(public readonly props: RequestDeliveryData) {
    super();
  }

  get customerId(): string {
    return this.props.customerId;
  }

  get preCandidates() {
    return this.props.preCandidates;
  }

  get mode() {
    return this.props.mode;
  }

  get order() {
    return this.props.order;
  }

  get customerLocation() {
    return this.props.customerLocation;
  }

  get serviceType() {
    return this.props.serviceType;
  }

  name(): string {
    return RequestDeliveryCommand.name;
  }
}
