import ServiceConfigurationsQueryRepository
    from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import StoreOrder from '@deliveryService/store/domain/models/store-order-model';
import StorePublicProfileQueryRepository
    from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import CustomerPublicProfileQueryRepository
    from '../../../customer/domain/repositories/customer-public-profile-query-repository';
import RequestDeliveryCommand from './request-delivery-command';
import RequestDeliveryUseCase from './request-delivery-use-case';
import DeliveryRequestCommandRepository
    from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';

@service()
export default class RequestDeliveryCommandHandler implements CommandHandler {
    constructor(
        @inject('RequestDeliveryUseCase') private useCase: RequestDeliveryUseCase,
        @inject('CustomerPublicProfileQueryRepository')
        private customerPublicProfileQueryRepository: CustomerPublicProfileQueryRepository,
        @inject('StorePublicProfileQueryRepository')
        private storePublicProfileQueryRepository: StorePublicProfileQueryRepository,
        @inject('ServiceConfigurationsQueryRepository')
        private serviceConfigQueryRepository: ServiceConfigurationsQueryRepository,
        @inject('DeliveryRequestCommandRepository')
        private tripRequestRepository: DeliveryRequestCommandRepository
    ) {
    }

    getCommandName(): string {
        return RequestDeliveryCommand.name;
    }

    async handle(command: RequestDeliveryCommand) {
        const customer =
            await this.customerPublicProfileQueryRepository.findProfileById(
                command.customerId
            );

        const store = await this.storePublicProfileQueryRepository.findProfileById(
            command.order.storeId
        );

        if (!customer) {
            throw new Error('customer_not_found');
        }

        if (!store) {
            throw new Error('store_not_found');
        }

        const id = new Id(command.order.id);

        const serviceConfigurations =
            await this.serviceConfigQueryRepository.getConfigurations();

        await this.useCase.execute({
            id: id,
            customer: customer,
            order: StoreOrder.fromPrimitives({
                ...command.order,
                customerLocation: command.customerLocation,
                customerId: command.customerId,
                paymentMethod: command.order.paymentMethod as any,
                store: store.toPrimitives()
            }),
            preCandidates: command.preCandidates,
            serviceConfigurations: serviceConfigurations,
            mode: command.mode as any,
            customerLocation: command.customerLocation,
            serviceType: command.serviceType
        });

        return {
            id: id.value
        };
    }
}
