import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { ObjectUtils } from '@shared/domain/utils';

interface CustomerPublicProfileProps {
  id: string;
  email: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  profilePictureUrl?: string;
  createdAt?: Date;
  pushToken: string;
  configurations: {
    referralPromotions: boolean;
    receiveEmails: boolean;
    receivePushNotifications: boolean;
    receiveSMS: boolean;
  };
}

export type CustomerPublicProfilePrimitiveProps = CustomerPublicProfileProps;

const DefaultProps = {};

export default class CustomerPublicProfile extends AggregateRoot<CustomerPublicProfileProps> {
  constructor(props: CustomerPublicProfileProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get email() {
    return this.props.email;
  }

  get firstName() {
    return this.props.firstName;
  }

  get lastName() {
    return this.props.lastName;
  }

  get phone() {
    return this.props.phone;
  }

  get profilePictureUrl() {
    return this.props.profilePictureUrl;
  }

  get pushToken() {
    return this.props.pushToken;
  }

  get createdAt() {
    return this.props.createdAt;
  }

  get wantsToReceivePushNotifications() {
    return this.props.configurations.receivePushNotifications;
  }

  static create({
    ...plainData
  }: CustomerPublicProfileProps): CustomerPublicProfile {
    return new CustomerPublicProfile(plainData);
  }

  static fromPrimitives({
    ...plainData
  }: CustomerPublicProfilePrimitiveProps): CustomerPublicProfile {
    return new CustomerPublicProfile(plainData);
  }

  toPrimitives(): CustomerPublicProfilePrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
