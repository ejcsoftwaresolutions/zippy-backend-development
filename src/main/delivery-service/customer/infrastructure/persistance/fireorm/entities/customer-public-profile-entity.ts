import { Collection } from 'fireorm';

@Collection('users')
export class CustomerPublicProfileEntity {
  id: string;
  email: string;
  lastName: string;
  firstName: string;
  phone: string;
}
