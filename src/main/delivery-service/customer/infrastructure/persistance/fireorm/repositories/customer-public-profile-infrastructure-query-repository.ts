import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';
import CustomerPublicProfileQueryRepository from '@deliveryService/customer/domain/repositories/customer-public-profile-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { CustomerPublicProfileEntity } from '../entities/customer-public-profile-entity';
import CustomerPublicProfileMapper from '../mappers/customer-public-profile-mapper';
import firebase from 'firebase-admin';
import { PUSH_TOKENS } from '@deliveryService/delivery/infrastructure/persistance/fireorm/constants';
import { CUSTOMERS } from '../../../../../../../../functions/src/constants';

export default class CustomerPublicProfileQueryInfrastructureRepository
  extends FireOrmRepository<CustomerPublicProfileEntity>
  implements CustomerPublicProfileQueryRepository
{
  static readonly bindingKey = 'CustomerPublicProfileQueryRepository';

  getEntityClass(): EntityConstructorOrPath<CustomerPublicProfileEntity> {
    return CustomerPublicProfileEntity;
  }

  async findProfileById(id: string): Promise<CustomerPublicProfile | null> {
    const userAccount = await this.repository()
      .whereEqualTo('id', id)
      .findOne();

    if (!userAccount) return null;

    const profile = (
      await firebase.firestore().collection(CUSTOMERS).doc(userAccount.id).get()
    ).data();

    if (!profile) return null;

    const pushTokens = (
      await firebase
        .firestore()
        .collection(PUSH_TOKENS)
        .doc(userAccount.id)
        .get()
    ).data();

    return CustomerPublicProfileMapper.toDomain({
      id: userAccount.id,
      email: userAccount.email,
      firstName: userAccount.firstName,
      lastName: userAccount.lastName,
      phone: userAccount.phone,
      profilePictureURL: profile.profilePictureURL,
      pushToken: pushTokens?.customer,
      configurations: profile.configurations ?? {
        receiveEmails: false,
        receiveSMS: false,
        receivePushNotifications: false,
        referralPromotions: false,
      },
    });
  }
}
