import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';

export default class CustomerPublicProfileMapper {
  static toDomain(plainValues: {
    id: string;
    email: string;
    lastName: string;
    firstName: string;
    phone: string;
    profilePictureURL: string;
    pushToken: string;
    configurations: {
      referralPromotions: boolean;
      receiveEmails: boolean;
      receivePushNotifications: boolean;
      receiveSMS: boolean;
    };
  }): CustomerPublicProfile {
    return CustomerPublicProfile.fromPrimitives({
      id: plainValues.id,
      email: plainValues.email,
      firstName: plainValues.firstName,
      lastName: plainValues.lastName,
      phone: plainValues.phone,
      profilePictureUrl: plainValues.profilePictureURL,
      pushToken: plainValues.pushToken,
      configurations: plainValues.configurations,
    });
  }

  static toPersistence(user: CustomerPublicProfile): any {
    return user.toPrimitives();
  }
}
