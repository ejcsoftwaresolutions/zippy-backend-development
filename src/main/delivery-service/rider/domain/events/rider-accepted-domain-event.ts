import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'RIDER_ACCEPTED';

interface EventProps extends DomainEventProps {
  eventData: {
    riderId: string;
    vendorOrderId: string;
    vendorId: string;
    driverFee: number;
  };
}

export default class RiderAcceptedDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
