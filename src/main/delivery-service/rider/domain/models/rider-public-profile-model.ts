import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { ObjectUtils } from '@shared/domain/utils';

export interface RiderPublicProfileProps {
  id: string;
  email: string;
  rating?: number;
  firstName?: string;
  lastName?: string;
  phone?: string;
  profilePictureUrl?: string;
  deliveryMethod: string;
  createdAt?: Date;
  pushToken?: string;
  identificationCard?: string;
  configurations: {
    referralPromotions: boolean;
    receiveEmails: boolean;
    receivePushNotifications: boolean;
    receiveSMS: boolean;
  };
}

export type DriverPublicProfilePrimitiveProps = RiderPublicProfileProps;

const DefaultProps = {
  rating: 5,
};

export default class RiderPublicProfile extends AggregateRoot<RiderPublicProfileProps> {
  constructor(props: RiderPublicProfileProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get email() {
    return this.props.email;
  }

  get rating() {
    return this.props.rating;
  }

  get firstName() {
    return this.props.firstName;
  }

  get lastName() {
    return this.props.lastName;
  }

  get phone() {
    return this.props.phone;
  }

  get profilePictureUrl() {
    return this.props.profilePictureUrl;
  }

  get deliveryMethod() {
    return this.props.deliveryMethod;
  }

  get pushToken() {
    return this.props.pushToken;
  }

  get createdAt() {
    return this.props.createdAt;
  }

  get identificationCard() {
    return this.props.identificationCard;
  }

  get wantsToReceivePushNotifications() {
    return this.props.configurations.receivePushNotifications;
  }

  static create(data: RiderPublicProfileProps): RiderPublicProfile {
    return new RiderPublicProfile(data);
  }

  static fromPrimitives({
    ...plainData
  }: DriverPublicProfilePrimitiveProps): RiderPublicProfile {
    return new RiderPublicProfile(plainData);
  }

  toPrimitives(): DriverPublicProfilePrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }

  updateRating(newRate: number) {
    this.props.rating = Math.round(((this.props.rating ?? 0) + newRate) / 2);
  }
}
