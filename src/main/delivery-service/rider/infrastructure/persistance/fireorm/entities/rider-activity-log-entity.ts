import { Collection } from 'fireorm';

@Collection('driver_delivery_activity_logs')
export class RiderActivityLogEntity {
  id: string;
  driverId: string;
  vendorId: string;
  vendorOrderId: string;
  date: Date;
  event: string;
  fee: number;
}
