import { Collection } from 'fireorm';

@Collection('users')
export class RiderPublicProfileEntity {
  id: string;
  firstName?: string;
  lastName?: string;
  email: string;
  phone?: string;
}
