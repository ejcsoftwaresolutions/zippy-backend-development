import RiderActivityLogCommandRepository from '@deliveryService/rider/domain/repositories/rider-activity-log-command-repository';
import Id from '@shared/domain/id/id';
import { ObjectUtils } from '@shared/domain/utils';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { RiderActivityLogEntity } from '../entities/rider-activity-log-entity';

export default class RiderActivityLogCommandInfrastructureRepository
  extends FireOrmRepository<RiderActivityLogEntity>
  implements RiderActivityLogCommandRepository
{
  static readonly bindingKey = 'RiderActivityLogCommandRepository';

  getEntityClass(): EntityConstructorOrPath<RiderActivityLogEntity> {
    return RiderActivityLogEntity;
  }

  async log(event: {
    riderId: string;
    vendorId: string;
    vendorOrderId: string;
    date: Date;
    event: string;
    fee: number;
  }): Promise<void> {
    const entity = new RiderActivityLogEntity();

    entity.id = new Id().value;
    entity.driverId = event.riderId;
    entity.vendorId = event.vendorId;
    entity.vendorOrderId = event.vendorOrderId;
    entity.date = event.date;
    entity.event = event.event;
    entity.fee = event.fee;

    await this.repository().create(ObjectUtils.omitUnknown(entity));
  }
}
