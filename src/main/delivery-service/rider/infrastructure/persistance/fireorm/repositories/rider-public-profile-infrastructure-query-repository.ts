import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';
import RiderPublicProfileQueryRepository from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { RiderPublicProfileEntity } from '../entities/rider-public-profile-entity';
import RiderPublicProfileMapper from '../mappers/rider-public-profile-mapper';
import firebase from 'firebase-admin';
import {
  PUSH_TOKENS,
  RIDERS,
} from '@deliveryService/delivery/infrastructure/persistance/fireorm/constants';

export default class RiderPublicProfileQueryInfrastructureRepository
  extends FireOrmRepository<RiderPublicProfileEntity>
  implements RiderPublicProfileQueryRepository
{
  static readonly bindingKey = 'RiderPublicProfileQueryRepository';

  getEntityClass(): EntityConstructorOrPath<RiderPublicProfileEntity> {
    return RiderPublicProfileEntity;
  }

  async findProfileById(id: string): Promise<RiderPublicProfile | null> {
    const userAccount = await this.repository()
      .whereEqualTo('id', id)
      .findOne();

    if (!userAccount) return null;

    const profile = (
      await firebase.firestore().collection(RIDERS).doc(userAccount.id).get()
    ).data();

    if (!profile) return null;

    const pushTokens = (
      await firebase
        .firestore()
        .collection(PUSH_TOKENS)
        .doc(userAccount.id)
        .get()
    ).data();

    return RiderPublicProfileMapper.toDomain({
      id: userAccount.id,
      email: userAccount.email,
      firstName: userAccount.firstName,
      lastName: userAccount.lastName,
      phone: userAccount.phone,
      identificationCard: profile.identificationCard,
      deliveryMethod: profile.deliveryMethod,
      profilePictureUrl: profile.profilePictureURL,
      reviewsCount: profile.reviewsCount,
      reviewsSum: profile.reviewsSum,
      pushToken: pushTokens?.rider,
      configurations: profile.configurations ?? {
        receiveEmails: false,
        receiveSMS: false,
        receivePushNotifications: false,
        referralPromotions: false,
      },
    });
  }
}
