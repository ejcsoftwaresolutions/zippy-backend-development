import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';

export default class RiderPublicProfileMapper {
  static toDomain(plainValues: {
    id: string;
    firstName?: string;
    lastName?: string;
    email: string;
    phone?: string;
    deliveryMethod: string;
    pushToken: string;
    identificationCard: string;
    profilePictureUrl: string;
    reviewsSum: number;
    reviewsCount: number;
    configurations: {
      referralPromotions: boolean;
      receiveEmails: boolean;
      receivePushNotifications: boolean;
      receiveSMS: boolean;
    };
  }): RiderPublicProfile {
    return RiderPublicProfile.fromPrimitives({
      id: plainValues.id,
      email: plainValues.email,
      firstName: plainValues.firstName,
      lastName: plainValues.lastName,
      phone: plainValues.phone,
      profilePictureUrl: plainValues.profilePictureUrl,
      rating: RiderPublicProfileMapper.getRiderRating(plainValues),
      deliveryMethod: plainValues.deliveryMethod,
      pushToken: plainValues.pushToken,
      identificationCard: plainValues.identificationCard,
      configurations: plainValues.configurations,
    });
  }

  static getRiderRating(item) {
    let reviewAvg =
      item.reviewsCount === undefined
        ? 0
        : Math.round(item.reviewsSum / item.reviewsCount);
    reviewAvg = Number(Math.round((reviewAvg + 'e' + 2) as any) + 'e-' + 2);
    if (Number.isNaN(reviewAvg)) {
      reviewAvg = Number(0);
    }
    return reviewAvg;
  }

  /* Only used to save in delivery */
  static toPersistence(user: RiderPublicProfile): {
    id: string;
    firstName?: string;
    lastName?: string;
    email: string;
    phone?: string;
    rating?: number;
  } {
    return user.toPrimitives();
  }
}
