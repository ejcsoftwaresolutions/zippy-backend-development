import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import RiderRejectedDomainEvent from '@deliveryService/rider/domain/events/rider-rejected-domain-event';
import EventBus from '@shared/domain/bus/event/event-bus';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';

@service()
export default class RejectDeliveryRequestUseCase {
  constructor(
    @inject('services.request.attempt.countdown.timer')
    private requestTimerService: RequestAttemptCountdownTimer,
    @inject('event.bus')
    private eventBus: EventBus,
  ) {}

  async execute(request: DeliveryRequest, driverId: string) {
    this.requestTimerService.toggleInterrupted(request.id.value, true);

    this.eventBus.publish([
      new RiderRejectedDomainEvent({
        occurredOn: new Date(),
        aggregateId: driverId,
        eventData: {
          riderId: driverId,
          vendorId: request.requestInfo.storeId,
          vendorOrderId: request.requestInfo.id,
          driverFee: request.finalFee.amount,
        },
      }),
    ]);
  }
}
