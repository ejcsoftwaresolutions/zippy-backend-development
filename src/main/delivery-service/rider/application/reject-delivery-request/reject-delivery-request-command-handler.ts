import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import RejectDeliveryRequestCommand from './reject-delivery-request-command';
import RejectDeliveryRequestUseCase from './reject-delivery-request-use-case';

@service()
export default class RejectDeliveryRequestCommandHandler
  implements CommandHandler
{
  constructor(
    @inject('RejectDeliveryRequestUseCase')
    private useCase: RejectDeliveryRequestUseCase,
    @inject('DeliveryRequestCommandRepository')
    private deliveryRequestRepository: DeliveryRequestCommandRepository,
  ) {}

  getCommandName(): string {
    return RejectDeliveryRequestCommand.name;
  }

  async handle(command: RejectDeliveryRequestCommand) {
    const request = await this.deliveryRequestRepository.getRequest(
      command.requestId,
    );

    if (request.currentAlertedCandidate.id !== command.driverId) {
      return;
    }

    await this.useCase.execute(request, command.driverId);
  }
}
