import DeliveryRequestCommandRepository
    from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import AssignDeliveryOrderCommand from './assign-delivery-order-command';
import RequestDeliveryUseCase from '@deliveryService/store/application/request-delivery/request-delivery-use-case';
import StoreOrderQueryRepository from '@deliveryService/store/domain/repositories/store-order-query-repository';
import Id from '@shared/domain/id/id';
import CustomerPublicProfileQueryRepository
    from '@deliveryService/customer/domain/repositories/customer-public-profile-query-repository';
import StorePublicProfileQueryRepository
    from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import ServiceConfigurationsQueryRepository
    from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import DeliveryRequestRiderCandidate
    from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';
import RiderPublicProfileQueryRepository
    from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import AcceptDeliveryRequestUseCase
    from '@deliveryService/rider/application/accept-delivery-request/accept-delivery-request-use-case';

@service()
export default class AssignDeliveryOrderCommandHandler implements CommandHandler {
    constructor(
        @inject('AcceptDeliveryRequestUseCase')
        private useCase: AcceptDeliveryRequestUseCase,
        @inject('DeliveryRequestCommandRepository')
        private deliveryRequestRepository: DeliveryRequestCommandRepository,
        @inject('StoreOrderQueryRepository')
        private orderRepository: StoreOrderQueryRepository,
        @inject('CustomerPublicProfileQueryRepository')
        private customerPublicProfileQueryRepository: CustomerPublicProfileQueryRepository,
        @inject('RiderPublicProfileQueryRepository')
        private riderPublicProfileQueryRepository: RiderPublicProfileQueryRepository,
        @inject('StorePublicProfileQueryRepository')
        private storePublicProfileQueryRepository: StorePublicProfileQueryRepository,
        @inject('ServiceConfigurationsQueryRepository')
        private serviceConfigQueryRepository: ServiceConfigurationsQueryRepository,
        @inject('RequestDeliveryUseCase')
        private requestDeliveryUseCase: RequestDeliveryUseCase
    ) {
    }

    getCommandName(): string {
        return AssignDeliveryOrderCommand.name;
    }

    async handle(command: AssignDeliveryOrderCommand) {

        let currentRequest: DeliveryRequest | undefined;
        const riderProfile = await this.riderPublicProfileQueryRepository.findProfileById(command.driverId);

        if (!riderProfile) throw new Error('rider_not_found');

        const candidate = DeliveryRequestRiderCandidate.fromPrimitives({
            id: command.driverId,
            attemptNumber: 1,
            initialCoords: {
                address: '',
                geoLocation: [-1, -1]
            },
            isBlockedToTakeRequest: false,
            notification: null,
            publicProfile: riderProfile.toPrimitives()
        });

        try {
            currentRequest = await this.deliveryRequestRepository.getRequest(command.orderId);
        } catch (e) {
            if (e.message == 'request_not_found') {
                currentRequest = await this.createDeliveryRequest(command, candidate);
            }
        }

        if (!currentRequest) throw new Error('empty_request');

        await this.useCase.setRiderToOrder({
            request: currentRequest,
            driver: candidate,
            assigned: true
        });
    }


    private async createDeliveryRequest(command: AssignDeliveryOrderCommand, candidate: DeliveryRequestRiderCandidate) {

        const storeOrder = await this.orderRepository.findById(command.orderId);

        if (!storeOrder) {
            throw new Error('order_not_found');
        }

        const customer = await this.customerPublicProfileQueryRepository.findProfileById(
            storeOrder.customerId
        );

        if (!customer) {
            throw new Error('customer_not_found');
        }

        const id = new Id(command.orderId);

        const serviceConfigurations =
            await this.serviceConfigQueryRepository.getConfigurations();

        const request = await this.requestDeliveryUseCase.saveRequest({
            id: id,
            customer: customer,
            order: storeOrder,
            serviceConfigurations: serviceConfigurations,
            mode: 'live',
            customerLocation: storeOrder.customerLocation,
            serviceType: 'CAR',
            candidate: candidate
        });

        return request;
    }

}
