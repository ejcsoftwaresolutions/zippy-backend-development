import Command from '@shared/domain/bus/command/command';

export default class AssignDeliveryOrderCommand extends Command {
    constructor(
        public readonly driverId: string,
        public readonly orderId: string
    ) {
        super();
    }

    name(): string {
        return AssignDeliveryOrderCommand.name;
    }
}
