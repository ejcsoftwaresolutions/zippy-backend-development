import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import DeliveryRequestRiderCandidate from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';
import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import AcceptDeliveryRequestCommand from './accept-delivery-request-command';
import AcceptDeliveryRequestUseCase from './accept-delivery-request-use-case';

@service()
export default class AcceptDeliveryRequestCommandHandler
  implements CommandHandler
{
  constructor(
    @inject('AcceptDeliveryRequestUseCase')
    private useCase: AcceptDeliveryRequestUseCase,
    @inject('DeliveryRequestCommandRepository')
    private deliveryRequestRepository: DeliveryRequestCommandRepository,
    @inject('services.request.attempt.countdown.timer')
    private requestTimerService: RequestAttemptCountdownTimer,
  ) {}

  getCommandName(): string {
    return AcceptDeliveryRequestCommand.name;
  }

  async handle(command: AcceptDeliveryRequestCommand) {
    const request = await this.deliveryRequestRepository.getRequest(
      command.requestId,
    );

    if (!request) {
      throw new Error('request_not_found');
    }

    const currentDriver = request.currentAlertedCandidate;

    this.guardProperResponse(request, currentDriver);

    await this.useCase.execute(request, currentDriver);
  }

  private guardProperResponse(
    request: DeliveryRequest,
    driver: DeliveryRequestRiderCandidate,
  ) {
    if (
      this.requestTimerService.get(request.id.value).candidateId !== driver.id
    ) {
      throw new Error('too_late_to_accept');
    }
  }
}
