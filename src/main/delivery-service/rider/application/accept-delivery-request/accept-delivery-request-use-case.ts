import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import Delivery from '@deliveryService/delivery/domain/models/delivery-model';
import DeliveryRequestRiderCandidate
    from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';
import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import DeliveryCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-command-repository';
import DeliveryRequestCommandRepository
    from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import RiderAcceptedDomainEvent from '@deliveryService/rider/domain/events/rider-accepted-domain-event';
import EventBus from '@shared/domain/bus/event/event-bus';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';

@service()
export default class AcceptDeliveryRequestUseCase {
    private request: DeliveryRequest;

    constructor(
        @inject('services.request.attempt.countdown.timer')
        private requestTimerService: RequestAttemptCountdownTimer,
        @inject('DeliveryRequestCommandRepository')
        private deliveryRequestRepository: DeliveryRequestCommandRepository,
        @inject('DeliveryCommandRepository')
        private deliveryCommandRepository: DeliveryCommandRepository,
        @inject('event.bus')
        private eventBus: EventBus
    ) {
    }

    async execute(
        request: DeliveryRequest,
        driver: DeliveryRequestRiderCandidate
    ) {
        this.request = request;

        this.requestTimerService.stop(this.request.id.value);
        this.requestTimerService.destroy(this.request.id.value);

        await this.setRiderToOrder({
            request, driver
        });
    }

    async setRiderToOrder({ request, driver, assigned }: {
        request: DeliveryRequest,
        driver: DeliveryRequestRiderCandidate,
        assigned?: boolean
    }) {

        console.log(
            `${request.id.value}
            'El conductor  ${driver.id}  acepto la solicitud`
        );

        request.setAcceptedRider(driver);

        if (!!request.currentAlertedCandidate) {
            request.currentAlertedCandidate.setNotification(null);
            await this.deliveryRequestRepository.updateDriverNotification(
                request.currentAlertedCandidate,
                request.mode
            );
        }

        await this.deliveryRequestRepository.updateRequest(request); // don't remove

        await this.deliveryRequestRepository.deleteRequest(request);

        this.afterAccepted({
            request,
            driver,
            assigned
        });

        this.eventBus.publish([
            new RiderAcceptedDomainEvent({
                occurredOn: new Date(),
                aggregateId: driver.id,
                eventData: {
                    riderId: driver.id,
                    vendorId: request.requestInfo.storeId,
                    vendorOrderId: request.requestInfo.id,
                    driverFee: request.toPrimitives().finalFee.amount
                }
            })
        ]);
    }

    private afterAccepted({
        request,
        driver,
        assigned
    }: {
        request: DeliveryRequest,
        driver: DeliveryRequestRiderCandidate,
        assigned?: boolean
    }) {
        const newDelivery = Delivery.fromDeliveryRequest(request);

        if (assigned) {
            newDelivery.setAssigned();
        }

        this.deliveryCommandRepository
        .createDelivery(newDelivery)
        .then(() => {
            console.log('Delivery created');
        })
        .catch((e) => {
        });
    }
}
