import Command from '@shared/domain/bus/command/command';

export default class AcceptDeliveryRequestCommand extends Command {
  constructor(
    public readonly driverId: string,
    public readonly requestId: string,
  ) {
    super();
  }

  name(): string {
    return AcceptDeliveryRequestCommand.name;
  }
}
