import RiderAcceptedDomainEvent from '@deliveryService/rider/domain/events/rider-accepted-domain-event';
import RiderRejectedDomainEvent from '@deliveryService/rider/domain/events/rider-rejected-domain-event';
import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import LogDriverActivityUseCase from './log-driver-activity-use-case';

@service()
export default class LogDriverActivityEventSubscriber
  implements EventSubscriber
{
  constructor(
    @inject('LogDriverActivityUseCase')
    private useCase: LogDriverActivityUseCase,
  ) {}

  public subscribedTo(): Map<string, Function> {
    const events = new Map<string, Function>();
    events.set(
      RiderAcceptedDomainEvent.eventName,
      this.handleAccepted.bind(this),
    );
    events.set(
      RiderRejectedDomainEvent.eventName,
      this.handleRejected.bind(this),
    );

    return events;
  }

  public async handleAccepted(event: RiderAcceptedDomainEvent) {
    this.useCase.execute({
      ...event.props.eventData,
      date: event.occurredOn,
      event: 'ORDER_ACCEPTED',
      fee: event.props.eventData.driverFee,
    });
  }

  public async handleRejected(event: RiderRejectedDomainEvent) {
    this.useCase.execute({
      ...event.props.eventData,
      date: event.occurredOn,
      event: 'ORDER_REJECTED',
      fee: event.props.eventData.driverFee,
    });
  }
}
