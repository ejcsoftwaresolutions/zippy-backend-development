import RiderActivityLogCommandRepository from '@deliveryService/rider/domain/repositories/rider-activity-log-command-repository';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';

@service()
export default class LogDriverActivityUseCase {
  constructor(
    @inject('RiderActivityLogCommandRepository')
    private driverLogRepo: RiderActivityLogCommandRepository,
  ) {}

  async execute(data: {
    riderId: string;
    vendorOrderId: string;
    vendorId: string;
    date: Date;
    event: 'ORDER_REJECTED' | 'ORDER_ACCEPTED' | 'ORDER_COMPLETED';
    fee: number;
  }) {
    this.driverLogRepo.log({
      riderId: data.riderId,
      vendorId: data.vendorId,
      vendorOrderId: data.vendorOrderId,
      date: data.date,
      event: data.event,
      fee: data.fee,
    });
  }
}
