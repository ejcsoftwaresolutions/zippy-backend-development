import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import { Timer } from '@shared/infrastructure/utils/interval-timer';
import RequestAttemptCountdownTimer from '../../domain/contracts/request-attempt-countdown-timer';

@service()
export default class IntervalTimerRequestAttemptCountdownTimer
  implements RequestAttemptCountdownTimer
{
  constructor(
    @inject('delivery.request.timers')
    private deliveryRequestTimers: {
      [requestId: string]: {
        candidateId?: string;
        timer: any;
        interrupted: boolean;
      };
    },
  ) {}

  initialize(
    requestId: string,
    startAtSecond: number,
  ): { candidateId?: string; timer: Timer } {
    const options = {
      startTime: startAtSecond * 1000,
      updateFrequency: 1000,
      countdown: true,
    };
    this.deliveryRequestTimers[requestId] = {
      timer: new Timer(options),
      interrupted: false,
    };

    return this.deliveryRequestTimers[requestId];
  }

  get(requestId: string): {
    candidateId?: string;
    timer: Timer;
    interrupted: boolean;
  } {
    return this.deliveryRequestTimers[requestId];
  }

  exists(requestId: string) {
    return !!this.deliveryRequestTimers[requestId];
  }

  destroy(requestId: string) {
    if (!this.exists(requestId)) throw new Error('timer_not_initialized');

    this.deliveryRequestTimers[requestId].timer.destroy();

    delete this.deliveryRequestTimers[requestId];
  }

  toggleInterrupted(requestId: string, interrupted: boolean): void {
    if (!this.exists(requestId)) throw new Error('timer_not_initialized');
    this.deliveryRequestTimers[requestId].interrupted = interrupted;
  }

  start(requestId: string, candidateId: string) {
    if (!this.exists(requestId)) throw new Error('timer_not_initialized');
    this.deliveryRequestTimers[requestId].candidateId = candidateId;
    this.deliveryRequestTimers[requestId].interrupted = false;

    this.deliveryRequestTimers[requestId].timer.start();
  }

  stop(requestId: string) {
    if (!this.exists(requestId)) throw new Error('timer_not_initialized');

    this.deliveryRequestTimers[requestId].interrupted = false;
    this.deliveryRequestTimers[requestId].timer.stop();
  }

  reset(requestId: string) {
    if (!this.exists(requestId)) throw new Error('timer_not_initialized');

    this.deliveryRequestTimers[requestId].interrupted = false;
    this.deliveryRequestTimers[requestId].candidateId = undefined;
    this.deliveryRequestTimers[requestId].timer.reset();
  }

  onTimeout(requestId: string, callback: Function) {
    if (!this.exists(requestId)) throw new Error('timer_not_initialized');

    this.deliveryRequestTimers[requestId].timer.addEventListener('end', () => {
      callback();
    });
  }

  onUpdate(requestId: string, callback: Function) {
    if (!this.exists(requestId)) throw new Error('timer_not_initialized');

    this.deliveryRequestTimers[requestId].timer.addEventListener(
      'update',
      (rs: any) => {
        callback(rs.getTime.seconds);
      },
    );
  }
}
