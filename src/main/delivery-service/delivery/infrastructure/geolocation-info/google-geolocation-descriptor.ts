import service from '@shared/domain/decorators/service';
import TripGeolocationDescriptor, {
  TripGeoInformation,
} from '../../domain/contracts/trip-geolocation-descriptor';
const distance = require('google-distance');

@service()
export default class GoogleGeolocationDescriptor
  implements TripGeolocationDescriptor
{
  constructor() {
    if (!process.env.GOOGLE_API_KEY) {
      throw new Error('Not google API set in env');
    }
    distance.apiKey = process.env.GOOGLE_API_KEY;
  }

  async basicInfo(
    origin: string,
    destination: string,
  ): Promise<TripGeoInformation> {
    return new Promise((resolve, reject) => {
      distance.get(
        {
          origin: origin,
          destination: destination,
          language: 'es',
        },
        (err: any, data: any) => {
          if (err) {
            console.log(err);
            return resolve(undefined);
          }

          return resolve({
            originAddress: data.origin,
            destinationAddress: data.destination,
            distance: data.distance,
            distanceValue: parseFloat((data.distanceValue / 1000).toFixed(2)), // meters to kilometers
            duration: data.duration,
            durationValue: parseFloat((data.durationValue / 60).toFixed(2)), // seconds to minutes
          });
        },
      );
    });
  }
}
