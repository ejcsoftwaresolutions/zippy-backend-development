import { injectable } from '@shared/domain/decorators';

const moment = require('moment-timezone');

@injectable()
export default class RestaurantOrdersHandler {
  constructor() {}

  onModuleInit() {
    this.execute();
  }

  async execute() {
    if (process.env.NODE_ENV !== 'development') return;
  }
}
