import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';
import Delivery, {
  DeliveryStatus,
} from '@deliveryService/delivery/domain/models/delivery-model';
import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';
import { DeliveryEntity } from '../entities/delivery-entity';

export default class DeliveryMapper {
  static toDomain(
    plainValues: DeliveryEntity & {
      riderPublicProfile: RiderPublicProfile;
      customerPublicProfile: CustomerPublicProfile;
      serviceConfig: ServiceConfigurations;
    },
  ): Delivery {
    return Delivery.fromPrimitives({
      id: plainValues.id,
      customer: plainValues.customerPublicProfile.toPrimitives(),
      rider: plainValues.riderPublicProfile.toPrimitives(),
      acceptedAt: plainValues.acceptedAt,
      distance: plainValues.distance,
      duration: plainValues.duration,
      finalFee: plainValues.finalFee,
      requestedAt: plainValues.requestedAt,
      status: plainValues.status as DeliveryStatus,
      mode: plainValues.mode as any,
      routes: plainValues.routes,
      serviceConfig: plainValues.serviceConfig,
      orderDetails: plainValues.orderDetails,
      leftStoreAt: plainValues.leftStoreAt,
      deliveredAt: plainValues.deliveredAt,
    });
  }

  static toPersistence(delivery: Delivery): DeliveryEntity {
    const primitives = delivery.toPrimitives();

    const entity = new DeliveryEntity();

    entity.id = delivery.id.value;
    entity.driverId = primitives.rider.id;
    entity.customerId = primitives.customer.id;
    entity.storeId = primitives.orderDetails.store.id;
    entity.acceptedAt = primitives.acceptedAt;
    entity.distance = primitives.distance;
    entity.duration = primitives.duration;
    entity.finalFee = primitives.finalFee;
    entity.requestedAt = new Date(); //primitives.requestedAt;
    entity.status = primitives.status;
    entity.mode = primitives.mode;
    entity.routes = primitives.routes;
    entity.orderDetails = primitives.orderDetails;
    entity.customerInfo = primitives.customer;
    entity.driverInfo = primitives.rider;
    entity.leftStoreAt = primitives.leftStoreAt;
    entity.deliveredAt = primitives.deliveredAt;
    entity.assigned = primitives.assigned;

    return entity;
  }
}
