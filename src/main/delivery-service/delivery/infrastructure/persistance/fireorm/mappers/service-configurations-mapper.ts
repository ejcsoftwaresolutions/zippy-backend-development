import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import { ServiceConfigurationsEntity } from '../entities/service-configurations-entity';

export default class ServiceConfigurationsMapper {
  static toDomain(
    plainValues: ServiceConfigurationsEntity,
  ): ServiceConfigurations {
    return ServiceConfigurations.fromPrimitives({
      feeConfigurations: plainValues.feeConfigurations,
      searchConfigurations: {
        acceptanceTimeout: plainValues.searchConfigurations.acceptanceTimeout,
        maxAttempts: plainValues.searchConfigurations.maxAttempts,
        attemptMaxCandidates:
          plainValues.searchConfigurations.attemptMaxCandidates,
        attemptSearchRadius:
          plainValues.searchConfigurations.attemptSearchRadius,
      },
    });
  }
}
