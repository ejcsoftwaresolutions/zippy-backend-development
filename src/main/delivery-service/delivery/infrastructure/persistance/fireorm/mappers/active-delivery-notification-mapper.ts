import Delivery from '@deliveryService/delivery/domain/models/delivery-model';
import { ObjectUtils } from '@shared/domain/utils';
import ActiveDeliveryNotificationEntity from '../entities/active-delivery-notification-entity';

export default class ActiveDeliveryNotificationMapper {
  static toPersistence(delivery: Delivery): ActiveDeliveryNotificationEntity {
    const primitives = delivery.toPrimitives();

    return {
      id: primitives.id,
      finalFee: primitives.finalFee,
      customer: ObjectUtils.omitUnknown(primitives.customer),
      acceptedDriver: ObjectUtils.omitUnknown(primitives.rider),
      tripDistance: primitives.distance,
      tripDuration: primitives.duration,
      status: primitives.status,
      requestInfo: primitives.orderDetails,
    };
  }
}
