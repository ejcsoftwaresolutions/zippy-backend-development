import { Collection } from 'fireorm';

@Collection('app_configurations')
export class ServiceConfigurationsEntity {
  id: string;
  searchConfigurations: any;
  feeConfigurations: any;
  globalFees: any;
}
