import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import TestDeliveryRequestsCommandRepository from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';
import { inject } from '@shared/domain/decorators';
import Id from '@shared/domain/id/id';
import {
  FIREBASE_ADMIN_INJECT,
  FirebaseAdminSDK,
} from '@tfarras/nestjs-firebase-admin';
import { RIDERS_LOCATIONS, TEST_CUSTOMERS } from '../constants';

const geoFire = require('geofire');
const randomLocation = require('random-location');

type MockDriver = {
  location: number[];
  id: string;
};

export default class TestDeliveryRequestCommandInfrastructureRepository
  implements TestDeliveryRequestsCommandRepository
{
  static readonly bindingKey = 'TestDeliveryRequestsCommandRepository';

  constructor(
    @inject('ServiceConfigurationsQueryRepository')
    private serviceConfigQueryRepository: ServiceConfigurationsQueryRepository,
    @inject(FIREBASE_ADMIN_INJECT) private firebase: FirebaseAdminSDK,
  ) {}

  async insertMockCandidatesInRadius(
    center: { latitude: number; longitude: number },
    radius: number,
  ): Promise<any> {
    const P = center;

    const R = radius; // meters

    const drivers: MockDriver[] = [...Array(5)].map((mockDriver) => {
      const randomPoint = randomLocation.randomCirclePoint(P, R);
      const id = new Id().value;

      return {
        location: [randomPoint.latitude, randomPoint.longitude],
        id: id,
      };
    });

    const plainCandidates: MockDriver[] = drivers;

    const promises = plainCandidates.map((candidate) => {
      const ref = this.firebase.database().ref(`${RIDERS_LOCATIONS}_test`);
      const geoFireInstance = new geoFire.GeoFire(ref);

      return geoFireInstance.set(candidate.id, candidate.location);
    });

    await Promise.all(promises);

    return plainCandidates;
  }

  async createTestRider(
    id: Id,
    serviceType,
    position: { latitude: number; longitude: number },
  ): Promise<any> {
    const ref = this.firebase.database().ref(`${RIDERS_LOCATIONS}_test`);
    const geoFireInstance = new geoFire.GeoFire(ref);

    await geoFireInstance.set(id.value, [
      position.latitude,
      position.longitude,
    ]);
  }

  async createTestCustomer(
    id: Id,
    position: { latitude: number; longitude: number },
  ): Promise<any> {
    const ref = this.firebase.database().ref(`${TEST_CUSTOMERS}`);
    const geoFireInstance = new geoFire.GeoFire(ref);

    await geoFireInstance.set(id.value, [
      position.latitude,
      position.longitude,
    ]);
  }

  async removeTestCustomer(id: Id): Promise<any> {
    await this.firebase
      .database()
      .ref(`${TEST_CUSTOMERS}/${id.value}`)
      .remove();
  }

  async removeTestDriver(id: Id, serviceType: string): Promise<any> {
    await this.firebase
      .database()
      .ref(
        `${RIDERS_LOCATIONS}_test/${serviceType
          .replace(/\./g, '_')
          .toUpperCase()}/${id.value}`,
      )
      .remove();
  }

  async removeAllCustomers(): Promise<any> {
    await this.firebase.database().ref(`${TEST_CUSTOMERS}`).remove();
  }

  async removeAllDrivers(): Promise<any> {
    await this.firebase.database().ref(`${RIDERS_LOCATIONS}_test`).remove();
  }
}
