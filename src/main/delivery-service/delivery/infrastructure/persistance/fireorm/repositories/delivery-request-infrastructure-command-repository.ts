import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import DeliveryRequestRiderCandidate from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import RiderPublicProfileQueryRepository from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import { inject } from '@shared/domain/decorators';
import { ArrayUtils, ObjectUtils } from '@shared/domain/utils';
import Collection from '@shared/domain/value-object/collection';
import {
  FIREBASE_ADMIN_INJECT,
  FirebaseAdminSDK,
} from '@tfarras/nestjs-firebase-admin';
import { RIDERS_ACTIVE_ORDERS } from '../../../../../../../../functions/src/constants';
import {
  AVAILABLE_RIDERS,
  BUSY_RIDERS,
  DELIVERY_REQUESTS,
  DRIVER_ALERTS,
  RESTAURANT_ORDERS,
} from '../constants';

const geoFire = require('geofire');

export default class DeliveryRequestCommandInfrastructureRepository
  implements DeliveryRequestCommandRepository
{
  static readonly bindingKey = 'DeliveryRequestCommandRepository';

  constructor(
    @inject('ServiceConfigurationsQueryRepository')
    private serviceConfigQueryRepository: ServiceConfigurationsQueryRepository,
    @inject('RiderPublicProfileQueryRepository')
    private driverPublicProfileRepo: RiderPublicProfileQueryRepository,
    @inject(FIREBASE_ADMIN_INJECT) private firebase: FirebaseAdminSDK,
  ) {}

  saveRequest(request: DeliveryRequest): void {
    const primitiveData = request.toPrimitives();

    this.firebase
      .database()
      .ref(this.getCollectionName(DELIVERY_REQUESTS, primitiveData.mode))
      .child(request.id.value)
      .set(ObjectUtils.omitUnknown(primitiveData))
      .then(() => {
        // console.log('saved');
      })
      .catch(() => {
        console.log('error saving request');
      });

    this.updateRestaurantOrderStatus(
      request.requestInfo.id,
      'Driver Pending',
    ).catch((error) => {
      console.log(error.message);
    });
  }

  async getRequest(requestId: string): Promise<DeliveryRequest> {
    const liveRequestSnap = await this.firebase
      .database()
      .ref(this.getCollectionName(DELIVERY_REQUESTS, 'live'))
      .child(requestId)
      .once('value');

    const testRequestSnap = await this.firebase
      .database()
      .ref(this.getCollectionName(DELIVERY_REQUESTS, 'test'))
      .child(requestId)
      .once('value');

    if (!liveRequestSnap.exists() && !testRequestSnap.exists()) {
      throw new Error('request_not_found');
    }

    const requestData = liveRequestSnap.exists()
      ? liveRequestSnap.val()
      : testRequestSnap.val();

    const serviceConfiguration =
      await this.serviceConfigQueryRepository.getConfigurations();

    return DeliveryRequest.fromPrimitives({
      ...requestData,
      serviceConfig: serviceConfiguration,
      date: new Date(requestData.date),
      preCandidates: requestData.preCandidates,
    });
  }

  async deleteRequest(request: DeliveryRequest): Promise<void> {
    try {
      await this.firebase
        .database()
        .ref(this.getCollectionName(DELIVERY_REQUESTS, request.mode))
        .child(request.id.value)
        .set(null);
    } catch (e) {
      console.log('error deleting request');
    }
  }

  async findClosestRiderCandidate(
    request: DeliveryRequest,
  ): Promise<DeliveryRequestRiderCandidate> {
    const { selected: nearestCandidatesInService } = await (async () => {
      const params = {
        radius: request.currentAttemptRadius,
        origin: request.requestInfo.storeLocation.geoLocation,
        mode: request.mode,
        attemptNumber: request.attemptNumber,
      };

      if (request.preCandidate) {
        return this.getPreCandidateDriver(
          {
            ...params,
            id: request.preCandidate,
          },
          request.discardedCandidates,
        );
      }

      console.log('Normal mode');

      return this.searchClosestDriversInService(
        {
          ...params,
        },
        request.discardedCandidates,
      );
    })();

    if (!nearestCandidatesInService) return undefined;

    const publicProfile = await this.driverPublicProfileRepo.findProfileById(
      nearestCandidatesInService.key,
    );

    if (!publicProfile) return undefined;

    return new DeliveryRequestRiderCandidate({
      id: nearestCandidatesInService.key,
      distanceToClient: nearestCandidatesInService.distance,
      publicProfile: publicProfile,
      isBlockedToTakeRequest: true,
      attemptNumber: request.attemptNumber,
      initialCoords: {
        address: '',
        geoLocation: nearestCandidatesInService.location,
      },
    });
  }

  async updateDriverNotification(
    candidate: DeliveryRequestRiderCandidate,
    mode: string,
  ) {
    await this.firebase
      .database()
      .ref(this.getCollectionName(DRIVER_ALERTS, mode))
      .child(candidate.id)
      .transaction((c) => {
        c = candidate.notification
          ? {
              ...ObjectUtils.omitUnknown({
                ...candidate.notification,
                countDown: candidate.countDown,
              }),
            }
          : null;
        return c;
      });
  }

  async updateRequest(request: DeliveryRequest): Promise<void> {
    const changes = {};

    const requestChanges = ObjectUtils.omitUnknown(
      ObjectUtils.omitUnknown(
        ObjectUtils.omit(request.toPrimitives(), ['date']),
      ),
    );

    changes[
      `${this.getCollectionName(DELIVERY_REQUESTS, request.mode)}/${
        request.id.value
      }`
    ] = requestChanges;

    try {
      await this.firebase.database().ref().update(changes);
    } catch (e) {}

    if (
      [
        'NOT_FOUND_MAX_ATTEMPTS_REACHED',
        'DRIVERS_NOT_FOUND_IN_ATTEMPT',
      ].includes(request.status)
    ) {
      this.updateRestaurantOrderStatus(
        request.requestInfo.id,
        'Driver Not Found',
      ).catch((error) => {
        console.log(error.message);
      });
    }
  }

  public async toggleLockedCandidates(
    candidates: Collection<DeliveryRequestRiderCandidate>,
    mode: string,
    isLocked: boolean,
  ) {
    let filteredCandidates = candidates;

    if (!isLocked) {
      const activeDeliveries = await this.getRidersActiveDeliveries(
        candidates.map((c) => c.id),
      );
      filteredCandidates = filteredCandidates.filter(
        (c, index) => !activeDeliveries[index],
      );
    }

    return this.toggleBlockDrivers(
      filteredCandidates.map((candidate) => ({
        id: candidate.id,
      })),
      mode,
      isLocked,
    );
  }

  private async getRidersActiveDeliveries(ridersIds: string[]) {
    try {
      const deliveries = await Promise.all(
        ridersIds.map((e) => {
          return new Promise(async (resolve) => {
            const d = await this.firebase
              .database()
              .ref(RIDERS_ACTIVE_ORDERS)
              .get();
            resolve(d.val());
          });
        }),
      );

      return deliveries;
    } catch (e) {
      return [];
    }
  }

  private async searchClosestDriversInService(
    {
      radius,
      origin,
      mode,
      attemptNumber,
    }: {
      radius: number;
      origin: any;
      mode: 'test' | 'live';
      attemptNumber: number;
    },
    discardedCandidates: Collection<DeliveryRequestRiderCandidate>,
  ): Promise<any> {
    const docKey = AVAILABLE_RIDERS;

    const AVAILABLE_DRIVERS_IN_SERVICE_REF = this.firebase
      .database()
      .ref(`${this.getCollectionName(docKey, mode)}`);

    return this.findCandidatesInRadius({
      source: AVAILABLE_DRIVERS_IN_SERVICE_REF,
      radius: radius,
      origin: origin,
      mode: mode,
      attemptNumber: attemptNumber,
      discardedCandidates: discardedCandidates,
    });
  }

  private async getPreCandidateDriver(
    {
      id,
      radius,
      origin,
      mode,
      attemptNumber,
    }: {
      id: string;
      radius: number;
      origin: any;
      mode: 'test' | 'live';
      attemptNumber: number;
    },
    discardedCandidates: Collection<DeliveryRequestRiderCandidate>,
  ): Promise<any> {
    const isAvailable = (
      await this.firebase
        .database()
        .ref(`${this.getCollectionName(AVAILABLE_RIDERS, mode)}/${id}`)
        .get()
    ).exists();

    const docKey = isAvailable ? AVAILABLE_RIDERS : BUSY_RIDERS;

    const AVAILABLE_DRIVERS_IN_SERVICE_REF = this.firebase
      .database()
      .ref(`${this.getCollectionName(docKey, mode)}`);

    return this.findCandidatesInRadius({
      source: AVAILABLE_DRIVERS_IN_SERVICE_REF,
      radius: radius,
      origin: origin,
      mode: mode,
      attemptNumber: attemptNumber,
      riderId: id,
      discardedCandidates: discardedCandidates,
    });
  }

  private async findCandidatesInRadius({
    riderId,
    source,
    origin,
    mode,
    radius,
    discardedCandidates,
    attemptNumber,
  }: {
    riderId?: string;
    source: any;
    origin: any;
    mode: string;
    radius: any;
    discardedCandidates: any;
    attemptNumber: number;
  }) {
    const geoFireInstance = new geoFire.GeoFire(source);

    let candidatesInRadius = [];

    return new Promise((resolve) => {
      const geoQuery = geoFireInstance.query({
        center: origin,
        radius: radius,
      });

      geoQuery.on(
        'key_entered',
        async (key: string, location: any, distance: number) => {
          const discartedC: DeliveryRequestRiderCandidate | undefined =
            discardedCandidates.find({
              id: key,
            });

          const canBeChosen = (() => {
            console.log('Selecting mode');
            if (riderId) {
              console.log('Extra delivery for ' + riderId);
              if (key !== riderId) return false;
              if (!discartedC) return true;
              return (
                discartedC.availableForRetry &&
                discartedC.attemptNumber !== attemptNumber
              );
            }

            if (!discartedC) return true;
            return (
              discartedC.availableForRetry &&
              discartedC.attemptNumber !== attemptNumber
            );
          })();

          if (canBeChosen) {
            candidatesInRadius.push({
              key,
              distance: distance,
              location: location,
            });
          }
        },
      );

      geoQuery.on('ready', async () => {
        geoQuery.cancel();

        candidatesInRadius = ArrayUtils.orderBy(
          candidatesInRadius,
          ['distance'],
          ['asc'],
        );

        const nearest = candidatesInRadius.slice(0, 1);

        const selected = nearest[0];

        if (selected) {
          this.toggleBlockDrivers(
            [selected].map((c) => ({ ...c, id: c.key })),
            mode,
            true,
          );
        }

        resolve({
          selected: selected ? selected : null,
        });
      });
    });
  }

  private async updateRestaurantOrderStatus(orderId: string, status: string) {
    return this.firebase
      .firestore()
      .collection(RESTAURANT_ORDERS)
      .doc(orderId)
      .update(ObjectUtils.omitUnknown({ status: status }));
  }

  private toggleBlockDrivers(
    drivers: { id: string; location?: any }[],
    mode: string,
    blocked: boolean,
  ) {
    const promises = drivers.map((driver) => {
      return new Promise(async (resolve) => {
        await this.createCandidateBlockedTransaction(
          driver.id,
          mode,
          (blockedDriver) => {
            blockedDriver = blocked && driver.location ? driver.location : null;
            return blockedDriver;
          },
        );
        resolve({});
      });
    });

    return Promise.all(promises);
  }

  private createCandidateBlockedTransaction(
    candidateId: string,
    mode: string,
    update: (element) => void,
  ) {
    const BLOCKED_DRIVERS_REF = this.firebase
      .database()
      .ref(`${this.getCollectionName(BUSY_RIDERS, mode)}`);

    return BLOCKED_DRIVERS_REF.child(candidateId).transaction(update);
  }

  private getCollectionName(name: string, mode: string) {
    return `${name}${mode == 'test' ? `_${mode}` : ''}`;
  }
}
