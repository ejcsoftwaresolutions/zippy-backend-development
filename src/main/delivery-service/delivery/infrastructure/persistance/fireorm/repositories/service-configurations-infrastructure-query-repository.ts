import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { ServiceConfigurationsEntity } from '../entities/service-configurations-entity';
import ServiceConfigurationsMapper from '../mappers/service-configurations-mapper';

const CONFIG_ID = 'RIDER_DELIVERY_CONFIGURATIONS';

export default class ServiceConfigurationsQueryInfrastructureRepository
  extends FireOrmRepository<ServiceConfigurationsEntity>
  implements ServiceConfigurationsQueryRepository
{
  static readonly bindingKey = 'ServiceConfigurationsQueryRepository';

  getEntityClass() {
    return ServiceConfigurationsEntity;
  }

  async getConfigurations(): Promise<ServiceConfigurations> {
    const config = await this.repository().findById(CONFIG_ID);

    if (!config) {
      throw new Error('App setting not configured, run fixtures first!');
    }
    return ServiceConfigurationsMapper.toDomain(config);
  }
}
