import CustomerPublicProfileQueryRepository
  from '@deliveryService/customer/domain/repositories/customer-public-profile-query-repository';
import Delivery from '@deliveryService/delivery/domain/models/delivery-model';
import DeliveryCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-command-repository';
import ServiceConfigurationsQueryRepository
  from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import RiderPublicProfileQueryRepository
  from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import RiderPublicProfileMapper
  from '@deliveryService/rider/infrastructure/persistance/fireorm/mappers/rider-public-profile-mapper';
import {inject} from '@shared/domain/decorators';
import Id from '@shared/domain/id/id';
import {ObjectUtils} from '@shared/domain/utils';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import {FIREBASE_ADMIN_INJECT, FirebaseAdminSDK,} from '@tfarras/nestjs-firebase-admin';
import {EntityConstructorOrPath} from 'fireorm';
import {BUSY_RIDERS, RESTAURANT_ORDERS} from '../constants';
import {DeliveryEntity} from '../entities/delivery-entity';
import DeliveryMapper from '../mappers/delivery-mapper';

export default class DeliveryCommandInfrastructureRepository extends FireOrmRepository<DeliveryEntity> implements DeliveryCommandRepository {
    static readonly bindingKey = 'DeliveryCommandRepository';

    constructor(@inject(FIREBASE_ADMIN_INJECT) private firebase: FirebaseAdminSDK, @inject('RiderPublicProfileQueryRepository') private driverPublicProfileRepo: RiderPublicProfileQueryRepository, @inject('CustomerPublicProfileQueryRepository') private customerPublicProfileRepo: CustomerPublicProfileQueryRepository, @inject('ServiceConfigurationsQueryRepository') private serviceConfigQueryRepository: ServiceConfigurationsQueryRepository,) {
        super();
    }

    getEntityClass(): EntityConstructorOrPath<DeliveryEntity> {
        return DeliveryEntity;
    }

    async createDelivery(delivery: Delivery): Promise<void> {
        const data = DeliveryMapper.toPersistence(delivery);

        await this.repository().create(ObjectUtils.omitUnknown(data));

        try {
            const rider = RiderPublicProfileMapper.toPersistence(delivery.rider);

            await this.updateRestaurantOrderRider(delivery.restaurantOrderId, {
                ...rider, // location: {
                //   latitude: delivery.currentRoute
                //     ? delivery.currentRoute.origin.geoLocation[0]
                //     : 0,
                //   longitude: delivery.currentRoute
                //     ? delivery.currentRoute.origin.geoLocation[1]
                //     : 0,
                // },
            }, /*  JSON.parse(
             JSON.stringify(),
             )*/ 'Driver Accepted',);
        } catch (error) {
            console.log(error.message);
        }
    }

    async updateDelivery(delivery: Delivery): Promise<void> {
        const data = DeliveryMapper.toPersistence(delivery);

        await this.repository().update(ObjectUtils.omitUnknown(data));

        try {
            await this.updateIntermediateOrderState(delivery);
        } catch (error) {
            console.log(error.message);
        }
    }

    async updateIntermediateOrderState(delivery: Delivery) {
        switch (delivery.status) {
            case 'ORDER_RECEIVED':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Order Received',);
                break;
            case 'ON_THE_WAY_TO_CUSTOMER':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'In Transit',);
                break;

            case 'ARRIVED_TO_STORE':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Arrived to Store',);
                break;

            case 'ARRIVED_TO_CUSTOMER':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Arrived to Customer',);
                break;
            default:
                break;
        }
    }

    async cancelDelivery(delivery: Delivery): Promise<void> {
        const data = DeliveryMapper.toPersistence(delivery);

        await this.repository().update(ObjectUtils.omitUnknown(data));

        await this.toggleBlockDriver(data.driverId, data.mode, false);
    }

    async completeDelivery(delivery: Delivery): Promise<void> {
        const data = DeliveryMapper.toPersistence(delivery);

        await this.repository().update(ObjectUtils.omitUnknown(data));

        try {
            await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Order Completed',);
        } catch (error) {
            console.log(error.message);
        }

        try {
            await this.resetChat(data.customerId, data.driverId);
        } catch (error) {
        }

        await this.toggleBlockDriver(data.driverId, data.mode, false);
    }

    async resetChat(clientId: string, driverId: string) {
        const actions = [this.firebase
                         .firestore()
                         .collection('channels')
                         .doc(`${clientId}${driverId}`)
                         .delete(),

                         this.firebase
                         .firestore()
                         .collection('channels')
                         .doc(`${driverId}${clientId}`)
                         .delete(),];

        Promise.all(actions);
    }

    async findDeliveryById(id: Id): Promise<Delivery> {
        const element = await this.repository()
        .whereEqualTo('id', id.value)
        .findOne();

        if (!element) return null;

        const customerPublicProfile = await this.customerPublicProfileRepo.findProfileById(element.customerId);

        const driverPublicProfile = await this.driverPublicProfileRepo.findProfileById(element.driverId);

        const serviceConfiguration = await this.serviceConfigQueryRepository.getConfigurations();

        if (!driverPublicProfile) {
            throw new Error('RIDER_PUBLIC_PROFILE_NOT_FOUND');
        }

        if (!customerPublicProfile) {
            throw new Error('CUSTOMER_PUBLIC_PROFILE_NOT_FOUND');
        }

        return DeliveryMapper.toDomain({
            ...element,
            riderPublicProfile: driverPublicProfile,
            customerPublicProfile: customerPublicProfile,
            serviceConfig: serviceConfiguration,
        });
    }

    private async toggleBlockDriver(driverId: string, mode: string, blocked: boolean,) {
        const BLOCKED_DRIVERS_REF = this.firebase
        .database()
        .ref(`${BUSY_RIDERS}${mode === 'test' ? '_test' : ''}`);

        return BLOCKED_DRIVERS_REF.child(driverId).transaction((blockedDriver) => {
            blockedDriver = blocked ? true : null;

            return blockedDriver;
        });
    }

    private async updateRestaurantOrderRider(orderId: string, driver: any, status: string,) {
        return this.firebase
        .firestore()
        .collection(RESTAURANT_ORDERS)
        .doc(orderId)
        .update(ObjectUtils.omitUnknown({
            status: status, driver: driver, driverID: driver.id, acceptedAt: new Date(),
        }),);
    }

    private async updateRestaurantOrderStatus(orderId: string, status: string) {
        return this.firebase
        .firestore()
        .collection(RESTAURANT_ORDERS)
        .doc(orderId)
        .update(ObjectUtils.omitUnknown({
            status: status,
        }),);
    }
}
