export const RIDERS_LOCATIONS = 'rider_locations';
export const CONNECTED_RIDERS = 'connected_riders';
export const AVAILABLE_RIDERS = 'available_riders';
export const BUSY_RIDERS = 'busy_riders';
export const DELIVERY_REQUESTS = 'delivery_requests';
export const DRIVER_ALERTS = 'driver_delivery_request_alerts';
export const TEST_CUSTOMERS = 'test_customers';
export const RESTAURANT_ORDERS = 'orders';
export const VENDOR_PRODUCTS = 'vendor_products';
export const RIDERS = 'riders';
export const VENDORS = 'vendors';
export const PUSH_TOKENS = 'user_push_tokens';
