import firebase from 'firebase-admin';
import {
  AVAILABLE_RIDERS,
  BUSY_RIDERS,
  CONNECTED_RIDERS,
  RIDERS_LOCATIONS,
} from '@deliveryService/delivery/infrastructure/persistance/fireorm/constants';

export default class AvailableRidersHandler {
  constructor() {}

  onModuleInit() {
    this.execute();
  }

  async execute() {
    const promises = ['live', 'test'].map(async (mode) => {
      const onlineRidersRef = await firebase
        .database()
        .ref(`${RIDERS_LOCATIONS}${mode === 'test' ? '_test' : ''}`);

      const availableRidersRef = await firebase
        .database()
        .ref(`${AVAILABLE_RIDERS}${mode === 'test' ? '_test' : ''}`);

      const busyRidersRef = await firebase
        .database()
        .ref(`${BUSY_RIDERS}${mode === 'test' ? '_test' : ''}`);

      const connectedRidersRef = await firebase
        .database()
        .ref(`${CONNECTED_RIDERS}${mode === 'test' ? '_test' : ''}`);

      const isBlocked = async (riderId) => {
        const ret = await busyRidersRef.child(riderId).get();
        return ret.exists();
      };

      const isConnected = async (riderId) => {
        const ret = await connectedRidersRef.child(riderId).get();
        if (!ret.exists()) return false;

        return ret.val();
      };

      async function handleLocationChange(snap) {
        if (!snap.exists()) {
          return;
        }

        const riderLocation = snap.val();
        const riderId = snap.key;

        if (!(await isConnected(riderId))) return;

        if (await isBlocked(riderId)) {
          return busyRidersRef
            .child(riderId)
            .set(riderLocation)
            .then(() => {});
        }

        availableRidersRef
          .child(riderId)
          .set(riderLocation)
          .then(() => {
            /*silent*/
          });
      }

      async function handleDisconnect(snap) {
        if (!snap.exists()) {
          return;
        }

        const riderId = snap.key;

        availableRidersRef
          .child(riderId)
          .set(null)
          .then(() => {
            /*silent*/
          });
      }

      function handleBlock(snap) {
        if (!snap.exists()) {
          return;
        }

        const riderId = snap.key;

        availableRidersRef
          .child(riderId)
          .set(null)
          .then(() => {
            /*silent*/
          });
      }

      async function handleUnblock(snap) {
        if (!snap.exists()) {
          return;
        }
        const riderId = snap.key;
        const lastCoords = await onlineRidersRef.child(riderId).get();

        if (!lastCoords.exists()) return;

        if (!(await isConnected(riderId))) return;

        availableRidersRef
          .child(riderId)
          .set(lastCoords.val())
          .then(() => {
            /*silent*/
          });
      }

      connectedRidersRef.on('child_added', async (snap) => {
        const riderId = snap.key;

        const ret = await onlineRidersRef.child(riderId).get();
        if (!ret.exists()) return;

        const location = ret.val();

        if (await isBlocked(riderId)) {
          return busyRidersRef
            .child(riderId)
            .set(location)
            .then(() => {});
        }

        availableRidersRef
          .child(riderId)
          .set(location)
          .then(() => {});
      });

      onlineRidersRef.on('child_added', handleLocationChange);
      onlineRidersRef.on('child_changed', handleLocationChange);
      onlineRidersRef.on('child_removed', handleDisconnect);

      busyRidersRef.on('child_removed', handleUnblock);
      busyRidersRef.on('child_added', handleBlock);
    });

    try {
      Promise.all(promises).then(() => {
        /*silent*/
      });
    } catch (e) {
      console.log(e.message);
    }
  }
}
