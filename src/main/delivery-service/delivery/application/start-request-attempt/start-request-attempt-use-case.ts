import RequestInformationBuilder from '@deliveryService/store/domain/services/request-information-builder';
import EventBus from '@shared/domain/bus/event/event-bus';
import Id from '@shared/domain/id/id';
import RequestAttemptCountdownTimer from '../../domain/contracts/request-attempt-countdown-timer';
import DeliveryRequest from '../../domain/models/delivery-request-model';
import DeliveryRequestCommandRepository from '../../domain/repositories/delivery-request-command-repository';
import ProcessAttemptNextCandidateUseCase
  from '../process-attempt-next-candidate/process-attempt-next-candidate-use-case';

export default class StartRequestAttemptUseCase {
    private request: DeliveryRequest;
    private processNextCandidateService: ProcessAttemptNextCandidateUseCase;

    constructor(private requestTimerService: RequestAttemptCountdownTimer, private deliveryRequestRepository: DeliveryRequestCommandRepository, requestInformationBuilder: RequestInformationBuilder, private eventBus: EventBus,) {
        this.processNextCandidateService = new ProcessAttemptNextCandidateUseCase(requestTimerService, deliveryRequestRepository, requestInformationBuilder, this.eventBus,);
    }

    async execute(request: DeliveryRequest) {
        this.request = request;

        if (!this.requestHasAttemptTimer()) {
            this.createRequestTimer();
            this.setUpTimerEvents();
        }

        this.request.processCurrentAttempt();

        await this.processNextCandidateService.execute(this.request);
    }

    private setUpTimerEvents() {
        const onUpdate = (seconds: number) => {
            try {
                if (seconds === this.request.acceptanceTimeout) return;

                const timer = this.requestTimerService.get(this.request.id.value);

                if (!timer || !timer.candidateId) {
                    return;
                }

                if (timer.interrupted) {
                    this.processNextCandidateService.execute(this.request);
                    return;
                }

                this.request.setCandidateCountDown(new Id(timer.candidateId), seconds);

                this.deliveryRequestRepository.updateRequest(this.request);
            } catch (error) {
                console.log(`Error in timer update: ${error.message}`);
            }
        };

        const onTimeout = () => {
            const timer = this.requestTimerService.get(this.request.id.value);

            if (!timer) return;

            this.processNextCandidateService.execute(this.request, false);
        };

        try {
            this.requestTimerService.onUpdate(this.request.id.value, onUpdate);

            this.requestTimerService.onTimeout(this.request.id.value, onTimeout);
        } catch (error) {
        }
    }

    private createRequestTimer() {
        this.requestTimerService.initialize(this.request.id.value, this.request.acceptanceTimeout,);
    }

    private requestHasAttemptTimer() {
        return this.requestTimerService.exists(this.request.id.value);
    }
}
