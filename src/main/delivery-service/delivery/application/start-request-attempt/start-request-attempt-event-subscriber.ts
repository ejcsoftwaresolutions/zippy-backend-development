import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import DeliveryRequestCommandRepository from '../../domain/repositories/delivery-request-command-repository';
import StartRequestAttemptUseCase from './start-request-attempt-use-case';

@service()
export default class StartRequestAttemptEventSubscriber
  implements EventSubscriber
{
  constructor(
    @inject('StartRequestAttemptUseCase')
    private useCase: StartRequestAttemptUseCase,
    @inject('DeliveryRequestCommandRepository')
    private deliveryRequestRepository: DeliveryRequestCommandRepository,
  ) {}

  public subscribedTo(): Map<string, Function> {
    const events = new Map<string, Function>();
    /*   events.set(
      DeliveryRequestAttemptCreatedDomainEvent.eventName,
      this.handle.bind(this),
    ); */

    return events;
  }

  public async handle(event: any) {
    const request = await this.deliveryRequestRepository.getRequest(
      event.aggregateId,
    );

    this.useCase.execute(request);
  }
}
