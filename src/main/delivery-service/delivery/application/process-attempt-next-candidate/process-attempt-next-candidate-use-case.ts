import RequestInformationBuilder from '@deliveryService/store/domain/services/request-information-builder';
import EventBus from '@shared/domain/bus/event/event-bus';
import Collection from '@shared/domain/value-object/collection';
import RequestAttemptCountdownTimer from '../../domain/contracts/request-attempt-countdown-timer';
import DeliveryRequest from '../../domain/models/delivery-request-model';
import DeliveryRequestCommandRepository from '../../domain/repositories/delivery-request-command-repository';

export default class ProcessAttemptNextCandidateUseCase {
  private request: DeliveryRequest;

  constructor(
    private requestTimerService: RequestAttemptCountdownTimer,
    private deliveryRequestRepository: DeliveryRequestCommandRepository,
    private requestInformationBuilder: RequestInformationBuilder,
    private eventBus: EventBus,
  ) {}

  async execute(
    request: DeliveryRequest,
    shouldDiscardCurrentCandidate = true,
  ) {
    this.request = request;

    if (this.request.acceptedRider) {
      throw new Error('request_already_accepted');
    }

    const currentCandidate = await this.getNextAvailableCandidate(
      shouldDiscardCurrentCandidate,
    );

    if (!currentCandidate) {
      console.log(`${this.request.id.value} Se llego al final`);
      await this.attemptGiveUp();
      return;
    }

    console.log(
      `${this.request.id.value} Se le esta enviando la notificación al rider ${currentCandidate.id}`,
    );

    this.request.setCurrentAttemptAlertedCandidate(currentCandidate);

    if (currentCandidate.publicProfile) {
      this.request.updateServiceType(
        currentCandidate.publicProfile.deliveryMethod as any,
      );
    }

    try {
      this.requestTimerService.start(
        this.request.id.value,
        currentCandidate.id,
      );
    } catch (error) {}

    const notificationData = {
      id: this.request.id.value,
      driverId: currentCandidate.id,
      requestInfo: this.request.requestInfo.toPrimitives(),
      customer: this.request.customer.toPrimitives(),
      tripDistance: this.request.tripDistance,
      tripDuration: this.request.tripDuration,
      finalFee: this.request.finalFee,
      distanceToClient: currentCandidate.distanceToClient,
      distanceToStore: currentCandidate.distanceToStore,
      routes: this.request.routes.toPrimitives(),
      acceptanceTimeout: this.request.acceptanceTimeout,
      timerStart: new Date().getTime(),
      pushToken: currentCandidate.publicProfile.pushToken,
    };

    this.request.setCurrentAttemptAlertedCandidateNotification(
      notificationData,
    );

    await this.deliveryRequestRepository.updateDriverNotification(
      this.request.currentAlertedCandidate,
      this.request.mode,
    );

    await this.deliveryRequestRepository.updateRequest(this.request);

    this.eventBus.publish(this.request.pullDomainEvents());
  }

  private async attemptGiveUp() {
    try {
      this.requestTimerService.stop(this.request.id.value);
      this.requestTimerService.destroy(this.request.id.value);
    } catch (error) {}

    if (this.request.currentAlertedCandidate) {
      this.request.setCurrentAttemptAlertedCandidateNotification(null);

      await this.deliveryRequestRepository.updateDriverNotification(
        this.request.currentAlertedCandidate,
        this.request.mode,
      );

      await this.deliveryRequestRepository.toggleLockedCandidates(
        new Collection([this.request.currentAlertedCandidate]),
        this.request.mode,
        false,
      );

      this.request.setCurrentAttemptAlertedCandidate(null);
    }

    this.request.isLastAttempt
      ? this.request.setMaxAttemptsReached()
      : this.request.setRidersNotFoundInAttempt();

    await this.deliveryRequestRepository.updateRequest(this.request);

    if (this.request.isLastAttempt) {
      await this.deliveryRequestRepository.deleteRequest(this.request);
    }
  }

  private async discardCurrentCandidate() {
    const discardedCandidate = this.request.currentAlertedCandidate;

    if (!discardedCandidate) {
      return;
    }

    this.request.discardCandidate(discardedCandidate);

    await this.deliveryRequestRepository.updateRequest(this.request);
  }

  private async cleanCurrentAlertedNotification() {
    const discardedCandidate = this.request.currentAlertedCandidate;

    if (!discardedCandidate) {
      return;
    }

    this.request.setCurrentAttemptAlertedCandidateNotification(null);

    try {
      await this.deliveryRequestRepository.updateDriverNotification(
        this.request.currentAlertedCandidate,
        this.request.mode,
      );

      await this.deliveryRequestRepository.toggleLockedCandidates(
        new Collection([discardedCandidate]),
        this.request.mode,
        false,
      );
    } catch (error) {
      console.log('ERROR AL DISCARD DB!');
    }
  }

  private async getNextAvailableCandidate(
    shouldDiscardCurrentCandidate: boolean,
  ) {
    this.request.currentAlertedCandidate?.setAvailableForRetry(
      !shouldDiscardCurrentCandidate,
    );

    await this.discardCurrentCandidate();
    await this.cleanCurrentAlertedNotification();

    try {
      this.requestTimerService.reset(this.request.id.value);
    } catch (error) {
      console.log('Error trying to stop the timer');
    }

    /*    if (this.request.currentAttemptCandidates.count() >= this.request.attemptMaxCandidates) {
         return null;
         }*/

    const nextCandidate =
      await this.deliveryRequestRepository.findClosestRiderCandidate(
        this.request,
      );

    if (!nextCandidate) {
      return null;
    }

    nextCandidate.setAttemptNumber(this.request.attemptNumber);
    this.request.addCandidateToCurrentAttempt(nextCandidate);

    return nextCandidate;
  }
}
