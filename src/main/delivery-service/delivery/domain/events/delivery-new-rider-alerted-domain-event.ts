import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'DELIVERY_NEW_RIDER_ALERTED';

interface EventProps extends DomainEventProps {
  eventData: {
    riderId: string;
  };
}

export default class DeliveryNewRiderAlertedDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
