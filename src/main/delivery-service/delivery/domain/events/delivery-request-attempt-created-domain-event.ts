import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'DELIVERY_REQUEST_ATTEMPT_CREATED';

interface EventProps extends DomainEventProps {
  eventData: {
    customerId: string;
  };
}

export default class DeliveryRequestAttemptCreatedDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
