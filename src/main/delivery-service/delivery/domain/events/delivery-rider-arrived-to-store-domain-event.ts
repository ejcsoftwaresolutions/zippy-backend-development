import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'DELIVERY_RIDER_ARRIVED_TO_STORE';

interface EventProps extends DomainEventProps {
  eventData: {
    riderId: string;
    deliveryId: string;
    storeId: string;
  };
}

export default class DeliveryRiderArrivedToStoreDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
