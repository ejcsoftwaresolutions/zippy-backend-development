import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'DELIVERY_FINISHED';

interface EventProps extends DomainEventProps {
  eventData: {
    riderId: string;
    deliveryId: string;
    storeId: string;
    vendorOrderId: string;
    driverFee: number;
  };
}

export default class DeliveryFinishedDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
