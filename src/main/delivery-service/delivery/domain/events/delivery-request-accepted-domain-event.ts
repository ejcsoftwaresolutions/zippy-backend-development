import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'DELIVERY_REQUEST_ACCEPTED';

interface EventProps extends DomainEventProps {
  eventData: {
    riderId: string;
  };
}

export default class DeliveryRequestAcceptedDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
