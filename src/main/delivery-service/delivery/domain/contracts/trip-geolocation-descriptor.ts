export type TripGeoInformation = {
  distance: string;
  distanceValue: number;
  duration: string;
  durationValue: number;
  originAddress?: string;
  destinationAddress?: string;
};

export default interface TripGeolocationDescriptor {
  basicInfo(origin: string, destination: string): Promise<TripGeoInformation>;
}
