import Collection from '@shared/domain/value-object/collection';
import DeliveryRequestRiderCandidate from '../models/delivery-request-rider-candidate-model';
import DeliveryRequest from '../models/delivery-request-model';

export default interface DeliveryRequestCommandRepository {
  saveRequest(request: DeliveryRequest): void;

  getRequest(requestId: string): Promise<DeliveryRequest>;

  findClosestRiderCandidate(
    request: DeliveryRequest,
  ): Promise<DeliveryRequestRiderCandidate>;

  updateRequest(request: DeliveryRequest): Promise<void>;

  updateDriverNotification(
    candidate: DeliveryRequestRiderCandidate,
    mode: 'live' | 'test',
  ): Promise<void>;

  toggleLockedCandidates(
    candidates: Collection<DeliveryRequestRiderCandidate>,
    mode: 'live' | 'test',
    locked: boolean,
  ): void;

  deleteRequest(request: DeliveryRequest): Promise<void>;
}
