import ServiceConfigurations from '../models/service-configurations-model';

export default interface ServiceConfigurationsQueryRepository {
  getConfigurations(): Promise<ServiceConfigurations>;
}
