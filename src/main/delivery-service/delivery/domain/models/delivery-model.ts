import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';
import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';
import StoreOrder, { StoreOrderPrimitiveProps } from '@deliveryService/store/domain/models/store-order-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import Collection from '@shared/domain/value-object/collection';
import DeliveryRequest, { RideMeasurement, RideRoute } from './delivery-request-model';
import ServiceConfigurations, { Price } from './service-configurations-model';

/* class constructor properties */

export type ActiveRideRoute = RideRoute & {
    completed: boolean;
    completedAt?: Date;
};

export type DeliveryStatus =
    | 'ACCEPTED'
    | 'ARRIVED_TO_STORE'
    | 'ORDER_RECEIVED'
    | 'ON_THE_WAY_TO_CUSTOMER'
    | 'ARRIVED_TO_CUSTOMER'
    | 'DELIVERED';

export interface DeliveryProps {
    id: Id;
    customer: CustomerPublicProfile;
    rider: RiderPublicProfile;
    serviceConfig: ServiceConfigurations;
    requestedAt: Date;
    acceptedAt: Date;
    distance: RideMeasurement;
    duration: RideMeasurement;
    finalFee: Price;
    status: DeliveryStatus;
    routes: Collection<ActiveRideRoute>;
    orderDetails: StoreOrder;
    leftStoreAt?: Date;
    deliveredAt?: Date;
    mode: 'test' | 'live';
    assigned?: boolean;
}

/* class properties to represent plain data */
interface DeliveryPrimitiveProps {
    id: string;
    customer: any;
    rider: any;
    requestedAt: Date;
    acceptedAt: Date;
    distance: RideMeasurement;
    duration: RideMeasurement;
    finalFee: Price;
    status: DeliveryStatus;
    serviceConfig?: ServiceConfigurations;
    routes: ActiveRideRoute[];
    orderDetails: StoreOrderPrimitiveProps;
    leftStoreAt?: Date;
    deliveredAt?: Date;
    mode: 'test' | 'live';
    assigned?: boolean;
}

export default class Delivery extends AggregateRoot<DeliveryProps> {
    constructor(constructParams: DeliveryProps) {
        super({ ...constructParams });
    }

    get id() {
        return this.props.id;
    }

    get customer() {
        return this.props.customer;
    }

    get rider() {
        return this.props.rider;
    }

    get requestedAt() {
        return this.props.requestedAt;
    }

    get restaurantOrderId() {
        return this.props.orderDetails.id;
    }

    get acceptedAt() {
        return this.props.acceptedAt;
    }

    get distance() {
        return this.props.distance;
    }

    get duration() {
        return this.props.duration;
    }

    get finalFee() {
        return this.props.finalFee;
    }

    get status() {
        return this.props.status;
    }

    get orderCode() {
        return this.props.orderDetails.code;
    }

    get mode() {
        return this.props.mode;
    }

    setAssigned() {
        this.props.assigned = true
    }

    static fromPrimitives(props: DeliveryPrimitiveProps) {
        return new Delivery({
            id: new Id(props.id),
            customer: CustomerPublicProfile.fromPrimitives(props.customer),
            rider: RiderPublicProfile.fromPrimitives(props.rider),
            requestedAt: props.requestedAt,
            acceptedAt: props.acceptedAt,
            distance: props.distance,
            duration: props.duration,
            finalFee: props.finalFee,
            status: props.status,
            mode: props.mode,
            routes: props.routes ? new Collection(props.routes) : undefined,
            serviceConfig: props.serviceConfig,
            orderDetails: StoreOrder.fromPrimitives(props.orderDetails),
            leftStoreAt: props.leftStoreAt,
            deliveredAt: props.deliveredAt,
            assigned: props.assigned
        });
    }

    static fromDeliveryRequest(request: DeliveryRequest): Delivery {
        if (!request.acceptedRider.publicProfile) {
            throw new Error('NOT_PUBLIC_PROFILE_RIDER');
        }

        return Delivery.fromPrimitives({
            id: new Id(request.requestInfo.id).value,
            customer: request.customer.toPrimitives(),
            rider: request.acceptedRider.publicProfile.toPrimitives(),
            status: 'ACCEPTED',
            duration: request.tripDuration,
            distance: request.tripDistance,
            finalFee: request.finalFee,
            acceptedAt: new Date(),
            requestedAt: request.date ? request.date : new Date(),
            mode: request.mode,
            routes: request.routes?.toPrimitives(),
            orderDetails: request.requestInfo.toPrimitives()
        });
    }

    toPrimitives(): DeliveryPrimitiveProps {
        return {
            id: this.id.value,
            customer: this.customer.toPrimitives(),
            rider: this.rider.toPrimitives(),
            requestedAt: this.requestedAt,
            acceptedAt: this.acceptedAt,
            distance: this.distance,
            duration: this.duration,
            finalFee: this.finalFee,
            status: this.status,
            mode: this.props.mode,
            routes: this.props.routes?.toPrimitives(),
            orderDetails: this.props.orderDetails.toPrimitives(),
            leftStoreAt: this.props.leftStoreAt,
            deliveredAt: this.props.deliveredAt,
            assigned: this.props.assigned
        };
    }

}
