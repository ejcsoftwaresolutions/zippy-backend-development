import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Collection from '@shared/domain/value-object/collection';
import DeliveryRequestRiderCandidate from './delivery-request-rider-candidate-model';

export interface DeliveryRequestAttemptProps {
  number: number;
  radius: number;
}

export default class DeliveryRequestAttempt extends AggregateRoot<DeliveryRequestAttemptProps> {
  constructor(constructParams: DeliveryRequestAttemptProps) {
    super({ ...constructParams });

    this._currentAlertedCandidate = null;

    this._candidates = new Collection<DeliveryRequestRiderCandidate>([]);
  }

  private _candidates: Collection<DeliveryRequestRiderCandidate>;

  get candidates() {
    return this._candidates;
  }

  private _currentAlertedCandidate: DeliveryRequestRiderCandidate | null;

  get currentAlertedCandidate() {
    return this._currentAlertedCandidate;
  }

  get number() {
    return this.props.number;
  }

  get radius() {
    return this.props.radius;
  }

  public setCandidates(candidates: Collection<DeliveryRequestRiderCandidate>) {
    this._candidates = candidates;
  }

  public addCandidate(candidate: DeliveryRequestRiderCandidate) {
    this._candidates.add(candidate);
  }

  public setCurrentAlertedCandidate(
    candidate: DeliveryRequestRiderCandidate | null,
  ) {
    this._currentAlertedCandidate = candidate;
  }
}
