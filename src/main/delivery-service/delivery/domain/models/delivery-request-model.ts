import StoreOrder, {
  StoreOrderPrimitiveProps,
} from '@deliveryService/store/domain/models/store-order-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { DeliveryCategory } from '@shared/domain/models/delivery-category';
import Collection from '@shared/domain/value-object/collection';
import CustomerPublicProfile, {
  CustomerPublicProfilePrimitiveProps,
} from '../../../customer/domain/models/customer-public-profile-model';
import DeliveryNewRiderAlertedDomainEvent from '../events/delivery-new-rider-alerted-domain-event';
import DeliveryRequestAcceptedDomainEvent from '../events/delivery-request-accepted-domain-event';
import DeliveryRequestAttemptCreatedDomainEvent from '../events/delivery-request-attempt-created-domain-event';
import DeliveryRequestAttemptStartedDomainEvent from '../events/delivery-request-attempt-started-domain-event';
import DeliveryRequestCreatedDomainEvent from '../events/delivery-request-created-domain-event';
import DeliveryRequestAttempt from './delivery-request-attempt-model';
import DeliveryRequestRiderCandidate, {
  DeliveryRequestRiderCandidatePrimitiveProps,
} from './delivery-request-rider-candidate-model';
import ServiceConfigurations, { Price } from './service-configurations-model';

export type DeliveryRequestStatus =
  | 'SEARCHING'
  | 'DRIVERS_NOT_FOUND_IN_ATTEMPT'
  | 'NOT_FOUND_MAX_ATTEMPTS_REACHED'
  | 'ACCEPTED'
  | 'CANCELED';

export type DeliveryServiceType = DeliveryCategory;

export type GeoCoordinates = number[];

export type RideGeoPoint = {
  address: string;
  geoLocation: GeoCoordinates;
  referencePoint?: string;
};

export type RideMeasurement = {
  value: number;
  text: string;
};

export type PaymentMethod = 'CASH' | 'CARD';

export type RideRoute = {
  origin: RideGeoPoint;
  destination: RideGeoPoint;
  distance: RideMeasurement;
  duration: RideMeasurement;
};

export interface DeliveryRequestProps {
  id: Id;
  customer: CustomerPublicProfile;
  requestInfo: StoreOrder;
  tripDistance: RideMeasurement;
  tripDuration: RideMeasurement;
  serviceConfig: ServiceConfigurations;
  date: Date;
  routes?: Collection<RideRoute>;
  serviceType: DeliveryServiceType;
  mode: 'test' | 'live';
  preCandidates?: string[];
}

export type DeliveryRequestPrimitiveProps = {
  id: string;
  currentAttempt: {
    number: number;
    radius: number;
    candidates: any;
    currentAlertedCandidate?: DeliveryRequestRiderCandidatePrimitiveProps | null;
  };
  customer: CustomerPublicProfilePrimitiveProps;
  requestInfo: StoreOrderPrimitiveProps;
  discardedCandidates: any[];
  tripDistance: RideMeasurement;
  tripDuration: RideMeasurement;
  finalFee: Price;
  status: DeliveryRequestStatus;
  acceptedRider?: any;
  serviceConfig?: ServiceConfigurations;
  date: Date;
  routes?: RideRoute[];
  serviceType: DeliveryServiceType;
  mode: 'test' | 'live';
  maxAttempts?: number;
  preCandidates?: string[];
};

const DefaultProps: Partial<DeliveryRequestProps> = {};

export default class DeliveryRequest extends AggregateRoot<DeliveryRequestProps> {
  private _currentAttempt: DeliveryRequestAttempt;

  constructor(props: DeliveryRequestProps) {
    super({ ...DefaultProps, ...props });
    this._discardedCandidates = new Collection<DeliveryRequestRiderCandidate>(
      [],
    );

    /* this.guardExistFeesForType();*/
  }

  private _status: DeliveryRequestStatus = 'SEARCHING';

  get status() {
    return this._status;
  }

  private _discardedCandidates: Collection<DeliveryRequestRiderCandidate>;

  get discardedCandidates() {
    return this._discardedCandidates;
  }

  private _acceptedRider?: DeliveryRequestRiderCandidate;

  get acceptedRider() {
    return this._acceptedRider;
  }

  get preCandidates() {
    return this.props.preCandidates;
  }

  get preCandidate() {
    if (!this.props.preCandidates) return undefined;
    if (this.props.preCandidates.length == 0) return undefined;

    return this.props.preCandidates[0];
  }

  get id() {
    return this.props.id;
  }

  get customer() {
    return this.props.customer;
  }

  get deliveryFee() {
    return this.props.requestInfo.deliveryFee;
  }

  get searchRadius() {
    return this.props.serviceConfig.attemptSearchRadius;
  }

  get acceptanceTimeout() {
    return this.props.serviceConfig.acceptanceTimeout;
  }

  get maxAttempts() {
    return this.props.serviceConfig.maxAttempts;
  }

  get attemptMaxCandidates() {
    return this.props.serviceConfig.attemptMaxCandidates;
  }

  get tripDistance() {
    return this.props.tripDistance;
  }

  get requestInfo() {
    return this.props.requestInfo;
  }

  get storeAddress() {
    return this.props.routes.getAt(0).destination.address;
  }

  get tripDuration() {
    return this.props.tripDuration;
  }

  get date() {
    return this.props.date;
  }

  get mode() {
    return this.props.mode;
  }

  get isTest() {
    return this.props.mode === 'test';
  }

  get routes() {
    return this.props.routes;
  }

  get serviceType() {
    return this.props.serviceType;
  }

  get attemptNumber() {
    return this._currentAttempt.number;
  }

  get currentAlertedCandidate() {
    return this._currentAttempt.currentAlertedCandidate;
  }

  get currentAttemptRadius() {
    return this._currentAttempt.radius;
  }

  get finalFee(): Price {
    return {
      amount: this.deliveryFee,
      currency: '$',
    };
    /*if (
             !this.tripDistance.value ||
             !this.props.routes ||
             this.props.routes?.count() === 0
             ) {
             return {
             amount: 0,
             currency: '$',
             };
             }
        
             return this.props.serviceConfig.calculateRoutesFee(
             this.props.routes,
             this.serviceType,
             );*/
  }

  get currentAttemptCandidates(): Collection<DeliveryRequestRiderCandidate> {
    return this._currentAttempt.candidates;
  }

  get remainingCandidatesCount() {
    if (!this.currentAttemptCandidates) {
      return 0;
    }

    return (
      this.currentAttemptCandidates.count() -
      this._discardedCandidates.items.filter(
        (candidate) => candidate.attemptNumber === this.attemptNumber,
      ).length
    );
  }

  get isLastAttempt() {
    return this.attemptNumber === this.maxAttempts;
  }

  static fromPrimitives(data: DeliveryRequestPrimitiveProps) {
    if (!data.serviceConfig) {
      throw new Error('Please provide service configurations model');
    }

    const request = new DeliveryRequest({
      id: new Id(data.id),
      customer: CustomerPublicProfile.fromPrimitives({
        ...data.customer,
      }),
      requestInfo: StoreOrder.fromPrimitives(data.requestInfo),
      serviceConfig: data.serviceConfig,
      tripDistance: data.tripDistance,
      tripDuration: data.tripDuration,
      date: data.date,
      mode: data.mode,
      preCandidates: data.preCandidates,
      routes: data.routes ? new Collection(data.routes) : undefined,
      serviceType: data.serviceType,
    });

    if (data.discardedCandidates) {
      request._discardedCandidates =
        new Collection<DeliveryRequestRiderCandidate>(
          data.discardedCandidates.map(
            (candidate) =>
              new DeliveryRequestRiderCandidate({
                ...candidate,
              }),
          ),
        );
    }

    request._currentAttempt = new DeliveryRequestAttempt({
      number: data.currentAttempt.number,
      radius: data.currentAttempt.radius,
    });

    if (data.currentAttempt.currentAlertedCandidate) {
      request.setCurrentAttemptAlertedCandidate(
        DeliveryRequestRiderCandidate.fromPrimitives({
          ...data.currentAttempt.currentAlertedCandidate,
          isBlockedToTakeRequest: true,
        }),
      );
    }

    request._status = data.status;

    return request;
  }

  static create(deliveryRequest: DeliveryRequestProps) {
    const newDeliveryRequest = new DeliveryRequest(deliveryRequest);
    newDeliveryRequest.record(
      new DeliveryRequestCreatedDomainEvent({
        aggregateId: newDeliveryRequest.id.value,
        eventData: {
          customerId: newDeliveryRequest.customer.id,
        },
        occurredOn: new Date(),
      }),
    );
    return newDeliveryRequest;
  }

  setPreCandidates(preCandidates) {
    if (!preCandidates) {
      this.props.preCandidates = [];
      return;
    }

    this.props.preCandidates = preCandidates;
  }

  updateServiceType(serviceType: DeliveryServiceType) {
    this.props.serviceType = serviceType;
  }

  guardExistFeesForType() {
    this.props.serviceConfig.guardExistFeesForType(this.props.serviceType);
  }

  discardCandidate(candidate: DeliveryRequestRiderCandidate) {
    const candidateIndex = this._discardedCandidates.findIndex(candidate);

    this._discardedCandidates.replaceAt(candidateIndex, candidate);
  }

  addCandidateToCurrentAttempt(candidate: DeliveryRequestRiderCandidate) {
    this._currentAttempt.addCandidate(candidate);
  }

  setCurrentAttemptAlertedCandidate(
    candidate: DeliveryRequestRiderCandidate | null,
  ) {
    this._currentAttempt.setCurrentAlertedCandidate(candidate);
  }

  setCurrentAttemptAlertedCandidateNotification(notification: any) {
    this._currentAttempt.currentAlertedCandidate.setNotification(notification);

    if (!notification) {
      return;
    }

    this.record(
      new DeliveryNewRiderAlertedDomainEvent({
        aggregateId: this.id.value,
        eventData: {
          riderId: this._currentAttempt.currentAlertedCandidate.id,
        },
        occurredOn: new Date(),
      }),
    );
  }

  setCandidateCountDown(candidateId: Id, seconds: number) {
    const candidate = this._currentAttempt.candidates.items.find(
      (c) => c.id === candidateId.value,
    );

    if (!candidate) {
      return;
    }

    candidate.setCountDown(seconds);
  }

  setMaxAttemptsReached() {
    this._status = 'NOT_FOUND_MAX_ATTEMPTS_REACHED';
  }

  setRidersNotFoundInAttempt() {
    this._status = 'DRIVERS_NOT_FOUND_IN_ATTEMPT';
  }

  markAsCanceled() {
    this._status = 'CANCELED';
  }

  toPrimitives(): DeliveryRequestPrimitiveProps {
    return {
      id: this.id.value,
      preCandidates: this.props.preCandidates,
      currentAttempt: {
        number: this._currentAttempt.number,
        radius: this._currentAttempt.radius,
        candidates: this._currentAttempt.candidates
          ? this._currentAttempt.candidates.map((candidate) =>
              candidate.toPrimitives(),
            )
          : [],
        currentAlertedCandidate:
          this._currentAttempt && this._currentAttempt.currentAlertedCandidate
            ? this._currentAttempt.currentAlertedCandidate.toPrimitives()
            : null,
      },
      customer: this.customer.toPrimitives(),
      discardedCandidates: this._discardedCandidates
        ? this._discardedCandidates.map((candidate) => candidate.toPrimitives())
        : [],
      tripDistance: this.tripDistance,
      tripDuration: this.tripDuration,
      finalFee: this.finalFee,
      status: this._status,
      acceptedRider: this._acceptedRider
        ? this._acceptedRider.toPrimitives()
        : null,
      requestInfo: this.props.requestInfo.toPrimitives(),
      date: this.date ? this.props.date : new Date(),
      mode: this.props.mode,
      routes: this.props.routes?.toPrimitives(),
      serviceType: this.props.serviceType,
      maxAttempts: this.props.serviceConfig.maxAttempts,
    };
  }

  registerNewAttempt(): DeliveryRequestAttempt {
    const number = !this._currentAttempt ? 1 : this._currentAttempt.number + 1;

    const newAttempt = new DeliveryRequestAttempt({
      number: number,
      radius: this.searchRadius, //number * this.searchRadius,
    });

    this._currentAttempt = newAttempt;

    this._status = 'SEARCHING';

    this.record(
      new DeliveryRequestAttemptCreatedDomainEvent({
        aggregateId: this.id.value,
        eventData: {
          customerId: this.id.value,
        },
        occurredOn: new Date(),
      }),
    );

    return newAttempt;
  }

  processCurrentAttempt() {
    if (!this._currentAttempt) {
      throw new Error(
        "There's not an active attempt registered. can't start processing",
      );
    }

    this.record(
      new DeliveryRequestAttemptStartedDomainEvent({
        aggregateId: this.id.value,
        eventData: {
          customerId: this.id.value,
        },
        occurredOn: new Date(),
      }),
    );
  }

  setAcceptedRider(rider: DeliveryRequestRiderCandidate) {
    this._acceptedRider = rider;
    this._status = 'ACCEPTED';

    this.record(
      new DeliveryRequestAcceptedDomainEvent({
        aggregateId: this.id.value,
        eventData: {
          riderId: this._acceptedRider.id,
        },
        occurredOn: new Date(),
      }),
    );
  }

  updateRoutes(routes: Collection<RideRoute>) {
    this.props.routes = routes;
  }
}
