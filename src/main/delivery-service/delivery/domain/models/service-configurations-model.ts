import AggregateRoot from '@shared/domain/aggregate/aggregate-root';

export type DeliveryAvailableTypes = 'CAR' | 'MOTOR_CYCLE' | 'BIKE';

export type Price = {
  amount: number;
  currency: string;
};

export type DistanceRange = {
  min: number;
  max?: number;
};

export type Fee = {
  distanceRange: DistanceRange;
  baseFee: Price;
  kmFee?: Price;
  durationFee?: Price;
  additionalDurationFee?: Price;
};

export type SearchConfigurations = {
  attemptSearchRadius: number;
  attemptMaxCandidates: number;
  maxAttempts: number;
  acceptanceTimeout: number;
};

export type ServiceConfigurationsProps = {
  searchConfigurations: SearchConfigurations;
  feeConfigurations: {
    [type in DeliveryAvailableTypes]: Fee[];
  };
};

export type ServiceConfigurationsPrimitiveProps = {
  searchConfigurations: SearchConfigurations;
  feeConfigurations: {
    [type in DeliveryAvailableTypes]: Fee[];
  };
};

export default class ServiceConfigurations extends AggregateRoot<ServiceConfigurationsProps> {
  constructor(props: ServiceConfigurationsProps) {
    super({ ...props });
  }

  get attemptSearchRadius() {
    return this.props.searchConfigurations.attemptSearchRadius;
  }

  get acceptanceTimeout() {
    return this.props.searchConfigurations.acceptanceTimeout;
  }

  get maxAttempts() {
    return this.props.searchConfigurations.maxAttempts;
  }

  get attemptMaxCandidates() {
    return this.props.searchConfigurations.attemptMaxCandidates;
  }

  static fromPrimitives(plainData: ServiceConfigurationsPrimitiveProps) {
    return new ServiceConfigurations({
      ...plainData,
    });
  }

  guardExistFeesForType(type: string) {
    if (!this.props.feeConfigurations[type]) {
      throw new Error(`fees_not_found_for_category`);
    }
  }

  toPrimitives() {
    const json = this.toJson();

    return json;
  }
}
