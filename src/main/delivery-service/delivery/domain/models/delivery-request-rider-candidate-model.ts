import RiderPublicProfile, {
  DriverPublicProfilePrimitiveProps,
} from '@deliveryService/rider/domain/models/rider-public-profile-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { ObjectUtils } from '@shared/domain/utils';
import { RideGeoPoint } from './delivery-request-model';

export interface DeliveryRequestRiderCandidateProps {
  distanceToClient?: number;
  durationToClient?: number;
  distanceToStore?: number;
  durationToStore?: number;
  id: string;
  publicProfile?: RiderPublicProfile;
  notification?: any;
  countDown?: number;
  isBlockedToTakeRequest: boolean;
  attemptNumber: number;
  initialCoords?: RideGeoPoint;
  availableForRetry?: boolean;
}

export interface DeliveryRequestRiderCandidatePrimitiveProps {
  distanceToClient?: number;
  durationToClient?: number;
  distanceToStore?: number;
  durationToStore?: number;
  id: string;
  publicProfile?: DriverPublicProfilePrimitiveProps;
  isBlockedToTakeRequest: boolean;
  attemptNumber: number;
  notification?: any;
  countDown?: number;
  initialCoords?: RideGeoPoint;
  availableForRetry?: boolean;
}

export default class DeliveryRequestRiderCandidate extends AggregateRoot<DeliveryRequestRiderCandidateProps> {
  constructor(constructorData: DeliveryRequestRiderCandidateProps) {
    super({ ...constructorData });
  }

  get id() {
    return this.props.id;
  }

  get distanceToClient() {
    return this.props.distanceToClient;
  }

  get durationToClient() {
    return this.props.durationToClient;
  }

  get distanceToStore() {
    return this.props.distanceToStore;
  }

  get durationToStore() {
    return this.props.durationToStore;
  }

  get publicProfile() {
    return this.props.publicProfile;
  }

  get isBlockedToTakeRequest() {
    return this.props.isBlockedToTakeRequest;
  }

  get countDown() {
    return this.props.countDown;
  }

  get notification() {
    return this.props.notification;
  }

  get attemptNumber() {
    return this.props.attemptNumber;
  }

  get initialCoords() {
    return this.props.initialCoords;
  }

  get availableForRetry() {
    return this.props.availableForRetry;
  }

  static fromPrimitives(data: DeliveryRequestRiderCandidatePrimitiveProps) {
    const profile = new DeliveryRequestRiderCandidate({
      ...data,
      publicProfile: data.publicProfile
        ? RiderPublicProfile.fromPrimitives(data.publicProfile)
        : undefined,
    });

    return profile;
  }

  setAttemptNumber(number: number) {
    this.props.attemptNumber = number;
  }

  setAvailableForRetry(available) {
    this.props.availableForRetry = available;
  }

  setNotification(notification: any) {
    this.props.notification = notification;
  }

  setCountDown(second: number) {
    this.props.countDown = second;
  }

  toggleBlock(isBlocked: boolean) {
    this.props.isBlockedToTakeRequest = isBlocked;
  }

  toPrimitives(): DeliveryRequestRiderCandidatePrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown({
      id: json.id,
      publicProfile: json.publicProfile,
      distanceToClient: json.distanceToClient,
      durationToClient: json.durationToClient ? json.durationToClient : 0,
      distanceToStore: json.distanceToStore,
      durationToStore: json.durationToStore ? json.durationToStore : 0,
      isBlockedToTakeRequest: json.isBlockedToTakeRequest,
      attemptNumber: json.attemptNumber,
      availableForRetry: json.availableForRetry,
      initialCoords: json.initialCoords,
    });
  }
}
