import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { ObjectUtils } from '@shared/domain/utils';
import { RiderProfileProps } from './rider-profile';
import RiderRegisteredDomainEvent from '../events/rider-registered-domain-event';

export type RiderSignupDocuments = {
  personalReferencesUrl: string[];
  driverLicenseFrontUrl: string;
  driverLicenseBackUrl: string;
  medicalCertificateUrl: string;
  RCVpolicyUrl: string;
  criminalRecordUrl: string;
  rifUrl?: string;
  dniUrl: string;
  drivingPermitUrl: string;
};

export type RiderSignupVehicle = {
  type: string;
  hasBag: boolean;
  year: string;
  model: string;
  comments: string;
};

export interface RiderSignupProps
  extends Omit<
    RiderProfileProps,
    | 'rating'
    | 'badgeCount'
    | 'configurations'
    | 'isActive'
    | 'plainPassword'
    | 'status'
    | 'newPasswordToken'
    | 'hasDefaultPassword'
  > {
  documents: RiderSignupDocuments;
  vehicle: RiderSignupVehicle;
  deliveryRegion: {
    state: string;
    city: string;
  };
  availableSchedule: string;
  withdrawalDetails: {
    accountType: string;
    bank: string;
    accountNumber: string;
  };
  state: string;
  linkAccountToken?: string;
  createdAt: Date;
}

export interface RiderSignupPrimitiveProps
  extends Omit<RiderSignupProps, 'id'> {
  id: string;
}

const DefaultProps = {
  availableSchedule: 'FULL_TIME',
};

export default class RiderSignup extends AggregateRoot<RiderSignupProps> {
  constructor(props: RiderSignupProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get code() {
    return this.props.code;
  }

  get rif() {
    return this.props.basicInfo.rif;
  }

  get birthday() {
    return this.props.basicInfo.birthday;
  }

  get linkAccountToken() {
    return this.props.linkAccountToken;
  }

  get email() {
    return this.props.basicInfo.email;
  }

  get firstName() {
    return this.props.basicInfo.firstName;
  }

  get lastName() {
    return this.props.basicInfo.lastName;
  }

  get phone() {
    return this.props.basicInfo.phone;
  }

  get vehicleType() {
    return this.props.vehicle.type;
  }

  get referralCode() {
    return this.props.referralCode;
  }

  get referrerCode() {
    return this.props.referrerCode;
  }

  get deliveryRegion() {
    return this.props.deliveryRegion;
  }

  get homeAddress() {
    return this.props.basicInfo.homeAddress;
  }

  get createdAt() {
    return this.props.createdAt;
  }

  get identificationCard() {
    return this.props.basicInfo.identificationCard;
  }

  static create(data: RiderSignupProps): RiderSignup {
    const newItem = new RiderSignup(data);

    newItem.record(
      new RiderRegisteredDomainEvent({
        aggregateId: data.id.value,
        occurredOn: new Date(),
        eventData: {},
      }),
    );

    return newItem;
  }

  static fromPrimitives({
    ...plainData
  }: RiderSignupPrimitiveProps): RiderSignup {
    return new RiderSignup({
      ...plainData,
      id: new Id(plainData.id),
    });
  }

  toPrimitives(): RiderSignupPrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
