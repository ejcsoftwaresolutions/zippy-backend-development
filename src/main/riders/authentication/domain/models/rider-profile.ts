import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { ObjectUtils } from '@shared/domain/utils';
import RiderCreatedDomainEvent from '../events/rider-created-domain-event';

export type RiderHomeAddress = {
  line1: string;
  line2?: string;
  city: string;
  state: string;
  country: string;
  address: string;
  latitude?: number;
  longitude?: number;
};

export type RiderBasicInfo = {
  identificationCard: string;
  email: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  birthday?: Date;
  homeAddress?: RiderHomeAddress;
  rif: string;
};

export interface RiderProfileProps {
  id: Id;
  code: string;
  status: string;
  basicInfo: RiderBasicInfo;
  referralCode: string;
  referrerCode?: string;
  deliveryMethod: string;
  deliveryRegion: {
    state: string;
    city: string;
  };
  badgeCount: number;
  configurations: {
    referralPromotions: boolean;
    receiveEmails: boolean;
    receivePushNotifications: boolean;
    receiveSMS: boolean;
  };
}

export interface RiderProfilePrimitiveProps
  extends Omit<RiderProfileProps, 'id'> {
  id: string;
}

const DefaultProps = {
  isActive: false,
  availableSchedule: 'FULL_TIME',
  hasDefaultPassword: true,
};

export default class RiderProfile extends AggregateRoot<RiderProfileProps> {
  constructor(props: RiderProfileProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get email() {
    return this.props.basicInfo.email;
  }

  get rif() {
    return this.props.basicInfo.rif;
  }

  get firstName() {
    return this.props.basicInfo.firstName;
  }

  get lastName() {
    return this.props.basicInfo.lastName;
  }

  get phone() {
    return this.props.basicInfo.phone;
  }

  get referralCode() {
    return this.props.referralCode;
  }

  get referrerCode() {
    return this.props.referrerCode;
  }

  get identificationCard() {
    return this.props.basicInfo.identificationCard;
  }

  static create(data: RiderProfileProps): RiderProfile {
    const newItem = new RiderProfile(data);

    newItem.record(
      new RiderCreatedDomainEvent({
        aggregateId: data.id.value,
        occurredOn: new Date(),
        eventData: {},
      }),
    );

    return newItem;
  }

  static fromPrimitives({
    ...plainData
  }: RiderProfilePrimitiveProps): RiderProfile {
    return new RiderProfile({
      ...plainData,
      id: new Id(plainData.id),
    });
  }

  update(updates: Partial<RiderProfileProps>) {
    Object.assign(this, {
      ...this.props,
      ...updates,
    });
  }

  toPrimitives(): RiderProfilePrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
