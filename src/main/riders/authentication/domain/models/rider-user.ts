import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { ObjectUtils } from '@shared/domain/utils';
import UserAccount, {
  UserAccountPrimitiveProps,
} from '@shared/domain/models/user-account';
import RiderProfile, { RiderProfilePrimitiveProps } from './rider-profile';

export interface RiderUserProps {
  id: Id;
  account: UserAccount;
  profile: RiderProfile;
}

export interface RiderUserPrimitiveProps {
  id: string;
  account: UserAccountPrimitiveProps;
  profile: RiderProfilePrimitiveProps;
}

const DefaultProps = {};

export default class RiderUser extends AggregateRoot<RiderUserProps> {
  constructor(props: RiderUserProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get email() {
    return this.props.account.email;
  }

  get firstName() {
    return this.props.account.firstName;
  }

  get lastName() {
    return this.props.account.lastName;
  }

  get phone() {
    return this.props.account.phone;
  }

  get plainPassword() {
    return this.props.account.plainPassword;
  }

  get createdAt() {
    return this.props.account.createdAt;
  }

  static create(data: RiderUserProps): RiderUser {
    return new RiderUser(data);
  }

  static fromPrimitives({ ...plainData }: RiderUserPrimitiveProps): RiderUser {
    return new RiderUser({
      ...plainData,
      id: new Id(plainData.id),
      profile: RiderProfile.fromPrimitives(plainData.profile),
      account: UserAccount.fromPrimitives(plainData.account),
    });
  }

  update(updates: Partial<RiderUserProps>) {
    Object.assign(this, {
      ...this.props,
      ...updates,
    });
  }

  toPrimitives(): RiderUserPrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
