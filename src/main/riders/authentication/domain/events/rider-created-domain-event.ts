import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'RIDER_CREATED';

interface EventProps extends DomainEventProps {
  eventData: {
  };
}

export default class RiderCreatedDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
