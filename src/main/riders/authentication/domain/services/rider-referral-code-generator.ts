import CodeGenerator from '@shared/domain/utils/code-generator';

export default class RiderReferralCodeGenerator {
  constructor(private codeGenerator: CodeGenerator) {}

  generate(params: { firstName: string; lastName: string }): string {
    return `${params.firstName.substring(0, 1).toUpperCase()}${params.lastName
      .substring(0, 1)
      .toUpperCase()}${this.codeGenerator.generate(4)}`;
  }
}
