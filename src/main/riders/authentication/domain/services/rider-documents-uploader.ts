import {UploadFile} from '@shared/domain/storage/storage-types';
import FileUploader from '@shared/domain/storage/storage-uploader';
import Collection from '@shared/domain/value-object/collection';
import {FileUploadResponse} from '@shared/domain/storage/file-upload-response';

export default class RiderDocumentsUploader {
    public static readonly uploadPath = 'riders';

    constructor(private fileUploader: FileUploader) {
    }

    async uploadDocuments(
        riderId: string,
        files: { file: UploadFile; key: string }[],
    ): Promise<FileUploadResponse[]> {
        try {
            const finalFiles = new Collection(
                files.map((f) => ({
                    file: f.file,
                    undefined,
                    fileKey: f.key,
                    filePath: `${
                        RiderDocumentsUploader.uploadPath
                    }/${riderId}/documents/${f.key
                    .replace('Url', '')
                    .replace(/[0-9]/g, '')
                    .toUpperCase()}`,
                })),
            );

            const response = await this.fileUploader.uploadFiles(finalFiles);

            return response.toPrimitives();
        } catch (error) {
            console.log(error)
            throw new Error('Not possible to upload driver documents');
        }
    }
}
