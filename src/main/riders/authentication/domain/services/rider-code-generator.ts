import CodeGenerator from '@shared/domain/utils/code-generator';

export default class RiderCodeGenerator {
  constructor(private codeGenerator: CodeGenerator) {}

  generate(params: { firstName: string; lastName: string }): string {
    const firstNamePart = params.firstName.substring(0, 3).toUpperCase(); // 3
    const lastNamePart = params.lastName.substring(0, 3).toUpperCase(); // 3
    const randomNumbers = this.codeGenerator.generate(4); //4
    return `${firstNamePart}${lastNamePart}${randomNumbers}`;
  }
}
