import {inject} from '@shared/domain/decorators';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import {FIREBASE_ADMIN_INJECT, FirebaseAdminSDK,} from '@tfarras/nestjs-firebase-admin';
import {EntityConstructorOrPath} from 'fireorm';
import RiderCommandRepository from 'src/main/riders/authentication/domain/repositories/rider-command-repository';
import {RiderAdminEntity} from '../entities/rider-admin-entity';
import RiderAdminMapper from '../mappers/rider-admin-mapper';
import RiderSignup from '../../../../domain/models/rider-signup';
import Id from '@shared/domain/id/id';
import {RiderWithdrawalDetailsEntity} from '../entities/rider-withdrawal-details-entity';
import RiderUser from '../../../../domain/models/rider-user';
import RiderUserMapper from '../mappers/rider-user-mapper';
import UserAccount from '@shared/domain/models/user-account';
import {UserRole} from '@shared/domain/models/user-role';
import {RiderUserEntity} from '../entities/rider-user-entity';
import {RiderEntity} from '../entities/rider-entity';
import RiderProfile from '../../../../domain/models/rider-profile';
import RiderProfileMapper from '../mappers/rider-profile-mapper';

export default class RiderCommandInfrastructureRepository
    extends FireOrmRepository<RiderUserEntity>
    implements RiderCommandRepository {
    static readonly bindingKey = 'RiderCommandRepository';

    constructor(
        @inject(FIREBASE_ADMIN_INJECT) private firebase: FirebaseAdminSDK,
    ) {
        super();
    }

    getEntityClass(): EntityConstructorOrPath<RiderUserEntity> {
        return RiderUserEntity;
    }

    async register(user: RiderSignup): Promise<void> {
        const entity = RiderAdminMapper.toPersistence(user);

        await Promise.all([
            this.getEntityRepository(RiderAdminEntity).create(entity),
        ]);
    }

    async existsUserByEmail(email: string): Promise<boolean> {
        const data = await this.getEntityRepository(RiderAdminEntity)
        .whereEqualTo('email', email)
        .findOne();

        return !!data;
    }

    async approve(publicProfile: RiderUser): Promise<void> {
        const account = RiderUserMapper.toPersistence(publicProfile);

        const profile = RiderProfile.fromPrimitives(
            publicProfile.toPrimitives().profile,
        );

        const withdrawalDetails = new RiderWithdrawalDetailsEntity();

        const signupRider = await this.findRiderSignupByEmail(publicProfile.email);

        if (!signupRider) {
            throw new Error('SIGNUP_INFO_NOT_FOUND');
        }

        const signupPrimitives = signupRider.toPrimitives();

        withdrawalDetails.create({
            id: account.id,
            type: 'bank',
            driverId: account.id,
            details: {
                ...signupPrimitives.withdrawalDetails,
            },
        });

        await Promise.all([
            ...(signupPrimitives.linkAccountToken
                ? [this.repository().update(account)]
                : [
                    this.firebase.auth().createUser({
                        email: publicProfile.email,
                        password: publicProfile.plainPassword,
                        uid: publicProfile.id.value,
                    }),
                    this.repository().create(account),
                ]),
            this.getEntityRepository(RiderEntity).create(
                RiderProfileMapper.toPersistence(profile),
            ),
            this.getEntityRepository(RiderWithdrawalDetailsEntity).create(
                withdrawalDetails,
            ),
        ] as Promise<any>[]);
    }

    async findRider(id: Id): Promise<RiderUser> {
        const item = await this.repository().whereEqualTo('id', id.value).findOne();
        if (!item) return;
        const profile = await this.getEntityRepository(RiderEntity)
        .whereEqualTo('id', id.value)
        .findOne();

        return RiderUserMapper.toDomain({account: item, profile: profile});
    }

    async findRiderSignup(id: Id): Promise<RiderSignup> {
        const item = await this.getEntityRepository(RiderAdminEntity)
        .whereEqualTo('id', id.value)
        .findOne();
        if (!item) return;
        return RiderAdminMapper.toDomain(item);
    }

    async findAccountByEmail(email: string): Promise<UserAccount> {
        const data = await this.repository().whereEqualTo('email', email).findOne();
        if (!data) return;
        return UserAccount.fromPrimitives({
            id: data.id,
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName,
            phone: data.phone,
            createdAt: data.createdAt,
            hasDefaultPassword: data.hasDefaultPassword,
            newPasswordToken: data.newPasswordToken,
            roles: data.roles as UserRole[],
            status: data.status,
        });
    }

    private async findRiderSignupByEmail(email: string): Promise<RiderSignup> {
        const item = await this.getEntityRepository(RiderAdminEntity)
        .whereEqualTo('email', email)
        .findOne();

        if (!item) return;

        return RiderAdminMapper.toDomain(item);
    }
}
