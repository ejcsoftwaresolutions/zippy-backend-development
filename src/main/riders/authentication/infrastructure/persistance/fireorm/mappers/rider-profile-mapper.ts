import RiderProfile from 'src/main/riders/authentication/domain/models/rider-profile';
import { RiderEntity } from '../entities/rider-entity';
import { ObjectUtils } from '@shared/domain/utils';

export default class RiderProfileMapper {
  static toDomain(plainValues: RiderEntity): RiderProfile {
    return RiderProfile.fromPrimitives({
      id: plainValues.id,
      basicInfo: {
        rif: plainValues.rif,
        email: plainValues.email,
        firstName: plainValues.firstName,
        lastName: plainValues.lastName,
        phone: plainValues.phone,
        identificationCard: plainValues.identificationCard,
        homeAddress: {
          ...plainValues.homeAddress,
          ...ObjectUtils.omit(plainValues.homeAddress, 'formattedAddress'),
        },
      },
      referralCode: plainValues.referralCode,
      referrerCode: plainValues.referrerCode,
      badgeCount: plainValues.badgeCount,
      configurations: plainValues.configurations,
      deliveryMethod: plainValues.deliveryMethod,
      status: plainValues.status,
      deliveryRegion: plainValues.deliveryRegion,
      code: plainValues.code,
    });
  }

  static toPersistence(user: RiderProfile): RiderEntity {
    const primitives = user.toPrimitives();

    const entity = new RiderEntity();

    entity.create(
      ObjectUtils.omitUnknown({
        id: primitives.id,
        email: primitives.basicInfo.email,
        firstName: primitives.basicInfo.firstName,
        lastName: primitives.basicInfo.lastName,
        phone: primitives.basicInfo.phone,
        identificationCard: primitives.basicInfo.identificationCard,
        referralCode: primitives.referralCode,
        referrerCode: primitives.referrerCode,
        badgeCount: primitives.badgeCount,
        configurations: primitives.configurations,
        deliveryMethod: primitives.deliveryMethod,
        homeAddress: {
          ...ObjectUtils.omit(primitives.basicInfo.homeAddress, [
            'latitude',
            'longitude',
            'address',
          ]),
        },
        rif: primitives.basicInfo.rif,
        status: primitives.status,
        deliveryRegion: primitives.deliveryRegion,
        code: primitives.code,
      }),
    );

    return entity;
  }
}
