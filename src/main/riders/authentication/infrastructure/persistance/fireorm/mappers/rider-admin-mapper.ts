import { RiderAdminEntity } from '../entities/rider-admin-entity';
import RiderSignup from '../../../../domain/models/rider-signup';
import { ObjectUtils } from '@shared/domain/utils';

export default class RiderAdminMapper {
  static toDomain(plainValues: RiderAdminEntity): RiderSignup {
    return RiderSignup.fromPrimitives({
      id: plainValues.id,
      state: plainValues.state,
      basicInfo: {
        rif: plainValues.rif,
        birthday: plainValues.birthday,
        email: plainValues.email,
        firstName: plainValues.firstName,
        lastName: plainValues.lastName,
        phone: plainValues.phone,
        identificationCard: plainValues.identificationCard,
        homeAddress: {
          ...plainValues.homeAddress,
          ...ObjectUtils.omit(plainValues.homeAddress, 'formattedAddress'),
        },
      },
      referralCode: plainValues.referralCode,
      referrerCode: plainValues.referrerCode,
      deliveryMethod: plainValues.deliveryMethod,
      createdAt: plainValues.createdAt,
      availableSchedule: plainValues.availableSchedule,
      deliveryRegion: plainValues.deliveryRegion,
      documents: plainValues.documents,
      vehicle: plainValues.vehicle,
      withdrawalDetails: plainValues.withdrawalDetails,
      code: plainValues.code,
      linkAccountToken: plainValues.linkAccountToken,
    });
  }

  static toPersistence(user: RiderSignup): RiderAdminEntity {
    const primitives = user.toPrimitives();

    const entity = new RiderAdminEntity();

    entity.create(
      ObjectUtils.omitUnknown({
        id: primitives.id,
        email: primitives.basicInfo.email,
        firstName: primitives.basicInfo.firstName,
        lastName: primitives.basicInfo.lastName,
        phone: primitives.basicInfo.phone,
        birthday: primitives.basicInfo.birthday,
        identificationCard: primitives.basicInfo.identificationCard,
        homeAddress: {
          ...ObjectUtils.omit(primitives.basicInfo.homeAddress, [
            'latitude',
            'longitude',
            'address',
          ]),
        },
        referralCode: primitives.referralCode,
        referrerCode: primitives.referrerCode,
        deliveryMethod: primitives.deliveryMethod,
        availableSchedule: primitives.availableSchedule,
        deliveryRegion: primitives.deliveryRegion,
        documents: ObjectUtils.omitUnknown(primitives.documents),
        vehicle: ObjectUtils.omitUnknown(primitives.vehicle),
        createdAt: primitives.createdAt,
        withdrawalDetails: ObjectUtils.omitUnknown(
          primitives.withdrawalDetails,
        ),
        rif: primitives.basicInfo.rif,
        state: primitives.state,
        code: primitives.code,
        linkAccountToken: primitives.linkAccountToken,
      }),
    );

    return entity;
  }
}
