import UserAccount from '@shared/domain/models/user-account';
import { UserRole } from '@shared/domain/models/user-role';
import { RiderUserEntity } from '../entities/rider-user-entity';
import { RiderEntity } from '../entities/rider-entity';
import RiderProfileMapper from './rider-profile-mapper';
import RiderUser from '../../../../domain/models/rider-user';
import { ObjectUtils } from '@shared/domain/utils';

export default class RiderUserMapper {
  static toDomain(plainValues: {
    account: RiderUserEntity;
    profile: RiderEntity;
  }): RiderUser {
    const { account, profile } = plainValues;

    return RiderUser.fromPrimitives({
      id: account.id,
      account: UserAccount.fromPrimitives({
        id: account.id,
        email: account.email,
        firstName: account.firstName,
        lastName: account.lastName,
        phone: account.phone,
        createdAt: account.createdAt,
        birthday: account.birthday,
        hasDefaultPassword: account.hasDefaultPassword,
        newPasswordToken: account.newPasswordToken,
        roles: account.roles as UserRole[],
        status: account.status,
      }).toPrimitives(),
      profile: RiderProfileMapper.toDomain(profile).toPrimitives(),
    });
  }

  static toPersistence(user: RiderUser): RiderUserEntity {
    const primitives = user.toPrimitives();

    const entity = new RiderUserEntity();

    entity.create(
      ObjectUtils.omitUnknown({
        id: primitives.id,
        email: primitives.account.email,
        firstName: primitives.account.firstName,
        lastName: primitives.account.lastName,
        phone: primitives.account.phone,
        createdAt: primitives.account.createdAt,
        hasDefaultPassword: primitives.account.hasDefaultPassword,
        newPasswordToken: primitives.account.newPasswordToken,
        roles: primitives.account.roles,
        status: primitives.account.status,
        birthday: primitives.account.birthday,
      }),
    );

    return entity;
  }
}
