import { Collection } from 'fireorm';
import { ObjectUtils } from '@shared/domain/utils';

@Collection('driver_withdrawal_details')
export class RiderWithdrawalDetailsEntity {
  id: string;
  driverId: string;
  type: string;
  details: any;

  create(props: any) {
    Object.assign(this, ObjectUtils.omitUnknown(props));
  }
}
