import CommandHandler from '@shared/domain/bus/command/command-handler';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import CodeGenerator from '@shared/domain/utils/code-generator';
import RiderReferralCodeGenerator from '../../domain/services/rider-referral-code-generator';
import RegisterRiderCommand from './register-rider-command';
import RegisterRiderUseCase from './register-rider-use-case';
import RiderDocumentsUploader from '../../domain/services/rider-documents-uploader';
import FileUploader from '@shared/domain/storage/storage-uploader';
import {inject} from '@shared/domain/decorators';
import RiderCodeGenerator from '../../domain/services/rider-code-generator';
import {RiderSignupDocuments, RiderSignupVehicle,} from '../../domain/models/rider-signup';
import {RiderBasicInfo} from '../../domain/models/rider-profile';

@service()
export default class RegisterRiderCommandHandler implements CommandHandler {
    private riderReferralCodeGenerator: RiderReferralCodeGenerator;
    private riderCodeGenerator: RiderCodeGenerator;
    private documentsUploader: RiderDocumentsUploader;

    constructor(
        @inject('RegisterRiderUseCase')
        private useCase: RegisterRiderUseCase,
        @inject('code.generator')
            codeGenerator: CodeGenerator,
        @inject('file.uploader')
            fileUploader: FileUploader,
    ) {
        this.riderReferralCodeGenerator = new RiderReferralCodeGenerator(
            codeGenerator,
        );

        this.riderCodeGenerator = new RiderCodeGenerator(codeGenerator);
        this.documentsUploader = new RiderDocumentsUploader(fileUploader);
    }

    getCommandName(): string {
        return RegisterRiderCommand.name;
    }

    async handle(command: RegisterRiderCommand) {
        const id = new Id(command.registerInfo.id); // new Id();

        const {registerInfo} = command;

        const referralCode = this.riderReferralCodeGenerator.generate({
            firstName: registerInfo.basicInfo.firstName,
            lastName: registerInfo.basicInfo.lastName,
        });

        const code = this.riderCodeGenerator.generate({
            firstName: registerInfo.basicInfo.firstName,
            lastName: registerInfo.basicInfo.lastName,
        });

        const filesUrls = command.registerInfo.documents;

        const vehicle: RiderSignupVehicle = {
            comments: registerInfo.vehicle.comments,
            hasBag: registerInfo.vehicle.hasBag,
            year: registerInfo.vehicle.year,
            type: registerInfo.vehicle.type,
            model: registerInfo.vehicle.model,
        };

        const documents: RiderSignupDocuments = {
            RCVpolicyUrl: filesUrls['RCVpolicyUrl'],
            dniUrl: filesUrls['dniUrl'],
            criminalRecordUrl: filesUrls['criminalRecordUrl'],
            driverLicenseBackUrl: filesUrls['driverLicenseBackUrl'],
            driverLicenseFrontUrl: filesUrls['driverLicenseFrontUrl'],
            medicalCertificateUrl: filesUrls['medicalCertificateUrl'],
            personalReferencesUrl: (filesUrls as any)['personalReferencesUrl'],
            rifUrl: filesUrls['rifUrl'],
            drivingPermitUrl: filesUrls['drivingPermitUrl'],
        };

        const basicInfo: RiderBasicInfo = registerInfo.basicInfo;

        await this.useCase.execute({
            id: id,
            code: code,
            basicInfo: basicInfo,
            vehicle: vehicle,
            deliveryMethod: registerInfo.vehicle.type,
            documents: documents,
            withdrawalDetails: registerInfo.withdrawalDetails,
            deliveryRegion: registerInfo.deliveryRegion,
            referrerCode: registerInfo.referrerCode,
            referralCode: referralCode,
            availableSchedule: registerInfo.availableSchedule,
            linkAccountToken: registerInfo.linkAccountToken,
        });

        return {
            id: id.value,
        };
    }

}
