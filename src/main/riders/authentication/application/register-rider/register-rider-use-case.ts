import EventBus from '@shared/domain/bus/event/event-bus';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import RiderCommandRepository from '../../domain/repositories/rider-command-repository';
import RiderSignup, {
  RiderSignupDocuments,
  RiderSignupVehicle,
} from '../../domain/models/rider-signup';
import { inject } from '@shared/domain/decorators';
import { RiderBasicInfo } from '../../domain/models/rider-profile';

interface Params {
  id: Id;
  basicInfo: RiderBasicInfo;
  referrerCode: string;
  referralCode: string;
  documents: RiderSignupDocuments;
  vehicle: RiderSignupVehicle;
  deliveryRegion: {
    state: string;
    city: string;
  };
  deliveryMethod: string;
  withdrawalDetails: {
    accountType: string;
    bank: string;
    accountNumber: string;
  };
  availableSchedule: string;
  code: string;
  linkAccountToken?: string;
}

@service()
export default class RegisterRiderUseCase {
  constructor(
    @inject('event.bus')
    private eventBus: EventBus,
    @inject('RiderCommandRepository')
    private riderRepository: RiderCommandRepository,
  ) {}

  async execute(data: Params) {
    const foundUser = await this.riderRepository.existsUserByEmail(
      data.basicInfo.email,
    );

    if (!data.linkAccountToken && foundUser) {
      throw new Error('user_already_exists');
    }

    const item = RiderSignup.create({
      id: data.id,
      code: data.code,
      basicInfo: data.basicInfo,
      createdAt: new Date(),
      referralCode: data.referralCode,
      referrerCode: data.referrerCode,
      deliveryMethod: data.deliveryMethod,
      availableSchedule: data.availableSchedule,
      deliveryRegion: {
        city: data.deliveryRegion.city,
        state: data.deliveryRegion.state,
      },
      documents: data.documents,
      vehicle: data.vehicle,
      withdrawalDetails: {
        accountNumber: data.withdrawalDetails.accountNumber,
        accountType: data.withdrawalDetails.accountType,
        bank: data.withdrawalDetails.bank,
      },
      state: 'NEW',
      linkAccountToken: data.linkAccountToken,
    });

    await this.riderRepository.register(item);

    this.eventBus.publish(item.pullDomainEvents());
  }
}
