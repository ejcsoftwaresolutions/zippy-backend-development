import Command from '@shared/domain/bus/command/command';

interface RegisterInfo {
  id: string;
  basicInfo: {
    identificationCard: string;
    email: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    birthday?: Date;
    homeAddress: {
      line1: string;
      line2?: string;
      address: string;
      city: string;
      state: string;
      country: string;
      latitude?: number;
      longitude?: number;
    };
    rif: string;
  };
  referrerCode: string;
  documents: {
    personalReferencesUrl: string[];
    driverLicenseFrontUrl: string;
    driverLicenseBackUrl: string;
    medicalCertificateUrl: string;
    RCVpolicyUrl: string;
    criminalRecordUrl: string;
    rifUrl: string;
    dniUrl: string;
    drivingPermitUrl: string;
  };
  vehicle: {
    type: string;
    hasBag: boolean;
    year: string;
    model: string;
    comments: string;
  };
  deliveryRegion: {
    state: string;
    city: string;
  };
  withdrawalDetails: {
    accountType: string;
    bank: string;
    accountNumber: string;
  };
  availableSchedule: string;
  linkAccountToken?: string;
}

export default class RegisterRiderCommand extends Command {
  constructor(public readonly registerInfo: RegisterInfo) {
    super();
  }

  name(): string {
    return RegisterRiderCommand.name;
  }
}
