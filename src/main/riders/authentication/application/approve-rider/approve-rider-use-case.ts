import EventBus from '@shared/domain/bus/event/event-bus';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import RiderCommandRepository from '../../domain/repositories/rider-command-repository';
import RiderProfile from '../../domain/models/rider-profile';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import UserAccount from '@shared/domain/models/user-account';
import RiderUser from '../../domain/models/rider-user';

@service()
export default class ApproveRiderUseCase {
  constructor(
    @inject('event.bus')
    private eventBus: EventBus,
    @inject('RiderCommandRepository')
    private repo: RiderCommandRepository,
    @inject('user.authentication.token.creator')
    private tokenCreator: AuthTokenCreator<any>,
  ) {}

  async execute(data: { id: Id; account: UserAccount; profile: RiderProfile }) {
    const user = RiderUser.create({
      id: data.id,
      account: data.account,
      profile: data.profile,
    });

    await this.repo.approve(user);
    this.eventBus.publish(user.pullDomainEvents());
  }
}
