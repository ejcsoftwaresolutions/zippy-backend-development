import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import CodeGenerator from '@shared/domain/utils/code-generator';
import ApproveRiderCommand from './approve-rider-command';
import ApproveRiderUseCase from './approve-rider-use-case';
import RiderCommandRepository from '../../domain/repositories/rider-command-repository';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import RiderLinkAccountRequestValidator from '../../domain/services/rider-link-account-request-validator';
import UserAccount from '@shared/domain/models/user-account';
import RiderProfile from '../../domain/models/rider-profile';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import RiderCodeGenerator from '../../domain/services/rider-code-generator';

@service()
export default class ApproveRiderCommandHandler implements CommandHandler {
  private uniqueCodeGenerator: RiderCodeGenerator;
  private linkAccountValidator: RiderLinkAccountRequestValidator;

  constructor(
    @inject('ApproveRiderUseCase')
    private useCase: ApproveRiderUseCase,
    @inject('code.generator')
    codeGenerator: CodeGenerator,
    @inject('RiderCommandRepository')
    private repository: RiderCommandRepository,
    @inject('user.authentication.token.verificator')
    private tokenVerificator: AuthTokenVerificator<any>,
    @inject('user.authentication.token.creator')
    private tokenCreator: AuthTokenCreator<any>,
  ) {
    this.uniqueCodeGenerator = new RiderCodeGenerator(codeGenerator);
    this.linkAccountValidator = new RiderLinkAccountRequestValidator(
      tokenVerificator,
    );
  }

  getCommandName(): string {
    return ApproveRiderCommand.name;
  }

  async handle(command: ApproveRiderCommand) {
    const { approveInfo } = command;

    const id = new Id(approveInfo.id);

    const signupData = await this.repository.findRiderSignup(id);

    const foundAccount = await this.repository.findAccountByEmail(
      signupData.email,
    );

    const code = this.uniqueCodeGenerator.generate({
      firstName: signupData.firstName,
      lastName: signupData.lastName,
    });

    const approvedAccount = await (async () => {
      if (foundAccount) {
        await this.linkAccountValidator.validate(signupData.linkAccountToken);
        foundAccount.addRole('RIDER');
        return foundAccount;
      }

      const newPassToken = await this.tokenCreator.generate({
        id: id.value,
        userType: 'RIDER',
      });

      return UserAccount.fromPrimitives({
        id: id.value,
        email: signupData.email,
        firstName: signupData.firstName,
        lastName: signupData.lastName,
        phone: signupData.phone,
        plainPassword: signupData.identificationCard,
        roles: ['RIDER'],
        birthday: signupData.birthday,
        hasDefaultPassword: true,
        createdAt: new Date(),
        newPasswordToken: newPassToken,
        status: 'ACTIVE',
      });
    })();

    const profile = RiderProfile.fromPrimitives({
      id: approvedAccount.id.value,
      code: code,
      referralCode: code,
      status: 'ACTIVE',
      badgeCount: 0,
      deliveryMethod: signupData.vehicleType,
      referrerCode: signupData.referrerCode,
      deliveryRegion: signupData.deliveryRegion,
      basicInfo: {
        rif: signupData.rif,
        email: signupData.email,
        firstName: signupData.firstName,
        lastName: signupData.lastName,
        phone: signupData.phone,
        homeAddress: signupData.homeAddress,
        identificationCard: signupData.identificationCard,
      },
      configurations: {
        referralPromotions: true,
        receivePushNotifications: true,
        receiveSMS: true,
        receiveEmails: true,
      },
    });

    await this.useCase.execute({
      id: approvedAccount.id,
      account: approvedAccount,
      profile: profile,
    });

    return {
      id: approvedAccount.id.value,
    };
  }
}
