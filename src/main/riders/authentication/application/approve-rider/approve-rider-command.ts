import Command from '@shared/domain/bus/command/command';

interface ApproveInfo {
  id: string;
}

export default class ApproveRiderCommand extends Command {
  constructor(public readonly approveInfo: ApproveInfo) {
    super();
  }

  name(): string {
    return ApproveRiderCommand.name;
  }
}
