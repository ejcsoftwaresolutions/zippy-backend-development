import { Collection } from 'fireorm';

@Collection('vendor_categories')
export class VendorProductCategoryEntity {
  id: string;
}
