import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { ObjectUtils } from '@shared/domain/utils';

export interface VendorExportProps {
  id: string;
  email: string;
  title: string;
  rif: string;
  phone: string;
  city: string;
  state: string;
  createdAt?: Date;
}

export type VendorExportPrimitiveProps = VendorExportProps;

export default class VendorExport extends AggregateRoot<VendorExportProps> {
  constructor(props: VendorExportProps) {
    super({
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get email() {
    return this.props.email;
  }

  get title() {
    return this.props.title;
  }

  get phone() {
    return this.props.phone;
  }

  get createdAt() {
    return this.props.createdAt;
  }

  static fromPrimitives({
    ...plainData
  }: VendorExportPrimitiveProps): VendorExport {
    return new VendorExport(plainData);
  }

  toPrimitives(): VendorExportPrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
