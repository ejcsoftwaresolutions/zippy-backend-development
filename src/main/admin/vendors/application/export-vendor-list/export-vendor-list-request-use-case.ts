import service from '@shared/domain/decorators/service';
import {inject} from '@shared/domain/decorators';
import PdfGenerator from '@shared/domain/pdf/pdf-generator';
import ExcelGenerator from '@shared/domain/excel/excel-generator';
import firebase from 'firebase-admin';
import {DateTimeUtils} from '@shared/domain/utils';
import ExcelRequest from '@shared/domain/excel/excel-request';
import VendorExport from '@admin/vendors/domain/models/vendor-export';
import Id from "@shared/domain/id/id";

const path = require('path');
const fs = require('fs');
const ejs = require('ejs');


@service()
export default class ExportVendorListRequestUseCase {
    constructor(
        @inject('pdf.generator')
        private pdfGenerator: PdfGenerator,
        @inject('excel.generator')
        private excelGenerator: ExcelGenerator,
    ) {
    }

    async execute(requestData: {
        format: string;
        email: string;
        filters?: any;
        limit?: number;
        skip?: number;
        order?: any;
    }) {
        const data = await this.getData();

        let fileUrl: string | undefined;

        if (requestData.format === "PDF") {
            await this.requestPdf({
                items: data,
                email: requestData.email,
                title: 'Lista de Zippi aliados',
            })
            return
        }

        try {
            fileUrl = await this.exportExcel(data);
            console.log(fileUrl);

            await this.sendEmail({
                email: requestData.email,
                title: 'Lista de Zippi aliados',
                url: fileUrl,
            });
        } catch (e) {
            console.log(e);
        }

    }

    private async getData(
        filters?: any,
        limit?: number,
        skip?: number,
        order?: any,
    ): Promise<VendorExport[]> {
        const items = await firebase
        .firestore()
        .collection('vendor_applications')
        .where('state', '==', 'APPROVED');

        const data = await items.get();

        const fItems = data.docs.map((s) => s.data());

        return fItems.map((item: any) => {
            return VendorExport.fromPrimitives({
                id: item.id,
                email: item.email,
                title: item.title,
                phone: item.phone,
                state: item.address.state,
                city: item.address.city,
                rif: item.rif,
                createdAt: new Date((item.createdAt as any).seconds * 1000),
            });
        });
    }

    private async requestPdf({
        email,
        items,
        title,
    }: {
        items: VendorExport[],
        email: string,
        title: string,
    }): Promise<void> {

        const pdfTemplatesFolder = path.resolve("public/pdf-templates")
        const pdfTemplate = fs.readFileSync(pdfTemplatesFolder + "/vendor-list.ejs", "utf-8")

        const pdfContent = await this.parseTemplate(pdfTemplate, {
            riders: items
            .map((i) => i.toPrimitives())
            .map((i) => ({
                email: i.email,
                title: i.title,
                phone: i.phone,
                state: i.state,
                city: i.city,
                rif: i.rif,
                createdAt: DateTimeUtils.format(i.createdAt, 'DD/MM/YYYY'),
            })),
        })


        await firebase
        .firestore()
        .collection('admin_pdf')
        .add({
            email: email,
            slug: "VENDOR_LIST",
            content: pdfContent,
            title: title,
            status: "REQUESTED",
            fileName: `admin/pdf/vendors-${new Id().value}.pdf`,
            pdfConfig: {}
        });
    }

    private async parseTemplate(
        template: string,
        variables: any,
    ): Promise<string> {
        return ejs.render(template, variables, {})
    }

    private async exportExcel(items: VendorExport[]): Promise<string> {
        const styles = {
            header: {
                font: {
                    color: {
                        rgb: '000',
                    },
                    sz: 12,
                    bold: true,
                    underline: false,
                },
            },
        };

        const result = await this.excelGenerator.generate(
            new ExcelRequest({
                uploadFolderUrl: 'admin/excel',
                data: items
                .map((i) => i.toPrimitives())
                .map((i) => ({
                    email: i.email,
                    title: i.title,
                    phone: i.phone,
                    state: i.state,
                    city: i.city,
                    rif: i.rif,
                    createdAt: DateTimeUtils.format(i.createdAt, 'DD/MM/YYYY'),
                })),
                specification: {
                    title: {
                        displayName: 'Nombre',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    phone: {
                        displayName: 'Teléfono',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    email: {
                        displayName: 'Email',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    rif: {
                        displayName: 'RIF',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    state: {
                        displayName: 'Estado',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    city: {
                        displayName: 'Ciudad',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    createdAt: {
                        displayName: 'Fecha de registro',
                        width: 120,
                        headerStyle: styles.header,
                    },
                },
            }),
        );

        return result.url;
    }

    private async sendEmail({
        email,
        url,
        title,
    }: {
        email: string;
        url: string;
        title: string;
    }) {
        await firebase
        .firestore()
        .collection('mail')
        .add({
            to: email,
            message: {
                subject: `${title}`,
                html: `
                  <div>
                       <p>
                         Reporte generado, puedes descargar el archivo  <a href="${url}">aquí</a> !.
                      </p>
                    <br/>
                  </div>
                `,
            },
        });
    }
}
