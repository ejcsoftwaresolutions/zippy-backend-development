import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import ExportVendorListRequestUseCase from './export-vendor-list-request-use-case';
import ExportVendorListRequestedDomainEvent from '@admin/vendors/domain/events/export-vendor-list-requested-domain-event';

@service()
export default class ExportVendorListRequestEventSubscriber
  implements EventSubscriber
{
  constructor(
    @inject('ExportVendorListRequestUseCase')
    private useCase: ExportVendorListRequestUseCase,
  ) {}

  public subscribedTo(): Map<string, Function> {
    const events = new Map<string, Function>();
    events.set(
      ExportVendorListRequestedDomainEvent.eventName,
      this.handle.bind(this),
    );
    return events;
  }

  public async handle(event: ExportVendorListRequestedDomainEvent) {
    await this.useCase.execute(event.props.eventData);
  }
}
