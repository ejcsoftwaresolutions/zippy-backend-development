import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import PasswordHasher from '@shared/domain/utils/password-hasher';
import AuthAdminCommandRepository from '../../domain/repositories/auth-admin-command-repository';

type UserProps = {
  id: Id;
  email: string;
  password: string;
  roleId: string;
};

@service()
export default class RegisterAdminUseCase {
  constructor(
    @inject('AuthAdminCommandRepository')
    private authRepository: AuthAdminCommandRepository,
    @inject('utils.passwordHasher')
    private passwordHasher: PasswordHasher,
  ) {}

  async execute(newUser: UserProps) {
    const hashedPassword = await this.passwordHasher.hashPassword(
      newUser.password,
    );

    const newUserEntity = AdminAuthUser.create({
      ...newUser,
      credentials: {
        password: hashedPassword,
        email: newUser.email,
      },
    });

    await this.authRepository.register(newUserEntity);
  }
}
