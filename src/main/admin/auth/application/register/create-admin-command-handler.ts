import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import CommandHandler from '../../../../shared/domain/bus/command/command-handler';
import CreateAdminCommand from './create-admin-command';
import RegisterAdminUseCase from './register-admin-use-case';

@service()
export default class CreateAdminCommandHandler implements CommandHandler {
  constructor(
    @inject('RegisterAdminUseCase')
    private userCreator: RegisterAdminUseCase,
  ) {}

  getCommandName(): string {
    return CreateAdminCommand.name;
  }

  async handle(command: CreateAdminCommand) {
    const id = new Id();

    try {
      await this.userCreator.execute({ id: id, ...command });
    } catch (e) {
      throw new Error('No fue posible registrar un usuario');
    }
  }
}
