import Command from '@shared/domain/bus/command/command';

export default class CreateAdminCommand extends Command {
  constructor(
    public readonly email: string,
    public readonly password: string,
    public readonly roleId: string,
  ) {
    super();
  }

  name(): string {
    return CreateAdminCommand.name;
  }
}
