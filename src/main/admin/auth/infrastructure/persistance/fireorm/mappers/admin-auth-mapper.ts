import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import { AdminEntity } from '@admin/auth/infrastructure/persistance/fireorm/entities/admin-entity';

export default class AdminAuthMapper {
  static toDomain(
    plainValues: AdminEntity,
    rolePermissions: any[],
  ): AdminAuthUser {
    return AdminAuthUser.fromPrimitives({
      id: plainValues.id,
      email: plainValues.email,
      password: plainValues.password,
      roleId: plainValues.roleId,
      firstName: plainValues.firstName,
      lastName: plainValues.lastName,
      rolePermissions: rolePermissions,
    });
  }

  static toPersistence(user: AdminAuthUser): AdminEntity {
    const primitives = user.toPrimitives();

    const entity = new AdminEntity();

    entity.create({
      id: primitives.id,
      email: primitives.email,
      firstName: primitives.firstName,
      lastName: primitives.lastName,
      password: user.password,
      roleId: primitives.roleId,
    });

    return entity;
  }
}
