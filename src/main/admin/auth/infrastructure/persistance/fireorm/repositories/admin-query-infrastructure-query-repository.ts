import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import AdminQueryRepository from '@admin/auth/domain/repositories/admin-query-repository';
import { inject } from '@shared/domain/decorators';

import AdminRoleCommandInfrastructureRepository from './admin-role-infrastructure-command-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { AdminEntity } from '@admin/auth/infrastructure/persistance/fireorm/entities/admin-entity';
import AdminAuthMapper from '@admin/auth/infrastructure/persistance/fireorm/mappers/admin-auth-mapper';

export default class AuthAdminInfrastructureQueryRepository
  extends FireOrmRepository<AdminEntity>
  implements AdminQueryRepository
{
  static readonly bindingKey = 'AdminQueryRepository';

  constructor(
    @inject('AdminRoleCommandRepository')
    private roleFinder: AdminRoleCommandInfrastructureRepository,
  ) {
    super();
  }

  getEntityClass() {
    return AdminEntity;
  }

  async findUser(email: string): Promise<AdminAuthUser> {
    const dto = await this.repository().whereEqualTo('email', email).findOne();
    if (!dto) return undefined;

    /* const role = await this.roleFinder.get(dto.roleId);

     if (!role) throw new Error('admin_role_doesnt_exists');
 */
    return AdminAuthMapper.toDomain(dto, []);
  }

  async findUserById(userId: string): Promise<AdminAuthUser> {
    const dto = await this.repository().whereEqualTo('id', userId).findOne();

    if (!dto) return null;

    /*  const role = await this.roleFinder.get(dto.roleId);
      if (!role) throw new Error('admin_role_doesnt_exists');*/

    return AdminAuthMapper.toDomain(dto, []);
  }
}
