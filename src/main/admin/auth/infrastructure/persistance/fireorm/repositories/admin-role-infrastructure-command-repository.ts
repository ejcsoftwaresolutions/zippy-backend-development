import { ObjectUtils } from '@shared/domain/utils';
import { AdminRoleEntity } from '../entities/admin-role-entity';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';

export default class AdminRoleCommandInfrastructureRepository extends FireOrmRepository<AdminRoleEntity> {
  static readonly bindingKey = 'AdminRoleCommandRepository';

  getEntityClass() {
    return AdminRoleEntity;
  }

  async getAll(filters?: any) {
    return this.repository().find();
  }

  async create(data: {
    id: string;
    name: string;
    permissions: any[];
    isSuperAdmin?: boolean;
  }) {
    const entity = new AdminRoleEntity();
    entity.create({
      ...ObjectUtils.omit(data, ['id']),
      id: data.id,
    });
    await this.repository().create(entity);
  }

  count(filters?: any) {
    return 0;
  }

  async updateRole(
    id: string,
    data: Partial<{ name: string; permissions: any[]; isSuperAdmin?: boolean }>,
  ) {
    const entity = new AdminRoleEntity();
    entity.create({
      ...ObjectUtils.omit(data, ['id']),
      id: id,
    });

    await this.repository().update(entity);
  }

  async get(id: string) {
    const dto = await this.repository().whereEqualTo('id', id).findOne();

    return dto;
  }

  async delete(id: string) {
    return await this.repository().delete(id);
  }
}
