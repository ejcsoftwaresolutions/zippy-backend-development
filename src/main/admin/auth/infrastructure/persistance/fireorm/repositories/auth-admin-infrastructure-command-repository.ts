import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import AuthAdminCommandRepository from '@admin/auth/domain/repositories/auth-admin-command-repository';

import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { AdminEntity } from '@admin/auth/infrastructure/persistance/fireorm/entities/admin-entity';
import AdminAuthMapper from '@admin/auth/infrastructure/persistance/fireorm/mappers/admin-auth-mapper';
import { inject } from '@shared/domain/decorators';
import AdminRoleCommandInfrastructureRepository from '@admin/auth/infrastructure/persistance/fireorm/repositories/admin-role-infrastructure-command-repository';

export default class AuthAdminInfrastructureCommandRepository
  extends FireOrmRepository<AdminEntity>
  implements AuthAdminCommandRepository
{
  static readonly bindingKey = 'AuthAdminCommandRepository';

  constructor(
    @inject('AdminRoleCommandRepository')
    private roleFinder: AdminRoleCommandInfrastructureRepository,
  ) {
    super();
  }

  getEntityClass() {
    return AdminEntity;
  }

  async register(user: AdminAuthUser): Promise<void> {
    const entity = AdminAuthMapper.toPersistence(user);
    await this.repository().create(entity);
  }

  async exists(email: string): Promise<boolean> {
    const dto = await this.repository().whereEqualTo('email', email).findOne();

    return !!dto;
  }

  async updateUser(user: AdminAuthUser): Promise<void> {
    const entity = AdminAuthMapper.toPersistence(user);

    await this.repository().update(entity);
  }

  async changePassword(user: AdminAuthUser): Promise<void> {
    const entity = AdminAuthMapper.toPersistence(user);

    await this.repository().update(entity);
  }

  getAll(filters?: any) {
    return this.repository().find();
  }

  async count(filters?: any) {
    return (await this.repository().find()).length;
  }

  async get(id: string) {
    const dto = await this.repository().whereEqualTo('id', id).findOne();

    if (!dto) return undefined;
    /*  const role = await this.roleFinder.get(dto.roleId);
     */
    return AdminAuthMapper.toDomain(dto, []);
  }

  async delete(id: string) {
    return await this.repository().delete(id);
  }
}
