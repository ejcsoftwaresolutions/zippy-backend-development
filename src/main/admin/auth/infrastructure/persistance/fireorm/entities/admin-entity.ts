import { ObjectUtils } from '@shared/domain/utils';
import { Collection } from 'fireorm';

@Collection('admin')
export class AdminEntity {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  roleId: string;

  create(props: any) {
    Object.assign(this, ObjectUtils.omitUnknown(props));
  }
}
