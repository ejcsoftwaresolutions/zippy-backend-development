import { ObjectUtils } from '@shared/domain/utils';
import { Collection } from 'fireorm';

@Collection('admin_roles')
export class AdminRoleEntity {
  id: string;

  name: string;

  permissions: string[];

  isSuperAdmin?: boolean;

  create(props: any) {
    Object.assign(this, ObjectUtils.omitUnknown(props));
  }
}
