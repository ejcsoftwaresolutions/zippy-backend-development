import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import AdminAuthCredentials, {
  AdminAuthCredentialsProps,
} from './admin-auth-credentials';

export interface AdminAuthUserProps {
  id: Id;
  roleId: string;
  email: string;
  firstName?: string;
  lastName?: string;
  credentials: AdminAuthCredentialsProps;
  rolePermissions?: string[];
}

export interface AdminAuthUserPrimitiveProps {
  id: string;
  roleId: string;
  email: string;
  firstName?: string;
  lastName?: string;
  password: string;
  plainPassword?: string;
  rolePermissions?: string[];
}

export default class AdminAuthUser extends AggregateRoot<AdminAuthUserProps> {
  static create(props: AdminAuthUserProps) {
    const newUser = new AdminAuthUser(props);
    return newUser;
  }

  static fromPrimitives(plainData: AdminAuthUserPrimitiveProps): AdminAuthUser {
    const credentials = AdminAuthCredentials.fromPrimitives({
      email: plainData.email,
      password: plainData.password,
      plainPassword: plainData.plainPassword,
    });

    return new AdminAuthUser({
      id: new Id(plainData.id),
      roleId: plainData.roleId,
      email: plainData.email,
      firstName: plainData.firstName,
      lastName: plainData.lastName,
      credentials: credentials,
      rolePermissions: plainData.rolePermissions,
    });
  }

  get id() {
    return this.props.id;
  }

  get roleId() {
    return this.props.roleId;
  }

  get rolePermissions() {
    return this.props.rolePermissions;
  }

  get email() {
    return this.props.email;
  }

  get firstName() {
    return this.props.firstName;
  }

  get lastName() {
    return this.props.lastName;
  }

  get plainPassword(): string | undefined {
    return this.props.credentials.plainPassword;
  }

  get password(): string | undefined {
    return this.props.credentials.password;
  }

  public changePassword(newPassword: string) {
    this.props.credentials = AdminAuthCredentials.fromPrimitives({
      ...this.props.credentials,
      password: newPassword,
    });
  }

  toPrimitives(): AdminAuthUserPrimitiveProps {
    const json: any = super.toJson();
    return json;
  }
}
