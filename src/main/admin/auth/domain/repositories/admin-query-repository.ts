import AdminAuthUser from '../models/admin-auth-user';

export default interface AdminQueryRepository {
  findUser(email: string): Promise<AdminAuthUser | null>;
  findUserById(userId: string): Promise<AdminAuthUser | null>;
}
