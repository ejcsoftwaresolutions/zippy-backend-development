import service from '@shared/domain/decorators/service';
import firebase from 'firebase-admin';
import RiderExport from '@admin/riders/domain/models/rider-export';
import {inject} from '@shared/domain/decorators';
import PdfGenerator from '@shared/domain/pdf/pdf-generator';
import ExcelGenerator from '@shared/domain/excel/excel-generator';
import ExcelRequest from '@shared/domain/excel/excel-request';
import {DateTimeUtils} from '@shared/domain/utils';
import Id from "@shared/domain/id/id";

const path = require('path');
const fs = require('fs');
const ejs = require('ejs');


@service()
export default class ExportRiderListRequestUseCase {
    constructor(
        @inject('pdf.generator')
        private pdfGenerator: PdfGenerator,
        @inject('excel.generator')
        private excelGenerator: ExcelGenerator,
    ) {
    }

    async execute(requestData: {
        format: string;
        email: string;
        filters?: any;
        limit?: number;
        skip?: number;
        order?: any;
    }) {

        const data = await this.getData();

        let fileUrl: string | undefined;

        if (requestData.format === "PDF") {
            await this.requestPdf({
                items: data,
                email: requestData.email,
                title: 'Lista de Zippers',
            })
            return
        }

        try {
            fileUrl = await this.exportExcel(data);
            console.log(fileUrl);

            await this.sendEmail({
                email: requestData.email,
                title: 'Lista de Zippers',
                url: fileUrl,
            });
        } catch (e) {
            console.log(e);
        }

    }

    private async getData(
        filters?: any,
        limit?: number,
        skip?: number,
        order?: any,
    ): Promise<RiderExport[]> {
        const items = await firebase
        .firestore()
        .collection('users')
        .where('role', '==', 'driver');

        const data = await items.get();

        const fItems = data.docs.map((s) => s.data());

        return fItems.map((item: any) => {
            return RiderExport.fromPrimitives({
                id: item.id,
                email: item.email,
                firstName: item.firstName,
                lastName: item.lastName,
                identificationCard: item.identificationCard,
                phone: item.phone,
                createdAt: new Date((item.createdAt as any).seconds * 1000),
                deliveryMethod: item.deliveryMethod,
            });
        });
    }

    private async requestPdf({
        email,
        items,
        title,
    }: {
        items: RiderExport[],
        email: string;
        title: string
    }): Promise<void> {

        const pdfTemplatesFolder = path.resolve("public/pdf-templates")
        const pdfTemplate = fs.readFileSync(pdfTemplatesFolder + "/rider-list.ejs", "utf-8")

        const pdfContent = await this.parseTemplate(pdfTemplate, {
            riders: items
            .map((i) => i.toPrimitives())
            .map((i) => ({
                firstName: i.firstName,
                lastName: i.lastName,
                email: i.email,
                identificationCard: i.identificationCard,
                createdAt: DateTimeUtils.format(i.createdAt, 'DD/MM/YYYY'),
            })),
        })

        await firebase
        .firestore()
        .collection('admin_pdf')
        .add({
            email: email,
            slug: "RIDER_LIST",
            content: pdfContent,
            title: title,
            status: "REQUESTED",
            fileName: `admin/pdf/riders-${new Id().value}.pdf`,
            pdfConfig: {}
        });

    }

    private async parseTemplate(
        template: string,
        variables: any,
    ): Promise<string> {
        return ejs.render(template, variables, {})
    }


    private async exportExcel(items: RiderExport[]): Promise<string> {
        const styles = {
            header: {
                font: {
                    color: {
                        rgb: '000',
                    },
                    sz: 12,
                    bold: true,
                    underline: false,
                },
            },
        };

        const result = await this.excelGenerator.generate(
            new ExcelRequest({
                uploadFolderUrl: 'admin/excel',
                data: items
                .map((i) => i.toPrimitives())
                .map((i) => ({
                    firstName: i.firstName,
                    lastName: i.lastName,
                    email: i.email,
                    identificationCard: i.identificationCard,
                    createdAt: DateTimeUtils.format(i.createdAt, 'DD/MM/YYYY'),
                })),
                specification: {
                    firstName: {
                        displayName: 'Nombre',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    lastName: {
                        displayName: 'Apellido',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    email: {
                        displayName: 'Email',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    identificationCard: {
                        displayName: 'Cédula',
                        width: 120,
                        headerStyle: styles.header,
                    },
                    createdAt: {
                        displayName: 'Fecha de registro',
                        width: 120,
                        headerStyle: styles.header,
                    },
                },
            }),
        );

        return result.url;
    }

    private async sendEmail({
        email,
        url,
        title,
    }: {
        email: string;
        url: string;
        title: string;
    }) {
        await firebase
        .firestore()
        .collection('mail')
        .add({
            to: email,
            message: {
                subject: `${title}`,
                html: `
                  <div>
                       <p>
                         Reporte generado, puedes descargar el archivo <a href="${url}">aquí</a> !.
                      </p>
                    <br/>
                  </div>
                `,
            },
        });
    }
}
