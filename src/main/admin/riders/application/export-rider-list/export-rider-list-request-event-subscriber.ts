import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import ExportRiderListRequestUseCase from './export-rider-list-request-use-case';
import ExportRiderListRequestedDomainEvent from '@admin/riders/domain/events/export-rider-list-requested-domain-event';

@service()
export default class ExportRiderListRequestEventSubscriber
  implements EventSubscriber
{
  constructor(
    @inject('ExportRiderListRequestUseCase')
    private useCase: ExportRiderListRequestUseCase,
  ) {}

  public subscribedTo(): Map<string, Function> {
    const events = new Map<string, Function>();
    events.set(
      ExportRiderListRequestedDomainEvent.eventName,
      this.handle.bind(this),
    );
    return events;
  }

  public async handle(event: ExportRiderListRequestedDomainEvent) {
    const eventData = event.props.eventData;

    await this.useCase.execute(eventData);
  }
}
