import DomainEvent, {
  DomainEventProps,
} from '@shared/domain/bus/event/domain-event';

const EVENT_NAME = 'EXPORT_RIDER_LIST_REQUESTED';

interface EventProps extends DomainEventProps {
  eventData: {
    format: string;
    email: string;
    filters?: any;
    limit?: number;
    skip?: number;
    order?: any;
  };
}

export default class ExportRiderListRequestedDomainEvent extends DomainEvent<EventProps> {
  public static eventName: string = EVENT_NAME;

  eventName(): string {
    return EVENT_NAME;
  }
}
