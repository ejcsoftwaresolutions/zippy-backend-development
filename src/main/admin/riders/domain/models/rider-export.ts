import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { ObjectUtils } from '@shared/domain/utils';

export interface RiderExportProps {
  id: string;
  email: string;
  rating?: number;
  firstName?: string;
  lastName?: string;
  phone?: string;
  profilePictureUrl?: string;
  deliveryMethod: string;
  createdAt?: Date;
  identificationCard?: string;
}

export type RiderExportPrimitiveProps = RiderExportProps;

export default class RiderExport extends AggregateRoot<RiderExportProps> {
  constructor(props: RiderExportProps) {
    super({
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get email() {
    return this.props.email;
  }

  get rating() {
    return this.props.rating;
  }

  get firstName() {
    return this.props.firstName;
  }

  get lastName() {
    return this.props.lastName;
  }

  get phone() {
    return this.props.phone;
  }

  get profilePictureUrl() {
    return this.props.profilePictureUrl;
  }

  get deliveryMethod() {
    return this.props.deliveryMethod;
  }

  get createdAt() {
    return this.props.createdAt;
  }

  get identificationCard() {
    return this.props.identificationCard;
  }

  static fromPrimitives({
    ...plainData
  }: RiderExportPrimitiveProps): RiderExport {
    return new RiderExport(plainData);
  }

  toPrimitives(): RiderExportPrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
