import { inject } from '@shared/domain/decorators';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import {
  FIREBASE_ADMIN_INJECT,
  FirebaseAdminSDK,
} from '@tfarras/nestjs-firebase-admin';
import { EntityConstructorOrPath } from 'fireorm';
import VendorProfile from 'src/main/vendors/authetication/domain/models/vendor-profile';
import VendorUser from 'src/main/vendors/authetication/domain/models/vendor-user';
import VendorCommandRepository from 'src/main/vendors/authetication/domain/repositories/vendor-command-repository';
import { VendorUserEntity } from '../entities/vendor-user-entity';
import { VendorAdminEntity } from '../entities/vendor-admin-entity';
import VendorAdminMapper from '../mappers/vendor-admin-mapper';
import VendorSignup from '../../../../domain/models/vendor-signup';
import Id from '@shared/domain/id/id';
import VendorUserMapper from '../mappers/vendor-user-mapper';
import VendorProfileMapper from '../mappers/vendor-profile-mapper';
import { VendorEntity } from '../entities/vendor-entity';
import { VendorWithdrawalDetailsEntity } from '../entities/vendor-withdrawal-details-entity';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import UserAccount from '@shared/domain/models/user-account';
import { UserRole } from '@shared/domain/models/user-role';

export default class VendorCommandInfrastructureRepository
  extends FireOrmRepository<VendorUserEntity>
  implements VendorCommandRepository
{
  static readonly bindingKey = 'VendorCommandRepository';

  constructor(
    @inject(FIREBASE_ADMIN_INJECT) private firebase: FirebaseAdminSDK,
    @inject('user.authentication.token.verificator')
    private tokenVerificator: AuthTokenVerificator<any>,
  ) {
    super();
  }

  getEntityClass(): EntityConstructorOrPath<VendorUserEntity> {
    return VendorUserEntity;
  }

  async register(user: VendorSignup): Promise<void> {
    const entity = VendorAdminMapper.toPersistence(user);

    await Promise.all([
      this.getEntityRepository(VendorAdminEntity).create(entity),
    ]);
  }

  async existsUserByEmail(email: string): Promise<boolean> {
    const data = await this.repository().whereEqualTo('email', email).findOne();

    return !!data;
  }

  async approve(publicProfile: VendorUser): Promise<void> {
    const account = VendorUserMapper.toPersistence(publicProfile);

    const profile = VendorProfile.fromPrimitives(
      publicProfile.toPrimitives().profile,
    );

    const withdrawalDetails = new VendorWithdrawalDetailsEntity();
    const signupVendor = await this.findVendorSignupByEmail(
      publicProfile.email,
    );

    if (!signupVendor) {
      throw new Error('SIGNUP_INFO_NOT_FOUND');
    }

    const signupPrimitives = signupVendor.toPrimitives();

    withdrawalDetails.create({
      id: publicProfile.id.value,
      type: 'bank',
      vendorId: publicProfile.id.value,
      details: {
        ...signupVendor.toPrimitives().withdrawalDetails,
      },
    });

    await Promise.all([
      ...(signupPrimitives.linkAccountToken
        ? [this.repository().update(account)]
        : [
            this.firebase.auth().createUser({
              email: publicProfile.email,
              password: publicProfile.plainPassword,
              uid: publicProfile.id.value,
            }),
            this.repository().create(account),
          ]),
      this.getEntityRepository(VendorEntity).create(
        VendorProfileMapper.toPersistence(profile),
      ),
      this.getEntityRepository(VendorWithdrawalDetailsEntity).create(
        withdrawalDetails,
      ),
    ] as Promise<any>[]);
  }

  async findVendor(id: Id): Promise<VendorUser> {
    const item = await this.repository().whereEqualTo('id', id.value).findOne();

    if (!item) return;

    const profile = await this.getEntityRepository(VendorEntity)
      .whereEqualTo('id', id.value)
      .findOne();

    return VendorUserMapper.toDomain({ account: item, profile: profile });
  }

  async findVendorSignup(id: Id): Promise<VendorSignup> {
    const item = await this.getEntityRepository(VendorAdminEntity)
      .whereEqualTo('id', id.value)
      .findOne();

    if (!item) return;

    return VendorAdminMapper.toDomain(item);
  }

  async findAccountByEmail(email: string): Promise<UserAccount> {
    const data = await this.repository().whereEqualTo('email', email).findOne();
    if (!data) return;

    return UserAccount.fromPrimitives({
      id: data.id,
      email: data.email,
      firstName: data.firstName,
      lastName: data.lastName,
      phone: data.phone,
      createdAt: data.createdAt,
      hasDefaultPassword: data.hasDefaultPassword,
      newPasswordToken: data.newPasswordToken,
      roles: data.roles as UserRole[],
      status: data.status,
    });
  }

  private async findVendorSignupByEmail(email: string): Promise<VendorSignup> {
    const item = await this.getEntityRepository(VendorAdminEntity)
      .whereEqualTo('email', email)
      .findOne();

    if (!item) return;

    return VendorAdminMapper.toDomain(item);
  }
}
