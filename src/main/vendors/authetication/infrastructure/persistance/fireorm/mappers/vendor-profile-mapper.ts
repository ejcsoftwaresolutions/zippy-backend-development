import { ObjectUtils } from '@shared/domain/utils';
import VendorProfile from 'src/main/vendors/authetication/domain/models/vendor-profile';
import { VendorEntity } from '../entities/vendor-entity';
import VendorDeliveryMethodMapper from './vendor-delivery-method-mapper';

export default class VendorProfileMapper {
  static toDomain(plainValues: VendorEntity): VendorProfile {
    return VendorProfile.fromPrimitives({
      id: plainValues.id,
      title: plainValues.title,
      isOpen: plainValues.isOpen,
      deliveryRadius: plainValues.deliveryRadius,
      rif: plainValues.rif,
      categoryId: plainValues.categoryID,
      reviews: {
        sum: plainValues.reviewsSum,
        count: plainValues.reviewsCount,
      },
      identificationCard: plainValues.identificationCard,
      location: {
        ...plainValues.location,
        ...ObjectUtils.omit(plainValues.address, 'formattedAddress'),
      },
      logoImage: plainValues.logoImage,
      schedule: plainValues.schedule,
      status: plainValues.status,
      description: plainValues.description,
      code: plainValues.code,
      deliveryMethod: VendorDeliveryMethodMapper.toDomain(plainValues),
      selectedSubcategories: plainValues.selectedSubcategories,
      linePhone: plainValues.linePhone,
      configurations: plainValues.configurations,
      salesCommissionPercentage: plainValues.salesCommissionPercentage,
      scheduledDeliveriesSchedule: plainValues.scheduledOrdersSchedule,
    });
  }

  static toPersistence(user: VendorProfile): VendorEntity {
    const primitives = user.toPrimitives();
    const entity = new VendorEntity();

    entity.create(
      ObjectUtils.omitUnknown({
        id: primitives.id,
        title: primitives.title,
        rif: primitives.rif,
        isOpen: primitives.isOpen,
        deliveryRadius: primitives.deliveryRadius ?? 0,
        reviewsSum: primitives.reviews.sum ?? 0,
        reviewsCount: primitives.reviews.count ?? 0,
        identificationCard: primitives.identificationCard,
        location: primitives.location
          ? {
              ...ObjectUtils.pick(primitives.location, [
                'latitude',
                'longitude',
              ]),
            }
          : undefined,
        address: {
          ...ObjectUtils.omit(primitives.location, [
            'latitude',
            'longitude',
            'address',
          ]),
        },
        schedule: primitives.schedule,
        categoryID: primitives.categoryId,
        description: primitives.description ?? '',
        code: primitives.code,
        status: primitives.status,
        ...VendorDeliveryMethodMapper.toPersistence(primitives.deliveryMethod),
        selectedSubcategories: primitives.selectedSubcategories,
        linePhone: primitives.linePhone,
        configurations: primitives.configurations,
        salesCommissionPercentage: primitives.salesCommissionPercentage,
        scheduledOrdersSchedule: primitives.scheduledDeliveriesSchedule,
      }),
    );

    return entity;
  }
}
