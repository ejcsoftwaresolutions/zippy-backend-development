import VendorUser from 'src/main/vendors/authetication/domain/models/vendor-user';
import { VendorEntity } from '../entities/vendor-entity';
import { VendorUserEntity } from '../entities/vendor-user-entity';
import VendorProfileMapper from './vendor-profile-mapper';
import UserAccount from '@shared/domain/models/user-account';
import { UserRole } from '@shared/domain/models/user-role';

export default class VendorUserMapper {
  static toDomain(plainValues: {
    account: VendorUserEntity;
    profile: VendorEntity;
  }): VendorUser {
    const { account, profile } = plainValues;

    return VendorUser.fromPrimitives({
      id: account.id,
      account: UserAccount.fromPrimitives({
        id: account.id,
        email: account.email,
        firstName: account.firstName,
        lastName: account.lastName,
        birthday: account.birthday,
        phone: account.phone,
        createdAt: account.createdAt,
        hasDefaultPassword: account.hasDefaultPassword,
        newPasswordToken: account.newPasswordToken,
        roles: account.roles as UserRole[],
        status: account.status,
      }).toPrimitives(),
      profile: VendorProfileMapper.toDomain(profile).toPrimitives(),
    });
  }

  static toPersistence(user: VendorUser): VendorUserEntity {
    const primitives = user.toPrimitives();

    const entity = new VendorUserEntity();

    entity.create({
      id: primitives.id,
      email: primitives.account.email,
      firstName: primitives.account.firstName,
      lastName: primitives.account.lastName,
      phone: primitives.account.phone,
      birthday: primitives.account.birthday,
      createdAt: primitives.account.createdAt,
      hasDefaultPassword: primitives.account.hasDefaultPassword,
      newPasswordToken: primitives.account.newPasswordToken,
      roles: primitives.account.roles,
      status: primitives.account.status,
    });

    return entity;
  }
}
