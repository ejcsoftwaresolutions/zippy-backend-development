import { ObjectUtils } from '@shared/domain/utils';
import VendorSignup from '../../../../domain/models/vendor-signup';
import { VendorAdminEntity } from '../entities/vendor-admin-entity';
import VendorDeliveryMethodMapper from './vendor-delivery-method-mapper';

export default class VendorAdminMapper {
  static toDomain(plainValues: VendorAdminEntity): VendorSignup {
    return VendorSignup.fromPrimitives({
      id: plainValues.id,
      code: plainValues.code,
      state: plainValues.state,
      basicInfo: {
        identificationCard: plainValues.identificationCard,
        email: plainValues.email,
        birthday: plainValues.birthday,
        firstName: plainValues.firstName,
        lastName: plainValues.lastName,
        phone: plainValues.phone,
        contactCompanyCharge: plainValues.contactCompanyCharge,
        linePhone: plainValues.linePhone,
      },
      deliveryMethod: VendorDeliveryMethodMapper.toDomain(plainValues),
      location: plainValues.location
        ? {
            ...plainValues.location,
            ...ObjectUtils.omit(plainValues.address, 'formattedAddress'),
          }
        : undefined,
      rif: plainValues.rif,
      social: {
        website: plainValues.social.website,
        instagram: plainValues.social.instagram,
      },
      categoryId: plainValues.categoryId,
      extra: plainValues.extra
        ? {
            productTypes: plainValues.extra.productTypes, //hasOwnDelivery: plainValues.extra.hasOwnDelivery,
          }
        : undefined,
      schedule: plainValues.schedule,
      shopName: plainValues.title,
      withdrawalDetails: plainValues.withdrawalDetails,
      documents: plainValues.documents,
      createdAt: plainValues.createdAt,
      selectedSubcategories: plainValues.selectedSubcategories,
      linkAccountToken: plainValues.linkAccountToken,
      salesCommissionPercentage: plainValues.salesCommissionPercentage,
    });
  }

  static toPersistence(user: VendorSignup): VendorAdminEntity {
    const primitives = user.toPrimitives();

    const entity = new VendorAdminEntity();

    entity.create(
      ObjectUtils.omitUnknown({
        id: primitives.id,
        email: primitives.basicInfo.email,
        firstName: primitives.basicInfo.firstName,
        lastName: primitives.basicInfo.lastName,
        birthday: primitives.basicInfo.birthday,
        title: primitives.shopName,
        contactCompanyCharge: primitives.basicInfo.contactCompanyCharge,
        phone: primitives.basicInfo.phone,
        linePhone: primitives.basicInfo.linePhone,
        identificationCard: primitives.basicInfo.identificationCard,
        deliveryMethod: primitives.deliveryMethod
          ? VendorDeliveryMethodMapper.toPersistenceFromApplication(
              primitives.deliveryMethod,
            )
          : undefined,
        location:
          primitives.location && primitives.location.latitude
            ? {
                ...ObjectUtils.pick(primitives.location, [
                  'latitude',
                  'longitude',
                ]),
              }
            : undefined,
        address: primitives.location
          ? {
              ...ObjectUtils.omit(primitives.location, [
                'latitude',
                'longitude',
                'address',
              ]),
            }
          : undefined,
        rif: primitives.rif,
        social: ObjectUtils.omitUnknown(primitives.social),
        extra: primitives.extra
          ? ObjectUtils.omitUnknown(primitives.extra)
          : undefined,
        schedule: primitives.schedule
          ? ObjectUtils.omitUnknown(primitives.schedule)
          : undefined,
        documents: primitives.documents
          ? ObjectUtils.omitUnknown(primitives.documents)
          : undefined,
        withdrawalDetails: primitives.withdrawalDetails
          ? ObjectUtils.omitUnknown(primitives.withdrawalDetails)
          : undefined,
        categoryId: primitives.categoryId,
        createdAt: primitives.createdAt,
        state: primitives.state,
        code: primitives.code,
        selectedSubcategories: primitives.selectedSubcategories,
        linkAccountToken: primitives.linkAccountToken,
        salesCommissionPercentage: primitives.salesCommissionPercentage,
      }),
    );

    return entity;
  }
}
