import { ArrayUtils } from '@shared/domain/utils';

export default class VendorDeliveryMethodMapper {
  static toDomain(dto: any): string[] {
    let methods: string[] = [];

    function toArray(item) {
      if (item == 'ALL') {
        return ['IMMEDIATELY', 'SCHEDULED', 'PICK_UP'];
      }
      return [item];
    }

    if (!!dto.deliveryMethod) {
      if (!ArrayUtils.isArray(dto.deliveryMethod)) {
        methods = toArray(dto.deliveryMethod);
        return methods;
      }
      methods = dto.deliveryMethod;
      return methods;
    }

    if (dto.immediateDelivery) {
      methods.push('IMMEDIATELY');
    }

    if (dto.scheduleDelivery) {
      methods.push('SCHEDULED');
    }

    if (dto.pickup) {
      methods.push('PICK_UP');
    }

    return methods;

    /*  if (dto.immediateDelivery &&
         dto.scheduleDelivery
         && dto.pickup
         ) return "ALL"

         if (dto.immediateDelivery &&
         !dto.scheduleDelivery
         && !dto.pickup
         ) return "IMMEDIATELY"

         if (!dto.immediateDelivery &&
         !dto.scheduleDelivery
         && dto.pickup
         ) return "PICK_UP"

         if (dto.immediateDelivery &&
         dto.scheduleDelivery
         && !dto.pickup
         ) return "SCHEDULED"*/
  }

  static toPersistence(types: string[]): any {
    /* switch (type) {
         case "ALL":
         return {
         immediateDelivery: true, scheduleDelivery: true, pickup: true
         }
         case "IMMEDIATELY":
         return {
         immediateDelivery: true, scheduleDelivery: false, pickup: false
         }
         case "PICK_UP":
         return {
         immediateDelivery: false, scheduleDelivery: false, pickup: true
         }
         case "SCHEDULED":
         return {
         immediateDelivery: false, scheduleDelivery: true, pickup: false
         }
         }*/

    return {
      immediateDelivery: types.includes('IMMEDIATELY'),
      scheduleDelivery: types.includes('SCHEDULED'),
      pickup: types.includes('PICK_UP'),
    };
  }

  static toPersistenceFromApplication(deliveryMethod: any): any {
    if (!ArrayUtils.isArray(deliveryMethod)) {
      return [deliveryMethod];
    }

    return deliveryMethod;
  }
}
