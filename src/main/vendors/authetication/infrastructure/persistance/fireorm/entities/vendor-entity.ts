import { ObjectUtils } from '@shared/domain/utils';
import { Collection } from 'fireorm';

@Collection('vendors')
export class VendorEntity {
  id: string;
  title: string;
  rif: string;
  identificationCard: string;
  categoryID: string;
  code: string;
  schedule: any;
  scheduledOrdersSchedule: any;
  location?: {
    latitude: number;
    longitude: number;
  };
  address: {
    country: string;
    city: string;
    state: string;
    line1: string;
    line2?: string;
  };
  reviewsSum: number;
  reviewsCount: number;
  isOpen: boolean;
  deliveryRadius: number;
  linePhone?: string;
  logoImage?: string;
  description: string;
  status: string;
  immediateDelivery: boolean;
  scheduleDelivery: boolean;
  pickup: boolean;
  selectedSubcategories: { id: string; name: string }[];
  configurations: {
    referralPromotions: boolean;
    receiveEmails: boolean;
    receivePushNotifications: boolean;
    receiveSMS: boolean;
  };
  salesCommissionPercentage: number;

  create(props: any) {
    Object.assign(this, ObjectUtils.omitUnknown(props));
  }
}
