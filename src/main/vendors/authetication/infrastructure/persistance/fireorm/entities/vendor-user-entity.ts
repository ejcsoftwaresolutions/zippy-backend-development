import {ObjectUtils} from '@shared/domain/utils';
import {Collection} from 'fireorm';

@Collection('users')
export class VendorUserEntity {
    id: string;
    firstName?: string;
    lastName?: string;
    birthday?: Date;
    email: string;
    phone?: string;
    createdAt: Date;
    status: string;
    hasDefaultPassword: boolean;
    newPasswordToken?: string;
    roles: string[];

    create(props: any) {
        Object.assign(this, ObjectUtils.omitUnknown(props));
    }
}
