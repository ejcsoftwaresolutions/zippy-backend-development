import { Collection } from 'fireorm';
import { ObjectUtils } from '@shared/domain/utils';

@Collection('vendor_withdrawal_details')
export class VendorWithdrawalDetailsEntity {
  id: string;
  vendorId: string;
  type: string;
  details: any;

  create(props: any) {
    Object.assign(this, ObjectUtils.omitUnknown(props));
  }
}
