import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { ObjectUtils } from '@shared/domain/utils';

export type VendorSignupLocation = {
  line1: string;
  line2?: string;
  city: string;
  state: string;
  country: string;
  address: string;
  latitude?: number;
  longitude?: number;
};

export interface VendorSignupProps {
  id: Id;
  shopName: string;
  code: string;
  location?: VendorSignupLocation;
  rif?: string;
  basicInfo: {
    identificationCard?: string;
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    birthday?: Date;
    linePhone?: string;
    contactCompanyCharge?: string;
  };
  social: {
    website?: string;
    instagram: string;
  };
  extra?: {
    productTypes: string; //hasOwnDelivery: boolean;
  };
  schedule?: any;
  deliveryMethod?: string[];
  documents?: {
    rifUrl: string;
    dniUrl: string;
    productCatalogUrl: string;
    LIAELicenseUrl: string;
    permitMedicalUrl: string;
    foodManipulationUrl: string;
  };
  withdrawalDetails?: {
    accountType: string;
    bank: string;
    accountNumber: string;
  };
  categoryId?: string;
  selectedSubcategories: { id: string; name: string }[];
  createdAt: Date;
  state: string;
  linkAccountToken?: string;
  salesCommissionPercentage: number;
}

export interface VendorSignupPrimitiveProps
  extends Omit<VendorSignupProps, 'id'> {
  id: string;
}

const DefaultProps = {};

export default class VendorSignup extends AggregateRoot<VendorSignupProps> {
  constructor(props: VendorSignupProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get code() {
    return this.props.code;
  }

  get rif() {
    return this.props.rif;
  }

  get salesCommissionPercentage() {
    return this.props.salesCommissionPercentage;
  }

  get linkAccountToken() {
    return this.props.linkAccountToken;
  }

  get identificationCard() {
    return this.props.basicInfo.identificationCard;
  }

  get selectedSubcategories() {
    return this.props.selectedSubcategories;
  }

  get birthday() {
    return this.props.basicInfo.birthday;
  }

  get email() {
    return this.props.basicInfo.email;
  }

  get deliveryMethod() {
    return this.props.deliveryMethod;
  }

  get categoryId() {
    return this.props.categoryId;
  }

  get firstName() {
    return this.props.basicInfo.firstName;
  }

  get lastName() {
    return this.props.basicInfo.lastName;
  }

  get schedule() {
    return this.props.schedule;
  }

  get phone() {
    return this.props.basicInfo.phone;
  }

  get linePhone() {
    return this.props.basicInfo.linePhone;
  }

  get shopName() {
    return this.props.shopName;
  }

  get createdAt() {
    return this.props.createdAt;
  }

  get location() {
    return this.props.location;
  }

  static create(data: VendorSignupProps): VendorSignup {
    return new VendorSignup(data);
  }

  static fromPrimitives(plainData: VendorSignupPrimitiveProps): VendorSignup {
    return new VendorSignup({
      ...plainData,
      deliveryMethod: plainData.deliveryMethod,
      id: new Id(plainData.id),
    });
  }

  update(updates: Partial<VendorSignupProps>) {
    Object.assign(this, {
      ...this.props,
      ...updates,
      deliveryMethod: updates.deliveryMethod
        ? updates.deliveryMethod
        : this.props.deliveryMethod,
    });
  }

  toPrimitives(): VendorSignupPrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown({
      ...json,
      deliveryMethod: this.props.deliveryMethod,
    });
  }
}
