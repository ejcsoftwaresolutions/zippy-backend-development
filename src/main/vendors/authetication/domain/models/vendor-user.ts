import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { ObjectUtils } from '@shared/domain/utils';
import UserAccount, {
  UserAccountPrimitiveProps,
} from '@shared/domain/models/user-account';
import VendorProfile, { VendorProfilePrimitiveProps } from './vendor-profile';

export interface VendorUserProps {
  id: Id;
  account: UserAccount;
  profile: VendorProfile;
}

export interface VendorUserPrimitiveProps {
  id: string;
  account: UserAccountPrimitiveProps;
  profile: VendorProfilePrimitiveProps;
}

const DefaultProps = {};

export default class VendorUser extends AggregateRoot<VendorUserProps> {
  constructor(props: VendorUserProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get email() {
    return this.props.account.email;
  }

  get firstName() {
    return this.props.account.firstName;
  }

  get lastName() {
    return this.props.account.lastName;
  }

  get phone() {
    return this.props.account.phone;
  }

  get plainPassword() {
    return this.props.account.plainPassword;
  }

  get createdAt() {
    return this.props.account.createdAt;
  }

  static create(data: VendorUserProps): VendorUser {
    return new VendorUser(data);
  }

  static fromPrimitives({
    ...plainData
  }: VendorUserPrimitiveProps): VendorUser {
    return new VendorUser({
      ...plainData,
      id: new Id(plainData.id),
      profile: VendorProfile.fromPrimitives(plainData.profile),
      account: UserAccount.fromPrimitives(plainData.account),
    });
  }

  update(updates: Partial<VendorUserProps>) {
    Object.assign(this, {
      ...this.props,
      ...updates,
    });
  }

  toPrimitives(): VendorUserPrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown(json);
  }
}
