import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { ObjectUtils } from '@shared/domain/utils';

export interface VendorProfileProps {
  id: Id;
  title: string;
  code: string;
  logoImage?: string;
  isOpen: boolean;
  deliveryRadius?: number;
  rif: string;
  identificationCard: string;
  categoryId: string;
  deliveryMethod: string[];
  schedule: any;
  scheduledDeliveriesSchedule?: any;
  location: {
    line1: string;
    line2?: string;
    city: string;
    state: string;
    country: string;
    address: string;
    latitude?: number;
    longitude?: number;
  };
  reviews: {
    count: number;
    sum: number;
  };
  description: string;
  status: string;
  selectedSubcategories: { id: string; name: string }[];
  linePhone?: string;
  configurations: {
    referralPromotions: boolean;
    receiveEmails: boolean;
    receivePushNotifications: boolean;
    receiveSMS: boolean;
  };
  salesCommissionPercentage: number;
}

export interface VendorProfilePrimitiveProps
  extends Omit<VendorProfileProps, 'id'> {
  id: string;
}

const DefaultProps = {
  reviews: {
    count: 0,
    sum: 0,
  },
};

export default class VendorProfile extends AggregateRoot<VendorProfileProps> {
  constructor(props: VendorProfileProps) {
    super({
      ...DefaultProps,
      ...props,
    });
  }

  get id() {
    return this.props.id;
  }

  get deliveryMethod() {
    return this.props.deliveryMethod;
  }

  static create(data: VendorProfileProps): VendorProfile {
    return new VendorProfile(data);
  }

  static fromPrimitives({
    ...plainData
  }: VendorProfilePrimitiveProps): VendorProfile {
    return new VendorProfile({
      ...plainData,
      deliveryMethod: plainData.deliveryMethod,
      id: new Id(plainData.id),
    });
  }

  update(updates: Partial<VendorProfileProps>) {
    Object.assign(this.props, {
      ...this.props,
      ...updates,
      deliveryMethod: updates.deliveryMethod
        ? updates.deliveryMethod
        : this.props.deliveryMethod,
    });
  }

  toPrimitives(): VendorProfilePrimitiveProps {
    const json = super.toJson();

    return ObjectUtils.omitUnknown({
      ...json,
      deliveryMethod: this.props.deliveryMethod,
    });
  }
}
