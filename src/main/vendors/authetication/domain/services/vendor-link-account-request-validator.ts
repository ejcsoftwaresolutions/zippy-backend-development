import firebase from 'firebase-admin';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';

export default class VendorLinkAccountRequestValidator {
  constructor(private tokenVerificator: AuthTokenVerificator<any>) {}

  async validate(token: string) {
    /*change this to a repo*/
    const res = await firebase
      .firestore()
      .collection('link_account_requests')
      .where('token', '==', token)
      .get();

    if (res.empty) throw new Error('INVALID_LINK_TOKEN');
    const decodedToken = await this.tokenVerificator.decodeToken(token);

    if (decodedToken.newRole !== 'VENDOR') {
      throw new Error('INVALID_LINK_TOKEN');
    }

    return decodedToken;
  }
}
