import CodeGenerator from '@shared/domain/utils/code-generator';

export default class VendorCodeGenerator {
  constructor(private codeGenerator: CodeGenerator) {
  }

  generate(params: { title: string }): string {
    const firstNamePart = params.title.substring(0, 3).toUpperCase();

    const randomNumbers = this.codeGenerator.generate(7);

    return `${firstNamePart}${randomNumbers}`;
  }
}
