import EventBus from '@shared/domain/bus/event/event-bus';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import VendorProfile from '../../domain/models/vendor-profile';
import VendorUser from '../../domain/models/vendor-user';
import VendorCommandRepository from '../../domain/repositories/vendor-command-repository';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import UserAccount from '@shared/domain/models/user-account';

@service()
export default class ApproveVendorUseCase {
  constructor(
    @inject('event.bus')
    private eventBus: EventBus,
    @inject('VendorCommandRepository')
    private repo: VendorCommandRepository,
    @inject('user.authentication.token.creator')
    private tokenCreator: AuthTokenCreator<any>,
  ) {}

  async execute(data: {
    id: Id;
    account: UserAccount;
    profile: VendorProfile;
  }) {
    const user = VendorUser.create({
      id: data.id,
      account: data.account,
      profile: data.profile,
    });

    await this.repo.approve(user);

    this.eventBus.publish(user.pullDomainEvents());
  }
}
