import Command from '@shared/domain/bus/command/command';

interface RegisterInfo {
  id: string;
}

export default class ApproveVendorCommand extends Command {
  constructor(public readonly approveInfo: RegisterInfo) {
    super();
  }

  name(): string {
    return ApproveVendorCommand.name;
  }
}
