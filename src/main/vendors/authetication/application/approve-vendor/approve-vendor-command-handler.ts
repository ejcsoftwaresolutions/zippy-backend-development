import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import VendorCommandRepository from '../../domain/repositories/vendor-command-repository';
import ApproveVendorCommand from './approve-vendor-command';
import ApproveVendorUseCase from './approve-vendor-use-case';
import VendorProfile from '../../domain/models/vendor-profile';
import UserAccount from '@shared/domain/models/user-account';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import VendorLinkAccountRequestValidator from '../../domain/services/vendor-link-account-request-validator';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import CodeGenerator from '@shared/domain/utils/code-generator';
import VendorCodeGenerator from '../../domain/services/vendor-code-generator';

@service()
export default class ApproveVendorCommandHandler implements CommandHandler {
  private linkAccountValidator: VendorLinkAccountRequestValidator;
  private uniqueCodeGenerator: VendorCodeGenerator;

  constructor(
    @inject('ApproveVendorUseCase')
    private useCase: ApproveVendorUseCase,
    @inject('code.generator')
    codeGenerator: CodeGenerator,
    @inject('VendorCommandRepository')
    private repository: VendorCommandRepository,
    @inject('user.authentication.token.creator')
    private tokenCreator: AuthTokenCreator<any>,
    @inject('user.authentication.token.verificator')
    private tokenVerificator: AuthTokenVerificator<any>,
  ) {
    this.uniqueCodeGenerator = new VendorCodeGenerator(codeGenerator);
    this.linkAccountValidator = new VendorLinkAccountRequestValidator(
      tokenVerificator,
    );
  }

  getCommandName(): string {
    return ApproveVendorCommand.name;
  }

  async handle(command: ApproveVendorCommand) {
    const { approveInfo } = command;
    const id = new Id(approveInfo.id);

    const signupData = await this.repository.findVendorSignup(id);

    const foundAccount = await this.repository.findAccountByEmail(
      signupData.email,
    );

    const code = this.uniqueCodeGenerator.generate({
      title: signupData.shopName,
    });

    const approvedAccount = await (async () => {
      if (foundAccount) {
        await this.linkAccountValidator.validate(signupData.linkAccountToken);

        foundAccount.addRole('VENDOR');

        return foundAccount;
      }

      const newPassToken = await this.tokenCreator.generate({
        id: id.value,
        userType: 'VENDOR',
      });

      return UserAccount.fromPrimitives({
        id: id.value,
        email: signupData.email,
        firstName: signupData.firstName,
        birthday: signupData.birthday,
        lastName: signupData.lastName,
        phone: signupData.phone,
        plainPassword: signupData.rif,
        roles: ['VENDOR'],
        hasDefaultPassword: true,
        createdAt: new Date(),
        newPasswordToken: newPassToken,
        status: 'ACTIVE',
      });
    })();

    const profile = VendorProfile.fromPrimitives({
      id: approvedAccount.id.value,
      identificationCard: signupData.identificationCard,
      rif: signupData.rif,
      categoryId: signupData.categoryId,
      title: signupData.shopName,
      logoImage: undefined,
      schedule: signupData.schedule,
      location: signupData.location,
      description: '',
      code: code,
      status: 'ACTIVE',
      reviews: {
        count: 0,
        sum: 0,
      },
      deliveryMethod: signupData.deliveryMethod,
      isOpen: false,
      deliveryRadius: 0,
      selectedSubcategories: signupData.selectedSubcategories,
      linePhone: signupData.linePhone,
      configurations: {
        referralPromotions: true,
        receivePushNotifications: true,
        receiveSMS: true,
        receiveEmails: true,
      },
      salesCommissionPercentage: signupData.salesCommissionPercentage,
      scheduledDeliveriesSchedule:
        signupData.deliveryMethod.includes('SCHEDULED') &&
        signupData.schedule &&
        signupData.schedule.MONDAY
          ? Object.values(signupData.schedule).reduce(
              (acc: any, current: any) => {
                return {
                  ...acc,
                  [current.day]: {
                    ...current,
                    slot: 60,
                  },
                };
              },
              {},
            )
          : undefined,
    });

    await this.useCase.execute({
      id: approvedAccount.id,
      account: approvedAccount,
      profile: profile,
    });

    return {
      id: approvedAccount.id.value,
    };
  }
}
