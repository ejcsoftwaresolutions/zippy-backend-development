import CommandHandler from '@shared/domain/bus/command/command-handler';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import RegisterVendorCommand from './register-vendor-command';
import RegisterVendorUseCase from './register-vendor-use-case';
import VendorDocumentsUploader from '../../domain/services/vendor-documents-uploader';
import FileUploader from '@shared/domain/storage/storage-uploader';
import CodeGenerator from '@shared/domain/utils/code-generator';
import VendorCodeGenerator from '../../domain/services/vendor-code-generator';

@service()
export default class RegisterVendorCommandHandler implements CommandHandler {
  private documentsUploader: VendorDocumentsUploader;
  private vendorCodeGenerator: VendorCodeGenerator;

  constructor(
    @inject('RegisterVendorUseCase')
    private useCase: RegisterVendorUseCase,
    @inject('code.generator')
    codeGenerator: CodeGenerator,
    @inject('file.uploader')
    fileUploader: FileUploader,
  ) {
    this.documentsUploader = new VendorDocumentsUploader(fileUploader);
    this.vendorCodeGenerator = new VendorCodeGenerator(codeGenerator);
  }

  getCommandName(): string {
    return RegisterVendorCommand.name;
  }

  async handle(command: RegisterVendorCommand) {
    const id = new Id(command.registerInfo.id); // new Id();

    const { registerInfo } = command;

    /* const documentUrls = ObjectUtils.omitBy(
                   {
                     ...command.registerInfo.documents,
                   },
                   (value) => {
                     return !value;
                   },
                 );*/

    const code = this.vendorCodeGenerator.generate({
      title: registerInfo.shopName,
    });

    await this.useCase.execute({
      id: id,
      code: code,
      basicInfo: registerInfo.basicInfo,
      /* withdrawalDetails: registerInfo.withdrawalDetails,
                   documents: {
                     dniUrl: documentUrls.dniUrl,
                     rifUrl: documentUrls.rifUrl,
                     LIAELicenseUrl: documentUrls.LIAELicenseUrl,
                     productCatalogUrl: documentUrls.productCatalogUrl,
                     foodManipulationUrl: documentUrls.foodManipulationUrl,
                     permitMedicalUrl: documentUrls.permitMedicalUrl,
                   },
                   extra: registerInfo.extra,*/
      social: registerInfo.social,
      /*  schedule: registerInfo.schedule,*/
      shopName: registerInfo.shopName,
      /* rif: registerInfo.rif,
             location: registerInfo.location,
             categoryId: registerInfo.categoryId,
             deliveryMethod: registerInfo.deliveryMethod,*/
      linkAccountToken: registerInfo.linkAccountToken,
    });

    return {
      id: id.value,
    };
  }
}
