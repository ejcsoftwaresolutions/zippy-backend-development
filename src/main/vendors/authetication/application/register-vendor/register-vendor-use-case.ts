import EventBus from '@shared/domain/bus/event/event-bus';
import { inject } from '@shared/domain/decorators';
import service from '@shared/domain/decorators/service';
import Id from '@shared/domain/id/id';
import VendorCommandRepository from '../../domain/repositories/vendor-command-repository';
import VendorSignup from '../../domain/models/vendor-signup';

@service()
export default class RegisterVendorUseCase {
  constructor(
    @inject('event.bus') private eventBus: EventBus,
    @inject('VendorCommandRepository')
    private vendorRepository: VendorCommandRepository,
  ) {}

  async execute(data: {
    id: Id;
    code: string;
    shopName: string;
    /*  location?: VendorSignupLocation;
                                  rif: string;*/
    basicInfo: {
      email: string;
      firstName: string;
      lastName: string;
      phone: string;
      birthday?: Date;
      linePhone?: string;
      identificationCard?: string;
      contactCompanyCharge?: string;
    };
    social: {
      website?: string;
      instagram: string;
    };
    /* extra: {
                                   productTypes: string; //hasOwnDelivery: boolean;
                                 };
                                 schedule: {
                                   range: string;
                                   startHour: string;
                                   endHour: string;
                                   comment?: string;
                                 };
                                 documents: {
                                   dniUrl: string;
                                   rifUrl: string;
                                   productCatalogUrl: string;
                                   LIAELicenseUrl: string;
                                   permitMedicalUrl: string;
                                   foodManipulationUrl: string;
                                 };
                                 withdrawalDetails: {
                                   accountType: string;
                                   bank: string;
                                   accountNumber: string;
                                 };
                                 categoryId: string;
                                 deliveryMethod: string[];*/
    linkAccountToken?: string;
  }) {
    const foundUser = await this.vendorRepository.existsUserByEmail(
      data.basicInfo.email,
    );

    if (!data.linkAccountToken && foundUser) {
      throw new Error('user_already_exists');
    }

    const vendor = VendorSignup.create({
      id: data.id,
      basicInfo: data.basicInfo,
      /* rif: data.rif,*/
      social: data.social,
      /*  extra: data.extra,
                                      withdrawalDetails: data.withdrawalDetails,
                                      documents: data.documents,*/
      createdAt: new Date(),
      /* schedule: data.schedule,*/
      shopName: data.shopName,
      /*      categoryId: data.categoryId,*/
      state: 'NEW',
      /*  location: data.location,*/
      code: data.code,
      /*      deliveryMethod: data.deliveryMethod,*/
      selectedSubcategories: [],
      linkAccountToken: data.linkAccountToken,
      salesCommissionPercentage: 0,
    });

    await this.vendorRepository.register(vendor);

    this.eventBus.publish(vendor.pullDomainEvents());
  }
}
