import Command from '@shared/domain/bus/command/command';

interface RegisterInfo {
  id: string;
  shopName: string;
  /*location: {
      line1: string;
      line2?: string;
      address: string;
      city: string;
      state: string;
      country: string;
      latitude?: number;
      longitude?: number;
    };*/
  /* rif: string;*/
  basicInfo: {
    identificationCard: string;
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    birthday?: Date;
    linePhone?: string;
    contactCompanyCharge: string;
  };
  social: {
    website?: string;
    instagram: string;
  };
  /*extra: {
        productTypes: string; //hasOwnDelivery: boolean;
      };
      schedule: {
        range: string;
        startHour: string;
        endHour: string;
        comment?: string;
      };
      documents: {
        dniUrl: string;
        rifUrl: string;
        productCatalogUrl: string;
        LIAELicenseUrl: string;
        permitMedicalUrl: string;
        foodManipulationUrl: string;
      };
      withdrawalDetails: {
        accountType: string;
        bank: string;
        accountNumber: string;
      };
      categoryId: string;
      deliveryMethod: string[];*/
  linkAccountToken?: string;
}

export default class RegisterVendorCommand extends Command {
  constructor(public readonly registerInfo: RegisterInfo) {
    super();
  }

  name(): string {
    return RegisterVendorCommand.name;
  }
}
