import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import {ObjectUtils} from '@shared/domain/utils';
import {UserRole} from '@shared/domain/models/user-role';

export interface UserAccountProps {
    id: Id;
    email: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    createdAt?: Date;
    birthday?: Date,
    plainPassword?: string;
    hasDefaultPassword: boolean;
    newPasswordToken?: string;
    roles: UserRole[];
    status: string;
}

export interface UserAccountPrimitiveProps
    extends Omit<UserAccountProps, 'id'> {
    id: string;
}

const DefaultProps = {};

export default class UserAccount extends AggregateRoot<UserAccountProps> {
    constructor(props: UserAccountProps) {
        super({
            ...DefaultProps,
            ...props,
        });
    }

    get id() {
        return this.props.id;
    }

    get email() {
        return this.props.email;
    }

    get firstName() {
        return this.props.firstName;
    }

    get status() {
        return this.props.status;
    }

    get lastName() {
        return this.props.lastName;
    }

    get roles() {
        return this.props.roles;
    }

    get phone() {
        return this.props.phone;
    }

    get plainPassword() {
        return this.props.plainPassword;
    }

    get createdAt() {
        return this.props.createdAt;
    }

    static create(data: UserAccountProps): UserAccount {
        return new UserAccount(data);
    }

    static fromPrimitives({
        ...plainData
    }: UserAccountPrimitiveProps): UserAccount {
        return new UserAccount({
            ...plainData,
            id: new Id(plainData.id),
        });
    }

    addRole(newRole: UserRole) {
        this.props.roles.push(newRole);
    }

    update(updates: Partial<UserAccountProps>) {
        Object.assign(this, {
            ...this.props,
            ...updates,
        });
    }

    toPrimitives(): UserAccountPrimitiveProps {
        const json = super.toJson();

        return ObjectUtils.omitUnknown({...json, roles: this.props.roles});
    }
}
