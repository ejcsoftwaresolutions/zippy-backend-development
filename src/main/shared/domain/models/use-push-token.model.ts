import Id from '../id/id';
import ValueObject from '../value-object/value-object';

/* class constructor properties */
export interface UserPushTokenProps {
  token: string;
  userId: Id;
}

/* class properties to represent plain data */
export interface UserPushTokenPrimitiveProps {
  token: string;
  userId: string;
}

export default class UserPushToken extends ValueObject<UserPushTokenProps> {
  get token() {
    return this.props.token;
  }

  get userId() {
    return this.props.userId;
  }

  static fromPrimitives(props: UserPushTokenPrimitiveProps) {
    return new UserPushToken({
      token: props.token,
      userId: new Id(props.userId),
    });
  }

  toPrimitives(): UserPushTokenPrimitiveProps {
    const json: any = super.toJson();
    return json;
  }
}
