import {
  bsAbbr,
  bsFormat,
  usdAbbr,
  usdFormat,
} from '@shared/domain/utils/currency';

interface CurrencyProps {
  name: string;
  abbr: string;
  format?: (value: number) => string;
}

export interface CurrencyPrimitiveProps {
  name: string;
  abbr: string;
  format?: (value: number) => string;
}

export default class Currency {
  constructor(private props: CurrencyProps) {}

  get name() {
    return this.props.name;
  }

  get abbr() {
    return this.props.abbr;
  }

  static fromPrimitives(props: CurrencyPrimitiveProps) {
    const curr = new Currency(props);

    if (curr.abbr === bsAbbr) {
      return new Currency({
        ...curr.props,
        format: bsFormat,
      });
    }

    if (curr.abbr === usdAbbr) {
      return new Currency({
        ...curr.props,
        format: usdFormat,
      });
    }

    return curr;
  }

  isEqual(otherCurrency: Currency) {
    return this.props.abbr == otherCurrency.abbr;
  }

  format(value: number) {
    return this.props.format
      ? this.props.format(value ? value : 0)
      : value.toString();
  }

  toPrimitives(): CurrencyPrimitiveProps {
    return {
      abbr: this.props.abbr,
      name: this.props.name,
      format: this.props.format,
    };
  }
}
