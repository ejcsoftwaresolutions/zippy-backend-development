import Currency from './currency';
import { bsAbbr, bsFormat } from '@shared/domain/utils/currency';

const BsCurrency = new Currency({
  abbr: bsAbbr,
  name: 'Bolivares Soberanos',
  format: bsFormat,
});

export default BsCurrency;
