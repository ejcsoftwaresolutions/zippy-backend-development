import Currency from './currency';
import { usdAbbr, usdFormat } from '@shared/domain/utils/currency';

const UsdCurrency = new Currency({
  abbr: usdAbbr,
  name: 'Dolares',
  format: usdFormat,
});

export default UsdCurrency;
