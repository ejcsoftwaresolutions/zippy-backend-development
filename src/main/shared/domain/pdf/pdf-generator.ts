import PDFRequest from '@shared/domain/pdf/pdf-request';

export default interface PdfGenerator<T = Object> {
  generate(pdf: PDFRequest): Promise<{ url: string }>;
}
