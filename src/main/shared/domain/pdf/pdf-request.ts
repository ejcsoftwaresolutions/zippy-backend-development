import ValueObject from '../value-object/value-object';

interface PDFRequestProps {
  template: string;
  variables?: any;
  uploadUrl: string;
  config?: any;
}

export default class PDFRequest extends ValueObject<PDFRequestProps> {
  constructor(props: PDFRequestProps) {
    super(props);
  }

  get config() {
    return this.props.config;
  }

  get template() {
    return this.props.template;
  }

  get variables() {
    return this.props.variables;
  }

  get uploadUrl() {
    return this.props.uploadUrl;
  }

  toPrimitives() {
    return this.props;
  }
}
