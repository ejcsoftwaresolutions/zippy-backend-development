import ValueObject from '../value-object/value-object';

interface ExcelRequestProps {
  heading?: any;
  uploadFolderUrl: string;
  specification: any;
  data: any;
}

export default class ExcelRequest extends ValueObject<ExcelRequestProps> {
  constructor(props: ExcelRequestProps) {
    super(props);
  }

  get data() {
    return this.props.data;
  }

  get specification() {
    return this.props.specification;
  }

  get uploadFolderUrl() {
    return this.props.uploadFolderUrl;
  }

  toPrimitives() {
    return this.props;
  }
}
