export namespace DriverEmailTemplates {
  export const welcome = 'drivers/welcome.ejs';
}

export namespace PassengerEmailTemplates {
  export const welcome = 'customers/welcome.ejs';
}
