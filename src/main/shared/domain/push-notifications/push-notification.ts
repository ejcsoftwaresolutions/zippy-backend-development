export type PushNotificationProps = {
  to: string;
  title?: string;
  content: string;
  appId: string;
  channelId?: string;
  metadata?: any;
  imageUrl?: string;
};

export default class PushNotification {
  private readonly to: string;
  private readonly title?: string;
  private readonly content: string;
  private readonly appId: string;
  private readonly channelId: string;
  private readonly metadata: any;
  private readonly platform: 'android' | 'ios';
  private readonly imageUrl?: string;

  constructor(notificationData: PushNotificationProps) {
    this.to = notificationData.to;
    this.title = notificationData.title;
    this.content = notificationData.content;
    this.appId = notificationData.appId;
    this.channelId = notificationData.channelId;
    this.metadata = notificationData.metadata;
    this.platform = notificationData.to.length > 64 ? 'android' : 'ios';
    this.imageUrl = notificationData.imageUrl;
  }

  get isAndroid() {
    return this.platform === 'android';
  }

  public toPrimitives() {
    return {
      to: this.to,
      title: this.title,
      content: this.content,
      appId: this.appId,
      channelId: this.channelId ?? this.appId.split('/')[1],
      metadata: this.metadata,
      platform: this.platform,
      imageUrl: this.imageUrl,
    };
  }
}
