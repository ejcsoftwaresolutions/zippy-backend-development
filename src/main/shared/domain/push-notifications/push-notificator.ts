import PushNotification from './push-notification';

export default interface PushNotificator {
  send(notification: PushNotification): Promise<any>;

  transformApnsToFcm(apnsToken: string, application: string): Promise<string>;
}
