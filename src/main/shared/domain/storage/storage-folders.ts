export namespace DriverStorageFolders {
  export const root = 'drivers';
  export const carDocuments = 'car-documents';
  export const documents = 'documents';
  export const profile = 'profile';
}

export namespace CustomerStorageFolders {
  export const root = 'customer';
  export const profile = 'profile';
}
