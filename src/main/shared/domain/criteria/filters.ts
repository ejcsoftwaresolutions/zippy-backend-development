/* export default class Filters extends Collection<Filter<any>> {
    public static fromValues(
        values: Array<FilterPrimitiveProps<any>>,
    ): Filters {
        return new Filters(
            values.map((f: FilterPrimitiveProps<any>) =>
                Filter.fromValues<any>(f),
            ),
        );
    }

    public add(filter: Filter<any>): Filters {
        return new Filters([...this.items, filter]);
    }

    public filters(): Array<Filter<any>> {
        return this.items;
    }

   
}
 */

import ValueObject from '../value-object/value-object';

export interface FiltersProps {
  filters: any;
}

export default class Filters extends ValueObject<FiltersProps> {
  public static fromValues(values: any): Filters {
    return new Filters({
      filters: values,
    });
  }

  get filters() {
    return this.props.filters;
  }

  toPrimitives() {
    return this.props.filters;
  }
}
