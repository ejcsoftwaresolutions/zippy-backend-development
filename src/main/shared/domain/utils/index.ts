import {
  differenceWith as fnDifferenceWith,
  filter as fnFilter,
  find as fnFind,
  findIndex as fnFindIndex,
  flattenDeep as fnFlatten,
  get as fnGet,
  isArray as fnIsArray,
  isNil,
  keys as fnKeys,
  map as fnMap,
  merge as fnMerge,
  omit as fnOmit,
  omitBy as fnOmitBy,
  orderBy as fnOrderBy,
  pick as fnPick,
  pickBy as fnPickBy,
  uniq as fnUniq,
  uniqBy as fnUniqBy,
  zipObject,
} from 'lodash';
import { AnyObject } from '../types';

const moment = require('moment');
export namespace ObjectUtils {
  export const omit = fnOmit;
  export const omitUnknown = (object: AnyObject) => {
    return fnOmitBy(object, isNil);
  };
  export const omitBy = fnOmitBy;
  export const pick = fnPick;
  export const pickBy = fnPickBy;
  export const keys = fnKeys;
  export const find = fnFind;
  export const zip = zipObject;
  export const get = fnGet;
  export const merge = fnMerge;
}

export namespace ArrayUtils {
  export const isArray = fnIsArray;
  export const filter = fnFilter;
  export const differenceWith = fnDifferenceWith;
  export const orderBy = fnOrderBy;
  export const uniq = fnUniq;
  export const uniqBy = fnUniqBy;
  export const map = fnMap;
  export const flatten = fnFlatten;
  export const findIndex = fnFindIndex;
  export const filterLike = (arr1: any[], fieldName: string, like: string) => {
    return arr1.filter((dto) => {
      const name = dto[fieldName] ?? '';
      const test = like;
      const reg1 = new RegExp('^' + test + '.*$', 'i');
      const reg2 = new RegExp('^.*' + test + '$', 'i');
      const regex3 = new RegExp([test].join('|'), 'i');

      const match = name.match(reg1);
      const match2 = name.match(reg2);
      const match3 = name.includes(test);
      const match4 = regex3.test(name);

      return !!match || !!match2 || !!match3 || !!match4;
    });
  };
}

export namespace DateTimeUtils {
  export const startOfWeek = (): Date => {
    return moment().startOf('week').toDate();
  };

  export const endOfWeek = (): Date => {
    return moment().endOf('week').toDate();
  };
  export const differenceInDays = (a: Date, b: Date): number => {
    return moment(a).diff(moment(b), 'days');
  };

  export const differenceInMinutes = (a: Date, b: Date): number => {
    return moment(a).diff(moment(b), 'minutes');
  };

  export const startOfDate = (date: Date) => {
    return moment(date).clone().startOf('day').toDate();
  };

  export const endOfDate = (date: Date) => {
    return moment(date).clone().endOf('day').toDate();
  };

  export const addMonths = (a: Date, months: number) => {
    return moment(a).clone().add(months, 'months').toDate();
  };

  export const addDays = (a: Date, days: number) => {
    return moment(a).clone().add(days, 'days').toDate();
  };

  export const format = (date: Date, format: string) => {
    return moment(date).format(format);
  };

  export const toString = (date: Date) => {
    return moment(date).toString();
  };
}
