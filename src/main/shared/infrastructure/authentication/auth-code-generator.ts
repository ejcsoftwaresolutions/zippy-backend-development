import CodeGenerator from '@shared/domain/utils/code-generator';

export default class AuthCodeGenerator implements CodeGenerator {
  generate(): string {
    return this.makeId(6);
  }

  private makeId(length: number) {
    let result = '';
    const characters = '0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
