import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { EntityManager, EntityTarget, getManager, Repository } from 'typeorm';

export default abstract class TypeOrmRepository<T> {
  protected entityManager: EntityManager;
  constructor() {
    //this.entityManager = getManager();
  }

  abstract getEntityClass(): EntityTarget<T>;

  protected async save(
    aggregateRoot: AggregateRoot<any>,
    dataMapper: any,
  ): Promise<void> {
    const entity = dataMapper.toPersistence(aggregateRoot);
    await getManager().save(entity);
  }

  protected async remove<T = any>(entity: AggregateRoot<T>): Promise<void> {
    await getManager().remove(entity);
  }

  protected repository(): Repository<T> {
    const name = this.getEntityClass();
    return getManager().getRepository(name);
  }
}
