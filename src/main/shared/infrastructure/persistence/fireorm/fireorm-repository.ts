import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import {
  BaseFirestoreRepository,
  EntityConstructorOrPath,
  getRepository,
  IEntity,
} from 'fireorm';

export default abstract class FireOrmRepository<T extends IEntity> {
  constructor() {}

  abstract getEntityClass(): EntityConstructorOrPath<T>;

  public getEntityRepository<M extends IEntity>(
    target: EntityConstructorOrPath<M>,
  ): BaseFirestoreRepository<M> {
    return getRepository(target);
  }

  protected async save(
    aggregateRoot: AggregateRoot<any>,
    dataMapper: any,
  ): Promise<void> {
    const entity = dataMapper.toPersistence(aggregateRoot);
    await this.repository().create(entity);
  }

  protected async createInBulk(
    aggregateRoots: AggregateRoot<any>[],
    dataMapper: any,
  ): Promise<void> {
    const entities = dataMapper.toPersistenceFromArray(aggregateRoots);
    const batch = this.repository().createBatch();
    entities.forEach((product) => {
      return batch.create(product);
    });
    await batch.commit();
  }

  protected async updateInBulk(
    aggregateRoots: AggregateRoot<any>[],
    dataMapper: any,
  ): Promise<void> {
    const entities = dataMapper.toPersistenceFromArray(aggregateRoots);
    const batch = this.repository().createBatch();
    entities.forEach((product) => {
      return batch.update(product);
    });
    await batch.commit();
  }

  protected async remove(entity: AggregateRoot<T>): Promise<void> {
    this.repository().delete(entity.getId());
  }

  protected async findById(id: string): Promise<T> {
    return await this.repository().findById(id);
  }

  protected async update(item: T): Promise<void> {
    await this.repository().update(item);
  }

  protected repository(): BaseFirestoreRepository<T> {
    const entityClass = this.getEntityClass();
    return getRepository(entityClass);
  }
}
