import { injectable } from '@shared/domain/decorators';
import TimeHumanizer, {
  HumanizerConfig,
} from '@shared/domain/utils/time-humanizer';

const humanizeDuration = require('humanize-duration');

@injectable()
export default class BasicTimeHumanizer implements TimeHumanizer {
  humanize(timeInMs: number, config: HumanizerConfig): string {
    return humanizeDuration(timeInMs, {
      language: config?.language ?? 'shortEs',
      units: ['h', 'm'],
      maxDecimalPoints: 2,
      languages: {
        shortEs: {
          y: () => 'a',
          mo: () => 'mo',
          w: () => 'semanas',
          d: () => 'd',
          h: () => 'h',
          m: () => 'min',
          s: () => 'seg',
          ms: () => 'ms',
        },
      },
    });
  }
}
