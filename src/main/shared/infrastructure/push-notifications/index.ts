import { ConfigService } from '@nestjs/config';
import service from '@shared/domain/decorators/service';
import PushNotification from '@shared/domain/push-notifications/push-notification';
import PushNotificator from '@shared/domain/push-notifications/push-notificator';
import FCMNotificationService from './fcm/fcm-notification-service';

@service()
export default class MobilePushNotificationService implements PushNotificator {
  private androidPushService: PushNotificator;

  constructor(private configService: ConfigService) {
    this.androidPushService = new FCMNotificationService(
      this.configService.get<string>('FCM_SERVER_KEY'),
      this.configService.get<string>('FB_PROJECT_ID'),
      this.configService.get<string>('GOOGLE_APPLICATION_CREDENTIALS'),
    );
  }

  async send(notification: PushNotification): Promise<any> {
    return this.androidPushService.send(notification);
  }

  async transformApnsToFcm(
    apnsToken: string,
    application: string,
  ): Promise<string> {
    return this.androidPushService.transformApnsToFcm(apnsToken, application);
  }
}
