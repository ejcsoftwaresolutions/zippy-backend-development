import PushNotification from '@shared/domain/push-notifications/push-notification';
import PushNotificator from '@shared/domain/push-notifications/push-notificator';
import { JWT } from 'google-auth-library';
import axios from 'axios';

const path = require('path');

export default class FCMNotificationService implements PushNotificator {
  constructor(
    private serverKey: string,
    private projectId: string,
    private serviceAccountLocation: string,
  ) {}

  async send(notification: PushNotification): Promise<any> {
    const url = `https://fcm.googleapis.com/v1/projects/${this.projectId}/messages:send`;

    const token = await this.getAccessToken();

    const notificationData = notification.toPrimitives();

    const imageUrl = notificationData.imageUrl;

    try {
      const result = await axios.post(
        url,
        {
          message: {
            token: notificationData.to,
            data: {
              experienceId: notificationData.appId,
              title: notificationData.title,
              message: notificationData.content,
              metadata: JSON.stringify(notificationData.metadata ?? {}),
            },
            notification: {
              title: notificationData.title,
              body: notificationData.content,
              ...(imageUrl
                ? {
                    image: imageUrl,
                  }
                : {}),
            },
            android: {
              priority: 'high',
              notification: {
                channel_id: notificationData.channelId,
              },
            },
            apns: {
              headers: {
                'apns-priority': '10',
              },
              payload: {
                aps: {
                  sound: 'notification.wav',
                  ...(imageUrl ? { 'mutable-content': 1 } : {}),
                },
                alert: {
                  title: notificationData.title,
                  body: notificationData.content,
                  sound: 'notification.wav',
                },
              },
              ...(imageUrl
                ? {
                    fcm_options: {
                      image: imageUrl,
                    },
                  }
                : {}),
            },
          },
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
          },
        },
      );

      console.log(result.data);
      return result.data;
    } catch (e) {
      console.log(e.response.data.error);

      return {
        error: e.response.data.error,
      };
    }
  }

  async transformApnsToFcm(
    apnsToken: string,
    application: string,
  ): Promise<string> {
    const url = 'https://iid.googleapis.com/iid/v1:batchImport';
    const result = await axios.post(
      url,
      {
        application: application,
        sandbox: false,
        apns_tokens: [apnsToken],
      },
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.serverKey,
        },
      },
    );

    return result.data?.results[0]?.registration_token;
  }

  private async getAccessToken() {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const key = require(path.join(
        __dirname,
        '../../../../../../../' + this.serviceAccountLocation,
      ));
      const jwtClient = new JWT(
        key.client_email,
        null,
        key.private_key,
        ['https://www.googleapis.com/auth/firebase.messaging'],
        null,
      );
      jwtClient.authorize(function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }

        resolve(tokens.access_token);
      });
    });
  }
}
