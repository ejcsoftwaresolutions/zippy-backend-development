import { ConfigService } from '@nestjs/config';
import service from '@shared/domain/decorators/service';
import { FileUploadResponse } from '@shared/domain/storage/file-upload-response';
import { UploadFile } from '@shared/domain/storage/storage-types';
import FileUploader from '@shared/domain/storage/storage-uploader';
import Collection from '@shared/domain/value-object/collection';
import { S3 } from 'aws-sdk';
import { ManagedUpload } from 'aws-sdk/clients/s3';
import { v4 as uuidv4 } from 'uuid';
const { Duplex } = require('stream');
const AWS = require('aws-sdk');
const FileType = require('file-type');

function bufferToStream(buffer: any) {
  const duplexStream = new Duplex();
  duplexStream.push(buffer);
  duplexStream.push(null);
  return duplexStream;
}

@service()
export default class S3Uploader implements FileUploader {
  private s3: S3;
  constructor(private configService: ConfigService) {
    this.s3 = new AWS.S3({
      accessKeyId: this.configService.get<string>('S3_ACCESS_KEY'),
      secretAccessKey: this.configService.get<string>('S3_SECRET_KEY'),
      bucket: this.configService.get<string>('S3_BUCKET'),
    });
  }

  async uploadFile(
    file: UploadFile,
    name = `${uuidv4()}`,
    path?: string,
    key?: string,
  ): Promise<FileUploadResponse> {
    const buckerName = this.configService.get<string>('S3_BUCKET');
    const buffer = Buffer.from(file);
    const type = await FileType.fromBuffer(buffer);
    const extension = type.ext;
    const envFolder = process.env.NODE_ENV === 'production' ? '' : 'dev/';

    return new Promise((resolve, reject) => {
      // Setting up S3 upload parameters

      const f = bufferToStream(buffer);

      const params: S3.Types.PutObjectRequest = {
        Bucket: buckerName,
        Key: `${path ? envFolder + path + '/' : ''}${name}.${
          extension === 'xml' ? 'svg' : extension
        }`,
        Body: f,
        ContentType: extension === 'xml' ? 'image/svg+xml' : extension,
        ACL: 'public-read',
      };

      // Uploading files to the bucket
      this.s3.upload(
        params,
        function (err: Error, data: ManagedUpload.SendData) {
          if (err) {
            return reject(err);
          }

          resolve({
            key: key,
            url: data.Location,
            mimeType: extension,
          });
        },
      );
    });
  }

  async uploadFiles(
    files: Collection<{
      file: UploadFile;
      name?: string;
      fileKey?: string;
      filePath?: string;
    }>,
  ): Promise<Collection<FileUploadResponse>> {
    const filesToUpload = files.map(
      (f: {
        file: UploadFile;
        name?: string;
        fileKey?: string;
        filePath?: string;
      }) => {
        return this.uploadFile(f.file, f.name, f.filePath, f.fileKey);
      },
    );

    const response = await Promise.all(filesToUpload);

    return new Collection(response);
  }
}
