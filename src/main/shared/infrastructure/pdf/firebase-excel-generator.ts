import service from '@shared/domain/decorators/service';
import ExcelGenerator from '@shared/domain/excel/excel-generator';
import ExcelRequest from '@shared/domain/excel/excel-request';
import { inject } from '@shared/domain/decorators';
import FileUploader from '@shared/domain/storage/storage-uploader';

const excel = require('node-excel-export');

@service()
export default class FirebaseExcelGenerator implements ExcelGenerator {
  constructor(
    @inject('file.uploader')
    private fileUploader: FileUploader,
  ) {}

  async generate(excelRequest: ExcelRequest): Promise<{ url: string }> {
    const fileName = excelRequest.uploadFolderUrl;

    const excel = this.buildExcel(
      excelRequest.data,
      excelRequest.specification,
    );

    const response = await this.fileUploader.uploadFile(
      excel,
      undefined,
      fileName,
    );

    return {
      url: response.url,
    };
  }

  private buildExcel(data: any, specification: any, heading?: any): any {
    return excel.buildExport([
      {
        name: 'Report',
        heading: heading,
        specification: specification,
        data: data,
      },
    ]);
  }
}
