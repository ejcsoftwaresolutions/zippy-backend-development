import service from '@shared/domain/decorators/service';
import PdfGenerator from '@shared/domain/pdf/pdf-generator';
import PDFRequest from '@shared/domain/pdf/pdf-request';
import * as admin from 'firebase-admin';
import Id from '@shared/domain/id/id';
import {inject} from '@shared/domain/decorators';
import moment = require('moment');

const ejs = require('ejs');

const htmlToPDF = require('html-pdf');

@service()
export default class FirebaseHtmlPdfGenerator implements PdfGenerator {
    constructor(
        @inject('pdf.templates.root.folder')
        private pdfTemplatePath: string,
    ) {
    }

    async generate(pdf: PDFRequest): Promise<{ url: string }> {
        const envFolder = process.env.NODE_ENV === 'production' ? '' : 'dev/';

        const fileTemplate = pdf.template;

        const fileName = `${envFolder}${pdf.uploadUrl}`;

        const fileRef = admin.storage().bucket().file(fileName, {});

        const parsedContent = await this.parseTemplate(fileTemplate, pdf.variables);

        return new Promise((resolve, reject) => {
            htmlToPDF
            .create(parsedContent, {
                timeout: '100000',
                ...(pdf.config ?? {}),
            })
            .toStream(async (err: any, stream: any) => {
                if (err) {
                    console.log("Error al generar pdf")
                    console.log(err)
                    reject(err)
                    return;
                }

                await stream.pipe(
                    fileRef.createWriteStream({
                        metadata: {
                            contentType: 'application/pdf',
                            metadata: {
                                firebaseStorageDownloadTokens: new Id().value,
                                lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                            },
                        },
                        resumable: false,
                        public: true,
                    }),
                );

                resolve({
                    url: fileRef.publicUrl(),
                });
            });
        });
    }

    private async parseTemplate(
        template: string,
        variables: any,
    ): Promise<string> {
        return new Promise(async (resolve, reject) => {
            const string = await ejs.render(template, variables, {})
            resolve(string)

        });
    }
}
