import UserJWTCreator from '@apps/shared/infrastructure/authentication/user-jwt-token-creator';
import UserJWTVerificator from '@apps/shared/infrastructure/authentication/user-jwt-token-verificator';
import { Global, Module } from '@nestjs/common';
import { AuthModule } from './auth-module';

export const UserTokenCreator = {
  provide: 'user.authentication.token.creator',
  useClass: UserJWTCreator,
};

export const UserTokenVerificator = {
  provide: 'user.authentication.token.verificator',
  useClass: UserJWTVerificator,
};

@Global()
@Module({
  imports: [AuthModule],
  providers: [UserTokenCreator, UserTokenVerificator],
  exports: [UserTokenCreator, UserTokenVerificator],
})
export class AppAuthModule {
  constructor() {}
}
