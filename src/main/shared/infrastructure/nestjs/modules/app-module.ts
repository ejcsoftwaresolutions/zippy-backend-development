import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AppAuthModule } from './app-auth-module';
import { CQRSModule } from './cqrs-module';
import { FireOrmModule } from './fireorm-module';
import { MobileModule } from './mobile-module';
import { TokenModule } from './token-module';

const path = require('path');

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: path.join(__dirname, '../../../../../../../public'),
    }),
    ConfigModule.forRoot({
      envFilePath: process.env.NODE_ENV
        ? `.env.${process.env.NODE_ENV}`
        : '.env.development',
      isGlobal: true,
    }),

    FireOrmModule,
    TokenModule,
    AppAuthModule,
    MobileModule,
    CQRSModule,
    /*   StatusMonitorModule.forRoot(), */
  ],
  controllers: [],
  exports: [],
  providers: [],
})
export class AppModule {
  constructor() {}
}
