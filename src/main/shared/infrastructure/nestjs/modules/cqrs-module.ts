import AvailableRidersHandler from '@deliveryService/delivery/infrastructure/available-drivers-handler';
import GoogleGeolocationDescriptor from '@deliveryService/delivery/infrastructure/geolocation-info/google-geolocation-descriptor';
import IntervalTimerRequestAttemptCountdownTimer from '@deliveryService/delivery/infrastructure/request-attempt/intervaltimer-request-attempt-countdown-timer';
import RestaurantOrdersHandler from '@deliveryService/delivery/infrastructure/restaurant-orders-handler';
import ServiceRecoveryService from '@deliveryService/delivery/infrastructure/service-recovery/service-recovery';
import { Global, Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import QueryHandler from '@shared/domain/bus/query/query-handler';
import AuthCodeGenerator from '@shared/infrastructure/authentication/auth-code-generator';
import NodejsCommandBus from '@shared/infrastructure/bus/command/nodejs-command-bus';
import NodejsEventBus from '@shared/infrastructure/bus/event/nodejs-event-bus';
import NodejsQueryBus from '@shared/infrastructure/bus/query/nodejs-query-bus';
import MobilePushNotificationService from '@shared/infrastructure/push-notifications';
import BasicTimeHumanizer from '@shared/infrastructure/utils/basic-time-humanizer';
import { FirebaseAdminModule } from '@tfarras/nestjs-firebase-admin';
import * as admin from 'firebase-admin';
import FirebaseUploader from '@shared/infrastructure/storage/firebase-uploader';
import MulterMultipartHandler from '@shared/infrastructure/storage/file-upload.service';
import FirebaseExcelGenerator from '@shared/infrastructure/pdf/firebase-excel-generator';
import FirebaseHtmlPdfGenerator from '@shared/infrastructure/pdf/firebase-html-pdf-generator';

const walkSync = require('walk-sync');
const path = require('path');
const multer = require('fastify-multer');

const commandHandlers = discoverCommandHandlers();
const queryHandlers = discoverQueryHandlers();
const eventSubscribers = discoverEventSubscribers();
const useCases = discoverUseCases();
const repositories = discoverRepositories();

const requestTimers = {};

const uploadDiskDest = path.join(
  __dirname,
  '../../../../../../../public/uploads',
);

/*
const pdfTemplatesFolder = path.join(
    __dirname,
    '../../../../../../../public/pdf-templates',
);
*/

const pdfTemplatesFolder = path.resolve('public/pdf-templates');

const multerOptions: any = {
  storage: multer.diskStorage({
    destination: path.join(__dirname, '../../../../../../.uploads'),
    // Use the original file name as is
    filename: (req, file, cb) => {
      cb(null, file.originalname);
    },
  }),
  limits: { fieldSize: 25 * 1024 * 1024 },
};
const CQRSProviders = [
  {
    provide: 'command.bus.handlers',
    useFactory: (...handlers) => {
      return handlers || [];
    },
    inject: [...commandHandlers],
  },
  {
    provide: 'query.bus.handlers',
    useFactory: (...handlers) => {
      return handlers || [];
    },
    inject: [...queryHandlers],
  },
  {
    provide: 'event.bus.subscribers',
    useFactory: (...subscribers) => {
      return subscribers || [];
    },
    inject: [...eventSubscribers],
  },
  {
    provide: 'command.bus',
    useClass: NodejsCommandBus,
  },
  {
    provide: 'query.bus',
    useClass: NodejsQueryBus,
  },
  {
    provide: 'event.bus',
    useClass: NodejsEventBus,
  },
];

const customProviders = [
  {
    provide: 'services.request.geolocation.descriptor',
    useClass: GoogleGeolocationDescriptor,
  },
  {
    provide: 'delivery.request.timers',
    useValue: requestTimers,
  },
  {
    provide: 'services.request.attempt.countdown.timer',
    useClass: IntervalTimerRequestAttemptCountdownTimer,
  },
  {
    provide: 'multipart.handler',
    useClass: MulterMultipartHandler,
  },
  {
    provide: 'file.uploader',
    useClass: FirebaseUploader,
  },
  {
    provide: 'code.generator',
    useClass: AuthCodeGenerator,
  },
  {
    provide: 'storage.upload.path',
    useValue: uploadDiskDest,
  },
  {
    provide: 'multer.options',
    useValue: multerOptions,
  },
  {
    provide: 'pdf.templates.root.folder',
    useValue: pdfTemplatesFolder,
  },
  {
    provide: 'pdf.generator',
    useClass: FirebaseHtmlPdfGenerator,
  },
  {
    provide: 'excel.generator',
    useClass: FirebaseExcelGenerator,
  },
  {
    provide: 'delivery.service.recovery',
    useClass: ServiceRecoveryService,
  },
  {
    provide: 'available.riders.handler',
    useClass: AvailableRidersHandler,
  },
  {
    provide: 'restaurant.orders.handler',
    useClass: RestaurantOrdersHandler,
  },
  {
    provide: 'time.humanizer',
    useClass: BasicTimeHumanizer,
  },
  {
    provide: 'push.notification.service',
    useClass: MobilePushNotificationService,
  },
];

const providers = [
  ...useCases,
  ...repositories,
  ...customProviders,

  ...commandHandlers,
  ...queryHandlers,
  ...eventSubscribers,
  ...CQRSProviders,
];

@Global()
@Module({
  imports: [
    FirebaseAdminModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        return {
          apiKey: configService.get<string>('FB_API_KEY'),
          authDomain: configService.get<string>('FB_AUTH_DOMAIN'),
          databaseURL: configService.get<string>('FB_DB_URL'),
          projectId: configService.get<string>('FB_PROJECT_ID'),
          storageBucket: configService.get<string>('FB_STORAGE_BUCKET'),
          messagingSenderId: configService.get<string>(
            'FB_MESSAGING_SENDER_ID',
          ),
          appId: configService.get<string>('FB_APP_ID'),
          measurementId: configService.get<string>('FB_MEASUREMENT_ID'),
          credential: admin.credential.applicationDefault(),
        };
      },
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
  ],
  providers: providers,
  exports: providers,
})
export class CQRSModule {
  constructor() {}
}

export function discoverCommandHandlers(): Array<Constructor<CommandHandler>> {
  return walkSync('src/main', {
    globs: ['**/*-command-handler.ts', '**/command-handlers/**/*.ts'],
    includeBasePath: false,
  }).map((p: string) => {
    const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
    const classConstructor = require(filePath);
    const instance: CommandHandler = classConstructor.default;
    return instance;
  });
}

export function discoverQueryHandlers(): Array<Constructor<QueryHandler>> {
  return walkSync('src/main', {
    globs: ['**/*-query-handler.ts', '**/query-handlers/**/*.ts'],
  }).map((p: string) => {
    const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
    const classConstructor = require(filePath);
    const instance: QueryHandler = classConstructor.default;
    return instance;
  });
}

export function discoverEventSubscribers(): Array<
  Constructor<EventSubscriber>
> {
  return walkSync('src/main', {
    globs: ['**/*-event-subscriber.ts'],
  }).map((p: string) => {
    const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));

    const classConstructor = require(filePath);
    const instance: EventSubscriber = classConstructor.default;
    return instance;
  });
}

export function discoverUseCases(): Array<Constructor<CommandHandler>> {
  return walkSync('src/main', {
    globs: ['**/*-use-case.ts', '**/use-cases/**/*.ts'],
  }).map((p: string) => {
    const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
    const classConstructor = require(filePath);
    const instance: CommandHandler = classConstructor.default;

    return {
      provide: `${classConstructor.default.name}`,
      useClass: instance,
    };
  });
}

export function discoverRepositories(): Array<Constructor<CommandHandler>> {
  return walkSync('src/main', {
    globs: ['**/*-infrastructure-*-repository.ts'],
  })
    .map((p: string) => {
      const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
      const classConstructor = require(filePath);
      const instance: CommandHandler = classConstructor.default;
      return instance;
    })
    .filter((r) => {
      const log = true;
      if (!r?.bindingKey && log) {
        console.log(`${r.name} does not have static bindingKey`);
      }

      if (!r) {
        return false;
      }

      return true;
    })
    .map((r) => {
      return {
        provide: `${r.bindingKey}`,
        useClass: r,
      };
    });
}

type Constructor<T> = any;
