import {
  CREDIT_LOG_REF,
  CREDIT_RECHARGES_REF,
  DELIVERY_ORDERS,
  DELIVERY_SERVICE_CHECKER_INTERVAL_MINUTES,
  DISCOUNT_CODES_REDEEMS,
  DRIVERS_ACTIVITIES,
  ENGAGEMENT_NOTIFICATIONS,
  HOLDING_VENDOR_ORDERS,
  RIDER_APPLICATIONS,
  RIDER_DELIVERY_ALERTS,
  RIDER_REVIEWS,
  RIDERS,
  RIDERS_WITHDRAWALS_REQUESTS,
  SCHEDULED_DELIVERY_INTERVAL_MINUTES,
  VENDOR_ORDERS,
  VENDOR_PRODUCTS,
  VENDOR_REVIEWS,
  VENDORS,
  VENDORS_WITHDRAWALS_REQUESTS,
} from './constants';

import deleteRider from './use-cases/riders/delete-rider';
import deleteVendor from './use-cases/vendors/delete-vendor';
import generateAdminPdfRequest from './use-cases/admin/generate-admin-pdf-request';
import generateRiderWithdrawalInvoice from './use-cases/riders/generate-rider-withdrawals-invoice';
import generateVendorSalesInvoice from './use-cases/vendors/generate-vendor-sales-invoice';
import { handleAppCreditsTransactions } from './use-cases/customers/handle-app-credits-transactions';
import { handleBestOffers } from './use-cases/products/handle-best-offers';
import { handleCustomerReferralPrizes } from './use-cases/customers/handle-customer-referral-prizes';
import notifyDriverWithdrawalRequest from './use-cases/riders/notify-driver-withdrawal-request';
import notifyVendorWithdrawalRequest from './use-cases/admin/notify-vendor-withdrawal-request';
import { processAppCreditsRecharges } from './use-cases/customers/process-app-credits-recharges';
import { processPurchasesWithAppCredits } from './use-cases/delivery/process-purchases-with-app-credits';
import runScheduledDeliveries from './use-cases/delivery/run-scheduled-deliveries';
import sendChatNotification from './use-cases/delivery/send-chat-notification';
import { sendCustomerOrderUpdatesNotifications } from './use-cases/delivery/send-customer-order-updates-notifications';
import sendDriverWelcomeEmail from './use-cases/riders/send-driver-welcome-email';
import sendNewVendorOrderPushNotification from './use-cases/delivery/send-new-vendor-order-push-notification';
import trackVendorsDemand from './use-cases/delivery/track-vendors-demand';
import updateDriversTotalEarnings from './use-cases/riders/update-drivers-total-earnings';
import updateOrderProductsDemand from './use-cases/products/update-order-products-demand';
import updateOrderProductsStock from './use-cases/products/update-products-stock';
import updateVendorTotalEarnings from './use-cases/vendors/update-vendors-total-earnings';
import generateOrderInvoice from './use-cases/delivery/generate-order-invoice';
import handleVendorReviews from './use-cases/delivery/handle-vendor-reviews';
import handleRiderReviews from './use-cases/delivery/handle-rider-reviews';
import { sendRiderNewDeliveryNotification } from './use-cases/riders/send-rider-new-delivery-notification';
import handleRiderReferrals from './use-cases/riders/handle-rider-referrals';
import { updateDeliveryServiceStatus } from './use-cases/delivery/update-delivery-service-status';
import { runDeliveryServiceChecker } from './use-cases/delivery/run-delivery-service-checker';
import { updateRiderServiceStatus } from './use-cases/delivery/update-rider-service-status';
import { syncVendorLocation } from './use-cases/vendors/sync-vendor-location';
import CloudFunctionsAppUtils from './utils/app';
import { handleCustomerDiscountCodeRedeems } from './use-cases/delivery/handle-customer-discount-code-redeems';
import { notifyNewPhonePayOrder } from './use-cases/admin/notify-new-phone-pay-order';
import { handleShopsOpenings } from './use-cases/vendors/handle-shops-openings';
import { handleImmediateBulkPushNotifications } from './use-cases/admin/handle-immediate-bulk-push-notifications';
import { handleScheduledBulkNotifications } from './use-cases/admin/handle-scheduled-bulk-notifications.ts';
import { recalculateProductInfo } from './use-cases/products/recalculate-product-info';
import syncExternalVendorProducts from './use-cases/products/sync-external-vendor-products';
import { handleDiscountCodeRedeemStats } from './use-cases/customers/handle-discount-code-redeem-stats';

const functions = require('firebase-functions');
const admin = require('firebase-admin');

if (admin.apps.length === 0) {
  admin.initializeApp();
}
exports.sendNewVendorOrderPushNotification = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onCreate(sendNewVendorOrderPushNotification);

exports.updateOrderProductsDemand = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onWrite(updateOrderProductsDemand);

exports.updateOrderProductsStock = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onWrite((change, context) => {
    return updateOrderProductsStock({ change, context }, VENDOR_ORDERS);
  });

exports.updateHoldingOrderProductsStock = functions.firestore
  .document(`/${HOLDING_VENDOR_ORDERS}/{documentId}`)
  .onWrite((change, context) => {
    return updateOrderProductsStock({ change, context }, HOLDING_VENDOR_ORDERS);
  });

exports.trackVendorsDemand = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onUpdate(trackVendorsDemand);

exports.sendChatNotification = functions.firestore
  .document(`/channels/{channelId}/thread/{threadId}`)
  .onCreate(sendChatNotification);

exports.generateVendorSalesInvoice = functions.firestore
  .document(`/vendor_invoices/{documentId}`)
  .onUpdate(generateVendorSalesInvoice);

exports.generateRiderWithdrawalInvoice = functions.firestore
  .document(`/rider_invoices/{documentId}`)
  .onUpdate(generateRiderWithdrawalInvoice);

exports.generateOrderInvoice = functions.firestore
  .document(`/customer_invoices/{documentId}`)
  .onUpdate(generateOrderInvoice);

exports.updateVendorTotalEarnings = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onUpdate(updateVendorTotalEarnings);

exports.updateDriversTotalEarnings = functions.firestore
  .document(`/${DRIVERS_ACTIVITIES}/{documentId}`)
  .onCreate(updateDriversTotalEarnings);

exports.handleRiderReferrals = functions.firestore
  .document(`/${RIDERS}/{documentId}`)
  .onCreate(handleRiderReferrals);

exports.notifyVendorWithdrawalRequest = functions.firestore
  .document(`/${VENDORS_WITHDRAWALS_REQUESTS}/{documentId}`)
  .onCreate(notifyVendorWithdrawalRequest);

exports.notifyDriverWithdrawalRequest = functions.firestore
  .document(`/${RIDERS_WITHDRAWALS_REQUESTS}/{documentId}`)
  .onCreate(notifyDriverWithdrawalRequest);

exports.sendDriverWelcomeEmail = functions.firestore
  .document(`/${RIDER_APPLICATIONS}/{documentId}`)
  .onCreate(sendDriverWelcomeEmail);

exports.deleteVendor = functions.firestore
  .document(`/${VENDORS}/{documentId}`)
  .onUpdate(deleteVendor);

exports.deleteRider = functions.firestore
  .document(`/${RIDERS}/{documentId}`)
  .onUpdate(deleteRider);

exports.generateAdminPdfRequest = functions.firestore
  .document(`/admin_pdf/{documentId}`)
  .onCreate(generateAdminPdfRequest);

exports.handleVendorReviews = functions.firestore
  .document(`/${VENDOR_REVIEWS}/{documentId}`)
  .onCreate(handleVendorReviews);

exports.handleRiderReviews = functions.firestore
  .document(`/${RIDER_REVIEWS}/{documentId}`)
  .onCreate(handleRiderReviews);

exports.handleBestOffers = functions.firestore
  .document(`/${VENDOR_PRODUCTS}/{documentId}`)
  .onWrite(handleBestOffers);

exports.recalculateProductInfo = functions.firestore
  .document(`/${VENDOR_PRODUCTS}/{documentId}`)
  .onWrite(recalculateProductInfo);

exports.handleAppCreditsTransactions = functions.firestore
  .document(`/${CREDIT_LOG_REF}/{documentId}`)
  .onWrite(handleAppCreditsTransactions);

exports.processAppCreditsRecharges = functions.firestore
  .document(`/${CREDIT_RECHARGES_REF}/{documentId}`)
  .onWrite(processAppCreditsRecharges);

exports.processPurchasesWithAppCredits = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onWrite(processPurchasesWithAppCredits);

exports.handleCustomerReferralPrizes = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onWrite(handleCustomerReferralPrizes);

exports.handleCustomerDiscountCodeRedeems = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onWrite(handleCustomerDiscountCodeRedeems);

exports.sendCustomerOrderUpdatesNotifications = functions.firestore
  .document(`/${VENDOR_ORDERS}/{documentId}`)
  .onWrite(sendCustomerOrderUpdatesNotifications);

exports.sendRiderNewDeliveryNotification = functions.database
  .ref(`${RIDER_DELIVERY_ALERTS}/{documentId}`)
  .onCreate(sendRiderNewDeliveryNotification);

exports.updateDeliveryServiceStatus = functions.firestore
  .document(`${DELIVERY_ORDERS}/{documentId}`)
  .onWrite(updateDeliveryServiceStatus);

exports.updateRiderServiceStatus = functions.firestore
  .document(`/${RIDERS}/{documentId}`)
  .onWrite(updateRiderServiceStatus);

exports.syncVendorLocation = functions.firestore
  .document(`/${VENDORS}/{documentId}`)
  .onWrite(syncVendorLocation);

exports.handleImmediateBulkPushNotifications = functions.firestore
  .document(`/${ENGAGEMENT_NOTIFICATIONS}/{documentId}`)
  .onWrite(handleImmediateBulkPushNotifications);

exports.runScheduledDeliveries = functions.pubsub
  .schedule(`every ${SCHEDULED_DELIVERY_INTERVAL_MINUTES} minutes`)
  .onRun(runScheduledDeliveries);

exports.handleScheduledBulkNotifications = functions.pubsub
  .schedule(`every 1 minutes`)
  .onRun(handleScheduledBulkNotifications);

exports.runDeliveryServiceChecker = functions.pubsub
  .schedule(`every ${DELIVERY_SERVICE_CHECKER_INTERVAL_MINUTES} minutes`)
  .onRun(runDeliveryServiceChecker);

exports.handleShopsOpenings = functions.pubsub
  .schedule(`every 1 minutes`)
  .onRun(handleShopsOpenings);

exports.notifyNewPhonePayOrder = functions.firestore
  .document(`/${HOLDING_VENDOR_ORDERS}/{documentId}`)
  .onWrite(notifyNewPhonePayOrder);

exports.syncExternalVendorProducts = functions.pubsub
  .schedule(`every 6 hours`)
  .onRun(syncExternalVendorProducts);

exports.handleDiscountCodeRedeemStats = functions.firestore
  .document(`/${DISCOUNT_CODES_REDEEMS}/{documentId}`)
  .onWrite(handleDiscountCodeRedeemStats);

exports.testNotifications = functions.https.onRequest(
  async (req: any, res: any) => {
    const data = req.body;

    try {
      const r = await CloudFunctionsAppUtils.sendPushNotification(
        {
          data: data.data,
          notification: data.notification,
        },
        data.role ?? 'CUSTOMER',
        data.userId,
      );

      res.json({
        ok: true,
        result: r,
        data: data,
      });
    } catch (e: any) {
      res.json({
        ok: false,
        e: e?.message,
      });
    }
  },
);
