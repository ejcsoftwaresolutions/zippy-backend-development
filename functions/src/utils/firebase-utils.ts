const FirebaseUtils = {
  getDate(firebaseDate: any) {
    return new Date(firebaseDate.seconds * 1000);
  },
};

export default FirebaseUtils;
