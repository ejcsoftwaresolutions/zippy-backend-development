import CurrencyPrice, {
  CurrencyPricePrimitiveProps,
} from '../../models/currency-price';
import Model from '../../models/model';

type ProductPhoto = {
  url: string;
  id: string;
  mode?: string;
};

type ProductType = 'REGULAR' | 'BUNDLE';

interface SellerProductProps {
  id: string;
  name: string;
  description: string;
  categoryId: string;
  subcategoryId: string;
  sellerId: string;
  price: CurrencyPrice;
  discountPrice: CurrencyPrice;
  available: boolean;
  photos: ProductPhoto[];
  stockCount?: number;
  imageUrl?: string;
  type: ProductType;
  isNew: boolean;
}

export type SellerProductPrimitiveProps = Omit<
  SellerProductProps,
  'price' | 'discountPrice' | 'addons' | 'type'
> & {
  price: CurrencyPricePrimitiveProps;
  discountPrice: CurrencyPricePrimitiveProps;
  photos: ProductPhoto[];
  type: string;
};

export default class SellerProduct extends Model<SellerProductProps> {
  get id() {
    return this.props.id;
  }

  get name() {
    return this.props.name;
  }

  get isNew() {
    return this.props.isNew;
  }

  get imageUrl() {
    return this.props.imageUrl;
  }

  get categoryId() {
    return this.props.categoryId;
  }

  get subcategoryId() {
    return this.props.subcategoryId;
  }

  get description() {
    return this.props.description;
  }

  get stockCount() {
    return this.props.stockCount;
  }

  get available() {
    return this.props.available;
  }

  get photo() {
    if (this.props.photos.length === 0) return null;
    return this.props.photos[0].url;
  }

  static fromPrimitives(props: SellerProductPrimitiveProps) {
    return new SellerProduct({
      ...props,
      price: CurrencyPrice.fromPrimitives(props.price),
      discountPrice: CurrencyPrice.fromPrimitives(props.discountPrice),
      type: props.type as any,
      isNew: false,
    });
  }

  static fromNew(
    props: Pick<
      SellerProductPrimitiveProps,
      | 'id'
      | 'name'
      | 'description'
      | 'photos'
      | 'price'
      | 'discountPrice'
      | 'sellerId'
      | 'categoryId'
      | 'subcategoryId'
      | 'stockCount'
    >,
  ) {
    return new SellerProduct({
      ...props,
      price: CurrencyPrice.fromPrimitives(props.price),
      discountPrice: CurrencyPrice.fromPrimitives(props.discountPrice),
      available: true,
      type: 'REGULAR' as any,
      isNew: true,
    });
  }

  toPrimitives(): SellerProductPrimitiveProps {
    return {
      ...this.props,
      price: this.props.price.toPrimitives(),
      discountPrice: this.props.discountPrice.toPrimitives(),
      type: this.props.type as string,
    };
  }
}
