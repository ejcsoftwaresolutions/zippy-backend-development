import axios from 'axios';
import TextUtils from '../../text-utils';

const isDev = process.env.NODE_ENV != 'production';
export default class SucasaProductFetcher {
  CAT_LIMIT = 100;
  PRODUCT_LIMIT = process.env.NODE_ENV !== 'production' ? 50 : 100;

  private readonly categoryUrl: string;
  private readonly productUrl: string;

  constructor() {
    const baseUrl = 'https://app.ecwid.com/api/v3/28254021';
    const token = 'public_xsaYDPM4r2HLZZHfSpVHwGhUh28WDcxP';

    this.categoryUrl = `${baseUrl}/categories?token=${token}&lang=es_419&parent=0`;
    this.productUrl = `${baseUrl}/products?token=${token}&lang=es_419&enabled=true`;
  }

  private static getOnlyUpdatedFilterString(filters?: any) {
    return filters && filters.updatedFrom
      ? `&updatedFrom=${filters.updatedFrom}&updatedTo=${filters.updatedTo}`
      : '';
  }

  private static getOnlyCreatedFilterString(filters?: any) {
    return filters && filters.createdFrom
      ? `&createdFrom=${filters.createdFrom}&createdTo=${filters.createdTo}`
      : '';
  }

  async getCategories() {
    const totalPages = await this.getTotalCategoriesPages();

    const data = (
      await Promise.all(
        [...new Array(totalPages)].map((x, p) => this.getCategoryPage(p + 1)),
      )
    ).flatMap((res) => res);

    return data
      .filter((c) => {
        return (
          !c.parentId &&
          ![
            'restaurante',
            'empaque y confeccion',
            'ferrehogar',
            'hogar',
            'papeleria',
            'licores',
          ].includes(
            c.name
              .toLowerCase()
              .normalize('NFD')
              .replace(/[\u0300-\u036f]/g, ''),
          )
        );
      })
      .map((c) => {
        return {
          ...c,
          name: TextUtils.capitalize(c.name.toLowerCase()),
        };
      });
  }

  public async getProductsByPage(
    number: number,
    filters?: {
      categoryId: string;
      updatedFrom?: string;
      updatedTo?: string;
      createdFrom?: string;
      createdTo?: string;
    },
  ) {
    if (!filters || !filters.categoryId) return [];

    const offset = (number - 1) * this.PRODUCT_LIMIT;

    const updatedFilters =
      SucasaProductFetcher.getOnlyUpdatedFilterString(filters);
    const createdFilters =
      SucasaProductFetcher.getOnlyCreatedFilterString(filters);

    const res = await axios.get(
      `${this.productUrl}&offset=${offset}&limit=${this.PRODUCT_LIMIT}${
        filters
          ? `&categories=${filters.categoryId}&includeProductsFromSubcategories=true${updatedFilters}${createdFilters}`
          : ''
      }`,
    );

    const data = res.data.items ?? [];

    return data
      .filter((p) => {
        /*Antojos y fiestas*/
        if (filters.categoryId.toString() === '120462504') {
          /*Confitería, Dulcería Galletas, Snacks y Pasapalos*/

          return p.categoryIds
            .map((id) => id.toString())
            .some((s) =>
              ['120462505', '120462508', '120462509', '120462512'].includes(s),
            );
        }
        return true;
      })
      .map((p: any) => {
        return {
          ...p,
          defaultCategoryId: filters.categoryId
            ? filters.categoryId
            : p.defaultCategoryId,
        };
      });
  }

  async getTotalProductPages(filters?: {
    categoryId: string;
    updatedFrom?: string;
    updatedTo?: string;
    createdFrom?: string;
    createdTo?: string;
  }) {
    if (!filters || !filters.categoryId) return [];

    const updatedFilters =
      SucasaProductFetcher.getOnlyUpdatedFilterString(filters);
    const createdFilters =
      SucasaProductFetcher.getOnlyCreatedFilterString(filters);

    const res = await axios.get(
      `${this.productUrl}&limit=${this.PRODUCT_LIMIT}${
        filters
          ? `&categories=${filters.categoryId}&includeProductsFromSubcategories=true${updatedFilters}${createdFilters}`
          : ''
      }`,
    );

    const totalPages = Math.ceil(res.data.total / this.PRODUCT_LIMIT);
    return totalPages > 0 ? (isDev ? 1 : totalPages) : 0; //totalPages;
  }

  private async getCategoryPage(number: number) {
    const offset = (number - 1) * this.CAT_LIMIT;
    const res = await axios.get(
      `${this.categoryUrl}&offset=${offset}&limit=${this.CAT_LIMIT}`,
    );

    const data = res.data.items ?? [];

    return data;
  }

  private async getTotalCategoriesPages() {
    const res = await axios.get(`${this.categoryUrl}&limit=${this.CAT_LIMIT}`);

    const totalPages = Math.ceil(res.data.total / this.CAT_LIMIT);
    return totalPages;
  }
}
