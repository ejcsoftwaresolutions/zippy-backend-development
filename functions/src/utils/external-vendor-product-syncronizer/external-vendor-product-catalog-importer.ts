import VendorProductsRepository from './repositories/vendor-products-repository';

export default class ExternalVendorProductCatalogImporter {
  private vendorProductRepo: VendorProductsRepository;

  constructor() {
    this.vendorProductRepo = new VendorProductsRepository();
  }

  public async syncProducts(
    sellerId: string,
    sellerCategoryId: string,
    extraCommissionPercentage: number,
    products: any[],
    mapper: any,
  ) {
    const productMappings =
      await this.vendorProductRepo.getVendorProductsMappings(sellerId);
    const subcategoriesMappings =
      await this.vendorProductRepo.getVendorProductSubcategoriesMappings(
        sellerId,
      );

    if (Object.values(subcategoriesMappings ?? {}).length === 0)
      throw new Error('NO_SUBCATEGORIES_MAPPINGS_FOUND');

    const existingProducts = await this.vendorProductRepo.getProductsById(
      Object.values(productMappings ?? {}),
    );

    const appExchangeRate = await this.vendorProductRepo.getAppExchangeRate();
    const mappedProducts = mapper.toDomainFromArray(products, {
      mappings: {
        products: productMappings,
        subcategories: subcategoriesMappings,
        existingProducts: existingProducts.reduce((acc, current) => {
          return { ...acc, [current.id]: current };
        }, {}),
      },
      sellerId: sellerId,
      categoryId: sellerCategoryId,
      currencyExchange: appExchangeRate,
      extraCommissionPercentage: extraCommissionPercentage,
    });

    await this.vendorProductRepo.saveVendorProducts(mappedProducts);

    const newMappings = mappedProducts.reduce(
      (acc: any, actual: any, index: number) => {
        return { ...acc, [products[index].id]: actual.id };
      },
      {},
    );

    await this.vendorProductRepo.saveVendorProductsMappings(
      sellerId,
      newMappings,
    );
  }

  public async syncSubcategories(
    sellerId: string,
    sellerCategoryId: string,
    categories: any[],
    mapper: any,
  ) {
    const appCategories = await this.vendorProductRepo.getSubcategories(
      sellerCategoryId,
    );

    const mappedCategories = mapper.mapCategories(categories, appCategories);

    await this.vendorProductRepo.saveVendorSubcategoriesProductsMappings(
      sellerId,
      mappedCategories,
    );
  }

  public async hasProductsMappings(sellerId: string) {
    const productMappings =
      await this.vendorProductRepo.getVendorProductsMappings(sellerId);

    return Object.values(productMappings ?? {}).length > 0;
  }
}
