import ExternalVendorProductCatalogImporter from '../external-vendor-product-catalog-importer';
import SucasaProductFetcher from '../fetchers/sucasa-product-fetcher';
import SucasaMapper from '../mappers/sucasa-mapper';

export default class SucasaProductBatcher {
  fetcher: SucasaProductFetcher;

  constructor(
    private productImporter: ExternalVendorProductCatalogImporter,
    private sellerInfo: {
      sellerId: string;
      sellerCategoryId: string;
      extraCommissionPercentage: number;
    },
  ) {
    this.fetcher = new SucasaProductFetcher();
  }

  async process(
    categories: any[],
    config: { mode: 'FULL-SYNC' | 'PARTIAL-SYNC' },
  ) {
    const promises = (() => {
      if (config.mode === 'FULL-SYNC') {
        return categories.map((category) => {
          return this.processCategory(category);
        });
      }

      const start = new Date();
      start.setUTCHours(0, 0, 0, 0);
      const end = new Date();
      end.setUTCHours(23, 59, 59, 999);
      const startUNIX = Math.floor(start.getTime() / 1000);
      const endUNIX = Math.floor(end.getTime() / 1000);

      return categories
        .map((category) => {
          return this.processCategory(category, {
            createdFrom: startUNIX,
            createdTo: endUNIX,
          });
        })
        .concat(
          categories.map((category) => {
            return this.processCategory(category, {
              updatedFrom: startUNIX,
              updatedTo: endUNIX,
            });
          }),
        );
    })();

    await Promise.all(promises);
  }

  private async processCategory(category: any, filters?: any) {
    const finalFilters = {
      categoryId: category.id,
      ...(filters ?? {}),
    };

    const totalCategoryPages = await this.fetcher.getTotalProductPages(
      finalFilters,
    );

    const catPromises = [...new Array(totalCategoryPages)].map((x, i) => {
      return new Promise(async (revolvePage) => {
        const page = i + 1;

        await this.processPage(page, finalFilters);

        return revolvePage({});
      });
    });

    await Promise.all(catPromises);
  }

  private async processPage(page: number, filters: any) {
    const currentChunk = await this.fetcher.getProductsByPage(page, filters);

    await this.productImporter.syncProducts(
      this.sellerInfo.sellerId,
      this.sellerInfo.sellerCategoryId,
      this.sellerInfo.extraCommissionPercentage,
      currentChunk,
      SucasaMapper,
    );
  }
}
