import admin from 'firebase-admin';
import SellerProduct from '../models/seller-product';
import ExternalProductMapper from '../mappers/external-product-mapper';
import FirestoreApiRepository from '../../firestore-api-repository';

export default class VendorProductsRepository extends FirestoreApiRepository {
  async saveVendorProducts(products: SellerProduct[]): Promise<void> {
    const entities = ExternalProductMapper.toPersistenceFromArray(products);
    if (entities.length === 0) return Promise.resolve();

    const batch = admin.firestore().batch();
    const ref = admin.firestore().collection('vendor_products');
    entities.forEach((product, index) => {
      const isNew = products[index].isNew;
      const productRef = ref.doc(product.id);

      return isNew
        ? batch.create(productRef, product)
        : batch.update(productRef, product);
    });
    await batch.commit();
  }

  async getProductsById(ids: string[]) {
    return this.getDocs('vendor_products', {
      filters: [
        {
          field: 'id',
          operator: 'in',
          value: ids,
        },
      ],
    });
  }

  async getVendorProductsMappings(
    sellerId: string,
  ): Promise<{ [p: string]: string }> {
    const mappings = (
      await admin
        .database()
        .ref(`external_vendor_product_mappings/${sellerId}`)
        .get()
    ).val();

    return Promise.resolve(mappings ?? {});
  }

  async saveVendorProductsMappings(
    sellerId: string,
    map: { [p: string]: string },
  ): Promise<void> {
    return admin
      .database()
      .ref(`external_vendor_product_mappings/${sellerId}`)
      .update(map);
  }

  async getVendorProductSubcategoriesMappings(
    sellerId: string,
  ): Promise<{ [p: string]: string }> {
    const mappings = (
      await admin
        .database()
        .ref(`external_vendor_subcategories_mappings/${sellerId}`)
        .get()
    ).val();

    return Promise.resolve(mappings ?? {});
  }

  async saveVendorSubcategoriesProductsMappings(
    sellerId: string,
    map: { [p: string]: string },
  ): Promise<void> {
    return admin
      .database()
      .ref(`external_vendor_subcategories_mappings/${sellerId}`)
      .update(map);
  }

  async getSubcategories(categoryId: string): Promise<any> {
    const docs = await admin
      .firestore()
      .collection('vendor_categories')
      .where('parentID', '==', categoryId)
      .get();

    return docs.docs.map((d) => d.data());
  }

  async getAppExchangeRate(): Promise<number> {
    const doc = await admin
      .firestore()
      .collection('app_configurations')
      .doc('GLOBAL_FEES')
      .get();

    return doc.data()?.exchangeRates?.bolivar ?? 1;
  }
}
