import { ArrayUtils } from '../../array-utils';
import CurrencyPrice from '../../models/currency-price';
import Id from '../../models/id';
import SellerProduct from '../models/seller-product';
import BsCurrency from '../../models/bs-currency';

export default class SucasaMapper {
  static toDomainFromArray(
    dtos: any[],
    config: {
      mappings: {
        products: any;
        subcategories: any;
        existingProducts: any;
      };
      currencyExchange: number;
      sellerId: string;
      categoryId: string;
      extraCommissionPercentage: number;
    },
  ) {
    return dtos.map((dto) => {
      const appId = config.mappings.products[dto.id];
      const existingProduct = config.mappings.existingProducts[appId];

      const subcategoryId =
        config.mappings.subcategories[dto.defaultCategoryId];

      const price = (() => {
        const n = parseFloat(dto.price);

        /*Inverse percentage */
        return parseFloat(
          ((n * 100) / (100 - config.extraCommissionPercentage ?? 0)).toFixed(
            2,
          ),
        );
      })();

      const discountPrice = (() => {
        if (!existingProduct) return price;

        return (
          price -
          parseFloat(((existingProduct.discountRate * price) / 100).toFixed(2))
        );
      })();

      const getName = (string: string) => {
        return string.replace('*Producto disponible en pocas horas!!', '');
      };

      const photos = dto.media
        ? dto.media.images.map((p) => {
            return {
              id: new Id().value,
              mode: 'CONTAIN',
              url: p.imageOriginalUrl,
              versions: [
                {
                  id: 'DEFAULT',
                  url: p.imageOriginalUrl,
                  mode: 'CONTAIN',
                },
                {
                  id: 'THUMBNAIL',
                  url: p.image160pxUrl ?? p.imageOriginalUrl,
                  mode: 'CONTAIN',
                },
                {
                  id: 'MEDIUM',
                  url: p.image800pxUrl ?? p.imageOriginalUrl,
                  mode: 'CONTAIN',
                },
                {
                  id: 'HIGH',
                  url: p.image1500pxUrl ?? p.imageOriginalUrl,
                  mode: 'CONTAIN',
                },
              ],
            };
          })
        : [];

      return !!appId
        ? SellerProduct.fromPrimitives({
            id: appId ?? new Id().value,
            name: getName(dto.name),
            description: dto.description.replace(/<[^>]*>?/gm, '') ?? '',
            photos: photos,
            price: CurrencyPrice.fromPrimitives({
              value: price,
              currency: BsCurrency.toPrimitives(),
            }).toPrimitives(),
            discountPrice: CurrencyPrice.fromPrimitives({
              value: discountPrice,
              currency: BsCurrency.toPrimitives(),
            }).toPrimitives(),
            sellerId: config.sellerId,
            categoryId: config.categoryId,
            subcategoryId: subcategoryId,
            available: true /*omitted at save*/,
            type: 'REGULAR' /*omitted at save*/,
            isNew: false /*omitted at save*/,
          })
        : SellerProduct.fromNew({
            id: appId ?? new Id().value,
            name: getName(dto.name),
            description: dto.description.replace(/<[^>]*>?/gm, '') ?? '',
            photos: photos,
            price: CurrencyPrice.fromPrimitives({
              value: price,
              currency: BsCurrency.toPrimitives(),
            }).toPrimitives(),
            discountPrice: CurrencyPrice.fromPrimitives({
              value: discountPrice,
              currency: BsCurrency.toPrimitives(),
            }).toPrimitives(),
            sellerId: config.sellerId,
            categoryId: config.categoryId,
            subcategoryId: subcategoryId,
            stockCount: dto.quantity ?? 10,
          });
    });
  }

  static mapCategories(categories: any[], appCategories: any[]) {
    const newMappings = categories.reduce((acc, actual) => {
      const found = this.findCorrespondingCategory(actual, appCategories);
      return { ...acc, [actual.id]: found.id };
    }, {});

    return newMappings;
  }

  private static findCorrespondingCategory(
    category: any,
    appCategories: any[],
  ) {
    const misceCate = appCategories.find(
      (c) => c.title.toLowerCase() === 'misceláneos',
    );
    if (!misceCate) throw new Error('MISCELANEOUS_SUBCAT_NOT_CREATED');

    const results = ArrayUtils.filterLike(
      appCategories.map((c) => ({
        ...c,
        title: c.title
          .toLowerCase()
          .normalize('NFD')
          .replace(/[\u0300-\u036f]/g, ''),
      })),
      'title',
      category.name
        .toLowerCase()
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, ''),
    );

    return results.length > 0 ? results[0] : misceCate;
  }
}
