import SellerProduct from '../models/seller-product';
import CloudFunctionsAppUtils from '../../app';

export default class ExternalProductMapper {
  static toPersistenceFromArray(products: SellerProduct[]): any[] {
    return products.map((orderDto) => {
      return ExternalProductMapper.toPersistence(orderDto);
    });
  }

  static toPersistence(product: SellerProduct) {
    const dto = product.toPrimitives();

    return CloudFunctionsAppUtils.omitUnknown({
      id: dto.id,
      name: dto.name,
      price: dto.price.value,
      discountPrice: dto.discountPrice.value,
      photo: product.photo,
      description: '', //dto.description,
      photos: dto.photos?.map((e) => {
        return CloudFunctionsAppUtils.omitUnknown(e);
      }),
      vendorID: dto.sellerId,
      categoryID: dto.categoryId,
      subcategoryID: dto.subcategoryId,
      currency: dto.price.currency.abbr !== 'USD' ? 'Bs' : undefined,
      ...(product.isNew
        ? {
            stockCount: parseFloat(dto.stockCount + ''),

            discountRate: 0,
            salesQuantity: 0,
            applyDiscount: false,
            available: dto.available,
            type: dto.type ?? 'REGULAR',
            createdAt: new Date(),
            isPromotion: false,
            delivery: true,
            pickup: false,
          }
        : {}),
    });
  }
}
