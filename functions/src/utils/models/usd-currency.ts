import Currency from './currency';
import { usdAbbr, usdFormat } from '../currency-utils';

const UsdCurrency = new Currency({
  abbr: usdAbbr,
  name: 'Dolares',
  format: usdFormat,
});

export default UsdCurrency;
