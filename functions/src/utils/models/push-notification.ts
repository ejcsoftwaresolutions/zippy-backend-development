export interface PushNotificationParams {
  data?: any;
  notification: {
    title: string;
    body: string;
    image?: string;
  };
  channel?: string;
}

export type PushNotificationProps = {
  id: string;
  to: string | string[];
  title?: string;
  content: string;
  appId: string;
  channelId?: string;
  metadata?: any;
  imageUrl?: string;
  linkUrl?: string;
};

export class PushNotification {
  constructor(private props: PushNotificationProps) {}

  get id() {
    return this.props.id;
  }

  get to() {
    return this.props.to;
  }

  get appName(): 'CUSTOMER' | 'RIDER' | 'VENDOR' {
    if (this.props.appId === process.env.CUSTOMER_APP_EXPERIENCE_ID)
      return 'CUSTOMER';
    if (this.props.appId === process.env.STORE_APP_EXPERIENCE_ID)
      return 'VENDOR';
    return 'RIDER';
  }

  get isSegment() {
    return !!this.props.metadata?.isSegment;
  }

  get isExternalId() {
    return !!this.props.metadata?.isExternalId;
  }

  public toPrimitives() {
    return {
      id: this.props.id,
      to: this.props.to,
      title: this.props.title,
      content: this.props.content,
      appId: this.props.appId,
      channelId: this.props.channelId ?? this.props.appId.split('/')[1],
      metadata: this.props.metadata,
      imageUrl: this.props.imageUrl,
      linkUrl: this.props.linkUrl,
    };
  }
}
