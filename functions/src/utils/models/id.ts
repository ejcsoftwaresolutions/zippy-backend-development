import fadmin from 'firebase-admin';
import ValueObject from './value-object';

interface IdProps {
  value: string;
}

export default class Id extends ValueObject<IdProps> {
  constructor(id?: string) {
    super({
      value: id as any,
    });

    if (!this.props.value) {
      this.props.value = this.gen();
    }
  }

  get value() {
    return this.props.value;
  }

  toPrimitives() {
    return this.value;
  }

  toString() {
    return this.value;
  }

  private gen() {
    const id: string = fadmin.firestore().collection('test').doc().id;
    return id;
  }
}
