import { bsAbbr, bsFormat } from '../currency-utils';
import Currency from './currency';

const BsCurrency = new Currency({
  abbr: bsAbbr,
  name: 'Bolivares Soberanos',
  format: bsFormat,
});

export default BsCurrency;
