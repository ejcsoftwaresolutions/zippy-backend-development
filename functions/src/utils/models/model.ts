import CloudFunctionsAppUtils from '../app';

function asObject(value: any, options?: any): any {
  if (value == null) return value;

  if (typeof value.toPrimitives === 'function') {
    return value.toPrimitives();
  }

  if (Array.isArray(value)) {
    return value.map((item) =>
      CloudFunctionsAppUtils.omitUnknown(asObject(item, options)),
    );
  }

  return value;
}

export default abstract class Model<T> {
  protected props: T;

  constructor(props: T) {
    this.props = props;
  }

  toJson(): any {
    return this.toObject();
  }

  toObject(options?: any): any {
    const obj: any = {};

    const props = this.props;
    const keys = Object.keys(props as any);
    keys.forEach((propertyName) => {
      const val = (props as any)[propertyName];
      obj[propertyName] = asObject(val, options);
    });

    return CloudFunctionsAppUtils.omitUnknown(obj);
  }

  toString(): string {
    return JSON.stringify(this.props);
  }
}
