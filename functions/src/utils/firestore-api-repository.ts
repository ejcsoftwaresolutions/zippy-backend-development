import firebase, { firestore } from 'firebase-admin';
import FieldValue = firestore.FieldValue;

type QueryFilter = {
  field: string;
  operator: any;
  value: string[] | number | string | boolean;
};

type QueryDocsParams = {
  filters: QueryFilter[];
  limit?: number;
  orderBy?: {
    field: string;
    direction: any;
  };
};

export const FirestoreValue = FieldValue;

export default abstract class FirestoreApiRepository {
  private db: firebase.firestore.Firestore;

  constructor() {
    this.db = firebase.firestore();
  }

  get firestore() {
    return this.db;
  }

  async getDoc(collectionName: string, docId: string) {
    const result = await this.db.doc(`${collectionName}/${docId}`).get();
    const data = result.data();
    return data;
  }

  async updateDoc(collectionName: string, docId: string, data: any) {
    await this.db.doc(`${collectionName}/${docId}`).update(data);
  }

  getDocsRef(collectionName: string) {
    return this.db.collection(`${collectionName}`);
  }

  async deleteDoc(collectionName: string, docId: string) {
    await this.db.doc(`${collectionName}/${docId}`).delete();
  }

  async setDoc(collectionName: string, docId: string, data: any) {
    await this.db.doc(`${collectionName}/${docId}`).set(data);
  }

  async saveDoc(collectionName: string, docId: string, data: any) {
    await this.db.doc(`${collectionName}/${docId}`).set(data);
  }

  async deleteDocField(collectionName: string, docId: string, field: string) {
    await this.updateDoc(collectionName, docId, {
      [field]: firebase.firestore.FieldValue.delete(),
    });
  }

  async getDocs(collectionName: string, params: QueryDocsParams) {
    const inFilter = params.filters.find(
      (f) => f.operator == 'in',
    ) as QueryFilter | null;
    const normalFilters = params.filters.filter((f) => f.operator !== 'in');

    if (!inFilter) {
      return this.getDocsData(collectionName, {
        filters: normalFilters,
      });
    }

    const queryChunks = this.getChunks(inFilter.value, 10);

    const chunksResults = queryChunks.map(async (chunk: string[]) => {
      let ref: any = this.getDocsWithFiltersRef(collectionName, {
        filters: normalFilters,
      });

      ref = ref.where(inFilter.field, 'in', chunk);

      const result = await ref.get();

      const chunkDtos = (await result.docs).map((d) => d.data());

      return chunkDtos;
    });

    const results = await Promise.all(chunksResults);
    return results?.flat();
  }

  async saveInBatch(collectionName: string, docs: any[]) {
    const batch = this.db.batch();

    docs.forEach((doc) => {
      batch.set(this.db.collection(collectionName).doc(doc.id), doc);
    });

    await batch.commit();
  }

  getChunks(inputArray, perChunk) {
    const result = inputArray.reduce((resultArray, item, index) => {
      const chunkIndex = Math.floor(index / perChunk);

      if (!resultArray[chunkIndex]) {
        resultArray[chunkIndex] = []; // start a new chunk
      }

      resultArray[chunkIndex].push(item);

      return resultArray;
    }, []);

    return result;
  }

  private async getDocsData(collectionName: string, params: QueryDocsParams) {
    const ref: any = this.getDocsWithFiltersRef(collectionName, params);
    return (await ref.get()).docs.map((doc) => doc.data());
  }

  private getDocsWithFiltersRef(
    collectionName: string,
    params: QueryDocsParams,
  ) {
    let ref: any = this.getDocsRef(collectionName);
    params.filters.forEach((filter) => {
      ref = ref.where(filter.field, filter.operator, filter.value);
    });

    if (params.limit) {
      ref = ref.limit(params.limit);
    }

    if (params.orderBy) {
      ref = ref.orderBy(params.orderBy.field, params.orderBy.direction);
    }

    return ref;
  }
}
