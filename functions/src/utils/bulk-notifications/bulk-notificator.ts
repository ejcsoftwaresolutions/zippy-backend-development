export type BulkNotificationResult = {
  targetSize: number;
  recipients: number;
  deliveryIds: string[];
};

export default interface BulkNotificator {
  sendCampaignNotification(
    targetGroup: string,
    message: {
      id: string;
      title: string;
      description: string;
      imageUrl?: string;
    },
  ): Promise<BulkNotificationResult>;

  sendBulkIndividualNotification(
    targetGroup: string,
    message: {
      id: string;
      title: string;
      description: string;
      imageUrl?: string;
    },
    targetRecipients: any[],
  ): Promise<BulkNotificationResult>;

  getBulkNotificationProcessResult(results: BulkNotificationResult): {
    deliveryIds: string[];
    total: number;
    successful: number;
    failed: number;
  };
}
