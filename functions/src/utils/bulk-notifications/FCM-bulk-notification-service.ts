import firebase from 'firebase-admin';
import { PushNotification } from '../models/push-notification';
import CloudFunctionsAppUtils from '../app';
import BulkNotificator, { BulkNotificationResult } from './bulk-notificator';

export default class FCMBulkNotificationService implements BulkNotificator {
  private registeredVariables = [
    '[[FIRST_NAME]]',
    '[[FULL_NAME]]',
    '[[EMAIL]]',
  ];

  constructor(
    private pushNotificationService: {
      send: any;
    },
    private config: {
      chunkSize?: number;
    },
  ) {}

  public async sendCampaignNotification(
    targetGroup: string,
    message: {
      id: string;
      title: string;
      description: string;
      imageUrl?: string;
    },
  ) {
    const users = await this.findTargetGroupUsers(targetGroup);

    const chunkedUsers = this.getChunks(users, this.config.chunkSize ?? 50);

    const result = await this.sendInChunks(chunkedUsers, {
      id: message.id,
      role: targetGroup,
      title: message.title,
      content: message.description,
      imageUrl: message.imageUrl,
    });

    return result;
  }

  public async sendBulkIndividualNotification(
    targetGroup: string,
    message: {
      id: string;
      title: string;
      description: string;
      imageUrl?: string;
    },
    targetRecipients: any[],
  ) {
    const chunkedUsers = this.getChunks(
      targetRecipients,
      this.config.chunkSize ?? 50,
    );

    const result = await this.sendInChunks(chunkedUsers, {
      id: message.id,
      role: targetGroup,
      title: message.title,
      content: message.description,
      imageUrl: message.imageUrl,
    });

    return result;
  }

  public getBulkNotificationProcessResult(results: BulkNotificationResult) {
    return CloudFunctionsAppUtils.omitUnknown({
      processed: true,
      processedAt: new Date(),
      error: undefined,
      stats: results
        ? (() => {
            return {
              deliveryIds: results.deliveryIds,
              total: results.targetSize,
              successful: results.recipients,
              failed: results.targetSize - results.recipients,
            };
          })()
        : undefined,
    }) as any;
  }

  private async sendInChunks(
    usersChunks: any[],
    message: {
      id: string;
      role: string;
      title: string;
      content: string;
      imageUrl?: string;
    },
  ): Promise<{
    targetSize: number;
    recipients: number;
    deliveryIds: string[];
  }> {
    const delay = (ms: number) => {
      return new Promise<void>((resolve) => setTimeout(resolve, ms));
    };

    const chunkPromises = usersChunks.map((chunk: any[]) => {
      const promises = chunk.map((user) => {
        return this.sendMessage(user, {
          ...message,
          title: this.replaceVariables(user, message.title),
          content: this.replaceVariables(user, message.content),
        });
      });

      return promises;
    });

    const res = chunkPromises.map(async (promises) => {
      return new Promise(async (resolve) => {
        const chunkResults = await Promise.all(promises);
        // wait some time ms
        await delay(500);
        resolve(chunkResults);
      });
    });

    const results = await Promise.all(res);
    const finalResults = results.flat();
    return {
      deliveryIds: finalResults
        .map((r: any) => r.deliveryId)
        .filter((id) => !!id),
      recipients: finalResults.reduce(
        (acc, current: any) => acc + current.recipients,
        0,
      ) as number,
      targetSize: usersChunks.reduce((acc, current) => acc + current.length, 0),
    };
  }

  private sendMessage(
    user: {
      id: string;
      firstName: string;
      lastName: string;
      email: string;
      token: string;
    },
    message: {
      id: string;
      role: string;
      title: string;
      content: string;
      imageUrl?: string;
    },
  ) {
    return new Promise<{
      success: boolean;
      error?: string;
      recipients: number;
      deliveryId?: string;
    }>(async (resolve) => {
      const appId = (() => {
        if (message.role === 'RIDER')
          return process.env.RIDER_APP_EXPERIENCE_ID;
        if (message.role === 'VENDOR')
          return process.env.STORE_APP_EXPERIENCE_ID;
        return process.env.CUSTOMER_APP_EXPERIENCE_ID;
      })();

      const channel = (appId as string).split('/')[1];

      try {
        const res = await this.pushNotificationService.send(
          new PushNotification({
            id: message.id,
            to: user.token as string,
            metadata: {
              appMessageId: message.id,
            },
            title: message.title,
            content: message.content,
            imageUrl: message.imageUrl,
            appId: appId as string,
            channelId: channel,
          }),
        );
        return resolve({
          success: !Boolean(res.error),
          recipients: res.recipients,
          error: res.error,
          deliveryId: res.deliveryId,
        });
      } catch (e: any) {
        return resolve({
          success: false,
          recipients: 0,
          error: e.message,
        });
      }
    });
  }

  private replaceVariables(user: any, text: string): string {
    return text
      .replace('[[FIRST_NAME]]', user.firstName)
      .replace('[[FULL_NAME]]', `${user.firstName} ${user.lastName}`)
      .replace('[[EMAIL]]', user.email);
  }

  private async findTargetGroupUsers(role: string) {
    const query = firebase
      .firestore()
      .collection('users')
      .where('roles', 'array-contains', role)
      .where('status', '==', 'ACTIVE');

    const users = (await query.get()).docs.map((d) => {
      const data = d.data();

      return {
        id: data.id,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
      };
    });

    const tokens = await this.findUsersTokens(
      users.map((u) => u.id),
      role,
    );

    return users
      .map((user, index) => {
        return {
          ...user,
          token: tokens[index],
        };
      })
      .filter((u) => !!u.token);
  }

  private async findUsersTokens(ids: string[], role: string) {
    const queryChunks = this.getChunks(ids, 10);
    const chunksResults = queryChunks.map(async (chunk: string[]) => {
      let ref: any = firebase.firestore().collection('user_push_tokens');

      ref = ref.where('userId', 'in', chunk);

      const result = await ref.get();

      const chunkDtos = (await result.docs).map((d: any) => d.data());

      return chunkDtos;
    });

    const results = await Promise.all(chunksResults);

    return results?.flat().map((tokenData: any) => {
      if (role === 'CUSTOMER') {
        return tokenData?.customer;
      }

      if (role === 'VENDOR') {
        return tokenData?.vendor;
      }

      return tokenData?.rider;
    });
  }

  private getChunks(inputArray: any[], perChunk: number) {
    const result = inputArray.reduce((resultArray, item, index) => {
      const chunkIndex = Math.floor(index / perChunk);

      if (!resultArray[chunkIndex]) {
        resultArray[chunkIndex] = []; // start a new chunk
      }

      resultArray[chunkIndex].push(item);

      return resultArray;
    }, []);

    return result;
  }
}
