import { PushNotification } from '../models/push-notification';
import CloudFunctionsAppUtils from '../app';
import BulkNotificator, { BulkNotificationResult } from './bulk-notificator';

export default class OneSignalBulkNotificationService
  implements BulkNotificator
{
  private registeredVariables = [
    '[[FIRST_NAME]]',
    '[[FULL_NAME]]',
    '[[EMAIL]]',
  ];

  constructor(
    private pushNotificationService: {
      send: any;
    },
    private config: {
      chunkSize?: number;
    },
  ) {}

  public async sendCampaignNotification(
    targetGroup: string,
    message: {
      id: string;
      title: string;
      description: string;
      imageUrl?: string;
      linkUrl?: string;
    },
  ) {
    const finalMessage = {
      id: message.id,
      role: targetGroup,
      title: this.cleanVariables(message.title),
      content: this.cleanVariables(message.description),
      imageUrl: message.imageUrl,
      isSegment: true,
      linkUrl: message.linkUrl,
    };

    const notification = this.buildNotification(
      ['Subscribed Users'],
      finalMessage,
    );

    const res = await this.pushNotificationService.send(notification);

    return {
      deliveryIds: [res.deliveryId],
      recipients: res.recipients,
      targetSize: 0,
    };
  }

  public async sendBulkIndividualNotification(
    targetGroup: string,
    message: {
      id: string;
      title: string;
      description: string;
      imageUrl?: string;
      linkUrl?: string;
    },
    targetRecipients: any[],
  ) {
    const chunkedUsers = this.getChunks(
      targetRecipients,
      this.config.chunkSize ?? 50,
    );

    const result = await this.sendInChunks(chunkedUsers, {
      id: message.id,
      role: targetGroup,
      title: message.title,
      content: message.description,
      imageUrl: message.imageUrl,
      linkUrl: message.linkUrl,
    });

    return result;
  }

  public getBulkNotificationProcessResult(results: BulkNotificationResult) {
    return CloudFunctionsAppUtils.omitUnknown({
      processed: true,
      processedAt: new Date(),
      error: undefined,
      stats: results
        ? (() => {
            return {
              deliveryIds: results.deliveryIds,
              total: results.targetSize,
              successful: results.recipients,
              failed: results.targetSize - results.recipients,
            };
          })()
        : undefined,
    }) as any;
  }

  private async sendInChunks(
    usersChunks: any[],
    message: {
      id: string;
      role: string;
      title: string;
      content: string;
      imageUrl?: string;
      linkUrl?: string;
    },
  ): Promise<{
    targetSize: number;
    recipients: number;
    deliveryIds: string[];
  }> {
    const chunkPromises = usersChunks.map((chunk: any[]) => {
      return this.sendMessage(chunk, {
        ...message,
        title: this.cleanVariables(message.title),
        content: this.cleanVariables(message.content),
      });
    });

    const results = await Promise.all(chunkPromises);
    return {
      deliveryIds: results.map((r: any) => r.deliveryId).filter((id) => !!id),
      recipients: results.reduce(
        (acc, current: any) => acc + current.recipients,
        0,
      ) as number,
      targetSize: usersChunks.reduce((acc, current) => acc + current.length, 0),
    };
  }

  private sendMessage(
    users: {
      id: string;
      firstName: string;
      lastName: string;
      email: string;
      token: string;
    }[],
    message: {
      id: string;
      role: string;
      title: string;
      content: string;
      imageUrl?: string;
      linkUrl?: string;
    },
  ) {
    return new Promise<{
      success: boolean;
      error?: string;
      recipients: number;
      deliveryId?: string;
    }>(async (resolve) => {
      try {
        const notification = this.buildNotification(
          users.map((u) => u.token),
          message,
        );

        const res = await this.pushNotificationService.send(notification);
        return resolve({
          success: !Boolean(res.error),
          recipients: res.recipients,
          error: res.error,
          deliveryId: res.deliveryId,
        });
      } catch (e: any) {
        return resolve({
          success: false,
          recipients: 0,
          error: e.message,
        });
      }
    });
  }

  private buildNotification(to: string[], message: any) {
    const appId = (() => {
      if (message.role === 'RIDER') return process.env.RIDER_APP_EXPERIENCE_ID;
      if (message.role === 'VENDOR') return process.env.STORE_APP_EXPERIENCE_ID;
      return process.env.CUSTOMER_APP_EXPERIENCE_ID;
    })();

    const channel = (appId as string).split('/')[1];

    return new PushNotification({
      id: message.id,
      to: to,
      metadata: {
        appMessageId: message.id,
        isSegment: message.isSegment !== undefined ? message.isSegment : false,
      },
      title: message.title,
      content: message.content,
      imageUrl: message.imageUrl,
      appId: appId as string,
      channelId: message.role === 'CUSTOMER' ? 'ads' : (channel as string),
      linkUrl: message.linkUrl,
    });
  }

  private cleanVariables(text: string): string {
    return text
      .replace('[[FIRST_NAME]]', '{{ first_name | "estimad@" }}')
      .replace('[[FULL_NAME]]', `{{ full_name  | "estimad@"}}`)
      .replace('[[EMAIL]]', '{{ email | ""}}');
  }

  private getChunks(inputArray: any[], perChunk: number) {
    const result = inputArray.reduce((resultArray, item, index) => {
      const chunkIndex = Math.floor(index / perChunk);

      if (!resultArray[chunkIndex]) {
        resultArray[chunkIndex] = []; // start a new chunk
      }

      resultArray[chunkIndex].push(item);

      return resultArray;
    }, []);

    return result;
  }
}
