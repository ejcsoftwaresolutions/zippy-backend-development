import { PushNotification } from '../models/push-notification';
import { NotificationService } from '../notification-service/notification-service';
import OneSignalBulkNotificationService from './one-signal-bulk-notification-service';
import PushNotificator from '../notification-service/push-notificator';
import FCMBulkNotificationService from './FCM-bulk-notification-service';

export function createBulkNotificationService(
  notificationService: PushNotificator,
) {
  const provider = notificationService.getName();

  if (provider !== 'ONE_SIGNAL') {
    return new FCMBulkNotificationService(
      {
        send: (notification: PushNotification) => {
          return notificationService.send(notification);
        },
      },
      {
        chunkSize: 50,
      },
    );
  }

  return new OneSignalBulkNotificationService(
    {
      send: (notification: PushNotification) => {
        return notificationService.send(notification);
      },
    },
    {
      chunkSize: 2000,
    },
  );
}

export const BulkNotificationService =
  createBulkNotificationService(NotificationService);
