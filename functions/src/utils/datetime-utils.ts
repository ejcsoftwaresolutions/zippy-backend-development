import moment = require('moment');
import { Moment } from 'moment';

const DateTimeUtils = {
  format: (date: Date, format: string, utc = true) => {
    if (utc) return moment(date).utc().format(format);
    return moment(date).format(format);
  },
  differenceInMinutes: (a: Date, b: Date): number => {
    return moment(a).diff(moment(b), 'minutes');
  },
  differenceInMinutesMoment: (a: Moment, b: Moment): number => {
    return a.diff(b, 'minutes');
  },
  fromTime(time: string, format = 'HH:mm') {
    return moment(time, format).toDate();
  },
};

export default DateTimeUtils;
