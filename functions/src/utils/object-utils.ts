import {
    isEqual as LisEqual,
    isNil,
    isNull as LIsNull,
    mapValues as LMapValues,
    merge as LLMerge,
    omit as LOmit,
    omitBy,
    pick as LPick
} from 'lodash';
import fnGet from 'lodash/get';

const ObjectUtils = {
    get: fnGet,
    isNull: LIsNull,
    omit: LOmit,
    mapValues: LMapValues,
    omitUnknown: (object: any) => {
        return omitBy(object, isNil);
    },
    pick: LPick,
    merge: LLMerge,
    isEqual: LisEqual,
    sort(obj: any) {
        return Object.keys(obj)
        .sort((e: string, i: string) => {
            return e > i ? 0 : 1;
        })
        .reduce(function(result, key) {
            // @ts-ignore
            result[key] = obj[key];
            return result;
        }, {});
    }
};

export default ObjectUtils;
