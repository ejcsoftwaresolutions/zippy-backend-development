import OneSignalNotificationService from './one-signal-notification-service';
import FCMNotificationService from './FCM-notification-service';

export function createNotificationService(defaultProvider = 'ONE_SIGNAL') {
  const provider =
    /*process.env.NODE_ENV === 'production' ? 'FCM' : */ 'ONE_SIGNAL';

  if (provider !== 'ONE_SIGNAL') {
    return new FCMNotificationService(
      process.env.CLOUD_SERVER_KEY as string,
      process.env.FB_PROJECT_ID as string,
      process.env.GOOGLE_APPLICATION_CREDENTIALS as string,
    );
  }

  return new OneSignalNotificationService({
    authKey: process.env.ONE_SIGNAL_AUTH_KEY as string,
    apps: {
      CUSTOMER: {
        id: process.env.ONE_SIGNAL_CUSTOMER_ID as string,
        apiKey: process.env.ONE_SIGNAL_CUSTOMER_KEY as string,
        channels: {
          android: process.env.CUSTOMER_ANDROID_CHANNELS
            ? (process.env.CUSTOMER_ANDROID_CHANNELS as string)
                .split(',')
                .map((ch) => {
                  const channel = ch.split('*');
                  return {
                    id: channel[1].trim(),
                    name: channel[0].trim(),
                  };
                })
            : [],
          ios: [
            {
              id: 'notification.wav',
              name: 'zippi-market',
            },
            {
              id: 'ads.wav',
              name: 'ads',
            },
          ],
        },
      },
      RIDER: {
        id: process.env.ONE_SIGNAL_RIDER_ID as string,
        apiKey: process.env.ONE_SIGNAL_RIDER_KEY as string,
        channels: {
          android: process.env.RIDER_ANDROID_CHANNELS
            ? (process.env.RIDER_ANDROID_CHANNELS as string)
                .split(',')
                .map((ch) => {
                  const channel = ch.split('*');
                  return {
                    id: channel[1].trim(),
                    name: channel[0].trim(),
                  };
                })
            : [],
          ios: [
            {
              id: 'notification.wav',
              name: 'zippi-rider-app' /*announcements*/,
            },
            {
              id: 'new_order.wav',
              name: 'new-order',
            },
            {
              id: 'order_update.wav',
              name: 'order-update',
            },
          ],
        },
      },
      VENDOR: {
        id: process.env.ONE_SIGNAL_VENDOR_ID as string,
        apiKey: process.env.ONE_SIGNAL_VENDOR_KEY as string,
        channels: {
          android: process.env.VENDOR_ANDROID_CHANNELS
            ? (process.env.VENDOR_ANDROID_CHANNELS as string)
                .split(',')
                .map((ch) => {
                  const channel = ch.split('*');
                  return {
                    id: channel[1].trim(),
                    name: channel[0].trim(),
                  };
                })
            : [],
          ios: [
            {
              id: 'notification.wav',
              name: 'zippi-vendor-app' /*announcements*/,
            },
            {
              id: 'new_order.wav',
              name: 'new-order',
            },
            {
              id: 'order_update.wav',
              name: 'order-update',
            },
          ],
        },
      },
    },
  });
}

export const NotificationService = createNotificationService();
