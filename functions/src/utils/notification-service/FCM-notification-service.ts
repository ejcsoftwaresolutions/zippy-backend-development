import { JWT } from 'google-auth-library';
import { PushNotification } from '../models/push-notification';
import PushNotificator, {
  PushNotificatorDeliveryResult,
  PushNotificatorStats,
} from './push-notificator';

const axios = require('axios');
export default class FCMNotificationService implements PushNotificator {
  constructor(
    private serverKey: string,
    private projectId: string,
    private serviceAccountLocation: string,
  ) {}

  getName(): string {
    return 'FCM';
  }

  async send(
    notification: PushNotification,
  ): Promise<PushNotificatorDeliveryResult> {
    const url = `https://fcm.googleapis.com/v1/projects/${this.projectId}/messages:send`;

    const token = await this.getAccessToken();

    const notificationData = notification.toPrimitives();
    const imageUrl = notificationData.imageUrl;

    try {
      const result = await axios.post(
        url,
        {
          message: {
            token: notificationData.to,
            data: {
              experienceId: notificationData.appId,
              title: notificationData.title,
              message: notificationData.content,
              metadata: JSON.stringify(notificationData.metadata ?? {}),
            },
            notification: {
              title: notificationData.title,
              body: notificationData.content,
              ...(imageUrl
                ? {
                    image: imageUrl,
                  }
                : {}),
            },
            android: {
              priority: 'high',
              notification: {
                channel_id: notificationData?.channelId,
                notification_count:
                  parseInt(notificationData.metadata?.badge) ?? undefined,
              },
            },
            apns: {
              headers: {
                'apns-priority': '10',
              },
              payload: {
                aps: {
                  sound: 'notification.wav',
                  ...(imageUrl ? { 'mutable-content': 1 } : {}),
                },
                alert: {
                  title: notificationData.title,
                  body: notificationData.content,
                  sound: 'notification.wav',
                },
              },
              ...(imageUrl
                ? {
                    fcm_options: {
                      image: imageUrl,
                    },
                  }
                : {}),
            },
          },
        },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token,
          },
        },
      );

      return {
        ok: true,
        recipients: 1,
        metadata: result.data,
        /* deliveryId: result.data.id,*/
      };
    } catch (e: any) {
      return {
        ok: false,
        recipients: 0,
        error: e.response.data.error,
      };
    }
  }

  async getNotificationStats(
    id: string,
    targetApp: string,
  ): Promise<PushNotificatorStats> {
    return Promise.resolve({
      audience: 0,
      failed: 0,
      successful: 0,
      clicks: 0,
    });
  }

  private async getAccessToken() {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-var-requires

      /*  const key = require('../../../' + this.serviceAccountLocation);*/
      const jwtClient = new JWT(
        undefined,
        this.serviceAccountLocation,
        undefined,
        ['https://www.googleapis.com/auth/firebase.messaging'],
        undefined,
      );
      jwtClient.authorize(function (err, tokens) {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens?.access_token);
      });
    });
  }
}
