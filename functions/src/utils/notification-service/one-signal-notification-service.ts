import PushNotificator, {
  PushNotificatorDeliveryResult,
  PushNotificatorStats,
} from './push-notificator';
import { PushNotification } from '../models/push-notification';
import { ObservableDefaultApi } from '@onesignal/node-onesignal/types/ObservableAPI';
import Id from '../models/id';
import { Notification as OneSignalNotification } from '@onesignal/node-onesignal';

const functions = require('firebase-functions');
const OneSignal = require('@onesignal/node-onesignal');

type PushNotificationChannels = {
  android: { id: string; name: string }[];
  ios: { id?: string; name: string }[];
};

type PushNotificationApp = {
  id: string;
  apiKey: string;
  channels: PushNotificationChannels;
};

export default class OneSignalNotificationService implements PushNotificator {
  private authKey: string;
  private client: ObservableDefaultApi | undefined;
  private apps: { [name: string]: PushNotificationApp } = {};

  constructor(params: {
    authKey: string;
    apps: { [name: string]: PushNotificationApp };
  }) {
    this.apps = params.apps;
    this.authKey = params.authKey;
  }

  getName(): string {
    return 'ONE_SIGNAL';
  }

  async send(
    notification: PushNotification,
  ): Promise<PushNotificatorDeliveryResult> {
    await this.configureClient(notification.appName);

    const osNotification = await this.buildBaseNotification(notification);

    const finalRecipients =
      typeof notification.to === 'string' ? [notification.to] : notification.to;

    if (notification.isSegment) {
      osNotification.included_segments = finalRecipients;
    } else if (notification.isExternalId) {
      osNotification.include_external_user_ids = finalRecipients;
    } else {
      osNotification.include_player_ids = finalRecipients;
    }

    if (!this.client) throw new Error('Client not initialized');

    try {
      const res: any = await this.client.createNotification(osNotification);

      return {
        ok: true,
        recipients: res.recipients,
        metadata: res,
        deliveryId: res.id,
      };
    } catch (e: any) {
      functions.logger.log('Error al enviar push notification', e);
      return {
        ok: false,
        recipients: 0,
        error: e.message,
      };
    }
  }

  async getNotificationStats(
    id: string,
    targetApp: string,
  ): Promise<PushNotificatorStats> {
    await this.configureClient(targetApp);
    const appId = await this.getAppId(targetApp);
    if (!this.client) throw new Error('Client not initialized');

    const notification: any = await this.client.getNotification(appId, id);

    if (!notification) {
      return {
        audience: 0,
        failed: 0,
        successful: 0,
        clicks: 0,
      };
    }

    const failed = (notification.failed ?? 0) + (notification.errored ?? 0);
    const successful = notification.successful ?? 0;

    return {
      audience: successful + failed,
      failed: failed,
      successful: successful,
      clicks: notification.converted ?? 0,
    };
  }

  private async getAppId(appName: string) {
    return this.apps[appName]?.id;
  }

  private async getAppKey(appName: string) {
    return this.apps[appName]?.apiKey;
  }

  private async configureClient(appName: string) {
    const appKey = await this.getAppKey(appName);
    if (!appKey) {
      throw new Error('One signal App Key not found');
    }

    const configuration = OneSignal.createConfiguration({
      authMethods: {
        user_key: {
          tokenProvider: {
            getToken: () => {
              return this.authKey;
            },
          },
        },
        app_key: {
          tokenProvider: {
            getToken: () => {
              return appKey;
            },
          },
        },
      },
    });

    this.client = new OneSignal.DefaultApi(configuration);
  }

  private async buildBaseNotification(
    notification: PushNotification,
  ): Promise<OneSignalNotification> {
    const notificationData = notification.toPrimitives();
    const appId = await this.getAppId(notification.appName);
    if (!appId) {
      throw new Error('One signal App Id not found');
    }

    const appChannels = this.apps[notification.appName].channels;

    const osNotification = new OneSignalNotification();

    osNotification.app_id = appId;
    osNotification.headings = {
      en: notificationData.title,
      es: notificationData.title,
    };
    osNotification.contents = {
      en: notificationData.content,
      es: notificationData.content,
    };

    osNotification.priority = 10;
    osNotification.big_picture = notificationData.imageUrl;
    osNotification.huawei_big_picture = notificationData.imageUrl;
    osNotification.ios_attachments = notificationData.imageUrl
      ? { [new Id().value]: notificationData.imageUrl }
      : undefined;

    osNotification.small_icon = 'notification';
    osNotification.huawei_small_icon = 'notification';

    osNotification.data = notificationData.metadata;
    osNotification.android_channel_id = appChannels.android.find(
      (ch) => ch.name === notificationData.channelId,
    )?.id;

    osNotification.huawei_channel_id = appChannels.android.find(
      (ch) => ch.name === notificationData.channelId,
    )?.id;

    osNotification.ios_sound = appChannels.ios.find(
      (ch) => ch.name === notificationData.channelId,
    )?.id;

    osNotification.content_available = true;
    osNotification.url = notificationData.linkUrl;

    return osNotification;
  }
}
