import { ArrayUtils } from '../array-utils';
import ObjectUtils from '../object-utils';
import firebase from 'firebase-admin';

export default function recalculateProduct(product: any) {
  if (!product) return undefined;

  const hasOptions = product.options?.length > 0 ?? false;

  const optionsWithDiscounts = hasOptions
    ? (product.options ?? []).filter((o: any) => !!o.applyDiscount)
    : [];

  const hasDiscount = () => {
    return hasOptions
      ? optionsWithDiscounts.length > 0
      : !!product.discountRate && !!product.applyDiscount;
  };

  const lowestOptionPrice = () => {
    if (!hasOptions) {
      return firebase.firestore.FieldValue.delete();
    }

    return (
      ArrayUtils.orderBy(product.options ?? [], 'price', 'asc')[0]?.price ?? 0
    );
  };

  const lowestOptionDiscountPrice = () => {
    if (!hasOptions) {
      return firebase.firestore.FieldValue.delete();
    }

    if (!hasDiscount()) return firebase.firestore.FieldValue.delete();

    return (
      ArrayUtils.orderBy(optionsWithDiscounts, 'discountPrice', 'asc')[0]
        ?.discountPrice ?? 0
    );
  };

  const highestOptionDiscountRate = () => {
    if (!hasOptions) {
      return firebase.firestore.FieldValue.delete();
    }

    if (!hasDiscount()) return firebase.firestore.FieldValue.delete();

    return (
      ArrayUtils.orderBy(optionsWithDiscounts, 'discountRate', 'desc')[0]
        ?.discountRate ?? 0
    );
  };

  const thumbnailUrl = () => {
    const defaultPhoto =
      product.photo ?? firebase.firestore.FieldValue.delete();
    if (!product.photos) return defaultPhoto;
    if (product.photos.length == 0) return defaultPhoto;

    const firstPhoto = product.photos[0];
    if (!firstPhoto.versions) return firstPhoto.url;
    const thumbnailVersion = firstPhoto.versions.find(
      (v: any) => v.id == 'THUMBNAIL',
    );

    return thumbnailVersion?.url ?? firstPhoto.url;
  };

  return ObjectUtils.omitUnknown({
    hasDiscount: hasDiscount(),
    hasOptions: hasOptions,
    highestOptionDiscountRate: highestOptionDiscountRate(),
    lowestOptionPrice: lowestOptionPrice(),
    lowestOptionDiscountPrice: lowestOptionDiscountPrice(),
    thumbnailUrl: thumbnailUrl(),
  });
}
