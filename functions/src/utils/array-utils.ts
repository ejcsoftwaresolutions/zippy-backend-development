import {
  differenceWith as fnDifferenceWith,
  filter as fnFilter,
  findIndex as fnFindIndex,
  flattenDeep as fnFlatten,
  isArray as fnIsArray,
  map as fnMap,
  orderBy as fnOrderBy,
  uniq as fnUniq,
  uniqBy as fnUniqBy,
} from 'lodash';

export namespace ArrayUtils {
  export const isArray = fnIsArray;
  export const filter = fnFilter;
  export const differenceWith = fnDifferenceWith;
  export const orderBy = fnOrderBy;
  export const uniq = fnUniq;
  export const uniqBy = fnUniqBy;
  export const map = fnMap;
  export const flatten = fnFlatten;
  export const findIndex = fnFindIndex;
  export const filterLike = (arr1: any[], fieldName: string, like: string) => {
    return arr1.filter((dto) => {
      const name = dto[fieldName] ?? '';
      const test = like;
      const reg1 = new RegExp('^' + test + '.*$', 'i');
      const reg2 = new RegExp('^.*' + test + '$', 'i');
      const regex3 = new RegExp([test].join('|'), 'i');

      const match = name.match(reg1);
      const match2 = name.match(reg2);
      const match3 = name.includes(test);
      const match4 = regex3.test(name);

      return !!match || !!match2 || !!match3 || !!match4;
    });
  };
}
