import admin from 'firebase-admin';
import {
  CUSTOMERS,
  PUSH_TOKENS,
  RIDERS,
  USERS,
  VENDOR_LOCATIONS,
  VENDOR_ORDERS,
  VENDORS,
} from '../constants';
import {
  PushNotification,
  PushNotificationParams,
} from './models/push-notification';
import { isNil, omitBy as fnOmitBy } from 'lodash';
import Id from './models/id';
import { NotificationService } from './notification-service/notification-service';

const functions = require('firebase-functions');
const geoFire = require('geofire');

const CloudFunctionsAppUtils = {
  async findAccount(id: string) {
    const item = await admin.firestore().doc(`${USERS}/${id}`).get();

    const itemDto = item.data();

    if (!itemDto) return;

    return itemDto;
  },
  async findCustomer(id: string) {
    const item = await admin.firestore().doc(`${CUSTOMERS}/${id}`).get();

    const itemDto = item.data();

    if (!itemDto) return;

    return itemDto;
  },
  async findVendor(id: string) {
    const item = await admin.firestore().doc(`${VENDORS}/${id}`).get();

    const itemDto = item.data();

    if (!itemDto) return;

    return itemDto;
  },

  async findRider(id: string) {
    const item = await admin.firestore().doc(`${RIDERS}/${id}`).get();

    const itemDto = item.data();

    if (!itemDto) return;

    return itemDto;
  },
  async findPushTokens(userId: string) {
    const item = await admin.firestore().doc(`${PUSH_TOKENS}/${userId}`).get();

    const itemDto = item.data();

    if (!itemDto) return;

    return itemDto;
  },
  async getOrder(id: string) {
    const orderQ = await admin
      .firestore()
      .collection(VENDOR_ORDERS)
      .where('id', '==', id)
      .get();

    const order = orderQ.docs.map((x) => x.data())?.[0] as any;
    return order;
  },
  async findUserNotificationInfo(
    userId: string,
    role: 'customer' | 'vendor' | 'rider',
  ): Promise<{
    pushToken?: string;
    email?: string;
    configurations?: {
      referralPromotions: boolean;
      receiveEmails: boolean;
      receivePushNotifications: boolean;
      receiveSMS: boolean;
    };
  }> {
    const pushTokens = await CloudFunctionsAppUtils.findPushTokens(userId);
    const user = await CloudFunctionsAppUtils.findAccount(userId);

    const configurations = await (async () => {
      if (role === 'rider')
        return (await this.findRider(userId))?.configurations;
      if (role === 'vendor')
        return (await this.findVendor(userId))?.configurations;
      return (await this.findCustomer(userId))?.configurations;
    })();

    return {
      pushToken: pushTokens?.[role],
      email: user?.email,
      configurations,
    };
  },
  async sendEmail(to: string, message: { subject: any; text: any; html: any }) {
    if (process.env.NODE_ENV !== 'production') return;

    const emails = to.split(',');
    const promises = emails.map((email) =>
      admin.firestore().collection('mail').add({
        to: email.trim(),
        message: message,
      }),
    );

    return Promise.all(promises);
  },
  async sendPushNotification(
    pushNotification: PushNotificationParams,
    role: 'RIDER' | 'VENDOR' | 'CUSTOMER',
    userId: string,
  ): Promise<any> {
    functions.logger.log('Intentando enviar un push notification', {
      pushNotification: pushNotification,
      role,
    });

    const { pushToken, configurations } =
      await CloudFunctionsAppUtils.findUserNotificationInfo(
        userId,
        role.toLowerCase() as any,
      );

    if (!pushToken) {
      functions.logger.log('Push token not found', {
        userId: userId,
        role,
      });
      return Promise.resolve();
    }

    if (!configurations?.receivePushNotifications) {
      functions.logger.log('Push notifications disabled', {
        userId: userId,
        role,
      });
      return Promise.resolve();
    }

    const appId = (() => {
      if (role === 'RIDER') return process.env.RIDER_APP_EXPERIENCE_ID;
      if (role === 'VENDOR') return process.env.STORE_APP_EXPERIENCE_ID;
      return process.env.CUSTOMER_APP_EXPERIENCE_ID;
    })();

    if (!appId) return Promise.resolve();

    const defaultChannel = (appId as string).split('/')[1];
    const channel = pushNotification.channel ?? defaultChannel;

    const service = await NotificationService;

    try {
      return await service.send(
        new PushNotification({
          id: new Id().value,
          to: pushToken,
          imageUrl: pushNotification.notification.image,
          metadata: pushNotification.data ?? {},
          channelId:
            service.getName() === 'ONE_SIGNAL' ? channel : defaultChannel,
          content: pushNotification.notification.body,
          title: pushNotification.notification.title,
          appId: appId,
        }),
      );
    } catch (e: any) {
      functions.logger.log(
        'Error al enviar push notification',
        e.response.data.error,
      );
      throw new Error(e);
    }
  },
  async createId() {
    return admin.firestore().collection('test').doc().id;
  },

  async syncVendorGeolocation(vendorId: string, location: number[]) {
    const vendorLocationRef = admin
      .database()
      .ref(`${VENDOR_LOCATIONS}/${vendorId}`);
    const snap = await vendorLocationRef.get();

    if (snap.exists()) {
      return Promise.resolve();
    }

    const geoFireInstance = new geoFire.GeoFire(
      admin.database().ref(VENDOR_LOCATIONS),
    );

    return geoFireInstance.set(vendorId, location);
  },
  async findAdminAccount(id?: string) {
    if (id) {
      const item = await admin.firestore().collection(`admin`).doc(id).get();

      const itemDto = item.data();

      if (!itemDto) return;

      return itemDto;
    }

    const item = await admin.firestore().collection(`admin`).limit(1).get();

    const itemDto = item.docs.map((c) => c.data())[0];

    if (!itemDto) return;

    return itemDto;
  },
  async getAppDeliveryServiceConfigurations() {
    const configs = await admin
      .firestore()
      .collection(`app_configurations`)
      .doc('RIDER_DELIVERY_CONFIGURATIONS')
      .get();

    const itemDto = configs.data();

    if (!itemDto) return;

    return itemDto;
  },
  omitUnknown(object: any) {
    return fnOmitBy(object, isNil);
  },
};

export default CloudFunctionsAppUtils;
