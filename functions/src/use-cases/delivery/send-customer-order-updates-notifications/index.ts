import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';

import CloudFunctionsAppUtils from '../../../utils/app';
import { PushNotificationParams } from '../../../utils/models/push-notification';

const functions = require('firebase-functions');
import DocumentSnapshot = firestore.DocumentSnapshot;

/*const fbadmin = require('firebase-admin');*/
export const sendCustomerOrderUpdatesNotifications = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const document = change.after.data();
  const oldDocument = change.before.data();

  if (!document) return Promise.resolve();
  /* if (!oldDocument) return Promise.resolve();*/
  if (document.status === oldDocument?.status)
    return Promise.resolve(); /*No status change*/

  const STATUS_TO_LISTEN_TO = getNotificationsTemplates();

  if (
    ![...Object.keys(STATUS_TO_LISTEN_TO), 'Driver Not Found'].includes(
      document.status,
    )
  )
    return Promise.resolve(); /*Not interested thank you and good night*/

  functions.logger.log('New order status update', context.params.documentId);
  const { email, configurations } =
    await CloudFunctionsAppUtils.findUserNotificationInfo(
      document.customer.id,
      'customer',
    );

  if (
    document.status === 'Order Accepted' &&
    oldDocument?.status === 'Driver Not Found'
  ) {
    return Promise.resolve(); /* Send notifications just for the first try, no retries. Put this cleaner later */
  }

  const notificationData = (STATUS_TO_LISTEN_TO as any)?.[
    document.status as any
  ]?.getPushNotificationData?.({ order: document });

  const emailData = (STATUS_TO_LISTEN_TO as any)?.[
    document.status as any
  ]?.getEmailData?.({
    order: document,
  });

  if (notificationData) {
    try {
      await CloudFunctionsAppUtils.sendPushNotification(
        {
          ...notificationData,
        },
        'CUSTOMER',
        document.customer.id,
      );
    } catch (e) {
      /*silent*/
    }
  }

  if (emailData && email && configurations?.receiveEmails) {
    try {
      await CloudFunctionsAppUtils.sendEmail(email, emailData);
    } catch (e) {
      /*silent*/
    }
  }

  if (document.status === 'Order Rejected') {
    try {
      await sendAdminOrderRejectedEmail(document);
    } catch (e) {
      /*silent*/
    }
  }

  if (document.status === 'Driver Not Found') {
    try {
      await sendAdminOrderRiderNotFoundEmail(document);
      /*    await subscribeAdminForVendorDeliveryAreaChangesEvents(document);*/
    } catch (e: any) {
      /*silent*/
    }
  }

  if (document.status === 'Order Placed') {
    try {
      await sendAdminNewOrderEmail(document);
      /*    await subscribeAdminForVendorDeliveryAreaChangesEvents(document);*/
    } catch (e: any) {
      /*silent*/
    }
  }
};

function getNotificationsTemplates() {
  return {
    'Order Placed': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        if (!data.order.paymentReport) {
          return undefined;
        }

        if (!data.order.paymentReport.validated) {
          return undefined;
        }

        return {
          notification: {
            title: `Ya hemos verificado tu pago`,
            body: `El establecimiento pronto procesará tu pedido`,
          },
        };
      },
    },
    'In Transit': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        return {
          notification: {
            title: `Tu Zipper ha partido a tu dirección`,
            body: `Tu pedido está casi en tus manos, tu Zipper va en camino!`,
          },
        };
      },
    },
    'Order Completed': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        return {
          notification: {
            title: `Orden completada`,
            body:
              data.order?.deliveryMethod !== 'PICK_UP'
                ? `Ayudanos calificando tu compra. Gracias por usar Zippi Market!`
                : `Gracias por usar Zippi Market!`,
          },
        };
      },
    },
    'Driver Accepted': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        return {
          notification: {
            title: `Tu pedido ya tiene un Zipper!`,
            body: `El Zipper ${data.order.driver.firstName} ${
              data.order.driver.lastName ?? ''
            } ha aceptado recoger y llevarte tu pedido!`,
          },
        };
      },
    },
    'Arrived to Store': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        return {
          notification: {
            title: `Tu Zipper ya ha llegado al establecimiento`,
            body: `Pronto irá a entregar tu pedido`,
          },
        };
      },
    },
    'Order Accepted': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        return {
          notification: {
            title: `El establecimiento ha confirmado tu pedido`,
            body:
              data.order?.deliveryMethod !== 'PICK_UP'
                ? `Se esta procesando tu compra`
                : 'Se esta procesando tu compra, te avisaremos cuando ya este listo para recoger',
          },
        };
      },
    },
    'Ready For Pickup': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        return {
          notification: {
            title: `Tu pedido esta listo para recoger`,
            body: `Ya puedes ir a recoger tu pedido en las instalaciones de ${
              data.order?.vendor?.name ?? 'la tienda'
            }`,
          },
        };
      },
    },
    'Order Rejected': {
      getPushNotificationData: (data: {
        order: any;
      }): PushNotificationParams | undefined => {
        return {
          notification: {
            title: `El establecimiento ha cancelado tu pedido`,
            body: `Nuestro equipo está revisando los detalles, pronto te contactaremos`,
          },
        };
      },
      getEmailData: (data: { order: any }) => {
        return {
          subject: `Zippi - El establecimiento ha cancelado tu pedido #${data.order.code}`,
          text: `Zippi - El establecimiento ha cancelado tu pedido #${data.order.code}`,
          html: `
          <div>
            <p>
                  Hola ${data.order.customer.firstName}, el establecimiento ${data.order.vendor.name} ha cancelado tu pedido #${data.order.code}. 
            </p>
            <br/>
            <p>
              No te preocupes, nuestro equipo está revisando los detalles y pronto te contactaremos.
            </p>
            <p> <strong>Gracias por tu confianza.</strong> </p>
          </div>
          `,
        };
      },
    },
  };
}

async function sendAdminOrderRejectedEmail(document: any) {
  const admin = await CloudFunctionsAppUtils.findAdminAccount();

  if (!admin || !admin.config?.notificationsEmail) return;

  const allEmails = ((admin.config?.notificationsEmail as string) +
    ',' +
    process.env.ZIPPI_PAYMENTS_EMAIL) as string;

  await CloudFunctionsAppUtils.sendEmail(allEmails, {
    subject: `${
      process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'
    } - Se ha cancelado la orden ${document.id}`,
    text: `Se ha cancelado la orden ${document.id}`,
    html: `
         <div>
            <p>
                  Establecimiento: ${document.vendor.name}  . ${
      document.vendor.phone ?? ''
    } 
            </p>
             <p>
                  Cliente: ${document.customer.firstName} ${
      document.customer.lastName
    } - ${document.customer.id}.  ${document.customer.email} ${
      document.customer.phone
    }
            </p>
           <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                  Total: $${document.total}
             </p>
          </div>
        `,
  });
}

async function sendAdminOrderRiderNotFoundEmail(document: any) {
  const admin = await CloudFunctionsAppUtils.findAdminAccount();

  if (!admin || !admin.config?.notificationsEmail) return;

  await CloudFunctionsAppUtils.sendEmail(
    admin.config?.notificationsEmail as string,
    {
      subject: `${
        process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'
      } - El establecimiento ${
        document.vendor.name
      } no ha podido encontrar Zipper para la orden #${document.code}`,
      text: `No se han encontrado Zippers cercanos a la tienda ${document.vendor.name} orden ${document.id}`,
      html: `
         <div>
            <p>
                  Establecimiento: ${document.vendor.name}  . ${
        document.vendor.phone ?? ''
      } 
            </p>
             <p>
                  Cliente: ${document.customer.firstName} ${
        document.customer.lastName
      } - ${document.customer.id}.  ${document.customer.email} ${
        document.customer.phone
      }
            </p>
           <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                  Total: $${document.total}
             </p>
          </div>
        `,
    },
  );
}

async function sendAdminNewOrderEmail(document: any) {
  const admin = await CloudFunctionsAppUtils.findAdminAccount();

  if (!admin || !admin.config?.notificationsEmail) return;

  await CloudFunctionsAppUtils.sendEmail(
    admin.config?.notificationsEmail as string,
    {
      subject: `${
        process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'
      } - Nueva orden #${document.code} 😎🐆`,
      text: `Nueva orden #${document.code} 😎🐆`,
      html: `
         <div>
            <p>
                  Establecimiento: ${document.vendor.name}  . ${
        document.vendor.phone ?? ''
      } 
            </p>
             <p>
                  Cliente: ${document.customer.firstName} ${
        document.customer.lastName
      } - ${document.customer.id}.  ${document.customer.email} ${
        document.customer.phone
      }
            </p>
             <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                  Total: $${document.total}
             </p>
          </div>
        `,
    },
  );
}
