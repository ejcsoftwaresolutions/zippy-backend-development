import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');

const sendNewVendorOrderPushNotification = async (snap: any, context: any) => {
  functions.logger.log(
    'Sending vendor new order notification',
    context.params.documentId,
  );
  const order = snap.data();

  await CloudFunctionsAppUtils.sendPushNotification(
    {
      data: {
        orderId: context.params.documentId,
        vendorId: order.vendor.id,
      },
      notification: {
        title: `Nueva orden #${order.code}`,
        body: `Tienes una nueva orden en tu tienda`,
      },
      channel: 'new-order',
    },
    'VENDOR',
    order.vendor.id,
  );
};

export default sendNewVendorOrderPushNotification;
