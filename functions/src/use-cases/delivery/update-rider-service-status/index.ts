import { Change, EventContext } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import {
  AVAILABLE_RIDERS,
  CONNECTED_RIDERS,
  RIDERS_LOCATIONS,
} from '../../../constants';
import DocumentSnapshot = firestore.DocumentSnapshot;

const admin = require('firebase-admin');
const functions = require('firebase-functions');
export const updateRiderServiceStatus = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const rider = change.after.data();
  if (!rider) return Promise.resolve();
  const isOnline = rider.isOnline;

  functions.logger.log('Updating rider status', context.params.documentId);
  functions.logger.log('rider', rider);
  await admin
    .database()
    .ref(`${CONNECTED_RIDERS}/${context.params.documentId}`)
    .set(isOnline ? new Date().toUTCString() : null);

  if (!isOnline) {
    await admin
      .database()
      .ref(`${RIDERS_LOCATIONS}/${context.params.documentId}`)
      .set(null);

    await admin
      .database()
      .ref(`${AVAILABLE_RIDERS}/${context.params.documentId}`)
      .set(null);
  }

  return Promise.resolve();
};
