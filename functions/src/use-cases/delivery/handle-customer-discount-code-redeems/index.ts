import { Change, EventContext } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import { DISCOUNT_CODES_REDEEMS } from '../../../constants';

import DocumentSnapshot = firestore.DocumentSnapshot;

const admin = require('firebase-admin');

export const handleCustomerDiscountCodeRedeems = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const order = change.after.data();
  if (!order) return Promise.resolve();
  if (!order.discountCode) return Promise.resolve();

  const customerId = order.customer.id;

  const ref = admin
    .firestore()
    .collection(DISCOUNT_CODES_REDEEMS)
    .where('userId', '==', customerId)
    .where('orderId', '==', order.id)
    .where('code.id', '==', order.discountCode.id);

  const existing = (await ref.get()).docs.length > 0;

  if (existing) {
    if (order.status === 'Order Rejected') {
      return ref.ref.remove();
    }

    return Promise.resolve();
  }

  await admin.firestore().collection(DISCOUNT_CODES_REDEEMS).add({
    userId: customerId,
    code: order.discountCode,
    orderId: order.id,
    redeemedAt: new Date(),
  });
};
