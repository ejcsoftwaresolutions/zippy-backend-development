import { Change, EventContext } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import {
  BUSY_RIDERS,
  DELIVERY_ORDERS,
  DELIVERY_SERVICE_STATUS,
  DRIVERS_ACTIVITIES,
  RIDERS_ACTIVE_ORDERS,
} from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');
const admin = require('firebase-admin');

import DocumentSnapshot = firestore.DocumentSnapshot;

export const updateDeliveryServiceStatus = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const delivery = (
    await admin
      .firestore()
      .collection(DELIVERY_ORDERS)
      .doc(context.params.documentId)
      .get()
  )?.data(); // change.after.data();

  const after = change.after.data();
  const last = change.before.data();

  if (!delivery && last) {
    return admin
      .database()
      .ref(`${DELIVERY_SERVICE_STATUS}/${last.id}`)
      .set(null);
  }

  if (!delivery) return Promise.resolve();

  if (last && delivery.status === last.status)
    return Promise.resolve(); /*No changes*/

  const deliveryId = delivery.id;
  const status = delivery.status;

  try {
    await sendVendorNotification(delivery, after?.status);
  } catch (e: any) {
    functions.logger.log('Vendor notification NOT SENT!', e.message);
  }

  await admin
    .database()
    .ref(`${RIDERS_ACTIVE_ORDERS}/${delivery.driverInfo.id}`)
    .update({
      [deliveryId]:
        status !== 'DELIVERED'
          ? {
              id: delivery.id,
              code: delivery?.orderDetails?.code ?? '',
              status: status,
              vendorName: delivery?.orderDetails?.store?.name ?? '',
            }
          : null,
    });

  await admin
    .database()
    .ref(`${DELIVERY_SERVICE_STATUS}/${deliveryId}`)
    .set(
      status !== 'DELIVERED'
        ? {
            status: status,
            orderId: deliveryId,
            customer: delivery.customerInfo,
            rider: delivery.driverInfo,
            vendor: delivery.orderDetails?.store,
            lastUpdate: new Date().toUTCString(),
          }
        : null,
    );

  if (!last && status == 'ACCEPTED' && delivery.assigned) {
    await CloudFunctionsAppUtils.sendPushNotification(
      {
        notification: {
          title: 'Pedido asignado',
          body: 'Te han asignado un nuevo pedido',
        },
        channel: 'new-order',
      },
      'RIDER',
      delivery.driverInfo.id,
    );
  }

  if (status == 'DELIVERED') {
    await handleDeliveryCompleted(delivery);
  }
};

async function handleDeliveryCompleted(delivery: any) {
  try {
    await finishRiderLog(delivery);
  } catch (e: any) {
    functions.logger.log('Error liberar al rider', e.message);
  }

  const riderActiveDeliveries = await getRiderActiveDeliveries(
    delivery.driverInfo.id,
  );

  try {
    if (!riderActiveDeliveries || riderActiveDeliveries?.length == 0) {
      await freeBusyRider(delivery.driverInfo.id);
    }
  } catch (e) {}

  try {
    return await deleteOrderChat(delivery);
  } catch (e: any) {
    functions.logger.log('Error al eliminar chat', e.message);
  }
}

async function sendVendorNotification(delivery: any, status: any) {
  if (!status) {
    throw new Error('STATUS NOT SET');
  }

  const vendorId = delivery.storeId;

  functions.logger.log('Sending vendor new order notification');

  const statusMessageMap: any = {
    ARRIVED_TO_STORE: {
      title: `El Zipper ${delivery.driverInfo.firstName} ha llegado a la tienda`,
      content: `Entrega el pedido #${delivery.orderDetails.code} al Zipper`,
    },
    ARRIVED_TO_CUSTOMER: {
      title: `El Zipper ${delivery.driverInfo.firstName} ha entregado la orden`,
      content: `El Zipper ha entregado la orden #${delivery.orderDetails.code} al cliente`,
    },
  };

  const notification = statusMessageMap[status];

  if (!notification) return;

  await CloudFunctionsAppUtils.sendPushNotification(
    {
      notification: {
        title: notification.title,
        body: notification.content,
      },
      channel: 'order-update',
    },
    'VENDOR',
    vendorId,
  );
}

async function deleteOrderChat(order: any) {
  const fb = admin.firestore();
  const viewerID = order.driverId;
  const userID = order.customerId;
  const channelId = viewerID < userID ? viewerID + userID : userID + viewerID;

  const bulkWriter = fb.bulkWriter();
  const MAX_RETRY_ATTEMPTS = 3;

  bulkWriter.onWriteError((error: any) => {
    if (error.failedAttempts < MAX_RETRY_ATTEMPTS) {
      return true;
    } else {
      console.log('Failed write at document: ', error.documentRef.path);
      return false;
    }
  });

  await fb.recursiveDelete(
    fb.collection('channels').doc(channelId),
    bulkWriter,
  );

  await fb.recursiveDelete(
    fb
      .collection('social_feeds')
      .doc(order.customerId)
      .collection('chat_feed')
      .doc(channelId),
    bulkWriter,
  );

  await fb.recursiveDelete(
    fb
      .collection('social_feeds')
      .doc(order.driverId)
      .collection('chat_feed')
      .doc(channelId),
    bulkWriter,
  );

  const deliveryOrderRef = admin
    .firestore()
    .collection(DELIVERY_ORDERS)
    .doc(order.id);

  await deliveryOrderRef.update({
    unseenMessages: admin.firestore.FieldValue.delete(),
  });

  await admin.firestore().collection('customers').doc(order.customerId).update({
    badgeCount: 0,
  });

  await admin.firestore().collection('riders').doc(order.driverId).update({
    badgeCount: 0,
  });
}

async function freeBusyRider(driverId: string) {
  const BLOCKED_DRIVERS_REF = admin.database().ref(`${BUSY_RIDERS}`);

  return BLOCKED_DRIVERS_REF.child(driverId).transaction(() => {
    return null;
  });
}

async function finishRiderLog(deliveryOrder: any) {
  const feeCommission = await admin
    .firestore()
    .collection('app_configurations')
    .doc('RIDER_EARNING_FEES')
    .get();

  const total = deliveryOrder.finalFee.amount;
  const percentage = feeCommission
    ? feeCommission.data().commisionPercentage
    : 0;
  const totalCommission = parseFloat(((total * percentage) / 100).toFixed(2));

  const earning = deliveryOrder.finalFee.amount - totalCommission;

  const log = {
    id: admin.firestore().collection('test').doc().id,
    driverId: deliveryOrder.driverInfo.id,
    vendorId: deliveryOrder.storeId,
    vendorOrderId: deliveryOrder.id,
    date: new Date(),
    event: 'ORDER_COMPLETED',
    fee: total,
    earningBreakdown: {
      commission: totalCommission,
      commissionPercentage: percentage,
      earned: earning,
    },
  };

  const existing = await admin
    .firestore()
    .collection(DRIVERS_ACTIVITIES)
    .where('event', '==', log.event)
    .where('vendorOrderId', '==', log.vendorOrderId)
    .get();

  if (existing.size > 0) return;

  await admin.firestore().collection(DRIVERS_ACTIVITIES).doc(log.id).set(log);
}

async function getRiderActiveDeliveries(riderId: string) {
  const activeDeliveries = await admin
    .database()
    .ref(`${RIDERS_ACTIVE_ORDERS}/${riderId}`)
    .get();

  const dto = activeDeliveries.val();

  return dto ? Object.values(dto) : undefined;
}
