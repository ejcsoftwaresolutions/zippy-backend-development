import firebase, { database } from 'firebase-admin';
import {
  AVAILABLE_RIDERS,
  BUSY_RIDERS,
  CONNECTED_RIDERS,
  DELIVERY_REQUESTS,
  DELIVERY_SERVICE_STATUS,
  RIDER_DELIVERY_ALERTS,
  RIDERS,
  RIDERS_ACTIVE_ORDERS,
  RIDERS_LOCATIONS,
  RIDERS_LOCATIONS_TIMESTAMP,
} from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';
import moment = require('moment-timezone');
import Reference = database.Reference;

const admin = require('firebase-admin');

const functions = require('firebase-functions');
export const runDeliveryServiceChecker = async (context: any) => {
  const onlineRidersRef = await firebase.database().ref(`${RIDERS_LOCATIONS}`);
  const ridersLastUpdatesRef = await firebase
    .database()
    .ref(`${RIDERS_LOCATIONS_TIMESTAMP}`);
  const availableRidersRef = await firebase
    .database()
    .ref(`${AVAILABLE_RIDERS}`);
  const busyRidersRef = await firebase.database().ref(`${BUSY_RIDERS}`);
  const connectedRidersRef = await firebase
    .database()
    .ref(`${CONNECTED_RIDERS}`);
  const riderAlertsRef = await firebase
    .database()
    .ref(`${RIDER_DELIVERY_ALERTS}`);
  const deliveryStatusRef = await firebase
    .database()
    .ref(`${DELIVERY_SERVICE_STATUS}`);
  const riderActiveOrderRef = await firebase
    .database()
    .ref(`${RIDERS_ACTIVE_ORDERS}`);
  const deliveryRequestsRef = await firebase
    .database()
    .ref(`${DELIVERY_REQUESTS}`);

  const REFS = {
    ridersLastUpdatesRef,
    onlineRidersRef,
    availableRidersRef,
    busyRidersRef,
    connectedRidersRef,
    riderAlertsRef,
    deliveryStatusRef,
    riderActiveOrderRef,
    deliveryRequestsRef,
  };

  /*checkBusyRiders(REFS)
                                                                                                               .then(() => {
                                                                                                               /!*silent*!/
                                                                                                               })
                                                                                                               .catch((e) => {
                                                                                                               functions.logger.log('Error', e.message);
                                                                                                               });
                                                                                                               */

  checkRiderAlerts(REFS)
    .then(() => {
      /*silent*/
    })
    .catch((e) => {
      functions.logger.log('Error', e.message);
    });

  checkRiderConnections(REFS)
    .then(() => {
      /*silent*/
    })
    .catch((e) => {
      functions.logger.log('Error', e.message);
    });

  checkRidersLastLocationUpdate(REFS)
    .then(() => {
      /*silent*/
    })
    .catch((e) => {
      functions.logger.log('Error', e.message);
    });
};

/*
 async function checkBusyRiders(refs: { [ref: string]: Reference }) {
 const busyDrivers = (await refs.busyRidersRef.get()).val();

 const unlockRidersPromises = Object.keys(busyDrivers ?? {}).map(
 (riderId: any) => {
 return new Promise(async (resolve) => {
 const freeRider = async () => {
 await refs.busyRidersRef.child(riderId).set(null);
 resolve({});
 };

 const activeDeliveryId = (
 await refs.riderActiveOrderRef.child(riderId).get()
 ).val();

 if (!activeDeliveryId) {
 await freeRider();
 }

 const activeDelivery = (
 await refs.deliveryStatusRef.child(activeDeliveryId).get()
 ).val();

 if (!activeDelivery) {
 await freeRider();
 }

 resolve({});
 });
 },
 );

 return Promise.all(unlockRidersPromises);
 }*/

async function checkRiderAlerts(refs: { [ref: string]: Reference }) {
  const alerts = (await refs.riderAlertsRef.get()).val();

  const cleanAlerts = Object.values(alerts ?? {}).map((d: any) => {
    return new Promise(async (resolve) => {
      const cleanAlert = async () => {
        await refs.riderAlertsRef.child(d.driverId).set(null);
        resolve({});
      };

      const request = (await refs.deliveryRequestsRef.child(d.id).get()).val();

      if (!request) {
        await cleanAlert();
      }

      if (request.currentAttempt?.currentAlertedCandidate?.id !== d.driverId) {
        await cleanAlert();
      }

      resolve({});
    });
  });

  return Promise.all(cleanAlerts);
}

async function checkRiderConnections(refs: { [ref: string]: Reference }) {
  const connections = (await refs.connectedRidersRef.get()).val();
  const riderIds = Object.keys(connections ?? {});
  const lastConnections: string[] = Object.values(connections ?? {});

  const MAX_DAYS_CONNECTED = 1;
  const cleanConnections = Object.values(riderIds).map((rId: any, key) => {
    return new Promise(async (resolve) => {
      const disconnectRider = async () => {
        await admin.firestore().doc(`${RIDERS}/${rId}`).update({
          isOnline: false,
        });
        resolve({});
      };

      const lastUpdateInDays = moment(new Date(new Date().toUTCString())).diff(
        new Date(lastConnections[key]),
        'days',
      );

      if (lastUpdateInDays < MAX_DAYS_CONNECTED) return resolve({});

      return disconnectRider();
    });
  });

  return Promise.all(cleanConnections);
}

async function checkRidersLastLocationUpdate(refs: {
  [ref: string]: Reference;
}) {
  const connections = (await refs.connectedRidersRef.get()).val();
  const lastUpdates = (await refs.ridersLastUpdatesRef.get()).val();
  const riderIds = Object.keys(connections ?? {});

  const MAX_MINUTES_OFF = 25;
  const notificationsPromises = riderIds.map((rId: any, key) => {
    return new Promise(async (resolve) => {
      const driverLastUpdate = lastUpdates[rId];

      if (!driverLastUpdate) return resolve({});

      const lastUpdateInMinutes = moment(
        new Date(new Date().toUTCString()),
      ).diff(new Date(driverLastUpdate), 'minutes');

      if (lastUpdateInMinutes <= MAX_MINUTES_OFF) return resolve({});

      await CloudFunctionsAppUtils.sendPushNotification(
        {
          notification: {
            title: `Te hemos perdido 😔!`,
            body: `No hemos recibido tu ubicación por más de ${lastUpdateInMinutes} minutos. Verifica si tienes activado tu GPS, esto es importante para que recibas deliveries cernanos y que el cliente pueda hacer track de su pedido y que su experiencia sea la mejor posible`,
          },
        },
        'RIDER',
        rId,
      );

      resolve({});
    });
  });

  return Promise.all(notificationsPromises);
}
