const admin = require('firebase-admin');
import {VENDORS} from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');

const handleVendorReviews = async (snap: any, context: any) => {
    functions.logger.log('New vendor review', context.params.documentId);

    const review = snap.data();

    const entityRef = await admin
    .firestore()
    .collection(VENDORS)
    .doc(review.entityID);

    const entity = await CloudFunctionsAppUtils.findVendor(review.entityID);

    if (!entity) return Promise.resolve();

    if (!entity.reviewsCount) {
        await entityRef.update({
            reviewsCount: 1, reviewsSum: review.rating,
        });
        return;
    }

    await entityRef.update({
        reviewsCount: admin.firestore.FieldValue.increment(1),
        reviewsSum: admin.firestore.FieldValue.increment(review.rating),
    });
};

export default handleVendorReviews;
