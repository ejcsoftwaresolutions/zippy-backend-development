const admin = require('firebase-admin');
import {InvoiceHtml} from './order-invoice';
import FirebaseUtils from '../../../utils/firebase-utils';
import DatetimeUtils from '../../../utils/datetime-utils';
import {CUSTOMER_INVOICES} from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';
import moment = require('moment');

const functions = require('firebase-functions');
const pdf = require('html-pdf');
const uuid = require('uuid');

const generateOrderInvoice = async (change: any, context: any) => {
    functions.logger.log('Generating order invoice', context.params.documentId);

    //return Promise.resolve();
    const newStatus = change.after.get('status');

    if (newStatus !== 'REQUESTED') return Promise.resolve();

    const invoiceRef = await admin
    .firestore()
    .collection(CUSTOMER_INVOICES)
    .doc(context.params.documentId);

    const invoiceQ = await invoiceRef.get();
    const invoice = invoiceQ.data();
    if (!invoice) return Promise.resolve();

    const order = await CloudFunctionsAppUtils.getOrder(invoice.orderId);

    const fileName = `orders/${invoice.orderId}/invoice/${context.params.documentId}${moment().format('DD/MM/YYYY HH:mm:ss')}.pdf`;

    const fileRef = admin.storage().bucket().file(fileName, {});

    const {generate} = useGenerateInvoiceHtml();

    const content = generate(order);

    await invoiceRef.update({status: 'GENERATING'});

    return new Promise((resolve) => {
        pdf
        .create(content, {
            /*    orientation: 'portrait',
             type: 'pdf', */
            timeout: '100000', width: 300, height: 800,
        })
        .toStream(async (err: any, stream: any) => {
            if (err) {
                await invoiceRef.update({
                    status: 'ERROR',
                });

                functions.logger.log('Error', err);
                return;
            }

            await stream.pipe(fileRef.createWriteStream({
                metadata: {
                    contentType: 'application/pdf', metadata: {
                        firebaseStorageDownloadTokens: uuid.v4(), lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                    },
                }, resumable: false, public: true,
            }),);

            await invoiceRef.update({
                status: 'CREATED', url: fileRef.publicUrl(), updatedAt: new Date(),
            });

            resolve({});
        });
    });
};

export default generateOrderInvoice;

function useGenerateInvoiceHtml() {
    function addonsHtml(addons: any) {
        return addons
        .map((addon: any) => {
            return `<div class='addon'>
                       <div>
                           ${addon.name}
                       </div>
                       <div class='price'>
                            $${addon.price}
                        </div>
                   </div>`;
        })
        .join('');
    }

    function getProducts(products: any[]) {
        return products.map((product: any) => {
            return `
                <div>
                    <div class='field product'>
                        <div>
                            <div><strong>${product.name}${product.option ? ' - ' + product.option.name : ''}</strong></div>
                            <div>${product.quantity} x $${product.price}</div>
                        </div>
                        <div>
                             $${product.quantity * product.price}
                        </div>
                    </div>
                    ${addonsHtml(product.addons ?? [])}
                </div>
            `;
        });
    }

    function getVendorHtml(vendorDto: any) {
        return `
             <div class='sectionContent'>
                <h4 class='title'>Tienda/Establecimiento</h4>
                <div class='field'>
                    <p><strong>Nombre: </strong> ${vendorDto.name}</p>
                </div>
            </div>
            `;
    }

    function getShippingAddress(shippingAddress: any) {
        function getCustomAddress() {
            const parts = [shippingAddress.line1, shippingAddress.line2, shippingAddress.city, shippingAddress.state,];
            return parts.join(', ');
        }

        return `
             <div class='sectionContent'>
                <h4 class='title'>Dirección de entrega</h4>
                <div class='field'>
                    <p>${shippingAddress.formattedAddress ? shippingAddress.formattedAddress : `${getCustomAddress()}`}</p>                 </div>
                </div>
            </div>
            `;
    }

    return {
        generate(order: any) {
            const productsHtml = getProducts(order.products).join('');

            return InvoiceHtml.replace('{{DATE}}', `${DatetimeUtils.format(new Date(), 'DD/MM/YYYY hh:mm A')} `,)
            .replace('{{INVOICE_DATE}}', `${DatetimeUtils.format(FirebaseUtils.getDate(order.createdAt), 'DD/MM/YYYY hh:mm A',)} `,)
            .replace('{{ORDER_ID}}', `${order.code}`)
            .replace('{{CUSTOMER_NAME}}', `${order.customer.firstName}`)
            .replace('{{VENDOR_HTML}}', getVendorHtml(order.vendor))
            .replace('{{SHIPPING_ADDRESS_HTML}}', order.deliveryMethod == 'PICK_UP' ? '' : getShippingAddress(order.shipAddr),)
            .replace('{{PRODUCTS_HTML}}', productsHtml)
            .replace('{{PAYMENT_METHOD_HTML}}', '')
            .replace('[[DELIVERY_FEES]]', (
                order.total - order.subtotal
            ).toFixed(2))
            .replace('[[TOTAL]]', order.total);
        },
    };
}
