import { VENDOR_ORDERS } from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';

const admin = require('firebase-admin');

const functions = require('firebase-functions');
import moment = require('moment-timezone');

const runScheduledDeliveries = async (context: any) => {
  const unprocessed = await getData();

  const promises = unprocessed.map((c: any) => {
    return new Promise(async (resolve, reject) => {
      try {
        await CloudFunctionsAppUtils.sendPushNotification(
          {
            notification: {
              title: `Recordatorio de pedido agendado #${c.data.code} para las ${c.data.scheduledTimeSlot.from} - ${c.data.scheduledTimeSlot.to}`,
              body: `Ya puedes ir buscando el Zipper de esta orden`,
            },
            channel: 'order-update',
          },
          'VENDOR',
          c.data.vendor.id,
        );
      } catch (e: any) {
        functions.logger.log(e?.message);
      }

      await c.ref.update({
        scheduleProcessed: true,
      });

      return resolve(true);
    });
  });

  return Promise.all(promises);
};

async function getData() {
  const currentDate = moment().tz('America/Caracas').format('MM-DD-YYYY');
  const startHour = moment().tz('America/Caracas').format('hh:mm A');
  /* const endHour = moment()
             .tz('America/Caracas')
             .add('1', 'hour')
             .format('hh:00 A');
    
             functions.logger.log('Log :D', currentDate, startHour, endHour);*/

  const todaysUnprocessedVendorOrdersQ = await admin
    .firestore()
    .collection(VENDOR_ORDERS)
    .where('deliveryMethod', '==', 'SCHEDULED_DELIVERY')
    .where('scheduleProcessed', '==', false)
    .where('scheduledDate', '==', currentDate)
    .where('scheduledTimeSlot.from', '==', startHour)
    /* .where('scheduledTimeSlot.to', '==', endHour)*/
    .where('status', '==', 'Order Accepted')
    .get();

  const unprocessed = todaysUnprocessedVendorOrdersQ.docs.map((d: any) => ({
    data: d.data(),
    ref: d.ref,
  }));
  return unprocessed;
}

export default runScheduledDeliveries;
