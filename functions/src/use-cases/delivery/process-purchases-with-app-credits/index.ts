const admin = require('firebase-admin');
import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import {
  createPurchaseCreditLog,
  createRefundCreditLog,
} from '../../customers/handle-app-credits-transactions';
import { CREDIT_LOG_REF } from '../../../constants';

const functions = require('firebase-functions');
import DocumentSnapshot = firestore.DocumentSnapshot;

export const processPurchasesWithAppCredits = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const document = change.after.data();

  const oldDocument = change.before.data();

  /* if (!document && oldDocument && oldDocument.logId) {
                           return reversePurchaseWithLog(oldDocument.logId);
                           }*/

  if (!document) return Promise.resolve();

  if (document && oldDocument) {
    const orderHasBeenRejected =
      document.status !== oldDocument.status &&
      document.status == 'Order Rejected';

    if (
      orderHasBeenRejected &&
      oldDocument.appCreditsUsed &&
      !oldDocument.appCreditsRefunded
    ) {
      await change.after.ref.update({
        appCreditsRefunded: true,
      });
      return insertRefundLog(
        await createRefundCreditLog({
          userId: document.customer.id,
          amount: document.appCreditsUsed,
          metadata: {
            orderId: document.id,
            orderCode: document.code,
          },
        }),
      );
    }

    return Promise.resolve(); // Not updates assuming order created = order paid
  }

  const orderData = document;

  if (!orderData.appCreditsUsed) return Promise.resolve();

  functions.logger.log('New Purchase with credits', context.params.documentId);

  return insertPurchaseLog({
    amount: orderData.appCreditsUsed,
    userId: orderData.customer.id,
    orderId: orderData.id,
    orderTotal: orderData.total,
    vendorId: orderData.vendor.id,
    vendorName: orderData.vendor.name,
  });
};

async function insertPurchaseLog(params: {
  userId: string;
  amount: number;
  orderId: string;
  orderTotal: number;
  vendorId: string;
  vendorName: string;
}) {
  const purchaseLog = await createPurchaseCreditLog({
    userId: params.userId,
    amount: params.amount,
    metadata: {
      orderId: params.orderId,
      orderTotal: params.orderTotal,
      vendorId: params.vendorId,
      vendorName: params.vendorName,
    },
  });

  await admin
    .firestore()
    .collection(CREDIT_LOG_REF)
    .doc(purchaseLog.id)
    .set(purchaseLog);
}

async function insertRefundLog(log: any) {
  await admin.firestore().collection(CREDIT_LOG_REF).doc(log.id).set(log);
}
