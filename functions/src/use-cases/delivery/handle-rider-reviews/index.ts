const admin = require('firebase-admin');
import {RIDERS} from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');

const handleRiderReviews = async (snap: any, context: any) => {
    functions.logger.log('New rider review', context.params.documentId);

    const review = snap.data();

    const entityRef = await admin
    .firestore()
    .collection(RIDERS)
    .doc(review.entityID);

    const entity = await CloudFunctionsAppUtils.findRider(review.entityID);

    if (!entity) return Promise.resolve();

    if (!entity.reviewsCount) {
        await entityRef.update({
            reviewsCount: 1, reviewsSum: review.rating,
        });
        return;
    }

    await entityRef.update({
        reviewsCount: admin.firestore.FieldValue.increment(1),
        reviewsSum: admin.firestore.FieldValue.increment(review.rating),
    });
};

export default handleRiderReviews;
