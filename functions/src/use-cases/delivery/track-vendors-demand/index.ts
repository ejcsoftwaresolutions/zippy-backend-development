import {VENDORS_DEMAND} from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';
import moment = require('moment-timezone');

const functions = require('firebase-functions');
const admin = require('firebase-admin');

const trackVendorsDemand = async (change: any, context: any) => {
    functions.logger.log('Tracking vendors demand', context.params.documentId);

    const oldStatus = change.before.get('status');

    const newStatus = change.after.get('status');

    if (oldStatus !== 'Order Placed') return Promise.resolve();

    if (newStatus !== 'Order Accepted') return Promise.resolve();

    const order = change.after.data();

    const today = moment().utc().format('YYYY-MM-DD');

    const vendor = await CloudFunctionsAppUtils.findVendor(order.vendor.id);

    if (!vendor) return;

    try {
        await CloudFunctionsAppUtils.syncVendorGeolocation(order.vendorID, [vendor.location.latitude, vendor.location.longitude,]);
    } catch (error) {
        console.log(error);
    }

    const vendorDailyDemandQ = await admin
    .firestore()
    .collection(VENDORS_DEMAND)
    .where('vendorID', '==', order.vendorID)
    .where('date', '==', today)
    .get();

    const vendorDailyDemand = vendorDailyDemandQ.docs.map((d: any) => d);

    if (vendorDailyDemand.length > 0) {
        const demandDoc = vendorDailyDemand[0];

        const demandDailyDocRef = await admin
        .firestore()
        .collection(VENDORS_DEMAND)
        .doc(demandDoc.id);

        return demandDailyDocRef.update({
            salesQuantity: admin.firestore.FieldValue.increment(1),
        });
    }

    const info = {
        vendorID: order.vendorID, date: today, salesQuantity: 1,
    };

    return admin.firestore().collection(VENDORS_DEMAND).add(info);
};

export default trackVendorsDemand;
