const admin = require('firebase-admin');
import {DRIVERS_TOTAL_EARNINGS} from '../../../constants';

const functions = require('firebase-functions');

const updateDriversTotalEarnings = async (snap: any, context: any) => {
    functions.logger.log('Updating driver total earnings', context.params.documentId,);

    const driverEvent = snap.data();

    if (['ORDER_COMPLETED', 'REFERRAL_CODE_REDEEM', 'REFERRAL_EARNING_PRIZE',].indexOf(driverEvent.event) <= -1) return Promise.resolve();

    if (!driverEvent) return Promise.resolve();

    const docId = context.params.documentId;

    const driverEarningsQ = await admin
    .firestore()
    .collection(DRIVERS_TOTAL_EARNINGS)
    .doc(driverEvent.driverId)
    .get();

    const driverEarnings = driverEarningsQ.data();

    if (driverEarnings) {
        const demandDailyDocRef = await admin
        .firestore()
        .collection(DRIVERS_TOTAL_EARNINGS)
        .doc(driverEvent.driverId);

        return demandDailyDocRef.update({
            total: admin.firestore.FieldValue.increment(parseFloat(driverEvent.fee)),
            lastUpdate: new Date(),
            events: [...(
                driverEarnings.events ?? []
            ), docId],
        });
    }

    const info = {
        driverID: driverEvent.driverId, total: parseFloat(driverEvent.fee), lastUpdate: new Date(), events: [docId],
    };

    return admin
    .firestore()
    .collection(DRIVERS_TOTAL_EARNINGS)
    .doc(driverEvent.driverId)
    .set(info);
};

export default updateDriversTotalEarnings;
