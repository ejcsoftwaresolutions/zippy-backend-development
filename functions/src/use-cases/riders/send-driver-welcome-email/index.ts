import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');

const sendDriverWelcomeEmail = async (snap: any, context: any) => {
    functions.logger.log('send welcome email', context.params.documentId);

    const user = snap.data();

    return Promise.all([sendEmail(user)]);
};

async function sendEmail(user: any) {
    return CloudFunctionsAppUtils.sendEmail(user.email.trim(), {
        subject: `Confirmación de Registro en la plataforma como Zipper, de ${user?.firstName} ${user?.lastName}`,
        text: `Hola, este es un mensaje de Confirmación de Registro en la plataforma como Zipper, para ${user?.firstName} ${user?.lastName}`,
        html: `
              <div>
                <p>Hola, este es un mensaje de Confirmación de Registro en la plataforma como Zipper, para
                ${user?.firstName} ${user?.lastName} </p>
                <br/>
                <p>Nombre: <strong> ${user?.firstName} ${user?.lastName}</strong> </p>
                <p>Número de teléfono: <strong>${user?.phone} </strong>  </p>
                <br/>
                <p> <strong> Gracias por Preferirnos.</strong> </p>
              </div>
          `,
    });
}

export default sendDriverWelcomeEmail;
