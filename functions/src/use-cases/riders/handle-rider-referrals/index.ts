const functions = require('firebase-functions');
const admin = require('firebase-admin');
import {DRIVERS_ACTIVITIES, RIDERS} from '../../../constants';

const PRIZE_AMOUNT = 5;

const handleRiderReferrals = async (snap: any, context: any) => {
    functions.logger.log('Handle referral redeem', context.params.documentId);

    const rider = snap.data();

    if (!rider.referrerCode) return Promise.resolve();

    if (await isCodeRedeemed(rider.referrerCode)) return Promise.resolve();

    const referrer = await findReferrer(rider.referrerCode);

    if (!referrer) return Promise.resolve();

    return Promise.all([redeemCode(rider.id, rider.referrerCode), rewardReferrer(referrer.id, rider.referrerCode),]);
};

async function isCodeRedeemed(code: string) {
    const driverCodeRedeemQ = await admin
    .firestore()
    .collection(DRIVERS_ACTIVITIES)
    .where('event', '==', 'REFERRAL_CODE_REDEEM')
    .where('code', '==', code)
    .get();

    return !driverCodeRedeemQ.empty;
}

async function findReferrer(code: string) {
    const referrerQ = await admin
    .firestore()
    .collection(RIDERS)
    .where('referralCode', '==', code)
    .limit(1)
    .get();

    const {0: rider} = referrerQ.docs.map((u: any) => u.data());

    return rider;
}

async function redeemCode(riderId: string, code: string) {
    const ref = await admin.firestore().collection(DRIVERS_ACTIVITIES).add({
        date: new Date(), event: 'REFERRAL_CODE_REDEEM', fee: PRIZE_AMOUNT, code: code, driverId: riderId,
    });

    await ref.update({id: ref.id});
}

async function rewardReferrer(riderId: string, code: string) {
    const ref = await admin.firestore().collection(DRIVERS_ACTIVITIES).add({
        date: new Date(), event: 'REFERRAL_EARNING_PRIZE', fee: PRIZE_AMOUNT, code: code, driverId: riderId,
    });

    await ref.update({id: ref.id});
}

export default handleRiderReferrals;
