import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');

const notifyDriverWithdrawalRequest = async (snap: any, context: any) => {
    functions.logger.log('Notify driver withdrawal request', context.params.documentId,);

    const data = snap.data();
    const ADMIN_EMAIL = process.env.ZIPPI_PAYMENTS_EMAIL;

    const account = await CloudFunctionsAppUtils.findAccount(data.driverId);
    if (!account) return Promise.resolve();

    return CloudFunctionsAppUtils.sendEmail(ADMIN_EMAIL as string, {
        subject: `Nueva solicitud de retiro de rider`,
        text: `Nueva solicitud de retiro para el rider ${account.firstName?.trim()} ${account.lastName?.trim()} `,
        html: `
      <div>
        <p>Hola, el rider ${account.firstName?.trim()} ${account.lastName?.trim()} ha solicitado el retiro de $${data.amount}</p>
        <br/>
      </div>
    `,
    });
};

export default notifyDriverWithdrawalRequest;
