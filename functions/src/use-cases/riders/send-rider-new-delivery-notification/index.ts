const functions = require('firebase-functions');
import CloudFunctionsAppUtils from '../../../utils/app';

export const sendRiderNewDeliveryNotification = async (
  snap: any,
  context: any,
) => {
  functions.logger.log(
    'Sending rider new delivery notification',
    context.params.documentId,
  );
  const data = snap.val();

  await CloudFunctionsAppUtils.sendPushNotification(
    {
      data: {
        riderId: data.driverId,
        orderId: data.id,
      },
      notification: {
        title: `Alerta de nueva solicitud de delivery`,
        body: `Tienes ${data.acceptanceTimeout} segundos para aceptar!`,
      },
      channel: 'new-order',
    },
    'RIDER',
    data.driverId,
  );
  return Promise.resolve();
};
