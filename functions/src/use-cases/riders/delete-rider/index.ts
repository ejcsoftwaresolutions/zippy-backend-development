const functions = require('firebase-functions');

const deleteRider = async (snap: any, context: any) => {
  functions.logger.log('Deleting rider', context.params.documentId);

  // Delete withdrawal details

  // Delete earnings summary drivers_total_earnings

  // Delete application

  // Delete withdrawal requests driver_withdrawal_requests

  // Delete activity logs  driver_delivery_activity_logs

  // Delete firebase auth user

  // Delete user
};

export default deleteRider;
