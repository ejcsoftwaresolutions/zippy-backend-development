import {firestore} from 'firebase-admin';
import {Change, EventContext} from 'firebase-functions';

import CloudFunctionsAppUtils from '../../../utils/app';

import DocumentSnapshot = firestore.DocumentSnapshot;

export const notifyNewPhonePayOrder = async (change: Change<DocumentSnapshot>, context: EventContext,) => {
    const document = change.after.data();
    /*const oldDocument = change.before.data();*/

    if (!document) return Promise.resolve();
    /*if (!oldDocument) return Promise.resolve();*/

    await sendAdminEmail(document);

    return Promise.resolve();
};

async function sendAdminEmail(document: any) {
    const admin = await CloudFunctionsAppUtils.findAdminAccount();

    if (!admin || !admin.config?.notificationsEmail) return;

    const emailData = {
        subject: `${process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'} - Nuevo reporte de pago móvil para orden #${document.code}`,
        text: `Nuevo reporte de pago móvil para orden #${document.code}`,
        html: `
         <div>
             <p>
                  Cliente: ${document.customer.firstName} ${document.customer.lastName} - ${document.customer.id}.  ${document.customer.email} ${document.customer.phone}
            </p>
            <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                 Total en USD: ${document.total} Tasa aplicada: ${document.paymentReport.exchangeRate}
            </p>
             <p>
                  Total en Bs.D pagado: ${document.paymentReport.amount}
             </p>
              <p>
                  Telefono pagador: ${document.paymentReport.phone}
             </p>
             <p>
                  TxID: ${document.paymentReport.transactionId}
             </p>
          </div>
        `,
    };

    await CloudFunctionsAppUtils.sendEmail(admin.config?.notificationsEmail as string, emailData,);

    await CloudFunctionsAppUtils.sendEmail(process.env.ZIPPI_PAYMENTS_EMAIL as string, emailData,);
}
