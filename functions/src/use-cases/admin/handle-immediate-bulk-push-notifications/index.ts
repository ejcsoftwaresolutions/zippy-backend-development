import { ENGAGEMENT_NOTIFICATIONS } from '../../../constants';
import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import { BulkNotificationService } from '../../../utils/bulk-notifications/bulk-notification-service';

const admin = require('firebase-admin');

const functions = require('firebase-functions');
import DocumentSnapshot = firestore.DocumentSnapshot;

export const handleImmediateBulkPushNotifications = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const document = change.after.data();

  const oldDocument = change.before.data();

  if (!document && oldDocument) {
    return Promise.resolve();
  }

  if (!document) return Promise.resolve();

  const ref = admin
    .firestore()
    .collection(ENGAGEMENT_NOTIFICATIONS)
    .doc(document.id);

  const reqData = (await ref.get()).data();
  if (!reqData) return Promise.resolve();
  if (reqData.deliveryType !== 'IMMEDIATELY') return Promise.resolve();
  if (reqData.processed) return Promise.resolve();
  if (reqData.draft) return Promise.resolve();

  const message = {
    id: reqData.id,
    title: reqData.content.title,
    description: reqData.content.description,
    imageUrl:
      reqData.content.attachments && reqData.content.attachments.length > 0
        ? reqData.content.attachments[0].url
        : undefined,
    linkUrl:
      reqData.content.link && reqData.content.link !== ''
        ? reqData.content.link
        : undefined,
  };

  const bulkService = await BulkNotificationService;
  const results =
    reqData.type === 'INDIVIDUAL'
      ? await bulkService.sendBulkIndividualNotification(
          reqData.targetGroup,
          message,
          reqData.targetRecipients,
        )
      : await bulkService.sendCampaignNotification(
          reqData.targetGroup,
          message,
        );

  functions.logger.log('res', results);

  await ref.update(bulkService.getBulkNotificationProcessResult(results));
};
