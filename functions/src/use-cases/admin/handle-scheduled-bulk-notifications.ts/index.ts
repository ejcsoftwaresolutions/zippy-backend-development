import DateTimeUtils from '../../../utils/datetime-utils';
import { ENGAGEMENT_NOTIFICATIONS } from '../../../constants';
import { BulkNotificationService } from '../../../utils/bulk-notifications/bulk-notification-service';

const admin = require('firebase-admin');
import moment = require('moment-timezone');

const functions = require('firebase-functions');

export const handleScheduledBulkNotifications = async (context: any) => {
  const bulkMessagesToProcess = await findScheduledMessages();

  functions.logger.log('toProcess', bulkMessagesToProcess);

  const bulkService = await BulkNotificationService;

  const promises = bulkMessagesToProcess.map((reqData: any) => {
    return new Promise(async (resolve) => {
      const message = {
        id: reqData.id,
        title: reqData.content.title,
        description: reqData.content.description,
        imageUrl:
          reqData.content.attachments && reqData.content.attachments.length > 0
            ? reqData.content.attachments[0].url
            : undefined,
        linkUrl:
          reqData.content.link && reqData.content.link !== ''
            ? reqData.content.link
            : undefined,
      };

      const results =
        reqData.type === 'INDIVIDUAL'
          ? await bulkService.sendBulkIndividualNotification(
              reqData.targetGroup,
              message,
              reqData.targetRecipients,
            )
          : await bulkService.sendCampaignNotification(
              reqData.targetGroup,
              message,
            );

      resolve({
        id: reqData.id,
        results: results,
      });
    });
  });
  const resultsPerMessage = await Promise.all(promises);

  functions.logger.log('res', resultsPerMessage);

  return updateMessagesStatus(resultsPerMessage, bulkService);
};

async function findScheduledMessages() {
  const todayMessages = await findTodayMessages();

  const bulkMessagesToProcess = todayMessages.filter((v: any) => {
    const timezone = v.schedule.timezone;
    const currentTime = moment(moment().tz(timezone).format('HH:mm'), 'HH:mm');

    const hour = moment(v.schedule.time, 'HH:mm');

    const remainingMinutes = DateTimeUtils.differenceInMinutesMoment(
      hour,
      currentTime,
    );

    functions.logger.log(
      'current time in timezone' + v.schedule.timezone,
      currentTime,
    );

    functions.logger.log('scheduled time', hour);

    functions.logger.log('remaining', remainingMinutes);

    return remainingMinutes == 0;
  });

  return bulkMessagesToProcess;
}

async function findTodayMessages() {
  const start = new Date();
  start.setUTCHours(0, 0, 0, 0);
  const end = new Date();
  end.setUTCHours(23, 59, 59, 999);

  return (
    await admin
      .firestore()
      .collection(ENGAGEMENT_NOTIFICATIONS)
      .where(`deliveryType`, '==', 'SCHEDULED')
      .where('processed', '==', false)
      .where('draft', '==', false)
      .where(
        'schedule.datetime',
        '>=',
        admin.firestore.Timestamp.fromDate(start),
      )
      .where('schedule.datetime', '<=', admin.firestore.Timestamp.fromDate(end))
      .get()
  ).docs.map((v: any) => v.data());
}

async function updateMessagesStatus(
  resultsPerMessage: any[],
  bulkNotificationService: any,
) {
  const ref = admin.firestore().collection(ENGAGEMENT_NOTIFICATIONS);
  const batch = admin.firestore().batch();

  resultsPerMessage.forEach((res) => {
    const sfRef = ref.doc(res.id);
    batch.update(
      sfRef,
      bulkNotificationService.getBulkNotificationProcessResult(res.results),
    );
  });

  return batch.commit();
}
