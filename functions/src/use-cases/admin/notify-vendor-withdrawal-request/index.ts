import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');

const notifyVendorWithdrawalRequest = async (snap: any, context: any) => {
    functions.logger.log('Notify vendor withdrawal request', context.params.documentId,);

    const ADMIN_EMAIL = process.env.ZIPPI_PAYMENTS_EMAIL;

    const data = snap.data();

    const vendor = await CloudFunctionsAppUtils.findVendor(data.vendorId);
    const account = await CloudFunctionsAppUtils.findAccount(data.vendorId);
    if (!vendor || !account) return Promise.resolve();

    return CloudFunctionsAppUtils.sendEmail(ADMIN_EMAIL as string, {
        subject: `Nueva solicitud de retiro de vendedor`,
        text: `Nueva solicitud de retiro para el vendedor ${account.firstName?.trim()} ${account.lastName?.trim()} de la tienda ${vendor.title}`,
        html: `
        <div>
          <p>Hola, el vendedor ${account.firstName?.trim()} ${account.lastName?.trim()} de la tienda ${vendor.title} ha solicitado el retiro de $${data.amount}</p>
          <br/>
        </div>
    `,
    });
};

export default notifyVendorWithdrawalRequest;
