import { ArrayUtils } from '../../../utils/array-utils';
import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import ObjectUtils from '../../../utils/object-utils';

const admin = require('firebase-admin');

const functions = require('firebase-functions');

import DocumentSnapshot = firestore.DocumentSnapshot;

export const handleBestOffers = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  functions.logger.log('Best offers ', context.params.documentId);

  const offerRef = await admin
    .firestore()
    .collection('product_promotion')
    .doc(context.params.documentId);

  const document = change.after.data();
  const oldDocument = change.before.data();

  const offerData = (await offerRef.get()).exists;

  if (!offerData) return Promise.resolve();

  if (!document) {
    return Promise.resolve();
  }

  const vendor = await findVendor(document.vendorID);

  const hasVariants = Boolean(document.options) && document.options.length > 0;

  if (hasVariants) {
    try {
      await syncVariantProducts(document, vendor);
    } catch (e: any) {
      functions.logger.log('Error at syncing variants ' + e.message);
    }

    return Promise.resolve();
  }

  try {
    await syncSingleProducts(document, oldDocument, offerRef);
  } catch (e: any) {
    functions.logger.log('Error at single product ' + e.message);
  }

  return Promise.resolve();
};

async function findVendor(vendorId: string) {
  const vendorRef = await admin.firestore().collection('vendors').doc(vendorId);

  return (await vendorRef.get()).data();
}

async function saveOffer(document: any) {
  const vendor = await findVendor(document.vendorID);

  if (!vendor) return {};

  const offer = getMappedOffer(document, vendor);

  return offer;
}

function getMappedOffer(document: any, vendor: any) {
  return ObjectUtils.omitUnknown({
    vendorProductID: document.id,
    productTitle: document.name,
    description: document.description,
    productFileName: document.thumbnailUrl ?? document.photo,
    oldPrice: document.price ?? 0,
    newPrice: document.discountPrice ?? 0,
    percentageDiscount: document.discountRate ?? 0,
    vendorID: document.vendorID,
    variantID: document.variantId,
    logoFileName: vendor.logo,
    isPromotion: document.isPromotion,
    type: document.type ?? 'REGULAR',
    categoryID: document.categoryID,
    subcategoryID: document.subcategoryID,
    currency: document.currency,
    order: document.promoOrder ?? 999999999,
  });
}

async function syncSingleProducts(
  document: any,
  oldDocument: any,
  offerRef: any,
) {
  functions.logger.log('Syncing Single product discounts', document);

  const offer = await saveOffer(document);
  return offerRef.set(offer);
}

async function syncVariantProducts(document: any, vendor: any, clean = false) {
  if (!vendor) return Promise.resolve();

  const offerRef = admin.firestore().collection('product_promotion');

  const lowestDiscountPriceVariant = ArrayUtils.orderBy(
    document.options,
    'discountPrice',
    'asc',
  ).find((o: any) => {
    return !!o.applyDiscount;
  });

  const selectedVariant = lowestDiscountPriceVariant ?? document.options?.[0];

  if (!selectedVariant) {
    return Promise.resolve();
  }

  const bestOffer = getMappedOffer(
    {
      ...document,
      price: selectedVariant.price,
      discountRate: selectedVariant.discountRate,
      discountPrice: selectedVariant.discountPrice,
      vendorID: document.vendorID,
      id: document.id,
      variantId: selectedVariant.id,
      name: `${document.name}`,
      photo: document.thumbnailUrl ?? document.photo,
      description: document.description,
    },
    vendor,
  );

  return offerRef.doc(document.id).set(bestOffer);
}
