import {HOLDING_VENDOR_ORDERS, VENDOR_PRODUCTS} from '../../../constants';
import {Change, EventContext} from 'firebase-functions';
import {firestore} from 'firebase-admin';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
import DocumentSnapshot = firestore.DocumentSnapshot;

const updateOrderProductsStock = async ({
    change, context,
}: {
    change: Change<DocumentSnapshot>; context: EventContext;
}, source: string,) => {
    const isHoldingOrder = source === HOLDING_VENDOR_ORDERS;
    const isDelete = change.before.exists && !change.after.exists;
    /* const oldStatus = change.before.get('status');*/
    const newStatus = change.after.get('status');

    if (!isHoldingOrder && !['Order Placed', 'Order Rejected'].includes(newStatus)) return Promise.resolve();

    if (newStatus === 'Order Placed' && change.before.exists) {
        return Promise.resolve(); /*  It should only discount when order/holding order is created with Order Placed */
    }

    if (isDelete && !isHoldingOrder) return Promise.resolve();

    const operation = (
        () => {
            if (isHoldingOrder) return isDelete ? 'ADD' : 'SUBTRACT';

            return newStatus === 'Order Placed' ? 'SUBTRACT' : 'ADD';
        }
    )();

    const vendorOrder = (
        () => {
            if (isHoldingOrder && isDelete) return change.before.data();
            return change.after.data();
        }
    )();

    if (!vendorOrder) return Promise.resolve();

    return processOrder(vendorOrder, operation);
};

async function processOrder(vendorOrder: any, operation: string) {
    functions.logger.log('Updating order products stock', vendorOrder.id);

    const mappedProducts = vendorOrder.products.map((p: any) => (
        {
            ...p,
            vendorID: vendorOrder.vendorID,
            orderId: vendorOrder.orderId,
            hasSections: !!p.sections && p.sections.length > 0,
        }
    ));

    const db = admin.firestore();

    try {

        await db.runTransaction(async (transaction) => {

            const changes: any = {}

            for (const p of mappedProducts) {

                const vendorProductRef = admin
                .firestore()
                .collection(VENDOR_PRODUCTS)
                .doc(p.id);

                const vendorProduct = changes[p.id] ? await Promise.resolve(changes[p.id].data) : (
                    await transaction.get(vendorProductRef)
                )?.data();

                if (!vendorProduct) {
                    functions.logger.log('Could not find product to update the stock: ', p.id);
                    continue
                }

                const updatedProduct = await handleProductStock(operation, p, vendorProduct);

                changes[p.id] = {
                    ref: vendorProductRef, data: updatedProduct
                }
            }

            Object.values(changes).forEach((item: any) => {
                transaction.update(item.ref, item.data)
            })
        });
    } catch (e: any) {
        functions.logger.log(' Error in transaction ' + e.message);
    }
}


async function handleProductStock(operation: string, orderProduct: any, vendorProduct: any) {

    let updatedProduct = vendorProduct;

    updatedProduct = await handleMainProductStock(updatedProduct, operation, orderProduct)

    if (orderProduct.optionId) {
        updatedProduct = await handleProductVariantStock(updatedProduct, operation, orderProduct);
    }

    if (orderProduct.hasSections) {
        updatedProduct = await handleProductsSectionsStock(updatedProduct, operation, orderProduct.sections);
    }

    return updatedProduct
}

async function handleMainProductStock(vendorProduct: any, operation: string, orderProduct: any) {

    const newStock = vendorProduct.stockCount + orderProduct.quantity * (
        operation === 'SUBTRACT' ? -1 : 1
    );

    functions.logger.log('Main stock for product ' + orderProduct.id, newStock);

    return {
        ...vendorProduct, stockCount: newStock >= 0 ? newStock : 0, available: newStock > 0,
    }
}

async function handleProductVariantStock(vendorProduct: any, operation: string, orderProduct: any) {
    const newVariants = vendorProduct.options.map((option: any) => {

        const isSelectedVariant = option.id == orderProduct.optionId

        if (!isSelectedVariant) return option

        const selectedOptionQuantity = orderProduct.quantity

        const newStock = (
                             option.stockCount ?? 0
                         ) + selectedOptionQuantity * (
                             operation === 'SUBTRACT' ? -1 : 1
                         );

        functions.logger.log('New variant stock for product ' + orderProduct.option, newStock);

        return {
            ...option, stockCount: newStock >= 0 ? newStock : 0, available: newStock > 0,
        }
    });


    return {
        ...vendorProduct, options: newVariants,
    }
}


async function handleProductsSectionsStock(vendorProduct: any, operation: string, orderProductSections: any[]) {
    const newSections = vendorProduct.sections.map((section: any) => {
        const orderSection = orderProductSections.find((oS) => oS.id === section.id,);
        return getUpdatedProductSectionOptions(section, operation, orderSection);
    });

    return {
        ...vendorProduct, sections: newSections,
    }
}

function getUpdatedProductSectionOptions(productSection: any, operation: string, orderSection: any,) {
    if (!orderSection) return productSection;

    const availableOptions = productSection.availableOptions.reduce((acc: any[], current: any) => {
        const selectedOptionQuantity = orderSection.selectedOptions.find((sO: any) => sO.id == current.id,)?.qty;

        if (!selectedOptionQuantity) return [...acc, current];

        const newStock = (
                             current.stockCount ?? 0
                         ) + selectedOptionQuantity * (
                             operation === 'SUBTRACT' ? -1 : 1
                         );

        return [...acc, {
            ...current, stockCount: newStock >= 0 ? newStock : 0, available: newStock > 0,
        },];
    }, [],);

    return {
        ...productSection, availableOptions: availableOptions,
    };
}

export default updateOrderProductsStock;
