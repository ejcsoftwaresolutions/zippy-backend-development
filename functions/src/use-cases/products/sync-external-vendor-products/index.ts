import SucasaProductFetcher from '../../../utils/external-vendor-product-syncronizer/fetchers/sucasa-product-fetcher';
import SucasaMapper from '../../../utils/external-vendor-product-syncronizer/mappers/sucasa-mapper';
import ExternalVendorProductCatalogImporter from '../../../utils/external-vendor-product-syncronizer/external-vendor-product-catalog-importer';
import SucasaProductBatcher from '../../../utils/external-vendor-product-syncronizer/batchers/sucasa-product-batcher';
import firebase from 'firebase-admin';

const functions = require('firebase-functions');

const syncExternalVendorProducts = async (context: any) => {
  const active = (
    await firebase
      .firestore()
      .collection('app_configurations')
      .doc('OTHER')
      .get()
  ).data()?.sucasaSyncEnabled;

  if (!active) {
    functions.logger.log('sucasa sync not enabled');
    return Promise.resolve();
  }

  syncSUCASA().then(() => {
    /*silent*/
  });
};

export default syncExternalVendorProducts;

async function syncSUCASA() {
  const productImporter = new ExternalVendorProductCatalogImporter();

  try {
    const isDev = process.env.NODE_ENV != 'production';
    const sellerId = isDev ? '2HvDlZbhgSSKAefzQv32' : 'y7EHAW9EDROgcRSSrKYk';

    const vendor = (
      await firebase.firestore().collection('vendors').doc(sellerId).get()
    ).data();
    if (!vendor) return;
    const sellerCategoryId = vendor.categoryID;
    const extraCommissionPercentage = vendor.salesCommissionPercentage;

    const fetcher = new SucasaProductFetcher();
    const categories = await fetcher.getCategories();

    await productImporter.syncSubcategories(
      sellerId,
      sellerCategoryId,
      categories,
      SucasaMapper,
    );

    const start = new Date();
    start.setUTCHours(0, 0, 0, 0);
    const end = new Date();
    end.setUTCHours(23, 59, 59, 999);

    const batcher = new SucasaProductBatcher(productImporter, {
      sellerId: sellerId,
      sellerCategoryId: sellerCategoryId,
      extraCommissionPercentage,
    });

    const hasMappings = await productImporter.hasProductsMappings(sellerId);

    if (!hasMappings) return Promise.resolve();

    batcher
      .process(categories, {
        mode: 'PARTIAL-SYNC',
      })
      .then(() => {
        /*silent*/
        functions.logger.log('sucasa product sync success');
      })
      .catch((e: any) => {
        functions.logger.log('sucasa product sync error: ' + e.message);
      });
  } catch (e: any) {
    functions.logger.log('sucasa product sync error: ' + e.message);
  }
}
