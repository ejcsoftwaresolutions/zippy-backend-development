import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import recalculateProduct from '../../../utils/vendor-products/recalculate-product';

const admin = require('firebase-admin');

const functions = require('firebase-functions');
import DocumentSnapshot = firestore.DocumentSnapshot;

export const recalculateProductInfo = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  functions.logger.log('Recalculate product data ', context.params.documentId);

  if (context.eventType.indexOf('delete') > -1) return;

  const document = change.after.data();

  const fields = recalculateProduct(document);

  if (!fields) return Promise.resolve();
  if (Object.keys(fields).length == 0) return Promise.resolve();

  try {
    await admin
      .firestore()
      .collection('vendor_products')
      .doc(context.params.documentId)
      .update(fields);
  } catch (e: any) {
    functions.logger.log('Recalculate product error:', e.message);
  }

  return Promise.resolve();
};
