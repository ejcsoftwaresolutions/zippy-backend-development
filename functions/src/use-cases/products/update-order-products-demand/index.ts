import { VENDOR_ORDERS, VENDOR_PRODUCTS } from '../../../constants';

const functions = require('firebase-functions');
const admin = require('firebase-admin');

const updateOrderProductsDemand = async (change: any, context: any) => {
  const oldStatus = change.before.get('status');

  const newStatus = change.after.get('status');

  if (!['Order Placed', 'Order Rejected'].includes(newStatus))
    return Promise.resolve();

  if (newStatus === 'Order Placed' && !!oldStatus) {
    return Promise.resolve(); /* It should only discount when order is created with Order Placed */
  }

  const operation = newStatus === 'Order Placed' ? 'SUBTRACT' : 'ADD';

  const vendorOrder = (
    await admin
      .firestore()
      .collection(VENDOR_ORDERS)
      .doc(context.params.documentId)
      .get()
  )?.data();

  if (!vendorOrder) return Promise.resolve();

  functions.logger.log(
    'Updating order products demand',
    context.params.documentId,
  );

  const mappedProducts = vendorOrder.products.map((p: any) => ({
    ...p,
    vendorID: vendorOrder.vendorID,
    orderId: vendorOrder.orderId,
  }));

  const promises = mappedProducts.map(async (p: any) => {
    return new Promise(async (resolve, reject) => {
      const vendorProductRef = admin
        .firestore()
        .collection(VENDOR_PRODUCTS)
        .doc(p.id);

      const vendorProduct = (await vendorProductRef.get()).data();
      if (!vendorProduct) return resolve({});

      const newSales =
        vendorProduct.salesQuantity +
        p.quantity * (operation === 'SUBTRACT' ? -1 : 1);

      return vendorProductRef.update({
        salesQuantity: newSales >= 0 ? newSales : 0,
      });
    });
  });

  return Promise.all(promises);
};
export default updateOrderProductsDemand;
