const functions = require('firebase-functions');

const deleteVendor = async (snap: any, context: any) => {
  functions.logger.log('Deleting vendor', context.params.documentId);
  // Delete withdrawal details vendor_withdrawal_details

  // Delete earnings summary  vendors_total_earnings

  // Delete application

  // Delete products vendor_products

  // Delete reviews vendor_reviews

  // Delete withdrawal requests vendor_withdrawal_requests

  // Delete firebase auth user

  // Delete user

  // Delete vendor
};

export default deleteVendor;
