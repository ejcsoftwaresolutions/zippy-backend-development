import DateTimeUtils from '../../../utils/datetime-utils';
import { VENDORS } from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';
import { PushNotificationParams } from '../../../utils/models/push-notification';

const admin = require('firebase-admin');
import moment = require('moment-timezone');

const functions = require('firebase-functions');
export const handleShopsOpenings = async (context: any) => {
  const currentDay = DateTimeUtils.format(new Date(), 'dddd').toUpperCase();
  const currentTime = moment(
    moment().tz('America/Caracas').format('HH:mm'),
    'HH:mm',
  );

  const todayWorkingVendors = (
    await admin
      .firestore()
      .collection(VENDORS)
      .where(`schedule.${currentDay}.active`, '==', true)
      .get()
  ).docs.map((v: any) => v.data());

  const vendorsToClose = todayWorkingVendors.filter((v: any) => {
    if (v.status !== 'ACTIVE') return false;
    const todaySchedule = v.schedule[currentDay];
    if (!todaySchedule.endHourTimestamp) return false;

    const closeHour = moment(todaySchedule.endHour, 'HH:mm');

    const remainingMinutesToClose = DateTimeUtils.differenceInMinutesMoment(
      closeHour,
      currentTime,
    );

    return v.isOpen && remainingMinutesToClose == 0;
  });

  const vendorsToOpen = todayWorkingVendors.filter((v: any) => {
    if (v.status !== 'ACTIVE') return false;
    const todaySchedule = v.schedule[currentDay];
    if (!todaySchedule.startHourTimestamp) return false;

    const startHour = moment(todaySchedule.startHour, 'HH:mm');
    const closeHour = moment(todaySchedule.endHour, 'HH:mm');

    const remainingMinutesToOpen = DateTimeUtils.differenceInMinutesMoment(
      startHour,
      currentTime,
    );

    const remainingMinutesToClose = DateTimeUtils.differenceInMinutesMoment(
      closeHour,
      currentTime,
    );

    return (
      !v.isOpen && remainingMinutesToOpen == 0 && remainingMinutesToClose > 0
    );
  });

  await toggleOpenVendors(vendorsToClose, false);

  await toggleOpenVendors(vendorsToOpen, true);
};

function toggleOpenVendors(vendors: any[], isOpen: boolean) {
  functions.logger.log(
    isOpen ? 'vendors to open' : 'vendors to close',
    vendors.map((v) => `${v.id} ${v.name}`),
  );

  const ref = admin.firestore().collection(VENDORS);

  const batch = admin.firestore().batch();

  vendors.forEach((v) => {
    const sfRef = ref.doc(v.id);
    batch.update(sfRef, { isOpen: isOpen });
  });

  // Commit the batch
  batch.commit().then(() => {
    // ...
  });

  vendors.forEach((v) => {
    const notiInfo: PushNotificationParams = isOpen
      ? {
          data: {},
          notification: {
            title: 'Hemos abierto tu tienda! 🔓🛒',
            body: `🔥Activo para recibir ordenes!🔥.`,
          },
        }
      : {
          data: {},
          notification: {
            title: '🕖 Tu tienda se ha cerrado por hoy 🔐',
            body: `Ya hemos bajado la santamaría por ti!, echa un vistazo a tus ventas de hoy 💵.`,
          },
        };
    CloudFunctionsAppUtils.sendPushNotification(notiInfo, 'VENDOR', v.id).then(
      () => {
        /*silent*/
      },
    );
  });
}
