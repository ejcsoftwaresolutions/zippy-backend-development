const admin = require('firebase-admin');
import {VENDOR_ORDERS, VENDORS_TOTAL_EARNINGS} from '../../../constants';

const functions = require('firebase-functions');

const updateVendorTotalEarnings = async (change: any, context: any) => {
    functions.logger.log('Updating vendor total earnings', context.params.documentId,);

    const newStatus = change.after.get('status');
    const oldStatus = change.before.get('status');

    if (newStatus === oldStatus) return Promise.resolve();

    if (newStatus !== 'Order Completed') return Promise.resolve();

    const orderRef = admin
    .firestore()
    .collection(VENDOR_ORDERS)
    .doc(context.params.documentId);

    const vendorOrder = (
        await orderRef.get()
    ).data();

    if (!vendorOrder) return Promise.resolve();

    const vendorOrderCommission = vendorOrder.vendorCommission;

    if (vendorOrderCommission.processed) return Promise.resolve();

    const newEarning = vendorOrderCommission ? vendorOrderCommission.earned : vendorOrder.subtotal;

    const vendorEarnings = (
        await admin
        .firestore()
        .collection(VENDORS_TOTAL_EARNINGS)
        .doc(vendorOrder.vendorID)
        .get()
    ).data();

    if (vendorEarnings) {
        const demandDailyDocRef = await admin
        .firestore()
        .collection(VENDORS_TOTAL_EARNINGS)
        .doc(vendorOrder.vendorID);

        return demandDailyDocRef.update({
            total: admin.firestore.FieldValue.increment(parseFloat(newEarning)), lastUpdate: new Date(),
        });
    }

    const info = {
        vendorID: vendorOrder.vendorID, total: parseFloat(newEarning), lastUpdate: new Date(),
    };

    await admin
    .firestore()
    .collection(VENDORS_TOTAL_EARNINGS)
    .doc(vendorOrder.vendorID)
    .set(info);

    return orderRef.update({
        ['vendorCommission.processed']: true,
    });
};

export default updateVendorTotalEarnings;
