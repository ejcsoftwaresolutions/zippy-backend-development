import {Change, EventContext} from 'firebase-functions';
import {firestore} from 'firebase-admin';
import CloudFunctionsAppUtils from '../../../utils/app';
import DocumentSnapshot = firestore.DocumentSnapshot;

const functions = require('firebase-functions');

export const syncVendorLocation = async (change: Change<DocumentSnapshot>, context: EventContext,) => {
    const vendor = change.after.data();
    const lastVendor = change.before.data();

    if (!vendor) return Promise.resolve();

    functions.logger.log('Updating vendor location', context.params.documentId);

    if (!vendor.location) return Promise.resolve();

    if (!lastVendor) {
        /* onCreated*/
        return CloudFunctionsAppUtils.syncVendorGeolocation(vendor.id, [vendor.location.latitude, vendor.location.longitude,]);
    }

    /*If it hasn't changed*/
    if (lastVendor.location.latitude === vendor.location.latitude && lastVendor.location.longitude === vendor.location.longitude) return Promise.resolve();

    return CloudFunctionsAppUtils.syncVendorGeolocation(vendor.id, [vendor.location.latitude, vendor.location.longitude,]);
};
