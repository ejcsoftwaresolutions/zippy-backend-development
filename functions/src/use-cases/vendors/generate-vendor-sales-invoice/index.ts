const admin = require('firebase-admin');
import {salesInvoiceHtml} from './sales-invoice';
import {VENDOR_ORDERS} from '../../../constants';
import moment = require('moment');

const functions = require('firebase-functions');
const pdf = require('html-pdf');
const uuid = require('uuid');

function getDate(firebaseDate: any) {
    return new Date(firebaseDate.seconds * 1000);
}

const generateVendorSalesInvoice = async (change: any, context: any) => {
    functions.logger.log('Generating sales invoice', context.params.documentId);

    //return Promise.resolve();
    const newStatus = change.after.get('status');

    if (newStatus !== 'REQUESTED') return Promise.resolve();

    const invoiceRef = await admin
    .firestore()
    .collection('vendor_invoices')
    .doc(context.params.documentId);

    const vendorInvoiceQ = await invoiceRef.get();

    const vendorInvoice = vendorInvoiceQ.data();

    if (!vendorInvoice) return Promise.resolve();

    const vendorSalesQ = await admin
    .firestore()
    .collection(VENDOR_ORDERS)
    .where('vendorID', '==', vendorInvoice.vendorId)
    .where('createdAt', '>=', moment(getDate(vendorInvoice.start)).startOf('day').toDate(),)
    .where('createdAt', '<=', moment(getDate(vendorInvoice.end)).endOf('day').toDate(),)
    .orderBy('createdAt', 'asc')
    .get();

    const vendorSales = vendorSalesQ.docs.map((s: any) => {
        const sale = s.data();
        return {
            date: moment(getDate(sale.createdAt)).format('DD/MM/YYYY HH:mm'),
            subtotal: parseFloat(sale.subtotal),
            total: parseFloat(sale.vendorCommission?.earned ?? 0),
        };
    });

    const subtotal = vendorSales
    .map((s: any) => s.subtotal)
    .reduce((a: any, b: any) => a + b, 0);
    const total = vendorSales
    .map((s: any) => s.total)
    .reduce((a: any, b: any) => a + b, 0);

    functions.logger.log('Total de ventas', vendorSales.length);

    const fileName = `vendors/${vendorInvoice.vendorId}/invoices/${context.params.documentId}${moment().format('DD/MM/YYYY HH:mm:ss')}.pdf`;

    const fileRef = admin.storage().bucket().file(fileName, {});

    const content = salesInvoiceHtml
    .replace('{{PERIOD}}', `${moment(getDate(vendorInvoice.start)).format('DD/MM/YYYY')} al ${moment(getDate(vendorInvoice.end),).format('DD/MM/YYYY')}`,)
    .replace('{{PRODUCTS_HTML}}', `
     <div class="product">
            ${vendorSales
    .map((s: any) => {
        return `<div class="field">
              <p>#${s.code} ${s.date}</p>
                <div class="totals">
                    <p style="margin-right: 3px">${s.subtotal} USD</p>
                    <p>${s.total} USD</p>
                </div>
              </div>`;
    })
    .join('')}
            </div>
    `,)
    .replace('{{SUBTOTAL}}', `${subtotal}`)
    .replace('{{TOTAL}}', `${total}`);

    await invoiceRef.update({status: 'GENERATING'});

    return new Promise((resolve) => {
        pdf
        .create(content, {
            /*    orientation: 'portrait',
             type: 'pdf', */
            timeout: '100000', width: 300, height: 800,
        })
        .toStream(async (err: any, stream: any) => {
            if (err) {
                await invoiceRef.update({
                    status: 'ERROR',
                });

                functions.logger.log('Error', err);
                return;
            }

            await stream.pipe(fileRef.createWriteStream({
                metadata: {
                    contentType: 'application/pdf', metadata: {
                        firebaseStorageDownloadTokens: uuid.v4(), lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                    },
                }, resumable: false, public: true,
            }),);

            await invoiceRef.update({
                status: 'CREATED', url: fileRef.publicUrl(), updatedAt: new Date(),
            });

            resolve({});
        });
    });
};

export default generateVendorSalesInvoice;
