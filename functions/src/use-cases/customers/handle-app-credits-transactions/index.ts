const admin = require('firebase-admin');
import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import { CREDIT_LOG_REF, CUSTOMERS } from '../../../constants';
import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');
import DocumentSnapshot = firestore.DocumentSnapshot;

export const handleAppCreditsTransactions = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  functions.logger.log('Credit transaction ', context.params.documentId);

  const document = change.after.data();

  const oldDocument = change.before.data();

  if (!document && oldDocument) {
    return resetUserLastCredits(oldDocument.userId);
  }

  if (!document) return Promise.resolve();

  const logData = document;

  if (logData.processed) return Promise.resolve();

  await processTransactionLog({
    ref: change.after.ref,
    amount: logData.amount,
    operation: logData.operation,
    userId: logData.userId,
    concept: logData.concept,
  });

  if (logData.operation === 'CREDIT' && logData.concept === 'OTHER') {
    await sendOtherCreditNotification({
      userId: logData.userId,
      amount: logData.amount,
      description: logData.conceptDescription,
    });
  }

  return Promise.resolve();
};

async function getUserLastCreditAmount(userId: string) {
  const lastLog = (
    await admin
      .firestore()
      .collection(CREDIT_LOG_REF)
      .where('userId', '==', userId)
      .where('processed', '==', true)
      .orderBy('date', 'desc')
      .get()
  ).docs[0]?.data();

  if (!lastLog) return 0;

  return lastLog.newCreditAmount ?? 0;
}

async function processTransactionLog(params: {
  ref: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>;
  operation: 'CREDIT' | 'DEBIT';
  userId: string;
  amount: number;
  concept: string;
}) {
  const lastCreditAmount = await getUserLastCreditAmount(params.userId);

  let newCreditAmount: number =
    lastCreditAmount + params.amount * (params.operation == 'CREDIT' ? 1 : -1);
  newCreditAmount =
    newCreditAmount < 0 ? 0 : parseFloat(newCreditAmount.toFixed(2));

  await params.ref.update({
    processed: true,
    newCreditAmount: newCreditAmount,
    lastCreditAmount: lastCreditAmount,
    processedAt: new Date(),
  });

  await admin.firestore().collection(CUSTOMERS).doc(params.userId).update({
    credits: newCreditAmount,
    lastAppCreditUpdate: new Date(),
  });
}

async function resetUserLastCredits(userId: string) {
  const lastCreditAmount = await getUserLastCreditAmount(userId);

  await admin.firestore().collection(CUSTOMERS).doc(userId).update({
    credits: lastCreditAmount,
    lastAppCreditUpdate: new Date(),
  });
}

export async function sendOtherCreditNotification({
  userId,
  amount,
  description,
}) {
  await CloudFunctionsAppUtils.sendPushNotification(
    {
      notification: {
        title: `Se han acreditado $${amount} en Zippi Credits!`,
        body: `Concepto: ${description}`,
      },
    },
    'CUSTOMER',
    userId,
  );
}

export async function createRechargeCreditLog({
  userId,
  amount,
  metadata,
}: {
  userId: string;
  amount: number;
  metadata: {
    rechargeId: string;
  };
}) {
  return {
    id: await CloudFunctionsAppUtils.createId(),
    date: new Date(),
    operation: 'CREDIT',
    concept: 'RECHARGE',
    processed: false,
    metadata: metadata,
    userId: userId,
    amount: amount,
  };
}

export async function createPurchaseCreditLog({
  userId,
  amount,
  metadata,
}: {
  userId: string;
  amount: number;
  metadata: {
    orderId: string;
    orderTotal: number;
    vendorId: string;
    vendorName: string;
  };
}) {
  return {
    id: await CloudFunctionsAppUtils.createId(),
    date: new Date(),
    operation: 'DEBIT',
    concept: 'PURCHASE',
    processed: false,
    metadata: metadata,
    userId: userId,
    amount: amount,
  };
}

export async function createRefundCreditLog({
  userId,
  amount,
  metadata,
}: {
  userId: string;
  amount: number;
  metadata: {
    orderId: string;
    orderCode: string;
  };
}) {
  return {
    id: await CloudFunctionsAppUtils.createId(),
    date: new Date(),
    operation: 'CREDIT',
    concept: `Reembolso Zippi Credits en compra #${metadata.orderCode}`,
    processed: false,
    metadata: metadata,
    userId: userId,
    amount: amount,
  };
}

export async function createReferralCreditLog({
  userId,
  amount,
  metadata,
}: {
  userId: string;
  amount: number;
  metadata: {
    conditionsMatched: {
      minOrderTotal: {
        orderId: string;
        orderTotal: number;
        minValue: number;
      };
    };
    code: string;
    referrerUserId: string;
    referrerUserName: string;
  };
}) {
  return {
    id: await CloudFunctionsAppUtils.createId(),
    date: new Date(),
    operation: 'CREDIT',
    concept: 'REFERRAL_PRIZE',
    processed: false,
    metadata: metadata,
    userId: userId,
    amount: amount,
  };
}

export async function createReferrerCreditLog({
  userId,
  amount,
  metadata,
}: {
  userId: string;
  amount: number;
  metadata: {
    conditionsMatched: {
      minOrderTotal: {
        orderId: string;
        orderTotal: number;
        minValue: number;
      };
    };
    code: string;
    referralUserId: string;
    referralUserName: string;
  };
}) {
  return {
    id: await CloudFunctionsAppUtils.createId(),
    date: new Date(),
    operation: 'CREDIT',
    concept: 'REFERRER_PRIZE',
    processed: false,
    metadata: metadata,
    userId: userId,
    amount: amount,
  };
}
