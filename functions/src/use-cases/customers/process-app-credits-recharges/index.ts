const admin = require('firebase-admin');
import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import { createRechargeCreditLog } from '../handle-app-credits-transactions';
import { CREDIT_LOG_REF } from '../../../constants';

import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');
import DocumentSnapshot = firestore.DocumentSnapshot;

export const processAppCreditsRecharges = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  functions.logger.log('Credit recharge ', context.params.documentId);

  const document = change.after.data();

  const oldDocument = change.before.data();

  if (!document && oldDocument && oldDocument.logId) {
    return resetUserLastCredits(oldDocument.logId);
  }

  if (!document) return Promise.resolve();

  const reqData = document;

  if (document && !oldDocument && !reqData.approved) {
    await sendAdminEmail(
      process.env.ZIPPI_RECHARGES_EMAIL as string,
      reqData.userFullName,
      document,
    );
  }

  if (!reqData.approved) return Promise.resolve();
  if (reqData.processed) return Promise.resolve();

  return approveAppCreditRecharge({
    ref: change.after.ref,
    amount: reqData.amount,
    userId: reqData.userId,
  });
};

async function approveAppCreditRecharge(params: {
  ref: FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>;
  userId: string;
  amount: number;
}) {
  const rechargeLog = await createRechargeCreditLog({
    userId: params.userId,
    amount: params.amount,
    metadata: {
      rechargeId: params.ref.id,
    },
  });

  await admin
    .firestore()
    .collection(CREDIT_LOG_REF)
    .doc(rechargeLog.id)
    .set(rechargeLog);

  await params.ref.update({
    processed: true,
    approvedAt: new Date(),
    logId: rechargeLog.id,
  });

  sendUserApproveNotification(params.userId, params.amount)
    .then(() => {
      /*silent*/
    })
    .then((e: any) => {
      /*silent*/
    });
}

async function resetUserLastCredits(logId: string) {
  return admin.firestore().doc(`${CREDIT_LOG_REF}/${logId}`).delete(); // triggers another cloud function
}

async function sendAdminEmail(
  email: string,
  userFullName: any,
  rechargeData: any,
) {
  return CloudFunctionsAppUtils.sendEmail(email, {
    subject: `${
      process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'
    } - Zippi Credits Recarga, de $${rechargeData.amount}`,
    text: `Hola, ${userFullName} ha reportado el pago de $${rechargeData.amount} usando ${rechargeData.paymentReport.method}`,
    html: `
              <div>
                <p> El usuario
                ${userFullName} ha reportado el pago de $${
      rechargeData.amount
    } usando ${rechargeData.paymentReport.method} 
                </p>
                <br/>
                 <p> ID: <strong>${rechargeData.id}</strong> </p>
                <p> <strong> ${JSON.stringify(
                  rechargeData.paymentReport.metadata ?? {},
                )}</strong> </p>
              </div>
          `,
  });
}

async function sendUserApproveNotification(userId: string, credit: number) {
  const { email, configurations } =
    await CloudFunctionsAppUtils.findUserNotificationInfo(userId, 'customer');

  try {
    await CloudFunctionsAppUtils.sendPushNotification(
      {
        notification: {
          title: `Tu recarga de Zippi Credits ha sido aprobada!`,
          body: `Se han cargado $${credit} a tu Zippi Wallet!`,
        },
      },
      'CUSTOMER',
      userId,
    );
  } catch (e) {
    /*silent*/
  }

  try {
    if (!configurations?.receiveEmails) return;

    await CloudFunctionsAppUtils.sendEmail(email as string, {
      subject: `Zippi - Tu recarga de Zippi Credits ha sido aprobada!`,
      text: `Se han cargado $${credit} a tu Zippi Wallet!`,
      html: `Se han cargado $${credit} a tu Zippi Wallet!`,
    });
  } catch (e) {
    /*silent*/
  }
}
