import { Change, EventContext } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import { CREDIT_LOG_REF, CUSTOMERS, VENDOR_ORDERS } from '../../../constants';
import {
  createReferralCreditLog,
  createReferrerCreditLog,
} from '../handle-app-credits-transactions';

import CloudFunctionsAppUtils from '../../../utils/app';

const functions = require('firebase-functions');
import DocumentSnapshot = firestore.DocumentSnapshot;

const admin = require('firebase-admin');
const MIN_ORDER_TOTAL = 20;
const PRIZE = 5;
export const handleCustomerReferralPrizes = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const order = change.after.data();

  /*  const oldOrder = change.before.data();*/

  if (!order) return Promise.resolve();
  const customerId = order.customer.id;

  const customer = await CloudFunctionsAppUtils.findAccount(customerId);
  if (!customer) return Promise.resolve();

  const referrerCode = customer.referrerCode;
  if (!referrerCode || referrerCode === '') return Promise.resolve();

  const referrer = await findReferrerAccount(referrerCode);

  if (!referrer) return Promise.resolve();
  if (customer.id === referrer.id) return Promise.resolve();

  if (order.status == 'Order Rejected') return Promise.resolve();

  if (!(await isCustomerFirstOrder(customerId))) return Promise.resolve();

  if (await customerAlreadyReceivedReferralPrize(customerId))
    return Promise.resolve();

  if (order.total < MIN_ORDER_TOTAL) return Promise.resolve();

  functions.logger.log('New referral prizes', referrerCode);

  try {
    await giveReferralPrize(customerId, {
      minOrderTotal: MIN_ORDER_TOTAL,
      referrer: referrer,
      code: referrerCode,
      orderTotal: order.total,
      orderId: order.id,
    });
  } catch (e) {
    functions.logger.log('Error at giving referral prize', e);
  }

  try {
    await giveReferrerPrize(referrer.id, {
      minOrderTotal: MIN_ORDER_TOTAL,
      referral: customer,
      code: referrerCode,
      orderTotal: order.total,
      orderId: order.id,
    });
  } catch (e) {
    functions.logger.log('Error at giving referrer prize', e);
  }
};

async function isCustomerFirstOrder(userId: string): Promise<boolean> {
  const item = await admin
    .firestore()
    .collection(`${VENDOR_ORDERS}`)
    .where('customer.id', '==', userId)
    .get();

  return item.size === 1;
}

async function customerAlreadyReceivedReferralPrize(userId: string) {
  const item = await admin
    .firestore()
    .collection(`${CREDIT_LOG_REF}`)
    .where('userId', '==', userId)
    .where('concept', '==', 'REFERRAL_PRIZE')
    .get();

  return !item.empty;
}

async function findReferrerAccount(code: string) {
  const item = await admin
    .firestore()
    .collection(`${CUSTOMERS}`)
    .where('referralCode', '==', code)
    .get();

  const itemsDto = item.docs.map((d: any) => d.data());

  if (itemsDto.length === 0) return;
  const user = itemsDto[0];

  return CloudFunctionsAppUtils.findAccount(user.id);
}

async function giveReferralPrize(
  userId: string,
  details: {
    referrer: any;
    orderId: string;
    orderTotal: number;
    code: string;
    minOrderTotal: number;
  },
) {
  const log = await createReferralCreditLog({
    userId: userId,
    amount: PRIZE,
    metadata: {
      conditionsMatched: {
        minOrderTotal: {
          minValue: details.minOrderTotal,
          orderTotal: details.orderTotal,
          orderId: details.orderId,
        },
      },
      code: details.code,
      referrerUserName: `${details.referrer.firstName} ${
        details.referrer.lastName ?? ''
      }`,
      referrerUserId: details.referrer.id,
    },
  });

  await admin.firestore().collection(CREDIT_LOG_REF).doc(log.id).set(log);

  /*Notify user*/

  try {
    await CloudFunctionsAppUtils.sendPushNotification(
      {
        notification: {
          title: `Has ganado $${PRIZE} en Zippi Credits!`,
          body: `Has ganado $${PRIZE} en Zippi Credits por procesar tu primera compra!`,
        },
      },
      'CUSTOMER',
      userId,
    );
  } catch (e) {
    /*silent*/
  }

  try {
    const { email, configurations } =
      await CloudFunctionsAppUtils.findUserNotificationInfo(userId, 'customer');

    if (!configurations?.receiveEmails) return;

    await CloudFunctionsAppUtils.sendEmail(email as string, {
      subject: `Zippi - Has ganado $${PRIZE} en Zippi Credits!`,
      text: `Zippi - Has ganado $${PRIZE} en Zippi Credits!`,
      html: `
       <div>
          <p>
          Felicidades! Has ganado $${PRIZE} en Zippi Credits por procesar tu primera compra!, tu amigo ${log.metadata.referrerUserName} también ha ganado!. 
          </p>
          <br/>
          <p>
          Comparte tu propio código con tus panas y gana aún más!
          </p>
          <p> <strong> Gracias por preferirnos.</strong> </p>
        </div>
      `,
    });
  } catch (e) {
    /*silent*/
  }
}

async function giveReferrerPrize(
  userId: string,
  details: {
    referral: any;
    orderId: string;
    orderTotal: number;
    code: string;
    minOrderTotal: number;
  },
) {
  const log = await createReferrerCreditLog({
    userId: userId,
    amount: PRIZE,
    metadata: {
      conditionsMatched: {
        minOrderTotal: {
          minValue: details.minOrderTotal,
          orderTotal: details.orderTotal,
          orderId: details.orderId,
        },
      },
      code: details.code,
      referralUserName: `${details.referral.firstName} ${
        details.referral.lastName ?? ''
      }`,
      referralUserId: details.referral.id,
    },
  });

  await admin.firestore().collection(CREDIT_LOG_REF).doc(log.id).set(log);

  /*Notify user*/

  const { email, configurations } =
    await CloudFunctionsAppUtils.findUserNotificationInfo(userId, 'customer');

  try {
    await CloudFunctionsAppUtils.sendPushNotification(
      {
        notification: {
          title: `Has ganado $${PRIZE} en Zippi Credits!`,
          body: `Has ganado $${PRIZE} en Zippi Credits. tu amigo ${log.metadata.referralUserName} se unió y realizó su primera compra!`,
        },
      },
      'CUSTOMER',
      userId,
    );
  } catch (e) {
    /*silent*/
  }
  try {
    if (!configurations?.receiveEmails) return;

    await CloudFunctionsAppUtils.sendEmail(email as string, {
      subject: `Zippi - Has ganado $${PRIZE} en Zippi Credits!`,
      text: `Has ganado $${PRIZE} en Zippi Credits. Tu amigo ${log.metadata.referralUserName} se unió y realizó su primera compra!`,
      html: `
       <div>
          <p>
          Felicidades! Haz ganado $${PRIZE} en Zippi Credits. tu amigo ${log.metadata.referralUserName} se unió y realizó su primera compra!
          </p>
          <br/>
          <p>
            Sigue enviando tu código a más panas y gana aún más!
          </p>
          <p> <strong> Gracias por preferirnos.</strong> </p>
        </div>
      `,
    });
  } catch (e) {
    /*silent*/
  }
}
