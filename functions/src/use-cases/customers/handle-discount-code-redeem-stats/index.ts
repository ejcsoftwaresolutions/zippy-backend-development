import { Change, EventContext } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import { DISCOUNT_CODES_REDEEMS_STATS } from '../../../constants';
import DatetimeUtils from '../../../utils/datetime-utils';

import DocumentSnapshot = firestore.DocumentSnapshot;
import FieldValue = firestore.FieldValue;

const admin = require('firebase-admin');
export const handleDiscountCodeRedeemStats = async (
  change: Change<DocumentSnapshot>,
  context: EventContext,
) => {
  const redeem = change.after.data();
  const old = change.before.data();

  if (!redeem && old) {
    /*delete*/
    return updateCodeCounter(old.code.id, 'SUBTRACT');
  }

  if (redeem && old) {
    return Promise.resolve();
  }

  if (!redeem) return Promise.resolve();

  return updateCodeCounter(redeem.code.id, 'ADD');
};

async function updateCodeCounter(codeId: string, operation: string) {
  const date = DatetimeUtils.format(new Date(), 'YYYY-MM-DD');
  const id = codeId + date;

  const ref = admin
    .firestore()
    .collection(DISCOUNT_CODES_REDEEMS_STATS)
    .doc(id);

  if ((await ref.get()).exists) {
    return ref.update({
      total: FieldValue.increment(operation === 'ADD' ? 1 : -1),
    });
  }

  return ref.set({
    id: id,
    codeId: codeId,
    timestamp: firestore.Timestamp.now(),
    date: date,
    total: operation === 'ADD' ? 1 : 0,
  });
}
