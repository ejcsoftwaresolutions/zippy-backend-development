declare const TextUtils: {
    truncate(text: string, max: number): string;
    insertAt(str: any, sub: any, pos: any): string;
    capitalize(str: string): string;
};
export default TextUtils;
