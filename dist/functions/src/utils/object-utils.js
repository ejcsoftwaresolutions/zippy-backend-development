"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const get_1 = require("lodash/get");
const ObjectUtils = {
    get: get_1.default,
    isNull: lodash_1.isNull,
    omit: lodash_1.omit,
    mapValues: lodash_1.mapValues,
    omitUnknown: (object) => {
        return (0, lodash_1.omitBy)(object, lodash_1.isNil);
    },
    pick: lodash_1.pick,
    merge: lodash_1.merge,
    isEqual: lodash_1.isEqual,
    sort(obj) {
        return Object.keys(obj)
            .sort((e, i) => {
            return e > i ? 0 : 1;
        })
            .reduce(function (result, key) {
            result[key] = obj[key];
            return result;
        }, {});
    }
};
exports.default = ObjectUtils;
//# sourceMappingURL=object-utils.js.map