import SellerProduct from '../models/seller-product';
import FirestoreApiRepository from '../../firestore-api-repository';
export default class VendorProductsRepository extends FirestoreApiRepository {
    saveVendorProducts(products: SellerProduct[]): Promise<void>;
    getProductsById(ids: string[]): Promise<any>;
    getVendorProductsMappings(sellerId: string): Promise<{
        [p: string]: string;
    }>;
    saveVendorProductsMappings(sellerId: string, map: {
        [p: string]: string;
    }): Promise<void>;
    getVendorProductSubcategoriesMappings(sellerId: string): Promise<{
        [p: string]: string;
    }>;
    saveVendorSubcategoriesProductsMappings(sellerId: string, map: {
        [p: string]: string;
    }): Promise<void>;
    getSubcategories(categoryId: string): Promise<any>;
    getAppExchangeRate(): Promise<number>;
}
