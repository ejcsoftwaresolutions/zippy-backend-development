"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = require("firebase-admin");
const external_product_mapper_1 = require("../mappers/external-product-mapper");
const firestore_api_repository_1 = require("../../firestore-api-repository");
class VendorProductsRepository extends firestore_api_repository_1.default {
    async saveVendorProducts(products) {
        const entities = external_product_mapper_1.default.toPersistenceFromArray(products);
        if (entities.length === 0)
            return Promise.resolve();
        const batch = firebase_admin_1.default.firestore().batch();
        const ref = firebase_admin_1.default.firestore().collection('vendor_products');
        entities.forEach((product, index) => {
            const isNew = products[index].isNew;
            const productRef = ref.doc(product.id);
            return isNew
                ? batch.create(productRef, product)
                : batch.update(productRef, product);
        });
        await batch.commit();
    }
    async getProductsById(ids) {
        return this.getDocs('vendor_products', {
            filters: [
                {
                    field: 'id',
                    operator: 'in',
                    value: ids,
                },
            ],
        });
    }
    async getVendorProductsMappings(sellerId) {
        const mappings = (await firebase_admin_1.default
            .database()
            .ref(`external_vendor_product_mappings/${sellerId}`)
            .get()).val();
        return Promise.resolve(mappings !== null && mappings !== void 0 ? mappings : {});
    }
    async saveVendorProductsMappings(sellerId, map) {
        return firebase_admin_1.default
            .database()
            .ref(`external_vendor_product_mappings/${sellerId}`)
            .update(map);
    }
    async getVendorProductSubcategoriesMappings(sellerId) {
        const mappings = (await firebase_admin_1.default
            .database()
            .ref(`external_vendor_subcategories_mappings/${sellerId}`)
            .get()).val();
        return Promise.resolve(mappings !== null && mappings !== void 0 ? mappings : {});
    }
    async saveVendorSubcategoriesProductsMappings(sellerId, map) {
        return firebase_admin_1.default
            .database()
            .ref(`external_vendor_subcategories_mappings/${sellerId}`)
            .update(map);
    }
    async getSubcategories(categoryId) {
        const docs = await firebase_admin_1.default
            .firestore()
            .collection('vendor_categories')
            .where('parentID', '==', categoryId)
            .get();
        return docs.docs.map((d) => d.data());
    }
    async getAppExchangeRate() {
        var _a, _b, _c;
        const doc = await firebase_admin_1.default
            .firestore()
            .collection('app_configurations')
            .doc('GLOBAL_FEES')
            .get();
        return (_c = (_b = (_a = doc.data()) === null || _a === void 0 ? void 0 : _a.exchangeRates) === null || _b === void 0 ? void 0 : _b.bolivar) !== null && _c !== void 0 ? _c : 1;
    }
}
exports.default = VendorProductsRepository;
//# sourceMappingURL=vendor-products-repository.js.map