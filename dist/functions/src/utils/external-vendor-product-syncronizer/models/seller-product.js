"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currency_price_1 = require("../../models/currency-price");
const model_1 = require("../../models/model");
class SellerProduct extends model_1.default {
    get id() {
        return this.props.id;
    }
    get name() {
        return this.props.name;
    }
    get isNew() {
        return this.props.isNew;
    }
    get imageUrl() {
        return this.props.imageUrl;
    }
    get categoryId() {
        return this.props.categoryId;
    }
    get subcategoryId() {
        return this.props.subcategoryId;
    }
    get description() {
        return this.props.description;
    }
    get stockCount() {
        return this.props.stockCount;
    }
    get available() {
        return this.props.available;
    }
    get photo() {
        if (this.props.photos.length === 0)
            return null;
        return this.props.photos[0].url;
    }
    static fromPrimitives(props) {
        return new SellerProduct({
            ...props,
            price: currency_price_1.default.fromPrimitives(props.price),
            discountPrice: currency_price_1.default.fromPrimitives(props.discountPrice),
            type: props.type,
            isNew: false,
        });
    }
    static fromNew(props) {
        return new SellerProduct({
            ...props,
            price: currency_price_1.default.fromPrimitives(props.price),
            discountPrice: currency_price_1.default.fromPrimitives(props.discountPrice),
            available: true,
            type: 'REGULAR',
            isNew: true,
        });
    }
    toPrimitives() {
        return {
            ...this.props,
            price: this.props.price.toPrimitives(),
            discountPrice: this.props.discountPrice.toPrimitives(),
            type: this.props.type,
        };
    }
}
exports.default = SellerProduct;
//# sourceMappingURL=seller-product.js.map