import CurrencyPrice, { CurrencyPricePrimitiveProps } from '../../models/currency-price';
import Model from '../../models/model';
type ProductPhoto = {
    url: string;
    id: string;
    mode?: string;
};
type ProductType = 'REGULAR' | 'BUNDLE';
interface SellerProductProps {
    id: string;
    name: string;
    description: string;
    categoryId: string;
    subcategoryId: string;
    sellerId: string;
    price: CurrencyPrice;
    discountPrice: CurrencyPrice;
    available: boolean;
    photos: ProductPhoto[];
    stockCount?: number;
    imageUrl?: string;
    type: ProductType;
    isNew: boolean;
}
export type SellerProductPrimitiveProps = Omit<SellerProductProps, 'price' | 'discountPrice' | 'addons' | 'type'> & {
    price: CurrencyPricePrimitiveProps;
    discountPrice: CurrencyPricePrimitiveProps;
    photos: ProductPhoto[];
    type: string;
};
export default class SellerProduct extends Model<SellerProductProps> {
    get id(): string;
    get name(): string;
    get isNew(): boolean;
    get imageUrl(): string;
    get categoryId(): string;
    get subcategoryId(): string;
    get description(): string;
    get stockCount(): number;
    get available(): boolean;
    get photo(): string;
    static fromPrimitives(props: SellerProductPrimitiveProps): SellerProduct;
    static fromNew(props: Pick<SellerProductPrimitiveProps, 'id' | 'name' | 'description' | 'photos' | 'price' | 'discountPrice' | 'sellerId' | 'categoryId' | 'subcategoryId' | 'stockCount'>): SellerProduct;
    toPrimitives(): SellerProductPrimitiveProps;
}
export {};
