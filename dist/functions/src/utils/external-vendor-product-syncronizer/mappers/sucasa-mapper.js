"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const array_utils_1 = require("../../array-utils");
const currency_price_1 = require("../../models/currency-price");
const id_1 = require("../../models/id");
const seller_product_1 = require("../models/seller-product");
const bs_currency_1 = require("../../models/bs-currency");
class SucasaMapper {
    static toDomainFromArray(dtos, config) {
        return dtos.map((dto) => {
            var _a, _b, _c;
            const appId = config.mappings.products[dto.id];
            const existingProduct = config.mappings.existingProducts[appId];
            const subcategoryId = config.mappings.subcategories[dto.defaultCategoryId];
            const price = (() => {
                var _a;
                const n = parseFloat(dto.price);
                return parseFloat(((n * 100) / ((_a = 100 - config.extraCommissionPercentage) !== null && _a !== void 0 ? _a : 0)).toFixed(2));
            })();
            const discountPrice = (() => {
                if (!existingProduct)
                    return price;
                return (price -
                    parseFloat(((existingProduct.discountRate * price) / 100).toFixed(2)));
            })();
            const getName = (string) => {
                return string.replace('*Producto disponible en pocas horas!!', '');
            };
            const photos = dto.media
                ? dto.media.images.map((p) => {
                    var _a, _b, _c;
                    return {
                        id: new id_1.default().value,
                        mode: 'CONTAIN',
                        url: p.imageOriginalUrl,
                        versions: [
                            {
                                id: 'DEFAULT',
                                url: p.imageOriginalUrl,
                                mode: 'CONTAIN',
                            },
                            {
                                id: 'THUMBNAIL',
                                url: (_a = p.image160pxUrl) !== null && _a !== void 0 ? _a : p.imageOriginalUrl,
                                mode: 'CONTAIN',
                            },
                            {
                                id: 'MEDIUM',
                                url: (_b = p.image800pxUrl) !== null && _b !== void 0 ? _b : p.imageOriginalUrl,
                                mode: 'CONTAIN',
                            },
                            {
                                id: 'HIGH',
                                url: (_c = p.image1500pxUrl) !== null && _c !== void 0 ? _c : p.imageOriginalUrl,
                                mode: 'CONTAIN',
                            },
                        ],
                    };
                })
                : [];
            return !!appId
                ? seller_product_1.default.fromPrimitives({
                    id: appId !== null && appId !== void 0 ? appId : new id_1.default().value,
                    name: getName(dto.name),
                    description: (_a = dto.description.replace(/<[^>]*>?/gm, '')) !== null && _a !== void 0 ? _a : '',
                    photos: photos,
                    price: currency_price_1.default.fromPrimitives({
                        value: price,
                        currency: bs_currency_1.default.toPrimitives(),
                    }).toPrimitives(),
                    discountPrice: currency_price_1.default.fromPrimitives({
                        value: discountPrice,
                        currency: bs_currency_1.default.toPrimitives(),
                    }).toPrimitives(),
                    sellerId: config.sellerId,
                    categoryId: config.categoryId,
                    subcategoryId: subcategoryId,
                    available: true,
                    type: 'REGULAR',
                    isNew: false,
                })
                : seller_product_1.default.fromNew({
                    id: appId !== null && appId !== void 0 ? appId : new id_1.default().value,
                    name: getName(dto.name),
                    description: (_b = dto.description.replace(/<[^>]*>?/gm, '')) !== null && _b !== void 0 ? _b : '',
                    photos: photos,
                    price: currency_price_1.default.fromPrimitives({
                        value: price,
                        currency: bs_currency_1.default.toPrimitives(),
                    }).toPrimitives(),
                    discountPrice: currency_price_1.default.fromPrimitives({
                        value: discountPrice,
                        currency: bs_currency_1.default.toPrimitives(),
                    }).toPrimitives(),
                    sellerId: config.sellerId,
                    categoryId: config.categoryId,
                    subcategoryId: subcategoryId,
                    stockCount: (_c = dto.quantity) !== null && _c !== void 0 ? _c : 10,
                });
        });
    }
    static mapCategories(categories, appCategories) {
        const newMappings = categories.reduce((acc, actual) => {
            const found = this.findCorrespondingCategory(actual, appCategories);
            return { ...acc, [actual.id]: found.id };
        }, {});
        return newMappings;
    }
    static findCorrespondingCategory(category, appCategories) {
        const misceCate = appCategories.find((c) => c.title.toLowerCase() === 'misceláneos');
        if (!misceCate)
            throw new Error('MISCELANEOUS_SUBCAT_NOT_CREATED');
        const results = array_utils_1.ArrayUtils.filterLike(appCategories.map((c) => ({
            ...c,
            title: c.title
                .toLowerCase()
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, ''),
        })), 'title', category.name
            .toLowerCase()
            .normalize('NFD')
            .replace(/[\u0300-\u036f]/g, ''));
        return results.length > 0 ? results[0] : misceCate;
    }
}
exports.default = SucasaMapper;
//# sourceMappingURL=sucasa-mapper.js.map