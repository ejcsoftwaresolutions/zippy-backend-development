"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../../app");
class ExternalProductMapper {
    static toPersistenceFromArray(products) {
        return products.map((orderDto) => {
            return ExternalProductMapper.toPersistence(orderDto);
        });
    }
    static toPersistence(product) {
        var _a, _b;
        const dto = product.toPrimitives();
        return app_1.default.omitUnknown({
            id: dto.id,
            name: dto.name,
            price: dto.price.value,
            discountPrice: dto.discountPrice.value,
            photo: product.photo,
            description: '',
            photos: (_a = dto.photos) === null || _a === void 0 ? void 0 : _a.map((e) => {
                return app_1.default.omitUnknown(e);
            }),
            vendorID: dto.sellerId,
            categoryID: dto.categoryId,
            subcategoryID: dto.subcategoryId,
            currency: dto.price.currency.abbr !== 'USD' ? 'Bs' : undefined,
            ...(product.isNew
                ? {
                    stockCount: parseFloat(dto.stockCount + ''),
                    discountRate: 0,
                    salesQuantity: 0,
                    applyDiscount: false,
                    available: dto.available,
                    type: (_b = dto.type) !== null && _b !== void 0 ? _b : 'REGULAR',
                    createdAt: new Date(),
                    isPromotion: false,
                    delivery: true,
                    pickup: false,
                }
                : {}),
        });
    }
}
exports.default = ExternalProductMapper;
//# sourceMappingURL=external-product-mapper.js.map