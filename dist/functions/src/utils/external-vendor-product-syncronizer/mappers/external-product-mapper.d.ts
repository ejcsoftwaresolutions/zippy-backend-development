import SellerProduct from '../models/seller-product';
export default class ExternalProductMapper {
    static toPersistenceFromArray(products: SellerProduct[]): any[];
    static toPersistence(product: SellerProduct): Partial<any>;
}
