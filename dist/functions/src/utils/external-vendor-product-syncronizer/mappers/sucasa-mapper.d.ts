import SellerProduct from '../models/seller-product';
export default class SucasaMapper {
    static toDomainFromArray(dtos: any[], config: {
        mappings: {
            products: any;
            subcategories: any;
            existingProducts: any;
        };
        currencyExchange: number;
        sellerId: string;
        categoryId: string;
        extraCommissionPercentage: number;
    }): SellerProduct[];
    static mapCategories(categories: any[], appCategories: any[]): any;
    private static findCorrespondingCategory;
}
