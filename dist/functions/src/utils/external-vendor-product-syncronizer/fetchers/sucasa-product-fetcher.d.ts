export default class SucasaProductFetcher {
    CAT_LIMIT: number;
    PRODUCT_LIMIT: number;
    private readonly categoryUrl;
    private readonly productUrl;
    constructor();
    private static getOnlyUpdatedFilterString;
    private static getOnlyCreatedFilterString;
    getCategories(): Promise<any[]>;
    getProductsByPage(number: number, filters?: {
        categoryId: string;
        updatedFrom?: string;
        updatedTo?: string;
        createdFrom?: string;
        createdTo?: string;
    }): Promise<any>;
    getTotalProductPages(filters?: {
        categoryId: string;
        updatedFrom?: string;
        updatedTo?: string;
        createdFrom?: string;
        createdTo?: string;
    }): Promise<number | any[]>;
    private getCategoryPage;
    private getTotalCategoriesPages;
}
