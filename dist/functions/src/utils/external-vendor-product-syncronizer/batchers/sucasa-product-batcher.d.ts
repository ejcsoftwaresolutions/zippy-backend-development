import ExternalVendorProductCatalogImporter from '../external-vendor-product-catalog-importer';
import SucasaProductFetcher from '../fetchers/sucasa-product-fetcher';
export default class SucasaProductBatcher {
    private productImporter;
    private sellerInfo;
    fetcher: SucasaProductFetcher;
    constructor(productImporter: ExternalVendorProductCatalogImporter, sellerInfo: {
        sellerId: string;
        sellerCategoryId: string;
        extraCommissionPercentage: number;
    });
    process(categories: any[], config: {
        mode: 'FULL-SYNC' | 'PARTIAL-SYNC';
    }): Promise<void>;
    private processCategory;
    private processPage;
}
