"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sucasa_product_fetcher_1 = require("../fetchers/sucasa-product-fetcher");
const sucasa_mapper_1 = require("../mappers/sucasa-mapper");
class SucasaProductBatcher {
    constructor(productImporter, sellerInfo) {
        this.productImporter = productImporter;
        this.sellerInfo = sellerInfo;
        this.fetcher = new sucasa_product_fetcher_1.default();
    }
    async process(categories, config) {
        const promises = (() => {
            if (config.mode === 'FULL-SYNC') {
                return categories.map((category) => {
                    return this.processCategory(category);
                });
            }
            const start = new Date();
            start.setUTCHours(0, 0, 0, 0);
            const end = new Date();
            end.setUTCHours(23, 59, 59, 999);
            const startUNIX = Math.floor(start.getTime() / 1000);
            const endUNIX = Math.floor(end.getTime() / 1000);
            return categories
                .map((category) => {
                return this.processCategory(category, {
                    createdFrom: startUNIX,
                    createdTo: endUNIX,
                });
            })
                .concat(categories.map((category) => {
                return this.processCategory(category, {
                    updatedFrom: startUNIX,
                    updatedTo: endUNIX,
                });
            }));
        })();
        await Promise.all(promises);
    }
    async processCategory(category, filters) {
        const finalFilters = {
            categoryId: category.id,
            ...(filters !== null && filters !== void 0 ? filters : {}),
        };
        const totalCategoryPages = await this.fetcher.getTotalProductPages(finalFilters);
        const catPromises = [...new Array(totalCategoryPages)].map((x, i) => {
            return new Promise(async (revolvePage) => {
                const page = i + 1;
                await this.processPage(page, finalFilters);
                return revolvePage({});
            });
        });
        await Promise.all(catPromises);
    }
    async processPage(page, filters) {
        const currentChunk = await this.fetcher.getProductsByPage(page, filters);
        await this.productImporter.syncProducts(this.sellerInfo.sellerId, this.sellerInfo.sellerCategoryId, this.sellerInfo.extraCommissionPercentage, currentChunk, sucasa_mapper_1.default);
    }
}
exports.default = SucasaProductBatcher;
//# sourceMappingURL=sucasa-product-batcher.js.map