export default class ExternalVendorProductCatalogImporter {
    private vendorProductRepo;
    constructor();
    syncProducts(sellerId: string, sellerCategoryId: string, extraCommissionPercentage: number, products: any[], mapper: any): Promise<void>;
    syncSubcategories(sellerId: string, sellerCategoryId: string, categories: any[], mapper: any): Promise<void>;
    hasProductsMappings(sellerId: string): Promise<boolean>;
}
