export default abstract class Model<T> {
    protected props: T;
    constructor(props: T);
    toJson(): any;
    toObject(options?: any): any;
    toString(): string;
}
