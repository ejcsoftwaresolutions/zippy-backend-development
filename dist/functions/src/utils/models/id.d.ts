import ValueObject from './value-object';
interface IdProps {
    value: string;
}
export default class Id extends ValueObject<IdProps> {
    constructor(id?: string);
    get value(): string;
    toPrimitives(): string;
    toString(): string;
    private gen;
}
export {};
