"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = require("firebase-admin");
const value_object_1 = require("./value-object");
class Id extends value_object_1.default {
    constructor(id) {
        super({
            value: id,
        });
        if (!this.props.value) {
            this.props.value = this.gen();
        }
    }
    get value() {
        return this.props.value;
    }
    toPrimitives() {
        return this.value;
    }
    toString() {
        return this.value;
    }
    gen() {
        const id = firebase_admin_1.default.firestore().collection('test').doc().id;
        return id;
    }
}
exports.default = Id;
//# sourceMappingURL=id.js.map