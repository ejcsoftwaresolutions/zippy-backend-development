export interface PushNotificationParams {
    data?: any;
    notification: {
        title: string;
        body: string;
        image?: string;
    };
    channel?: string;
}
export type PushNotificationProps = {
    id: string;
    to: string | string[];
    title?: string;
    content: string;
    appId: string;
    channelId?: string;
    metadata?: any;
    imageUrl?: string;
    linkUrl?: string;
};
export declare class PushNotification {
    private props;
    constructor(props: PushNotificationProps);
    get id(): string;
    get to(): string | string[];
    get appName(): 'CUSTOMER' | 'RIDER' | 'VENDOR';
    get isSegment(): boolean;
    get isExternalId(): boolean;
    toPrimitives(): {
        id: string;
        to: string | string[];
        title: string;
        content: string;
        appId: string;
        channelId: string;
        metadata: any;
        imageUrl: string;
        linkUrl: string;
    };
}
