import Currency, { CurrencyPrimitiveProps } from './currency';
interface CurrencyPriceProps {
    value: number;
    formattedValue?: string;
    currency: Currency;
}
export interface CurrencyPricePrimitiveProps {
    value: number;
    formattedValue?: string;
    currency: CurrencyPrimitiveProps;
}
export default class CurrencyPrice {
    private props;
    constructor(props: CurrencyPriceProps);
    get value(): number;
    get formattedValue(): string;
    get currency(): Currency;
    static sum(priceA: CurrencyPrice, priceB: CurrencyPrice): CurrencyPrice;
    static subtract(priceA: CurrencyPrice, priceB: CurrencyPrice): CurrencyPrice;
    static times(currencyPrice: CurrencyPrice, times: number, totalCurrency?: Currency): CurrencyPrice;
    static div(currencyPrice: CurrencyPrice, times: number, totalCurrency?: Currency): CurrencyPrice;
    static getAcc(prices: CurrencyPrice[]): CurrencyPrice;
    static fromPrimitives(props: CurrencyPricePrimitiveProps): CurrencyPrice;
    static isEqual(priceA: CurrencyPrice, priceB: CurrencyPrice): boolean;
    static equalPrices(pricesA: CurrencyPrice[], pricesB: CurrencyPrice[]): boolean;
    static fromZero(currency: Currency): CurrencyPrice;
    static fromPrimitiveArray(items: CurrencyPricePrimitiveProps[]): CurrencyPrice[];
    toPrimitives(): CurrencyPricePrimitiveProps;
}
export {};
