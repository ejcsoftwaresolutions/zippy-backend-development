"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currency_1 = require("./currency");
const currency_utils_1 = require("../currency-utils");
const UsdCurrency = new currency_1.default({
    abbr: currency_utils_1.usdAbbr,
    name: 'Dolares',
    format: currency_utils_1.usdFormat,
});
exports.default = UsdCurrency;
//# sourceMappingURL=usd-currency.js.map