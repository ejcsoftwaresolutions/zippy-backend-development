"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currency_utils_1 = require("../currency-utils");
const currency_1 = require("./currency");
const BsCurrency = new currency_1.default({
    abbr: currency_utils_1.bsAbbr,
    name: 'Bolivares Soberanos',
    format: currency_utils_1.bsFormat,
});
exports.default = BsCurrency;
//# sourceMappingURL=bs-currency.js.map