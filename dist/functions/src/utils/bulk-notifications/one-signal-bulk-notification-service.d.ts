import BulkNotificator, { BulkNotificationResult } from './bulk-notificator';
export default class OneSignalBulkNotificationService implements BulkNotificator {
    private pushNotificationService;
    private config;
    private registeredVariables;
    constructor(pushNotificationService: {
        send: any;
    }, config: {
        chunkSize?: number;
    });
    sendCampaignNotification(targetGroup: string, message: {
        id: string;
        title: string;
        description: string;
        imageUrl?: string;
        linkUrl?: string;
    }): Promise<{
        deliveryIds: any[];
        recipients: any;
        targetSize: number;
    }>;
    sendBulkIndividualNotification(targetGroup: string, message: {
        id: string;
        title: string;
        description: string;
        imageUrl?: string;
        linkUrl?: string;
    }, targetRecipients: any[]): Promise<{
        targetSize: number;
        recipients: number;
        deliveryIds: string[];
    }>;
    getBulkNotificationProcessResult(results: BulkNotificationResult): any;
    private sendInChunks;
    private sendMessage;
    private buildNotification;
    private cleanVariables;
    private getChunks;
}
