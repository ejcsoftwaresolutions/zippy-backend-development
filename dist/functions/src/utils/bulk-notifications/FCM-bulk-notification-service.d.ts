import BulkNotificator, { BulkNotificationResult } from './bulk-notificator';
export default class FCMBulkNotificationService implements BulkNotificator {
    private pushNotificationService;
    private config;
    private registeredVariables;
    constructor(pushNotificationService: {
        send: any;
    }, config: {
        chunkSize?: number;
    });
    sendCampaignNotification(targetGroup: string, message: {
        id: string;
        title: string;
        description: string;
        imageUrl?: string;
    }): Promise<{
        targetSize: number;
        recipients: number;
        deliveryIds: string[];
    }>;
    sendBulkIndividualNotification(targetGroup: string, message: {
        id: string;
        title: string;
        description: string;
        imageUrl?: string;
    }, targetRecipients: any[]): Promise<{
        targetSize: number;
        recipients: number;
        deliveryIds: string[];
    }>;
    getBulkNotificationProcessResult(results: BulkNotificationResult): any;
    private sendInChunks;
    private sendMessage;
    private replaceVariables;
    private findTargetGroupUsers;
    private findUsersTokens;
    private getChunks;
}
