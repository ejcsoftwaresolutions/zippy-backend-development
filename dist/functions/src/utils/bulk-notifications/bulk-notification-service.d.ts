import OneSignalBulkNotificationService from './one-signal-bulk-notification-service';
import PushNotificator from '../notification-service/push-notificator';
import FCMBulkNotificationService from './FCM-bulk-notification-service';
export declare function createBulkNotificationService(notificationService: PushNotificator): OneSignalBulkNotificationService | FCMBulkNotificationService;
export declare const BulkNotificationService: OneSignalBulkNotificationService | FCMBulkNotificationService;
