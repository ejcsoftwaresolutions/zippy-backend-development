"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = require("firebase-admin");
const push_notification_1 = require("../models/push-notification");
const app_1 = require("../app");
class FCMBulkNotificationService {
    constructor(pushNotificationService, config) {
        this.pushNotificationService = pushNotificationService;
        this.config = config;
        this.registeredVariables = [
            '[[FIRST_NAME]]',
            '[[FULL_NAME]]',
            '[[EMAIL]]',
        ];
    }
    async sendCampaignNotification(targetGroup, message) {
        var _a;
        const users = await this.findTargetGroupUsers(targetGroup);
        const chunkedUsers = this.getChunks(users, (_a = this.config.chunkSize) !== null && _a !== void 0 ? _a : 50);
        const result = await this.sendInChunks(chunkedUsers, {
            id: message.id,
            role: targetGroup,
            title: message.title,
            content: message.description,
            imageUrl: message.imageUrl,
        });
        return result;
    }
    async sendBulkIndividualNotification(targetGroup, message, targetRecipients) {
        var _a;
        const chunkedUsers = this.getChunks(targetRecipients, (_a = this.config.chunkSize) !== null && _a !== void 0 ? _a : 50);
        const result = await this.sendInChunks(chunkedUsers, {
            id: message.id,
            role: targetGroup,
            title: message.title,
            content: message.description,
            imageUrl: message.imageUrl,
        });
        return result;
    }
    getBulkNotificationProcessResult(results) {
        return app_1.default.omitUnknown({
            processed: true,
            processedAt: new Date(),
            error: undefined,
            stats: results
                ? (() => {
                    return {
                        deliveryIds: results.deliveryIds,
                        total: results.targetSize,
                        successful: results.recipients,
                        failed: results.targetSize - results.recipients,
                    };
                })()
                : undefined,
        });
    }
    async sendInChunks(usersChunks, message) {
        const delay = (ms) => {
            return new Promise((resolve) => setTimeout(resolve, ms));
        };
        const chunkPromises = usersChunks.map((chunk) => {
            const promises = chunk.map((user) => {
                return this.sendMessage(user, {
                    ...message,
                    title: this.replaceVariables(user, message.title),
                    content: this.replaceVariables(user, message.content),
                });
            });
            return promises;
        });
        const res = chunkPromises.map(async (promises) => {
            return new Promise(async (resolve) => {
                const chunkResults = await Promise.all(promises);
                await delay(500);
                resolve(chunkResults);
            });
        });
        const results = await Promise.all(res);
        const finalResults = results.flat();
        return {
            deliveryIds: finalResults
                .map((r) => r.deliveryId)
                .filter((id) => !!id),
            recipients: finalResults.reduce((acc, current) => acc + current.recipients, 0),
            targetSize: usersChunks.reduce((acc, current) => acc + current.length, 0),
        };
    }
    sendMessage(user, message) {
        return new Promise(async (resolve) => {
            const appId = (() => {
                if (message.role === 'RIDER')
                    return process.env.RIDER_APP_EXPERIENCE_ID;
                if (message.role === 'VENDOR')
                    return process.env.STORE_APP_EXPERIENCE_ID;
                return process.env.CUSTOMER_APP_EXPERIENCE_ID;
            })();
            const channel = appId.split('/')[1];
            try {
                const res = await this.pushNotificationService.send(new push_notification_1.PushNotification({
                    id: message.id,
                    to: user.token,
                    metadata: {
                        appMessageId: message.id,
                    },
                    title: message.title,
                    content: message.content,
                    imageUrl: message.imageUrl,
                    appId: appId,
                    channelId: channel,
                }));
                return resolve({
                    success: !Boolean(res.error),
                    recipients: res.recipients,
                    error: res.error,
                    deliveryId: res.deliveryId,
                });
            }
            catch (e) {
                return resolve({
                    success: false,
                    recipients: 0,
                    error: e.message,
                });
            }
        });
    }
    replaceVariables(user, text) {
        return text
            .replace('[[FIRST_NAME]]', user.firstName)
            .replace('[[FULL_NAME]]', `${user.firstName} ${user.lastName}`)
            .replace('[[EMAIL]]', user.email);
    }
    async findTargetGroupUsers(role) {
        const query = firebase_admin_1.default
            .firestore()
            .collection('users')
            .where('roles', 'array-contains', role)
            .where('status', '==', 'ACTIVE');
        const users = (await query.get()).docs.map((d) => {
            const data = d.data();
            return {
                id: data.id,
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email,
            };
        });
        const tokens = await this.findUsersTokens(users.map((u) => u.id), role);
        return users
            .map((user, index) => {
            return {
                ...user,
                token: tokens[index],
            };
        })
            .filter((u) => !!u.token);
    }
    async findUsersTokens(ids, role) {
        const queryChunks = this.getChunks(ids, 10);
        const chunksResults = queryChunks.map(async (chunk) => {
            let ref = firebase_admin_1.default.firestore().collection('user_push_tokens');
            ref = ref.where('userId', 'in', chunk);
            const result = await ref.get();
            const chunkDtos = (await result.docs).map((d) => d.data());
            return chunkDtos;
        });
        const results = await Promise.all(chunksResults);
        return results === null || results === void 0 ? void 0 : results.flat().map((tokenData) => {
            if (role === 'CUSTOMER') {
                return tokenData === null || tokenData === void 0 ? void 0 : tokenData.customer;
            }
            if (role === 'VENDOR') {
                return tokenData === null || tokenData === void 0 ? void 0 : tokenData.vendor;
            }
            return tokenData === null || tokenData === void 0 ? void 0 : tokenData.rider;
        });
    }
    getChunks(inputArray, perChunk) {
        const result = inputArray.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perChunk);
            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = [];
            }
            resultArray[chunkIndex].push(item);
            return resultArray;
        }, []);
        return result;
    }
}
exports.default = FCMBulkNotificationService;
//# sourceMappingURL=FCM-bulk-notification-service.js.map