"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const push_notification_1 = require("../models/push-notification");
const app_1 = require("../app");
class OneSignalBulkNotificationService {
    constructor(pushNotificationService, config) {
        this.pushNotificationService = pushNotificationService;
        this.config = config;
        this.registeredVariables = [
            '[[FIRST_NAME]]',
            '[[FULL_NAME]]',
            '[[EMAIL]]',
        ];
    }
    async sendCampaignNotification(targetGroup, message) {
        const finalMessage = {
            id: message.id,
            role: targetGroup,
            title: this.cleanVariables(message.title),
            content: this.cleanVariables(message.description),
            imageUrl: message.imageUrl,
            isSegment: true,
            linkUrl: message.linkUrl,
        };
        const notification = this.buildNotification(['Subscribed Users'], finalMessage);
        const res = await this.pushNotificationService.send(notification);
        return {
            deliveryIds: [res.deliveryId],
            recipients: res.recipients,
            targetSize: 0,
        };
    }
    async sendBulkIndividualNotification(targetGroup, message, targetRecipients) {
        var _a;
        const chunkedUsers = this.getChunks(targetRecipients, (_a = this.config.chunkSize) !== null && _a !== void 0 ? _a : 50);
        const result = await this.sendInChunks(chunkedUsers, {
            id: message.id,
            role: targetGroup,
            title: message.title,
            content: message.description,
            imageUrl: message.imageUrl,
            linkUrl: message.linkUrl,
        });
        return result;
    }
    getBulkNotificationProcessResult(results) {
        return app_1.default.omitUnknown({
            processed: true,
            processedAt: new Date(),
            error: undefined,
            stats: results
                ? (() => {
                    return {
                        deliveryIds: results.deliveryIds,
                        total: results.targetSize,
                        successful: results.recipients,
                        failed: results.targetSize - results.recipients,
                    };
                })()
                : undefined,
        });
    }
    async sendInChunks(usersChunks, message) {
        const chunkPromises = usersChunks.map((chunk) => {
            return this.sendMessage(chunk, {
                ...message,
                title: this.cleanVariables(message.title),
                content: this.cleanVariables(message.content),
            });
        });
        const results = await Promise.all(chunkPromises);
        return {
            deliveryIds: results.map((r) => r.deliveryId).filter((id) => !!id),
            recipients: results.reduce((acc, current) => acc + current.recipients, 0),
            targetSize: usersChunks.reduce((acc, current) => acc + current.length, 0),
        };
    }
    sendMessage(users, message) {
        return new Promise(async (resolve) => {
            try {
                const notification = this.buildNotification(users.map((u) => u.token), message);
                const res = await this.pushNotificationService.send(notification);
                return resolve({
                    success: !Boolean(res.error),
                    recipients: res.recipients,
                    error: res.error,
                    deliveryId: res.deliveryId,
                });
            }
            catch (e) {
                return resolve({
                    success: false,
                    recipients: 0,
                    error: e.message,
                });
            }
        });
    }
    buildNotification(to, message) {
        const appId = (() => {
            if (message.role === 'RIDER')
                return process.env.RIDER_APP_EXPERIENCE_ID;
            if (message.role === 'VENDOR')
                return process.env.STORE_APP_EXPERIENCE_ID;
            return process.env.CUSTOMER_APP_EXPERIENCE_ID;
        })();
        const channel = appId.split('/')[1];
        return new push_notification_1.PushNotification({
            id: message.id,
            to: to,
            metadata: {
                appMessageId: message.id,
                isSegment: message.isSegment !== undefined ? message.isSegment : false,
            },
            title: message.title,
            content: message.content,
            imageUrl: message.imageUrl,
            appId: appId,
            channelId: message.role === 'CUSTOMER' ? 'ads' : channel,
            linkUrl: message.linkUrl,
        });
    }
    cleanVariables(text) {
        return text
            .replace('[[FIRST_NAME]]', '{{ first_name | "estimad@" }}')
            .replace('[[FULL_NAME]]', `{{ full_name  | "estimad@"}}`)
            .replace('[[EMAIL]]', '{{ email | ""}}');
    }
    getChunks(inputArray, perChunk) {
        const result = inputArray.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perChunk);
            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = [];
            }
            resultArray[chunkIndex].push(item);
            return resultArray;
        }, []);
        return result;
    }
}
exports.default = OneSignalBulkNotificationService;
//# sourceMappingURL=one-signal-bulk-notification-service.js.map