import { firestore } from 'firebase-admin';
type QueryFilter = {
    field: string;
    operator: any;
    value: string[] | number | string | boolean;
};
type QueryDocsParams = {
    filters: QueryFilter[];
    limit?: number;
    orderBy?: {
        field: string;
        direction: any;
    };
};
export declare const FirestoreValue: typeof firestore.FieldValue;
export default abstract class FirestoreApiRepository {
    private db;
    constructor();
    get firestore(): firestore.Firestore;
    getDoc(collectionName: string, docId: string): Promise<firestore.DocumentData>;
    updateDoc(collectionName: string, docId: string, data: any): Promise<void>;
    getDocsRef(collectionName: string): firestore.CollectionReference<firestore.DocumentData>;
    deleteDoc(collectionName: string, docId: string): Promise<void>;
    setDoc(collectionName: string, docId: string, data: any): Promise<void>;
    saveDoc(collectionName: string, docId: string, data: any): Promise<void>;
    deleteDocField(collectionName: string, docId: string, field: string): Promise<void>;
    getDocs(collectionName: string, params: QueryDocsParams): Promise<any>;
    saveInBatch(collectionName: string, docs: any[]): Promise<void>;
    getChunks(inputArray: any, perChunk: any): any;
    private getDocsData;
    private getDocsWithFiltersRef;
}
export {};
