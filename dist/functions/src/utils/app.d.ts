import { PushNotificationParams } from './models/push-notification';
declare const CloudFunctionsAppUtils: {
    findAccount(id: string): Promise<FirebaseFirestore.DocumentData>;
    findCustomer(id: string): Promise<FirebaseFirestore.DocumentData>;
    findVendor(id: string): Promise<FirebaseFirestore.DocumentData>;
    findRider(id: string): Promise<FirebaseFirestore.DocumentData>;
    findPushTokens(userId: string): Promise<FirebaseFirestore.DocumentData>;
    getOrder(id: string): Promise<any>;
    findUserNotificationInfo(userId: string, role: 'customer' | 'vendor' | 'rider'): Promise<{
        pushToken?: string;
        email?: string;
        configurations?: {
            referralPromotions: boolean;
            receiveEmails: boolean;
            receivePushNotifications: boolean;
            receiveSMS: boolean;
        };
    }>;
    sendEmail(to: string, message: {
        subject: any;
        text: any;
        html: any;
    }): Promise<FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>[]>;
    sendPushNotification(pushNotification: PushNotificationParams, role: 'RIDER' | 'VENDOR' | 'CUSTOMER', userId: string): Promise<any>;
    createId(): Promise<string>;
    syncVendorGeolocation(vendorId: string, location: number[]): Promise<any>;
    findAdminAccount(id?: string): Promise<FirebaseFirestore.DocumentData>;
    getAppDeliveryServiceConfigurations(): Promise<FirebaseFirestore.DocumentData>;
    omitUnknown(object: any): Partial<any>;
};
export default CloudFunctionsAppUtils;
