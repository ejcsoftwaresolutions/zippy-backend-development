import { Moment } from 'moment';
declare const DateTimeUtils: {
    format: (date: Date, format: string, utc?: boolean) => string;
    differenceInMinutes: (a: Date, b: Date) => number;
    differenceInMinutesMoment: (a: Moment, b: Moment) => number;
    fromTime(time: string, format?: string): Date;
};
export default DateTimeUtils;
