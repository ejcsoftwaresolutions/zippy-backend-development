"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const array_utils_1 = require("../array-utils");
const object_utils_1 = require("../object-utils");
const firebase_admin_1 = require("firebase-admin");
function recalculateProduct(product) {
    var _a, _b, _c;
    if (!product)
        return undefined;
    const hasOptions = (_b = ((_a = product.options) === null || _a === void 0 ? void 0 : _a.length) > 0) !== null && _b !== void 0 ? _b : false;
    const optionsWithDiscounts = hasOptions
        ? ((_c = product.options) !== null && _c !== void 0 ? _c : []).filter((o) => !!o.applyDiscount)
        : [];
    const hasDiscount = () => {
        return hasOptions
            ? optionsWithDiscounts.length > 0
            : !!product.discountRate && !!product.applyDiscount;
    };
    const lowestOptionPrice = () => {
        var _a, _b, _c;
        if (!hasOptions) {
            return firebase_admin_1.default.firestore.FieldValue.delete();
        }
        return ((_c = (_b = array_utils_1.ArrayUtils.orderBy((_a = product.options) !== null && _a !== void 0 ? _a : [], 'price', 'asc')[0]) === null || _b === void 0 ? void 0 : _b.price) !== null && _c !== void 0 ? _c : 0);
    };
    const lowestOptionDiscountPrice = () => {
        var _a, _b;
        if (!hasOptions) {
            return firebase_admin_1.default.firestore.FieldValue.delete();
        }
        if (!hasDiscount())
            return firebase_admin_1.default.firestore.FieldValue.delete();
        return ((_b = (_a = array_utils_1.ArrayUtils.orderBy(optionsWithDiscounts, 'discountPrice', 'asc')[0]) === null || _a === void 0 ? void 0 : _a.discountPrice) !== null && _b !== void 0 ? _b : 0);
    };
    const highestOptionDiscountRate = () => {
        var _a, _b;
        if (!hasOptions) {
            return firebase_admin_1.default.firestore.FieldValue.delete();
        }
        if (!hasDiscount())
            return firebase_admin_1.default.firestore.FieldValue.delete();
        return ((_b = (_a = array_utils_1.ArrayUtils.orderBy(optionsWithDiscounts, 'discountRate', 'desc')[0]) === null || _a === void 0 ? void 0 : _a.discountRate) !== null && _b !== void 0 ? _b : 0);
    };
    const thumbnailUrl = () => {
        var _a, _b;
        const defaultPhoto = (_a = product.photo) !== null && _a !== void 0 ? _a : firebase_admin_1.default.firestore.FieldValue.delete();
        if (!product.photos)
            return defaultPhoto;
        if (product.photos.length == 0)
            return defaultPhoto;
        const firstPhoto = product.photos[0];
        if (!firstPhoto.versions)
            return firstPhoto.url;
        const thumbnailVersion = firstPhoto.versions.find((v) => v.id == 'THUMBNAIL');
        return (_b = thumbnailVersion === null || thumbnailVersion === void 0 ? void 0 : thumbnailVersion.url) !== null && _b !== void 0 ? _b : firstPhoto.url;
    };
    return object_utils_1.default.omitUnknown({
        hasDiscount: hasDiscount(),
        hasOptions: hasOptions,
        highestOptionDiscountRate: highestOptionDiscountRate(),
        lowestOptionPrice: lowestOptionPrice(),
        lowestOptionDiscountPrice: lowestOptionDiscountPrice(),
        thumbnailUrl: thumbnailUrl(),
    });
}
exports.default = recalculateProduct;
//# sourceMappingURL=recalculate-product.js.map