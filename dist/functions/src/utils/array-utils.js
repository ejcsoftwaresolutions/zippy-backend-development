"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrayUtils = void 0;
const lodash_1 = require("lodash");
var ArrayUtils;
(function (ArrayUtils) {
    ArrayUtils.isArray = lodash_1.isArray;
    ArrayUtils.filter = lodash_1.filter;
    ArrayUtils.differenceWith = lodash_1.differenceWith;
    ArrayUtils.orderBy = lodash_1.orderBy;
    ArrayUtils.uniq = lodash_1.uniq;
    ArrayUtils.uniqBy = lodash_1.uniqBy;
    ArrayUtils.map = lodash_1.map;
    ArrayUtils.flatten = lodash_1.flattenDeep;
    ArrayUtils.findIndex = lodash_1.findIndex;
    ArrayUtils.filterLike = (arr1, fieldName, like) => {
        return arr1.filter((dto) => {
            var _a;
            const name = (_a = dto[fieldName]) !== null && _a !== void 0 ? _a : '';
            const test = like;
            const reg1 = new RegExp('^' + test + '.*$', 'i');
            const reg2 = new RegExp('^.*' + test + '$', 'i');
            const regex3 = new RegExp([test].join('|'), 'i');
            const match = name.match(reg1);
            const match2 = name.match(reg2);
            const match3 = name.includes(test);
            const match4 = regex3.test(name);
            return !!match || !!match2 || !!match3 || !!match4;
        });
    };
})(ArrayUtils = exports.ArrayUtils || (exports.ArrayUtils = {}));
//# sourceMappingURL=array-utils.js.map