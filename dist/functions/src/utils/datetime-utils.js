"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment");
const DateTimeUtils = {
    format: (date, format, utc = true) => {
        if (utc)
            return moment(date).utc().format(format);
        return moment(date).format(format);
    },
    differenceInMinutes: (a, b) => {
        return moment(a).diff(moment(b), 'minutes');
    },
    differenceInMinutesMoment: (a, b) => {
        return a.diff(b, 'minutes');
    },
    fromTime(time, format = 'HH:mm') {
        return moment(time, format).toDate();
    },
};
exports.default = DateTimeUtils;
//# sourceMappingURL=datetime-utils.js.map