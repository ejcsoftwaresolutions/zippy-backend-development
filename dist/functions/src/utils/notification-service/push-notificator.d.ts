import { PushNotification } from '../models/push-notification';
export type PushNotificatorDeliveryResult = {
    ok: boolean;
    recipients: number;
    deliveryId?: string;
    error?: any;
    metadata?: any;
};
export type PushNotificatorStats = {
    successful: number;
    failed: number;
    audience: number;
    clicks: number;
};
export default interface PushNotificator {
    getName(): string;
    send(notification: PushNotification): Promise<PushNotificatorDeliveryResult>;
    getNotificationStats(id: string, targetApp: string): Promise<PushNotificatorStats>;
}
