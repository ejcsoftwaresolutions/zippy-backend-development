"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotificationService = exports.createNotificationService = void 0;
const one_signal_notification_service_1 = require("./one-signal-notification-service");
const FCM_notification_service_1 = require("./FCM-notification-service");
function createNotificationService(defaultProvider = 'ONE_SIGNAL') {
    const provider = 'ONE_SIGNAL';
    if (provider !== 'ONE_SIGNAL') {
        return new FCM_notification_service_1.default(process.env.CLOUD_SERVER_KEY, process.env.FB_PROJECT_ID, process.env.GOOGLE_APPLICATION_CREDENTIALS);
    }
    return new one_signal_notification_service_1.default({
        authKey: process.env.ONE_SIGNAL_AUTH_KEY,
        apps: {
            CUSTOMER: {
                id: process.env.ONE_SIGNAL_CUSTOMER_ID,
                apiKey: process.env.ONE_SIGNAL_CUSTOMER_KEY,
                channels: {
                    android: process.env.CUSTOMER_ANDROID_CHANNELS
                        ? process.env.CUSTOMER_ANDROID_CHANNELS
                            .split(',')
                            .map((ch) => {
                            const channel = ch.split('*');
                            return {
                                id: channel[1].trim(),
                                name: channel[0].trim(),
                            };
                        })
                        : [],
                    ios: [
                        {
                            id: 'notification.wav',
                            name: 'zippi-market',
                        },
                        {
                            id: 'ads.wav',
                            name: 'ads',
                        },
                    ],
                },
            },
            RIDER: {
                id: process.env.ONE_SIGNAL_RIDER_ID,
                apiKey: process.env.ONE_SIGNAL_RIDER_KEY,
                channels: {
                    android: process.env.RIDER_ANDROID_CHANNELS
                        ? process.env.RIDER_ANDROID_CHANNELS
                            .split(',')
                            .map((ch) => {
                            const channel = ch.split('*');
                            return {
                                id: channel[1].trim(),
                                name: channel[0].trim(),
                            };
                        })
                        : [],
                    ios: [
                        {
                            id: 'notification.wav',
                            name: 'zippi-rider-app',
                        },
                        {
                            id: 'new_order.wav',
                            name: 'new-order',
                        },
                        {
                            id: 'order_update.wav',
                            name: 'order-update',
                        },
                    ],
                },
            },
            VENDOR: {
                id: process.env.ONE_SIGNAL_VENDOR_ID,
                apiKey: process.env.ONE_SIGNAL_VENDOR_KEY,
                channels: {
                    android: process.env.VENDOR_ANDROID_CHANNELS
                        ? process.env.VENDOR_ANDROID_CHANNELS
                            .split(',')
                            .map((ch) => {
                            const channel = ch.split('*');
                            return {
                                id: channel[1].trim(),
                                name: channel[0].trim(),
                            };
                        })
                        : [],
                    ios: [
                        {
                            id: 'notification.wav',
                            name: 'zippi-vendor-app',
                        },
                        {
                            id: 'new_order.wav',
                            name: 'new-order',
                        },
                        {
                            id: 'order_update.wav',
                            name: 'order-update',
                        },
                    ],
                },
            },
        },
    });
}
exports.createNotificationService = createNotificationService;
exports.NotificationService = createNotificationService();
//# sourceMappingURL=notification-service.js.map