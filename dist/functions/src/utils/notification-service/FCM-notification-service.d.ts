import { PushNotification } from '../models/push-notification';
import PushNotificator, { PushNotificatorDeliveryResult, PushNotificatorStats } from './push-notificator';
export default class FCMNotificationService implements PushNotificator {
    private serverKey;
    private projectId;
    private serviceAccountLocation;
    constructor(serverKey: string, projectId: string, serviceAccountLocation: string);
    getName(): string;
    send(notification: PushNotification): Promise<PushNotificatorDeliveryResult>;
    getNotificationStats(id: string, targetApp: string): Promise<PushNotificatorStats>;
    private getAccessToken;
}
