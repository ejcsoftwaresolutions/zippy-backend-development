"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const id_1 = require("../models/id");
const node_onesignal_1 = require("@onesignal/node-onesignal");
const functions = require('firebase-functions');
const OneSignal = require('@onesignal/node-onesignal');
class OneSignalNotificationService {
    constructor(params) {
        this.apps = {};
        this.apps = params.apps;
        this.authKey = params.authKey;
    }
    getName() {
        return 'ONE_SIGNAL';
    }
    async send(notification) {
        await this.configureClient(notification.appName);
        const osNotification = await this.buildBaseNotification(notification);
        const finalRecipients = typeof notification.to === 'string' ? [notification.to] : notification.to;
        if (notification.isSegment) {
            osNotification.included_segments = finalRecipients;
        }
        else if (notification.isExternalId) {
            osNotification.include_external_user_ids = finalRecipients;
        }
        else {
            osNotification.include_player_ids = finalRecipients;
        }
        if (!this.client)
            throw new Error('Client not initialized');
        try {
            const res = await this.client.createNotification(osNotification);
            return {
                ok: true,
                recipients: res.recipients,
                metadata: res,
                deliveryId: res.id,
            };
        }
        catch (e) {
            functions.logger.log('Error al enviar push notification', e);
            return {
                ok: false,
                recipients: 0,
                error: e.message,
            };
        }
    }
    async getNotificationStats(id, targetApp) {
        var _a, _b, _c, _d;
        await this.configureClient(targetApp);
        const appId = await this.getAppId(targetApp);
        if (!this.client)
            throw new Error('Client not initialized');
        const notification = await this.client.getNotification(appId, id);
        if (!notification) {
            return {
                audience: 0,
                failed: 0,
                successful: 0,
                clicks: 0,
            };
        }
        const failed = ((_a = notification.failed) !== null && _a !== void 0 ? _a : 0) + ((_b = notification.errored) !== null && _b !== void 0 ? _b : 0);
        const successful = (_c = notification.successful) !== null && _c !== void 0 ? _c : 0;
        return {
            audience: successful + failed,
            failed: failed,
            successful: successful,
            clicks: (_d = notification.converted) !== null && _d !== void 0 ? _d : 0,
        };
    }
    async getAppId(appName) {
        var _a;
        return (_a = this.apps[appName]) === null || _a === void 0 ? void 0 : _a.id;
    }
    async getAppKey(appName) {
        var _a;
        return (_a = this.apps[appName]) === null || _a === void 0 ? void 0 : _a.apiKey;
    }
    async configureClient(appName) {
        const appKey = await this.getAppKey(appName);
        if (!appKey) {
            throw new Error('One signal App Key not found');
        }
        const configuration = OneSignal.createConfiguration({
            authMethods: {
                user_key: {
                    tokenProvider: {
                        getToken: () => {
                            return this.authKey;
                        },
                    },
                },
                app_key: {
                    tokenProvider: {
                        getToken: () => {
                            return appKey;
                        },
                    },
                },
            },
        });
        this.client = new OneSignal.DefaultApi(configuration);
    }
    async buildBaseNotification(notification) {
        var _a, _b, _c;
        const notificationData = notification.toPrimitives();
        const appId = await this.getAppId(notification.appName);
        if (!appId) {
            throw new Error('One signal App Id not found');
        }
        const appChannels = this.apps[notification.appName].channels;
        const osNotification = new node_onesignal_1.Notification();
        osNotification.app_id = appId;
        osNotification.headings = {
            en: notificationData.title,
            es: notificationData.title,
        };
        osNotification.contents = {
            en: notificationData.content,
            es: notificationData.content,
        };
        osNotification.priority = 10;
        osNotification.big_picture = notificationData.imageUrl;
        osNotification.huawei_big_picture = notificationData.imageUrl;
        osNotification.ios_attachments = notificationData.imageUrl
            ? { [new id_1.default().value]: notificationData.imageUrl }
            : undefined;
        osNotification.small_icon = 'notification';
        osNotification.huawei_small_icon = 'notification';
        osNotification.data = notificationData.metadata;
        osNotification.android_channel_id = (_a = appChannels.android.find((ch) => ch.name === notificationData.channelId)) === null || _a === void 0 ? void 0 : _a.id;
        osNotification.huawei_channel_id = (_b = appChannels.android.find((ch) => ch.name === notificationData.channelId)) === null || _b === void 0 ? void 0 : _b.id;
        osNotification.ios_sound = (_c = appChannels.ios.find((ch) => ch.name === notificationData.channelId)) === null || _c === void 0 ? void 0 : _c.id;
        osNotification.content_available = true;
        osNotification.url = notificationData.linkUrl;
        return osNotification;
    }
}
exports.default = OneSignalNotificationService;
//# sourceMappingURL=one-signal-notification-service.js.map