import PushNotificator, { PushNotificatorDeliveryResult, PushNotificatorStats } from './push-notificator';
import { PushNotification } from '../models/push-notification';
type PushNotificationChannels = {
    android: {
        id: string;
        name: string;
    }[];
    ios: {
        id?: string;
        name: string;
    }[];
};
type PushNotificationApp = {
    id: string;
    apiKey: string;
    channels: PushNotificationChannels;
};
export default class OneSignalNotificationService implements PushNotificator {
    private authKey;
    private client;
    private apps;
    constructor(params: {
        authKey: string;
        apps: {
            [name: string]: PushNotificationApp;
        };
    });
    getName(): string;
    send(notification: PushNotification): Promise<PushNotificatorDeliveryResult>;
    getNotificationStats(id: string, targetApp: string): Promise<PushNotificatorStats>;
    private getAppId;
    private getAppKey;
    private configureClient;
    private buildBaseNotification;
}
export {};
