import OneSignalNotificationService from './one-signal-notification-service';
import FCMNotificationService from './FCM-notification-service';
export declare function createNotificationService(defaultProvider?: string): FCMNotificationService | OneSignalNotificationService;
export declare const NotificationService: FCMNotificationService | OneSignalNotificationService;
