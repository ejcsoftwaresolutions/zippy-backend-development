export declare namespace ArrayUtils {
    const isArray: {
        (value?: any): value is any[];
        <T>(value?: any): value is any[];
    };
    const filter: {
        (collection: string, predicate?: import("lodash").StringIterator<boolean>): string[];
        <T, S extends T>(collection: import("lodash").List<T>, predicate: import("lodash").ListIteratorTypeGuard<T, S>): S[];
        <T_1>(collection: import("lodash").List<T_1>, predicate?: import("lodash").ListIterateeCustom<T_1, boolean>): T_1[];
        <T_2 extends object, S_1 extends T_2[keyof T_2]>(collection: T_2, predicate: import("lodash").ObjectIteratorTypeGuard<T_2, S_1>): S_1[];
        <T_3 extends object>(collection: T_3, predicate?: import("lodash").ObjectIterateeCustom<T_3, boolean>): T_3[keyof T_3][];
    };
    const differenceWith: {
        <T1, T2>(array: import("lodash").List<T1>, values: import("lodash").List<T2>, comparator: import("lodash").Comparator2<T1, T2>): T1[];
        <T1_1, T2_1, T3>(array: import("lodash").List<T1_1>, values1: import("lodash").List<T2_1>, values2: import("lodash").List<T3>, comparator: import("lodash").Comparator2<T1_1, T2_1 | T3>): T1_1[];
        <T1_2, T2_2, T3_1, T4>(array: import("lodash").List<T1_2>, values1: import("lodash").List<T2_2>, values2: import("lodash").List<T3_1>, ...values: (import("lodash").List<T4> | import("lodash").Comparator2<T1_2, T2_2 | T3_1 | T4>)[]): T1_2[];
        <T>(array: import("lodash").List<T>, ...values: import("lodash").List<T>[]): T[];
    };
    const orderBy: {
        <T>(collection: import("lodash").List<T>, iteratees?: import("lodash").Many<import("lodash").ListIterator<T, unknown>>, orders?: import("lodash").Many<boolean | "asc" | "desc">): T[];
        <T_1>(collection: import("lodash").List<T_1>, iteratees?: import("lodash").Many<import("lodash").ListIteratee<T_1>>, orders?: import("lodash").Many<boolean | "asc" | "desc">): T_1[];
        <T_2 extends object>(collection: T_2, iteratees?: import("lodash").Many<import("lodash").ObjectIterator<T_2, unknown>>, orders?: import("lodash").Many<boolean | "asc" | "desc">): T_2[keyof T_2][];
        <T_3 extends object>(collection: T_3, iteratees?: import("lodash").Many<import("lodash").ObjectIteratee<T_3>>, orders?: import("lodash").Many<boolean | "asc" | "desc">): T_3[keyof T_3][];
    };
    const uniq: <T>(array: import("lodash").List<T>) => T[];
    const uniqBy: <T>(array: import("lodash").List<T>, iteratee: import("lodash").ValueIteratee<T>) => T[];
    const map: {
        <T, TResult>(collection: T[], iteratee: import("lodash").ArrayIterator<T, TResult>): TResult[];
        <T_1, TResult_1>(collection: import("lodash").List<T_1>, iteratee: import("lodash").ListIterator<T_1, TResult_1>): TResult_1[];
        <T_2>(collection: import("lodash").Dictionary<T_2> | import("lodash").NumericDictionary<T_2>): T_2[];
        <T_3 extends object, TResult_2>(collection: T_3, iteratee: import("lodash").ObjectIterator<T_3, TResult_2>): TResult_2[];
        <T_4, K extends keyof T_4>(collection: import("lodash").Dictionary<T_4> | import("lodash").NumericDictionary<T_4>, iteratee: K): T_4[K][];
        <T_5>(collection: import("lodash").Dictionary<T_5> | import("lodash").NumericDictionary<T_5>, iteratee?: string): any[];
        <T_6>(collection: import("lodash").Dictionary<T_6> | import("lodash").NumericDictionary<T_6>, iteratee?: object): boolean[];
    };
    const flatten: <T>(array: import("lodash").ListOfRecursiveArraysOrValues<T>) => import("lodash").Flat<T>[];
    const findIndex: <T>(array: import("lodash").List<T>, predicate?: import("lodash").ListIterateeCustom<T, boolean>, fromIndex?: number) => number;
    const filterLike: (arr1: any[], fieldName: string, like: string) => any[];
}
