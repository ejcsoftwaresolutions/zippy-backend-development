declare const ObjectUtils: {
    get: {
        <TObject extends object, TKey extends keyof TObject>(object: TObject, path: TKey | [TKey]): TObject[TKey];
        <TObject_1 extends object, TKey_1 extends keyof TObject_1>(object: TObject_1, path: TKey_1 | [TKey_1]): TObject_1[TKey_1];
        <TObject_2 extends object, TKey_2 extends keyof TObject_2, TDefault>(object: TObject_2, path: TKey_2 | [TKey_2], defaultValue: TDefault): TDefault | Exclude<TObject_2[TKey_2], undefined>;
        <TObject_3 extends object, TKey1 extends keyof TObject_3, TKey2 extends keyof TObject_3[TKey1]>(object: TObject_3, path: [TKey1, TKey2]): TObject_3[TKey1][TKey2];
        <TObject_4 extends object, TKey1_1 extends keyof TObject_4, TKey2_1 extends keyof TObject_4[TKey1_1]>(object: TObject_4, path: [TKey1_1, TKey2_1]): TObject_4[TKey1_1][TKey2_1];
        <TObject_5 extends object, TKey1_2 extends keyof TObject_5, TKey2_2 extends keyof TObject_5[TKey1_2], TDefault_1>(object: TObject_5, path: [TKey1_2, TKey2_2], defaultValue: TDefault_1): TDefault_1 | Exclude<TObject_5[TKey1_2][TKey2_2], undefined>;
        <TObject_6 extends object, TKey1_3 extends keyof TObject_6, TKey2_3 extends keyof TObject_6[TKey1_3], TKey3 extends keyof TObject_6[TKey1_3][TKey2_3]>(object: TObject_6, path: [TKey1_3, TKey2_3, TKey3]): TObject_6[TKey1_3][TKey2_3][TKey3];
        <TObject_7 extends object, TKey1_4 extends keyof TObject_7, TKey2_4 extends keyof TObject_7[TKey1_4], TKey3_1 extends keyof TObject_7[TKey1_4][TKey2_4]>(object: TObject_7, path: [TKey1_4, TKey2_4, TKey3_1]): TObject_7[TKey1_4][TKey2_4][TKey3_1];
        <TObject_8 extends object, TKey1_5 extends keyof TObject_8, TKey2_5 extends keyof TObject_8[TKey1_5], TKey3_2 extends keyof TObject_8[TKey1_5][TKey2_5], TDefault_2>(object: TObject_8, path: [TKey1_5, TKey2_5, TKey3_2], defaultValue: TDefault_2): TDefault_2 | Exclude<TObject_8[TKey1_5][TKey2_5][TKey3_2], undefined>;
        <TObject_9 extends object, TKey1_6 extends keyof TObject_9, TKey2_6 extends keyof TObject_9[TKey1_6], TKey3_3 extends keyof TObject_9[TKey1_6][TKey2_6], TKey4 extends keyof TObject_9[TKey1_6][TKey2_6][TKey3_3]>(object: TObject_9, path: [TKey1_6, TKey2_6, TKey3_3, TKey4]): TObject_9[TKey1_6][TKey2_6][TKey3_3][TKey4];
        <TObject_10 extends object, TKey1_7 extends keyof TObject_10, TKey2_7 extends keyof TObject_10[TKey1_7], TKey3_4 extends keyof TObject_10[TKey1_7][TKey2_7], TKey4_1 extends keyof TObject_10[TKey1_7][TKey2_7][TKey3_4]>(object: TObject_10, path: [TKey1_7, TKey2_7, TKey3_4, TKey4_1]): TObject_10[TKey1_7][TKey2_7][TKey3_4][TKey4_1];
        <TObject_11 extends object, TKey1_8 extends keyof TObject_11, TKey2_8 extends keyof TObject_11[TKey1_8], TKey3_5 extends keyof TObject_11[TKey1_8][TKey2_8], TKey4_2 extends keyof TObject_11[TKey1_8][TKey2_8][TKey3_5], TDefault_3>(object: TObject_11, path: [TKey1_8, TKey2_8, TKey3_5, TKey4_2], defaultValue: TDefault_3): TDefault_3 | Exclude<TObject_11[TKey1_8][TKey2_8][TKey3_5][TKey4_2], undefined>;
        <T>(object: import("lodash").NumericDictionary<T>, path: number): T;
        <T_1>(object: import("lodash").NumericDictionary<T_1>, path: number): T_1;
        <T_2, TDefault_4>(object: import("lodash").NumericDictionary<T_2>, path: number, defaultValue: TDefault_4): T_2 | TDefault_4;
        <TDefault_5>(object: null, path: import("lodash").PropertyPath, defaultValue: TDefault_5): TDefault_5;
        (object: null, path: import("lodash").PropertyPath): undefined;
        (object: any, path: import("lodash").PropertyPath, defaultValue?: any): any;
    };
    isNull: (value: any) => value is null;
    omit: {
        <T_3 extends object, K extends import("lodash").PropertyName[]>(object: T_3, ...paths: K): Pick<T_3, Exclude<keyof T_3, K[number]>>;
        <T_4 extends object, K_1 extends keyof T_4>(object: T_4, ...paths: import("lodash").Many<K_1>[]): import("lodash").Omit<T_4, K_1>;
        <T_5 extends object>(object: T_5, ...paths: import("lodash").Many<import("lodash").PropertyName>[]): Partial<T_5>;
    };
    mapValues: {
        <TResult>(obj: string, callback: import("lodash").StringIterator<TResult>): import("lodash").NumericDictionary<TResult>;
        <T_6 extends object, TResult_1>(obj: T_6, callback: import("lodash").ObjectIterator<T_6, TResult_1>): { [P in keyof T_6]: TResult_1; };
        <T_7>(obj: import("lodash").Dictionary<T_7> | import("lodash").NumericDictionary<T_7>, iteratee: object): import("lodash").Dictionary<boolean>;
        <T_8 extends object>(obj: T_8, iteratee: object): { [P_1 in keyof T_8]: boolean; };
        <T_9, TKey_3 extends keyof T_9>(obj: import("lodash").Dictionary<T_9> | import("lodash").NumericDictionary<T_9>, iteratee: TKey_3): import("lodash").Dictionary<T_9[TKey_3]>;
        <T_10>(obj: import("lodash").Dictionary<T_10> | import("lodash").NumericDictionary<T_10>, iteratee: string): import("lodash").Dictionary<any>;
        <T_11 extends object>(obj: T_11, iteratee: string): { [P_2 in keyof T_11]: any; };
        (obj: string): import("lodash").NumericDictionary<string>;
        <T_12>(obj: import("lodash").Dictionary<T_12> | import("lodash").NumericDictionary<T_12>): import("lodash").Dictionary<T_12>;
        <T_13 extends object>(obj: T_13): T_13;
        <T_14 extends object>(obj: T_14): Partial<T_14>;
    };
    omitUnknown: (object: any) => Partial<any>;
    pick: {
        <T_15 extends object, U extends keyof T_15>(object: T_15, ...props: import("lodash").Many<U>[]): Pick<T_15, U>;
        <T_16>(object: T_16, ...props: import("lodash").Many<import("lodash").PropertyPath>[]): Partial<T_16>;
    };
    merge: {
        <TObject_12, TSource>(object: TObject_12, source: TSource): TObject_12 & TSource;
        <TObject_13, TSource1, TSource2>(object: TObject_13, source1: TSource1, source2: TSource2): TObject_13 & TSource1 & TSource2;
        <TObject_14, TSource1_1, TSource2_1, TSource3>(object: TObject_14, source1: TSource1_1, source2: TSource2_1, source3: TSource3): TObject_14 & TSource1_1 & TSource2_1 & TSource3;
        <TObject_15, TSource1_2, TSource2_2, TSource3_1, TSource4>(object: TObject_15, source1: TSource1_2, source2: TSource2_2, source3: TSource3_1, source4: TSource4): TObject_15 & TSource1_2 & TSource2_2 & TSource3_1 & TSource4;
        (object: any, ...otherArgs: any[]): any;
    };
    isEqual: (value: any, other: any) => boolean;
    sort(obj: any): {};
};
export default ObjectUtils;
