"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const updateOrderProductsStock = async ({ change, context, }, source) => {
    const isHoldingOrder = source === constants_1.HOLDING_VENDOR_ORDERS;
    const isDelete = change.before.exists && !change.after.exists;
    const newStatus = change.after.get('status');
    if (!isHoldingOrder && !['Order Placed', 'Order Rejected'].includes(newStatus))
        return Promise.resolve();
    if (newStatus === 'Order Placed' && change.before.exists) {
        return Promise.resolve();
    }
    if (isDelete && !isHoldingOrder)
        return Promise.resolve();
    const operation = (() => {
        if (isHoldingOrder)
            return isDelete ? 'ADD' : 'SUBTRACT';
        return newStatus === 'Order Placed' ? 'SUBTRACT' : 'ADD';
    })();
    const vendorOrder = (() => {
        if (isHoldingOrder && isDelete)
            return change.before.data();
        return change.after.data();
    })();
    if (!vendorOrder)
        return Promise.resolve();
    return processOrder(vendorOrder, operation);
};
async function processOrder(vendorOrder, operation) {
    functions.logger.log('Updating order products stock', vendorOrder.id);
    const mappedProducts = vendorOrder.products.map((p) => ({
        ...p,
        vendorID: vendorOrder.vendorID,
        orderId: vendorOrder.orderId,
        hasSections: !!p.sections && p.sections.length > 0,
    }));
    const db = admin.firestore();
    try {
        await db.runTransaction(async (transaction) => {
            var _a;
            const changes = {};
            for (const p of mappedProducts) {
                const vendorProductRef = admin
                    .firestore()
                    .collection(constants_1.VENDOR_PRODUCTS)
                    .doc(p.id);
                const vendorProduct = changes[p.id] ? await Promise.resolve(changes[p.id].data) : (_a = (await transaction.get(vendorProductRef))) === null || _a === void 0 ? void 0 : _a.data();
                if (!vendorProduct) {
                    functions.logger.log('Could not find product to update the stock: ', p.id);
                    continue;
                }
                const updatedProduct = await handleProductStock(operation, p, vendorProduct);
                changes[p.id] = {
                    ref: vendorProductRef, data: updatedProduct
                };
            }
            Object.values(changes).forEach((item) => {
                transaction.update(item.ref, item.data);
            });
        });
    }
    catch (e) {
        functions.logger.log(' Error in transaction ' + e.message);
    }
}
async function handleProductStock(operation, orderProduct, vendorProduct) {
    let updatedProduct = vendorProduct;
    updatedProduct = await handleMainProductStock(updatedProduct, operation, orderProduct);
    if (orderProduct.optionId) {
        updatedProduct = await handleProductVariantStock(updatedProduct, operation, orderProduct);
    }
    if (orderProduct.hasSections) {
        updatedProduct = await handleProductsSectionsStock(updatedProduct, operation, orderProduct.sections);
    }
    return updatedProduct;
}
async function handleMainProductStock(vendorProduct, operation, orderProduct) {
    const newStock = vendorProduct.stockCount + orderProduct.quantity * (operation === 'SUBTRACT' ? -1 : 1);
    functions.logger.log('Main stock for product ' + orderProduct.id, newStock);
    return {
        ...vendorProduct, stockCount: newStock >= 0 ? newStock : 0, available: newStock > 0,
    };
}
async function handleProductVariantStock(vendorProduct, operation, orderProduct) {
    const newVariants = vendorProduct.options.map((option) => {
        var _a;
        const isSelectedVariant = option.id == orderProduct.optionId;
        if (!isSelectedVariant)
            return option;
        const selectedOptionQuantity = orderProduct.quantity;
        const newStock = ((_a = option.stockCount) !== null && _a !== void 0 ? _a : 0) + selectedOptionQuantity * (operation === 'SUBTRACT' ? -1 : 1);
        functions.logger.log('New variant stock for product ' + orderProduct.option, newStock);
        return {
            ...option, stockCount: newStock >= 0 ? newStock : 0, available: newStock > 0,
        };
    });
    return {
        ...vendorProduct, options: newVariants,
    };
}
async function handleProductsSectionsStock(vendorProduct, operation, orderProductSections) {
    const newSections = vendorProduct.sections.map((section) => {
        const orderSection = orderProductSections.find((oS) => oS.id === section.id);
        return getUpdatedProductSectionOptions(section, operation, orderSection);
    });
    return {
        ...vendorProduct, sections: newSections,
    };
}
function getUpdatedProductSectionOptions(productSection, operation, orderSection) {
    if (!orderSection)
        return productSection;
    const availableOptions = productSection.availableOptions.reduce((acc, current) => {
        var _a, _b;
        const selectedOptionQuantity = (_a = orderSection.selectedOptions.find((sO) => sO.id == current.id)) === null || _a === void 0 ? void 0 : _a.qty;
        if (!selectedOptionQuantity)
            return [...acc, current];
        const newStock = ((_b = current.stockCount) !== null && _b !== void 0 ? _b : 0) + selectedOptionQuantity * (operation === 'SUBTRACT' ? -1 : 1);
        return [...acc, {
                ...current, stockCount: newStock >= 0 ? newStock : 0, available: newStock > 0,
            },];
    }, []);
    return {
        ...productSection, availableOptions: availableOptions,
    };
}
exports.default = updateOrderProductsStock;
//# sourceMappingURL=index.js.map