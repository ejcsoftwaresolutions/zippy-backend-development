import { Change, EventContext } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import DocumentSnapshot = firestore.DocumentSnapshot;
declare const updateOrderProductsStock: ({ change, context, }: {
    change: Change<DocumentSnapshot>;
    context: EventContext;
}, source: string) => Promise<void>;
export default updateOrderProductsStock;
