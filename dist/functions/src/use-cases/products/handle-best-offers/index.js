"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleBestOffers = void 0;
const array_utils_1 = require("../../../utils/array-utils");
const object_utils_1 = require("../../../utils/object-utils");
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const handleBestOffers = async (change, context) => {
    functions.logger.log('Best offers ', context.params.documentId);
    const offerRef = await admin
        .firestore()
        .collection('product_promotion')
        .doc(context.params.documentId);
    const document = change.after.data();
    const oldDocument = change.before.data();
    const offerData = (await offerRef.get()).exists;
    if (!offerData)
        return Promise.resolve();
    if (!document) {
        return Promise.resolve();
    }
    const vendor = await findVendor(document.vendorID);
    const hasVariants = Boolean(document.options) && document.options.length > 0;
    if (hasVariants) {
        try {
            await syncVariantProducts(document, vendor);
        }
        catch (e) {
            functions.logger.log('Error at syncing variants ' + e.message);
        }
        return Promise.resolve();
    }
    try {
        await syncSingleProducts(document, oldDocument, offerRef);
    }
    catch (e) {
        functions.logger.log('Error at single product ' + e.message);
    }
    return Promise.resolve();
};
exports.handleBestOffers = handleBestOffers;
async function findVendor(vendorId) {
    const vendorRef = await admin.firestore().collection('vendors').doc(vendorId);
    return (await vendorRef.get()).data();
}
async function saveOffer(document) {
    const vendor = await findVendor(document.vendorID);
    if (!vendor)
        return {};
    const offer = getMappedOffer(document, vendor);
    return offer;
}
function getMappedOffer(document, vendor) {
    var _a, _b, _c, _d, _e, _f;
    return object_utils_1.default.omitUnknown({
        vendorProductID: document.id,
        productTitle: document.name,
        description: document.description,
        productFileName: (_a = document.thumbnailUrl) !== null && _a !== void 0 ? _a : document.photo,
        oldPrice: (_b = document.price) !== null && _b !== void 0 ? _b : 0,
        newPrice: (_c = document.discountPrice) !== null && _c !== void 0 ? _c : 0,
        percentageDiscount: (_d = document.discountRate) !== null && _d !== void 0 ? _d : 0,
        vendorID: document.vendorID,
        variantID: document.variantId,
        logoFileName: vendor.logo,
        isPromotion: document.isPromotion,
        type: (_e = document.type) !== null && _e !== void 0 ? _e : 'REGULAR',
        categoryID: document.categoryID,
        subcategoryID: document.subcategoryID,
        currency: document.currency,
        order: (_f = document.promoOrder) !== null && _f !== void 0 ? _f : 999999999,
    });
}
async function syncSingleProducts(document, oldDocument, offerRef) {
    functions.logger.log('Syncing Single product discounts', document);
    const offer = await saveOffer(document);
    return offerRef.set(offer);
}
async function syncVariantProducts(document, vendor, clean = false) {
    var _a, _b;
    if (!vendor)
        return Promise.resolve();
    const offerRef = admin.firestore().collection('product_promotion');
    const lowestDiscountPriceVariant = array_utils_1.ArrayUtils.orderBy(document.options, 'discountPrice', 'asc').find((o) => {
        return !!o.applyDiscount;
    });
    const selectedVariant = lowestDiscountPriceVariant !== null && lowestDiscountPriceVariant !== void 0 ? lowestDiscountPriceVariant : (_a = document.options) === null || _a === void 0 ? void 0 : _a[0];
    if (!selectedVariant) {
        return Promise.resolve();
    }
    const bestOffer = getMappedOffer({
        ...document,
        price: selectedVariant.price,
        discountRate: selectedVariant.discountRate,
        discountPrice: selectedVariant.discountPrice,
        vendorID: document.vendorID,
        id: document.id,
        variantId: selectedVariant.id,
        name: `${document.name}`,
        photo: (_b = document.thumbnailUrl) !== null && _b !== void 0 ? _b : document.photo,
        description: document.description,
    }, vendor);
    return offerRef.doc(document.id).set(bestOffer);
}
//# sourceMappingURL=index.js.map