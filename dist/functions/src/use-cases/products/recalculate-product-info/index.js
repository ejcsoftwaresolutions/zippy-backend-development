"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.recalculateProductInfo = void 0;
const recalculate_product_1 = require("../../../utils/vendor-products/recalculate-product");
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const recalculateProductInfo = async (change, context) => {
    functions.logger.log('Recalculate product data ', context.params.documentId);
    if (context.eventType.indexOf('delete') > -1)
        return;
    const document = change.after.data();
    const fields = (0, recalculate_product_1.default)(document);
    if (!fields)
        return Promise.resolve();
    if (Object.keys(fields).length == 0)
        return Promise.resolve();
    try {
        await admin
            .firestore()
            .collection('vendor_products')
            .doc(context.params.documentId)
            .update(fields);
    }
    catch (e) {
        functions.logger.log('Recalculate product error:', e.message);
    }
    return Promise.resolve();
};
exports.recalculateProductInfo = recalculateProductInfo;
//# sourceMappingURL=index.js.map