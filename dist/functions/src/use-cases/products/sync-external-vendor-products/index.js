"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sucasa_product_fetcher_1 = require("../../../utils/external-vendor-product-syncronizer/fetchers/sucasa-product-fetcher");
const sucasa_mapper_1 = require("../../../utils/external-vendor-product-syncronizer/mappers/sucasa-mapper");
const external_vendor_product_catalog_importer_1 = require("../../../utils/external-vendor-product-syncronizer/external-vendor-product-catalog-importer");
const sucasa_product_batcher_1 = require("../../../utils/external-vendor-product-syncronizer/batchers/sucasa-product-batcher");
const firebase_admin_1 = require("firebase-admin");
const functions = require('firebase-functions');
const syncExternalVendorProducts = async (context) => {
    var _a;
    const active = (_a = (await firebase_admin_1.default
        .firestore()
        .collection('app_configurations')
        .doc('OTHER')
        .get()).data()) === null || _a === void 0 ? void 0 : _a.sucasaSyncEnabled;
    if (!active) {
        functions.logger.log('sucasa sync not enabled');
        return Promise.resolve();
    }
    syncSUCASA().then(() => {
    });
};
exports.default = syncExternalVendorProducts;
async function syncSUCASA() {
    const productImporter = new external_vendor_product_catalog_importer_1.default();
    try {
        const isDev = process.env.NODE_ENV != 'production';
        const sellerId = isDev ? '2HvDlZbhgSSKAefzQv32' : 'y7EHAW9EDROgcRSSrKYk';
        const vendor = (await firebase_admin_1.default.firestore().collection('vendors').doc(sellerId).get()).data();
        if (!vendor)
            return;
        const sellerCategoryId = vendor.categoryID;
        const extraCommissionPercentage = vendor.salesCommissionPercentage;
        const fetcher = new sucasa_product_fetcher_1.default();
        const categories = await fetcher.getCategories();
        await productImporter.syncSubcategories(sellerId, sellerCategoryId, categories, sucasa_mapper_1.default);
        const start = new Date();
        start.setUTCHours(0, 0, 0, 0);
        const end = new Date();
        end.setUTCHours(23, 59, 59, 999);
        const batcher = new sucasa_product_batcher_1.default(productImporter, {
            sellerId: sellerId,
            sellerCategoryId: sellerCategoryId,
            extraCommissionPercentage,
        });
        const hasMappings = await productImporter.hasProductsMappings(sellerId);
        if (!hasMappings)
            return Promise.resolve();
        batcher
            .process(categories, {
            mode: 'PARTIAL-SYNC',
        })
            .then(() => {
            functions.logger.log('sucasa product sync success');
        })
            .catch((e) => {
            functions.logger.log('sucasa product sync error: ' + e.message);
        });
    }
    catch (e) {
        functions.logger.log('sucasa product sync error: ' + e.message);
    }
}
//# sourceMappingURL=index.js.map