"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const updateOrderProductsDemand = async (change, context) => {
    var _a;
    const oldStatus = change.before.get('status');
    const newStatus = change.after.get('status');
    if (!['Order Placed', 'Order Rejected'].includes(newStatus))
        return Promise.resolve();
    if (newStatus === 'Order Placed' && !!oldStatus) {
        return Promise.resolve();
    }
    const operation = newStatus === 'Order Placed' ? 'SUBTRACT' : 'ADD';
    const vendorOrder = (_a = (await admin
        .firestore()
        .collection(constants_1.VENDOR_ORDERS)
        .doc(context.params.documentId)
        .get())) === null || _a === void 0 ? void 0 : _a.data();
    if (!vendorOrder)
        return Promise.resolve();
    functions.logger.log('Updating order products demand', context.params.documentId);
    const mappedProducts = vendorOrder.products.map((p) => ({
        ...p,
        vendorID: vendorOrder.vendorID,
        orderId: vendorOrder.orderId,
    }));
    const promises = mappedProducts.map(async (p) => {
        return new Promise(async (resolve, reject) => {
            const vendorProductRef = admin
                .firestore()
                .collection(constants_1.VENDOR_PRODUCTS)
                .doc(p.id);
            const vendorProduct = (await vendorProductRef.get()).data();
            if (!vendorProduct)
                return resolve({});
            const newSales = vendorProduct.salesQuantity +
                p.quantity * (operation === 'SUBTRACT' ? -1 : 1);
            return vendorProductRef.update({
                salesQuantity: newSales >= 0 ? newSales : 0,
            });
        });
    });
    return Promise.all(promises);
};
exports.default = updateOrderProductsDemand;
//# sourceMappingURL=index.js.map