declare const notifyDriverWithdrawalRequest: (snap: any, context: any) => Promise<void | FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>[]>;
export default notifyDriverWithdrawalRequest;
