"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const notifyDriverWithdrawalRequest = async (snap, context) => {
    var _a, _b, _c, _d;
    functions.logger.log('Notify driver withdrawal request', context.params.documentId);
    const data = snap.data();
    const ADMIN_EMAIL = process.env.ZIPPI_PAYMENTS_EMAIL;
    const account = await app_1.default.findAccount(data.driverId);
    if (!account)
        return Promise.resolve();
    return app_1.default.sendEmail(ADMIN_EMAIL, {
        subject: `Nueva solicitud de retiro de rider`,
        text: `Nueva solicitud de retiro para el rider ${(_a = account.firstName) === null || _a === void 0 ? void 0 : _a.trim()} ${(_b = account.lastName) === null || _b === void 0 ? void 0 : _b.trim()} `,
        html: `
      <div>
        <p>Hola, el rider ${(_c = account.firstName) === null || _c === void 0 ? void 0 : _c.trim()} ${(_d = account.lastName) === null || _d === void 0 ? void 0 : _d.trim()} ha solicitado el retiro de $${data.amount}</p>
        <br/>
      </div>
    `,
    });
};
exports.default = notifyDriverWithdrawalRequest;
//# sourceMappingURL=index.js.map