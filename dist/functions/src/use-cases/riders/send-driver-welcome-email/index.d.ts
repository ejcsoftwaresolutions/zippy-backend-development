declare const sendDriverWelcomeEmail: (snap: any, context: any) => Promise<[FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>[]]>;
export default sendDriverWelcomeEmail;
