"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendRiderNewDeliveryNotification = void 0;
const functions = require('firebase-functions');
const app_1 = require("../../../utils/app");
const sendRiderNewDeliveryNotification = async (snap, context) => {
    functions.logger.log('Sending rider new delivery notification', context.params.documentId);
    const data = snap.val();
    await app_1.default.sendPushNotification({
        data: {
            riderId: data.driverId,
            orderId: data.id,
        },
        notification: {
            title: `Alerta de nueva solicitud de delivery`,
            body: `Tienes ${data.acceptanceTimeout} segundos para aceptar!`,
        },
        channel: 'new-order',
    }, 'RIDER', data.driverId);
    return Promise.resolve();
};
exports.sendRiderNewDeliveryNotification = sendRiderNewDeliveryNotification;
//# sourceMappingURL=index.js.map