"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncVendorLocation = void 0;
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const syncVendorLocation = async (change, context) => {
    const vendor = change.after.data();
    const lastVendor = change.before.data();
    if (!vendor)
        return Promise.resolve();
    functions.logger.log('Updating vendor location', context.params.documentId);
    if (!vendor.location)
        return Promise.resolve();
    if (!lastVendor) {
        return app_1.default.syncVendorGeolocation(vendor.id, [vendor.location.latitude, vendor.location.longitude,]);
    }
    if (lastVendor.location.latitude === vendor.location.latitude && lastVendor.location.longitude === vendor.location.longitude)
        return Promise.resolve();
    return app_1.default.syncVendorGeolocation(vendor.id, [vendor.location.latitude, vendor.location.longitude,]);
};
exports.syncVendorLocation = syncVendorLocation;
//# sourceMappingURL=index.js.map