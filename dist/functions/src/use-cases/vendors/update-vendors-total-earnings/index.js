"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const constants_1 = require("../../../constants");
const functions = require('firebase-functions');
const updateVendorTotalEarnings = async (change, context) => {
    functions.logger.log('Updating vendor total earnings', context.params.documentId);
    const newStatus = change.after.get('status');
    const oldStatus = change.before.get('status');
    if (newStatus === oldStatus)
        return Promise.resolve();
    if (newStatus !== 'Order Completed')
        return Promise.resolve();
    const orderRef = admin
        .firestore()
        .collection(constants_1.VENDOR_ORDERS)
        .doc(context.params.documentId);
    const vendorOrder = (await orderRef.get()).data();
    if (!vendorOrder)
        return Promise.resolve();
    const vendorOrderCommission = vendorOrder.vendorCommission;
    if (vendorOrderCommission.processed)
        return Promise.resolve();
    const newEarning = vendorOrderCommission ? vendorOrderCommission.earned : vendorOrder.subtotal;
    const vendorEarnings = (await admin
        .firestore()
        .collection(constants_1.VENDORS_TOTAL_EARNINGS)
        .doc(vendorOrder.vendorID)
        .get()).data();
    if (vendorEarnings) {
        const demandDailyDocRef = await admin
            .firestore()
            .collection(constants_1.VENDORS_TOTAL_EARNINGS)
            .doc(vendorOrder.vendorID);
        return demandDailyDocRef.update({
            total: admin.firestore.FieldValue.increment(parseFloat(newEarning)), lastUpdate: new Date(),
        });
    }
    const info = {
        vendorID: vendorOrder.vendorID, total: parseFloat(newEarning), lastUpdate: new Date(),
    };
    await admin
        .firestore()
        .collection(constants_1.VENDORS_TOTAL_EARNINGS)
        .doc(vendorOrder.vendorID)
        .set(info);
    return orderRef.update({
        ['vendorCommission.processed']: true,
    });
};
exports.default = updateVendorTotalEarnings;
//# sourceMappingURL=index.js.map