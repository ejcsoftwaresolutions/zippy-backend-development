"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const sales_invoice_1 = require("./sales-invoice");
const constants_1 = require("../../../constants");
const moment = require("moment");
const functions = require('firebase-functions');
const pdf = require('html-pdf');
const uuid = require('uuid');
function getDate(firebaseDate) {
    return new Date(firebaseDate.seconds * 1000);
}
const generateVendorSalesInvoice = async (change, context) => {
    functions.logger.log('Generating sales invoice', context.params.documentId);
    const newStatus = change.after.get('status');
    if (newStatus !== 'REQUESTED')
        return Promise.resolve();
    const invoiceRef = await admin
        .firestore()
        .collection('vendor_invoices')
        .doc(context.params.documentId);
    const vendorInvoiceQ = await invoiceRef.get();
    const vendorInvoice = vendorInvoiceQ.data();
    if (!vendorInvoice)
        return Promise.resolve();
    const vendorSalesQ = await admin
        .firestore()
        .collection(constants_1.VENDOR_ORDERS)
        .where('vendorID', '==', vendorInvoice.vendorId)
        .where('createdAt', '>=', moment(getDate(vendorInvoice.start)).startOf('day').toDate())
        .where('createdAt', '<=', moment(getDate(vendorInvoice.end)).endOf('day').toDate())
        .orderBy('createdAt', 'asc')
        .get();
    const vendorSales = vendorSalesQ.docs.map((s) => {
        var _a, _b;
        const sale = s.data();
        return {
            date: moment(getDate(sale.createdAt)).format('DD/MM/YYYY HH:mm'),
            subtotal: parseFloat(sale.subtotal),
            total: parseFloat((_b = (_a = sale.vendorCommission) === null || _a === void 0 ? void 0 : _a.earned) !== null && _b !== void 0 ? _b : 0),
        };
    });
    const subtotal = vendorSales
        .map((s) => s.subtotal)
        .reduce((a, b) => a + b, 0);
    const total = vendorSales
        .map((s) => s.total)
        .reduce((a, b) => a + b, 0);
    functions.logger.log('Total de ventas', vendorSales.length);
    const fileName = `vendors/${vendorInvoice.vendorId}/invoices/${context.params.documentId}${moment().format('DD/MM/YYYY HH:mm:ss')}.pdf`;
    const fileRef = admin.storage().bucket().file(fileName, {});
    const content = sales_invoice_1.salesInvoiceHtml
        .replace('{{PERIOD}}', `${moment(getDate(vendorInvoice.start)).format('DD/MM/YYYY')} al ${moment(getDate(vendorInvoice.end)).format('DD/MM/YYYY')}`)
        .replace('{{PRODUCTS_HTML}}', `
     <div class="product">
            ${vendorSales
        .map((s) => {
        return `<div class="field">
              <p>#${s.code} ${s.date}</p>
                <div class="totals">
                    <p style="margin-right: 3px">${s.subtotal} USD</p>
                    <p>${s.total} USD</p>
                </div>
              </div>`;
    })
        .join('')}
            </div>
    `)
        .replace('{{SUBTOTAL}}', `${subtotal}`)
        .replace('{{TOTAL}}', `${total}`);
    await invoiceRef.update({ status: 'GENERATING' });
    return new Promise((resolve) => {
        pdf
            .create(content, {
            timeout: '100000', width: 300, height: 800,
        })
            .toStream(async (err, stream) => {
            if (err) {
                await invoiceRef.update({
                    status: 'ERROR',
                });
                functions.logger.log('Error', err);
                return;
            }
            await stream.pipe(fileRef.createWriteStream({
                metadata: {
                    contentType: 'application/pdf', metadata: {
                        firebaseStorageDownloadTokens: uuid.v4(), lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                    },
                }, resumable: false, public: true,
            }));
            await invoiceRef.update({
                status: 'CREATED', url: fileRef.publicUrl(), updatedAt: new Date(),
            });
            resolve({});
        });
    });
};
exports.default = generateVendorSalesInvoice;
//# sourceMappingURL=index.js.map