"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleCustomerReferralPrizes = void 0;
const constants_1 = require("../../../constants");
const handle_app_credits_transactions_1 = require("../handle-app-credits-transactions");
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const MIN_ORDER_TOTAL = 20;
const PRIZE = 5;
const handleCustomerReferralPrizes = async (change, context) => {
    const order = change.after.data();
    if (!order)
        return Promise.resolve();
    const customerId = order.customer.id;
    const customer = await app_1.default.findAccount(customerId);
    if (!customer)
        return Promise.resolve();
    const referrerCode = customer.referrerCode;
    if (!referrerCode || referrerCode === '')
        return Promise.resolve();
    const referrer = await findReferrerAccount(referrerCode);
    if (!referrer)
        return Promise.resolve();
    if (customer.id === referrer.id)
        return Promise.resolve();
    if (order.status == 'Order Rejected')
        return Promise.resolve();
    if (!(await isCustomerFirstOrder(customerId)))
        return Promise.resolve();
    if (await customerAlreadyReceivedReferralPrize(customerId))
        return Promise.resolve();
    if (order.total < MIN_ORDER_TOTAL)
        return Promise.resolve();
    functions.logger.log('New referral prizes', referrerCode);
    try {
        await giveReferralPrize(customerId, {
            minOrderTotal: MIN_ORDER_TOTAL,
            referrer: referrer,
            code: referrerCode,
            orderTotal: order.total,
            orderId: order.id,
        });
    }
    catch (e) {
        functions.logger.log('Error at giving referral prize', e);
    }
    try {
        await giveReferrerPrize(referrer.id, {
            minOrderTotal: MIN_ORDER_TOTAL,
            referral: customer,
            code: referrerCode,
            orderTotal: order.total,
            orderId: order.id,
        });
    }
    catch (e) {
        functions.logger.log('Error at giving referrer prize', e);
    }
};
exports.handleCustomerReferralPrizes = handleCustomerReferralPrizes;
async function isCustomerFirstOrder(userId) {
    const item = await admin
        .firestore()
        .collection(`${constants_1.VENDOR_ORDERS}`)
        .where('customer.id', '==', userId)
        .get();
    return item.size === 1;
}
async function customerAlreadyReceivedReferralPrize(userId) {
    const item = await admin
        .firestore()
        .collection(`${constants_1.CREDIT_LOG_REF}`)
        .where('userId', '==', userId)
        .where('concept', '==', 'REFERRAL_PRIZE')
        .get();
    return !item.empty;
}
async function findReferrerAccount(code) {
    const item = await admin
        .firestore()
        .collection(`${constants_1.CUSTOMERS}`)
        .where('referralCode', '==', code)
        .get();
    const itemsDto = item.docs.map((d) => d.data());
    if (itemsDto.length === 0)
        return;
    const user = itemsDto[0];
    return app_1.default.findAccount(user.id);
}
async function giveReferralPrize(userId, details) {
    var _a;
    const log = await (0, handle_app_credits_transactions_1.createReferralCreditLog)({
        userId: userId,
        amount: PRIZE,
        metadata: {
            conditionsMatched: {
                minOrderTotal: {
                    minValue: details.minOrderTotal,
                    orderTotal: details.orderTotal,
                    orderId: details.orderId,
                },
            },
            code: details.code,
            referrerUserName: `${details.referrer.firstName} ${(_a = details.referrer.lastName) !== null && _a !== void 0 ? _a : ''}`,
            referrerUserId: details.referrer.id,
        },
    });
    await admin.firestore().collection(constants_1.CREDIT_LOG_REF).doc(log.id).set(log);
    try {
        await app_1.default.sendPushNotification({
            notification: {
                title: `Has ganado $${PRIZE} en Zippi Credits!`,
                body: `Has ganado $${PRIZE} en Zippi Credits por procesar tu primera compra!`,
            },
        }, 'CUSTOMER', userId);
    }
    catch (e) {
    }
    try {
        const { email, configurations } = await app_1.default.findUserNotificationInfo(userId, 'customer');
        if (!(configurations === null || configurations === void 0 ? void 0 : configurations.receiveEmails))
            return;
        await app_1.default.sendEmail(email, {
            subject: `Zippi - Has ganado $${PRIZE} en Zippi Credits!`,
            text: `Zippi - Has ganado $${PRIZE} en Zippi Credits!`,
            html: `
       <div>
          <p>
          Felicidades! Has ganado $${PRIZE} en Zippi Credits por procesar tu primera compra!, tu amigo ${log.metadata.referrerUserName} también ha ganado!. 
          </p>
          <br/>
          <p>
          Comparte tu propio código con tus panas y gana aún más!
          </p>
          <p> <strong> Gracias por preferirnos.</strong> </p>
        </div>
      `,
        });
    }
    catch (e) {
    }
}
async function giveReferrerPrize(userId, details) {
    var _a;
    const log = await (0, handle_app_credits_transactions_1.createReferrerCreditLog)({
        userId: userId,
        amount: PRIZE,
        metadata: {
            conditionsMatched: {
                minOrderTotal: {
                    minValue: details.minOrderTotal,
                    orderTotal: details.orderTotal,
                    orderId: details.orderId,
                },
            },
            code: details.code,
            referralUserName: `${details.referral.firstName} ${(_a = details.referral.lastName) !== null && _a !== void 0 ? _a : ''}`,
            referralUserId: details.referral.id,
        },
    });
    await admin.firestore().collection(constants_1.CREDIT_LOG_REF).doc(log.id).set(log);
    const { email, configurations } = await app_1.default.findUserNotificationInfo(userId, 'customer');
    try {
        await app_1.default.sendPushNotification({
            notification: {
                title: `Has ganado $${PRIZE} en Zippi Credits!`,
                body: `Has ganado $${PRIZE} en Zippi Credits. tu amigo ${log.metadata.referralUserName} se unió y realizó su primera compra!`,
            },
        }, 'CUSTOMER', userId);
    }
    catch (e) {
    }
    try {
        if (!(configurations === null || configurations === void 0 ? void 0 : configurations.receiveEmails))
            return;
        await app_1.default.sendEmail(email, {
            subject: `Zippi - Has ganado $${PRIZE} en Zippi Credits!`,
            text: `Has ganado $${PRIZE} en Zippi Credits. Tu amigo ${log.metadata.referralUserName} se unió y realizó su primera compra!`,
            html: `
       <div>
          <p>
          Felicidades! Haz ganado $${PRIZE} en Zippi Credits. tu amigo ${log.metadata.referralUserName} se unió y realizó su primera compra!
          </p>
          <br/>
          <p>
            Sigue enviando tu código a más panas y gana aún más!
          </p>
          <p> <strong> Gracias por preferirnos.</strong> </p>
        </div>
      `,
        });
    }
    catch (e) {
    }
}
//# sourceMappingURL=index.js.map