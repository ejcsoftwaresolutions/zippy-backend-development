"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createReferrerCreditLog = exports.createReferralCreditLog = exports.createRefundCreditLog = exports.createPurchaseCreditLog = exports.createRechargeCreditLog = exports.sendOtherCreditNotification = exports.handleAppCreditsTransactions = void 0;
const admin = require('firebase-admin');
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const handleAppCreditsTransactions = async (change, context) => {
    functions.logger.log('Credit transaction ', context.params.documentId);
    const document = change.after.data();
    const oldDocument = change.before.data();
    if (!document && oldDocument) {
        return resetUserLastCredits(oldDocument.userId);
    }
    if (!document)
        return Promise.resolve();
    const logData = document;
    if (logData.processed)
        return Promise.resolve();
    await processTransactionLog({
        ref: change.after.ref,
        amount: logData.amount,
        operation: logData.operation,
        userId: logData.userId,
        concept: logData.concept,
    });
    if (logData.operation === 'CREDIT' && logData.concept === 'OTHER') {
        await sendOtherCreditNotification({
            userId: logData.userId,
            amount: logData.amount,
            description: logData.conceptDescription,
        });
    }
    return Promise.resolve();
};
exports.handleAppCreditsTransactions = handleAppCreditsTransactions;
async function getUserLastCreditAmount(userId) {
    var _a, _b;
    const lastLog = (_a = (await admin
        .firestore()
        .collection(constants_1.CREDIT_LOG_REF)
        .where('userId', '==', userId)
        .where('processed', '==', true)
        .orderBy('date', 'desc')
        .get()).docs[0]) === null || _a === void 0 ? void 0 : _a.data();
    if (!lastLog)
        return 0;
    return (_b = lastLog.newCreditAmount) !== null && _b !== void 0 ? _b : 0;
}
async function processTransactionLog(params) {
    const lastCreditAmount = await getUserLastCreditAmount(params.userId);
    let newCreditAmount = lastCreditAmount + params.amount * (params.operation == 'CREDIT' ? 1 : -1);
    newCreditAmount =
        newCreditAmount < 0 ? 0 : parseFloat(newCreditAmount.toFixed(2));
    await params.ref.update({
        processed: true,
        newCreditAmount: newCreditAmount,
        lastCreditAmount: lastCreditAmount,
        processedAt: new Date(),
    });
    await admin.firestore().collection(constants_1.CUSTOMERS).doc(params.userId).update({
        credits: newCreditAmount,
        lastAppCreditUpdate: new Date(),
    });
}
async function resetUserLastCredits(userId) {
    const lastCreditAmount = await getUserLastCreditAmount(userId);
    await admin.firestore().collection(constants_1.CUSTOMERS).doc(userId).update({
        credits: lastCreditAmount,
        lastAppCreditUpdate: new Date(),
    });
}
async function sendOtherCreditNotification({ userId, amount, description, }) {
    await app_1.default.sendPushNotification({
        notification: {
            title: `Se han acreditado $${amount} en Zippi Credits!`,
            body: `Concepto: ${description}`,
        },
    }, 'CUSTOMER', userId);
}
exports.sendOtherCreditNotification = sendOtherCreditNotification;
async function createRechargeCreditLog({ userId, amount, metadata, }) {
    return {
        id: await app_1.default.createId(),
        date: new Date(),
        operation: 'CREDIT',
        concept: 'RECHARGE',
        processed: false,
        metadata: metadata,
        userId: userId,
        amount: amount,
    };
}
exports.createRechargeCreditLog = createRechargeCreditLog;
async function createPurchaseCreditLog({ userId, amount, metadata, }) {
    return {
        id: await app_1.default.createId(),
        date: new Date(),
        operation: 'DEBIT',
        concept: 'PURCHASE',
        processed: false,
        metadata: metadata,
        userId: userId,
        amount: amount,
    };
}
exports.createPurchaseCreditLog = createPurchaseCreditLog;
async function createRefundCreditLog({ userId, amount, metadata, }) {
    return {
        id: await app_1.default.createId(),
        date: new Date(),
        operation: 'CREDIT',
        concept: `Reembolso Zippi Credits en compra #${metadata.orderCode}`,
        processed: false,
        metadata: metadata,
        userId: userId,
        amount: amount,
    };
}
exports.createRefundCreditLog = createRefundCreditLog;
async function createReferralCreditLog({ userId, amount, metadata, }) {
    return {
        id: await app_1.default.createId(),
        date: new Date(),
        operation: 'CREDIT',
        concept: 'REFERRAL_PRIZE',
        processed: false,
        metadata: metadata,
        userId: userId,
        amount: amount,
    };
}
exports.createReferralCreditLog = createReferralCreditLog;
async function createReferrerCreditLog({ userId, amount, metadata, }) {
    return {
        id: await app_1.default.createId(),
        date: new Date(),
        operation: 'CREDIT',
        concept: 'REFERRER_PRIZE',
        processed: false,
        metadata: metadata,
        userId: userId,
        amount: amount,
    };
}
exports.createReferrerCreditLog = createReferrerCreditLog;
//# sourceMappingURL=index.js.map