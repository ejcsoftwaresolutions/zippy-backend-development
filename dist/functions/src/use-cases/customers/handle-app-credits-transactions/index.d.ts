import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import DocumentSnapshot = firestore.DocumentSnapshot;
export declare const handleAppCreditsTransactions: (change: Change<DocumentSnapshot>, context: EventContext) => Promise<void>;
export declare function sendOtherCreditNotification({ userId, amount, description, }: {
    userId: any;
    amount: any;
    description: any;
}): Promise<void>;
export declare function createRechargeCreditLog({ userId, amount, metadata, }: {
    userId: string;
    amount: number;
    metadata: {
        rechargeId: string;
    };
}): Promise<{
    id: string;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: {
        rechargeId: string;
    };
    userId: string;
    amount: number;
}>;
export declare function createPurchaseCreditLog({ userId, amount, metadata, }: {
    userId: string;
    amount: number;
    metadata: {
        orderId: string;
        orderTotal: number;
        vendorId: string;
        vendorName: string;
    };
}): Promise<{
    id: string;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: {
        orderId: string;
        orderTotal: number;
        vendorId: string;
        vendorName: string;
    };
    userId: string;
    amount: number;
}>;
export declare function createRefundCreditLog({ userId, amount, metadata, }: {
    userId: string;
    amount: number;
    metadata: {
        orderId: string;
        orderCode: string;
    };
}): Promise<{
    id: string;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: {
        orderId: string;
        orderCode: string;
    };
    userId: string;
    amount: number;
}>;
export declare function createReferralCreditLog({ userId, amount, metadata, }: {
    userId: string;
    amount: number;
    metadata: {
        conditionsMatched: {
            minOrderTotal: {
                orderId: string;
                orderTotal: number;
                minValue: number;
            };
        };
        code: string;
        referrerUserId: string;
        referrerUserName: string;
    };
}): Promise<{
    id: string;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: {
        conditionsMatched: {
            minOrderTotal: {
                orderId: string;
                orderTotal: number;
                minValue: number;
            };
        };
        code: string;
        referrerUserId: string;
        referrerUserName: string;
    };
    userId: string;
    amount: number;
}>;
export declare function createReferrerCreditLog({ userId, amount, metadata, }: {
    userId: string;
    amount: number;
    metadata: {
        conditionsMatched: {
            minOrderTotal: {
                orderId: string;
                orderTotal: number;
                minValue: number;
            };
        };
        code: string;
        referralUserId: string;
        referralUserName: string;
    };
}): Promise<{
    id: string;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: {
        conditionsMatched: {
            minOrderTotal: {
                orderId: string;
                orderTotal: number;
                minValue: number;
            };
        };
        code: string;
        referralUserId: string;
        referralUserName: string;
    };
    userId: string;
    amount: number;
}>;
