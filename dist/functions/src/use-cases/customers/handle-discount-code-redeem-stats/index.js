"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleDiscountCodeRedeemStats = void 0;
const firebase_admin_1 = require("firebase-admin");
const constants_1 = require("../../../constants");
const datetime_utils_1 = require("../../../utils/datetime-utils");
var FieldValue = firebase_admin_1.firestore.FieldValue;
const admin = require('firebase-admin');
const handleDiscountCodeRedeemStats = async (change, context) => {
    const redeem = change.after.data();
    const old = change.before.data();
    if (!redeem && old) {
        return updateCodeCounter(old.code.id, 'SUBTRACT');
    }
    if (redeem && old) {
        return Promise.resolve();
    }
    if (!redeem)
        return Promise.resolve();
    return updateCodeCounter(redeem.code.id, 'ADD');
};
exports.handleDiscountCodeRedeemStats = handleDiscountCodeRedeemStats;
async function updateCodeCounter(codeId, operation) {
    const date = datetime_utils_1.default.format(new Date(), 'YYYY-MM-DD');
    const id = codeId + date;
    const ref = admin
        .firestore()
        .collection(constants_1.DISCOUNT_CODES_REDEEMS_STATS)
        .doc(id);
    if ((await ref.get()).exists) {
        return ref.update({
            total: FieldValue.increment(operation === 'ADD' ? 1 : -1),
        });
    }
    return ref.set({
        id: id,
        codeId: codeId,
        timestamp: firebase_admin_1.firestore.Timestamp.now(),
        date: date,
        total: operation === 'ADD' ? 1 : 0,
    });
}
//# sourceMappingURL=index.js.map