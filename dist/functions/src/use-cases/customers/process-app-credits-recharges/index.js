"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processAppCreditsRecharges = void 0;
const admin = require('firebase-admin');
const handle_app_credits_transactions_1 = require("../handle-app-credits-transactions");
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const processAppCreditsRecharges = async (change, context) => {
    functions.logger.log('Credit recharge ', context.params.documentId);
    const document = change.after.data();
    const oldDocument = change.before.data();
    if (!document && oldDocument && oldDocument.logId) {
        return resetUserLastCredits(oldDocument.logId);
    }
    if (!document)
        return Promise.resolve();
    const reqData = document;
    if (document && !oldDocument && !reqData.approved) {
        await sendAdminEmail(process.env.ZIPPI_RECHARGES_EMAIL, reqData.userFullName, document);
    }
    if (!reqData.approved)
        return Promise.resolve();
    if (reqData.processed)
        return Promise.resolve();
    return approveAppCreditRecharge({
        ref: change.after.ref,
        amount: reqData.amount,
        userId: reqData.userId,
    });
};
exports.processAppCreditsRecharges = processAppCreditsRecharges;
async function approveAppCreditRecharge(params) {
    const rechargeLog = await (0, handle_app_credits_transactions_1.createRechargeCreditLog)({
        userId: params.userId,
        amount: params.amount,
        metadata: {
            rechargeId: params.ref.id,
        },
    });
    await admin
        .firestore()
        .collection(constants_1.CREDIT_LOG_REF)
        .doc(rechargeLog.id)
        .set(rechargeLog);
    await params.ref.update({
        processed: true,
        approvedAt: new Date(),
        logId: rechargeLog.id,
    });
    sendUserApproveNotification(params.userId, params.amount)
        .then(() => {
    })
        .then((e) => {
    });
}
async function resetUserLastCredits(logId) {
    return admin.firestore().doc(`${constants_1.CREDIT_LOG_REF}/${logId}`).delete();
}
async function sendAdminEmail(email, userFullName, rechargeData) {
    var _a;
    return app_1.default.sendEmail(email, {
        subject: `${process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'} - Zippi Credits Recarga, de $${rechargeData.amount}`,
        text: `Hola, ${userFullName} ha reportado el pago de $${rechargeData.amount} usando ${rechargeData.paymentReport.method}`,
        html: `
              <div>
                <p> El usuario
                ${userFullName} ha reportado el pago de $${rechargeData.amount} usando ${rechargeData.paymentReport.method} 
                </p>
                <br/>
                 <p> ID: <strong>${rechargeData.id}</strong> </p>
                <p> <strong> ${JSON.stringify((_a = rechargeData.paymentReport.metadata) !== null && _a !== void 0 ? _a : {})}</strong> </p>
              </div>
          `,
    });
}
async function sendUserApproveNotification(userId, credit) {
    const { email, configurations } = await app_1.default.findUserNotificationInfo(userId, 'customer');
    try {
        await app_1.default.sendPushNotification({
            notification: {
                title: `Tu recarga de Zippi Credits ha sido aprobada!`,
                body: `Se han cargado $${credit} a tu Zippi Wallet!`,
            },
        }, 'CUSTOMER', userId);
    }
    catch (e) {
    }
    try {
        if (!(configurations === null || configurations === void 0 ? void 0 : configurations.receiveEmails))
            return;
        await app_1.default.sendEmail(email, {
            subject: `Zippi - Tu recarga de Zippi Credits ha sido aprobada!`,
            text: `Se han cargado $${credit} a tu Zippi Wallet!`,
            html: `Se han cargado $${credit} a tu Zippi Wallet!`,
        });
    }
    catch (e) {
    }
}
//# sourceMappingURL=index.js.map