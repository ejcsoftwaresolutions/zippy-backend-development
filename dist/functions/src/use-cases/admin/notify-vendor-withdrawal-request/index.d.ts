declare const notifyVendorWithdrawalRequest: (snap: any, context: any) => Promise<void | FirebaseFirestore.DocumentReference<FirebaseFirestore.DocumentData>[]>;
export default notifyVendorWithdrawalRequest;
