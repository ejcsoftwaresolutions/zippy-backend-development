"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const app_1 = require("../../../utils/app");
const moment = require("moment");
const functions = require('firebase-functions');
const pdf = require('html-pdf');
const uuid = require('uuid');
const generateAdminPdfRequest = async (snap, context) => {
    functions.logger.log('Generating admin pdf request', context.params.documentId);
    const pdfRequestRef = await findPdfRequestRef(context.params.documentId);
    const query = await pdfRequestRef.get();
    const pdfRequest = query.data();
    if (!pdfRequest) {
        functions.logger.log('Not found');
        return Promise.resolve();
    }
    functions.logger.log('Generating for: ', pdfRequest.slug);
    const fileRef = admin.storage().bucket().file(pdfRequest.fileName, {});
    const content = pdfRequest.content;
    const adminEmail = pdfRequest.email;
    await pdfRequestRef.update({ status: 'GENERATING' });
    return new Promise((resolve) => {
        var _a;
        pdf
            .create(content, {
            ...((_a = pdfRequest.pdfConfig) !== null && _a !== void 0 ? _a : {}),
        })
            .toStream(async (err, stream) => {
            if (err) {
                await pdfRequestRef.update({
                    status: 'ERROR',
                });
                functions.logger.log('Error', err);
                return;
            }
            await stream.pipe(fileRef.createWriteStream({
                metadata: {
                    contentType: 'application/pdf', metadata: {
                        firebaseStorageDownloadTokens: uuid.v4(), lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                    },
                }, resumable: false, public: true,
            }));
            const pdfFileUrl = fileRef.publicUrl();
            await pdfRequestRef.update({
                status: 'CREATED', url: pdfFileUrl, updatedAt: new Date(),
            });
            await app_1.default.sendEmail(adminEmail, {
                subject: `${pdfRequest.title}`, text: `${pdfRequest.title}`, html: `
                <div>
                     <p>
                       Reporte generado, puedes descargar el archivo  <a href="${pdfFileUrl}">aquí</a> !.
                    </p>
                  <br/>
                </div>
            `,
            });
            resolve({});
        });
    });
};
async function findPdfRequestRef(id) {
    return admin.firestore().collection('admin_pdf').doc(id);
}
exports.default = generateAdminPdfRequest;
//# sourceMappingURL=index.js.map