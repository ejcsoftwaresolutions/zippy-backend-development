"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.salesInvoiceHtml = void 0;
exports.salesInvoiceHtml = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta
        content="IE=edge"
        http-equiv="X-UA-Compatible"
    />
    <meta
        content="width=device-width, initial-scale=1.0"
        name="viewport"
    />


    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet"
    />

    <title>Document</title>
    <style type="text/css">
        html {
            box-sizing: border-box;
        }

        *,
        *:before,
        *:after {
            box-sizing: inherit;
        }

        p,
        a,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-size: 0.5rem;
            font-family: 'Poppins', sans-serif;
            margin: 0;
            padding: 0;
        }

        body {
            margin: 0 auto;
        }

        .container {
            background-color: #ffffff;
            margin: 0 auto;
            max-height: 800px;
            position: relative;
            width: 300px;
        }

        .head {
            background-color: #4979d0;
            background-position: center center;
            background-size: cover;
            padding: 1rem 3rem 5rem 3rem;
            color: white !important;
        }

        .containerContent {
            left: 0px;
            padding: 0 1.4rem;
            position: absolute;
            right: 0px;
            top: 120px;
        }

        .sectionImage {
            text-align: center;
        }

        .imageLogo {
            width: 4rem;
        }

        .head p {
            color: white;
        }

        .date {
            font-size: 0.5rem;
            margin: 0.5rem 0;
            text-align: center;
        }

        .sectionContent {
            background-color: #f8f8fa;
            border-radius: 0.5rem;
            border: 0.3px solid #eeeeee;
            margin: 0.5rem 0;
            padding: 0.5rem 1rem;
        }

        .package {
            width: 1rem;
        }

        .title {
            font-weight: 500;
            font-size: 0.6rem;
            margin: 0.5rem 0;
        }

        .field {
            -ms-flex-align: center;
            -ms-flex-direction: row;
            -ms-flex-pack: justify;
            -webkit-box-align: center;
            -webkit-box-direction: normal;
            -webkit-box-orient: horizontal;
            -webkit-box-pack: justify;
            align-items: center;
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin: 0.5rem 0;
        }

        .field :first-child {
            -webkit-box-flex: 10%;
            -ms-flex: 10%;
            flex: 10%;
        }

        .field :nth-last-child(2) {
            -webkit-box-flex: 80%;
            -ms-flex: 80%;
            flex: 80%;
        }

        .field :nth-last-child(3) {
            -webkit-box-flex: 10%;
            -ms-flex: 10%;
            flex: 10%;
        }

        .divider {
            border: 1px dashed #dfdfdf;
            margin: 1rem 0;
        }

        .total {
            -ms-flex-align: center;
            -ms-flex-direction: row;
            -ms-flex-pack: justify;
            -webkit-box-align: center;
            -webkit-box-direction: normal;
            -webkit-box-orient: horizontal;
            -webkit-box-pack: justify;
            align-items: center;
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin: 0.8rem 0;
        }

        .icon {
            width: 0.8rem;
        }

        .sectionPay {
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .imgCard {
            width: 18px;
        }

        .totalPay {
            text-align: end;
            width: 100%;
        }

        .totalPay h4 {
            margin: 0.5rem 0;
        }

        .product {
            margin: 1.2rem 0 0 0;
        }

        .name {
            font-size: .6rem
        }

    </style>
</head>
<body>
<div class="container">
    <div class="head">
        <div class="sectionImage">
            <img
                src="https://firebasestorage.googleapis.com/v0/b/zippi-market-dev-a6385.appspot.com/o/static%2Flogo-pdf.png?alt=media&token=91cced3d-f804-470d-8dce-25c44ff22ed5"
                style="width:60px"
            />
        </div>
        <h3>Zippi Ventas</h3>
        <h3>Periodo {{PERIOD}}</h3>
    </div>
    <div class="containerContent">
        <div class="sectionContent">

            <div>

                {{PRODUCTS_HTML}}

                <div class="divider"></div>
                <div class="total">
                    <p>Total</p>
                    <p>{{TOTAL}} USD</p>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>
`;
//# sourceMappingURL=sales-invoice.js.map