"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const moment = require("moment-timezone");
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const trackVendorsDemand = async (change, context) => {
    functions.logger.log('Tracking vendors demand', context.params.documentId);
    const oldStatus = change.before.get('status');
    const newStatus = change.after.get('status');
    if (oldStatus !== 'Order Placed')
        return Promise.resolve();
    if (newStatus !== 'Order Accepted')
        return Promise.resolve();
    const order = change.after.data();
    const today = moment().utc().format('YYYY-MM-DD');
    const vendor = await app_1.default.findVendor(order.vendor.id);
    if (!vendor)
        return;
    try {
        await app_1.default.syncVendorGeolocation(order.vendorID, [vendor.location.latitude, vendor.location.longitude,]);
    }
    catch (error) {
        console.log(error);
    }
    const vendorDailyDemandQ = await admin
        .firestore()
        .collection(constants_1.VENDORS_DEMAND)
        .where('vendorID', '==', order.vendorID)
        .where('date', '==', today)
        .get();
    const vendorDailyDemand = vendorDailyDemandQ.docs.map((d) => d);
    if (vendorDailyDemand.length > 0) {
        const demandDoc = vendorDailyDemand[0];
        const demandDailyDocRef = await admin
            .firestore()
            .collection(constants_1.VENDORS_DEMAND)
            .doc(demandDoc.id);
        return demandDailyDocRef.update({
            salesQuantity: admin.firestore.FieldValue.increment(1),
        });
    }
    const info = {
        vendorID: order.vendorID, date: today, salesQuantity: 1,
    };
    return admin.firestore().collection(constants_1.VENDORS_DEMAND).add(info);
};
exports.default = trackVendorsDemand;
//# sourceMappingURL=index.js.map