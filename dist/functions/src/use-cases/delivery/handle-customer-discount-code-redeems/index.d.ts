import { Change, EventContext } from 'firebase-functions';
import { firestore } from 'firebase-admin';
import DocumentSnapshot = firestore.DocumentSnapshot;
export declare const handleCustomerDiscountCodeRedeems: (change: Change<DocumentSnapshot>, context: EventContext) => Promise<any>;
