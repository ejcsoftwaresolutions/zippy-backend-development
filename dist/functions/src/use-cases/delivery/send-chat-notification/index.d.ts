declare const sendChatNotification: (snap: any, context: any) => Promise<unknown[]>;
export default sendChatNotification;
