"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sendCustomerOrderUpdatesNotifications = void 0;
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const sendCustomerOrderUpdatesNotifications = async (change, context) => {
    var _a, _b, _c, _d;
    const document = change.after.data();
    const oldDocument = change.before.data();
    if (!document)
        return Promise.resolve();
    if (document.status === (oldDocument === null || oldDocument === void 0 ? void 0 : oldDocument.status))
        return Promise.resolve();
    const STATUS_TO_LISTEN_TO = getNotificationsTemplates();
    if (![...Object.keys(STATUS_TO_LISTEN_TO), 'Driver Not Found'].includes(document.status))
        return Promise.resolve();
    functions.logger.log('New order status update', context.params.documentId);
    const { email, configurations } = await app_1.default.findUserNotificationInfo(document.customer.id, 'customer');
    if (document.status === 'Order Accepted' &&
        (oldDocument === null || oldDocument === void 0 ? void 0 : oldDocument.status) === 'Driver Not Found') {
        return Promise.resolve();
    }
    const notificationData = (_b = (_a = STATUS_TO_LISTEN_TO === null || STATUS_TO_LISTEN_TO === void 0 ? void 0 : STATUS_TO_LISTEN_TO[document.status]) === null || _a === void 0 ? void 0 : _a.getPushNotificationData) === null || _b === void 0 ? void 0 : _b.call(_a, { order: document });
    const emailData = (_d = (_c = STATUS_TO_LISTEN_TO === null || STATUS_TO_LISTEN_TO === void 0 ? void 0 : STATUS_TO_LISTEN_TO[document.status]) === null || _c === void 0 ? void 0 : _c.getEmailData) === null || _d === void 0 ? void 0 : _d.call(_c, {
        order: document,
    });
    if (notificationData) {
        try {
            await app_1.default.sendPushNotification({
                ...notificationData,
            }, 'CUSTOMER', document.customer.id);
        }
        catch (e) {
        }
    }
    if (emailData && email && (configurations === null || configurations === void 0 ? void 0 : configurations.receiveEmails)) {
        try {
            await app_1.default.sendEmail(email, emailData);
        }
        catch (e) {
        }
    }
    if (document.status === 'Order Rejected') {
        try {
            await sendAdminOrderRejectedEmail(document);
        }
        catch (e) {
        }
    }
    if (document.status === 'Driver Not Found') {
        try {
            await sendAdminOrderRiderNotFoundEmail(document);
        }
        catch (e) {
        }
    }
    if (document.status === 'Order Placed') {
        try {
            await sendAdminNewOrderEmail(document);
        }
        catch (e) {
        }
    }
};
exports.sendCustomerOrderUpdatesNotifications = sendCustomerOrderUpdatesNotifications;
function getNotificationsTemplates() {
    return {
        'Order Placed': {
            getPushNotificationData: (data) => {
                if (!data.order.paymentReport) {
                    return undefined;
                }
                if (!data.order.paymentReport.validated) {
                    return undefined;
                }
                return {
                    notification: {
                        title: `Ya hemos verificado tu pago`,
                        body: `El establecimiento pronto procesará tu pedido`,
                    },
                };
            },
        },
        'In Transit': {
            getPushNotificationData: (data) => {
                return {
                    notification: {
                        title: `Tu Zipper ha partido a tu dirección`,
                        body: `Tu pedido está casi en tus manos, tu Zipper va en camino!`,
                    },
                };
            },
        },
        'Order Completed': {
            getPushNotificationData: (data) => {
                var _a;
                return {
                    notification: {
                        title: `Orden completada`,
                        body: ((_a = data.order) === null || _a === void 0 ? void 0 : _a.deliveryMethod) !== 'PICK_UP'
                            ? `Ayudanos calificando tu compra. Gracias por usar Zippi Market!`
                            : `Gracias por usar Zippi Market!`,
                    },
                };
            },
        },
        'Driver Accepted': {
            getPushNotificationData: (data) => {
                var _a;
                return {
                    notification: {
                        title: `Tu pedido ya tiene un Zipper!`,
                        body: `El Zipper ${data.order.driver.firstName} ${(_a = data.order.driver.lastName) !== null && _a !== void 0 ? _a : ''} ha aceptado recoger y llevarte tu pedido!`,
                    },
                };
            },
        },
        'Arrived to Store': {
            getPushNotificationData: (data) => {
                return {
                    notification: {
                        title: `Tu Zipper ya ha llegado al establecimiento`,
                        body: `Pronto irá a entregar tu pedido`,
                    },
                };
            },
        },
        'Order Accepted': {
            getPushNotificationData: (data) => {
                var _a;
                return {
                    notification: {
                        title: `El establecimiento ha confirmado tu pedido`,
                        body: ((_a = data.order) === null || _a === void 0 ? void 0 : _a.deliveryMethod) !== 'PICK_UP'
                            ? `Se esta procesando tu compra`
                            : 'Se esta procesando tu compra, te avisaremos cuando ya este listo para recoger',
                    },
                };
            },
        },
        'Ready For Pickup': {
            getPushNotificationData: (data) => {
                var _a, _b, _c;
                return {
                    notification: {
                        title: `Tu pedido esta listo para recoger`,
                        body: `Ya puedes ir a recoger tu pedido en las instalaciones de ${(_c = (_b = (_a = data.order) === null || _a === void 0 ? void 0 : _a.vendor) === null || _b === void 0 ? void 0 : _b.name) !== null && _c !== void 0 ? _c : 'la tienda'}`,
                    },
                };
            },
        },
        'Order Rejected': {
            getPushNotificationData: (data) => {
                return {
                    notification: {
                        title: `El establecimiento ha cancelado tu pedido`,
                        body: `Nuestro equipo está revisando los detalles, pronto te contactaremos`,
                    },
                };
            },
            getEmailData: (data) => {
                return {
                    subject: `Zippi - El establecimiento ha cancelado tu pedido #${data.order.code}`,
                    text: `Zippi - El establecimiento ha cancelado tu pedido #${data.order.code}`,
                    html: `
          <div>
            <p>
                  Hola ${data.order.customer.firstName}, el establecimiento ${data.order.vendor.name} ha cancelado tu pedido #${data.order.code}. 
            </p>
            <br/>
            <p>
              No te preocupes, nuestro equipo está revisando los detalles y pronto te contactaremos.
            </p>
            <p> <strong>Gracias por tu confianza.</strong> </p>
          </div>
          `,
                };
            },
        },
    };
}
async function sendAdminOrderRejectedEmail(document) {
    var _a, _b, _c;
    const admin = await app_1.default.findAdminAccount();
    if (!admin || !((_a = admin.config) === null || _a === void 0 ? void 0 : _a.notificationsEmail))
        return;
    const allEmails = (((_b = admin.config) === null || _b === void 0 ? void 0 : _b.notificationsEmail) +
        ',' +
        process.env.ZIPPI_PAYMENTS_EMAIL);
    await app_1.default.sendEmail(allEmails, {
        subject: `${process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'} - Se ha cancelado la orden ${document.id}`,
        text: `Se ha cancelado la orden ${document.id}`,
        html: `
         <div>
            <p>
                  Establecimiento: ${document.vendor.name}  . ${(_c = document.vendor.phone) !== null && _c !== void 0 ? _c : ''} 
            </p>
             <p>
                  Cliente: ${document.customer.firstName} ${document.customer.lastName} - ${document.customer.id}.  ${document.customer.email} ${document.customer.phone}
            </p>
           <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                  Total: $${document.total}
             </p>
          </div>
        `,
    });
}
async function sendAdminOrderRiderNotFoundEmail(document) {
    var _a, _b, _c;
    const admin = await app_1.default.findAdminAccount();
    if (!admin || !((_a = admin.config) === null || _a === void 0 ? void 0 : _a.notificationsEmail))
        return;
    await app_1.default.sendEmail((_b = admin.config) === null || _b === void 0 ? void 0 : _b.notificationsEmail, {
        subject: `${process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'} - El establecimiento ${document.vendor.name} no ha podido encontrar Zipper para la orden #${document.code}`,
        text: `No se han encontrado Zippers cercanos a la tienda ${document.vendor.name} orden ${document.id}`,
        html: `
         <div>
            <p>
                  Establecimiento: ${document.vendor.name}  . ${(_c = document.vendor.phone) !== null && _c !== void 0 ? _c : ''} 
            </p>
             <p>
                  Cliente: ${document.customer.firstName} ${document.customer.lastName} - ${document.customer.id}.  ${document.customer.email} ${document.customer.phone}
            </p>
           <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                  Total: $${document.total}
             </p>
          </div>
        `,
    });
}
async function sendAdminNewOrderEmail(document) {
    var _a, _b, _c;
    const admin = await app_1.default.findAdminAccount();
    if (!admin || !((_a = admin.config) === null || _a === void 0 ? void 0 : _a.notificationsEmail))
        return;
    await app_1.default.sendEmail((_b = admin.config) === null || _b === void 0 ? void 0 : _b.notificationsEmail, {
        subject: `${process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'} - Nueva orden #${document.code} 😎🐆`,
        text: `Nueva orden #${document.code} 😎🐆`,
        html: `
         <div>
            <p>
                  Establecimiento: ${document.vendor.name}  . ${(_c = document.vendor.phone) !== null && _c !== void 0 ? _c : ''} 
            </p>
             <p>
                  Cliente: ${document.customer.firstName} ${document.customer.lastName} - ${document.customer.id}.  ${document.customer.email} ${document.customer.phone}
            </p>
             <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                  Total: $${document.total}
             </p>
          </div>
        `,
    });
}
//# sourceMappingURL=index.js.map