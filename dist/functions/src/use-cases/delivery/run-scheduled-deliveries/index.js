"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const moment = require("moment-timezone");
const runScheduledDeliveries = async (context) => {
    const unprocessed = await getData();
    const promises = unprocessed.map((c) => {
        return new Promise(async (resolve, reject) => {
            try {
                await app_1.default.sendPushNotification({
                    notification: {
                        title: `Recordatorio de pedido agendado #${c.data.code} para las ${c.data.scheduledTimeSlot.from} - ${c.data.scheduledTimeSlot.to}`,
                        body: `Ya puedes ir buscando el Zipper de esta orden`,
                    },
                    channel: 'order-update',
                }, 'VENDOR', c.data.vendor.id);
            }
            catch (e) {
                functions.logger.log(e === null || e === void 0 ? void 0 : e.message);
            }
            await c.ref.update({
                scheduleProcessed: true,
            });
            return resolve(true);
        });
    });
    return Promise.all(promises);
};
async function getData() {
    const currentDate = moment().tz('America/Caracas').format('MM-DD-YYYY');
    const startHour = moment().tz('America/Caracas').format('hh:mm A');
    const todaysUnprocessedVendorOrdersQ = await admin
        .firestore()
        .collection(constants_1.VENDOR_ORDERS)
        .where('deliveryMethod', '==', 'SCHEDULED_DELIVERY')
        .where('scheduleProcessed', '==', false)
        .where('scheduledDate', '==', currentDate)
        .where('scheduledTimeSlot.from', '==', startHour)
        .where('status', '==', 'Order Accepted')
        .get();
    const unprocessed = todaysUnprocessedVendorOrdersQ.docs.map((d) => ({
        data: d.data(),
        ref: d.ref,
    }));
    return unprocessed;
}
exports.default = runScheduledDeliveries;
//# sourceMappingURL=index.js.map