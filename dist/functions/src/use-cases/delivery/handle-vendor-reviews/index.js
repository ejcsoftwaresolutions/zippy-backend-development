"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const handleVendorReviews = async (snap, context) => {
    functions.logger.log('New vendor review', context.params.documentId);
    const review = snap.data();
    const entityRef = await admin
        .firestore()
        .collection(constants_1.VENDORS)
        .doc(review.entityID);
    const entity = await app_1.default.findVendor(review.entityID);
    if (!entity)
        return Promise.resolve();
    if (!entity.reviewsCount) {
        await entityRef.update({
            reviewsCount: 1, reviewsSum: review.rating,
        });
        return;
    }
    await entityRef.update({
        reviewsCount: admin.firestore.FieldValue.increment(1),
        reviewsSum: admin.firestore.FieldValue.increment(review.rating),
    });
};
exports.default = handleVendorReviews;
//# sourceMappingURL=index.js.map