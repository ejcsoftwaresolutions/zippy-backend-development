"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceHtml = void 0;
exports.InvoiceHtml = `
    <!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'/>
    <meta
        http-equiv='X-UA-Compatible'
        content='IE=edge'
    />
    <meta
        name='viewport'
        content='width=device-width, initial-scale=1.0'
    />
    <link
        rel='preconnect'
        href='https://fonts.googleapis.com'
    />
    <link
        rel='preconnect'
        href='https://fonts.gstatic.com'
        crossorigin
    />
    <link
        rel='stylesheet'
        href='https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.css'
        integrity='sha512-oHDEc8Xed4hiW6CxD7qjbnI+B07vDdX7hEPTvn9pSZO1bcRqHp8mj9pyr+8RVC2GmtEfI2Bi9Ke9Ass0as+zpg=='
        crossorigin='anonymous'
        referrerpolicy='no-referrer'
    />
    <link
        href='https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap'
        rel='stylesheet'
    />
    <script
        src='https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js'
        integrity='sha512-RNLkV3d+aLtfcpEyFG8jRbnWHxUqVZozacROI4J2F1sTaDqo1dPQYs01OMi1t1w9Y2FdbSCDSQ2ZVdAC8bzgAg=='
        crossorigin='anonymous'
        referrerpolicy='no-referrer'
    ></script>
    <title>Document</title>
    <style type='text/css'>
        html {
            box-sizing: border-box;
        }

        *,
        *:before,
        *:after {
            box-sizing: inherit;
        }

        p,
        a,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-size: 0.5rem;
            font-family: 'Poppins', sans-serif;
            margin: 0;
            padding: 0;
        }

        body {
            margin: 0 auto;
        }

        .container {
            background-color: #ffffff;
            margin: 0 auto;
            max-height: 800px;
            position: relative;
            width: 300px;
        }

        .head {
            background-color: #4979d0;
            background-position: center center;
            background-size: cover;
            padding: 1rem 3rem 5rem 3rem;
        }

        .containerContent {
            left: 0px;
            padding: 0 1.4rem;
            position: absolute;
            right: 0px;
            top: 120px;
        }

        .sectionImage {
            text-align: center;
        }

        .imageLogo {
            width: 4rem;
        }

        .head p {
            color: white;
        }

        .date {
            font-size: 0.5rem;
            margin: 0.5rem 0;
            text-align: center;
        }

        .sectionContent {
            background-color: #f8f8fa;
            border-radius: 0.5rem;
            border: 0.3px solid #eeeeee;
            margin: 0.5rem 0;
            padding: 0.5rem 1rem;
        }

        .package {
            width: 1rem;
        }

        .title {
            font-weight: 500;
            font-size: 0.6rem;
            margin: 0.5rem 0;
        }

        .field {
            -ms-flex-align: center;
            -ms-flex-direction: row;
            -ms-flex-pack: justify;
            -webkit-box-align: center;
            -webkit-box-direction: normal;
            -webkit-box-orient: horizontal;
            -webkit-box-pack: justify;
            align-items: center;
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin: 0.5rem 0;
        }


        .field :first-child {
            -webkit-box-flex: 10%;
            -ms-flex: 10%;
            flex: 10%;
        }

        .field :nth-last-child(2) {
            -webkit-box-flex: 80%;
            -ms-flex: 80%;
            flex: 80%;
        }

        .field :nth-last-child(3) {
            -webkit-box-flex: 10%;
            -ms-flex: 10%;
            flex: 10%;
        }

        .divider {
            border: 1px dashed #dfdfdf;
            margin: 1rem 0;
        }

        .total {
            -ms-flex-align: center;
            -ms-flex-direction: row;
            -ms-flex-pack: justify;
            -webkit-box-align: center;
            -webkit-box-direction: normal;
            -webkit-box-orient: horizontal;
            -webkit-box-pack: justify;
            align-items: center;
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin: 0.8rem 0;
        }

        .icon {
            width: 0.8rem;
        }

        .sectionPay {
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }

        .imgCard {
            width: 18px;
        }

        .totalPay {
            text-align: end;
            width: 100%;
        }

        .totalPay h4 {
            margin: 0.5rem 0;
        }

        .product {
            margin: 1.2rem 0 0 0;
            margin-bottom: 2em;
        }

        .name {
            font-size: .6rem
        }

        .product {
            font-size:  0.5rem;
        }
        .addon {
            -ms-flex-align: center;
            -ms-flex-direction: row;
            -ms-flex-pack: justify;
            -webkit-box-align: center;
            -webkit-box-direction: normal;
            -webkit-box-orient: horizontal;
            -webkit-box-pack: justify;
            align-items: center;
            display: -ms-flexbox;
            display: -webkit-box;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin: 0.3rem 0;
            font-size:  0.5rem;
            margin-left: 1.5em;
        }
        
         .addon .price {
            text-align: right
         }
        
    </style>
</head>
<body>
<div class='container'>
    <div class='head'>
        <div class='sectionImage'>
            <image
                src='https://firebasestorage.googleapis.com/v0/b/zippi-market-dev-a6385.appspot.com/o/static%2Flogo-pdf.png?alt=media&token=91cced3d-f804-470d-8dce-25c44ff22ed5'
                class='imageLogo'
            />
        </div>
        <p class='date'>{{DATE}}</p>
        <p
            class='name'
            style='text-align: center'
        >Gracias por tu pedido, {{CUSTOMER_NAME}}</p>
    </div>
    <div class='containerContent'>
        <div class='sectionContent'>
            <image
                alt='paquete'
                class='package'
                src='https://firebasestorage.googleapis.com/v0/b/zippi-1e248.appspot.com/o/imagesZippi%2Fpaquete.svg?alt=media&token=71fdef1e-3ea7-4270-9e94-52426f7dac1b'
            />
            <h4 class='title'><strong>Pedido #{{ORDER_ID}}</strong></h4>
            <h5>Fecha: {{INVOICE_DATE}}</h5>
            
            <div>
                <div class='product'>
                    {{PRODUCTS_HTML}}
                </div>
                <div class='divider'></div>
                <div class='total'>
                    <p>Delivery</p>
                    <p>$[[DELIVERY_FEES]]</p>
                </div>
                <div class='total'>
                    <p>Total</p>
                    <p>$[[TOTAL]]</p>
                </div>
            </div>
        </div>
   

        {{VENDOR_HTML}}
        
        {{PAYMENT_METHOD_HTML}}
        
        {{SHIPPING_ADDRESS_HTML}}
        
    </div>
</div>
<!-- Include Handlebars from a CDN -->
<script src='https://cdn.jsdelivr.net/npm/handlebars@latest/dist/handlebars.js'></script>
<script>
    // compile the template
    var template = Handlebars.compile("Handlebars <b>{{doesWhat}}</b>");
    // execute the compiled template and print the output to the console
    console.log(template({doesWhat: "rocks!"}));
</script>
</body>
</html>
`;
//# sourceMappingURL=order-invoice.js.map