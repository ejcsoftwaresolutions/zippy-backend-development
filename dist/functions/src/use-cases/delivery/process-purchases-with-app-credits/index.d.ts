import { firestore } from 'firebase-admin';
import { Change, EventContext } from 'firebase-functions';
import DocumentSnapshot = firestore.DocumentSnapshot;
export declare const processPurchasesWithAppCredits: (change: Change<DocumentSnapshot>, context: EventContext) => Promise<void>;
