"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const sendNewVendorOrderPushNotification = async (snap, context) => {
    functions.logger.log('Sending vendor new order notification', context.params.documentId);
    const order = snap.data();
    await app_1.default.sendPushNotification({
        data: {
            orderId: context.params.documentId,
            vendorId: order.vendor.id,
        },
        notification: {
            title: `Nueva orden #${order.code}`,
            body: `Tienes una nueva orden en tu tienda`,
        },
        channel: 'new-order',
    }, 'VENDOR', order.vendor.id);
};
exports.default = sendNewVendorOrderPushNotification;
//# sourceMappingURL=index.js.map