"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.runDeliveryServiceChecker = void 0;
const firebase_admin_1 = require("firebase-admin");
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const moment = require("moment-timezone");
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const runDeliveryServiceChecker = async (context) => {
    const onlineRidersRef = await firebase_admin_1.default.database().ref(`${constants_1.RIDERS_LOCATIONS}`);
    const ridersLastUpdatesRef = await firebase_admin_1.default
        .database()
        .ref(`${constants_1.RIDERS_LOCATIONS_TIMESTAMP}`);
    const availableRidersRef = await firebase_admin_1.default
        .database()
        .ref(`${constants_1.AVAILABLE_RIDERS}`);
    const busyRidersRef = await firebase_admin_1.default.database().ref(`${constants_1.BUSY_RIDERS}`);
    const connectedRidersRef = await firebase_admin_1.default
        .database()
        .ref(`${constants_1.CONNECTED_RIDERS}`);
    const riderAlertsRef = await firebase_admin_1.default
        .database()
        .ref(`${constants_1.RIDER_DELIVERY_ALERTS}`);
    const deliveryStatusRef = await firebase_admin_1.default
        .database()
        .ref(`${constants_1.DELIVERY_SERVICE_STATUS}`);
    const riderActiveOrderRef = await firebase_admin_1.default
        .database()
        .ref(`${constants_1.RIDERS_ACTIVE_ORDERS}`);
    const deliveryRequestsRef = await firebase_admin_1.default
        .database()
        .ref(`${constants_1.DELIVERY_REQUESTS}`);
    const REFS = {
        ridersLastUpdatesRef,
        onlineRidersRef,
        availableRidersRef,
        busyRidersRef,
        connectedRidersRef,
        riderAlertsRef,
        deliveryStatusRef,
        riderActiveOrderRef,
        deliveryRequestsRef,
    };
    checkRiderAlerts(REFS)
        .then(() => {
    })
        .catch((e) => {
        functions.logger.log('Error', e.message);
    });
    checkRiderConnections(REFS)
        .then(() => {
    })
        .catch((e) => {
        functions.logger.log('Error', e.message);
    });
    checkRidersLastLocationUpdate(REFS)
        .then(() => {
    })
        .catch((e) => {
        functions.logger.log('Error', e.message);
    });
};
exports.runDeliveryServiceChecker = runDeliveryServiceChecker;
async function checkRiderAlerts(refs) {
    const alerts = (await refs.riderAlertsRef.get()).val();
    const cleanAlerts = Object.values(alerts !== null && alerts !== void 0 ? alerts : {}).map((d) => {
        return new Promise(async (resolve) => {
            var _a, _b;
            const cleanAlert = async () => {
                await refs.riderAlertsRef.child(d.driverId).set(null);
                resolve({});
            };
            const request = (await refs.deliveryRequestsRef.child(d.id).get()).val();
            if (!request) {
                await cleanAlert();
            }
            if (((_b = (_a = request.currentAttempt) === null || _a === void 0 ? void 0 : _a.currentAlertedCandidate) === null || _b === void 0 ? void 0 : _b.id) !== d.driverId) {
                await cleanAlert();
            }
            resolve({});
        });
    });
    return Promise.all(cleanAlerts);
}
async function checkRiderConnections(refs) {
    const connections = (await refs.connectedRidersRef.get()).val();
    const riderIds = Object.keys(connections !== null && connections !== void 0 ? connections : {});
    const lastConnections = Object.values(connections !== null && connections !== void 0 ? connections : {});
    const MAX_DAYS_CONNECTED = 1;
    const cleanConnections = Object.values(riderIds).map((rId, key) => {
        return new Promise(async (resolve) => {
            const disconnectRider = async () => {
                await admin.firestore().doc(`${constants_1.RIDERS}/${rId}`).update({
                    isOnline: false,
                });
                resolve({});
            };
            const lastUpdateInDays = moment(new Date(new Date().toUTCString())).diff(new Date(lastConnections[key]), 'days');
            if (lastUpdateInDays < MAX_DAYS_CONNECTED)
                return resolve({});
            return disconnectRider();
        });
    });
    return Promise.all(cleanConnections);
}
async function checkRidersLastLocationUpdate(refs) {
    const connections = (await refs.connectedRidersRef.get()).val();
    const lastUpdates = (await refs.ridersLastUpdatesRef.get()).val();
    const riderIds = Object.keys(connections !== null && connections !== void 0 ? connections : {});
    const MAX_MINUTES_OFF = 25;
    const notificationsPromises = riderIds.map((rId, key) => {
        return new Promise(async (resolve) => {
            const driverLastUpdate = lastUpdates[rId];
            if (!driverLastUpdate)
                return resolve({});
            const lastUpdateInMinutes = moment(new Date(new Date().toUTCString())).diff(new Date(driverLastUpdate), 'minutes');
            if (lastUpdateInMinutes <= MAX_MINUTES_OFF)
                return resolve({});
            await app_1.default.sendPushNotification({
                notification: {
                    title: `Te hemos perdido 😔!`,
                    body: `No hemos recibido tu ubicación por más de ${lastUpdateInMinutes} minutos. Verifica si tienes activado tu GPS, esto es importante para que recibas deliveries cernanos y que el cliente pueda hacer track de su pedido y que su experiencia sea la mejor posible`,
                },
            }, 'RIDER', rId);
            resolve({});
        });
    });
    return Promise.all(notificationsPromises);
}
//# sourceMappingURL=index.js.map