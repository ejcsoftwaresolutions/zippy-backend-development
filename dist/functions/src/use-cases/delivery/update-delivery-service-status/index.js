"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateDeliveryServiceStatus = void 0;
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const admin = require('firebase-admin');
const updateDeliveryServiceStatus = async (change, context) => {
    var _a, _b, _c, _d, _e, _f, _g;
    const delivery = (_a = (await admin
        .firestore()
        .collection(constants_1.DELIVERY_ORDERS)
        .doc(context.params.documentId)
        .get())) === null || _a === void 0 ? void 0 : _a.data();
    const after = change.after.data();
    const last = change.before.data();
    if (!delivery && last) {
        return admin
            .database()
            .ref(`${constants_1.DELIVERY_SERVICE_STATUS}/${last.id}`)
            .set(null);
    }
    if (!delivery)
        return Promise.resolve();
    if (last && delivery.status === last.status)
        return Promise.resolve();
    const deliveryId = delivery.id;
    const status = delivery.status;
    try {
        await sendVendorNotification(delivery, after === null || after === void 0 ? void 0 : after.status);
    }
    catch (e) {
        functions.logger.log('Vendor notification NOT SENT!', e.message);
    }
    await admin
        .database()
        .ref(`${constants_1.RIDERS_ACTIVE_ORDERS}/${delivery.driverInfo.id}`)
        .update({
        [deliveryId]: status !== 'DELIVERED'
            ? {
                id: delivery.id,
                code: (_c = (_b = delivery === null || delivery === void 0 ? void 0 : delivery.orderDetails) === null || _b === void 0 ? void 0 : _b.code) !== null && _c !== void 0 ? _c : '',
                status: status,
                vendorName: (_f = (_e = (_d = delivery === null || delivery === void 0 ? void 0 : delivery.orderDetails) === null || _d === void 0 ? void 0 : _d.store) === null || _e === void 0 ? void 0 : _e.name) !== null && _f !== void 0 ? _f : '',
            }
            : null,
    });
    await admin
        .database()
        .ref(`${constants_1.DELIVERY_SERVICE_STATUS}/${deliveryId}`)
        .set(status !== 'DELIVERED'
        ? {
            status: status,
            orderId: deliveryId,
            customer: delivery.customerInfo,
            rider: delivery.driverInfo,
            vendor: (_g = delivery.orderDetails) === null || _g === void 0 ? void 0 : _g.store,
            lastUpdate: new Date().toUTCString(),
        }
        : null);
    if (!last && status == 'ACCEPTED' && delivery.assigned) {
        await app_1.default.sendPushNotification({
            notification: {
                title: 'Pedido asignado',
                body: 'Te han asignado un nuevo pedido',
            },
            channel: 'new-order',
        }, 'RIDER', delivery.driverInfo.id);
    }
    if (status == 'DELIVERED') {
        await handleDeliveryCompleted(delivery);
    }
};
exports.updateDeliveryServiceStatus = updateDeliveryServiceStatus;
async function handleDeliveryCompleted(delivery) {
    try {
        await finishRiderLog(delivery);
    }
    catch (e) {
        functions.logger.log('Error liberar al rider', e.message);
    }
    const riderActiveDeliveries = await getRiderActiveDeliveries(delivery.driverInfo.id);
    try {
        if (!riderActiveDeliveries || (riderActiveDeliveries === null || riderActiveDeliveries === void 0 ? void 0 : riderActiveDeliveries.length) == 0) {
            await freeBusyRider(delivery.driverInfo.id);
        }
    }
    catch (e) { }
    try {
        return await deleteOrderChat(delivery);
    }
    catch (e) {
        functions.logger.log('Error al eliminar chat', e.message);
    }
}
async function sendVendorNotification(delivery, status) {
    if (!status) {
        throw new Error('STATUS NOT SET');
    }
    const vendorId = delivery.storeId;
    functions.logger.log('Sending vendor new order notification');
    const statusMessageMap = {
        ARRIVED_TO_STORE: {
            title: `El Zipper ${delivery.driverInfo.firstName} ha llegado a la tienda`,
            content: `Entrega el pedido #${delivery.orderDetails.code} al Zipper`,
        },
        ARRIVED_TO_CUSTOMER: {
            title: `El Zipper ${delivery.driverInfo.firstName} ha entregado la orden`,
            content: `El Zipper ha entregado la orden #${delivery.orderDetails.code} al cliente`,
        },
    };
    const notification = statusMessageMap[status];
    if (!notification)
        return;
    await app_1.default.sendPushNotification({
        notification: {
            title: notification.title,
            body: notification.content,
        },
        channel: 'order-update',
    }, 'VENDOR', vendorId);
}
async function deleteOrderChat(order) {
    const fb = admin.firestore();
    const viewerID = order.driverId;
    const userID = order.customerId;
    const channelId = viewerID < userID ? viewerID + userID : userID + viewerID;
    const bulkWriter = fb.bulkWriter();
    const MAX_RETRY_ATTEMPTS = 3;
    bulkWriter.onWriteError((error) => {
        if (error.failedAttempts < MAX_RETRY_ATTEMPTS) {
            return true;
        }
        else {
            console.log('Failed write at document: ', error.documentRef.path);
            return false;
        }
    });
    await fb.recursiveDelete(fb.collection('channels').doc(channelId), bulkWriter);
    await fb.recursiveDelete(fb
        .collection('social_feeds')
        .doc(order.customerId)
        .collection('chat_feed')
        .doc(channelId), bulkWriter);
    await fb.recursiveDelete(fb
        .collection('social_feeds')
        .doc(order.driverId)
        .collection('chat_feed')
        .doc(channelId), bulkWriter);
    const deliveryOrderRef = admin
        .firestore()
        .collection(constants_1.DELIVERY_ORDERS)
        .doc(order.id);
    await deliveryOrderRef.update({
        unseenMessages: admin.firestore.FieldValue.delete(),
    });
    await admin.firestore().collection('customers').doc(order.customerId).update({
        badgeCount: 0,
    });
    await admin.firestore().collection('riders').doc(order.driverId).update({
        badgeCount: 0,
    });
}
async function freeBusyRider(driverId) {
    const BLOCKED_DRIVERS_REF = admin.database().ref(`${constants_1.BUSY_RIDERS}`);
    return BLOCKED_DRIVERS_REF.child(driverId).transaction(() => {
        return null;
    });
}
async function finishRiderLog(deliveryOrder) {
    const feeCommission = await admin
        .firestore()
        .collection('app_configurations')
        .doc('RIDER_EARNING_FEES')
        .get();
    const total = deliveryOrder.finalFee.amount;
    const percentage = feeCommission
        ? feeCommission.data().commisionPercentage
        : 0;
    const totalCommission = parseFloat(((total * percentage) / 100).toFixed(2));
    const earning = deliveryOrder.finalFee.amount - totalCommission;
    const log = {
        id: admin.firestore().collection('test').doc().id,
        driverId: deliveryOrder.driverInfo.id,
        vendorId: deliveryOrder.storeId,
        vendorOrderId: deliveryOrder.id,
        date: new Date(),
        event: 'ORDER_COMPLETED',
        fee: total,
        earningBreakdown: {
            commission: totalCommission,
            commissionPercentage: percentage,
            earned: earning,
        },
    };
    const existing = await admin
        .firestore()
        .collection(constants_1.DRIVERS_ACTIVITIES)
        .where('event', '==', log.event)
        .where('vendorOrderId', '==', log.vendorOrderId)
        .get();
    if (existing.size > 0)
        return;
    await admin.firestore().collection(constants_1.DRIVERS_ACTIVITIES).doc(log.id).set(log);
}
async function getRiderActiveDeliveries(riderId) {
    const activeDeliveries = await admin
        .database()
        .ref(`${constants_1.RIDERS_ACTIVE_ORDERS}/${riderId}`)
        .get();
    const dto = activeDeliveries.val();
    return dto ? Object.values(dto) : undefined;
}
//# sourceMappingURL=index.js.map