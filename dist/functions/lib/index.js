"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
const delete_rider_1 = require("./use-cases/riders/delete-rider");
const delete_vendor_1 = require("./use-cases/vendors/delete-vendor");
const generate_admin_pdf_request_1 = require("./use-cases/admin/generate-admin-pdf-request");
const generate_rider_withdrawals_invoice_1 = require("./use-cases/riders/generate-rider-withdrawals-invoice");
const generate_vendor_sales_invoice_1 = require("./use-cases/vendors/generate-vendor-sales-invoice");
const handle_app_credits_transactions_1 = require("./use-cases/customers/handle-app-credits-transactions");
const handle_best_offers_1 = require("./use-cases/products/handle-best-offers");
const handle_customer_referral_prizes_1 = require("./use-cases/customers/handle-customer-referral-prizes");
const notify_driver_withdrawal_request_1 = require("./use-cases/riders/notify-driver-withdrawal-request");
const notify_vendor_withdrawal_request_1 = require("./use-cases/admin/notify-vendor-withdrawal-request");
const process_app_credits_recharges_1 = require("./use-cases/customers/process-app-credits-recharges");
const process_purchases_with_app_credits_1 = require("./use-cases/delivery/process-purchases-with-app-credits");
const run_scheduled_deliveries_1 = require("./use-cases/delivery/run-scheduled-deliveries");
const send_chat_notification_1 = require("./use-cases/delivery/send-chat-notification");
const send_customer_order_updates_notifications_1 = require("./use-cases/delivery/send-customer-order-updates-notifications");
const send_driver_welcome_email_1 = require("./use-cases/riders/send-driver-welcome-email");
const send_new_vendor_order_push_notification_1 = require("./use-cases/delivery/send-new-vendor-order-push-notification");
const track_vendors_demand_1 = require("./use-cases/delivery/track-vendors-demand");
const update_drivers_total_earnings_1 = require("./use-cases/riders/update-drivers-total-earnings");
const update_order_products_demand_1 = require("./use-cases/products/update-order-products-demand");
const update_products_stock_1 = require("./use-cases/products/update-products-stock");
const update_vendors_total_earnings_1 = require("./use-cases/vendors/update-vendors-total-earnings");
const generate_order_invoice_1 = require("./use-cases/delivery/generate-order-invoice");
const handle_vendor_reviews_1 = require("./use-cases/delivery/handle-vendor-reviews");
const handle_rider_reviews_1 = require("./use-cases/delivery/handle-rider-reviews");
const send_rider_new_delivery_notification_1 = require("./use-cases/riders/send-rider-new-delivery-notification");
const handle_rider_referrals_1 = require("./use-cases/riders/handle-rider-referrals");
const update_delivery_service_status_1 = require("./use-cases/delivery/update-delivery-service-status");
const run_delivery_service_checker_1 = require("./use-cases/delivery/run-delivery-service-checker");
const update_rider_service_status_1 = require("./use-cases/delivery/update-rider-service-status");
const sync_vendor_location_1 = require("./use-cases/vendors/sync-vendor-location");
const app_1 = require("./utils/app");
const handle_customer_discount_code_redeems_1 = require("./use-cases/delivery/handle-customer-discount-code-redeems");
const notify_new_phone_pay_order_1 = require("./use-cases/admin/notify-new-phone-pay-order");
const handle_shops_openings_1 = require("./use-cases/vendors/handle-shops-openings");
const handle_immediate_bulk_push_notifications_1 = require("./use-cases/admin/handle-immediate-bulk-push-notifications");
const handle_scheduled_bulk_notifications_ts_1 = require("./use-cases/admin/handle-scheduled-bulk-notifications.ts");
const recalculate_product_info_1 = require("./use-cases/products/recalculate-product-info");
const sync_external_vendor_products_1 = require("./use-cases/products/sync-external-vendor-products");
const handle_discount_code_redeem_stats_1 = require("./use-cases/customers/handle-discount-code-redeem-stats");
const functions = require('firebase-functions');
const admin = require('firebase-admin');
if (admin.apps.length === 0) {
    admin.initializeApp();
}
exports.sendNewVendorOrderPushNotification = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onCreate(send_new_vendor_order_push_notification_1.default);
exports.updateOrderProductsDemand = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onWrite(update_order_products_demand_1.default);
exports.updateOrderProductsStock = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onWrite((change, context) => {
    return (0, update_products_stock_1.default)({ change, context }, constants_1.VENDOR_ORDERS);
});
exports.updateHoldingOrderProductsStock = functions.firestore
    .document(`/${constants_1.HOLDING_VENDOR_ORDERS}/{documentId}`)
    .onWrite((change, context) => {
    return (0, update_products_stock_1.default)({ change, context }, constants_1.HOLDING_VENDOR_ORDERS);
});
exports.trackVendorsDemand = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onUpdate(track_vendors_demand_1.default);
exports.sendChatNotification = functions.firestore
    .document(`/channels/{channelId}/thread/{threadId}`)
    .onCreate(send_chat_notification_1.default);
exports.generateVendorSalesInvoice = functions.firestore
    .document(`/vendor_invoices/{documentId}`)
    .onUpdate(generate_vendor_sales_invoice_1.default);
exports.generateRiderWithdrawalInvoice = functions.firestore
    .document(`/rider_invoices/{documentId}`)
    .onUpdate(generate_rider_withdrawals_invoice_1.default);
exports.generateOrderInvoice = functions.firestore
    .document(`/customer_invoices/{documentId}`)
    .onUpdate(generate_order_invoice_1.default);
exports.updateVendorTotalEarnings = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onUpdate(update_vendors_total_earnings_1.default);
exports.updateDriversTotalEarnings = functions.firestore
    .document(`/${constants_1.DRIVERS_ACTIVITIES}/{documentId}`)
    .onCreate(update_drivers_total_earnings_1.default);
exports.handleRiderReferrals = functions.firestore
    .document(`/${constants_1.RIDERS}/{documentId}`)
    .onCreate(handle_rider_referrals_1.default);
exports.notifyVendorWithdrawalRequest = functions.firestore
    .document(`/${constants_1.VENDORS_WITHDRAWALS_REQUESTS}/{documentId}`)
    .onCreate(notify_vendor_withdrawal_request_1.default);
exports.notifyDriverWithdrawalRequest = functions.firestore
    .document(`/${constants_1.RIDERS_WITHDRAWALS_REQUESTS}/{documentId}`)
    .onCreate(notify_driver_withdrawal_request_1.default);
exports.sendDriverWelcomeEmail = functions.firestore
    .document(`/${constants_1.RIDER_APPLICATIONS}/{documentId}`)
    .onCreate(send_driver_welcome_email_1.default);
exports.deleteVendor = functions.firestore
    .document(`/${constants_1.VENDORS}/{documentId}`)
    .onUpdate(delete_vendor_1.default);
exports.deleteRider = functions.firestore
    .document(`/${constants_1.RIDERS}/{documentId}`)
    .onUpdate(delete_rider_1.default);
exports.generateAdminPdfRequest = functions.firestore
    .document(`/admin_pdf/{documentId}`)
    .onCreate(generate_admin_pdf_request_1.default);
exports.handleVendorReviews = functions.firestore
    .document(`/${constants_1.VENDOR_REVIEWS}/{documentId}`)
    .onCreate(handle_vendor_reviews_1.default);
exports.handleRiderReviews = functions.firestore
    .document(`/${constants_1.RIDER_REVIEWS}/{documentId}`)
    .onCreate(handle_rider_reviews_1.default);
exports.handleBestOffers = functions.firestore
    .document(`/${constants_1.VENDOR_PRODUCTS}/{documentId}`)
    .onWrite(handle_best_offers_1.handleBestOffers);
exports.recalculateProductInfo = functions.firestore
    .document(`/${constants_1.VENDOR_PRODUCTS}/{documentId}`)
    .onWrite(recalculate_product_info_1.recalculateProductInfo);
exports.handleAppCreditsTransactions = functions.firestore
    .document(`/${constants_1.CREDIT_LOG_REF}/{documentId}`)
    .onWrite(handle_app_credits_transactions_1.handleAppCreditsTransactions);
exports.processAppCreditsRecharges = functions.firestore
    .document(`/${constants_1.CREDIT_RECHARGES_REF}/{documentId}`)
    .onWrite(process_app_credits_recharges_1.processAppCreditsRecharges);
exports.processPurchasesWithAppCredits = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onWrite(process_purchases_with_app_credits_1.processPurchasesWithAppCredits);
exports.handleCustomerReferralPrizes = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onWrite(handle_customer_referral_prizes_1.handleCustomerReferralPrizes);
exports.handleCustomerDiscountCodeRedeems = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onWrite(handle_customer_discount_code_redeems_1.handleCustomerDiscountCodeRedeems);
exports.sendCustomerOrderUpdatesNotifications = functions.firestore
    .document(`/${constants_1.VENDOR_ORDERS}/{documentId}`)
    .onWrite(send_customer_order_updates_notifications_1.sendCustomerOrderUpdatesNotifications);
exports.sendRiderNewDeliveryNotification = functions.database
    .ref(`${constants_1.RIDER_DELIVERY_ALERTS}/{documentId}`)
    .onCreate(send_rider_new_delivery_notification_1.sendRiderNewDeliveryNotification);
exports.updateDeliveryServiceStatus = functions.firestore
    .document(`${constants_1.DELIVERY_ORDERS}/{documentId}`)
    .onWrite(update_delivery_service_status_1.updateDeliveryServiceStatus);
exports.updateRiderServiceStatus = functions.firestore
    .document(`/${constants_1.RIDERS}/{documentId}`)
    .onWrite(update_rider_service_status_1.updateRiderServiceStatus);
exports.syncVendorLocation = functions.firestore
    .document(`/${constants_1.VENDORS}/{documentId}`)
    .onWrite(sync_vendor_location_1.syncVendorLocation);
exports.handleImmediateBulkPushNotifications = functions.firestore
    .document(`/${constants_1.ENGAGEMENT_NOTIFICATIONS}/{documentId}`)
    .onWrite(handle_immediate_bulk_push_notifications_1.handleImmediateBulkPushNotifications);
exports.runScheduledDeliveries = functions.pubsub
    .schedule(`every ${constants_1.SCHEDULED_DELIVERY_INTERVAL_MINUTES} minutes`)
    .onRun(run_scheduled_deliveries_1.default);
exports.handleScheduledBulkNotifications = functions.pubsub
    .schedule(`every 1 minutes`)
    .onRun(handle_scheduled_bulk_notifications_ts_1.handleScheduledBulkNotifications);
exports.runDeliveryServiceChecker = functions.pubsub
    .schedule(`every ${constants_1.DELIVERY_SERVICE_CHECKER_INTERVAL_MINUTES} minutes`)
    .onRun(run_delivery_service_checker_1.runDeliveryServiceChecker);
exports.handleShopsOpenings = functions.pubsub
    .schedule(`every 1 minutes`)
    .onRun(handle_shops_openings_1.handleShopsOpenings);
exports.notifyNewPhonePayOrder = functions.firestore
    .document(`/${constants_1.HOLDING_VENDOR_ORDERS}/{documentId}`)
    .onWrite(notify_new_phone_pay_order_1.notifyNewPhonePayOrder);
exports.syncExternalVendorProducts = functions.pubsub
    .schedule(`every 6 hours`)
    .onRun(sync_external_vendor_products_1.default);
exports.handleDiscountCodeRedeemStats = functions.firestore
    .document(`/${constants_1.DISCOUNT_CODES_REDEEMS}/{documentId}`)
    .onWrite(handle_discount_code_redeem_stats_1.handleDiscountCodeRedeemStats);
exports.testNotifications = functions.https.onRequest(async (req, res) => {
    var _a;
    const data = req.body;
    try {
        const r = await app_1.default.sendPushNotification({
            data: data.data,
            notification: data.notification,
        }, (_a = data.role) !== null && _a !== void 0 ? _a : 'CUSTOMER', data.userId);
        res.json({
            ok: true,
            result: r,
            data: data,
        });
    }
    catch (e) {
        res.json({
            ok: false,
            e: e === null || e === void 0 ? void 0 : e.message,
        });
    }
});
//# sourceMappingURL=index.js.map