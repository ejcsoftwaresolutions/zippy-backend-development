"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateRiderServiceStatus = void 0;
const constants_1 = require("../../../constants");
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const updateRiderServiceStatus = async (change, context) => {
    const rider = change.after.data();
    if (!rider)
        return Promise.resolve();
    const isOnline = rider.isOnline;
    functions.logger.log('Updating rider status', context.params.documentId);
    functions.logger.log('rider', rider);
    await admin
        .database()
        .ref(`${constants_1.CONNECTED_RIDERS}/${context.params.documentId}`)
        .set(isOnline ? new Date().toUTCString() : null);
    if (!isOnline) {
        await admin
            .database()
            .ref(`${constants_1.RIDERS_LOCATIONS}/${context.params.documentId}`)
            .set(null);
        await admin
            .database()
            .ref(`${constants_1.AVAILABLE_RIDERS}/${context.params.documentId}`)
            .set(null);
    }
    return Promise.resolve();
};
exports.updateRiderServiceStatus = updateRiderServiceStatus;
//# sourceMappingURL=index.js.map