"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const handleRiderReviews = async (snap, context) => {
    functions.logger.log('New rider review', context.params.documentId);
    const review = snap.data();
    const entityRef = await admin
        .firestore()
        .collection(constants_1.RIDERS)
        .doc(review.entityID);
    const entity = await app_1.default.findRider(review.entityID);
    if (!entity)
        return Promise.resolve();
    if (!entity.reviewsCount) {
        await entityRef.update({
            reviewsCount: 1, reviewsSum: review.rating,
        });
        return;
    }
    await entityRef.update({
        reviewsCount: admin.firestore.FieldValue.increment(1),
        reviewsSum: admin.firestore.FieldValue.increment(review.rating),
    });
};
exports.default = handleRiderReviews;
//# sourceMappingURL=index.js.map