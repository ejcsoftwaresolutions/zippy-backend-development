"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const order_invoice_1 = require("./order-invoice");
const firebase_utils_1 = require("../../../utils/firebase-utils");
const datetime_utils_1 = require("../../../utils/datetime-utils");
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const moment = require("moment");
const functions = require('firebase-functions');
const pdf = require('html-pdf');
const uuid = require('uuid');
const generateOrderInvoice = async (change, context) => {
    functions.logger.log('Generating order invoice', context.params.documentId);
    const newStatus = change.after.get('status');
    if (newStatus !== 'REQUESTED')
        return Promise.resolve();
    const invoiceRef = await admin
        .firestore()
        .collection(constants_1.CUSTOMER_INVOICES)
        .doc(context.params.documentId);
    const invoiceQ = await invoiceRef.get();
    const invoice = invoiceQ.data();
    if (!invoice)
        return Promise.resolve();
    const order = await app_1.default.getOrder(invoice.orderId);
    const fileName = `orders/${invoice.orderId}/invoice/${context.params.documentId}${moment().format('DD/MM/YYYY HH:mm:ss')}.pdf`;
    const fileRef = admin.storage().bucket().file(fileName, {});
    const { generate } = useGenerateInvoiceHtml();
    const content = generate(order);
    await invoiceRef.update({ status: 'GENERATING' });
    return new Promise((resolve) => {
        pdf
            .create(content, {
            timeout: '100000', width: 300, height: 800,
        })
            .toStream(async (err, stream) => {
            if (err) {
                await invoiceRef.update({
                    status: 'ERROR',
                });
                functions.logger.log('Error', err);
                return;
            }
            await stream.pipe(fileRef.createWriteStream({
                metadata: {
                    contentType: 'application/pdf', metadata: {
                        firebaseStorageDownloadTokens: uuid.v4(), lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                    },
                }, resumable: false, public: true,
            }));
            await invoiceRef.update({
                status: 'CREATED', url: fileRef.publicUrl(), updatedAt: new Date(),
            });
            resolve({});
        });
    });
};
exports.default = generateOrderInvoice;
function useGenerateInvoiceHtml() {
    function addonsHtml(addons) {
        return addons
            .map((addon) => {
            return `<div class='addon'>
                       <div>
                           ${addon.name}
                       </div>
                       <div class='price'>
                            $${addon.price}
                        </div>
                   </div>`;
        })
            .join('');
    }
    function getProducts(products) {
        return products.map((product) => {
            var _a;
            return `
                <div>
                    <div class='field product'>
                        <div>
                            <div><strong>${product.name}${product.option ? ' - ' + product.option.name : ''}</strong></div>
                            <div>${product.quantity} x $${product.price}</div>
                        </div>
                        <div>
                             $${product.quantity * product.price}
                        </div>
                    </div>
                    ${addonsHtml((_a = product.addons) !== null && _a !== void 0 ? _a : [])}
                </div>
            `;
        });
    }
    function getVendorHtml(vendorDto) {
        return `
             <div class='sectionContent'>
                <h4 class='title'>Tienda/Establecimiento</h4>
                <div class='field'>
                    <p><strong>Nombre: </strong> ${vendorDto.name}</p>
                </div>
            </div>
            `;
    }
    function getShippingAddress(shippingAddress) {
        function getCustomAddress() {
            const parts = [shippingAddress.line1, shippingAddress.line2, shippingAddress.city, shippingAddress.state,];
            return parts.join(', ');
        }
        return `
             <div class='sectionContent'>
                <h4 class='title'>Dirección de entrega</h4>
                <div class='field'>
                    <p>${shippingAddress.formattedAddress ? shippingAddress.formattedAddress : `${getCustomAddress()}`}</p>                 </div>
                </div>
            </div>
            `;
    }
    return {
        generate(order) {
            const productsHtml = getProducts(order.products).join('');
            return order_invoice_1.InvoiceHtml.replace('{{DATE}}', `${datetime_utils_1.default.format(new Date(), 'DD/MM/YYYY hh:mm A')} `)
                .replace('{{INVOICE_DATE}}', `${datetime_utils_1.default.format(firebase_utils_1.default.getDate(order.createdAt), 'DD/MM/YYYY hh:mm A')} `)
                .replace('{{ORDER_ID}}', `${order.code}`)
                .replace('{{CUSTOMER_NAME}}', `${order.customer.firstName}`)
                .replace('{{VENDOR_HTML}}', getVendorHtml(order.vendor))
                .replace('{{SHIPPING_ADDRESS_HTML}}', order.deliveryMethod == 'PICK_UP' ? '' : getShippingAddress(order.shipAddr))
                .replace('{{PRODUCTS_HTML}}', productsHtml)
                .replace('{{PAYMENT_METHOD_HTML}}', '')
                .replace('[[DELIVERY_FEES]]', (order.total - order.subtotal).toFixed(2))
                .replace('[[TOTAL]]', order.total);
        },
    };
}
//# sourceMappingURL=index.js.map