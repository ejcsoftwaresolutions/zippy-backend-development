"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleCustomerDiscountCodeRedeems = void 0;
const constants_1 = require("../../../constants");
const admin = require('firebase-admin');
const handleCustomerDiscountCodeRedeems = async (change, context) => {
    const order = change.after.data();
    if (!order)
        return Promise.resolve();
    if (!order.discountCode)
        return Promise.resolve();
    const customerId = order.customer.id;
    const ref = admin
        .firestore()
        .collection(constants_1.DISCOUNT_CODES_REDEEMS)
        .where('userId', '==', customerId)
        .where('orderId', '==', order.id)
        .where('code.id', '==', order.discountCode.id);
    const existing = (await ref.get()).docs.length > 0;
    if (existing) {
        if (order.status === 'Order Rejected') {
            return ref.ref.remove();
        }
        return Promise.resolve();
    }
    await admin.firestore().collection(constants_1.DISCOUNT_CODES_REDEEMS).add({
        userId: customerId,
        code: order.discountCode,
        orderId: order.id,
        redeemedAt: new Date(),
    });
};
exports.handleCustomerDiscountCodeRedeems = handleCustomerDiscountCodeRedeems;
//# sourceMappingURL=index.js.map