"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.processPurchasesWithAppCredits = void 0;
const admin = require('firebase-admin');
const handle_app_credits_transactions_1 = require("../../customers/handle-app-credits-transactions");
const constants_1 = require("../../../constants");
const functions = require('firebase-functions');
const processPurchasesWithAppCredits = async (change, context) => {
    const document = change.after.data();
    const oldDocument = change.before.data();
    if (!document)
        return Promise.resolve();
    if (document && oldDocument) {
        const orderHasBeenRejected = document.status !== oldDocument.status &&
            document.status == 'Order Rejected';
        if (orderHasBeenRejected &&
            oldDocument.appCreditsUsed &&
            !oldDocument.appCreditsRefunded) {
            await change.after.ref.update({
                appCreditsRefunded: true,
            });
            return insertRefundLog(await (0, handle_app_credits_transactions_1.createRefundCreditLog)({
                userId: document.customer.id,
                amount: document.appCreditsUsed,
                metadata: {
                    orderId: document.id,
                    orderCode: document.code,
                },
            }));
        }
        return Promise.resolve();
    }
    const orderData = document;
    if (!orderData.appCreditsUsed)
        return Promise.resolve();
    functions.logger.log('New Purchase with credits', context.params.documentId);
    return insertPurchaseLog({
        amount: orderData.appCreditsUsed,
        userId: orderData.customer.id,
        orderId: orderData.id,
        orderTotal: orderData.total,
        vendorId: orderData.vendor.id,
        vendorName: orderData.vendor.name,
    });
};
exports.processPurchasesWithAppCredits = processPurchasesWithAppCredits;
async function insertPurchaseLog(params) {
    const purchaseLog = await (0, handle_app_credits_transactions_1.createPurchaseCreditLog)({
        userId: params.userId,
        amount: params.amount,
        metadata: {
            orderId: params.orderId,
            orderTotal: params.orderTotal,
            vendorId: params.vendorId,
            vendorName: params.vendorName,
        },
    });
    await admin
        .firestore()
        .collection(constants_1.CREDIT_LOG_REF)
        .doc(purchaseLog.id)
        .set(purchaseLog);
}
async function insertRefundLog(log) {
    await admin.firestore().collection(constants_1.CREDIT_LOG_REF).doc(log.id).set(log);
}
//# sourceMappingURL=index.js.map