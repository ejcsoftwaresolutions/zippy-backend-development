"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const admin = require('firebase-admin');
const functions = require('firebase-functions');
const sendChatNotification = async (snap, context) => {
    functions.logger.log('Chat notification ', context.params.threadId);
    const messageData = snap.data();
    const senderId = messageData.senderID;
    const channelS = await admin
        .firestore()
        .collection('channels')
        .doc(context.params.channelId)
        .get();
    const channel = channelS.data();
    if (!channel)
        return;
    const recipients = channel.participants.filter((p) => p.id !== senderId);
    functions.logger.log('Recipients', recipients);
    const senderName = messageData.senderFirstName + ' ' + messageData.senderLastName;
    const promises = recipients.map((u) => {
        return new Promise(async (resolve, reject) => {
            if (!u.role)
                return resolve();
            await handleUserBadgeCount(u.id, u.role);
            try {
                await handleDeliveryBadgeCount(u.metadata.orderId, u.role);
            }
            catch (e) { }
            const message = !messageData.url
                ? messageData.content
                : messageData.url.mime == 'image'
                    ? 'Te ha enviado una imagen'
                    : messageData.url.mime == 'video'
                        ? 'Te ha enviado un video'
                        : '';
            await app_1.default.sendPushNotification({
                data: {
                    toUserID: u.id,
                    type: 'chat_message',
                    channelID: context.params.channelId,
                },
                notification: {
                    title: `${senderName} te ha enviado un mensaje`,
                    body: message,
                },
                channel: u.role === 'CUSTOMER' ? undefined : 'order-update',
            }, u.role, u.id);
            return resolve();
        });
    });
    return Promise.all(promises);
};
const handleUserBadgeCount = async (userID, role) => {
    const collection = (() => {
        if (role === 'RIDER')
            return constants_1.RIDERS;
        if (role === 'VENDOR')
            return constants_1.VENDORS;
        return constants_1.CUSTOMERS;
    })();
    const userRef = admin.firestore().collection(collection).doc(userID);
    const user = (await userRef.get()).data();
    if (!user)
        return 0;
    const { badgeCount } = user;
    const current = parseInt(badgeCount ? badgeCount : 0);
    const newBadgeCount = (isNaN(current) ? 0 : current) + 1;
    await userRef.update({ badgeCount: newBadgeCount });
    return newBadgeCount;
};
const handleDeliveryBadgeCount = async (orderID, role) => {
    const deliveryOrderRef = admin
        .firestore()
        .collection(constants_1.DELIVERY_ORDERS)
        .doc(orderID);
    const deliveryOrder = (await deliveryOrderRef.get()).data();
    if (!deliveryOrder)
        return 0;
    const badgeCount = deliveryOrder.unseenMessages
        ? deliveryOrder.unseenMessages[role]
        : 0;
    const current = parseInt(badgeCount ? badgeCount : 0);
    const newBadgeCount = (isNaN(current) ? 0 : current) + 1;
    await deliveryOrderRef.update({
        [`unseenMessages.${role}`]: newBadgeCount,
    });
    return newBadgeCount;
};
exports.default = sendChatNotification;
//# sourceMappingURL=index.js.map