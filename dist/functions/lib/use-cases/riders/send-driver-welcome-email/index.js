"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const sendDriverWelcomeEmail = async (snap, context) => {
    functions.logger.log('send welcome email', context.params.documentId);
    const user = snap.data();
    return Promise.all([sendEmail(user)]);
};
async function sendEmail(user) {
    return app_1.default.sendEmail(user.email.trim(), {
        subject: `Confirmación de Registro en la plataforma como Zipper, de ${user === null || user === void 0 ? void 0 : user.firstName} ${user === null || user === void 0 ? void 0 : user.lastName}`,
        text: `Hola, este es un mensaje de Confirmación de Registro en la plataforma como Zipper, para ${user === null || user === void 0 ? void 0 : user.firstName} ${user === null || user === void 0 ? void 0 : user.lastName}`,
        html: `
              <div>
                <p>Hola, este es un mensaje de Confirmación de Registro en la plataforma como Zipper, para
                ${user === null || user === void 0 ? void 0 : user.firstName} ${user === null || user === void 0 ? void 0 : user.lastName} </p>
                <br/>
                <p>Nombre: <strong> ${user === null || user === void 0 ? void 0 : user.firstName} ${user === null || user === void 0 ? void 0 : user.lastName}</strong> </p>
                <p>Número de teléfono: <strong>${user === null || user === void 0 ? void 0 : user.phone} </strong>  </p>
                <br/>
                <p> <strong> Gracias por Preferirnos.</strong> </p>
              </div>
          `,
    });
}
exports.default = sendDriverWelcomeEmail;
//# sourceMappingURL=index.js.map