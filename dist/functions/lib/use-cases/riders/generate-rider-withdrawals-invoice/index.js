"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const rider_withdrawals_invoice_1 = require("./rider-withdrawals-invoice");
const app_1 = require("../../../utils/app");
const moment = require("moment");
const functions = require('firebase-functions');
const pdf = require('html-pdf');
const uuid = require('uuid');
const FirebaseUtils = {
    getDate(firebaseDate) {
        return new Date(firebaseDate.seconds * 1000);
    },
};
const generateRiderWithdrawalInvoice = async (change, context) => {
    var _a;
    functions.logger.log('Generating withdrawal invoice', context.params.documentId);
    const newStatus = change.after.get('status');
    if (newStatus !== 'REQUESTED')
        return Promise.resolve();
    const invoiceRef = await admin
        .firestore()
        .collection('rider_invoices')
        .doc(context.params.documentId);
    const invoiceQ = await invoiceRef.get();
    const riderInvoice = invoiceQ.data();
    if (!riderInvoice)
        return Promise.resolve();
    const requestQ = await admin
        .firestore()
        .collection('driver_withdrawal_requests')
        .doc(riderInvoice.requestId)
        .get();
    const request = requestQ.data();
    if (!request)
        return Promise.resolve();
    functions.logger.log('Retiro', request.id);
    functions.logger.log('User', riderInvoice.userId);
    const fileName = `riders/${riderInvoice.userId}/invoices/${context.params.documentId}-${request.id}${moment().format('DD/MM/YYYY HH:mm:ss')}.pdf`;
    const fileRef = admin.storage().bucket().file(fileName, {});
    const rider = await app_1.default.findRider(riderInvoice.userId);
    if (!rider)
        return Promise.resolve();
    const content = rider_withdrawals_invoice_1.withdrawalHTMLTemplate
        .replace(/{{CLIENT_NAME}}/g, `${rider === null || rider === void 0 ? void 0 : rider.firstName} ${rider === null || rider === void 0 ? void 0 : rider.lastName}`)
        .replace('{{CLIENT_DOCUMENT}}', rider === null || rider === void 0 ? void 0 : rider.identificationCard)
        .replace('{{CLIENT_EMAIL}}', rider === null || rider === void 0 ? void 0 : rider.email)
        .replace('{{CLIENT_ADDRESS}}', (_a = rider === null || rider === void 0 ? void 0 : rider.homeAddress) !== null && _a !== void 0 ? _a : '')
        .replace('{{CLIENT_PHONE}}', rider === null || rider === void 0 ? void 0 : rider.phone)
        .replace('{{CLIENT_ACCOUNT_NUMBER}}', `${request.details.accountType
        .replace('SAVINGS', 'C.A')
        .replace('CHECKING', 'C.C')} ${request.details.accountNumber}`)
        .replace('{{CLIENT_ACCOUNT_BANK_NAME}}', request.details.bankName)
        .replace('{{INVOICE_NUMBER}}', riderInvoice.code)
        .replace('{{INVOICE_DATE}}', `${moment().format('DD/MM/YYYY')}`)
        .replace('{{INVOICE_WEEK_RANGE}}', `${moment(FirebaseUtils.getDate(request.weekStart)).format('DD/MM/YYYY')} al ${moment(FirebaseUtils.getDate(request.weekEnd)).format('DD/MM/YYYY')}`)
        .replace('INVOICE_TOTAL', request.amount);
    await invoiceRef.update({ status: 'GENERATING' });
    return new Promise((resolve) => {
        pdf
            .create(content, {
            timeout: '100000', width: 300, height: 800,
        })
            .toStream(async (err, stream) => {
            if (err) {
                await invoiceRef.update({
                    status: 'ERROR',
                });
                functions.logger.log('Error', err);
                return;
            }
            await stream.pipe(fileRef.createWriteStream({
                metadata: {
                    contentType: 'application/pdf', metadata: {
                        firebaseStorageDownloadTokens: uuid.v4(), lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                    },
                }, resumable: false, public: true,
            }));
            await invoiceRef.update({
                status: 'CREATED', url: fileRef.publicUrl(), updatedAt: new Date(),
            });
            resolve({});
        });
    });
};
exports.default = generateRiderWithdrawalInvoice;
//# sourceMappingURL=index.js.map