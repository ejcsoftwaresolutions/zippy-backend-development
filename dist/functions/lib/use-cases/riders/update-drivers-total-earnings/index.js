"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require('firebase-admin');
const constants_1 = require("../../../constants");
const functions = require('firebase-functions');
const updateDriversTotalEarnings = async (snap, context) => {
    var _a;
    functions.logger.log('Updating driver total earnings', context.params.documentId);
    const driverEvent = snap.data();
    if (['ORDER_COMPLETED', 'REFERRAL_CODE_REDEEM', 'REFERRAL_EARNING_PRIZE',].indexOf(driverEvent.event) <= -1)
        return Promise.resolve();
    if (!driverEvent)
        return Promise.resolve();
    const docId = context.params.documentId;
    const driverEarningsQ = await admin
        .firestore()
        .collection(constants_1.DRIVERS_TOTAL_EARNINGS)
        .doc(driverEvent.driverId)
        .get();
    const driverEarnings = driverEarningsQ.data();
    if (driverEarnings) {
        const demandDailyDocRef = await admin
            .firestore()
            .collection(constants_1.DRIVERS_TOTAL_EARNINGS)
            .doc(driverEvent.driverId);
        return demandDailyDocRef.update({
            total: admin.firestore.FieldValue.increment(parseFloat(driverEvent.fee)),
            lastUpdate: new Date(),
            events: [...((_a = driverEarnings.events) !== null && _a !== void 0 ? _a : []), docId],
        });
    }
    const info = {
        driverID: driverEvent.driverId, total: parseFloat(driverEvent.fee), lastUpdate: new Date(), events: [docId],
    };
    return admin
        .firestore()
        .collection(constants_1.DRIVERS_TOTAL_EARNINGS)
        .doc(driverEvent.driverId)
        .set(info);
};
exports.default = updateDriversTotalEarnings;
//# sourceMappingURL=index.js.map