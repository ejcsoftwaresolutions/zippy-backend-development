"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require('firebase-functions');
const deleteRider = async (snap, context) => {
    functions.logger.log('Deleting rider', context.params.documentId);
};
exports.default = deleteRider;
//# sourceMappingURL=index.js.map