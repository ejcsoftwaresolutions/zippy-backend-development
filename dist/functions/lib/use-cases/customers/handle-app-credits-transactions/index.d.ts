export const __esModule: boolean;
export function handleAppCreditsTransactions(change: any, context: any): Promise<void>;
export function sendOtherCreditNotification({ userId, amount, description, }: {
    userId: any;
    amount: any;
    description: any;
}): Promise<void>;
export function createRechargeCreditLog({ userId, amount, metadata, }: {
    userId: any;
    amount: any;
    metadata: any;
}): Promise<{
    id: any;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: any;
    userId: any;
    amount: any;
}>;
export function createPurchaseCreditLog({ userId, amount, metadata, }: {
    userId: any;
    amount: any;
    metadata: any;
}): Promise<{
    id: any;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: any;
    userId: any;
    amount: any;
}>;
export function createRefundCreditLog({ userId, amount, metadata, }: {
    userId: any;
    amount: any;
    metadata: any;
}): Promise<{
    id: any;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: any;
    userId: any;
    amount: any;
}>;
export function createReferralCreditLog({ userId, amount, metadata, }: {
    userId: any;
    amount: any;
    metadata: any;
}): Promise<{
    id: any;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: any;
    userId: any;
    amount: any;
}>;
export function createReferrerCreditLog({ userId, amount, metadata, }: {
    userId: any;
    amount: any;
    metadata: any;
}): Promise<{
    id: any;
    date: Date;
    operation: string;
    concept: string;
    processed: boolean;
    metadata: any;
    userId: any;
    amount: any;
}>;
