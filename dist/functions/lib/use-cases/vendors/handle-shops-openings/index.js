"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleShopsOpenings = void 0;
const datetime_utils_1 = require("../../../utils/datetime-utils");
const constants_1 = require("../../../constants");
const app_1 = require("../../../utils/app");
const admin = require('firebase-admin');
const moment = require("moment-timezone");
const functions = require('firebase-functions');
const handleShopsOpenings = async (context) => {
    const currentDay = datetime_utils_1.default.format(new Date(), 'dddd').toUpperCase();
    const currentTime = moment(moment().tz('America/Caracas').format('HH:mm'), 'HH:mm');
    const todayWorkingVendors = (await admin
        .firestore()
        .collection(constants_1.VENDORS)
        .where(`schedule.${currentDay}.active`, '==', true)
        .get()).docs.map((v) => v.data());
    const vendorsToClose = todayWorkingVendors.filter((v) => {
        if (v.status !== 'ACTIVE')
            return false;
        const todaySchedule = v.schedule[currentDay];
        if (!todaySchedule.endHourTimestamp)
            return false;
        const closeHour = moment(todaySchedule.endHour, 'HH:mm');
        const remainingMinutesToClose = datetime_utils_1.default.differenceInMinutesMoment(closeHour, currentTime);
        return v.isOpen && remainingMinutesToClose == 0;
    });
    const vendorsToOpen = todayWorkingVendors.filter((v) => {
        if (v.status !== 'ACTIVE')
            return false;
        const todaySchedule = v.schedule[currentDay];
        if (!todaySchedule.startHourTimestamp)
            return false;
        const startHour = moment(todaySchedule.startHour, 'HH:mm');
        const closeHour = moment(todaySchedule.endHour, 'HH:mm');
        const remainingMinutesToOpen = datetime_utils_1.default.differenceInMinutesMoment(startHour, currentTime);
        const remainingMinutesToClose = datetime_utils_1.default.differenceInMinutesMoment(closeHour, currentTime);
        return (!v.isOpen && remainingMinutesToOpen == 0 && remainingMinutesToClose > 0);
    });
    await toggleOpenVendors(vendorsToClose, false);
    await toggleOpenVendors(vendorsToOpen, true);
};
exports.handleShopsOpenings = handleShopsOpenings;
function toggleOpenVendors(vendors, isOpen) {
    functions.logger.log(isOpen ? 'vendors to open' : 'vendors to close', vendors.map((v) => `${v.id} ${v.name}`));
    const ref = admin.firestore().collection(constants_1.VENDORS);
    const batch = admin.firestore().batch();
    vendors.forEach((v) => {
        const sfRef = ref.doc(v.id);
        batch.update(sfRef, { isOpen: isOpen });
    });
    batch.commit().then(() => {
    });
    vendors.forEach((v) => {
        const notiInfo = isOpen
            ? {
                data: {},
                notification: {
                    title: 'Hemos abierto tu tienda! 🔓🛒',
                    body: `🔥Activo para recibir ordenes!🔥.`,
                },
            }
            : {
                data: {},
                notification: {
                    title: '🕖 Tu tienda se ha cerrado por hoy 🔐',
                    body: `Ya hemos bajado la santamaría por ti!, echa un vistazo a tus ventas de hoy 💵.`,
                },
            };
        app_1.default.sendPushNotification(notiInfo, 'VENDOR', v.id).then(() => {
        });
    });
}
//# sourceMappingURL=index.js.map