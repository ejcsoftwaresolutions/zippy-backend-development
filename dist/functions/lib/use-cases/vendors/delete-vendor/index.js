"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require('firebase-functions');
const deleteVendor = async (snap, context) => {
    functions.logger.log('Deleting vendor', context.params.documentId);
};
exports.default = deleteVendor;
//# sourceMappingURL=index.js.map