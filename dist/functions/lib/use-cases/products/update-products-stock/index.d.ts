export const __esModule: boolean;
export default updateOrderProductsStock;
declare function updateOrderProductsStock({ change, context, }: {
    change: any;
    context: any;
}, source: any): Promise<void>;
