"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../../../utils/app");
const functions = require('firebase-functions');
const notifyVendorWithdrawalRequest = async (snap, context) => {
    var _a, _b, _c, _d;
    functions.logger.log('Notify vendor withdrawal request', context.params.documentId);
    const ADMIN_EMAIL = process.env.ZIPPI_PAYMENTS_EMAIL;
    const data = snap.data();
    const vendor = await app_1.default.findVendor(data.vendorId);
    const account = await app_1.default.findAccount(data.vendorId);
    if (!vendor || !account)
        return Promise.resolve();
    return app_1.default.sendEmail(ADMIN_EMAIL, {
        subject: `Nueva solicitud de retiro de vendedor`,
        text: `Nueva solicitud de retiro para el vendedor ${(_a = account.firstName) === null || _a === void 0 ? void 0 : _a.trim()} ${(_b = account.lastName) === null || _b === void 0 ? void 0 : _b.trim()} de la tienda ${vendor.title}`,
        html: `
        <div>
          <p>Hola, el vendedor ${(_c = account.firstName) === null || _c === void 0 ? void 0 : _c.trim()} ${(_d = account.lastName) === null || _d === void 0 ? void 0 : _d.trim()} de la tienda ${vendor.title} ha solicitado el retiro de $${data.amount}</p>
          <br/>
        </div>
    `,
    });
};
exports.default = notifyVendorWithdrawalRequest;
//# sourceMappingURL=index.js.map