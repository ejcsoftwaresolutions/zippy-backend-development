"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.notifyNewPhonePayOrder = void 0;
const app_1 = require("../../../utils/app");
const notifyNewPhonePayOrder = async (change, context) => {
    const document = change.after.data();
    if (!document)
        return Promise.resolve();
    await sendAdminEmail(document);
    return Promise.resolve();
};
exports.notifyNewPhonePayOrder = notifyNewPhonePayOrder;
async function sendAdminEmail(document) {
    var _a, _b;
    const admin = await app_1.default.findAdminAccount();
    if (!admin || !((_a = admin.config) === null || _a === void 0 ? void 0 : _a.notificationsEmail))
        return;
    const emailData = {
        subject: `${process.env.ENVIRONMENT === 'production' ? 'LIVE' : 'TEST'} - Nuevo reporte de pago móvil para orden #${document.code}`,
        text: `Nuevo reporte de pago móvil para orden #${document.code}`,
        html: `
         <div>
             <p>
                  Cliente: ${document.customer.firstName} ${document.customer.lastName} - ${document.customer.id}.  ${document.customer.email} ${document.customer.phone}
            </p>
            <p>
                  Orden: ${document.id} - ${document.code} 
            </p>
             <p>
                 Total en USD: ${document.total} Tasa aplicada: ${document.paymentReport.exchangeRate}
            </p>
             <p>
                  Total en Bs.D pagado: ${document.paymentReport.amount}
             </p>
              <p>
                  Telefono pagador: ${document.paymentReport.phone}
             </p>
             <p>
                  TxID: ${document.paymentReport.transactionId}
             </p>
          </div>
        `,
    };
    await app_1.default.sendEmail((_b = admin.config) === null || _b === void 0 ? void 0 : _b.notificationsEmail, emailData);
    await app_1.default.sendEmail(process.env.ZIPPI_PAYMENTS_EMAIL, emailData);
}
//# sourceMappingURL=index.js.map