export const __esModule: boolean;
export const BulkNotificationService: one_signal_bulk_notification_service_1.default | FCM_bulk_notification_service_1.default;
export function createBulkNotificationService(notificationService: any): one_signal_bulk_notification_service_1.default | FCM_bulk_notification_service_1.default;
import one_signal_bulk_notification_service_1 = require("./one-signal-bulk-notification-service");
import FCM_bulk_notification_service_1 = require("./FCM-bulk-notification-service");
