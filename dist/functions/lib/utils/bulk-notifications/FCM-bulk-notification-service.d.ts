export const __esModule: boolean;
export default FCMBulkNotificationService;
declare class FCMBulkNotificationService {
    constructor(pushNotificationService: any, config: any);
    pushNotificationService: any;
    config: any;
    registeredVariables: string[];
    sendCampaignNotification(targetGroup: any, message: any): Promise<{
        deliveryIds: any[];
        recipients: any;
        targetSize: any;
    }>;
    sendBulkIndividualNotification(targetGroup: any, message: any, targetRecipients: any): Promise<{
        deliveryIds: any[];
        recipients: any;
        targetSize: any;
    }>;
    getBulkNotificationProcessResult(results: any): Partial<any>;
    sendInChunks(usersChunks: any, message: any): Promise<{
        deliveryIds: any[];
        recipients: any;
        targetSize: any;
    }>;
    sendMessage(user: any, message: any): Promise<any>;
    replaceVariables(user: any, text: any): any;
    findTargetGroupUsers(role: any): Promise<any>;
    findUsersTokens(ids: any, role: any): Promise<any[]>;
    getChunks(inputArray: any, perChunk: any): any;
}
