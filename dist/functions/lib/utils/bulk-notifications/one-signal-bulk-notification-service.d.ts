export const __esModule: boolean;
export default OneSignalBulkNotificationService;
declare class OneSignalBulkNotificationService {
    constructor(pushNotificationService: any, config: any);
    pushNotificationService: any;
    config: any;
    registeredVariables: string[];
    sendCampaignNotification(targetGroup: any, message: any): Promise<{
        deliveryIds: any[];
        recipients: any;
        targetSize: number;
    }>;
    sendBulkIndividualNotification(targetGroup: any, message: any, targetRecipients: any): Promise<{
        deliveryIds: any[];
        recipients: any;
        targetSize: any;
    }>;
    getBulkNotificationProcessResult(results: any): Partial<any>;
    sendInChunks(usersChunks: any, message: any): Promise<{
        deliveryIds: any[];
        recipients: any;
        targetSize: any;
    }>;
    sendMessage(users: any, message: any): Promise<any>;
    buildNotification(to: any, message: any): push_notification_1.PushNotification;
    cleanVariables(text: any): any;
    getChunks(inputArray: any, perChunk: any): any;
}
import push_notification_1 = require("../models/push-notification");
