"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BulkNotificationService = exports.createBulkNotificationService = void 0;
const notification_service_1 = require("../notification-service/notification-service");
const one_signal_bulk_notification_service_1 = require("./one-signal-bulk-notification-service");
const FCM_bulk_notification_service_1 = require("./FCM-bulk-notification-service");
function createBulkNotificationService(notificationService) {
    const provider = notificationService.getName();
    if (provider !== 'ONE_SIGNAL') {
        return new FCM_bulk_notification_service_1.default({
            send: (notification) => {
                return notificationService.send(notification);
            },
        }, {
            chunkSize: 50,
        });
    }
    return new one_signal_bulk_notification_service_1.default({
        send: (notification) => {
            return notificationService.send(notification);
        },
    }, {
        chunkSize: 2000,
    });
}
exports.createBulkNotificationService = createBulkNotificationService;
exports.BulkNotificationService = createBulkNotificationService(notification_service_1.NotificationService);
//# sourceMappingURL=bulk-notification-service.js.map