export const __esModule: boolean;
export default Model;
declare class Model {
    constructor(props: any);
    props: any;
    toJson(): Partial<any>;
    toObject(options: any): Partial<any>;
    toString(): string;
}
