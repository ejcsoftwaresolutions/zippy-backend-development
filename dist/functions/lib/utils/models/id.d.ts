export const __esModule: boolean;
export default Id;
declare class Id extends value_object_1.default {
    get value(): any;
    toPrimitives(): any;
    toString(): any;
    gen(): any;
}
import value_object_1 = require("./value-object");
