export const __esModule: boolean;
export default Currency;
declare class Currency {
    static fromPrimitives(props: any): Currency;
    constructor(props: any);
    props: any;
    get name(): any;
    get abbr(): any;
    isEqual(otherCurrency: any): boolean;
    format(value: any): any;
    toPrimitives(): {
        abbr: any;
        name: any;
        format: any;
    };
}
