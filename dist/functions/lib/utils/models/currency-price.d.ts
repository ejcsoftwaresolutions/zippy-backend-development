export const __esModule: boolean;
export default CurrencyPrice;
declare class CurrencyPrice {
    static sum(priceA: any, priceB: any): CurrencyPrice;
    static subtract(priceA: any, priceB: any): CurrencyPrice;
    static times(currencyPrice: any, times: any, totalCurrency: any): CurrencyPrice;
    static div(currencyPrice: any, times: any, totalCurrency: any): CurrencyPrice;
    static getAcc(prices: any): CurrencyPrice;
    static fromPrimitives(props: any): CurrencyPrice;
    static isEqual(priceA: any, priceB: any): boolean;
    static equalPrices(pricesA: any, pricesB: any): boolean;
    static fromZero(currency: any): CurrencyPrice;
    static fromPrimitiveArray(items: any): any;
    constructor(props: any);
    props: any;
    get value(): any;
    get formattedValue(): any;
    get currency(): any;
    toPrimitives(): any;
}
