export const __esModule: boolean;
export class PushNotification {
    constructor(props: any);
    props: any;
    get id(): any;
    get to(): any;
    get appName(): "RIDER" | "CUSTOMER" | "VENDOR";
    get isSegment(): boolean;
    get isExternalId(): boolean;
    toPrimitives(): {
        id: any;
        to: any;
        title: any;
        content: any;
        appId: any;
        channelId: any;
        metadata: any;
        imageUrl: any;
        linkUrl: any;
    };
}
