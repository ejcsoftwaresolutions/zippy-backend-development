"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PushNotification = void 0;
class PushNotification {
    constructor(props) {
        this.props = props;
    }
    get id() {
        return this.props.id;
    }
    get to() {
        return this.props.to;
    }
    get appName() {
        if (this.props.appId === process.env.CUSTOMER_APP_EXPERIENCE_ID)
            return 'CUSTOMER';
        if (this.props.appId === process.env.STORE_APP_EXPERIENCE_ID)
            return 'VENDOR';
        return 'RIDER';
    }
    get isSegment() {
        var _a;
        return !!((_a = this.props.metadata) === null || _a === void 0 ? void 0 : _a.isSegment);
    }
    get isExternalId() {
        var _a;
        return !!((_a = this.props.metadata) === null || _a === void 0 ? void 0 : _a.isExternalId);
    }
    toPrimitives() {
        var _a;
        return {
            id: this.props.id,
            to: this.props.to,
            title: this.props.title,
            content: this.props.content,
            appId: this.props.appId,
            channelId: (_a = this.props.channelId) !== null && _a !== void 0 ? _a : this.props.appId.split('/')[1],
            metadata: this.props.metadata,
            imageUrl: this.props.imageUrl,
            linkUrl: this.props.linkUrl,
        };
    }
}
exports.PushNotification = PushNotification;
//# sourceMappingURL=push-notification.js.map