"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currency_utils_1 = require("../currency-utils");
class Currency {
    constructor(props) {
        this.props = props;
    }
    get name() {
        return this.props.name;
    }
    get abbr() {
        return this.props.abbr;
    }
    static fromPrimitives(props) {
        const curr = new Currency(props);
        if (curr.abbr === currency_utils_1.bsAbbr) {
            return new Currency(Object.assign(Object.assign({}, curr.props), { format: currency_utils_1.bsFormat }));
        }
        if (curr.abbr === currency_utils_1.usdAbbr) {
            return new Currency(Object.assign(Object.assign({}, curr.props), { format: currency_utils_1.usdFormat }));
        }
        return curr;
    }
    isEqual(otherCurrency) {
        return this.props.abbr == otherCurrency.abbr;
    }
    format(value) {
        return this.props.format
            ? this.props.format(value ? value : 0)
            : value.toString();
    }
    toPrimitives() {
        return {
            abbr: this.props.abbr,
            name: this.props.name,
            format: this.props.format,
        };
    }
}
exports.default = Currency;
//# sourceMappingURL=currency.js.map