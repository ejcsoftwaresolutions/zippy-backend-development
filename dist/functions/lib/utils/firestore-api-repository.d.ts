export const __esModule: boolean;
export { FieldValue as FirestoreValue };
export default FirestoreApiRepository;
declare var FieldValue: typeof FirebaseFirestore.FieldValue;
declare class FirestoreApiRepository {
    db: any;
    get firestore(): any;
    getDoc(collectionName: any, docId: any): Promise<any>;
    updateDoc(collectionName: any, docId: any, data: any): Promise<void>;
    getDocsRef(collectionName: any): any;
    deleteDoc(collectionName: any, docId: any): Promise<void>;
    setDoc(collectionName: any, docId: any, data: any): Promise<void>;
    saveDoc(collectionName: any, docId: any, data: any): Promise<void>;
    deleteDocField(collectionName: any, docId: any, field: any): Promise<void>;
    getDocs(collectionName: any, params: any): Promise<any>;
    saveInBatch(collectionName: any, docs: any): Promise<void>;
    getChunks(inputArray: any, perChunk: any): any;
    getDocsData(collectionName: any, params: any): Promise<any>;
    getDocsWithFiltersRef(collectionName: any, params: any): any;
}
