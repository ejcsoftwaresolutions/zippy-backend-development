"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const numeral = require('numeral');
const NumberUtils = {
    format: (value, formatString) => {
        return numeral(value).format(formatString);
    },
};
exports.default = NumberUtils;
//# sourceMappingURL=number-utils.js.map