export const __esModule: boolean;
export default FCMNotificationService;
declare class FCMNotificationService {
    constructor(serverKey: any, projectId: any, serviceAccountLocation: any);
    serverKey: any;
    projectId: any;
    serviceAccountLocation: any;
    getName(): string;
    send(notification: any): Promise<{
        ok: boolean;
        recipients: number;
        metadata: any;
        error?: undefined;
    } | {
        ok: boolean;
        recipients: number;
        error: any;
        metadata?: undefined;
    }>;
    getNotificationStats(id: any, targetApp: any): Promise<{
        audience: number;
        failed: number;
        successful: number;
        clicks: number;
    }>;
    getAccessToken(): Promise<any>;
}
