export const __esModule: boolean;
export const NotificationService: one_signal_notification_service_1.default | FCM_notification_service_1.default;
export function createNotificationService(defaultProvider?: string): one_signal_notification_service_1.default | FCM_notification_service_1.default;
import one_signal_notification_service_1 = require("./one-signal-notification-service");
import FCM_notification_service_1 = require("./FCM-notification-service");
