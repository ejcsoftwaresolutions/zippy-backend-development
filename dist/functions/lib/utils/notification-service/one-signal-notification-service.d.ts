export const __esModule: boolean;
export default OneSignalNotificationService;
declare class OneSignalNotificationService {
    constructor(params: any);
    apps: any;
    authKey: any;
    getName(): string;
    send(notification: any): Promise<{
        ok: boolean;
        recipients: number;
        metadata: node_onesignal_1.CreateNotificationSuccessResponse;
        deliveryId: string;
        error?: undefined;
    } | {
        ok: boolean;
        recipients: number;
        error: any;
        metadata?: undefined;
        deliveryId?: undefined;
    }>;
    getNotificationStats(id: any, targetApp: any): Promise<{
        audience: number;
        failed: number;
        successful: number;
        clicks: number;
    }>;
    getAppId(appName: any): Promise<any>;
    getAppKey(appName: any): Promise<any>;
    configureClient(appName: any): Promise<void>;
    client: node_onesignal_1.DefaultApi;
    buildBaseNotification(notification: any): Promise<node_onesignal_1.Notification>;
}
import node_onesignal_1 = require("@onesignal/node-onesignal");
