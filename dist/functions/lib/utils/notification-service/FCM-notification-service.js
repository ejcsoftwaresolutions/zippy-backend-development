"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const google_auth_library_1 = require("google-auth-library");
const axios = require('axios');
class FCMNotificationService {
    constructor(serverKey, projectId, serviceAccountLocation) {
        this.serverKey = serverKey;
        this.projectId = projectId;
        this.serviceAccountLocation = serviceAccountLocation;
    }
    getName() {
        return 'FCM';
    }
    async send(notification) {
        var _a, _b, _c;
        const url = `https://fcm.googleapis.com/v1/projects/${this.projectId}/messages:send`;
        const token = await this.getAccessToken();
        const notificationData = notification.toPrimitives();
        const imageUrl = notificationData.imageUrl;
        try {
            const result = await axios.post(url, {
                message: {
                    token: notificationData.to,
                    data: {
                        experienceId: notificationData.appId,
                        title: notificationData.title,
                        message: notificationData.content,
                        metadata: JSON.stringify((_a = notificationData.metadata) !== null && _a !== void 0 ? _a : {}),
                    },
                    notification: Object.assign({ title: notificationData.title, body: notificationData.content }, (imageUrl
                        ? {
                            image: imageUrl,
                        }
                        : {})),
                    android: {
                        priority: 'high',
                        notification: {
                            channel_id: notificationData === null || notificationData === void 0 ? void 0 : notificationData.channelId,
                            notification_count: (_c = parseInt((_b = notificationData.metadata) === null || _b === void 0 ? void 0 : _b.badge)) !== null && _c !== void 0 ? _c : undefined,
                        },
                    },
                    apns: Object.assign({ headers: {
                            'apns-priority': '10',
                        }, payload: {
                            aps: Object.assign({ sound: 'notification.wav' }, (imageUrl ? { 'mutable-content': 1 } : {})),
                            alert: {
                                title: notificationData.title,
                                body: notificationData.content,
                                sound: 'notification.wav',
                            },
                        } }, (imageUrl
                        ? {
                            fcm_options: {
                                image: imageUrl,
                            },
                        }
                        : {})),
                },
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            });
            return {
                ok: true,
                recipients: 1,
                metadata: result.data,
            };
        }
        catch (e) {
            return {
                ok: false,
                recipients: 0,
                error: e.response.data.error,
            };
        }
    }
    async getNotificationStats(id, targetApp) {
        return Promise.resolve({
            audience: 0,
            failed: 0,
            successful: 0,
            clicks: 0,
        });
    }
    async getAccessToken() {
        return new Promise((resolve, reject) => {
            const jwtClient = new google_auth_library_1.JWT(undefined, this.serviceAccountLocation, undefined, ['https://www.googleapis.com/auth/firebase.messaging'], undefined);
            jwtClient.authorize(function (err, tokens) {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(tokens === null || tokens === void 0 ? void 0 : tokens.access_token);
            });
        });
    }
}
exports.default = FCMNotificationService;
//# sourceMappingURL=FCM-notification-service.js.map