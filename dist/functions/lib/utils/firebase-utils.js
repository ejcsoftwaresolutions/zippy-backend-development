"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FirebaseUtils = {
    getDate(firebaseDate) {
        return new Date(firebaseDate.seconds * 1000);
    },
};
exports.default = FirebaseUtils;
//# sourceMappingURL=firebase-utils.js.map