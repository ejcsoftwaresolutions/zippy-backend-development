export const __esModule: boolean;
export default CloudFunctionsAppUtils;
declare namespace CloudFunctionsAppUtils {
    function findAccount(id: any): Promise<any>;
    function findCustomer(id: any): Promise<any>;
    function findVendor(id: any): Promise<any>;
    function findRider(id: any): Promise<any>;
    function findPushTokens(userId: any): Promise<any>;
    function getOrder(id: any): Promise<any>;
    function findUserNotificationInfo(userId: any, role: any): Promise<{
        pushToken: any;
        email: any;
        configurations: any;
    }>;
    function sendEmail(to: any, message: any): Promise<any[]>;
    function sendPushNotification(pushNotification: any, role: any, userId: any): Promise<void | {
        ok: boolean;
        recipients: number;
        metadata: any;
        error?: undefined;
    } | {
        ok: boolean;
        recipients: number;
        error: any;
        metadata?: undefined;
    }>;
    function createId(): Promise<any>;
    function syncVendorGeolocation(vendorId: any, location: any): Promise<any>;
    function findAdminAccount(id: any): Promise<any>;
    function getAppDeliveryServiceConfigurations(): Promise<any>;
    function omitUnknown(object: any): Partial<any>;
}
