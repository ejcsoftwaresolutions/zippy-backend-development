"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.usdAbbr = exports.usdFormat = exports.bsAbbr = exports.bsFormat = void 0;
const number_utils_1 = require("./number-utils");
const bsFormat = (value) => {
    return `Bs.D ${number_utils_1.default.format(value, '0.0[,]')}`;
};
exports.bsFormat = bsFormat;
exports.bsAbbr = 'Bs.S';
const usdFormat = (value) => {
    return `$${number_utils_1.default.format(value, '0.0[,]')}`;
};
exports.usdFormat = usdFormat;
exports.usdAbbr = 'USD';
//# sourceMappingURL=currency-utils.js.map