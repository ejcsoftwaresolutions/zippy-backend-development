"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FirestoreValue = void 0;
const firebase_admin_1 = require("firebase-admin");
var FieldValue = firebase_admin_1.firestore.FieldValue;
exports.FirestoreValue = FieldValue;
class FirestoreApiRepository {
    constructor() {
        this.db = firebase_admin_1.default.firestore();
    }
    get firestore() {
        return this.db;
    }
    async getDoc(collectionName, docId) {
        const result = await this.db.doc(`${collectionName}/${docId}`).get();
        const data = result.data();
        return data;
    }
    async updateDoc(collectionName, docId, data) {
        await this.db.doc(`${collectionName}/${docId}`).update(data);
    }
    getDocsRef(collectionName) {
        return this.db.collection(`${collectionName}`);
    }
    async deleteDoc(collectionName, docId) {
        await this.db.doc(`${collectionName}/${docId}`).delete();
    }
    async setDoc(collectionName, docId, data) {
        await this.db.doc(`${collectionName}/${docId}`).set(data);
    }
    async saveDoc(collectionName, docId, data) {
        await this.db.doc(`${collectionName}/${docId}`).set(data);
    }
    async deleteDocField(collectionName, docId, field) {
        await this.updateDoc(collectionName, docId, {
            [field]: firebase_admin_1.default.firestore.FieldValue.delete(),
        });
    }
    async getDocs(collectionName, params) {
        const inFilter = params.filters.find((f) => f.operator == 'in');
        const normalFilters = params.filters.filter((f) => f.operator !== 'in');
        if (!inFilter) {
            return this.getDocsData(collectionName, {
                filters: normalFilters,
            });
        }
        const queryChunks = this.getChunks(inFilter.value, 10);
        const chunksResults = queryChunks.map(async (chunk) => {
            let ref = this.getDocsWithFiltersRef(collectionName, {
                filters: normalFilters,
            });
            ref = ref.where(inFilter.field, 'in', chunk);
            const result = await ref.get();
            const chunkDtos = (await result.docs).map((d) => d.data());
            return chunkDtos;
        });
        const results = await Promise.all(chunksResults);
        return results === null || results === void 0 ? void 0 : results.flat();
    }
    async saveInBatch(collectionName, docs) {
        const batch = this.db.batch();
        docs.forEach((doc) => {
            batch.set(this.db.collection(collectionName).doc(doc.id), doc);
        });
        await batch.commit();
    }
    getChunks(inputArray, perChunk) {
        const result = inputArray.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perChunk);
            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = [];
            }
            resultArray[chunkIndex].push(item);
            return resultArray;
        }, []);
        return result;
    }
    async getDocsData(collectionName, params) {
        const ref = this.getDocsWithFiltersRef(collectionName, params);
        return (await ref.get()).docs.map((doc) => doc.data());
    }
    getDocsWithFiltersRef(collectionName, params) {
        let ref = this.getDocsRef(collectionName);
        params.filters.forEach((filter) => {
            ref = ref.where(filter.field, filter.operator, filter.value);
        });
        if (params.limit) {
            ref = ref.limit(params.limit);
        }
        if (params.orderBy) {
            ref = ref.orderBy(params.orderBy.field, params.orderBy.direction);
        }
        return ref;
    }
}
exports.default = FirestoreApiRepository;
//# sourceMappingURL=firestore-api-repository.js.map