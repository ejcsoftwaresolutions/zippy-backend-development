export const __esModule: boolean;
export default SucasaProductBatcher;
declare class SucasaProductBatcher {
    constructor(productImporter: any, sellerInfo: any);
    productImporter: any;
    sellerInfo: any;
    fetcher: sucasa_product_fetcher_1.default;
    process(categories: any, config: any): Promise<void>;
    processCategory(category: any, filters: any): Promise<void>;
    processPage(page: any, filters: any): Promise<void>;
}
import sucasa_product_fetcher_1 = require("../fetchers/sucasa-product-fetcher");
