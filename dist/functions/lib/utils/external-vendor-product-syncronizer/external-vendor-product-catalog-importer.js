"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vendor_products_repository_1 = require("./repositories/vendor-products-repository");
class ExternalVendorProductCatalogImporter {
    constructor() {
        this.vendorProductRepo = new vendor_products_repository_1.default();
    }
    async syncProducts(sellerId, sellerCategoryId, extraCommissionPercentage, products, mapper) {
        const productMappings = await this.vendorProductRepo.getVendorProductsMappings(sellerId);
        const subcategoriesMappings = await this.vendorProductRepo.getVendorProductSubcategoriesMappings(sellerId);
        if (Object.values(subcategoriesMappings !== null && subcategoriesMappings !== void 0 ? subcategoriesMappings : {}).length === 0)
            throw new Error('NO_SUBCATEGORIES_MAPPINGS_FOUND');
        const existingProducts = await this.vendorProductRepo.getProductsById(Object.values(productMappings !== null && productMappings !== void 0 ? productMappings : {}));
        const appExchangeRate = await this.vendorProductRepo.getAppExchangeRate();
        const mappedProducts = mapper.toDomainFromArray(products, {
            mappings: {
                products: productMappings,
                subcategories: subcategoriesMappings,
                existingProducts: existingProducts.reduce((acc, current) => {
                    return Object.assign(Object.assign({}, acc), { [current.id]: current });
                }, {}),
            },
            sellerId: sellerId,
            categoryId: sellerCategoryId,
            currencyExchange: appExchangeRate,
            extraCommissionPercentage: extraCommissionPercentage,
        });
        await this.vendorProductRepo.saveVendorProducts(mappedProducts);
        const newMappings = mappedProducts.reduce((acc, actual, index) => {
            return Object.assign(Object.assign({}, acc), { [products[index].id]: actual.id });
        }, {});
        await this.vendorProductRepo.saveVendorProductsMappings(sellerId, newMappings);
    }
    async syncSubcategories(sellerId, sellerCategoryId, categories, mapper) {
        const appCategories = await this.vendorProductRepo.getSubcategories(sellerCategoryId);
        const mappedCategories = mapper.mapCategories(categories, appCategories);
        await this.vendorProductRepo.saveVendorSubcategoriesProductsMappings(sellerId, mappedCategories);
    }
    async hasProductsMappings(sellerId) {
        const productMappings = await this.vendorProductRepo.getVendorProductsMappings(sellerId);
        return Object.values(productMappings !== null && productMappings !== void 0 ? productMappings : {}).length > 0;
    }
}
exports.default = ExternalVendorProductCatalogImporter;
//# sourceMappingURL=external-vendor-product-catalog-importer.js.map