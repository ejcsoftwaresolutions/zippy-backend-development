export const __esModule: boolean;
export default VendorProductsRepository;
declare class VendorProductsRepository extends firestore_api_repository_1.default {
    saveVendorProducts(products: any): Promise<void>;
    getProductsById(ids: any): Promise<any>;
    getVendorProductsMappings(sellerId: any): Promise<any>;
    saveVendorProductsMappings(sellerId: any, map: any): Promise<any>;
    getVendorProductSubcategoriesMappings(sellerId: any): Promise<any>;
    saveVendorSubcategoriesProductsMappings(sellerId: any, map: any): Promise<any>;
    getSubcategories(categoryId: any): Promise<any>;
    getAppExchangeRate(): Promise<any>;
}
import firestore_api_repository_1 = require("../../firestore-api-repository");
