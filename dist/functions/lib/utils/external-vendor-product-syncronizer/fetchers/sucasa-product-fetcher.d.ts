export const __esModule: boolean;
export default SucasaProductFetcher;
declare class SucasaProductFetcher {
    static getOnlyUpdatedFilterString(filters: any): string;
    static getOnlyCreatedFilterString(filters: any): string;
    CAT_LIMIT: number;
    PRODUCT_LIMIT: number;
    categoryUrl: string;
    productUrl: string;
    getCategories(): Promise<any[]>;
    getProductsByPage(number: any, filters: any): Promise<any>;
    getTotalProductPages(filters: any): Promise<number | any[]>;
    getCategoryPage(number: any): Promise<any>;
    getTotalCategoriesPages(): Promise<number>;
}
