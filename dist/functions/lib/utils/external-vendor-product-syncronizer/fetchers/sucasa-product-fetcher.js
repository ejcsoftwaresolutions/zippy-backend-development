"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const text_utils_1 = require("../../text-utils");
const isDev = process.env.NODE_ENV != 'production';
class SucasaProductFetcher {
    constructor() {
        this.CAT_LIMIT = 100;
        this.PRODUCT_LIMIT = process.env.NODE_ENV !== 'production' ? 50 : 100;
        const baseUrl = 'https://app.ecwid.com/api/v3/28254021';
        const token = 'public_xsaYDPM4r2HLZZHfSpVHwGhUh28WDcxP';
        this.categoryUrl = `${baseUrl}/categories?token=${token}&lang=es_419&parent=0`;
        this.productUrl = `${baseUrl}/products?token=${token}&lang=es_419&enabled=true`;
    }
    static getOnlyUpdatedFilterString(filters) {
        return filters && filters.updatedFrom
            ? `&updatedFrom=${filters.updatedFrom}&updatedTo=${filters.updatedTo}`
            : '';
    }
    static getOnlyCreatedFilterString(filters) {
        return filters && filters.createdFrom
            ? `&createdFrom=${filters.createdFrom}&createdTo=${filters.createdTo}`
            : '';
    }
    async getCategories() {
        const totalPages = await this.getTotalCategoriesPages();
        const data = (await Promise.all([...new Array(totalPages)].map((x, p) => this.getCategoryPage(p + 1)))).flatMap((res) => res);
        return data
            .filter((c) => {
            return (!c.parentId &&
                ![
                    'restaurante',
                    'empaque y confeccion',
                    'ferrehogar',
                    'hogar',
                    'papeleria',
                    'licores',
                ].includes(c.name
                    .toLowerCase()
                    .normalize('NFD')
                    .replace(/[\u0300-\u036f]/g, '')));
        })
            .map((c) => {
            return Object.assign(Object.assign({}, c), { name: text_utils_1.default.capitalize(c.name.toLowerCase()) });
        });
    }
    async getProductsByPage(number, filters) {
        var _a;
        if (!filters || !filters.categoryId)
            return [];
        const offset = (number - 1) * this.PRODUCT_LIMIT;
        const updatedFilters = SucasaProductFetcher.getOnlyUpdatedFilterString(filters);
        const createdFilters = SucasaProductFetcher.getOnlyCreatedFilterString(filters);
        const res = await axios_1.default.get(`${this.productUrl}&offset=${offset}&limit=${this.PRODUCT_LIMIT}${filters
            ? `&categories=${filters.categoryId}&includeProductsFromSubcategories=true${updatedFilters}${createdFilters}`
            : ''}`);
        const data = (_a = res.data.items) !== null && _a !== void 0 ? _a : [];
        return data
            .filter((p) => {
            if (filters.categoryId.toString() === '120462504') {
                return p.categoryIds
                    .map((id) => id.toString())
                    .some((s) => ['120462505', '120462508', '120462509', '120462512'].includes(s));
            }
            return true;
        })
            .map((p) => {
            return Object.assign(Object.assign({}, p), { defaultCategoryId: filters.categoryId
                    ? filters.categoryId
                    : p.defaultCategoryId });
        });
    }
    async getTotalProductPages(filters) {
        if (!filters || !filters.categoryId)
            return [];
        const updatedFilters = SucasaProductFetcher.getOnlyUpdatedFilterString(filters);
        const createdFilters = SucasaProductFetcher.getOnlyCreatedFilterString(filters);
        const res = await axios_1.default.get(`${this.productUrl}&limit=${this.PRODUCT_LIMIT}${filters
            ? `&categories=${filters.categoryId}&includeProductsFromSubcategories=true${updatedFilters}${createdFilters}`
            : ''}`);
        const totalPages = Math.ceil(res.data.total / this.PRODUCT_LIMIT);
        return totalPages > 0 ? (isDev ? 1 : totalPages) : 0;
    }
    async getCategoryPage(number) {
        var _a;
        const offset = (number - 1) * this.CAT_LIMIT;
        const res = await axios_1.default.get(`${this.categoryUrl}&offset=${offset}&limit=${this.CAT_LIMIT}`);
        const data = (_a = res.data.items) !== null && _a !== void 0 ? _a : [];
        return data;
    }
    async getTotalCategoriesPages() {
        const res = await axios_1.default.get(`${this.categoryUrl}&limit=${this.CAT_LIMIT}`);
        const totalPages = Math.ceil(res.data.total / this.CAT_LIMIT);
        return totalPages;
    }
}
exports.default = SucasaProductFetcher;
//# sourceMappingURL=sucasa-product-fetcher.js.map