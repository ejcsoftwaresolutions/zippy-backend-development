export const __esModule: boolean;
export default ExternalVendorProductCatalogImporter;
declare class ExternalVendorProductCatalogImporter {
    vendorProductRepo: vendor_products_repository_1.default;
    syncProducts(sellerId: any, sellerCategoryId: any, extraCommissionPercentage: any, products: any, mapper: any): Promise<void>;
    syncSubcategories(sellerId: any, sellerCategoryId: any, categories: any, mapper: any): Promise<void>;
    hasProductsMappings(sellerId: any): Promise<boolean>;
}
import vendor_products_repository_1 = require("./repositories/vendor-products-repository");
