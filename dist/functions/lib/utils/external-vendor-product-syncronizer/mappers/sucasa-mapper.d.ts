export const __esModule: boolean;
export default SucasaMapper;
declare class SucasaMapper {
    static toDomainFromArray(dtos: any, config: any): any;
    static mapCategories(categories: any, appCategories: any): any;
    static findCorrespondingCategory(category: any, appCategories: any): any;
}
