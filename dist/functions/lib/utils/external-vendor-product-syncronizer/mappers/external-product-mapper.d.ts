export const __esModule: boolean;
export default ExternalProductMapper;
declare class ExternalProductMapper {
    static toPersistenceFromArray(products: any): any;
    static toPersistence(product: any): Partial<any>;
}
