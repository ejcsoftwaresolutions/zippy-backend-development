export const __esModule: boolean;
export default SellerProduct;
declare class SellerProduct extends model_1.default {
    static fromPrimitives(props: any): SellerProduct;
    static fromNew(props: any): SellerProduct;
    get id(): any;
    get name(): any;
    get isNew(): any;
    get imageUrl(): any;
    get categoryId(): any;
    get subcategoryId(): any;
    get description(): any;
    get stockCount(): any;
    get available(): any;
    get photo(): any;
    toPrimitives(): any;
}
import model_1 = require("../../models/model");
