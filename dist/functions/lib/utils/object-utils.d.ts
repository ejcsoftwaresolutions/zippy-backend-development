export const __esModule: boolean;
export default ObjectUtils;
declare namespace ObjectUtils {
    const get: any;
    const isNull: (value: any) => value is null;
    const omit: {
        <T extends object, K extends lodash_1.PropertyName[]>(object: T, ...paths: K): Pick<T, Exclude<keyof T, K[number]>>;
        <T_1 extends object, K_1 extends keyof T_1>(object: T_1, ...paths: lodash_1.Many<K_1>[]): lodash_1.Omit<T_1, K_1>;
        <T_2 extends object>(object: T_2, ...paths: lodash_1.Many<lodash_1.PropertyName>[]): Partial<T_2>;
    };
    const mapValues: {
        <TResult>(obj: string, callback: lodash_1.StringIterator<TResult>): lodash_1.NumericDictionary<TResult>;
        <T extends object, TResult_1>(obj: T, callback: lodash_1.ObjectIterator<T, TResult_1>): { [P in keyof T]: TResult_1; };
        <T_1>(obj: lodash_1.Dictionary<T_1> | lodash_1.NumericDictionary<T_1>, iteratee: object): lodash_1.Dictionary<boolean>;
        <T_2 extends object>(obj: T_2, iteratee: object): { [P_1 in keyof T_2]: boolean; };
        <T_3, TKey extends keyof T_3>(obj: lodash_1.Dictionary<T_3> | lodash_1.NumericDictionary<T_3>, iteratee: TKey): lodash_1.Dictionary<T_3[TKey]>;
        <T_4>(obj: lodash_1.Dictionary<T_4> | lodash_1.NumericDictionary<T_4>, iteratee: string): lodash_1.Dictionary<any>;
        <T_5 extends object>(obj: T_5, iteratee: string): { [P_2 in keyof T_5]: any; };
        (obj: string): lodash_1.NumericDictionary<string>;
        <T_6>(obj: lodash_1.Dictionary<T_6> | lodash_1.NumericDictionary<T_6>): lodash_1.Dictionary<T_6>;
        <T_7 extends object>(obj: T_7): T_7;
        <T_8 extends object>(obj: T_8): Partial<T_8>;
    };
    function omitUnknown(object: any): Partial<any>;
    const pick: {
        <T extends object, U extends keyof T>(object: T, ...props: lodash_1.Many<U>[]): Pick<T, U>;
        <T_1>(object: T_1, ...props: lodash_1.Many<lodash_1.PropertyPath>[]): Partial<T_1>;
    };
    const merge: {
        <TObject, TSource>(object: TObject, source: TSource): TObject & TSource;
        <TObject_1, TSource1, TSource2>(object: TObject_1, source1: TSource1, source2: TSource2): TObject_1 & TSource1 & TSource2;
        <TObject_2, TSource1_1, TSource2_1, TSource3>(object: TObject_2, source1: TSource1_1, source2: TSource2_1, source3: TSource3): TObject_2 & TSource1_1 & TSource2_1 & TSource3;
        <TObject_3, TSource1_2, TSource2_2, TSource3_1, TSource4>(object: TObject_3, source1: TSource1_2, source2: TSource2_2, source3: TSource3_1, source4: TSource4): TObject_3 & TSource1_2 & TSource2_2 & TSource3_1 & TSource4;
        (object: any, ...otherArgs: any[]): any;
    };
    const isEqual: (value: any, other: any) => boolean;
    function sort(obj: any): {};
}
import lodash_1 = require("lodash");
