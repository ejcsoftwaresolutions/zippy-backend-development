export const __esModule: boolean;
export const bsAbbr: "Bs.S";
export const usdAbbr: "USD";
export function bsFormat(value: any): string;
export function usdFormat(value: any): string;
