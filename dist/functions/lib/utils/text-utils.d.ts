export const __esModule: boolean;
export default TextUtils;
declare namespace TextUtils {
    function truncate(text: any, max: any): any;
    function insertAt(str: any, sub: any, pos: any): string;
    function capitalize(str: any): any;
}
