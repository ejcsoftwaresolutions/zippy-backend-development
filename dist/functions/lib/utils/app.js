"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = require("firebase-admin");
const constants_1 = require("../constants");
const push_notification_1 = require("./models/push-notification");
const lodash_1 = require("lodash");
const id_1 = require("./models/id");
const notification_service_1 = require("./notification-service/notification-service");
const functions = require('firebase-functions');
const geoFire = require('geofire');
const CloudFunctionsAppUtils = {
    async findAccount(id) {
        const item = await firebase_admin_1.default.firestore().doc(`${constants_1.USERS}/${id}`).get();
        const itemDto = item.data();
        if (!itemDto)
            return;
        return itemDto;
    },
    async findCustomer(id) {
        const item = await firebase_admin_1.default.firestore().doc(`${constants_1.CUSTOMERS}/${id}`).get();
        const itemDto = item.data();
        if (!itemDto)
            return;
        return itemDto;
    },
    async findVendor(id) {
        const item = await firebase_admin_1.default.firestore().doc(`${constants_1.VENDORS}/${id}`).get();
        const itemDto = item.data();
        if (!itemDto)
            return;
        return itemDto;
    },
    async findRider(id) {
        const item = await firebase_admin_1.default.firestore().doc(`${constants_1.RIDERS}/${id}`).get();
        const itemDto = item.data();
        if (!itemDto)
            return;
        return itemDto;
    },
    async findPushTokens(userId) {
        const item = await firebase_admin_1.default.firestore().doc(`${constants_1.PUSH_TOKENS}/${userId}`).get();
        const itemDto = item.data();
        if (!itemDto)
            return;
        return itemDto;
    },
    async getOrder(id) {
        var _a;
        const orderQ = await firebase_admin_1.default
            .firestore()
            .collection(constants_1.VENDOR_ORDERS)
            .where('id', '==', id)
            .get();
        const order = (_a = orderQ.docs.map((x) => x.data())) === null || _a === void 0 ? void 0 : _a[0];
        return order;
    },
    async findUserNotificationInfo(userId, role) {
        const pushTokens = await CloudFunctionsAppUtils.findPushTokens(userId);
        const user = await CloudFunctionsAppUtils.findAccount(userId);
        const configurations = await (async () => {
            var _a, _b, _c;
            if (role === 'rider')
                return (_a = (await this.findRider(userId))) === null || _a === void 0 ? void 0 : _a.configurations;
            if (role === 'vendor')
                return (_b = (await this.findVendor(userId))) === null || _b === void 0 ? void 0 : _b.configurations;
            return (_c = (await this.findCustomer(userId))) === null || _c === void 0 ? void 0 : _c.configurations;
        })();
        return {
            pushToken: pushTokens === null || pushTokens === void 0 ? void 0 : pushTokens[role],
            email: user === null || user === void 0 ? void 0 : user.email,
            configurations,
        };
    },
    async sendEmail(to, message) {
        if (process.env.NODE_ENV !== 'production')
            return;
        const emails = to.split(',');
        const promises = emails.map((email) => firebase_admin_1.default.firestore().collection('mail').add({
            to: email.trim(),
            message: message,
        }));
        return Promise.all(promises);
    },
    async sendPushNotification(pushNotification, role, userId) {
        var _a, _b;
        functions.logger.log('Intentando enviar un push notification', {
            pushNotification: pushNotification,
            role,
        });
        const { pushToken, configurations } = await CloudFunctionsAppUtils.findUserNotificationInfo(userId, role.toLowerCase());
        if (!pushToken) {
            functions.logger.log('Push token not found', {
                userId: userId,
                role,
            });
            return Promise.resolve();
        }
        if (!(configurations === null || configurations === void 0 ? void 0 : configurations.receivePushNotifications)) {
            functions.logger.log('Push notifications disabled', {
                userId: userId,
                role,
            });
            return Promise.resolve();
        }
        const appId = (() => {
            if (role === 'RIDER')
                return process.env.RIDER_APP_EXPERIENCE_ID;
            if (role === 'VENDOR')
                return process.env.STORE_APP_EXPERIENCE_ID;
            return process.env.CUSTOMER_APP_EXPERIENCE_ID;
        })();
        if (!appId)
            return Promise.resolve();
        const defaultChannel = appId.split('/')[1];
        const channel = (_a = pushNotification.channel) !== null && _a !== void 0 ? _a : defaultChannel;
        const service = await notification_service_1.NotificationService;
        try {
            return await service.send(new push_notification_1.PushNotification({
                id: new id_1.default().value,
                to: pushToken,
                imageUrl: pushNotification.notification.image,
                metadata: (_b = pushNotification.data) !== null && _b !== void 0 ? _b : {},
                channelId: service.getName() === 'ONE_SIGNAL' ? channel : defaultChannel,
                content: pushNotification.notification.body,
                title: pushNotification.notification.title,
                appId: appId,
            }));
        }
        catch (e) {
            functions.logger.log('Error al enviar push notification', e.response.data.error);
            throw new Error(e);
        }
    },
    async createId() {
        return firebase_admin_1.default.firestore().collection('test').doc().id;
    },
    async syncVendorGeolocation(vendorId, location) {
        const vendorLocationRef = firebase_admin_1.default
            .database()
            .ref(`${constants_1.VENDOR_LOCATIONS}/${vendorId}`);
        const snap = await vendorLocationRef.get();
        if (snap.exists()) {
            return Promise.resolve();
        }
        const geoFireInstance = new geoFire.GeoFire(firebase_admin_1.default.database().ref(constants_1.VENDOR_LOCATIONS));
        return geoFireInstance.set(vendorId, location);
    },
    async findAdminAccount(id) {
        if (id) {
            const item = await firebase_admin_1.default.firestore().collection(`admin`).doc(id).get();
            const itemDto = item.data();
            if (!itemDto)
                return;
            return itemDto;
        }
        const item = await firebase_admin_1.default.firestore().collection(`admin`).limit(1).get();
        const itemDto = item.docs.map((c) => c.data())[0];
        if (!itemDto)
            return;
        return itemDto;
    },
    async getAppDeliveryServiceConfigurations() {
        const configs = await firebase_admin_1.default
            .firestore()
            .collection(`app_configurations`)
            .doc('RIDER_DELIVERY_CONFIGURATIONS')
            .get();
        const itemDto = configs.data();
        if (!itemDto)
            return;
        return itemDto;
    },
    omitUnknown(object) {
        return (0, lodash_1.omitBy)(object, lodash_1.isNil);
    },
};
exports.default = CloudFunctionsAppUtils;
//# sourceMappingURL=app.js.map