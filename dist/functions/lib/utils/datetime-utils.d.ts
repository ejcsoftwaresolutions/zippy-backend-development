export const __esModule: boolean;
export default DateTimeUtils;
declare namespace DateTimeUtils {
    function format(date: any, format: any, utc?: boolean): string;
    function differenceInMinutes(a: any, b: any): number;
    function differenceInMinutesMoment(a: any, b: any): any;
    function fromTime(time: any, format?: string): Date;
}
