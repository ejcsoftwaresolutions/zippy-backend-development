"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminNotificationsController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const notification_service_1 = require("../../../../functions/src/utils/notification-service/notification-service");
class NotificationStatsDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], NotificationStatsDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], NotificationStatsDto.prototype, "targetApp", void 0);
let AdminNotificationsController = class AdminNotificationsController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
        this.notificationService = (0, notification_service_1.createNotificationService)();
    }
    async notificationStats(data) {
        try {
            const res = await this.notificationService.getNotificationStats(data.id, data.targetApp);
            return {
                ...res,
            };
        }
        catch (e) {
            return {
                res: 0,
                message: e.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('notifications/stats'),
    (0, swagger_1.ApiOperation)({
        summary: 'test',
        description: '',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [NotificationStatsDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminNotificationsController.prototype, "notificationStats", null);
AdminNotificationsController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], AdminNotificationsController);
exports.AdminNotificationsController = AdminNotificationsController;
//# sourceMappingURL=admin-notifications-controller.js.map