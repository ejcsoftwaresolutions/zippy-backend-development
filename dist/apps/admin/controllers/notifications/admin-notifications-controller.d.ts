import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class NotificationStatsDto {
    id: string;
    targetApp: any;
}
export declare class AdminNotificationsController extends ApiController {
    private notificationService;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    notificationStats(data: NotificationStatsDto): Promise<{
        successful: number;
        failed: number;
        audience: number;
        clicks: number;
        res?: undefined;
        message?: undefined;
    } | {
        res: number;
        message: any;
    }>;
}
export {};
