"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminLoginController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const authenticate_admin_command_1 = require("../../../../src/main/admin/auth/application/authenticate/authenticate-admin-command");
class AdminCredentialsDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], AdminCredentialsDto.prototype, "email", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], AdminCredentialsDto.prototype, "password", void 0);
let AdminLoginController = class AdminLoginController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async execute(credentials) {
        try {
            const token = await this.dispatch(new authenticate_admin_command_1.default(credentials.email, credentials.password));
            return {
                token: token,
            };
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('/login'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Admin regular authentication',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Iniciar sesion como Admin (Deprecated)',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [AdminCredentialsDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminLoginController.prototype, "execute", null);
AdminLoginController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], AdminLoginController);
exports.AdminLoginController = AdminLoginController;
//# sourceMappingURL=admin.login.controller.js.map