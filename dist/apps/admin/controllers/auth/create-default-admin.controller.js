"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateDefaultAdminController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const firebase_admin_1 = require("firebase-admin");
const utils_1 = require("../../../../src/main/shared/domain/utils");
class UpdateFBUser {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], UpdateFBUser.prototype, "firebaseId", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({
        type: 'object',
        properties: {
            email: {
                type: 'string',
            },
            password: {
                type: 'string',
            },
            phone: {
                type: 'string',
            },
        },
    }),
    tslib_1.__metadata("design:type", Object)
], UpdateFBUser.prototype, "changes", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], UpdateFBUser.prototype, "secret", void 0);
const SECRET = 'P6fllo65l@nS';
let CreateDefaultAdminController = class CreateDefaultAdminController extends api_controller_1.default {
    async execute(credentials) {
        try {
            await firebase_admin_1.default.auth().createUser({
                uid: credentials.id,
                email: 'superadmin@admin.com',
                password: 'zippi*2022',
            });
        }
        catch (error) {
            return {
                ok: false,
            };
        }
    }
    async updateUser(updateUserRequest) {
        var _a, _b;
        if (updateUserRequest.secret !== SECRET) {
            return {
                ok: false,
                msg: "That's not the secret folk! -,-",
            };
        }
        try {
            const changes = updateUserRequest.changes;
            if (!changes.email && !changes.password && !changes.phone) {
                return {
                    ok: false,
                    msg: 'Send email or password or phone number',
                };
            }
            await firebase_admin_1.default.auth().updateUser(updateUserRequest.firebaseId, {
                email: changes.email,
                password: changes.password,
                phoneNumber: changes.phone,
            });
            const userRef = firebase_admin_1.default
                .firestore()
                .collection('users')
                .doc(updateUserRequest.firebaseId);
            const user = (_a = (await userRef.get())) === null || _a === void 0 ? void 0 : _a.data();
            const finalChanges = utils_1.ObjectUtils.omitUnknown(utils_1.ObjectUtils.pick(changes, ['email', 'phone']));
            await userRef.update({
                ...finalChanges,
                hasDefaultPassword: false,
                newPasswordToken: '',
            });
            if ((_b = user === null || user === void 0 ? void 0 : user.roles) === null || _b === void 0 ? void 0 : _b.includes('RIDER')) {
                await firebase_admin_1.default
                    .firestore()
                    .collection('riders')
                    .doc(user.id)
                    .update(finalChanges);
            }
            return {
                ok: true,
            };
        }
        catch (error) {
            return {
                ok: false,
                msg: error.message,
            };
        }
    }
    async syncVendorTotalEarnings() {
        try {
            return {
                ok: true,
            };
        }
        catch (error) {
            return {
                ok: false,
                msg: error.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('/auth/default'),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], CreateDefaultAdminController.prototype, "execute", null);
tslib_1.__decorate([
    (0, common_1.Post)('/update-firebase-user'),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [UpdateFBUser]),
    tslib_1.__metadata("design:returntype", Promise)
], CreateDefaultAdminController.prototype, "updateUser", null);
tslib_1.__decorate([
    (0, common_1.Post)('/sync-vendor-total-earnings'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Promise)
], CreateDefaultAdminController.prototype, "syncVendorTotalEarnings", null);
CreateDefaultAdminController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin',
    })
], CreateDefaultAdminController);
exports.CreateDefaultAdminController = CreateDefaultAdminController;
//# sourceMappingURL=create-default-admin.controller.js.map