import ApiController from '@shared/infrastructure/controller/api-controller';
declare class UpdateFBUser {
    firebaseId: string;
    changes: {
        email?: string;
        password?: string;
        phone?: string;
    };
    secret: string;
}
export declare class CreateDefaultAdminController extends ApiController {
    execute(credentials: {
        id: string;
    }): Promise<any>;
    updateUser(updateUserRequest: UpdateFBUser): Promise<any>;
    syncVendorTotalEarnings(): Promise<any>;
}
export {};
