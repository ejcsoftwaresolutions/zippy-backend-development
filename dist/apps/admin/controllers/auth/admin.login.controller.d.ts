import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class AdminCredentialsDto {
    email: string;
    password: string;
}
export declare class AdminLoginController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    execute(credentials: AdminCredentialsDto): Promise<{
        token: string;
    }>;
}
export {};
