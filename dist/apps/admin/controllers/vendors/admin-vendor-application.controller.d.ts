import ApiController from '@shared/infrastructure/controller/api-controller';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
export declare class AdminVendorApplicationController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    downloadDocument(id: string, key: string, res: any): Promise<void>;
}
