"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminVendorRejectController = void 0;
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const approve_vendor_command_1 = require("../../../../src/main/vendors/authetication/application/approve-vendor/approve-vendor-command");
const firebase_admin_1 = require("firebase-admin");
class VendorRejectDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], VendorRejectDto.prototype, "userId", void 0);
let AdminVendorRejectController = class AdminVendorRejectController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async reject(body) {
        try {
            const res = await this.dispatch(new approve_vendor_command_1.default({
                id: body.userId,
            }));
            await firebase_admin_1.default
                .firestore()
                .collection('vendor_applications')
                .doc(body.userId)
                .update({ state: 'REJECTED', rejectedAt: new Date() });
            const userData = await this.findUser(body.userId);
            if (!userData)
                return {
                    ok: true,
                };
            try {
                await this.sendEmail({
                    user: userData
                });
            }
            catch (e) {
                console.log("No se pudo enviar el correo!");
            }
            return {
                id: res.id,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async findUser(id) {
        var _a;
        const userS = await firebase_admin_1.default
            .firestore()
            .collection('users')
            .where('id', '==', id)
            .get();
        const user = !userS.empty ? (_a = userS.docs.shift()) === null || _a === void 0 ? void 0 : _a.data() : undefined;
        return user;
    }
    async sendEmail({ user }) {
        var _a;
        await firebase_admin_1.default
            .firestore()
            .collection('mail')
            .add({
            to: user.email,
            message: {
                subject: `Zippi - Cuenta rechazada`,
                text: `Tu solicitud ha sido rechazada.`,
                html: `
                  <div>
                       <p>
                         Malas noticias ${(_a = user.firstName) === null || _a === void 0 ? void 0 : _a.trim()}, tu cuenta no pudo ser aprobada.
                      </p>
                    <br/>
                  </div>
                `,
            },
        });
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('reject'),
    (0, swagger_1.ApiOperation)({
        summary: 'Reject a store',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [VendorRejectDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminVendorRejectController.prototype, "reject", null);
AdminVendorRejectController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin/vendors',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], AdminVendorRejectController);
exports.AdminVendorRejectController = AdminVendorRejectController;
//# sourceMappingURL=admin-vendor-reject.controller.js.map