import ApiController from '@shared/infrastructure/controller/api-controller';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import EventBus from '@shared/domain/bus/event/event-bus';
declare class VendorExportListExportDto {
    filters?: string;
    limit?: number;
    skip?: number;
    order?: string;
    format: string;
    email: string;
}
export declare class AdminVendorListExportController extends ApiController {
    private eventBus;
    constructor(commandBus: CommandBus, queryBus: QueryBus, eventBus: EventBus, multipartHandler: MultipartHandler<Request, Response>);
    execute(query: VendorExportListExportDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
}
export {};
