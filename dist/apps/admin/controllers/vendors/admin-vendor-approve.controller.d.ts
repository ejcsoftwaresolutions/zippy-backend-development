import ApiController from '@shared/infrastructure/controller/api-controller';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import { ConfigService } from '@nestjs/config';
declare class VendorApproveDto {
    userId: string;
}
export declare class AdminVendorApproveController extends ApiController {
    private configService;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>, configService: ConfigService);
    approve(body: VendorApproveDto): Promise<{
        ok: boolean;
        id?: undefined;
        error?: undefined;
    } | {
        id: any;
        ok?: undefined;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
        id?: undefined;
    }>;
    private findUser;
    private sendEmail;
}
export {};
