"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRolesController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const criteria_1 = require("../../../../src/main/shared/domain/criteria/criteria");
const filters_1 = require("../../../../src/main/shared/domain/criteria/filters");
const order_1 = require("../../../../src/main/shared/domain/criteria/order");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const id_1 = require("../../../../src/main/shared/domain/id/id");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const collection_1 = require("../../../../src/main/shared/domain/value-object/collection");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const admin_role_infrastructure_command_repository_1 = require("../../../../src/main/admin/auth/infrastructure/persistance/fireorm/repositories/admin-role-infrastructure-command-repository");
class AdminRoleDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], AdminRoleDto.prototype, "name", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Array)
], AdminRoleDto.prototype, "permissions", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Boolean)
], AdminRoleDto.prototype, "isSuperAdmin", void 0);
let AdminRolesController = class AdminRolesController extends api_controller_1.default {
    constructor(commandBus, queryBus, repo, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
        this.repo = repo;
    }
    async list(params) {
        try {
            const order = params['filter[order]'];
            const limit = params['filter[limit]'];
            const skip = params['filter[skip]'];
            const where = params['filter[where]'];
            const criteria = params
                ? new criteria_1.default({
                    order: new collection_1.default(order
                        ? [order_1.default.fromValues(order.split(' ')[0], order.split(' ')[1])]
                        : []),
                    limit: limit ? parseInt(limit) : undefined,
                    offset: skip ? parseInt(skip) : undefined,
                    filters: filters_1.default.fromValues(where !== null && where !== void 0 ? where : {}),
                })
                : undefined;
            return await this.repo.getAll(criteria);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async getTotal(data) {
        try {
            return {
                count: await this.repo.count(),
            };
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async get(id) {
        try {
            return await this.repo.get(id);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async create(data) {
        try {
            const id = new id_1.default().value;
            const newEntity = { ...data, id: id };
            await this.repo.create(newEntity);
            return newEntity;
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async edit(id, data) {
        try {
            return await this.repo.updateRole(id, data);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async delete(id) {
        try {
            return await this.repo.delete(id);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'List',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'List',
    }),
    tslib_1.__param(0, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRolesController.prototype, "list", null);
tslib_1.__decorate([
    (0, common_1.Get)('/count'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Get count',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Total',
    }),
    tslib_1.__param(0, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRolesController.prototype, "getTotal", null);
tslib_1.__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Get',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get',
    }),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRolesController.prototype, "get", null);
tslib_1.__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Create',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [AdminRoleDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRolesController.prototype, "create", null);
tslib_1.__decorate([
    (0, common_1.Patch)(':id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Update',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update',
    }),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, AdminRoleDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRolesController.prototype, "edit", null);
tslib_1.__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'delete',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'delete',
    }),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRolesController.prototype, "delete", null);
AdminRolesController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin/admin-roles',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('AdminRoleCommandRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, admin_role_infrastructure_command_repository_1.default, Object])
], AdminRolesController);
exports.AdminRolesController = AdminRolesController;
//# sourceMappingURL=admin-roles-controller.js.map