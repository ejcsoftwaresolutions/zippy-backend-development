import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import AdminRoleCommandInfrastructureRepository from '@admin/auth/infrastructure/persistance/fireorm/repositories/admin-role-infrastructure-command-repository';
declare class AdminRoleDto {
    name: string;
    permissions: string[];
    isSuperAdmin: boolean;
}
export declare class AdminRolesController extends ApiController {
    private repo;
    constructor(commandBus: CommandBus, queryBus: QueryBus, repo: AdminRoleCommandInfrastructureRepository, multipartHandler: MultipartHandler<Request, Response>);
    list(params: any): Promise<any>;
    getTotal(data: any): Promise<any>;
    get(id: string): Promise<any>;
    create(data: AdminRoleDto): Promise<any>;
    edit(id: string, data: AdminRoleDto): Promise<any>;
    delete(id: string): Promise<any>;
}
export {};
