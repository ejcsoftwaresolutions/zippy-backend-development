"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRiderListExportController = void 0;
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const event_bus_1 = require("../../../../src/main/shared/domain/bus/event/event-bus");
const export_rider_list_requested_domain_event_1 = require("../../../../src/main/admin/riders/domain/events/export-rider-list-requested-domain-event");
class RiderListExportDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: false }),
    tslib_1.__metadata("design:type", String)
], RiderListExportDto.prototype, "filters", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: false, type: 'number' }),
    tslib_1.__metadata("design:type", Number)
], RiderListExportDto.prototype, "limit", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: false, type: 'number' }),
    tslib_1.__metadata("design:type", Number)
], RiderListExportDto.prototype, "skip", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: false }),
    tslib_1.__metadata("design:type", String)
], RiderListExportDto.prototype, "order", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: true }),
    tslib_1.__metadata("design:type", String)
], RiderListExportDto.prototype, "format", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: true }),
    tslib_1.__metadata("design:type", String)
], RiderListExportDto.prototype, "email", void 0);
let AdminRiderListExportController = class AdminRiderListExportController extends api_controller_1.default {
    constructor(commandBus, queryBus, eventBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
        this.eventBus = eventBus;
    }
    async execute(query) {
        this.eventBus.publish([
            new export_rider_list_requested_domain_event_1.default({
                eventData: {
                    format: query.format,
                    email: query.email,
                },
                aggregateId: new Date().toUTCString(),
                occurredOn: new Date(),
            }),
        ]);
        try {
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('export'),
    (0, swagger_1.ApiOperation)({
        summary: 'Export riders in pdf/excel',
    }),
    tslib_1.__param(0, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [RiderListExportDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRiderListExportController.prototype, "execute", null);
AdminRiderListExportController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin/riders',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('event.bus')),
    tslib_1.__param(3, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object])
], AdminRiderListExportController);
exports.AdminRiderListExportController = AdminRiderListExportController;
//# sourceMappingURL=admin-rider-list-export.controller.js.map