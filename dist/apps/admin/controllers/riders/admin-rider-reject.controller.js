"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRiderRejectController = void 0;
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const firebase_admin_1 = require("firebase-admin");
class RiderRejectDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], RiderRejectDto.prototype, "userId", void 0);
let AdminRiderRejectController = class AdminRiderRejectController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async reject(body) {
        try {
            await firebase_admin_1.default
                .firestore()
                .collection('rider_applications')
                .doc(body.userId)
                .update({ state: 'REJECTED', rejectedAt: new Date() });
            const userData = await this.findUser(body.userId);
            if (!userData)
                return {
                    ok: true,
                };
            try {
                await this.sendEmail({
                    user: userData
                });
            }
            catch (e) {
                console.log("No se pudo enviar el correo!");
            }
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async findUser(id) {
        var _a;
        const userS = await firebase_admin_1.default
            .firestore()
            .collection('users')
            .where('id', '==', id)
            .get();
        const user = !userS.empty ? (_a = userS.docs.shift()) === null || _a === void 0 ? void 0 : _a.data() : undefined;
        return user;
    }
    async sendEmail({ user }) {
        var _a;
        await firebase_admin_1.default
            .firestore()
            .collection('mail')
            .add({
            to: user.email,
            message: {
                subject: `Zippi - Cuenta rechazada`,
                text: `Tu solicitud ha sido rechazada.`,
                html: `
                  <div>
                       <p>
                         Malas noticias ${(_a = user.firstName) === null || _a === void 0 ? void 0 : _a.trim()}, tu cuenta no pudo ser aprobada.
                      </p>
                    <br/>
                  </div>
                `,
            },
        });
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('reject'),
    (0, swagger_1.ApiOperation)({
        summary: 'Reject a new rider',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [RiderRejectDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRiderRejectController.prototype, "reject", null);
AdminRiderRejectController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin/riders',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], AdminRiderRejectController);
exports.AdminRiderRejectController = AdminRiderRejectController;
//# sourceMappingURL=admin-rider-reject.controller.js.map