import ApiController from '@shared/infrastructure/controller/api-controller';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import { ConfigService } from '@nestjs/config';
declare class RiderApproveDto {
    userId: string;
}
export declare class AdminRiderApproveController extends ApiController {
    private configService;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>, configService: ConfigService);
    approve(body: RiderApproveDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
    private findUser;
    private sendEmail;
}
export {};
