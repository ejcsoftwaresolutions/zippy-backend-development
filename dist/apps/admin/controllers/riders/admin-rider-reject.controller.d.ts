import ApiController from '@shared/infrastructure/controller/api-controller';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
declare class RiderRejectDto {
    userId: string;
}
export declare class AdminRiderRejectController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    reject(body: RiderRejectDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
    private findUser;
    private sendEmail;
}
export {};
