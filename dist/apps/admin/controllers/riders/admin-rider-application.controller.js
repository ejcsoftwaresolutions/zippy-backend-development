"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRiderApplicationController = void 0;
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const firebase_admin_1 = require("firebase-admin");
const axios_1 = require("axios");
let AdminRiderApplicationController = class AdminRiderApplicationController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async downloadDocument(id, key, res) {
        const applicationRef = await firebase_admin_1.default
            .firestore()
            .doc(`rider_applications/${id}`)
            .get();
        if (!applicationRef.exists) {
            res.status(404).send("User application not found");
            return;
        }
        const application = applicationRef.data();
        let documentUrl = application.documents[key];
        if (key === "personalReferencesUrl1") {
            documentUrl = application.documents.personalReferencesUrl[0];
        }
        if (key === "personalReferencesUrl2") {
            documentUrl = application.documents.personalReferencesUrl[1];
        }
        await this.sendResponse({
            application,
            documentKey: key,
            documentUrl: documentUrl,
            res
        });
    }
    async sendResponse({ documentUrl, documentKey, application, res }) {
        if (!documentUrl) {
            res.status(404).send("Document doesn't exist");
            return;
        }
        const ext = documentUrl.split(".").pop().split("?")[0];
        const response = await (0, axios_1.default)({
            url: documentUrl,
            method: 'GET',
            responseType: 'stream',
        });
        const fullName = `${application.firstName}_${application.lastName}`;
        res.header('Content-disposition', `attachment; filename=${fullName}-${documentKey.replace("Url", "")}.${ext}`);
        res.send(response.data);
    }
};
tslib_1.__decorate([
    (0, common_1.Get)('application/:id/document/:key'),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__param(1, (0, common_1.Param)('key')),
    tslib_1.__param(2, (0, common_1.Res)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, String, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRiderApplicationController.prototype, "downloadDocument", null);
AdminRiderApplicationController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin/riders',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], AdminRiderApplicationController);
exports.AdminRiderApplicationController = AdminRiderApplicationController;
//# sourceMappingURL=admin-rider-application.controller.js.map