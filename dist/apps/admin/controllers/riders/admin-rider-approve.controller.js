"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRiderApproveController = void 0;
const tslib_1 = require("tslib");
const swagger_1 = require("@nestjs/swagger");
const common_1 = require("@nestjs/common");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const approve_rider_command_1 = require("../../../../src/main/riders/authentication/application/approve-rider/approve-rider-command");
const firebase_admin_1 = require("firebase-admin");
const config_1 = require("@nestjs/config");
const USERS = 'users';
class RiderApproveDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], RiderApproveDto.prototype, "userId", void 0);
let AdminRiderApproveController = class AdminRiderApproveController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler, configService) {
        super(commandBus, queryBus, multipartHandler);
        this.configService = configService;
    }
    async approve(body) {
        try {
            const res = await this.dispatch(new approve_rider_command_1.default({
                id: body.userId,
            }));
            await firebase_admin_1.default
                .firestore()
                .collection('rider_applications')
                .doc(body.userId)
                .update({
                state: 'APPROVED',
                rejectedAt: null,
                approvedAt: new Date(),
            });
            const userData = await this.findUser(body.userId);
            if (!userData)
                return {
                    ok: true,
                };
            try {
                await this.sendEmail({
                    user: userData,
                });
            }
            catch (e) {
                console.log('No se pudo enviar el correo!');
            }
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async findUser(id) {
        var _a, _b;
        const userS = await firebase_admin_1.default
            .firestore()
            .collection(USERS)
            .where('id', '==', id)
            .get();
        const user = !userS.empty ? (_a = userS.docs.shift()) === null || _a === void 0 ? void 0 : _a.data() : {};
        const riderS = await firebase_admin_1.default
            .firestore()
            .collection(USERS)
            .where('id', '==', id)
            .get();
        const rider = !riderS.empty ? (_b = riderS.docs.shift()) === null || _b === void 0 ? void 0 : _b.data() : {};
        return { ...user, identificationCard: rider.identificationCard };
    }
    async sendEmail({ user, }) {
        var _a;
        const resetPasswordUrl = `${this.configService.get('WEBSITE_URL')}/new-password/${user.newPasswordToken}`;
        await firebase_admin_1.default
            .firestore()
            .collection('mail')
            .add({
            to: user.email,
            message: {
                subject: `Zippi - Cuenta aprobada`,
                text: `Buenas noticias!. Tu solicitud ha sido aprobada. ¡Ya puedes comenzar a utilizar Zippi!.`,
                html: `
                  <div>
                       <p>
                         Buenas noticias, ${(_a = user.firstName) === null || _a === void 0 ? void 0 : _a.trim()}!. Tu cuenta ha sido aprobada. ¡Ya puedes comenzar a utilizar Zippi!.
                         Tu clave inicial es  <strong>${user.identificationCard}</strong> para cambiarla ingresa <a href="${resetPasswordUrl}">aquí</a>
                      </p>
                    <br/>
                  </div>
                `,
            },
        });
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('approve'),
    (0, swagger_1.ApiOperation)({
        summary: 'Approve a new rider',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [RiderApproveDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminRiderApproveController.prototype, "approve", null);
AdminRiderApproveController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin/riders',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, config_1.ConfigService])
], AdminRiderApproveController);
exports.AdminRiderApproveController = AdminRiderApproveController;
//# sourceMappingURL=admin-rider-approve.controller.js.map