import AuthenticatedUser from '@apps/shared/dto/authenticated-user';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import PasswordHasher from '@shared/domain/utils/password-hasher';
import ApiController from '@shared/infrastructure/controller/api-controller';
import AuthAdminInfrastructureCommandRepository from '@admin/auth/infrastructure/persistance/fireorm/repositories/auth-admin-infrastructure-command-repository';
declare class ObjectDto {
    firstName: string;
    lastName: string;
    email: string;
    roleId: string;
    password: string;
}
export declare class AdminAccountsController extends ApiController {
    private repo;
    private passwordHasher;
    constructor(commandBus: CommandBus, queryBus: QueryBus, repo: AuthAdminInfrastructureCommandRepository, multipartHandler: MultipartHandler<Request, Response>, passwordHasher: PasswordHasher);
    list(user: AuthenticatedUser, params: any): Promise<any>;
    getTotal(user: AuthenticatedUser, data: any): Promise<any>;
    get(id: string): Promise<any>;
    create(data: ObjectDto): Promise<any>;
    edit(id: string, data: ObjectDto): Promise<any>;
    delete(id: string): Promise<any>;
}
export {};
