"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminAccountsController = void 0;
const tslib_1 = require("tslib");
const admin_auth_user_1 = require("../../../../src/main/admin/auth/domain/models/admin-auth-user");
const user_decorator_1 = require("../../../shared/decorators/user-decorator");
const authenticated_user_1 = require("../../../shared/dto/authenticated-user");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const criteria_1 = require("../../../../src/main/shared/domain/criteria/criteria");
const filters_1 = require("../../../../src/main/shared/domain/criteria/filters");
const order_1 = require("../../../../src/main/shared/domain/criteria/order");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const id_1 = require("../../../../src/main/shared/domain/id/id");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const password_hasher_1 = require("../../../../src/main/shared/domain/utils/password-hasher");
const collection_1 = require("../../../../src/main/shared/domain/value-object/collection");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const auth_admin_infrastructure_command_repository_1 = require("../../../../src/main/admin/auth/infrastructure/persistance/fireorm/repositories/auth-admin-infrastructure-command-repository");
class ObjectDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ObjectDto.prototype, "firstName", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ObjectDto.prototype, "lastName", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ObjectDto.prototype, "email", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ObjectDto.prototype, "roleId", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ObjectDto.prototype, "password", void 0);
let AdminAccountsController = class AdminAccountsController extends api_controller_1.default {
    constructor(commandBus, queryBus, repo, multipartHandler, passwordHasher) {
        super(commandBus, queryBus, multipartHandler);
        this.repo = repo;
        this.passwordHasher = passwordHasher;
    }
    async list(user, params) {
        try {
            const order = params['filter[order]'];
            const limit = params['filter[limit]'];
            const skip = params['filter[skip]'];
            const where = params['filter[where]'];
            const criteria = params
                ? new criteria_1.default({
                    order: new collection_1.default(order
                        ? [order_1.default.fromValues(order.split(' ')[0], order.split(' ')[1])]
                        : []),
                    limit: limit ? parseInt(limit) : undefined,
                    offset: skip ? parseInt(skip) : undefined,
                    filters: filters_1.default.fromValues({
                        ...(where !== null && where !== void 0 ? where : {}),
                        id: { $ne: user.id },
                    }),
                })
                : undefined;
            return await this.repo.getAll(criteria);
        }
        catch (error) {
            console.log(error);
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async getTotal(user, data) {
        try {
            return {
                count: await this.repo.count({
                    ...(data !== null && data !== void 0 ? data : {}),
                    id: { $ne: user.id },
                }),
            };
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async get(id) {
        try {
            return await this.repo.get(id);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async create(data) {
        try {
            const id = new id_1.default();
            const newEntity = { ...data, id: id };
            const hashedPassword = await this.passwordHasher.hashPassword(newEntity.password);
            await this.repo.register(admin_auth_user_1.default.create({
                ...newEntity,
                credentials: {
                    password: hashedPassword,
                    email: newEntity.email,
                },
            }));
            return { ...newEntity, id: id.value };
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async edit(id, data) {
        try {
            const admin = await this.repo.get(id);
            if (!admin)
                return;
            return await this.repo.updateUser(admin_auth_user_1.default.fromPrimitives({
                id: admin.id.value,
                ...data,
            }));
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async delete(id) {
        try {
            return await this.repo.delete(id);
        }
        catch (error) {
            throw new common_1.HttpException({
                status: common_1.HttpStatus.INTERNAL_SERVER_ERROR,
                error: error.message,
            }, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Get)(),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'List',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'List',
    }),
    tslib_1.__param(0, (0, user_decorator_1.User)()),
    tslib_1.__param(1, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [authenticated_user_1.default, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminAccountsController.prototype, "list", null);
tslib_1.__decorate([
    (0, common_1.Get)('/count'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Get count',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Total',
    }),
    tslib_1.__param(0, (0, user_decorator_1.User)()),
    tslib_1.__param(1, (0, common_1.Query)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [authenticated_user_1.default, Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminAccountsController.prototype, "getTotal", null);
tslib_1.__decorate([
    (0, common_1.Get)(':id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Get',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Get',
    }),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminAccountsController.prototype, "get", null);
tslib_1.__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Create',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Create',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [ObjectDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminAccountsController.prototype, "create", null);
tslib_1.__decorate([
    (0, common_1.Patch)(':id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'Update',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'Update',
    }),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, ObjectDto]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminAccountsController.prototype, "edit", null);
tslib_1.__decorate([
    (0, common_1.Delete)(':id'),
    (0, swagger_1.ApiResponse)({
        status: 200,
        description: 'delete',
    }),
    (0, swagger_1.ApiOperation)({
        summary: 'delete',
    }),
    tslib_1.__param(0, (0, common_1.Param)('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], AdminAccountsController.prototype, "delete", null);
AdminAccountsController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Admin'),
    (0, common_1.Controller)({
        path: 'admin/accounts',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('AuthAdminCommandRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__param(4, (0, decorators_1.inject)('utils.passwordHasher')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, auth_admin_infrastructure_command_repository_1.default, Object, Object])
], AdminAccountsController);
exports.AdminAccountsController = AdminAccountsController;
//# sourceMappingURL=admin-accounts-controller.js.map