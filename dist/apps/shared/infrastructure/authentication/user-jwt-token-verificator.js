"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const auth_token_verificator_1 = require("../../../../src/main/shared/domain/authentication/auth-token-verificator");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const jwt_base_verificator_1 = require("../../../../src/main/shared/infrastructure/authentication/jwt-base-verificator");
const authenticated_user_1 = require("../../dto/authenticated-user");
const jwt_decode_1 = require("jwt-decode");
let UserJWTVerificator = class UserJWTVerificator extends jwt_base_verificator_1.JWTBaseVerificator {
    constructor(jwtSecret) {
        super(jwtSecret);
    }
    async verify(params) {
        if (!params.userType) {
            throw new common_1.UnauthorizedException('INVALID_TOKEN');
        }
        const user = await this.searchUser(params);
        if (!user) {
            throw new common_1.UnauthorizedException('INVALID_USER');
        }
        return authenticated_user_1.default.fromPrimitives(user);
    }
    decodeToken(token) {
        return (0, jwt_decode_1.default)(token);
    }
    async searchUser(params) {
        throw new Error('Not implemented');
    }
};
UserJWTVerificator = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('authentication.token.secret')),
    tslib_1.__metadata("design:paramtypes", [String])
], UserJWTVerificator);
exports.default = UserJWTVerificator;
//# sourceMappingURL=user-jwt-token-verificator.js.map