"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const auth_token_creator_1 = require("../../../../src/main/shared/domain/authentication/auth-token-creator");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const jwt_base_creator_1 = require("../../../../src/main/shared/infrastructure/authentication/jwt-base-creator");
let UserJWTCreator = class UserJWTCreator extends jwt_base_creator_1.JWTBaseCreator {
    constructor(jwtSecret, jwtExpiresIn) {
        super(jwtSecret, jwtExpiresIn);
    }
    async generate(data) {
        return this.generateToken(data);
    }
};
UserJWTCreator = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('authentication.token.secret')),
    tslib_1.__param(1, (0, decorators_1.inject)('authentication.token.expiresIn')),
    tslib_1.__metadata("design:paramtypes", [String, String])
], UserJWTCreator);
exports.default = UserJWTCreator;
//# sourceMappingURL=user-jwt-token-creator.js.map