import TokenCreator from '@shared/domain/authentication/auth-token-creator';
import { JWTBaseCreator } from '@shared/infrastructure/authentication/jwt-base-creator';
import AuthenticatedUser from '../../dto/authenticated-user';
export default class UserJWTCreator extends JWTBaseCreator implements TokenCreator<AuthenticatedUser> {
    constructor(jwtSecret: string, jwtExpiresIn: string);
    generate(data: any): Promise<string>;
}
