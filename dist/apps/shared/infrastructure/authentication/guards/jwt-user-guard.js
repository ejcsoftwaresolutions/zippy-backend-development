"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtUserGuard = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
let JwtUserGuard = class JwtUserGuard extends (0, passport_1.AuthGuard)('jwt') {
};
JwtUserGuard = tslib_1.__decorate([
    (0, common_1.Injectable)()
], JwtUserGuard);
exports.JwtUserGuard = JwtUserGuard;
//# sourceMappingURL=jwt-user-guard.js.map