import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import { JWTBaseVerificator } from '@shared/infrastructure/authentication/jwt-base-verificator';
import AuthenticatedUser from '../../dto/authenticated-user';
export default class UserJWTVerificator extends JWTBaseVerificator implements AuthTokenVerificator<AuthenticatedUser> {
    constructor(jwtSecret: string);
    verify(params: any): Promise<AuthenticatedUser>;
    decodeToken(token: string): Promise<any>;
    private searchUser;
}
