import SocialMediaProfile from '@shared/infrastructure/authentication/social-media-profile';
export default function getGooglePublicProfile(accessToken: string): Promise<SocialMediaProfile | null>;
