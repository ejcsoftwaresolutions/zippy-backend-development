"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const social_media_profile_1 = require("../../../../../src/main/shared/infrastructure/authentication/social-media-profile");
const axios_1 = require("axios");
async function getFacebookProfile(accessToken) {
    const response = await axios_1.default.get('https://graph.facebook.com/v9.0/me/', {
        params: {
            access_token: accessToken,
            fields: 'id,first_name,last_name,email,picture.width(640)',
            redirect: false,
        },
    });
    const profile = response.data;
    return {
        id: profile.id,
        firstName: profile.first_name,
        lastName: profile.last_name,
        fullName: undefined,
        pictureUrl: profile.picture.data.url,
        email: profile.email,
    };
}
exports.default = getFacebookProfile;
//# sourceMappingURL=get-facebook-public-profile.js.map