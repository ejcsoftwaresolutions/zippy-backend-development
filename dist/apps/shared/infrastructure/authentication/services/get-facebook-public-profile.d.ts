import SocialMediaProfile from '@shared/infrastructure/authentication/social-media-profile';
export default function getFacebookProfile(accessToken: string): Promise<SocialMediaProfile | null>;
