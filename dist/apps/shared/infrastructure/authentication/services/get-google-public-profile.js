"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const social_media_profile_1 = require("../../../../../src/main/shared/infrastructure/authentication/social-media-profile");
const axios_1 = require("axios");
async function getGooglePublicProfile(accessToken) {
    const response = await axios_1.default.get('https://www.googleapis.com/oauth2/v1/userinfo', {
        params: {
            alt: 'json',
            access_token: accessToken,
        },
    });
    const profile = response.data;
    return {
        id: profile.id,
        email: profile.email,
        firstName: profile.given_name,
        lastName: profile.family_name,
        fullName: undefined,
        pictureUrl: profile.picture + '-s640',
    };
}
exports.default = getGooglePublicProfile;
//# sourceMappingURL=get-google-public-profile.js.map