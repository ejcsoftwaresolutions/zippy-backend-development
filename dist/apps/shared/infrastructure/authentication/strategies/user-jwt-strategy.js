"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserJwtStrategy = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const auth_token_verificator_1 = require("../../../../../src/main/shared/domain/authentication/auth-token-verificator");
const decorators_1 = require("../../../../../src/main/shared/domain/decorators");
const passport_jwt_1 = require("passport-jwt");
let UserJwtStrategy = class UserJwtStrategy extends (0, passport_1.PassportStrategy)(passport_jwt_1.Strategy) {
    constructor(tokenService, jwtSecret) {
        super({
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: jwtSecret,
        });
        this.tokenService = tokenService;
    }
    async validate(payload) {
        const userProfile = await this.tokenService.verify(payload);
        return userProfile;
    }
};
UserJwtStrategy = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__param(0, (0, decorators_1.inject)('user.authentication.token.verificator')),
    tslib_1.__param(1, (0, decorators_1.inject)('authentication.token.secret')),
    tslib_1.__metadata("design:paramtypes", [Object, String])
], UserJwtStrategy);
exports.UserJwtStrategy = UserJwtStrategy;
//# sourceMappingURL=user-jwt-strategy.js.map