import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import AuthenticatedUser from '../../../dto/authenticated-user';
declare const UserJwtStrategy_base: new (...args: any[]) => any;
export declare class UserJwtStrategy extends UserJwtStrategy_base {
    tokenService: AuthTokenVerificator<AuthenticatedUser>;
    constructor(tokenService: AuthTokenVerificator<AuthenticatedUser>, jwtSecret: string);
    validate(payload: any): Promise<AuthenticatedUser>;
}
export {};
