interface AuthenticatedUserProps {
    id: string;
    email: string;
    profilePictureUrl?: string;
}
export interface AuthenticatedUserPrimitiveProps {
    id: string;
    email: string;
    profilePictureUrl?: string;
}
export default class AuthenticatedUser {
    private props;
    constructor(props: AuthenticatedUserProps);
    get id(): string;
    get email(): string;
    get profilePictureUrl(): string;
    static fromPrimitives(props: AuthenticatedUserPrimitiveProps): AuthenticatedUser;
    toPrimitives(): AuthenticatedUserProps;
}
export {};
