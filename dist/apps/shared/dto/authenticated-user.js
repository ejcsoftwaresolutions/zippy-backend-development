"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AuthenticatedUser {
    constructor(props) {
        this.props = props;
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.email;
    }
    get profilePictureUrl() {
        return this.props.profilePictureUrl;
    }
    static fromPrimitives(props) {
        return new AuthenticatedUser(props);
    }
    toPrimitives() {
        return this.props;
    }
}
exports.default = AuthenticatedUser;
//# sourceMappingURL=authenticated-user.js.map