"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerAuthController = void 0;
const tslib_1 = require("tslib");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const common_1 = require("@nestjs/common");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const swagger_1 = require("@nestjs/swagger");
const firebase_admin_1 = require("firebase-admin");
class CustomerLinkEmailDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CustomerLinkEmailDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], CustomerLinkEmailDto.prototype, "credentials", void 0);
class DeletePhoneUserDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], DeletePhoneUserDto.prototype, "id", void 0);
let CustomerAuthController = class CustomerAuthController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async linkEmail(body) {
        try {
            await firebase_admin_1.default.auth().updateUser(body.id, {
                email: body.credentials.email,
                password: body.credentials.password,
            });
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('link-email'),
    (0, swagger_1.ApiOperation)({
        summary: 'Link email and password to customer auth',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [CustomerLinkEmailDto]),
    tslib_1.__metadata("design:returntype", Promise)
], CustomerAuthController.prototype, "linkEmail", null);
CustomerAuthController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Customer'),
    (0, common_1.Controller)({
        path: 'customer/auth',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], CustomerAuthController);
exports.CustomerAuthController = CustomerAuthController;
//# sourceMappingURL=customer.auth.controller.js.map