import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class CustomerLinkEmailDto {
    id: string;
    credentials: {
        email: string;
        password: string;
    };
}
export declare class CustomerAuthController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    linkEmail(body: CustomerLinkEmailDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
}
export {};
