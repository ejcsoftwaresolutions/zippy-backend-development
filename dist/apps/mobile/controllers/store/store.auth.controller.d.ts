import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class StoreRegisterDto {
    id: string;
    shopName: string;
    basicInfo: {
        email: string;
        firstName: string;
        lastName: string;
        phone: string;
        birthday?: string;
        linePhone?: string;
        contactCompanyCharge?: string;
        identificationCard?: string;
    };
    social: {
        website?: string;
        instagram: string;
    };
    linkAccountToken?: string;
}
export declare class StoreAuthController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    register(body: StoreRegisterDto): Promise<{
        id: any;
        ok?: undefined;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
        id?: undefined;
    }>;
    categories(): Promise<{
        categories: {
            id: any;
            name: any;
            imageUrl: any;
        }[];
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
        categories?: undefined;
    }>;
}
export {};
