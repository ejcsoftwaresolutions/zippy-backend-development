"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StoreServiceController = void 0;
const tslib_1 = require("tslib");
const cancel_delivery_request_command_1 = require("../../../../src/main/delivery-service/store/application/cancel-delivery-request/cancel-delivery-request-command");
const request_delivery_command_1 = require("../../../../src/main/delivery-service/store/application/request-delivery/request-delivery-command");
const store_public_profile_query_repository_1 = require("../../../../src/main/delivery-service/store/domain/repositories/store-public-profile-query-repository");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const firebase_admin_1 = require("firebase-admin");
var FieldValue = firebase_admin_1.firestore.FieldValue;
const ORDER_COLLECTION = 'orders';
const DELIVERY_COLLECTION = 'delivery_orders';
const DRIVER_LOGS_COLLECTION = 'driver_delivery_activity_logs';
const BUSY_RIDERS = 'busy_riders';
const RIDERS_ACTIVE_ORDERS = 'riders_active_orders';
class SearchOrderRiderDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], SearchOrderRiderDto.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Array)
], SearchOrderRiderDto.prototype, "preCandidates", void 0);
class CancelPassengerRequestDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CancelPassengerRequestDto.prototype, "requestId", void 0);
class ResetDeliveryDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ResetDeliveryDto.prototype, "orderId", void 0);
let StoreServiceController = class StoreServiceController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler, storePublicProfileRepo) {
        super(commandBus, queryBus, multipartHandler);
        this.storePublicProfileRepo = storePublicProfileRepo;
    }
    async searchOrderRider(data) {
        var _a, _b, _c;
        try {
            const orderRef = firebase_admin_1.default
                .firestore()
                .collection(ORDER_COLLECTION)
                .doc(data.id);
            const vendorOrderQ = await orderRef.get();
            const vendorOrder = vendorOrderQ.data();
            if (vendorOrder.deliveryMethod === 'PICK_UP') {
                return {
                    ok: true,
                    message: 'pickup request',
                };
            }
            const deliveryOrder = {
                id: vendorOrder.id,
                items: vendorOrder.products.map((p) => ({
                    id: p.id,
                    name: p.name,
                    quantity: p.quantity,
                    unitPrice: p.price,
                })),
                paymentMethod: 'CASH',
                storeId: vendorOrder.vendorID,
                subtotal: vendorOrder.subtotal,
                total: vendorOrder.total,
                deliveryFee: (_b = (_a = vendorOrder === null || vendorOrder === void 0 ? void 0 : vendorOrder.fees) === null || _a === void 0 ? void 0 : _a.delivery) !== null && _b !== void 0 ? _b : vendorOrder.total - vendorOrder.subtotal,
                date: vendorOrder.createdAt,
                code: (_c = vendorOrder.code) !== null && _c !== void 0 ? _c : vendorOrder.id,
            };
            const originPoint = {
                geoLocation: [
                    parseFloat(vendorOrder.shipAddr.location.latitude),
                    parseFloat(vendorOrder.shipAddr.location.longitude),
                ],
                address: vendorOrder.shipAddr.formattedAddress,
                referencePoint: vendorOrder.shipAddr.referencePoint,
            };
            const { id } = await this.dispatch(new request_delivery_command_1.default({
                preCandidates: data.preCandidates,
                customerId: vendorOrder.customerID,
                customerLocation: originPoint,
                order: {
                    ...deliveryOrder,
                    date: deliveryOrder.date
                        ? new Date(deliveryOrder.date.seconds * 1000)
                        : new Date(),
                },
                mode: 'live',
                serviceType: 'CAR',
            }));
            return {
                ok: true,
                id: id,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async cancelRequest(body) {
        try {
            await this.dispatch(new cancel_delivery_request_command_1.default(body.requestId));
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async resetDelivery(body) {
        try {
            await firebase_admin_1.default.firestore().runTransaction(async (transaction) => {
                const driverLogsRef = firebase_admin_1.default
                    .firestore()
                    .collection(DRIVER_LOGS_COLLECTION)
                    .where('vendorOrderId', '==', body.orderId);
                const driverLogRefs = (await transaction.get(driverLogsRef)).docs.map((d) => {
                    return d.ref;
                });
                const orderRef = firebase_admin_1.default
                    .firestore()
                    .collection(ORDER_COLLECTION)
                    .doc(body.orderId);
                const order = (await transaction.get(orderRef)).data();
                if (!order)
                    return;
                const driverId = order.driverID;
                await transaction.update(orderRef, {
                    status: 'Order Accepted',
                    driver: FieldValue.delete(),
                    driverID: FieldValue.delete(),
                });
                const deliveryRef = firebase_admin_1.default
                    .firestore()
                    .collection(DELIVERY_COLLECTION)
                    .doc(body.orderId);
                await transaction.delete(deliveryRef);
                driverLogRefs.forEach((ref) => {
                    transaction.delete(ref);
                });
                await firebase_admin_1.default
                    .database()
                    .ref(`${RIDERS_ACTIVE_ORDERS}/${driverId}/${body.orderId}`)
                    .remove();
                await firebase_admin_1.default.database().ref(`${BUSY_RIDERS}/${driverId}`).remove();
            });
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('search-order-rider'),
    (0, swagger_1.ApiOperation)({
        summary: 'Creates an attempt delivery order to find a near rider',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [SearchOrderRiderDto]),
    tslib_1.__metadata("design:returntype", Promise)
], StoreServiceController.prototype, "searchOrderRider", null);
tslib_1.__decorate([
    (0, common_1.Post)('cancel-request'),
    (0, swagger_1.ApiOperation)({
        summary: 'Cancels an active delivery request (Rider search)',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [CancelPassengerRequestDto]),
    tslib_1.__metadata("design:returntype", Promise)
], StoreServiceController.prototype, "cancelRequest", null);
tslib_1.__decorate([
    (0, common_1.Post)('reset-delivery'),
    (0, swagger_1.ApiOperation)({
        summary: 'Resets active order delivery taken by a rider (Puts order back to Order Accepted)',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [ResetDeliveryDto]),
    tslib_1.__metadata("design:returntype", Promise)
], StoreServiceController.prototype, "resetDelivery", null);
StoreServiceController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Store'),
    (0, common_1.Controller)({
        path: 'store/delivery',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__param(3, (0, decorators_1.inject)('StorePublicProfileQueryRepository')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object])
], StoreServiceController);
exports.StoreServiceController = StoreServiceController;
//# sourceMappingURL=store-service.controller.js.map