import StorePublicProfileQueryRepository from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class SearchOrderRiderDto {
    id: string;
    preCandidates?: string[];
}
declare class CancelPassengerRequestDto {
    requestId: string;
}
declare class ResetDeliveryDto {
    orderId: string;
}
export declare class StoreServiceController extends ApiController {
    private storePublicProfileRepo;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>, storePublicProfileRepo: StorePublicProfileQueryRepository);
    searchOrderRider(data: SearchOrderRiderDto): Promise<{
        ok: boolean;
        message: string;
        id?: undefined;
        error?: undefined;
    } | {
        ok: boolean;
        id: any;
        message?: undefined;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
        message?: undefined;
        id?: undefined;
    }>;
    cancelRequest(body: CancelPassengerRequestDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
    resetDelivery(body: ResetDeliveryDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
}
export {};
