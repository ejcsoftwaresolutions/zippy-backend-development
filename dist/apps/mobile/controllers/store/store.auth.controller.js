"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StoreAuthController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const firebase_admin_1 = require("firebase-admin");
const register_vendor_command_1 = require("../../../../src/main/vendors/authetication/application/register-vendor/register-vendor-command");
class StoreRegisterDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: true }),
    tslib_1.__metadata("design:type", String)
], StoreRegisterDto.prototype, "shopName", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: true }),
    tslib_1.__metadata("design:type", Object)
], StoreRegisterDto.prototype, "basicInfo", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: true }),
    tslib_1.__metadata("design:type", Object)
], StoreRegisterDto.prototype, "social", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], StoreRegisterDto.prototype, "linkAccountToken", void 0);
let StoreAuthController = class StoreAuthController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async register(body) {
        try {
            const res = await this.dispatch(new register_vendor_command_1.default({
                id: body.id,
                social: body.social,
                basicInfo: {
                    linePhone: body.basicInfo.linePhone,
                    contactCompanyCharge: body.basicInfo.contactCompanyCharge,
                    lastName: body.basicInfo.lastName,
                    firstName: body.basicInfo.firstName,
                    phone: body.basicInfo.phone,
                    identificationCard: body.basicInfo.identificationCard,
                    birthday: body.basicInfo.birthday
                        ? new Date(body.basicInfo.birthday)
                        : undefined,
                    email: body.basicInfo.email.toLowerCase(),
                },
                shopName: body.shopName,
                linkAccountToken: body.linkAccountToken,
            }));
            return {
                id: res.id,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async categories() {
        try {
            const db = firebase_admin_1.default.firestore();
            const query = db
                .collection(`vendor_categories`)
                .where('parentID', '==', '');
            const result = await query.get();
            const categoriesDto = (await result.docs).map((d) => d.data());
            return {
                categories: categoriesDto.map((item) => {
                    return {
                        id: item.id,
                        name: item.title,
                        imageUrl: item.photo,
                    };
                }),
                ok: true,
            };
        }
        catch (error) {
            return {
                ok: false,
                error: error.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('register'),
    (0, swagger_1.ApiOperation)({
        summary: 'Register a store',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [StoreRegisterDto]),
    tslib_1.__metadata("design:returntype", Promise)
], StoreAuthController.prototype, "register", null);
tslib_1.__decorate([
    (0, common_1.Get)('categories'),
    (0, swagger_1.ApiOperation)({
        summary: 'store categories',
    }),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Promise)
], StoreAuthController.prototype, "categories", null);
StoreAuthController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Store'),
    (0, common_1.Controller)({
        path: 'store/auth',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], StoreAuthController);
exports.StoreAuthController = StoreAuthController;
//# sourceMappingURL=store.auth.controller.js.map