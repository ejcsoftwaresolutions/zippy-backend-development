import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import MobilePushNotificationService from '@shared/infrastructure/push-notifications';
export declare class MigrationServiceController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>, notificationService: MobilePushNotificationService);
    recalculateProducts(data: any): Promise<{
        ok: boolean;
        message?: undefined;
    } | {
        message: any;
        ok: boolean;
    }>;
    private update;
    private getChunks;
}
