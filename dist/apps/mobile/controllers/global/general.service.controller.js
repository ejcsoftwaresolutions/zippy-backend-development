"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GeneralServiceController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const firebase_admin_1 = require("firebase-admin");
class CountDocsDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CountDocsDto.prototype, "collection", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], CountDocsDto.prototype, "filters", void 0);
let GeneralServiceController = class GeneralServiceController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async docCounter(data) {
        try {
            let query = firebase_admin_1.default.firestore().collection(data.collection);
            if (data.filters.length === 0) {
                const res = await query.count().get();
                return {
                    count: res.data().count,
                    ok: true,
                };
            }
            data.filters.forEach((filter) => {
                query = query.where(filter.field, filter.operator, filter.value);
            });
            const res = await query.count().get();
            return {
                count: res.data().count,
                ok: true,
            };
        }
        catch (e) {
            return {
                count: 0,
                message: e.message,
                ok: false,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('doc-counter'),
    (0, swagger_1.ApiOperation)({
        summary: 'test',
        description: '',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [CountDocsDto]),
    tslib_1.__metadata("design:returntype", Promise)
], GeneralServiceController.prototype, "docCounter", null);
GeneralServiceController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('General'),
    (0, common_1.Controller)({
        path: 'general',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], GeneralServiceController);
exports.GeneralServiceController = GeneralServiceController;
//# sourceMappingURL=general.service.controller.js.map