"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExternalVendorProductSyncronizerController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const firebase_admin_1 = require("firebase-admin");
const firebase_admin_2 = require("firebase-admin");
const external_vendor_product_catalog_importer_1 = require("../../../../functions/src/utils/external-vendor-product-syncronizer/external-vendor-product-catalog-importer");
const sucasa_product_fetcher_1 = require("../../../../functions/src/utils/external-vendor-product-syncronizer/fetchers/sucasa-product-fetcher");
const sucasa_mapper_1 = require("../../../../functions/src/utils/external-vendor-product-syncronizer/mappers/sucasa-mapper");
const sucasa_product_batcher_1 = require("../../../../functions/src/utils/external-vendor-product-syncronizer/batchers/sucasa-product-batcher");
class RemoveProductsDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], RemoveProductsDto.prototype, "vendorId", void 0);
class SyncSucasaDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], SyncSucasaDto.prototype, "mode", void 0);
let ExternalVendorProductSyncronizerController = class ExternalVendorProductSyncronizerController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
        this.productImporter = new external_vendor_product_catalog_importer_1.default();
    }
    async sucasaProductSync(data) {
        try {
            const isDev = process.env.NODE_ENV != 'production';
            const sellerId = isDev ? '2HvDlZbhgSSKAefzQv32' : 'y7EHAW9EDROgcRSSrKYk';
            const vendor = (await firebase_admin_2.default.firestore().collection('vendors').doc(sellerId).get()).data();
            if (!vendor)
                return;
            const sellerCategoryId = vendor.categoryID;
            const extraCommissionPercentage = vendor.salesCommissionPercentage;
            const fetcher = new sucasa_product_fetcher_1.default();
            const categories = await fetcher.getCategories();
            await this.productImporter.syncSubcategories(sellerId, sellerCategoryId, categories, sucasa_mapper_1.default);
            const start = new Date();
            start.setUTCHours(0, 0, 0, 0);
            const end = new Date();
            end.setUTCHours(23, 59, 59, 999);
            const batcher = new sucasa_product_batcher_1.default(this.productImporter, {
                sellerId: sellerId,
                sellerCategoryId: sellerCategoryId,
                extraCommissionPercentage,
            });
            const hasMappings = await this.productImporter.hasProductsMappings(sellerId);
            const mode = (() => {
                if (!data.mode)
                    return !hasMappings ? 'FULL-SYNC' : 'PARTIAL-SYNC';
                if (['FULL-SYNC', 'PARTIAL-SYNC'].includes(data.mode))
                    return data.mode;
                return 'PARTIAL-SYNC';
            })();
            await batcher.process(categories, {
                mode: mode,
            });
            return {
                ok: true,
            };
        }
        catch (e) {
            return {
                message: e.message,
                ok: false,
            };
        }
    }
    async removeExternalVendorProducts(data) {
        try {
            const ref = await firebase_admin_1.default.firestore().collection('vendor_products');
            const products = await ref.where('vendorID', '==', data.vendorId).get();
            const chunks = this.getChunks(products.docs, 500);
            for (const chunk of chunks) {
                const batch = firebase_admin_1.default.firestore().batch();
                chunk.forEach((d) => {
                    batch.delete(d.ref);
                });
                await batch.commit();
            }
            await firebase_admin_1.default
                .database()
                .ref(`external_vendor_product_mappings/${data.vendorId}`)
                .set(null);
            return {
                ok: true,
            };
        }
        catch (e) {
            return {
                message: e.message,
                ok: false,
            };
        }
    }
    getChunks(inputArray, perChunk) {
        const result = inputArray.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perChunk);
            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = [];
            }
            resultArray[chunkIndex].push(item);
            return resultArray;
        }, []);
        return result;
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('sucasa-syncronize'),
    (0, swagger_1.ApiOperation)({
        summary: 'test',
        description: '',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [SyncSucasaDto]),
    tslib_1.__metadata("design:returntype", Promise)
], ExternalVendorProductSyncronizerController.prototype, "sucasaProductSync", null);
tslib_1.__decorate([
    (0, common_1.Post)('remove-external-vendor-products'),
    (0, swagger_1.ApiOperation)({
        summary: 'test',
        description: '',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [RemoveProductsDto]),
    tslib_1.__metadata("design:returntype", Promise)
], ExternalVendorProductSyncronizerController.prototype, "removeExternalVendorProducts", null);
ExternalVendorProductSyncronizerController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('General'),
    (0, common_1.Controller)({
        path: 'general',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], ExternalVendorProductSyncronizerController);
exports.ExternalVendorProductSyncronizerController = ExternalVendorProductSyncronizerController;
//# sourceMappingURL=external-vendor-product-syncronizer.controller.js.map