"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MigrationServiceController = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const push_notifications_1 = require("../../../../src/main/shared/infrastructure/push-notifications");
const firebase_admin_1 = require("firebase-admin");
const firebase_admin_2 = require("firebase-admin");
const recalculate_product_1 = require("../../../../functions/src/utils/vendor-products/recalculate-product");
let MigrationServiceController = class MigrationServiceController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler, notificationService) {
        super(commandBus, queryBus, multipartHandler);
    }
    async recalculateProducts(data) {
        let query = await firebase_admin_1.default.firestore()
            .collection("vendor_products")
            .get();
        const docs = await query.docs;
        const finalDocs = docs.map(e => e.data());
        const updates = finalDocs.map((doc) => {
            return {
                id: doc.id, updates: (0, recalculate_product_1.default)(doc)
            };
        }).filter(f => !!f.updates);
        try {
            await this.update(updates);
        }
        catch (e) {
            console.log(e);
        }
        try {
            return {
                ok: true,
            };
        }
        catch (e) {
            return {
                message: e.message, ok: false,
            };
        }
    }
    async update(docs) {
        const ref = firebase_admin_1.default.firestore().collection("vendor_products");
        const chunks = this.getChunks(docs, 500);
        for (const chunk of chunks) {
            const batch = firebase_admin_2.default.firestore().batch();
            chunk.forEach((v) => {
                const sfRef = ref.doc(v.id);
                batch.update(sfRef, v.updates);
            });
            await batch.commit();
        }
    }
    getChunks(inputArray, perChunk) {
        const result = inputArray.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perChunk);
            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = [];
            }
            resultArray[chunkIndex].push(item);
            return resultArray;
        }, []);
        return result;
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('recalculate-products'),
    (0, swagger_1.ApiOperation)({
        summary: 'migration', description: '',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], MigrationServiceController.prototype, "recalculateProducts", null);
MigrationServiceController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Migration'),
    (0, common_1.Controller)({
        path: 'migration',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__param(3, (0, decorators_1.inject)('push.notification.service')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, push_notifications_1.default])
], MigrationServiceController);
exports.MigrationServiceController = MigrationServiceController;
//# sourceMappingURL=migration.service.controller.js.map