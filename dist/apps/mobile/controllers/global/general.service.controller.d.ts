import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class CountDocsDto {
    collection: string;
    filters: any;
}
export declare class GeneralServiceController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    docCounter(data: CountDocsDto): Promise<{
        count: any;
        ok: boolean;
        message?: undefined;
    } | {
        count: number;
        message: any;
        ok: boolean;
    }>;
}
export {};
