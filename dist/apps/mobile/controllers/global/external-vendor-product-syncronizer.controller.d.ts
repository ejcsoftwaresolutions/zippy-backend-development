import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class RemoveProductsDto {
    vendorId: string;
}
declare class SyncSucasaDto {
    mode?: string;
}
export declare class ExternalVendorProductSyncronizerController extends ApiController {
    private productImporter;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    sucasaProductSync(data: SyncSucasaDto): Promise<{
        ok: boolean;
        message?: undefined;
    } | {
        message: any;
        ok: boolean;
    }>;
    removeExternalVendorProducts(data: RemoveProductsDto): Promise<{
        ok: boolean;
        message?: undefined;
    } | {
        message: any;
        ok: boolean;
    }>;
    private getChunks;
}
export {};
