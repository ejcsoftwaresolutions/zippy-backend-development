"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderServiceController = void 0;
const tslib_1 = require("tslib");
const delivery_request_command_repository_1 = require("../../../../src/main/delivery-service/delivery/domain/repositories/delivery-request-command-repository");
const test_delivery_requests_command_repository_1 = require("../../../../src/main/delivery-service/delivery/domain/repositories/test-delivery-requests-command-repository");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const accept_delivery_request_command_1 = require("../../../../src/main/delivery-service/rider/application/accept-delivery-request/accept-delivery-request-command");
const reject_delivery_request_command_1 = require("../../../../src/main/delivery-service/rider/application/reject-delivery-request/reject-delivery-request-command");
const assign_delivery_order_command_1 = require("../../../../src/main/delivery-service/rider/application/assign-delivery-order/assign-delivery-order-command");
class DeliveryRequestDriverResponseDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], DeliveryRequestDriverResponseDto.prototype, "driverId", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], DeliveryRequestDriverResponseDto.prototype, "requestId", void 0);
let RiderServiceController = class RiderServiceController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler, deliveryRequestRepository, testDeliveryRequestsRepository) {
        super(commandBus, queryBus, multipartHandler);
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.testDeliveryRequestsRepository = testDeliveryRequestsRepository;
    }
    async acceptRequest(body) {
        try {
            await this.dispatch(new accept_delivery_request_command_1.default(body.driverId, body.requestId));
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async assignRequest(body) {
        try {
            await this.dispatch(new assign_delivery_order_command_1.default(body.driverId, body.requestId));
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async rejectRequest(body) {
        try {
            await this.dispatch(new reject_delivery_request_command_1.default(body.driverId, body.requestId));
            const res = new Promise((resolve) => {
                setTimeout(() => {
                    resolve({});
                }, 2000);
            });
            await res;
            return {
                ok: true,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('request/accept'),
    (0, swagger_1.ApiOperation)({
        summary: 'Accept a delivery order request',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [DeliveryRequestDriverResponseDto]),
    tslib_1.__metadata("design:returntype", Promise)
], RiderServiceController.prototype, "acceptRequest", null);
tslib_1.__decorate([
    (0, common_1.Post)('request/assign'),
    (0, swagger_1.ApiOperation)({
        summary: 'Assign a delivery order request to a rider',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [DeliveryRequestDriverResponseDto]),
    tslib_1.__metadata("design:returntype", Promise)
], RiderServiceController.prototype, "assignRequest", null);
tslib_1.__decorate([
    (0, common_1.Post)('request/reject'),
    (0, swagger_1.ApiOperation)({
        summary: 'Reject a delivery order request (Discard current candidate)',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [DeliveryRequestDriverResponseDto]),
    tslib_1.__metadata("design:returntype", Promise)
], RiderServiceController.prototype, "rejectRequest", null);
RiderServiceController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Rider'),
    (0, common_1.Controller)({
        path: 'rider/delivery',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__param(3, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(4, (0, decorators_1.inject)('TestDeliveryRequestsCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object])
], RiderServiceController);
exports.RiderServiceController = RiderServiceController;
//# sourceMappingURL=rider-service.controller.js.map