"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderAuthController = void 0;
const tslib_1 = require("tslib");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const common_1 = require("@nestjs/common");
const register_rider_command_1 = require("../../../../src/main/riders/authentication/application/register-rider/register-rider-command");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const swagger_1 = require("@nestjs/swagger");
class RiderRegisterDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], RiderRegisterDto.prototype, "basicInfo", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], RiderRegisterDto.prototype, "referrerCode", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], RiderRegisterDto.prototype, "documents", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], RiderRegisterDto.prototype, "vehicle", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], RiderRegisterDto.prototype, "deliveryRegion", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], RiderRegisterDto.prototype, "withdrawalDetails", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], RiderRegisterDto.prototype, "availableSchedule", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], RiderRegisterDto.prototype, "linkAccountToken", void 0);
let RiderAuthController = class RiderAuthController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler) {
        super(commandBus, queryBus, multipartHandler);
    }
    async register(body) {
        try {
            const files = {
                drivingPermitUrl: body.documents.drivingPermitUrl,
                personalReferencesUrl: body.documents.personalReferencesUrl,
                driverLicenseFrontUrl: body.documents.driverLicenseFrontUrl,
                driverLicenseBackUrl: body.documents.driverLicenseBackUrl,
                medicalCertificateUrl: body.documents.medicalCertificateUrl,
                RCVpolicyUrl: body.documents.RCVpolicyUrl,
                criminalRecordUrl: body.documents.criminalRecordUrl,
                rifUrl: body.documents.rifUrl,
                dniUrl: body.documents.dniUrl,
            };
            const res = await this.dispatch(new register_rider_command_1.default({
                id: body.id,
                basicInfo: {
                    ...body.basicInfo,
                    email: body.basicInfo.email.toLowerCase(),
                    birthday: body.basicInfo.birthday
                        ? new Date(body.basicInfo.birthday)
                        : undefined,
                    homeAddress: {
                        longitude: body.basicInfo.homeAddress.longitude,
                        latitude: body.basicInfo.homeAddress.latitude,
                        line1: body.basicInfo.homeAddress.line1,
                        line2: body.basicInfo.homeAddress.line2,
                        address: body.basicInfo.homeAddress.formattedAddress,
                        state: body.basicInfo.homeAddress.state,
                        city: body.basicInfo.homeAddress.city,
                        country: body.basicInfo.homeAddress.country,
                    },
                },
                referrerCode: body.referrerCode,
                availableSchedule: body.availableSchedule,
                withdrawalDetails: body.withdrawalDetails,
                deliveryRegion: body.deliveryRegion,
                vehicle: {
                    type: body.vehicle.type,
                    model: body.vehicle.model,
                    comments: body.vehicle.comments,
                    year: body.vehicle.year,
                    hasBag: body.vehicle.hasBag,
                },
                documents: {
                    ...files,
                },
                linkAccountToken: body.linkAccountToken,
            }));
            return {
                id: res.id,
            };
        }
        catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('register'),
    (0, swagger_1.ApiOperation)({
        summary: 'Register a new rider',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [RiderRegisterDto]),
    tslib_1.__metadata("design:returntype", Promise)
], RiderAuthController.prototype, "register", null);
RiderAuthController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Rider'),
    (0, common_1.Controller)({
        path: 'rider/auth',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], RiderAuthController);
exports.RiderAuthController = RiderAuthController;
//# sourceMappingURL=rider.auth.controller.js.map