import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class RiderRegisterDto {
    id: string;
    basicInfo: {
        identificationCard: string;
        email: string;
        firstName: string;
        lastName: string;
        phone: string;
        birthday?: string;
        rif: string;
        homeAddress: {
            formattedAddress: string;
            line1: string;
            line2: string;
            city: string;
            address: string;
            state: string;
            country: string;
            latitude: number;
            longitude: number;
        };
    };
    referrerCode?: string;
    documents: {
        personalReferencesUrl: string[];
        driverLicenseFrontUrl: string;
        driverLicenseBackUrl: string;
        medicalCertificateUrl: string;
        RCVpolicyUrl: string;
        criminalRecordUrl: string;
        rifUrl?: string;
        dniUrl: string;
        drivingPermitUrl: string;
    };
    vehicle: {
        type: string;
        hasBag: boolean;
        year: string;
        model: string;
        comments: string;
    };
    deliveryRegion: {
        state: string;
        city: string;
    };
    withdrawalDetails: {
        accountType: string;
        bank: string;
        accountNumber: string;
    };
    availableSchedule: string;
    linkAccountToken?: string;
}
export declare class RiderAuthController extends ApiController {
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    register(body: RiderRegisterDto): Promise<{
        id: any;
        ok?: undefined;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
        id?: undefined;
    }>;
}
export {};
