import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import TestDeliveryRequestsCommandRepository from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
declare class DeliveryRequestDriverResponseDto {
    driverId: string;
    requestId: string;
}
export declare class RiderServiceController extends ApiController {
    private deliveryRequestRepository;
    private testDeliveryRequestsRepository;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>, deliveryRequestRepository: DeliveryRequestCommandRepository, testDeliveryRequestsRepository: TestDeliveryRequestsCommandRepository);
    acceptRequest(body: DeliveryRequestDriverResponseDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
    assignRequest(body: DeliveryRequestDriverResponseDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
    rejectRequest(body: DeliveryRequestDriverResponseDto): Promise<{
        ok: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
    }>;
}
export {};
