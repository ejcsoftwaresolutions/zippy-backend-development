"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigController = void 0;
const tslib_1 = require("tslib");
const service_configurations_model_1 = require("../../../../src/main/delivery-service/delivery/domain/models/service-configurations-model");
const delivery_request_command_repository_1 = require("../../../../src/main/delivery-service/delivery/domain/repositories/delivery-request-command-repository");
const test_delivery_requests_command_repository_1 = require("../../../../src/main/delivery-service/delivery/domain/repositories/test-delivery-requests-command-repository");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const utils_1 = require("../../../../src/main/shared/domain/utils");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const firebase_admin_1 = require("firebase-admin");
const auth_token_creator_1 = require("../../../../src/main/shared/domain/authentication/auth-token-creator");
const config_1 = require("@nestjs/config");
const axios_1 = require("axios");
const auth_token_verificator_1 = require("../../../../src/main/shared/domain/authentication/auth-token-verificator");
const push_notifications_1 = require("../../../../src/main/shared/infrastructure/push-notifications");
const FeeSchema = {
    type: 'object', properties: {
        baseFee: {
            type: 'object', properties: {
                amount: {
                    type: 'number',
                }, currency: {
                    type: 'string',
                },
            },
        }, durationFee: {
            type: 'object', properties: {
                amount: {
                    type: 'number',
                }, currency: {
                    type: 'string',
                },
            },
        }, kmFee: {
            type: 'object', properties: {
                amount: {
                    type: 'number',
                }, currency: {
                    type: 'string',
                },
            },
        }, distanceRange: {
            type: 'object', properties: {
                min: {
                    type: 'number',
                },
            },
        },
    },
};
const FeeConfigurationsSchema = {
    type: 'object', properties: {
        CAR: {
            type: 'array', items: FeeSchema,
        }, MOTOR_CYCLE: {
            type: 'array', items: FeeSchema,
        }, BIKE: {
            type: 'array', items: FeeSchema,
        },
    },
};
const SearchConfigSchema = {
    type: 'object', properties: {
        acceptanceTimeout: {
            type: 'number',
        }, attemptMaxCandidates: {
            type: 'number',
        }, attemptSearchRadius: {
            type: 'number',
        }, maxAttempts: {
            type: 'number',
        },
    },
};
const ConfigSchema = {
    type: 'object', properties: {
        feeConfigurations: FeeConfigurationsSchema, searchConfigurations: SearchConfigSchema,
    },
};
class ConfigurationsDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ ...ConfigSchema }),
    tslib_1.__metadata("design:type", Object)
], ConfigurationsDto.prototype, "config", void 0);
class CheckNewPassTokenDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CheckNewPassTokenDto.prototype, "token", void 0);
class CreateNewPassDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CreateNewPassDto.prototype, "token", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CreateNewPassDto.prototype, "password", void 0);
class AccountLinkVerifyDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], AccountLinkVerifyDto.prototype, "email", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], AccountLinkVerifyDto.prototype, "password", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], AccountLinkVerifyDto.prototype, "newRole", void 0);
class TransformApnsTokenDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], TransformApnsTokenDto.prototype, "token", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], TransformApnsTokenDto.prototype, "application", void 0);
let ConfigController = class ConfigController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler, deliveryRequestRepository, testDeliveryRequestsRepository, tokenCreator, tokenVerificator, configService, pushNotificationService) {
        super(commandBus, queryBus, multipartHandler);
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.testDeliveryRequestsRepository = testDeliveryRequestsRepository;
        this.tokenCreator = tokenCreator;
        this.tokenVerificator = tokenVerificator;
        this.configService = configService;
        this.pushNotificationService = pushNotificationService;
    }
    async saveServiceConfig() {
        const defaultServiceConfig = service_configurations_model_1.default.fromPrimitives({
            feeConfigurations: {
                CAR: [{
                        baseFee: {
                            amount: 1, currency: '$',
                        }, distanceRange: {
                            min: 0,
                        }, durationFee: {
                            amount: 0, currency: '$',
                        }, kmFee: {
                            amount: 0.48, currency: '$',
                        },
                    },], MOTOR_CYCLE: [{
                        baseFee: {
                            amount: 1, currency: '$',
                        }, distanceRange: {
                            min: 0,
                        }, durationFee: {
                            amount: 0, currency: '$',
                        }, kmFee: {
                            amount: 0.48, currency: '$',
                        },
                    },], BIKE: [{
                        baseFee: {
                            amount: 1, currency: '$',
                        }, distanceRange: {
                            min: 0,
                        }, durationFee: {
                            amount: 0, currency: '$',
                        }, kmFee: {
                            amount: 0.48, currency: '$',
                        },
                    },],
            }, searchConfigurations: {
                acceptanceTimeout: 60, attemptMaxCandidates: 5, attemptSearchRadius: 10, maxAttempts: 3,
            },
        });
        try {
            await firebase_admin_1.default
                .firestore()
                .doc('app_configurations/RIDER_DELIVERY_CONFIGURATIONS')
                .set(utils_1.ObjectUtils.omitUnknown(defaultServiceConfig.toPrimitives()));
        }
        catch (error) {
            console.log(error);
            return {
                ok: false,
            };
        }
        return {
            ok: true,
        };
    }
    async saveConfig(body) {
        const defaultServiceConfig = service_configurations_model_1.default.fromPrimitives(body.config);
        const current = await firebase_admin_1.default
            .firestore()
            .doc('app_configurations/RIDER_DELIVERY_CONFIGURATIONS')
            .get();
        if (!current.exists) {
            return {
                ok: false,
            };
        }
        await firebase_admin_1.default
            .firestore()
            .doc('app_configurations/RIDER_DELIVERY_CONFIGURATIONS')
            .update(utils_1.ObjectUtils.omitUnknown(utils_1.ObjectUtils.merge(current.data(), defaultServiceConfig.toPrimitives())));
        await firebase_admin_1.default
            .firestore()
            .doc('app_configurations/GLOBAL_FEES')
            .set(utils_1.ObjectUtils.omitUnknown({
            exchangeRates: {
                bolivar: 8.02,
            },
        }));
        await firebase_admin_1.default
            .firestore()
            .doc('app_configurations/PAYMENT_METHODS')
            .set(utils_1.ObjectUtils.omitUnknown({
            BINANCE_PAY: {
                slug: "BINANCE_PAY", available: true,
            }, PAYPAL: {
                slug: "PAYPAL", available: true,
            }, CARD: {
                slug: "CARD", available: true,
            },
        }));
        await firebase_admin_1.default
            .firestore()
            .doc('app_configurations/GLOBAL_STATE')
            .set(utils_1.ObjectUtils.omitUnknown({
            available: true
        }));
        await firebase_admin_1.default
            .firestore()
            .doc('app_configurations/REFERRAL_SYSTEM')
            .set(utils_1.ObjectUtils.omitUnknown({
            available: true
        }));
        return {
            ok: true,
        };
    }
    async checkEnv() {
        try {
            return {
                projectId: this.configService.get('FB_PROJECT_ID'), env: process.env.NODE_ENV,
            };
        }
        catch (e) {
            return {
                result: false,
            };
        }
    }
    async validateRegistration(email) {
        try {
            const result = await firebase_admin_1.default.auth().getUserByEmail(email);
            return {
                result: true,
            };
        }
        catch (e) {
            return {
                result: false,
            };
        }
    }
    async checkNewPasswordToken(body) {
        if (body.token === '')
            return {
                isValid: false,
            };
        try {
            const result = await firebase_admin_1.default
                .firestore()
                .collection('users')
                .where('newPasswordToken', '==', body.token)
                .get();
            const user = result.docs[0].data();
            return {
                isValid: !!!result.empty, email: user.email,
            };
        }
        catch (e) {
            return {
                isValid: false,
            };
        }
    }
    async createNewPassword(body) {
        try {
            const result = await firebase_admin_1.default
                .firestore()
                .collection('users')
                .where('newPasswordToken', '==', body.token)
                .get();
            if (result.empty)
                throw new Error('INVALID_TOKEN');
            const user = result.docs[0].data();
            await firebase_admin_1.default.auth().updateUser(user.id, {
                password: body.password,
            });
            await firebase_admin_1.default.firestore().collection('users').doc(user.id).update({
                newPasswordToken: '', hasDefaultPassword: false, customPasswordCreatedAt: new Date(),
            });
            return {
                result: true,
            };
        }
        catch (e) {
            return {
                result: false, error: e.message,
            };
        }
    }
    async accountLinkVerification(body) {
        const result = await firebase_admin_1.default
            .firestore()
            .collection('users')
            .where('email', '==', body.email)
            .get();
        if (result.empty)
            throw new Error('USER_NOT_FOUND');
        try {
            const res = await axios_1.default.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${this.configService.get('FB_API_KEY')}`, {
                email: body.email, password: body.password, returnSecureToken: true,
            });
            const info = await res.data;
            const token = await this.tokenCreator.generate({
                userType: '', userId: info.localId, newRole: body.newRole, email: body.email,
            });
            await firebase_admin_1.default.firestore().collection('link_account_requests').add({
                token: token, userId: info.localId, date: new Date(),
            });
            return {
                ok: true, token: token,
            };
        }
        catch (e) {
            return {
                ok: false,
            };
        }
    }
    async transformApnsToken(body) {
        try {
            const token = await this.pushNotificationService.transformApnsToFcm(body.token, body.application);
            return {
                ok: true, token: token,
            };
        }
        catch (e) {
            console.log(e.message);
            return {
                ok: false,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Post)('fixture'),
    (0, swagger_1.ApiOperation)({
        summary: 'Saves default delivery service configurations', description: `{
    EXAMPLE:
      feeConfigurations: {
        CAR: [
          {
            baseFee: {
              amount: 5,
              currency: '$',
            },
            distanceRange: {
              min: 0,
            },
            durationFee: {
              amount: 0.5,
              currency: '$',
            },
            kmFee: {
              amount: 0.5,
              currency: '$',
            },
          },
        ],
        MOTOR_CYCLE: [
          {
            baseFee: {
              amount: 2,
              currency: '$',
            },
            distanceRange: {
              min: 0,
            },
            durationFee: {
              amount: 0.2,
              currency: '$',
            },
            kmFee: {
              amount: 0.2,
              currency: '$',
            },
          },
        ],
        BIKE: [
          {
            baseFee: {
              amount: 1,
              currency: '$',
            },
            distanceRange: {
              min: 0,
            },
            durationFee: {
              amount: 0.1,
              currency: '$',
            },
            kmFee: {
              amount: 0.1,
              currency: '$',
            },
          },
        ],
      },
      searchConfigurations: {
        acceptanceTimeout: 60,
        attemptMaxCandidates: 5,
        attemptSearchRadius: 1,
        maxAttempts: 3,
      },
    }`,
    }),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "saveServiceConfig", null);
tslib_1.__decorate([
    (0, common_1.Post)('save'),
    (0, swagger_1.ApiOperation)({
        summary: 'Updates delivery service configurations', description: `Send part of the updates. For example: {
      "config": {
        "searchConfigurations": {
          "attemptSearchRadius": 2
        }
      }
    } to increase attempt search radius`,
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [ConfigurationsDto]),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "saveConfig", null);
tslib_1.__decorate([
    (0, common_1.Get)('check-env'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "checkEnv", null);
tslib_1.__decorate([
    (0, common_1.Get)('registration/user/:email'),
    tslib_1.__param(0, (0, common_1.Param)('email')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "validateRegistration", null);
tslib_1.__decorate([
    (0, common_1.Post)('registration/create-password/check-token'),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [CheckNewPassTokenDto]),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "checkNewPasswordToken", null);
tslib_1.__decorate([
    (0, common_1.Post)('registration/create-password'),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [CreateNewPassDto]),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "createNewPassword", null);
tslib_1.__decorate([
    (0, common_1.Post)('registration/validate-account-linking'),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [AccountLinkVerifyDto]),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "accountLinkVerification", null);
tslib_1.__decorate([
    (0, common_1.Post)('transform-apns-token'),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [TransformApnsTokenDto]),
    tslib_1.__metadata("design:returntype", Promise)
], ConfigController.prototype, "transformApnsToken", null);
ConfigController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Configurations'),
    (0, common_1.Controller)({
        path: 'configurations',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__param(3, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(4, (0, decorators_1.inject)('TestDeliveryRequestsCommandRepository')),
    tslib_1.__param(5, (0, decorators_1.inject)('user.authentication.token.creator')),
    tslib_1.__param(6, (0, decorators_1.inject)('user.authentication.token.verificator')),
    tslib_1.__param(8, (0, decorators_1.inject)('push.notification.service')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object, Object, config_1.ConfigService, push_notifications_1.default])
], ConfigController);
exports.ConfigController = ConfigController;
//# sourceMappingURL=config.controller.js.map