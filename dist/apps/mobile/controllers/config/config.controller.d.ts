import { ServiceConfigurationsPrimitiveProps } from '@deliveryService/delivery/domain/models/service-configurations-model';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import TestDeliveryRequestsCommandRepository from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import { ConfigService } from '@nestjs/config';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import MobilePushNotificationService from '@shared/infrastructure/push-notifications';
declare class ConfigurationsDto {
    config: ServiceConfigurationsPrimitiveProps;
}
declare class CheckNewPassTokenDto {
    token: string;
}
declare class CreateNewPassDto {
    token: string;
    password: string;
}
declare class AccountLinkVerifyDto {
    email: string;
    password: string;
    newRole: string;
}
declare class TransformApnsTokenDto {
    token: string;
    application: string;
}
export declare class ConfigController extends ApiController {
    private deliveryRequestRepository;
    private testDeliveryRequestsRepository;
    private tokenCreator;
    private tokenVerificator;
    private configService;
    private pushNotificationService;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>, deliveryRequestRepository: DeliveryRequestCommandRepository, testDeliveryRequestsRepository: TestDeliveryRequestsCommandRepository, tokenCreator: AuthTokenCreator<any>, tokenVerificator: AuthTokenVerificator<any>, configService: ConfigService, pushNotificationService: MobilePushNotificationService);
    saveServiceConfig(): Promise<any>;
    saveConfig(body: ConfigurationsDto): Promise<any>;
    checkEnv(): Promise<{
        projectId: string;
        env: string;
        result?: undefined;
    } | {
        result: boolean;
        projectId?: undefined;
        env?: undefined;
    }>;
    validateRegistration(email: string): Promise<{
        result: boolean;
    }>;
    checkNewPasswordToken(body: CheckNewPassTokenDto): Promise<{
        isValid: boolean;
        email?: undefined;
    } | {
        isValid: boolean;
        email: any;
    }>;
    createNewPassword(body: CreateNewPassDto): Promise<{
        result: boolean;
        error?: undefined;
    } | {
        result: boolean;
        error: any;
    }>;
    accountLinkVerification(body: AccountLinkVerifyDto): Promise<{
        ok: boolean;
        token: string;
    } | {
        ok: boolean;
        token?: undefined;
    }>;
    transformApnsToken(body: TransformApnsTokenDto): Promise<{
        ok: boolean;
        token: string;
    } | {
        ok: boolean;
        token?: undefined;
    }>;
}
export {};
