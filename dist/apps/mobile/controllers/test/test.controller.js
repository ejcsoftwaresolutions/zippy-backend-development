"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestController = void 0;
const tslib_1 = require("tslib");
const delivery_request_command_repository_1 = require("../../../../src/main/delivery-service/delivery/domain/repositories/delivery-request-command-repository");
const test_delivery_requests_command_repository_1 = require("../../../../src/main/delivery-service/delivery/domain/repositories/test-delivery-requests-command-repository");
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const command_bus_1 = require("../../../../src/main/shared/domain/bus/command/command-bus");
const query_bus_1 = require("../../../../src/main/shared/domain/bus/query/query-bus");
const decorators_1 = require("../../../../src/main/shared/domain/decorators");
const multipart_handler_1 = require("../../../../src/main/shared/domain/storage/multipart-handler");
const api_controller_1 = require("../../../../src/main/shared/infrastructure/controller/api-controller");
const pdf_generator_1 = require("../../../../src/main/shared/domain/pdf/pdf-generator");
const excel_generator_1 = require("../../../../src/main/shared/domain/excel/excel-generator");
const config_1 = require("@nestjs/config");
const firebase_admin_1 = require("firebase-admin");
const push_notification_1 = require("../../../../functions/src/utils/models/push-notification");
const id_1 = require("../../../../src/main/shared/domain/id/id");
const bulk_notification_service_1 = require("../../../../functions/src/utils/bulk-notifications/bulk-notification-service");
const notification_service_1 = require("../../../../functions/src/utils/notification-service/notification-service");
class NotificationDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], NotificationDto.prototype, "userId", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], NotificationDto.prototype, "role", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Object)
], NotificationDto.prototype, "notificationData", void 0);
let TestController = class TestController extends api_controller_1.default {
    constructor(commandBus, queryBus, multipartHandler, deliveryRequestRepository, testDeliveryRequestsRepository, pdfGenerator, excelGenerator, configService) {
        super(commandBus, queryBus, multipartHandler);
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.testDeliveryRequestsRepository = testDeliveryRequestsRepository;
        this.pdfGenerator = pdfGenerator;
        this.excelGenerator = excelGenerator;
        this.configService = configService;
        this.notificationService = (0, notification_service_1.createNotificationService)();
        this.bulkNotificationService = (0, bulk_notification_service_1.createBulkNotificationService)(this.notificationService);
    }
    async ping() {
        try {
            return {
                ok: true,
                x: true,
            };
        }
        catch (error) {
            return {
                ok: false,
                error: error.message,
            };
        }
    }
    async test(data) {
        const appId = (() => {
            if (data.role.toUpperCase() === 'CUSTOMER') {
                return this.configService.get('CUSTOMER_APP_EXPERIENCE_ID');
            }
            if (data.role.toUpperCase() === 'VENDOR') {
                return this.configService.get('STORE_APP_EXPERIENCE_ID');
            }
            return this.configService.get('RIDER_APP_EXPERIENCE_ID');
        })();
        const userToken = await (async () => {
            const tokens = (await firebase_admin_1.default
                .firestore()
                .collection('user_push_tokens')
                .doc(data.userId)
                .get()).data();
            if (data.role.toUpperCase() === 'CUSTOMER') {
                return tokens === null || tokens === void 0 ? void 0 : tokens.customer;
            }
            if (data.role.toUpperCase() === 'VENDOR') {
                return tokens === null || tokens === void 0 ? void 0 : tokens.vendor;
            }
            return tokens === null || tokens === void 0 ? void 0 : tokens.rider;
        })();
        if (!userToken)
            return {
                ok: false,
                message: 'Token not found',
            };
        const channel = appId.split('/')[1];
        try {
            const res = await this.notificationService.send(new push_notification_1.PushNotification({
                id: new id_1.default().value,
                to: userToken,
                title: 'Tap the notification, Mike :)',
                content: 'This is a product link now :)',
                channelId: channel,
                appId: appId,
                linkUrl: 'zippi-market://v/LAL043974',
            }));
            console.log(res);
            return {
                res: res,
                ok: true,
            };
        }
        catch (e) {
            return {
                res: e.message,
                ok: false,
            };
        }
    }
    async testBulk() {
        try {
            const promises = [
                {
                    id: 'message-id',
                    content: {
                        title: 'Hola [[FIRST_NAME]]',
                        description: 'Hola desc',
                    },
                    type: 'CAMPAIGN',
                    targetGroup: 'CUSTOMER',
                },
            ].map((reqData) => {
                return new Promise(async (resolve) => {
                    const message = {
                        id: reqData.id,
                        title: reqData.content.title,
                        description: reqData.content.description,
                        imageUrl: reqData.content.attachments &&
                            reqData.content.attachments.length > 0
                            ? reqData.content.attachments[0].url
                            : undefined,
                    };
                    const results = reqData.type === 'INDIVIDUAL'
                        ? await this.bulkNotificationService.sendBulkIndividualNotification(reqData.targetGroup, message, reqData.targetRecipients)
                        : await this.bulkNotificationService.sendCampaignNotification(reqData.targetGroup, message);
                    resolve({
                        id: reqData.id,
                        results: results,
                    });
                });
            });
            const resultsPerMessage = await Promise.all(promises);
            console.log(resultsPerMessage[0].results);
            return {
                ok: true,
            };
        }
        catch (e) {
            return {
                ok: false,
                message: e.message,
            };
        }
    }
};
tslib_1.__decorate([
    (0, common_1.Get)('ping'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Promise)
], TestController.prototype, "ping", null);
tslib_1.__decorate([
    (0, common_1.Post)('test-notifications'),
    (0, swagger_1.ApiOperation)({
        summary: 'test',
        description: '{\n' +
            '        "userId": "string",\n' +
            '        "role": "VENDOR or RIDER or CUSTOMER",\n' +
            '        "notificationData": {\n' +
            '          "title": "Hola",\n' +
            '          "content": "Hola"\n' +
            '        }\n' +
            '      }',
    }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [NotificationDto]),
    tslib_1.__metadata("design:returntype", Promise)
], TestController.prototype, "test", null);
tslib_1.__decorate([
    (0, common_1.Post)('test-bulk-notifications'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Promise)
], TestController.prototype, "testBulk", null);
TestController = tslib_1.__decorate([
    (0, swagger_1.ApiTags)('Test'),
    (0, common_1.Controller)({
        path: 'test',
    }),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__param(3, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(4, (0, decorators_1.inject)('TestDeliveryRequestsCommandRepository')),
    tslib_1.__param(5, (0, decorators_1.inject)('pdf.generator')),
    tslib_1.__param(6, (0, decorators_1.inject)('excel.generator')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object, Object, Object, config_1.ConfigService])
], TestController);
exports.TestController = TestController;
//# sourceMappingURL=test.controller.js.map