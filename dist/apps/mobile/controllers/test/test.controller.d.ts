import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import TestDeliveryRequestsCommandRepository from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import PdfGenerator from '@shared/domain/pdf/pdf-generator';
import ExcelGenerator from '@shared/domain/excel/excel-generator';
import { ConfigService } from '@nestjs/config';
declare class NotificationDto {
    userId: string;
    role: string;
    notificationData: any;
}
export declare class TestController extends ApiController {
    private deliveryRequestRepository;
    private testDeliveryRequestsRepository;
    private pdfGenerator;
    private excelGenerator;
    private configService;
    private notificationService;
    private bulkNotificationService;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>, deliveryRequestRepository: DeliveryRequestCommandRepository, testDeliveryRequestsRepository: TestDeliveryRequestsCommandRepository, pdfGenerator: PdfGenerator, excelGenerator: ExcelGenerator, configService: ConfigService);
    ping(): Promise<{
        ok: boolean;
        x: boolean;
        error?: undefined;
    } | {
        ok: boolean;
        error: any;
        x?: undefined;
    }>;
    test(data: NotificationDto): Promise<{
        ok: boolean;
        message: string;
        res?: undefined;
    } | {
        res: any;
        ok: boolean;
        message?: undefined;
    }>;
    testBulk(): Promise<{
        ok: boolean;
        message?: undefined;
    } | {
        ok: boolean;
        message: any;
    }>;
}
export {};
