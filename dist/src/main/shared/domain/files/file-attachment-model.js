"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
class FileAttachment extends value_object_1.default {
    get url() {
        return this.props.url;
    }
    get type() {
        return this.props.type;
    }
    static fromPrimitives(props) {
        return new FileAttachment({
            ...props,
            type: props.type,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return json;
    }
}
exports.default = FileAttachment;
//# sourceMappingURL=file-attachment-model.js.map