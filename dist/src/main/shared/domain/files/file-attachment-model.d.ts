import ValueObject from '../value-object/value-object';
export type FileAttachmentType = 'csv' | 'jpg' | 'png' | 'pdf';
export interface FileAttachmentProps {
    url: string;
    type?: FileAttachmentType;
    description?: string;
}
export interface FileAttachmentPrimitiveProps {
    url: string;
    type?: string;
    description?: string;
}
export default class FileAttachment extends ValueObject<FileAttachmentProps> {
    get url(): string;
    get type(): FileAttachmentType;
    static fromPrimitives(props: FileAttachmentPrimitiveProps): FileAttachment;
    toPrimitives(): FileAttachmentPrimitiveProps;
}
