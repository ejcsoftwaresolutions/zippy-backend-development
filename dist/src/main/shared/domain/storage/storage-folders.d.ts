export declare namespace DriverStorageFolders {
    const root = "drivers";
    const carDocuments = "car-documents";
    const documents = "documents";
    const profile = "profile";
}
export declare namespace CustomerStorageFolders {
    const root = "customer";
    const profile = "profile";
}
