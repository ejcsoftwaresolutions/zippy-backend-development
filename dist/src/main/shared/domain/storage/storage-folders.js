"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerStorageFolders = exports.DriverStorageFolders = void 0;
var DriverStorageFolders;
(function (DriverStorageFolders) {
    DriverStorageFolders.root = 'drivers';
    DriverStorageFolders.carDocuments = 'car-documents';
    DriverStorageFolders.documents = 'documents';
    DriverStorageFolders.profile = 'profile';
})(DriverStorageFolders = exports.DriverStorageFolders || (exports.DriverStorageFolders = {}));
var CustomerStorageFolders;
(function (CustomerStorageFolders) {
    CustomerStorageFolders.root = 'customer';
    CustomerStorageFolders.profile = 'profile';
})(CustomerStorageFolders = exports.CustomerStorageFolders || (exports.CustomerStorageFolders = {}));
//# sourceMappingURL=storage-folders.js.map