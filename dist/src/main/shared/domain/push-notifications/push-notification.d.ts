export type PushNotificationProps = {
    to: string;
    title?: string;
    content: string;
    appId: string;
    channelId?: string;
    metadata?: any;
    imageUrl?: string;
};
export default class PushNotification {
    private readonly to;
    private readonly title?;
    private readonly content;
    private readonly appId;
    private readonly channelId;
    private readonly metadata;
    private readonly platform;
    private readonly imageUrl?;
    constructor(notificationData: PushNotificationProps);
    get isAndroid(): boolean;
    toPrimitives(): {
        to: string;
        title: string;
        content: string;
        appId: string;
        channelId: string;
        metadata: any;
        platform: "android" | "ios";
        imageUrl: string;
    };
}
