"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PushNotification {
    constructor(notificationData) {
        this.to = notificationData.to;
        this.title = notificationData.title;
        this.content = notificationData.content;
        this.appId = notificationData.appId;
        this.channelId = notificationData.channelId;
        this.metadata = notificationData.metadata;
        this.platform = notificationData.to.length > 64 ? 'android' : 'ios';
        this.imageUrl = notificationData.imageUrl;
    }
    get isAndroid() {
        return this.platform === 'android';
    }
    toPrimitives() {
        var _a;
        return {
            to: this.to,
            title: this.title,
            content: this.content,
            appId: this.appId,
            channelId: (_a = this.channelId) !== null && _a !== void 0 ? _a : this.appId.split('/')[1],
            metadata: this.metadata,
            platform: this.platform,
            imageUrl: this.imageUrl,
        };
    }
}
exports.default = PushNotification;
//# sourceMappingURL=push-notification.js.map