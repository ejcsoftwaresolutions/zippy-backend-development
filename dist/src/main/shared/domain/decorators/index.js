"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = exports.inject = exports.injectable = void 0;
const common_1 = require("@nestjs/common");
function injectable() {
    return (0, common_1.Injectable)();
}
exports.injectable = injectable;
function inject(key) {
    return (0, common_1.Inject)(key);
}
exports.inject = inject;
function config() {
    return null;
}
exports.config = config;
//# sourceMappingURL=index.js.map