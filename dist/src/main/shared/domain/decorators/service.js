"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
function service(scope) {
    return (0, common_1.Injectable)({ scope: scope !== null && scope !== void 0 ? scope : common_1.Scope.DEFAULT });
}
exports.default = service;
//# sourceMappingURL=service.js.map