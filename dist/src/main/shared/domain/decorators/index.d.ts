export declare function injectable(): ClassDecorator;
export declare function inject(key: any): (target: object, key: string | symbol, index?: number) => void;
export declare function config(): any;
