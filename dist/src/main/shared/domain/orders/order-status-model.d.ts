import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
export type StatusType = 'PROCESSING' | 'SCHEDULED' | 'IN_BACK_ORDER' | 'NEED_MORE_INFO' | 'SENT';
export interface OrderStatusProps {
    type: StatusType;
    displayName?: string;
}
export interface OrderStatusPrimitiveProps {
    type: string;
    displayName?: string;
}
export default class OrderStatus extends AggregateRoot<OrderStatusProps> {
    get type(): StatusType;
    getType(format?: 'camelCase' | 'default'): string;
    get isPlaced(): boolean;
    get isSent(): boolean;
    get orderNeedsMoreInfo(): boolean;
    static fromPrimitives(props: OrderStatusPrimitiveProps): OrderStatus;
    isEqual(status: OrderStatus): boolean;
    toPrimitives(): OrderStatusPrimitiveProps;
}
