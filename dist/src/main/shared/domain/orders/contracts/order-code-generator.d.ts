export interface OrderCodeGenerator {
    generate(): string;
}
