"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../aggregate/aggregate-root");
const toCamel = (str) => str.replace(/([-_][a-z])/g, (group) => group.toUpperCase().replace('_', '').replace('-', ''));
class OrderStatus extends aggregate_root_1.default {
    get type() {
        return this.props.type;
    }
    getType(format = 'default') {
        if (format === 'camelCase') {
            return toCamel(this.props.type.toLocaleLowerCase());
        }
        return this.props.type;
    }
    get isPlaced() {
        return this.props.type === 'PROCESSING';
    }
    get isSent() {
        return this.props.type === 'SENT';
    }
    get orderNeedsMoreInfo() {
        return this.props.type === 'NEED_MORE_INFO';
    }
    static fromPrimitives(props) {
        return new OrderStatus({
            ...props,
            type: props.type,
        });
    }
    isEqual(status) {
        return this.type === status.type;
    }
    toPrimitives() {
        const json = super.toJson();
        return json;
    }
}
exports.default = OrderStatus;
//# sourceMappingURL=order-status-model.js.map