"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
const model_1 = require("../models/model");
class Collection {
    constructor(items) {
        this._items = items;
    }
    get items() {
        return this._items;
    }
    each(callback) {
        this.items.forEach(callback);
    }
    concat(newItems) {
        return new Collection(this.items.concat(newItems.items));
    }
    map(callback) {
        return this.items.map(callback);
    }
    find(properties) {
        return utils_1.ObjectUtils.find(this.items, properties);
    }
    add(...items) {
        this.items.push(...items);
    }
    getAt(index) {
        return this.items[index];
    }
    filter(criteria) {
        return new Collection(utils_1.ArrayUtils.filter(this.items, criteria));
    }
    findIndex(criteria) {
        return utils_1.ArrayUtils.findIndex(this.items, criteria);
    }
    findById(id) {
        const found = this.items.find((item) => {
            return item.id.value == id.value;
        });
        return found;
    }
    removeById(id) {
        this._items = this.items.filter((item) => {
            return item.id.value !== id.value;
        });
        return this._items;
    }
    findContaining(containingItems) {
        const containing = this.items.filter((item) => {
            return (containingItems.items
                .map((e) => e.id.value)
                .indexOf(item.id.value) > -1);
        });
        return containing;
    }
    subtract(collection) {
        return new Collection(utils_1.ArrayUtils.differenceWith(this.items, collection.items, (e, i) => {
            return e.id.value === i.id.value;
        }));
    }
    replaceAt(index, newElement) {
        this.items.splice(index, 1, newElement);
    }
    sortItemsBy(sortBy, sortMethod) {
        return new Collection(utils_1.ArrayUtils.orderBy(this.items, sortBy, sortMethod));
    }
    count() {
        return this.items.length;
    }
    toPrimitives() {
        return this.items.map((item) => {
            if (typeof item.toPrimitives === 'function') {
                return item.toPrimitives();
            }
            return item instanceof model_1.default ? item.toJson() : item;
        });
    }
}
exports.default = Collection;
//# sourceMappingURL=collection.js.map