import Id from '../id/id';
export default class Collection<T> {
    private _items;
    constructor(items: Array<T>);
    get items(): T[];
    each(callback: (item: T) => any): void;
    concat(newItems: Collection<T>): Collection<T>;
    map(callback: (item: T) => any): any[];
    find(properties: Partial<T> | any): any;
    add(...items: T[]): void;
    getAt(index: number): T;
    filter(criteria: any): Collection<T>;
    findIndex(criteria: any): number;
    findById(id: Id): T;
    removeById(id: Id): T[];
    findContaining(containingItems: Collection<T>): T[];
    subtract(collection: Collection<T>): Collection<T>;
    replaceAt(index: any, newElement: T): void;
    sortItemsBy(sortBy: string, sortMethod: 'asc' | 'desc'): Collection<unknown>;
    count(): number;
    toPrimitives(): any[];
}
