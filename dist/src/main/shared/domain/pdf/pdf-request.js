"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
class PDFRequest extends value_object_1.default {
    constructor(props) {
        super(props);
    }
    get config() {
        return this.props.config;
    }
    get template() {
        return this.props.template;
    }
    get variables() {
        return this.props.variables;
    }
    get uploadUrl() {
        return this.props.uploadUrl;
    }
    toPrimitives() {
        return this.props;
    }
}
exports.default = PDFRequest;
//# sourceMappingURL=pdf-request.js.map