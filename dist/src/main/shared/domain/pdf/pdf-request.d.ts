import ValueObject from '../value-object/value-object';
interface PDFRequestProps {
    template: string;
    variables?: any;
    uploadUrl: string;
    config?: any;
}
export default class PDFRequest extends ValueObject<PDFRequestProps> {
    constructor(props: PDFRequestProps);
    get config(): any;
    get template(): string;
    get variables(): any;
    get uploadUrl(): string;
    toPrimitives(): PDFRequestProps;
}
export {};
