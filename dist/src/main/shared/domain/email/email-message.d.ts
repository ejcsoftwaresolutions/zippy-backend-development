export type EmailRecipient = {
    name?: string;
    email: string;
};
type AttributesMap = {
    [key: string]: string;
};
export type EmailMessageProperties = {
    from?: EmailRecipient;
    to: EmailRecipient;
    content?: string;
    attachments?: Array<string>;
    attributes?: AttributesMap;
    subject?: string;
    templateId?: string;
};
export default class EmailMessage {
    private readonly from?;
    private readonly to;
    private readonly subject?;
    private content?;
    private readonly attachments?;
    private readonly attributes?;
    private templateId?;
    constructor(emailData: EmailMessageProperties);
    toPrimitives(): {
        toEmail: string;
        toName: string;
        fromEmail: string;
        fromName: string;
        subject: string;
        content: string;
        attachments: string[];
        attributes: AttributesMap;
        templateId: string;
    };
    private ensureValidEmail;
}
export {};
