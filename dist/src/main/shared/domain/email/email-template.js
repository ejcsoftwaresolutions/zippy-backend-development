"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EmailTemplate {
    constructor(emailData) {
        this.ensureValidEmail(emailData.from);
        this.ensureValidEmail(emailData.to);
        this.from = emailData.from;
        this.to = emailData.to;
        this.subject = emailData.subject;
        this.text = emailData.text;
        this.html = emailData.html;
        this.attachments = emailData.attachments;
    }
    static fromPrimitive(plainObject) {
        return new EmailTemplate(plainObject);
    }
    toPrimitives() {
        return {
            to: this.to,
            from: this.from,
            subject: this.subject,
            text: this.text,
            html: this.html,
            attachments: this.attachments,
        };
    }
    ensureValidEmail(email) {
        if (!isValidEmail(email)) {
            throw new Error(`${email} is not a valid email`);
        }
    }
}
exports.default = EmailTemplate;
function isValidEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
//# sourceMappingURL=email-template.js.map