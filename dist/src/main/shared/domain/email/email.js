"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
class Email extends value_object_1.default {
    constructor(value) {
        super({ value: '' });
        if (!this.validate(value)) {
            throw new Error('INVALID_EMAIL');
        }
        this.props.value = value.toLowerCase();
    }
    get value() {
        return this.props.value;
    }
    validate(value) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(value).toLowerCase());
    }
    toString() {
        return this.props.value;
    }
    toPrimitives() {
        return this.props.value;
    }
}
exports.default = Email;
//# sourceMappingURL=email.js.map