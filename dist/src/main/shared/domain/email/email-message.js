"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EmailMessage {
    constructor(emailData) {
        emailData.from && this.ensureValidEmail(emailData.from.email);
        this.ensureValidEmail(emailData.to.email);
        this.from = emailData.from;
        this.to = emailData.to;
        this.subject = emailData.subject;
        this.content = emailData.content;
        this.attachments = emailData.attachments;
        this.templateId = emailData.templateId;
        this.attributes = emailData.attributes;
    }
    toPrimitives() {
        var _a, _b;
        return {
            toEmail: this.to.email,
            toName: this.to.name,
            fromEmail: (_a = this.from) === null || _a === void 0 ? void 0 : _a.email,
            fromName: (_b = this.from) === null || _b === void 0 ? void 0 : _b.name,
            subject: this.subject,
            content: this.content,
            attachments: this.attachments,
            attributes: this.attributes,
            templateId: this.templateId,
        };
    }
    ensureValidEmail(email) {
        if (!isValidEmail(email)) {
            throw new Error(`${email} is not a valid email`);
        }
    }
}
exports.default = EmailMessage;
function isValidEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
//# sourceMappingURL=email-message.js.map