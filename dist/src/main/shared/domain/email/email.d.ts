import ValueObject from '../value-object/value-object';
interface EmailProps {
    value: string;
}
export default class Email extends ValueObject<EmailProps> {
    constructor(value: string);
    get value(): string;
    private validate;
    toString(): string;
    toPrimitives(): string;
}
export {};
