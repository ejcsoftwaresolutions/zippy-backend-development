export default class EmailTemplate {
    private readonly from;
    private readonly to;
    private readonly subject?;
    private readonly text?;
    private readonly html?;
    private readonly attachments?;
    constructor(emailData: {
        from: string;
        to: string;
        text?: string;
        html?: string;
        attachments?: Array<string>;
        subject?: string;
    });
    static fromPrimitive(plainObject: {
        from: string;
        to: string;
        subject?: string;
        text?: string;
        html?: string;
        attachments?: Array<string>;
    }): EmailTemplate;
    toPrimitives(): {
        to: string;
        from: string;
        subject: string;
        text: string;
        html: string;
        attachments: string[];
    };
    private ensureValidEmail;
}
