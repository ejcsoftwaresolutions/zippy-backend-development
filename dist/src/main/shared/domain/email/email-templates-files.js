"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PassengerEmailTemplates = exports.DriverEmailTemplates = void 0;
var DriverEmailTemplates;
(function (DriverEmailTemplates) {
    DriverEmailTemplates.welcome = 'drivers/welcome.ejs';
})(DriverEmailTemplates = exports.DriverEmailTemplates || (exports.DriverEmailTemplates = {}));
var PassengerEmailTemplates;
(function (PassengerEmailTemplates) {
    PassengerEmailTemplates.welcome = 'customers/welcome.ejs';
})(PassengerEmailTemplates = exports.PassengerEmailTemplates || (exports.PassengerEmailTemplates = {}));
//# sourceMappingURL=email-templates-files.js.map