export interface AnyObject {
    [property: string]: any;
}
export type Options = AnyObject;
export type PropertyType = string | Function | object;
export interface PropertyForm {
    in?: boolean;
    out?: boolean;
    name?: string;
}
export interface PropertyDefinition {
    type: PropertyType;
    id?: boolean | number;
    json?: PropertyForm;
    [attribute: string]: any;
}
export interface ModelDefinitionSyntax {
    name: string;
    properties?: {
        [name: string]: PropertyDefinition | PropertyType;
    };
    [attribute: string]: any;
    settings?: {
        [name: string]: any;
    };
}
export type DeepPartial<T> = Partial<T> | {
    [P in keyof T]?: DeepPartial<T[P]>;
};
export type DataObject<T extends object> = T | DeepPartial<T>;
export type ValidateStructure<T, Struct> = T extends Struct ? Exclude<keyof T, keyof Struct> extends never ? T : never : never;
export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
