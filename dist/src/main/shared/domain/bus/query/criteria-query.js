"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const query_1 = require("./query");
class CriteriaQuery extends query_1.default {
    constructor(filters, order, limit, offset) {
        super();
        this.filters = filters;
        this.order = order;
        this.limit = limit;
        this.offset = offset;
    }
}
exports.default = CriteriaQuery;
//# sourceMappingURL=criteria-query.js.map