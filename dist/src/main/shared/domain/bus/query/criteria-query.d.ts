import Query from './query';
export type OrderData = {
    orderType: string;
    orderBy: string;
};
export default abstract class CriteriaQuery extends Query {
    readonly filters: any;
    readonly order: OrderData[];
    readonly limit?: number;
    readonly offset?: number;
    constructor(filters: any, order: OrderData[], limit?: number, offset?: number);
}
