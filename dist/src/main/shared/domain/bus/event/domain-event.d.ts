import { AnyObject } from '@shared/domain/types';
type EventData = AnyObject;
export interface DomainEventProps {
    aggregateId: string;
    occurredOn: Date;
    eventId?: string;
    eventData: EventData;
}
export default abstract class DomainEvent<T extends DomainEventProps> {
    readonly props: T;
    constructor(props: T);
    abstract eventName(): string;
    get aggregateId(): string;
    get occurredOn(): Date;
    get eventId(): string;
}
export {};
