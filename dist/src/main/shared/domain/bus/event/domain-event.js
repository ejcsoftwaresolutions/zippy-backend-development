"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../../types");
class DomainEvent {
    constructor(props) {
        Object.assign(this, {
            ...{ eventData: {} },
            props,
            eventData: props.eventData,
        });
    }
    get aggregateId() {
        return this.props.aggregateId;
    }
    get occurredOn() {
        return this.props.occurredOn;
    }
    get eventId() {
        return this.props.eventId;
    }
}
exports.default = DomainEvent;
//# sourceMappingURL=domain-event.js.map