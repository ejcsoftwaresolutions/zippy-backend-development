"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../../types");
class Command {
    constructor(data) {
        Object.assign(this, data);
    }
}
exports.default = Command;
//# sourceMappingURL=command.js.map