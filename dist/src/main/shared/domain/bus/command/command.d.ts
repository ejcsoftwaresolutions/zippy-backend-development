import { AnyObject } from '@shared/domain/types';
export default abstract class Command {
    constructor(data?: AnyObject);
    abstract name(): string;
}
