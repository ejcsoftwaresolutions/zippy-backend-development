export default interface AppVersionsRepository {
    getDriverVersion(): Promise<string>;
    getPassengerVersion(): Promise<string>;
}
