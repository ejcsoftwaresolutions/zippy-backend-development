import Model from '../models/model';
export default abstract class Entity<T> extends Model<T> {
    getId(): string;
    getClassName(): string;
}
