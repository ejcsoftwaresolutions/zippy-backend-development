"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("../models/model");
class Entity extends model_1.default {
    getId() {
        return typeof this.props.id == 'string'
            ? this.props.id
            : this.props.id.toString();
    }
    getClassName() {
        return this.constructor.name;
    }
}
exports.default = Entity;
//# sourceMappingURL=entity.js.map