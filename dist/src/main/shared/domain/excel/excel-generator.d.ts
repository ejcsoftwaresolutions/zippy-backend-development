import ExcelRequest from '@shared/domain/excel/excel-request';
export default interface ExcelGenerator<T = Object> {
    generate(excel: ExcelRequest): Promise<{
        url: string;
    }>;
}
