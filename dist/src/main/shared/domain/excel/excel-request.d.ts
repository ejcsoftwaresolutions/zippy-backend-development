import ValueObject from '../value-object/value-object';
interface ExcelRequestProps {
    heading?: any;
    uploadFolderUrl: string;
    specification: any;
    data: any;
}
export default class ExcelRequest extends ValueObject<ExcelRequestProps> {
    constructor(props: ExcelRequestProps);
    get data(): any;
    get specification(): any;
    get uploadFolderUrl(): string;
    toPrimitives(): ExcelRequestProps;
}
export {};
