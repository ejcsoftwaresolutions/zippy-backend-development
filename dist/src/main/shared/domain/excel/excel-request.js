"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
class ExcelRequest extends value_object_1.default {
    constructor(props) {
        super(props);
    }
    get data() {
        return this.props.data;
    }
    get specification() {
        return this.props.specification;
    }
    get uploadFolderUrl() {
        return this.props.uploadFolderUrl;
    }
    toPrimitives() {
        return this.props;
    }
}
exports.default = ExcelRequest;
//# sourceMappingURL=excel-request.js.map