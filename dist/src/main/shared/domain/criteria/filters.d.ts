import ValueObject from '../value-object/value-object';
export interface FiltersProps {
    filters: any;
}
export default class Filters extends ValueObject<FiltersProps> {
    static fromValues(values: any): Filters;
    get filters(): any;
    toPrimitives(): any;
}
