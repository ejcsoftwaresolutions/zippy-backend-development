"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class FilterValue {
    constructor(value) {
        this.value = value;
    }
    get() {
        return this.value;
    }
}
exports.default = FilterValue;
//# sourceMappingURL=filter-value.js.map