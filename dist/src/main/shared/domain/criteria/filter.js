"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
const filter_value_1 = require("./filter-value");
class Filter extends value_object_1.default {
    static fromValues(values) {
        return new Filter({
            field: values.field,
            operator: values.operator,
            value: new filter_value_1.default(values.value),
        });
    }
    field() {
        return this.props.field;
    }
    operator() {
        return this.props.operator;
    }
    value() {
        return this.props.value;
    }
    serialize() {
        return '';
    }
}
exports.default = Filter;
//# sourceMappingURL=filter.js.map