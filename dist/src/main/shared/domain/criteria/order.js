"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const collection_1 = require("../value-object/collection");
const value_object_1 = require("../value-object/value-object");
class Order extends value_object_1.default {
    static createDesc(orderBy) {
        return new Order({
            orderBy: orderBy,
            orderType: 'DESC',
        });
    }
    static fromValues(orderBy, orderType) {
        return new Order({
            orderBy: orderBy,
            orderType: orderType,
        });
    }
    static fromArray(orders) {
        return new collection_1.default(orders.map((o) => Order.fromValues(o.orderType, o.orderBy)));
    }
    orderBy() {
        return this.props.orderBy;
    }
    orderType() {
        return this.props.orderType;
    }
    toPrimitives() {
        return `${this.props.orderType} ${this.props.orderBy}`;
    }
}
exports.default = Order;
//# sourceMappingURL=order.js.map