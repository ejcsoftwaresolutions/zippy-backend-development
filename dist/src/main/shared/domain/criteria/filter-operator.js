"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilterOperator = void 0;
var FilterOperator;
(function (FilterOperator) {
    FilterOperator["EQUAL"] = "=";
    FilterOperator["NOT_EQUAL"] = "!=";
    FilterOperator["GT"] = ">";
    FilterOperator["LT"] = "<";
    FilterOperator["CONTAINS"] = "CONTAINS";
    FilterOperator["NOT_CONTAINS"] = "NOT_CONTAINS";
    FilterOperator["LIKE"] = "LIKE";
    FilterOperator["NOT_LIKE"] = "NOT_LIKE";
})(FilterOperator = exports.FilterOperator || (exports.FilterOperator = {}));
//# sourceMappingURL=filter-operator.js.map