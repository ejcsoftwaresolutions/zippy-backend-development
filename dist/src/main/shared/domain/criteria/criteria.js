"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
class Criteria extends value_object_1.default {
    plainFilters() {
        return this.props.filters.filters();
    }
    filters() {
        return this.props.filters;
    }
    order() {
        return this.props.order;
    }
    offset() {
        return this.props.offset;
    }
    limit() {
        return this.props.limit;
    }
    toPrimitives() {
        const order = this.props.order.map((o) => {
            const orderData = o.toPrimitives();
            return orderData;
        });
        return {
            filters: this.props.filters.toPrimitives(),
            order: order,
            offset: this.props.offset,
            limit: this.props.limit,
        };
    }
    serialize() {
        return '';
    }
}
exports.default = Criteria;
//# sourceMappingURL=criteria.js.map