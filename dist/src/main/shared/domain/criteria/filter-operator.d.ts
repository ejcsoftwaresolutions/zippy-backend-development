export declare enum FilterOperator {
    EQUAL = "=",
    NOT_EQUAL = "!=",
    GT = ">",
    LT = "<",
    CONTAINS = "CONTAINS",
    NOT_CONTAINS = "NOT_CONTAINS",
    LIKE = "LIKE",
    NOT_LIKE = "NOT_LIKE"
}
