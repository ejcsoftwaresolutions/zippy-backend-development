import ValueObject from '../value-object/value-object';
import { FilterField } from './filter-field';
import { FilterOperator } from './filter-operator';
import FilterValue, { PrimitiveValue } from './filter-value';
export interface FilterProps<V extends PrimitiveValue> {
    field: FilterField;
    operator: FilterOperator;
    value: FilterValue<V>;
}
export interface FilterPrimitiveProps<T> {
    field: string;
    operator: FilterOperator;
    value: T;
}
export default class Filter<T extends PrimitiveValue> extends ValueObject<FilterProps<T>> {
    static fromValues<T extends PrimitiveValue>(values: FilterPrimitiveProps<T>): Filter<T>;
    field(): FilterField;
    operator(): FilterOperator;
    value(): FilterValue<T>;
    serialize(): string;
}
