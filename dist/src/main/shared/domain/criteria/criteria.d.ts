import Collection from '../value-object/collection';
import ValueObject from '../value-object/value-object';
import Filters from './filters';
import Order from './order';
interface CriteriaProperties {
    filters: Filters;
    order: Collection<Order>;
    offset?: number;
    limit?: number;
}
export default class Criteria extends ValueObject<CriteriaProperties> {
    plainFilters(): Array<any>;
    filters(): Filters;
    order(): Collection<Order>;
    offset(): number;
    limit(): number;
    toPrimitives(): {
        filters: any;
        order: string[];
        offset: number;
        limit: number;
    };
    serialize(): string;
}
export {};
