import Collection from '../value-object/collection';
import ValueObject from '../value-object/value-object';
import { OrderBy } from './order-by';
import { OrderType } from './order-type';
interface OrderProps {
    orderBy: OrderBy;
    orderType: OrderType;
}
interface OrderPrimitiveProps {
    orderBy: string;
    orderType: string;
}
export default class Order extends ValueObject<OrderProps> {
    static createDesc(orderBy: OrderBy): Order;
    static fromValues(orderBy: OrderBy, orderType: string): Order;
    static fromArray(orders: OrderPrimitiveProps[]): Collection<Order>;
    orderBy(): OrderBy;
    orderType(): OrderType;
    toPrimitives(): string;
}
export {};
