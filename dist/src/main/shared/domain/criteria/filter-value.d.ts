export type PrimitiveValue = Date | string | number | boolean;
export type Primitive = PrimitiveValue;
export default class FilterValue<T extends Primitive> {
    private value;
    constructor(value: T);
    get(): PrimitiveValue;
}
