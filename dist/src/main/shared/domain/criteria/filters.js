"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
class Filters extends value_object_1.default {
    static fromValues(values) {
        return new Filters({
            filters: values,
        });
    }
    get filters() {
        return this.props.filters;
    }
    toPrimitives() {
        return this.props.filters;
    }
}
exports.default = Filters;
//# sourceMappingURL=filters.js.map