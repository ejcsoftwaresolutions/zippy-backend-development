import Id from '../id/id';
import ValueObject from '../value-object/value-object';
export interface UserPushTokenProps {
    token: string;
    userId: Id;
}
export interface UserPushTokenPrimitiveProps {
    token: string;
    userId: string;
}
export default class UserPushToken extends ValueObject<UserPushTokenProps> {
    get token(): string;
    get userId(): Id;
    static fromPrimitives(props: UserPushTokenPrimitiveProps): UserPushToken;
    toPrimitives(): UserPushTokenPrimitiveProps;
}
