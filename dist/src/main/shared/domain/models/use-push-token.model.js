"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const id_1 = require("../id/id");
const value_object_1 = require("../value-object/value-object");
class UserPushToken extends value_object_1.default {
    get token() {
        return this.props.token;
    }
    get userId() {
        return this.props.userId;
    }
    static fromPrimitives(props) {
        return new UserPushToken({
            token: props.token,
            userId: new id_1.default(props.userId),
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return json;
    }
}
exports.default = UserPushToken;
//# sourceMappingURL=use-push-token.model.js.map