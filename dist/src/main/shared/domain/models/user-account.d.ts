import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { UserRole } from '@shared/domain/models/user-role';
export interface UserAccountProps {
    id: Id;
    email: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    createdAt?: Date;
    birthday?: Date;
    plainPassword?: string;
    hasDefaultPassword: boolean;
    newPasswordToken?: string;
    roles: UserRole[];
    status: string;
}
export interface UserAccountPrimitiveProps extends Omit<UserAccountProps, 'id'> {
    id: string;
}
export default class UserAccount extends AggregateRoot<UserAccountProps> {
    constructor(props: UserAccountProps);
    get id(): Id;
    get email(): string;
    get firstName(): string;
    get status(): string;
    get lastName(): string;
    get roles(): UserRole[];
    get phone(): string;
    get plainPassword(): string;
    get createdAt(): Date;
    static create(data: UserAccountProps): UserAccount;
    static fromPrimitives({ ...plainData }: UserAccountPrimitiveProps): UserAccount;
    addRole(newRole: UserRole): void;
    update(updates: Partial<UserAccountProps>): void;
    toPrimitives(): UserAccountPrimitiveProps;
}
