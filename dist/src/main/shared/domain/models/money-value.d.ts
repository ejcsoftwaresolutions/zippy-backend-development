import ValueObject from '../value-object/value-object';
import Currency, { CurrencyPrimitiveProps } from './currency-model';
export interface MoneyProps {
    value: number;
    currency?: Currency;
}
export interface MoneyPrimitiveProps {
    value: number;
    currency?: CurrencyPrimitiveProps;
}
export declare const DefaultCurrency: Currency;
export default class Money extends ValueObject<MoneyProps> {
    get value(): number;
    get currency(): Currency;
    static create(props: MoneyProps): Money;
    static fromPrimitives(props: MoneyPrimitiveProps): Money;
    toPrimitives(): MoneyPrimitiveProps;
}
