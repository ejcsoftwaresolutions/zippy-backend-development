import ValueObject from '../value-object/value-object';
export interface CurrencyProps {
    name: string;
    symbol: string;
}
export interface CurrencyPrimitiveProps {
    name: string;
    symbol: string;
}
export default class Currency extends ValueObject<CurrencyProps> {
    get name(): string;
    get symbol(): string;
    static fromPrimitives(props: CurrencyPrimitiveProps): Currency;
    toPrimitives(): CurrencyPrimitiveProps;
}
