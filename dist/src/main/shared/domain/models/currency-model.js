"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../value-object/value-object");
class Currency extends value_object_1.default {
    get name() {
        return this.props.name;
    }
    get symbol() {
        return this.props.symbol;
    }
    static fromPrimitives(props) {
        return new Currency({
            name: props.name,
            symbol: props.symbol,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return json;
    }
}
exports.default = Currency;
//# sourceMappingURL=currency-model.js.map