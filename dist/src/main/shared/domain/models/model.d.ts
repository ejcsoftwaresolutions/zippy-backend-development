import { AnyObject, Options } from '../types';
export default abstract class Model<T> {
    protected props: T;
    constructor(props: T);
    toJson(): AnyObject;
    toObject(options?: Options): Object;
    toString(): string;
}
