"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currency_1 = require("./currency");
const currency_2 = require("../utils/currency");
const BsCurrency = new currency_1.default({
    abbr: currency_2.bsAbbr,
    name: 'Bolivares Soberanos',
    format: currency_2.bsFormat,
});
exports.default = BsCurrency;
//# sourceMappingURL=bs-currency.js.map