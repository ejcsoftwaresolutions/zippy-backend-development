"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currency_1 = require("./currency");
const usd_currency_1 = require("./usd-currency");
const utils_1 = require("../utils");
class CurrencyPrice {
    constructor(props) {
        this.props = props;
        if (isNaN(props.value)) {
            props.value = 0;
        }
        props.value = parseFloat(props.value);
    }
    get value() {
        return this.props.value;
    }
    get formattedValue() {
        if (this.props.formattedValue) {
            return this.props.formattedValue;
        }
        return this.props.currency.format(this.props.value);
    }
    get currency() {
        return this.props.currency;
    }
    static sum(priceA, priceB) {
        return new CurrencyPrice({
            value: priceA.value + priceB.value,
            currency: priceA.currency,
        });
    }
    static subtract(priceA, priceB) {
        if (priceA.value - priceB.value < 0) {
            return CurrencyPrice.fromZero(priceA.currency);
        }
        return new CurrencyPrice({
            value: priceA.value - priceB.value,
            currency: priceA.currency,
        });
    }
    static times(currencyPrice, times, totalCurrency) {
        return new CurrencyPrice({
            value: currencyPrice.value * times,
            currency: totalCurrency !== null && totalCurrency !== void 0 ? totalCurrency : currencyPrice.currency,
        });
    }
    static div(currencyPrice, times, totalCurrency) {
        return new CurrencyPrice({
            value: currencyPrice.value / times,
            currency: totalCurrency !== null && totalCurrency !== void 0 ? totalCurrency : currencyPrice.currency,
        });
    }
    static getAcc(prices) {
        if (prices.length == 0)
            return CurrencyPrice.fromZero(usd_currency_1.default);
        const total = prices.reduce((a, b) => {
            return a + b.value;
        }, 0);
        const accCurrency = prices[0].currency;
        return new CurrencyPrice({
            currency: accCurrency,
            value: total,
        });
    }
    static fromPrimitives(props) {
        return new CurrencyPrice({
            currency: currency_1.default.fromPrimitives(props.currency),
            value: props.value,
            formattedValue: props.formattedValue,
        });
    }
    static isEqual(priceA, priceB) {
        if (!priceA.props.currency.isEqual(priceB.currency))
            return false;
        return priceA.props.value == priceB.props.value;
    }
    static equalPrices(pricesA, pricesB) {
        const difference = utils_1.ArrayUtils.differenceWith(pricesA, pricesB, (a, b) => CurrencyPrice.isEqual(a, b));
        return difference.length == 0;
    }
    static fromZero(currency) {
        return new CurrencyPrice({
            currency: currency_1.default.fromPrimitives(currency),
            value: 0,
            formattedValue: '0',
        });
    }
    static fromPrimitiveArray(items) {
        return items.map((e) => CurrencyPrice.fromPrimitives(e));
    }
    toPrimitives() {
        return {
            ...this.props,
            currency: this.props.currency.toPrimitives(),
        };
    }
}
exports.default = CurrencyPrice;
//# sourceMappingURL=currency-price.js.map