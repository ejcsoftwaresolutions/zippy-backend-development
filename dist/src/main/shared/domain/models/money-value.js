"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultCurrency = void 0;
const value_object_1 = require("../value-object/value-object");
const currency_model_1 = require("./currency-model");
exports.DefaultCurrency = new currency_model_1.default({
    name: 'ARS',
    symbol: '$',
});
class Money extends value_object_1.default {
    get value() {
        return this.props.value;
    }
    get currency() {
        return this.props.currency;
    }
    static create(props) {
        return new Money({
            ...props,
            currency: props.currency ? props.currency : exports.DefaultCurrency,
        });
    }
    static fromPrimitives(props) {
        return new Money({
            value: props.value,
            currency: props.currency
                ? currency_model_1.default.fromPrimitives(props.currency)
                : exports.DefaultCurrency,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return json;
    }
}
exports.default = Money;
//# sourceMappingURL=money-value.js.map