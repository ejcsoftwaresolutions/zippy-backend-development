"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../utils");
function asObject(value, options) {
    if (value == null)
        return value;
    if (typeof value.toPrimitives === 'function') {
        return value.toPrimitives();
    }
    if (Array.isArray(value)) {
        return value.map((item) => utils_1.ObjectUtils.omitUnknown(asObject(item, options)));
    }
    return value;
}
class Model {
    constructor(props) {
        this.props = props;
    }
    toJson() {
        return this.toObject();
    }
    toObject(options) {
        const obj = {};
        const props = this.props;
        const keys = Object.keys(props);
        keys.forEach((propertyName) => {
            const val = props[propertyName];
            obj[propertyName] = asObject(val, options);
        });
        return utils_1.ObjectUtils.omitUnknown(obj);
    }
    toString() {
        return JSON.stringify(this.props);
    }
}
exports.default = Model;
//# sourceMappingURL=model.js.map