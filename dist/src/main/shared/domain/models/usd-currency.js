"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const currency_1 = require("./currency");
const currency_2 = require("../utils/currency");
const UsdCurrency = new currency_1.default({
    abbr: currency_2.usdAbbr,
    name: 'Dolares',
    format: currency_2.usdFormat,
});
exports.default = UsdCurrency;
//# sourceMappingURL=usd-currency.js.map