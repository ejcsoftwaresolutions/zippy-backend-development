"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../aggregate/aggregate-root");
const id_1 = require("../id/id");
const utils_1 = require("../utils");
const user_role_1 = require("./user-role");
const DefaultProps = {};
class UserAccount extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.email;
    }
    get firstName() {
        return this.props.firstName;
    }
    get status() {
        return this.props.status;
    }
    get lastName() {
        return this.props.lastName;
    }
    get roles() {
        return this.props.roles;
    }
    get phone() {
        return this.props.phone;
    }
    get plainPassword() {
        return this.props.plainPassword;
    }
    get createdAt() {
        return this.props.createdAt;
    }
    static create(data) {
        return new UserAccount(data);
    }
    static fromPrimitives({ ...plainData }) {
        return new UserAccount({
            ...plainData,
            id: new id_1.default(plainData.id),
        });
    }
    addRole(newRole) {
        this.props.roles.push(newRole);
    }
    update(updates) {
        Object.assign(this, {
            ...this.props,
            ...updates,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown({ ...json, roles: this.props.roles });
    }
}
exports.default = UserAccount;
//# sourceMappingURL=user-account.js.map