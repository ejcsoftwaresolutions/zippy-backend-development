interface CurrencyProps {
    name: string;
    abbr: string;
    format?: (value: number) => string;
}
export interface CurrencyPrimitiveProps {
    name: string;
    abbr: string;
    format?: (value: number) => string;
}
export default class Currency {
    private props;
    constructor(props: CurrencyProps);
    get name(): string;
    get abbr(): string;
    static fromPrimitives(props: CurrencyPrimitiveProps): Currency;
    isEqual(otherCurrency: Currency): boolean;
    format(value: number): string;
    toPrimitives(): CurrencyPrimitiveProps;
}
export {};
