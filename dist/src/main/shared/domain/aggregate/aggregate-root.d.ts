import DomainEvent from '../bus/event/domain-event';
import Entity from '../entity/entity';
export default abstract class AggregateRoot<T> extends Entity<T> {
    private domainEvents;
    pullDomainEvents(): Array<DomainEvent<any>>;
    protected record(event: DomainEvent<any>): void;
}
