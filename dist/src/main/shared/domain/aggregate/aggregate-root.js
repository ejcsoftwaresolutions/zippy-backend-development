"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const entity_1 = require("../entity/entity");
class AggregateRoot extends entity_1.default {
    constructor() {
        super(...arguments);
        this.domainEvents = [];
    }
    pullDomainEvents() {
        const domainEvents = [...this.domainEvents];
        this.domainEvents = [];
        return domainEvents;
    }
    record(event) {
        this.domainEvents.push(event);
    }
}
exports.default = AggregateRoot;
//# sourceMappingURL=aggregate-root.js.map