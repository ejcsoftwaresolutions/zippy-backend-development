import ValueObject from '../value-object/value-object';
interface IdProps {
    value: string;
}
export default class Id extends ValueObject<IdProps> {
    constructor(id?: string);
    private gen;
    get value(): string;
    toPrimitives(): string;
    toString(): string;
}
export {};
