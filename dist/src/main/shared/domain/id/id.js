"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = require("firebase-admin");
const uuid_1 = require("uuid");
const value_object_1 = require("../value-object/value-object");
class Id extends value_object_1.default {
    constructor(id) {
        super({
            value: id,
        });
        if (!this.props.value) {
            this.props.value = this.gen();
        }
    }
    gen() {
        const id = firebase_admin_1.default.firestore().collection('test').doc().id;
        return id;
        return (0, uuid_1.v4)();
    }
    get value() {
        return this.props.value;
    }
    toPrimitives() {
        return this.value;
    }
    toString() {
        return this.value;
    }
}
exports.default = Id;
//# sourceMappingURL=id.js.map