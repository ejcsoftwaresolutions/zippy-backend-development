import { AnyObject } from '../types';
export declare namespace ObjectUtils {
    const omit: any;
    const omitUnknown: (object: AnyObject) => any;
    const omitBy: any;
    const pick: any;
    const pickBy: any;
    const keys: any;
    const find: any;
    const zip: any;
    const get: any;
    const merge: any;
}
export declare namespace ArrayUtils {
    const isArray: any;
    const filter: any;
    const differenceWith: any;
    const orderBy: any;
    const uniq: any;
    const uniqBy: any;
    const map: any;
    const flatten: any;
    const findIndex: any;
    const filterLike: (arr1: any[], fieldName: string, like: string) => any[];
}
export declare namespace DateTimeUtils {
    const startOfWeek: () => Date;
    const endOfWeek: () => Date;
    const differenceInDays: (a: Date, b: Date) => number;
    const differenceInMinutes: (a: Date, b: Date) => number;
    const startOfDate: (date: Date) => any;
    const endOfDate: (date: Date) => any;
    const addMonths: (a: Date, months: number) => any;
    const addDays: (a: Date, days: number) => any;
    const format: (date: Date, format: string) => any;
    const toString: (date: Date) => any;
}
