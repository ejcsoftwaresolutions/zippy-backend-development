"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DateTimeUtils = exports.ArrayUtils = exports.ObjectUtils = void 0;
const lodash_1 = require("lodash");
const moment = require('moment');
var ObjectUtils;
(function (ObjectUtils) {
    ObjectUtils.omit = lodash_1.omit;
    ObjectUtils.omitUnknown = (object) => {
        return (0, lodash_1.omitBy)(object, lodash_1.isNil);
    };
    ObjectUtils.omitBy = lodash_1.omitBy;
    ObjectUtils.pick = lodash_1.pick;
    ObjectUtils.pickBy = lodash_1.pickBy;
    ObjectUtils.keys = lodash_1.keys;
    ObjectUtils.find = lodash_1.find;
    ObjectUtils.zip = lodash_1.zipObject;
    ObjectUtils.get = lodash_1.get;
    ObjectUtils.merge = lodash_1.merge;
})(ObjectUtils = exports.ObjectUtils || (exports.ObjectUtils = {}));
var ArrayUtils;
(function (ArrayUtils) {
    ArrayUtils.isArray = lodash_1.isArray;
    ArrayUtils.filter = lodash_1.filter;
    ArrayUtils.differenceWith = lodash_1.differenceWith;
    ArrayUtils.orderBy = lodash_1.orderBy;
    ArrayUtils.uniq = lodash_1.uniq;
    ArrayUtils.uniqBy = lodash_1.uniqBy;
    ArrayUtils.map = lodash_1.map;
    ArrayUtils.flatten = lodash_1.flattenDeep;
    ArrayUtils.findIndex = lodash_1.findIndex;
    ArrayUtils.filterLike = (arr1, fieldName, like) => {
        return arr1.filter((dto) => {
            var _a;
            const name = (_a = dto[fieldName]) !== null && _a !== void 0 ? _a : '';
            const test = like;
            const reg1 = new RegExp('^' + test + '.*$', 'i');
            const reg2 = new RegExp('^.*' + test + '$', 'i');
            const regex3 = new RegExp([test].join('|'), 'i');
            const match = name.match(reg1);
            const match2 = name.match(reg2);
            const match3 = name.includes(test);
            const match4 = regex3.test(name);
            return !!match || !!match2 || !!match3 || !!match4;
        });
    };
})(ArrayUtils = exports.ArrayUtils || (exports.ArrayUtils = {}));
var DateTimeUtils;
(function (DateTimeUtils) {
    DateTimeUtils.startOfWeek = () => {
        return moment().startOf('week').toDate();
    };
    DateTimeUtils.endOfWeek = () => {
        return moment().endOf('week').toDate();
    };
    DateTimeUtils.differenceInDays = (a, b) => {
        return moment(a).diff(moment(b), 'days');
    };
    DateTimeUtils.differenceInMinutes = (a, b) => {
        return moment(a).diff(moment(b), 'minutes');
    };
    DateTimeUtils.startOfDate = (date) => {
        return moment(date).clone().startOf('day').toDate();
    };
    DateTimeUtils.endOfDate = (date) => {
        return moment(date).clone().endOf('day').toDate();
    };
    DateTimeUtils.addMonths = (a, months) => {
        return moment(a).clone().add(months, 'months').toDate();
    };
    DateTimeUtils.addDays = (a, days) => {
        return moment(a).clone().add(days, 'days').toDate();
    };
    DateTimeUtils.format = (date, format) => {
        return moment(date).format(format);
    };
    DateTimeUtils.toString = (date) => {
        return moment(date).toString();
    };
})(DateTimeUtils = exports.DateTimeUtils || (exports.DateTimeUtils = {}));
//# sourceMappingURL=index.js.map