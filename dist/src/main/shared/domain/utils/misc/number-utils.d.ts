declare const NumberUtils: {
    format: (value: number, formatString: string) => string;
};
export default NumberUtils;
