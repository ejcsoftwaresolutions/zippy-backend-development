export declare const bsFormat: (value: number) => string;
export declare const bsAbbr = "Bs.S";
export declare const usdFormat: (value: number) => string;
export declare const usdAbbr = "USD";
