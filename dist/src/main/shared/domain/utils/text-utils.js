"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TextUtils = {
    truncate(text, max) {
        if (!text)
            return '';
        if (text.length <= max)
            return text;
        return text.slice(0, max) + '...';
    },
    insertAt(str, sub, pos) {
        return `${str.slice(0, pos)}${sub}${str.slice(pos)}`;
    },
    capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    },
};
exports.default = TextUtils;
//# sourceMappingURL=text-utils.js.map