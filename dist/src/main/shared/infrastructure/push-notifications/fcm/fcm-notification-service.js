"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const push_notification_1 = require("../../../domain/push-notifications/push-notification");
const push_notificator_1 = require("../../../domain/push-notifications/push-notificator");
const google_auth_library_1 = require("google-auth-library");
const axios_1 = require("axios");
const path = require('path');
class FCMNotificationService {
    constructor(serverKey, projectId, serviceAccountLocation) {
        this.serverKey = serverKey;
        this.projectId = projectId;
        this.serviceAccountLocation = serviceAccountLocation;
    }
    async send(notification) {
        var _a;
        const url = `https://fcm.googleapis.com/v1/projects/${this.projectId}/messages:send`;
        const token = await this.getAccessToken();
        const notificationData = notification.toPrimitives();
        const imageUrl = notificationData.imageUrl;
        try {
            const result = await axios_1.default.post(url, {
                message: {
                    token: notificationData.to,
                    data: {
                        experienceId: notificationData.appId,
                        title: notificationData.title,
                        message: notificationData.content,
                        metadata: JSON.stringify((_a = notificationData.metadata) !== null && _a !== void 0 ? _a : {}),
                    },
                    notification: {
                        title: notificationData.title,
                        body: notificationData.content,
                        ...(imageUrl
                            ? {
                                image: imageUrl,
                            }
                            : {}),
                    },
                    android: {
                        priority: 'high',
                        notification: {
                            channel_id: notificationData.channelId,
                        },
                    },
                    apns: {
                        headers: {
                            'apns-priority': '10',
                        },
                        payload: {
                            aps: {
                                sound: 'notification.wav',
                                ...(imageUrl ? { 'mutable-content': 1 } : {}),
                            },
                            alert: {
                                title: notificationData.title,
                                body: notificationData.content,
                                sound: 'notification.wav',
                            },
                        },
                        ...(imageUrl
                            ? {
                                fcm_options: {
                                    image: imageUrl,
                                },
                            }
                            : {}),
                    },
                },
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: 'Bearer ' + token,
                },
            });
            console.log(result.data);
            return result.data;
        }
        catch (e) {
            console.log(e.response.data.error);
            return {
                error: e.response.data.error,
            };
        }
    }
    async transformApnsToFcm(apnsToken, application) {
        var _a, _b;
        const url = 'https://iid.googleapis.com/iid/v1:batchImport';
        const result = await axios_1.default.post(url, {
            application: application,
            sandbox: false,
            apns_tokens: [apnsToken],
        }, {
            headers: {
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + this.serverKey,
            },
        });
        return (_b = (_a = result.data) === null || _a === void 0 ? void 0 : _a.results[0]) === null || _b === void 0 ? void 0 : _b.registration_token;
    }
    async getAccessToken() {
        return new Promise((resolve, reject) => {
            const key = require(path.join(__dirname, '../../../../../../../' + this.serviceAccountLocation));
            const jwtClient = new google_auth_library_1.JWT(key.client_email, null, key.private_key, ['https://www.googleapis.com/auth/firebase.messaging'], null);
            jwtClient.authorize(function (err, tokens) {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(tokens.access_token);
            });
        });
    }
}
exports.default = FCMNotificationService;
//# sourceMappingURL=fcm-notification-service.js.map