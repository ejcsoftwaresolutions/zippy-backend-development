import PushNotification from '@shared/domain/push-notifications/push-notification';
import PushNotificator from '@shared/domain/push-notifications/push-notificator';
export default class FCMNotificationService implements PushNotificator {
    private serverKey;
    private projectId;
    private serviceAccountLocation;
    constructor(serverKey: string, projectId: string, serviceAccountLocation: string);
    send(notification: PushNotification): Promise<any>;
    transformApnsToFcm(apnsToken: string, application: string): Promise<string>;
    private getAccessToken;
}
