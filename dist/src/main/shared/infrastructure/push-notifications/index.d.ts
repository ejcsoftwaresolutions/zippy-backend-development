import { ConfigService } from '@nestjs/config';
import PushNotification from '@shared/domain/push-notifications/push-notification';
import PushNotificator from '@shared/domain/push-notifications/push-notificator';
export default class MobilePushNotificationService implements PushNotificator {
    private configService;
    private androidPushService;
    constructor(configService: ConfigService);
    send(notification: PushNotification): Promise<any>;
    transformApnsToFcm(apnsToken: string, application: string): Promise<string>;
}
