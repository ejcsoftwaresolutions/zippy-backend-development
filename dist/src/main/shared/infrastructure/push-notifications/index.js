"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const config_1 = require("@nestjs/config");
const service_1 = require("../../domain/decorators/service");
const push_notification_1 = require("../../domain/push-notifications/push-notification");
const push_notificator_1 = require("../../domain/push-notifications/push-notificator");
const fcm_notification_service_1 = require("./fcm/fcm-notification-service");
let MobilePushNotificationService = class MobilePushNotificationService {
    constructor(configService) {
        this.configService = configService;
        this.androidPushService = new fcm_notification_service_1.default(this.configService.get('FCM_SERVER_KEY'), this.configService.get('FB_PROJECT_ID'), this.configService.get('GOOGLE_APPLICATION_CREDENTIALS'));
    }
    async send(notification) {
        return this.androidPushService.send(notification);
    }
    async transformApnsToFcm(apnsToken, application) {
        return this.androidPushService.transformApnsToFcm(apnsToken, application);
    }
};
MobilePushNotificationService = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__metadata("design:paramtypes", [config_1.ConfigService])
], MobilePushNotificationService);
exports.default = MobilePushNotificationService;
//# sourceMappingURL=index.js.map