"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_1 = require("../../domain/decorators/service");
const excel_generator_1 = require("../../domain/excel/excel-generator");
const excel_request_1 = require("../../domain/excel/excel-request");
const decorators_1 = require("../../domain/decorators");
const storage_uploader_1 = require("../../domain/storage/storage-uploader");
const excel = require('node-excel-export');
let FirebaseExcelGenerator = class FirebaseExcelGenerator {
    constructor(fileUploader) {
        this.fileUploader = fileUploader;
    }
    async generate(excelRequest) {
        const fileName = excelRequest.uploadFolderUrl;
        const excel = this.buildExcel(excelRequest.data, excelRequest.specification);
        const response = await this.fileUploader.uploadFile(excel, undefined, fileName);
        return {
            url: response.url,
        };
    }
    buildExcel(data, specification, heading) {
        return excel.buildExport([
            {
                name: 'Report',
                heading: heading,
                specification: specification,
                data: data,
            },
        ]);
    }
};
FirebaseExcelGenerator = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('file.uploader')),
    tslib_1.__metadata("design:paramtypes", [Object])
], FirebaseExcelGenerator);
exports.default = FirebaseExcelGenerator;
//# sourceMappingURL=firebase-excel-generator.js.map