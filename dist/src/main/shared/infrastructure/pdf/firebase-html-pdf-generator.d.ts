import PdfGenerator from '@shared/domain/pdf/pdf-generator';
import PDFRequest from '@shared/domain/pdf/pdf-request';
export default class FirebaseHtmlPdfGenerator implements PdfGenerator {
    private pdfTemplatePath;
    constructor(pdfTemplatePath: string);
    generate(pdf: PDFRequest): Promise<{
        url: string;
    }>;
    private parseTemplate;
}
