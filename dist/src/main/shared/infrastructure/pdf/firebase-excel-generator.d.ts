import ExcelGenerator from '@shared/domain/excel/excel-generator';
import ExcelRequest from '@shared/domain/excel/excel-request';
import FileUploader from '@shared/domain/storage/storage-uploader';
export default class FirebaseExcelGenerator implements ExcelGenerator {
    private fileUploader;
    constructor(fileUploader: FileUploader);
    generate(excelRequest: ExcelRequest): Promise<{
        url: string;
    }>;
    private buildExcel;
}
