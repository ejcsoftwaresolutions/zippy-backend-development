"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_1 = require("../../domain/decorators/service");
const pdf_generator_1 = require("../../domain/pdf/pdf-generator");
const pdf_request_1 = require("../../domain/pdf/pdf-request");
const admin = require("firebase-admin");
const id_1 = require("../../domain/id/id");
const decorators_1 = require("../../domain/decorators");
const moment = require("moment");
const ejs = require('ejs');
const htmlToPDF = require('html-pdf');
let FirebaseHtmlPdfGenerator = class FirebaseHtmlPdfGenerator {
    constructor(pdfTemplatePath) {
        this.pdfTemplatePath = pdfTemplatePath;
    }
    async generate(pdf) {
        const envFolder = process.env.NODE_ENV === 'production' ? '' : 'dev/';
        const fileTemplate = pdf.template;
        const fileName = `${envFolder}${pdf.uploadUrl}`;
        const fileRef = admin.storage().bucket().file(fileName, {});
        const parsedContent = await this.parseTemplate(fileTemplate, pdf.variables);
        return new Promise((resolve, reject) => {
            var _a;
            htmlToPDF
                .create(parsedContent, {
                timeout: '100000',
                ...((_a = pdf.config) !== null && _a !== void 0 ? _a : {}),
            })
                .toStream(async (err, stream) => {
                if (err) {
                    console.log("Error al generar pdf");
                    console.log(err);
                    reject(err);
                    return;
                }
                await stream.pipe(fileRef.createWriteStream({
                    metadata: {
                        contentType: 'application/pdf',
                        metadata: {
                            firebaseStorageDownloadTokens: new id_1.default().value,
                            lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                        },
                    },
                    resumable: false,
                    public: true,
                }));
                resolve({
                    url: fileRef.publicUrl(),
                });
            });
        });
    }
    async parseTemplate(template, variables) {
        return new Promise(async (resolve, reject) => {
            const string = await ejs.render(template, variables, {});
            resolve(string);
        });
    }
};
FirebaseHtmlPdfGenerator = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('pdf.templates.root.folder')),
    tslib_1.__metadata("design:paramtypes", [String])
], FirebaseHtmlPdfGenerator);
exports.default = FirebaseHtmlPdfGenerator;
//# sourceMappingURL=firebase-html-pdf-generator.js.map