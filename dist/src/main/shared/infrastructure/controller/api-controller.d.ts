/// <reference types="node" />
import Query from '@shared/domain/bus/query/query';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { QueryResponse } from '@shared/domain/bus/query/query-response';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import Command from '../../domain/bus/command/command';
import CommandBus from '../../domain/bus/command/command-bus';
export default abstract class ApiController {
    private commandBus;
    private queryBus;
    private multipartHandler;
    constructor(commandBus: CommandBus, queryBus: QueryBus, multipartHandler: MultipartHandler<Request, Response>);
    protected ask(query: Query): Promise<QueryResponse>;
    protected getQueryOrderFromDto(order: string[]): {
        orderBy: string;
        orderType: string;
    }[];
    protected getFilesAndFields<T>(request: Request, response: Response): Promise<{
        files: any;
        fields: T;
    }>;
    protected pickFile(path: string, files: any): any;
    protected base64ToBuffer(base64String: string): Buffer;
    protected dispatch(command: Command): Promise<any>;
}
