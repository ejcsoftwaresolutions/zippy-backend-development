"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const query_1 = require("../../domain/bus/query/query");
const query_bus_1 = require("../../domain/bus/query/query-bus");
const query_response_1 = require("../../domain/bus/query/query-response");
const decorators_1 = require("../../domain/decorators");
const multipart_handler_1 = require("../../domain/storage/multipart-handler");
const utils_1 = require("../../domain/utils");
let ApiController = class ApiController {
    constructor(commandBus, queryBus, multipartHandler) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
        this.multipartHandler = multipartHandler;
    }
    ask(query) {
        return this.queryBus.ask(query);
    }
    getQueryOrderFromDto(order) {
        const orderString = order;
        if (order.length === 0)
            return [
                {
                    orderBy: 'id',
                    orderType: 'DESC',
                },
            ];
        return order.map && order.length > 1
            ?
                order.map((orderString) => ({
                    orderBy: orderString.split(' ')[0],
                    orderType: orderString.split(' ')[1],
                }))
            : [
                {
                    orderBy: orderString.split(' ')[0],
                    orderType: orderString.split(' ')[1],
                },
            ];
    }
    async getFilesAndFields(request, response) {
        const { files, fields } = await this.multipartHandler.getFilesAndFields(request, response);
        return { files: files, fields };
    }
    pickFile(path, files) {
        return utils_1.ObjectUtils.get(files, path);
    }
    base64ToBuffer(base64String) {
        if (!base64String)
            return;
        const matches = base64String.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        if (!matches)
            throw new Error('Invalid base64 file string');
        const data = Buffer.from(matches[2], 'base64');
        return data;
    }
    dispatch(command) {
        return this.commandBus.dispatch(command);
    }
};
ApiController = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('query.bus')),
    tslib_1.__param(2, (0, decorators_1.inject)('multipart.handler')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], ApiController);
exports.default = ApiController;
//# sourceMappingURL=api-controller.js.map