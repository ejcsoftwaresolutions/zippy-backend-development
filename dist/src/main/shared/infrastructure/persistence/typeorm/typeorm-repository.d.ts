import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { EntityManager, EntityTarget, Repository } from 'typeorm';
export default abstract class TypeOrmRepository<T> {
    protected entityManager: EntityManager;
    constructor();
    abstract getEntityClass(): EntityTarget<T>;
    protected save(aggregateRoot: AggregateRoot<any>, dataMapper: any): Promise<void>;
    protected remove<T = any>(entity: AggregateRoot<T>): Promise<void>;
    protected repository(): Repository<T>;
}
