"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../domain/aggregate/aggregate-root");
const typeorm_1 = require("typeorm");
class TypeOrmRepository {
    constructor() {
    }
    async save(aggregateRoot, dataMapper) {
        const entity = dataMapper.toPersistence(aggregateRoot);
        await (0, typeorm_1.getManager)().save(entity);
    }
    async remove(entity) {
        await (0, typeorm_1.getManager)().remove(entity);
    }
    repository() {
        const name = this.getEntityClass();
        return (0, typeorm_1.getManager)().getRepository(name);
    }
}
exports.default = TypeOrmRepository;
//# sourceMappingURL=typeorm-repository.js.map