import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { BaseFirestoreRepository, EntityConstructorOrPath, IEntity } from 'fireorm';
export default abstract class FireOrmRepository<T extends IEntity> {
    constructor();
    abstract getEntityClass(): EntityConstructorOrPath<T>;
    getEntityRepository<M extends IEntity>(target: EntityConstructorOrPath<M>): BaseFirestoreRepository<M>;
    protected save(aggregateRoot: AggregateRoot<any>, dataMapper: any): Promise<void>;
    protected createInBulk(aggregateRoots: AggregateRoot<any>[], dataMapper: any): Promise<void>;
    protected updateInBulk(aggregateRoots: AggregateRoot<any>[], dataMapper: any): Promise<void>;
    protected remove(entity: AggregateRoot<T>): Promise<void>;
    protected findById(id: string): Promise<T>;
    protected update(item: T): Promise<void>;
    protected repository(): BaseFirestoreRepository<T>;
}
