"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../domain/aggregate/aggregate-root");
const fireorm_1 = require("fireorm");
class FireOrmRepository {
    constructor() { }
    getEntityRepository(target) {
        return (0, fireorm_1.getRepository)(target);
    }
    async save(aggregateRoot, dataMapper) {
        const entity = dataMapper.toPersistence(aggregateRoot);
        await this.repository().create(entity);
    }
    async createInBulk(aggregateRoots, dataMapper) {
        const entities = dataMapper.toPersistenceFromArray(aggregateRoots);
        const batch = this.repository().createBatch();
        entities.forEach((product) => {
            return batch.create(product);
        });
        await batch.commit();
    }
    async updateInBulk(aggregateRoots, dataMapper) {
        const entities = dataMapper.toPersistenceFromArray(aggregateRoots);
        const batch = this.repository().createBatch();
        entities.forEach((product) => {
            return batch.update(product);
        });
        await batch.commit();
    }
    async remove(entity) {
        this.repository().delete(entity.getId());
    }
    async findById(id) {
        return await this.repository().findById(id);
    }
    async update(item) {
        await this.repository().update(item);
    }
    repository() {
        const entityClass = this.getEntityClass();
        return (0, fireorm_1.getRepository)(entityClass);
    }
}
exports.default = FireOrmRepository;
//# sourceMappingURL=fireorm-repository.js.map