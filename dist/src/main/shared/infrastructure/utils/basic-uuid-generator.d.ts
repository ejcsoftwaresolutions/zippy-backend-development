import CodeGenerator from '../../domain/utils/code-generator';
export default class BasicUuidGenerator implements CodeGenerator {
    generate(): string;
}
