"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SimpleCodeGenerator {
    generate(length) {
        return this.makeId(length !== null && length !== void 0 ? length : 5);
    }
    makeId(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}
exports.default = SimpleCodeGenerator;
//# sourceMappingURL=simple-code-generator.js.map