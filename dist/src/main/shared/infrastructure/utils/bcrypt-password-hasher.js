"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../domain/decorators");
const bcryptjs_1 = require("bcryptjs");
let BcryptHasher = class BcryptHasher {
    constructor(rounds) {
        this.rounds = rounds;
    }
    async hashPassword(password) {
        const salt = await (0, bcryptjs_1.genSalt)(this.rounds);
        return (0, bcryptjs_1.hash)(password, salt);
    }
    async comparePassword(providedPass, storedPass) {
        const passwordIsMatched = await (0, bcryptjs_1.compare)(providedPass, storedPass);
        return passwordIsMatched;
    }
};
BcryptHasher = tslib_1.__decorate([
    (0, decorators_1.injectable)(),
    tslib_1.__param(0, (0, decorators_1.inject)('utils.hasher.round')),
    tslib_1.__metadata("design:paramtypes", [Number])
], BcryptHasher);
exports.default = BcryptHasher;
//# sourceMappingURL=bcrypt-password-hasher.js.map