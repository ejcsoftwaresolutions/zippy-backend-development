import PasswordHasher from '../../domain/utils/password-hasher';
export default class BcryptHasher implements PasswordHasher<string> {
    private readonly rounds;
    constructor(rounds: number);
    hashPassword(password: string): Promise<string>;
    comparePassword(providedPass: string, storedPass: string): Promise<boolean>;
}
