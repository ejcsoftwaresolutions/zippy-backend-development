import TimeHumanizer, { HumanizerConfig } from '@shared/domain/utils/time-humanizer';
export default class BasicTimeHumanizer implements TimeHumanizer {
    humanize(timeInMs: number, config: HumanizerConfig): string;
}
