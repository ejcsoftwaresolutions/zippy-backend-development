"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Timer = exports.pad = void 0;
const events_1 = require("events");
const events = typeof module !== 'undefined' &&
    module.exports &&
    typeof require === 'function'
    ? require('events')
    : undefined;
function hasDOM() {
    return typeof document !== 'undefined';
}
function hasEventEmitter() {
    return events;
}
function pad(pad, string, padLeft) {
    if (typeof string === 'undefined')
        return pad;
    if (padLeft) {
        return (pad + string).slice(-pad.length);
    }
    else {
        return (string + pad).substring(0, pad.length);
    }
}
exports.pad = pad;
class Timer extends events_1.EventEmitter {
    constructor(options = {}) {
        super();
        this.startTime = 0;
        this.endTime = null;
        this.updateFrequency = 100;
        this.selfAdjust = true;
        this.countdown = false;
        this.animationFrame = false;
        this._timer;
        this._startTime = 0;
        this._timeAtStart = 0;
        this._currentTime = 0;
        this._expected = 0;
        this._drift = 0;
        this._isRunning = false;
        this._isPaused = false;
        this._eventEmitter = hasDOM()
            ? document.createElement('span')
            : hasEventEmitter()
                ? new events.EventEmitter()
                : undefined;
        this.on = this.addEventListener;
        this.off = this.removeEventListener;
        Object.assign(this, options);
    }
    _instance() {
        this._isRunning = true;
        this._currentTime = this.countdown
            ? this._timeAtStart - new Date() + this._startTime
            : new Date() - this._timeAtStart + this._startTime;
        if (this.selfAdjust && !this.animationFrame) {
            this._drift = this.countdown
                ? this._expected - this._currentTime
                : this._currentTime - this._expected;
        }
        if (this.countdown
            ? (this.endTime !== null && this._currentTime <= this.endTime) ||
                this._currentTime <= 0
            : this.endTime !== null && this._currentTime >= this.endTime) {
            this._currentTime = this.endTime;
            this._isRunning = false;
            this.dispatchEvent('update', this);
            this.dispatchEvent('end', this);
            return;
        }
        this.dispatchEvent('update', this);
        this.countdown
            ? (this._expected -= this.updateFrequency)
            : (this._expected += this.updateFrequency);
        this._timer = this.animationFrame
            ? requestAnimationFrame(() => this._instance())
            : setTimeout(() => this._instance(), this.selfAdjust
                ? Math.max(0, this.updateFrequency - this._drift)
                : this.updateFrequency);
    }
    start(options = {}) {
        if (this._isRunning)
            return;
        if (this._isPaused) {
            this._isPaused = false;
            this._timeAtStart = new Date().getTime();
            this._startTime = this._currentTime;
            this._expected = this._startTime;
            this.dispatchEvent('start', this);
            this._instance();
            return;
        }
        Object.assign(this, options);
        this._timeAtStart = new Date().getTime();
        this._startTime = this.startTime;
        this._currentTime = this.startTime;
        this._expected = this.startTime;
        this.dispatchEvent('start', this);
        this._instance();
    }
    destroy() {
        this.stop();
        this._eventEmitter.removeAllListeners();
    }
    stop() {
        if (!this._isRunning || this._isPaused)
            return;
        this._isRunning = false;
        this._isPaused = false;
        this.animationFrame
            ? cancelAnimationFrame(this._timer)
            : clearTimeout(this._timer);
        this.dispatchEvent('stop', this);
    }
    pause() {
        if (!this._isRunning || this._isPaused)
            return;
        this._isRunning = false;
        this._isPaused = true;
        this.animationFrame
            ? cancelAnimationFrame(this._timer)
            : clearTimeout(this._timer);
        this.dispatchEvent('pause', this);
    }
    reset(options = {}) {
        this.stop();
        this._isRunning = false;
        this._isPaused = false;
        Object.assign(this, options);
        this._currentTime = this.startTime;
        this._expected = this.startTime;
        this.dispatchEvent('update', this);
        this.dispatchEvent('reset', this);
    }
    adjustTime(val) {
        if (!this._isRunning || this._isPaused)
            return;
        this._expected = this._expected + val;
        this._startTime = this._startTime + val;
    }
    get getTime() {
        return {
            milliseconds: Math.floor(this._currentTime % 1000),
            millisecondsTotal: Math.floor(this._currentTime),
            hundredths: Math.floor(((this._currentTime % 1000) / 10).toFixed(0)),
            hundredthsTotal: Math.floor((this._currentTime / 10).toFixed(0)),
            tenths: Math.floor(((this._currentTime % 1000) / 100).toFixed(0)),
            tenthsTotal: Math.floor((this._currentTime % 1000).toFixed(0)),
            seconds: Math.floor((this._currentTime / 1000) % 60),
            secondsTotal: Math.floor(this._currentTime / 1000),
            minutes: Math.floor((this._currentTime / 1000 / 60) % 60),
            minutesTotal: Math.floor(this._currentTime / 1000 / 60),
            hours: Math.floor((this._currentTime / 1000 / 60 / 60) % 24),
            hoursTotal: Math.floor(this._currentTime / 1000 / 60 / 60),
            days: Math.floor(this._currentTime / 1000 / 60 / 60 / 24),
            daysTotal: Math.floor(this._currentTime / 1000 / 60 / 60 / 24),
        };
    }
    get isRunning() {
        return this._isRunning;
    }
    get isPaused() {
        return this._isPaused;
    }
    addEventListener(event, listener) {
        if (hasDOM()) {
            this._eventEmitter.addEventListener(event, listener);
        }
        else if (hasEventEmitter()) {
            this._eventEmitter.on(event, listener);
        }
    }
    removeEventListener(event, listener) {
        if (hasDOM()) {
            this._eventEmitter.removeEventListener(event, listener);
        }
        else if (hasEventEmitter()) {
            this._eventEmitter.removeListener(event, listener);
        }
    }
    dispatchEvent(event, data) {
        if (hasDOM()) {
            this._eventEmitter.dispatchEvent(new CustomEvent(event, { detail: data }));
        }
        else if (hasEventEmitter()) {
            this._eventEmitter.emit(event, data);
        }
    }
}
exports.Timer = Timer;
//# sourceMappingURL=interval-timer.js.map