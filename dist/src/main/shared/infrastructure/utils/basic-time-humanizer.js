"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../domain/decorators");
const time_humanizer_1 = require("../../domain/utils/time-humanizer");
const humanizeDuration = require('humanize-duration');
let BasicTimeHumanizer = class BasicTimeHumanizer {
    humanize(timeInMs, config) {
        var _a;
        return humanizeDuration(timeInMs, {
            language: (_a = config === null || config === void 0 ? void 0 : config.language) !== null && _a !== void 0 ? _a : 'shortEs',
            units: ['h', 'm'],
            maxDecimalPoints: 2,
            languages: {
                shortEs: {
                    y: () => 'a',
                    mo: () => 'mo',
                    w: () => 'semanas',
                    d: () => 'd',
                    h: () => 'h',
                    m: () => 'min',
                    s: () => 'seg',
                    ms: () => 'ms',
                },
            },
        });
    }
};
BasicTimeHumanizer = tslib_1.__decorate([
    (0, decorators_1.injectable)()
], BasicTimeHumanizer);
exports.default = BasicTimeHumanizer;
//# sourceMappingURL=basic-time-humanizer.js.map