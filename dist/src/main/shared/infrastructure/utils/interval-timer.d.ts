export function pad(pad: string, string: string, padLeft: boolean): string;
export class Timer extends EventEmitter {
    constructor(options?: {
        startTime?: number;
        endTime?: number;
        updateFrequency?: number;
        selfAdjust?: boolean;
        countdown?: boolean;
        animationFrame?: boolean;
    });
    startTime: number;
    endTime: any;
    updateFrequency: number;
    selfAdjust: boolean;
    countdown: boolean;
    animationFrame: boolean;
    _startTime: number;
    _timeAtStart: number;
    _currentTime: number;
    _expected: number;
    _drift: number;
    _isRunning: boolean;
    _isPaused: boolean;
    _eventEmitter: EventEmitter | HTMLSpanElement;
    on: (event: string, listener: Function) => void;
    off: (event: string, listener: Function) => void;
    private _instance;
    _timer: number | NodeJS.Timeout;
    start(options?: {
        startTime?: number;
        endTime?: number;
        updateFrequency?: number;
        selfAdjust?: boolean;
        countdown?: boolean;
        animationFrame?: boolean;
    }): void;
    destroy(): void;
    stop(): void;
    pause(): void;
    reset(options?: {
        startTime?: number;
        endTime?: number;
        updateFrequency?: number;
        selfAdjust?: boolean;
        countdown?: boolean;
        animationFrame?: boolean;
    }): void;
    adjustTime(val: number): void;
    get getTime(): any;
    get isRunning(): boolean;
    get isPaused(): boolean;
    addEventListener(event: string, listener: Function): void;
    removeEventListener(event: string, listener: Function): void;
    dispatchEvent(event: string, data: any): void;
}
import { EventEmitter } from "events";
