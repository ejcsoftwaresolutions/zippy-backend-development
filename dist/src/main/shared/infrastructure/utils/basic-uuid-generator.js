"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = require("uuid");
class BasicUuidGenerator {
    generate() {
        return (0, uuid_1.v4)();
    }
}
exports.default = BasicUuidGenerator;
//# sourceMappingURL=basic-uuid-generator.js.map