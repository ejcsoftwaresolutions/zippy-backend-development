import CodeGenerator from '../../domain/utils/code-generator';
export default class SimpleCodeGenerator implements CodeGenerator {
    generate(length?: number): string;
    private makeId;
}
