import Translator from '@shared/domain/translations/translator';
import { AnyObject } from '@shared/domain/types';
export default class PolyglotTranslator implements Translator {
    private polyglot;
    constructor();
    translate(key: string, params?: AnyObject, lang?: string): string;
}
