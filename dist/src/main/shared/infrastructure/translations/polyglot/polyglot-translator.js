"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const translator_1 = require("../../../domain/translations/translator");
const types_1 = require("../../../domain/types");
const node_polyglot_1 = require("node-polyglot");
class PolyglotTranslator {
    constructor() {
        this.polyglot = new node_polyglot_1.default();
    }
    translate(key, params, lang) {
        return this.polyglot.t(key, params);
    }
}
exports.default = PolyglotTranslator;
//# sourceMappingURL=polyglot-translator.js.map