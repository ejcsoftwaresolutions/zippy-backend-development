"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_1 = require("../../domain/decorators/service");
const storage_uploader_1 = require("../../domain/storage/storage-uploader");
const file_upload_response_1 = require("../../domain/storage/file-upload-response");
const collection_1 = require("../../domain/value-object/collection");
const storage_types_1 = require("../../domain/storage/storage-types");
const firebase_admin_1 = require("firebase-admin");
const uuid_1 = require("uuid");
const FileType = require('file-type');
const uuid = require('uuid');
const moment = require("moment");
let FirebaseUploader = class FirebaseUploader {
    async uploadFile(file, name = `${(0, uuid_1.v4)()}`, path, key) {
        const buffer = Buffer.from(file);
        const type = await FileType.fromBuffer(buffer);
        const extension = type.ext;
        return new Promise(async (resolve, reject) => {
            const envFolder = process.env.NODE_ENV === 'production' ? '' : 'dev/';
            const fileName = `${path ? envFolder + path + '/' : ''}${name}.${extension === 'xml' ? 'svg' : extension}`;
            const fileRef = firebase_admin_1.default.storage().bucket().file(fileName, {});
            await fileRef.save(buffer, {
                public: true,
                metadata: {
                    metadata: {
                        firebaseStorageDownloadTokens: uuid.v4(),
                        lastUpdate: moment().format('DD/MM/YYYY HH:mm:ss'),
                    },
                },
                resumable: false,
            });
            resolve({
                key: key,
                url: fileRef.publicUrl(),
                mimeType: extension,
            });
        });
    }
    async uploadFiles(files) {
        const promises = files.map((item) => this.uploadFile(item.file, item.name, item.filePath, item.fileKey));
        const res = await Promise.all(promises);
        return new collection_1.default(res);
    }
};
FirebaseUploader = tslib_1.__decorate([
    (0, service_1.default)()
], FirebaseUploader);
exports.default = FirebaseUploader;
//# sourceMappingURL=firebase-uploader.js.map