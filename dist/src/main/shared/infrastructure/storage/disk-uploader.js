"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const config_1 = require("@nestjs/config");
const decorators_1 = require("../../domain/decorators");
const service_1 = require("../../domain/decorators/service");
const file_upload_response_1 = require("../../domain/storage/file-upload-response");
const storage_types_1 = require("../../domain/storage/storage-types");
const storage_uploader_1 = require("../../domain/storage/storage-uploader");
const collection_1 = require("../../domain/value-object/collection");
const uuid_1 = require("uuid");
const fs = require('fs');
const stream = require('stream');
const FileType = require('file-type');
const mkdirp = require('mkdirp');
const { Duplex } = stream;
function bufferToStream(buffer) {
    const duplexStream = new Duplex();
    duplexStream.push(buffer);
    duplexStream.push(null);
    return duplexStream;
}
let DiskUploader = class DiskUploader {
    constructor(storageDirectory, configService) {
        this.storageDirectory = storageDirectory;
        this.configService = configService;
    }
    async uploadFile(file, name = `${(0, uuid_1.v4)()}`, path) {
        const publicUrl = this.configService.get('PUBLIC_URL');
        const buffer = Buffer.from(file);
        const type = await FileType.fromBuffer(buffer);
        const extension = type.ext;
        const fileName = `${name}.${extension}`;
        const pathRoute = path ? path + '/' : '';
        const uploadDir = this.storageDirectory + '/' + pathRoute;
        const uploadFolder = this.storageDirectory.slice(this.storageDirectory.lastIndexOf('/') + 1, this.storageDirectory.length);
        const fileUrl = `${publicUrl}/${uploadFolder}/${pathRoute}${fileName}`;
        await mkdirp(uploadDir, '0777');
        try {
            fs.writeFileSync(uploadDir + fileName, file, 'utf8');
        }
        catch (error) {
            throw new Error(error);
        }
        return {
            url: fileUrl,
            mimeType: extension,
        };
    }
    async uploadFiles(files) {
        const filesToUpload = files.map((f) => {
            return this.uploadFile(f.file, f.name, f.filePath);
        });
        const response = await Promise.all(filesToUpload);
        return new collection_1.default(response);
    }
};
DiskUploader = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('storage.upload.path')),
    tslib_1.__metadata("design:paramtypes", [String, config_1.ConfigService])
], DiskUploader);
exports.default = DiskUploader;
//# sourceMappingURL=disk-uploader.js.map