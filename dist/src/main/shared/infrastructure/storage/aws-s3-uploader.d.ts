import { ConfigService } from '@nestjs/config';
import { FileUploadResponse } from '@shared/domain/storage/file-upload-response';
import { UploadFile } from '@shared/domain/storage/storage-types';
import FileUploader from '@shared/domain/storage/storage-uploader';
import Collection from '@shared/domain/value-object/collection';
export default class S3Uploader implements FileUploader {
    private configService;
    private s3;
    constructor(configService: ConfigService);
    uploadFile(file: UploadFile, name?: string, path?: string, key?: string): Promise<FileUploadResponse>;
    uploadFiles(files: Collection<{
        file: UploadFile;
        name?: string;
        fileKey?: string;
        filePath?: string;
    }>): Promise<Collection<FileUploadResponse>>;
}
