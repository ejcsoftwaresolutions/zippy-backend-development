import { ConfigService } from '@nestjs/config';
import { FileUploadResponse } from '@shared/domain/storage/file-upload-response';
import { UploadFile } from '@shared/domain/storage/storage-types';
import FileUploader from '@shared/domain/storage/storage-uploader';
import Collection from '@shared/domain/value-object/collection';
export default class DiskUploader implements FileUploader {
    private storageDirectory;
    private configService;
    constructor(storageDirectory: string, configService: ConfigService);
    uploadFile(file: UploadFile, name?: string, path?: string): Promise<FileUploadResponse>;
    uploadFiles(files: Collection<{
        file: UploadFile;
        name?: string;
        fileKey?: string;
        filePath?: string;
    }>): Promise<Collection<FileUploadResponse>>;
}
