"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../domain/decorators");
const service_1 = require("../../domain/decorators/service");
const multipart_handler_1 = require("../../domain/storage/multipart-handler");
const multer = require('fastify-multer');
const fs = require('fs');
let MulterMultipartHandler = class MulterMultipartHandler {
    constructor(options = {}) {
        this.options = options;
        if (!this.options.storage) {
            this.options.storage = multer.memoryStorage();
        }
        this.multer = multer(this.options).any();
    }
    getFilesAndFields(request, response) {
        return new Promise((resolve, reject) => {
            const handle = async (err) => {
                if (err)
                    reject(err);
                else {
                    const { files, body: fields } = request;
                    const multerFiles = (files !== null && files !== void 0 ? files : []);
                    const cleanFilesPromises = multerFiles.map((file) => {
                        return new Promise((resolve) => {
                            const readFile = fs.readFileSync(file.path);
                            resolve(readFile);
                        });
                    });
                    const cleanFiles = await Promise.all(cleanFilesPromises);
                    const bufferFiles = cleanFiles.map((file) => {
                        const encoded = file.toString('base64');
                        return Buffer.from(encoded, 'base64');
                    });
                    const indexedFiles = {};
                    multerFiles.forEach((file, index) => {
                        indexedFiles[file.fieldname] = bufferFiles[index];
                    });
                    console.log(multerFiles);
                    resolve({ files: indexedFiles, fields });
                }
            };
            this.multer(request, response, handle);
        });
    }
};
MulterMultipartHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('multer.options')),
    tslib_1.__metadata("design:paramtypes", [Object])
], MulterMultipartHandler);
exports.default = MulterMultipartHandler;
//# sourceMappingURL=file-upload.service.js.map