"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const config_1 = require("@nestjs/config");
const service_1 = require("../../domain/decorators/service");
const file_upload_response_1 = require("../../domain/storage/file-upload-response");
const storage_types_1 = require("../../domain/storage/storage-types");
const storage_uploader_1 = require("../../domain/storage/storage-uploader");
const collection_1 = require("../../domain/value-object/collection");
const uuid_1 = require("uuid");
const { Duplex } = require('stream');
const AWS = require('aws-sdk');
const FileType = require('file-type');
function bufferToStream(buffer) {
    const duplexStream = new Duplex();
    duplexStream.push(buffer);
    duplexStream.push(null);
    return duplexStream;
}
let S3Uploader = class S3Uploader {
    constructor(configService) {
        this.configService = configService;
        this.s3 = new AWS.S3({
            accessKeyId: this.configService.get('S3_ACCESS_KEY'),
            secretAccessKey: this.configService.get('S3_SECRET_KEY'),
            bucket: this.configService.get('S3_BUCKET'),
        });
    }
    async uploadFile(file, name = `${(0, uuid_1.v4)()}`, path, key) {
        const buckerName = this.configService.get('S3_BUCKET');
        const buffer = Buffer.from(file);
        const type = await FileType.fromBuffer(buffer);
        const extension = type.ext;
        const envFolder = process.env.NODE_ENV === 'production' ? '' : 'dev/';
        return new Promise((resolve, reject) => {
            const f = bufferToStream(buffer);
            const params = {
                Bucket: buckerName,
                Key: `${path ? envFolder + path + '/' : ''}${name}.${extension === 'xml' ? 'svg' : extension}`,
                Body: f,
                ContentType: extension === 'xml' ? 'image/svg+xml' : extension,
                ACL: 'public-read',
            };
            this.s3.upload(params, function (err, data) {
                if (err) {
                    return reject(err);
                }
                resolve({
                    key: key,
                    url: data.Location,
                    mimeType: extension,
                });
            });
        });
    }
    async uploadFiles(files) {
        const filesToUpload = files.map((f) => {
            return this.uploadFile(f.file, f.name, f.filePath, f.fileKey);
        });
        const response = await Promise.all(filesToUpload);
        return new collection_1.default(response);
    }
};
S3Uploader = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__metadata("design:paramtypes", [config_1.ConfigService])
], S3Uploader);
exports.default = S3Uploader;
//# sourceMappingURL=aws-s3-uploader.js.map