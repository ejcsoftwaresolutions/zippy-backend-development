import FileUploader from '@shared/domain/storage/storage-uploader';
import { FileUploadResponse } from '@shared/domain/storage/file-upload-response';
import Collection from '@shared/domain/value-object/collection';
import { UploadFile } from '@shared/domain/storage/storage-types';
export default class FirebaseUploader implements FileUploader {
    uploadFile(file: UploadFile, name?: string, path?: string, key?: string): Promise<FileUploadResponse>;
    uploadFiles(files: Collection<{
        file: UploadFile;
        name?: string;
        fileKey?: string;
        filePath?: string;
    }>): Promise<Collection<FileUploadResponse>>;
}
