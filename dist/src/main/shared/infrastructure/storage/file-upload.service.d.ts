import MultipartHandler from '@shared/domain/storage/multipart-handler';
import { Request, Response } from 'express';
export default class MulterMultipartHandler implements MultipartHandler<Request, Response> {
    private options;
    private multer;
    constructor(options?: any);
    getFilesAndFields<F>(request: Request, response: Response): Promise<{
        files: any;
        fields: F;
    }>;
}
