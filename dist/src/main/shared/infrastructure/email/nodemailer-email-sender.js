"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer = require('nodemailer');
class NodemailerEmailSender {
    constructor() {
        var _a;
        const config = {
            host: process.env.MAIL_HOST,
            port: process.env.MAIL_PORT,
            auth: {
                user: process.env.MAIL_USERNAME,
                pass: process.env.MAIL_PASSWORD,
            },
            ...(process.env.MAIL_HOST &&
                ((_a = process.env.MAIL_HOST) === null || _a === void 0 ? void 0 : _a.indexOf('mail.antagonist.nl')) > -1 && {
                secure: false,
                tls: {
                    rejectUnauthorized: false,
                },
            }),
        };
    }
    async send(email) {
        var _a;
        const content = email.toPrimitives();
        const defaultFromRecipient = {
            email: (_a = process.env.MAIL_FROM_ADDRESS) !== null && _a !== void 0 ? _a : '',
            name: process.env.MAIL_FROM_NAME,
        };
        console.log('EMAILS ARE DISABLED IN NODEMAILER WHEN DEVELOPMENT');
        return;
    }
    formatRecipient(recipient) {
        return `${(recipient === null || recipient === void 0 ? void 0 : recipient.name) && `"${recipient === null || recipient === void 0 ? void 0 : recipient.name}" `}${recipient.email}`;
    }
    verify() {
        this.sender.verify(function (error, success) {
            if (error) {
                console.log(error);
            }
            else {
                console.log('Server is ready to take our messages');
            }
        });
    }
}
exports.default = NodemailerEmailSender;
//# sourceMappingURL=nodemailer-email-sender.js.map