import { SentMessageInfo } from 'nodemailer';
import EmailMessage from '../../domain/email/email-message';
import EmailSender from '../../domain/email/email-sender';
export default class NodemailerEmailSender implements EmailSender {
    private sender;
    constructor();
    send(email: EmailMessage): Promise<SentMessageInfo>;
    formatRecipient(recipient: {
        email: string;
        name?: string;
    }): string;
    verify(): void;
}
