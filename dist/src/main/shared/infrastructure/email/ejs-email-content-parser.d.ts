import EmailContentParser from '../../domain/email/email-content-parser';
export default class EJSEmailContentParser implements EmailContentParser {
    private emailsPath;
    constructor(emailsPath: string);
    parseFromFile(filePath: string, params?: any): Promise<string>;
}
