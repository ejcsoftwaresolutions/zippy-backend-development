"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../domain/decorators");
const ejs = require('ejs');
const fs = require('fs');
const path = require('path');
let EJSEmailContentParser = class EJSEmailContentParser {
    constructor(emailsPath) {
        this.emailsPath = emailsPath;
    }
    async parseFromFile(filePath, params) {
        const file = this.emailsPath + '/' + filePath;
        return new Promise((resolve, reject) => {
            ejs.renderFile(file, params, {}, function (err, str) {
                if (err)
                    reject(err);
                resolve(str);
            });
        });
    }
};
EJSEmailContentParser = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('emails.root.folder')),
    tslib_1.__metadata("design:paramtypes", [String])
], EJSEmailContentParser);
exports.default = EJSEmailContentParser;
//# sourceMappingURL=ejs-email-content-parser.js.map