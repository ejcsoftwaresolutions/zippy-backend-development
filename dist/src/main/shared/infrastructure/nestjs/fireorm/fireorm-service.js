"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const decorators_1 = require("../../../domain/decorators");
const nestjs_firebase_admin_1 = require("@tfarras/nestjs-firebase-admin");
const fireorm = require("fireorm");
let FireOrmService = class FireOrmService {
    constructor(firebase) {
        this.firebase = firebase;
        fireorm.initialize(firebase.firestore());
    }
};
FireOrmService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__param(0, (0, decorators_1.inject)(nestjs_firebase_admin_1.FIREBASE_ADMIN_INJECT)),
    tslib_1.__metadata("design:paramtypes", [Object])
], FireOrmService);
exports.default = FireOrmService;
//# sourceMappingURL=fireorm-service.js.map