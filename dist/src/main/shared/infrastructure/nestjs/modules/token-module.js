"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenModule = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const bcrypt_password_hasher_1 = require("../../utils/bcrypt-password-hasher");
const hashProviders = [
    {
        provide: 'utils.hasher.round',
        useValue: 10,
    },
    {
        provide: 'utils.passwordHasher',
        useClass: bcrypt_password_hasher_1.default,
    },
];
let TokenModule = class TokenModule {
    constructor() { }
};
TokenModule = tslib_1.__decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [],
        providers: hashProviders,
        exports: hashProviders,
    }),
    tslib_1.__metadata("design:paramtypes", [])
], TokenModule);
exports.TokenModule = TokenModule;
//# sourceMappingURL=token-module.js.map