"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthModule = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const providers = [
    {
        provide: 'authentication.token.secret',
        useValue: 'zippy@t0k3n-2021-l0gin-s3cr3t',
    },
    {
        provide: 'authentication.token.expiresIn',
        useValue: '365d',
    },
];
let AuthModule = class AuthModule {
    constructor() { }
};
AuthModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [passport_1.PassportModule],
        providers: providers,
        exports: providers,
    }),
    tslib_1.__metadata("design:paramtypes", [])
], AuthModule);
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth-module.js.map