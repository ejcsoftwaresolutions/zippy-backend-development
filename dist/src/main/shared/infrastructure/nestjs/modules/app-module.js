"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const serve_static_1 = require("@nestjs/serve-static");
const app_auth_module_1 = require("./app-auth-module");
const cqrs_module_1 = require("./cqrs-module");
const fireorm_module_1 = require("./fireorm-module");
const mobile_module_1 = require("./mobile-module");
const token_module_1 = require("./token-module");
const path = require('path');
let AppModule = class AppModule {
    constructor() { }
};
AppModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [
            serve_static_1.ServeStaticModule.forRoot({
                rootPath: path.join(__dirname, '../../../../../../../public'),
            }),
            config_1.ConfigModule.forRoot({
                envFilePath: process.env.NODE_ENV
                    ? `.env.${process.env.NODE_ENV}`
                    : '.env.development',
                isGlobal: true,
            }),
            fireorm_module_1.FireOrmModule,
            token_module_1.TokenModule,
            app_auth_module_1.AppAuthModule,
            mobile_module_1.MobileModule,
            cqrs_module_1.CQRSModule,
        ],
        controllers: [],
        exports: [],
        providers: [],
    }),
    tslib_1.__metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app-module.js.map