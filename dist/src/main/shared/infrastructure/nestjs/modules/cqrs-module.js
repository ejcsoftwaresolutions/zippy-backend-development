"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.discoverRepositories = exports.discoverUseCases = exports.discoverEventSubscribers = exports.discoverQueryHandlers = exports.discoverCommandHandlers = exports.CQRSModule = void 0;
const tslib_1 = require("tslib");
const available_drivers_handler_1 = require("../../../../delivery-service/delivery/infrastructure/available-drivers-handler");
const google_geolocation_descriptor_1 = require("../../../../delivery-service/delivery/infrastructure/geolocation-info/google-geolocation-descriptor");
const intervaltimer_request_attempt_countdown_timer_1 = require("../../../../delivery-service/delivery/infrastructure/request-attempt/intervaltimer-request-attempt-countdown-timer");
const restaurant_orders_handler_1 = require("../../../../delivery-service/delivery/infrastructure/restaurant-orders-handler");
const service_recovery_1 = require("../../../../delivery-service/delivery/infrastructure/service-recovery/service-recovery");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const schedule_1 = require("@nestjs/schedule");
const command_handler_1 = require("../../../domain/bus/command/command-handler");
const event_subscriber_1 = require("../../../domain/bus/event/event-subscriber");
const query_handler_1 = require("../../../domain/bus/query/query-handler");
const auth_code_generator_1 = require("../../authentication/auth-code-generator");
const nodejs_command_bus_1 = require("../../bus/command/nodejs-command-bus");
const nodejs_event_bus_1 = require("../../bus/event/nodejs-event-bus");
const nodejs_query_bus_1 = require("../../bus/query/nodejs-query-bus");
const push_notifications_1 = require("../../push-notifications");
const basic_time_humanizer_1 = require("../../utils/basic-time-humanizer");
const nestjs_firebase_admin_1 = require("@tfarras/nestjs-firebase-admin");
const admin = require("firebase-admin");
const firebase_uploader_1 = require("../../storage/firebase-uploader");
const file_upload_service_1 = require("../../storage/file-upload.service");
const firebase_excel_generator_1 = require("../../pdf/firebase-excel-generator");
const firebase_html_pdf_generator_1 = require("../../pdf/firebase-html-pdf-generator");
const walkSync = require('walk-sync');
const path = require('path');
const multer = require('fastify-multer');
const commandHandlers = discoverCommandHandlers();
const queryHandlers = discoverQueryHandlers();
const eventSubscribers = discoverEventSubscribers();
const useCases = discoverUseCases();
const repositories = discoverRepositories();
const requestTimers = {};
const uploadDiskDest = path.join(__dirname, '../../../../../../../public/uploads');
const pdfTemplatesFolder = path.resolve('public/pdf-templates');
const multerOptions = {
    storage: multer.diskStorage({
        destination: path.join(__dirname, '../../../../../../.uploads'),
        filename: (req, file, cb) => {
            cb(null, file.originalname);
        },
    }),
    limits: { fieldSize: 25 * 1024 * 1024 },
};
const CQRSProviders = [
    {
        provide: 'command.bus.handlers',
        useFactory: (...handlers) => {
            return handlers || [];
        },
        inject: [...commandHandlers],
    },
    {
        provide: 'query.bus.handlers',
        useFactory: (...handlers) => {
            return handlers || [];
        },
        inject: [...queryHandlers],
    },
    {
        provide: 'event.bus.subscribers',
        useFactory: (...subscribers) => {
            return subscribers || [];
        },
        inject: [...eventSubscribers],
    },
    {
        provide: 'command.bus',
        useClass: nodejs_command_bus_1.default,
    },
    {
        provide: 'query.bus',
        useClass: nodejs_query_bus_1.default,
    },
    {
        provide: 'event.bus',
        useClass: nodejs_event_bus_1.default,
    },
];
const customProviders = [
    {
        provide: 'services.request.geolocation.descriptor',
        useClass: google_geolocation_descriptor_1.default,
    },
    {
        provide: 'delivery.request.timers',
        useValue: requestTimers,
    },
    {
        provide: 'services.request.attempt.countdown.timer',
        useClass: intervaltimer_request_attempt_countdown_timer_1.default,
    },
    {
        provide: 'multipart.handler',
        useClass: file_upload_service_1.default,
    },
    {
        provide: 'file.uploader',
        useClass: firebase_uploader_1.default,
    },
    {
        provide: 'code.generator',
        useClass: auth_code_generator_1.default,
    },
    {
        provide: 'storage.upload.path',
        useValue: uploadDiskDest,
    },
    {
        provide: 'multer.options',
        useValue: multerOptions,
    },
    {
        provide: 'pdf.templates.root.folder',
        useValue: pdfTemplatesFolder,
    },
    {
        provide: 'pdf.generator',
        useClass: firebase_html_pdf_generator_1.default,
    },
    {
        provide: 'excel.generator',
        useClass: firebase_excel_generator_1.default,
    },
    {
        provide: 'delivery.service.recovery',
        useClass: service_recovery_1.default,
    },
    {
        provide: 'available.riders.handler',
        useClass: available_drivers_handler_1.default,
    },
    {
        provide: 'restaurant.orders.handler',
        useClass: restaurant_orders_handler_1.default,
    },
    {
        provide: 'time.humanizer',
        useClass: basic_time_humanizer_1.default,
    },
    {
        provide: 'push.notification.service',
        useClass: push_notifications_1.default,
    },
];
const providers = [
    ...useCases,
    ...repositories,
    ...customProviders,
    ...commandHandlers,
    ...queryHandlers,
    ...eventSubscribers,
    ...CQRSProviders,
];
let CQRSModule = class CQRSModule {
    constructor() { }
};
CQRSModule = tslib_1.__decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [
            nestjs_firebase_admin_1.FirebaseAdminModule.forRootAsync({
                useFactory: (configService) => {
                    return {
                        apiKey: configService.get('FB_API_KEY'),
                        authDomain: configService.get('FB_AUTH_DOMAIN'),
                        databaseURL: configService.get('FB_DB_URL'),
                        projectId: configService.get('FB_PROJECT_ID'),
                        storageBucket: configService.get('FB_STORAGE_BUCKET'),
                        messagingSenderId: configService.get('FB_MESSAGING_SENDER_ID'),
                        appId: configService.get('FB_APP_ID'),
                        measurementId: configService.get('FB_MEASUREMENT_ID'),
                        credential: admin.credential.applicationDefault(),
                    };
                },
                inject: [config_1.ConfigService],
            }),
            schedule_1.ScheduleModule.forRoot(),
        ],
        providers: providers,
        exports: providers,
    }),
    tslib_1.__metadata("design:paramtypes", [])
], CQRSModule);
exports.CQRSModule = CQRSModule;
function discoverCommandHandlers() {
    return walkSync('src/main', {
        globs: ['**/*-command-handler.ts', '**/command-handlers/**/*.ts'],
        includeBasePath: false,
    }).map((p) => {
        const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
        const classConstructor = require(filePath);
        const instance = classConstructor.default;
        return instance;
    });
}
exports.discoverCommandHandlers = discoverCommandHandlers;
function discoverQueryHandlers() {
    return walkSync('src/main', {
        globs: ['**/*-query-handler.ts', '**/query-handlers/**/*.ts'],
    }).map((p) => {
        const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
        const classConstructor = require(filePath);
        const instance = classConstructor.default;
        return instance;
    });
}
exports.discoverQueryHandlers = discoverQueryHandlers;
function discoverEventSubscribers() {
    return walkSync('src/main', {
        globs: ['**/*-event-subscriber.ts'],
    }).map((p) => {
        const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
        const classConstructor = require(filePath);
        const instance = classConstructor.default;
        return instance;
    });
}
exports.discoverEventSubscribers = discoverEventSubscribers;
function discoverUseCases() {
    return walkSync('src/main', {
        globs: ['**/*-use-case.ts', '**/use-cases/**/*.ts'],
    }).map((p) => {
        const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
        const classConstructor = require(filePath);
        const instance = classConstructor.default;
        return {
            provide: `${classConstructor.default.name}`,
            useClass: instance,
        };
    });
}
exports.discoverUseCases = discoverUseCases;
function discoverRepositories() {
    return walkSync('src/main', {
        globs: ['**/*-infrastructure-*-repository.ts'],
    })
        .map((p) => {
        const filePath = '../../../../../main/' + p.slice(0, p.lastIndexOf('.'));
        const classConstructor = require(filePath);
        const instance = classConstructor.default;
        return instance;
    })
        .filter((r) => {
        const log = true;
        if (!(r === null || r === void 0 ? void 0 : r.bindingKey) && log) {
            console.log(`${r.name} does not have static bindingKey`);
        }
        if (!r) {
            return false;
        }
        return true;
    })
        .map((r) => {
        return {
            provide: `${r.bindingKey}`,
            useClass: r,
        };
    });
}
exports.discoverRepositories = discoverRepositories;
//# sourceMappingURL=cqrs-module.js.map