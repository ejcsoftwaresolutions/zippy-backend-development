"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppAuthModule = exports.UserTokenVerificator = exports.UserTokenCreator = void 0;
const tslib_1 = require("tslib");
const user_jwt_token_creator_1 = require("../../../../../../apps/shared/infrastructure/authentication/user-jwt-token-creator");
const user_jwt_token_verificator_1 = require("../../../../../../apps/shared/infrastructure/authentication/user-jwt-token-verificator");
const common_1 = require("@nestjs/common");
const auth_module_1 = require("./auth-module");
exports.UserTokenCreator = {
    provide: 'user.authentication.token.creator',
    useClass: user_jwt_token_creator_1.default,
};
exports.UserTokenVerificator = {
    provide: 'user.authentication.token.verificator',
    useClass: user_jwt_token_verificator_1.default,
};
let AppAuthModule = class AppAuthModule {
    constructor() { }
};
AppAuthModule = tslib_1.__decorate([
    (0, common_1.Global)(),
    (0, common_1.Module)({
        imports: [auth_module_1.AuthModule],
        providers: [exports.UserTokenCreator, exports.UserTokenVerificator],
        exports: [exports.UserTokenCreator, exports.UserTokenVerificator],
    }),
    tslib_1.__metadata("design:paramtypes", [])
], AppAuthModule);
exports.AppAuthModule = AppAuthModule;
//# sourceMappingURL=app-auth-module.js.map