"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FireOrmModule = void 0;
const tslib_1 = require("tslib");
const common_1 = require("@nestjs/common");
const fireorm_service_1 = require("../fireorm/fireorm-service");
let FireOrmModule = class FireOrmModule {
    constructor() { }
};
FireOrmModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [],
        providers: [fireorm_service_1.default],
    }),
    tslib_1.__metadata("design:paramtypes", [])
], FireOrmModule);
exports.FireOrmModule = FireOrmModule;
//# sourceMappingURL=fireorm-module.js.map