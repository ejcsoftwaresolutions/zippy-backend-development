import UserJWTCreator from '@apps/shared/infrastructure/authentication/user-jwt-token-creator';
import UserJWTVerificator from '@apps/shared/infrastructure/authentication/user-jwt-token-verificator';
export declare const UserTokenCreator: {
    provide: string;
    useClass: typeof UserJWTCreator;
};
export declare const UserTokenVerificator: {
    provide: string;
    useClass: typeof UserJWTVerificator;
};
export declare class AppAuthModule {
    constructor();
}
