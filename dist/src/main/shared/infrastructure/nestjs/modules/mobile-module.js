"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.discoverControllers = exports.MobileModule = void 0;
const tslib_1 = require("tslib");
const user_jwt_strategy_1 = require("../../../../../../apps/shared/infrastructure/authentication/strategies/user-jwt-strategy");
const common_1 = require("@nestjs/common");
const app_auth_module_1 = require("./app-auth-module");
const auth_module_1 = require("./auth-module");
const walkSync = require('walk-sync');
const controllers = discoverControllers();
let MobileModule = class MobileModule {
    constructor() { }
};
MobileModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [app_auth_module_1.AppAuthModule, auth_module_1.AuthModule],
        exports: [user_jwt_strategy_1.UserJwtStrategy],
        providers: [user_jwt_strategy_1.UserJwtStrategy],
        controllers: [...controllers],
    }),
    tslib_1.__metadata("design:paramtypes", [])
], MobileModule);
exports.MobileModule = MobileModule;
function discoverControllers() {
    return walkSync('apps', {
        globs: [
            'mobile/**/*.controller.ts',
            'mobile/**/*-controller.ts',
            'admin/**/*.controller.ts',
            'admin/**/*-controller.ts',
        ],
        includeBasePath: false,
    }).map((p) => {
        const filePath = '../../../../../../apps/' + p.slice(0, p.lastIndexOf('.'));
        const classConstructor = require(filePath);
        const instance = classConstructor;
        return Object.values(instance)[0];
    });
}
exports.discoverControllers = discoverControllers;
//# sourceMappingURL=mobile-module.js.map