import CommandHandler from '@shared/domain/bus/command/command-handler';
import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import QueryHandler from '@shared/domain/bus/query/query-handler';
export declare class CQRSModule {
    constructor();
}
export declare function discoverCommandHandlers(): Array<Constructor<CommandHandler>>;
export declare function discoverQueryHandlers(): Array<Constructor<QueryHandler>>;
export declare function discoverEventSubscribers(): Array<Constructor<EventSubscriber>>;
export declare function discoverUseCases(): Array<Constructor<CommandHandler>>;
export declare function discoverRepositories(): Array<Constructor<CommandHandler>>;
type Constructor<T> = any;
export {};
