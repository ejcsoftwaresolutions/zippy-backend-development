"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const EventEmitter = require('events');
const query_1 = require("../../../domain/bus/query/query");
const query_bus_1 = require("../../../domain/bus/query/query-bus");
const query_response_1 = require("../../../domain/bus/query/query-response");
const decorators_1 = require("../../../domain/decorators");
const service_1 = require("../../../domain/decorators/service");
let NodejsQueryBus = class NodejsQueryBus {
    constructor(handlers) {
        this.handlers = handlers;
        this.emitter = new EventEmitter();
        this.execute();
    }
    onSuccess(query, successCallback) {
        this.emitter.on(query.name + 'SuccessResponse', successCallback);
    }
    onError(query, errorCallback) {
        this.emitter.on(query.name + 'ErrorResponse', errorCallback);
    }
    execute() {
        this.handlers.forEach((handler) => {
            this.emitter.on(handler.getQueryName(), async (query) => {
                console.log(`New ${handler.getQueryName()}`);
                try {
                    const response = await handler.handle(query);
                    this.emitter.emit(query.name + 'SuccessResponse', response);
                }
                catch (e) {
                    this.emitter.emit(query.name + 'ErrorResponse', e);
                }
            });
        });
    }
    async ask(query) {
        return new Promise((resolve, reject) => {
            this.emitter.emit(query.name(), query);
            this.onSuccess(query, (result) => {
                resolve(result);
            });
            this.onError(query, (error) => {
                reject(error);
            });
        });
    }
};
NodejsQueryBus = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('query.bus.handlers')),
    tslib_1.__metadata("design:paramtypes", [Array])
], NodejsQueryBus);
exports.default = NodejsQueryBus;
//# sourceMappingURL=nodejs-query-bus.js.map