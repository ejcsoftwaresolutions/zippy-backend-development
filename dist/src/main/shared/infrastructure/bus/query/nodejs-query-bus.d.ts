declare const EventEmitter: any;
import Query from '@shared/domain/bus/query/query';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { QueryResponse } from '@shared/domain/bus/query/query-response';
import QueryHandler from '../../../domain/bus/query/query-handler';
export default class NodejsQueryBus implements QueryBus {
    emitter: typeof EventEmitter;
    handlers: Array<QueryHandler>;
    constructor(handlers: Array<QueryHandler>);
    onSuccess(query: Query, successCallback: (data?: any) => void): void;
    onError(query: Query, errorCallback: (data?: any) => void): void;
    execute(): void;
    ask(query: Query): Promise<QueryResponse>;
}
export {};
