"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const EventEmitter = require('events');
const decorators_1 = require("../../../domain/decorators");
const service_1 = require("../../../domain/decorators/service");
let NodejsCommandBus = class NodejsCommandBus {
    constructor(handlers) {
        this.handlers = handlers;
        this.emitter = new EventEmitter();
        this.execute();
    }
    onSuccess(command, successCallback) {
        this.emitter.on(command.name + 'SuccessResponse', successCallback);
    }
    onError(command, errorCallback) {
        this.emitter.on(command.name + 'ErrorResponse', errorCallback);
    }
    execute() {
        this.handlers.forEach((handler) => {
            this.emitter.on(handler.getCommandName(), async (command) => {
                console.log(`New ${handler.getCommandName()}`);
                try {
                    const response = await handler.handle(command);
                    this.emitter.emit(command.name + 'SuccessResponse', response);
                }
                catch (e) {
                    this.emitter.emit(command.name + 'ErrorResponse', e);
                }
            });
        });
    }
    async dispatch(command) {
        return new Promise((resolve, reject) => {
            this.emitter.emit(command.name(), command);
            this.onSuccess(command, (result) => {
                resolve(result);
            });
            this.onError(command, (error) => {
                reject(error);
            });
        });
    }
};
NodejsCommandBus = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('command.bus.handlers')),
    tslib_1.__metadata("design:paramtypes", [Array])
], NodejsCommandBus);
exports.default = NodejsCommandBus;
//# sourceMappingURL=nodejs-command-bus.js.map