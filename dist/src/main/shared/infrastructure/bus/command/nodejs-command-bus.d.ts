declare const EventEmitter: any;
import Command from '../../../domain/bus/command/command';
import CommandBus from '../../../domain/bus/command/command-bus';
import CommandHandler from '../../../domain/bus/command/command-handler';
export default class NodejsCommandBus implements CommandBus {
    emitter: typeof EventEmitter;
    handlers: Array<CommandHandler>;
    constructor(handlers: Array<CommandHandler>);
    onSuccess(command: Command, successCallback: (data?: any) => void): void;
    onError(command: Command, errorCallback: (data?: any) => void): void;
    execute(): void;
    dispatch(command: Command): Promise<any>;
}
export {};
