"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const EventEmitter = require('events');
const decorators_1 = require("../../../domain/decorators");
const service_1 = require("../../../domain/decorators/service");
let NodejsEventBus = class NodejsEventBus {
    constructor(subscribers) {
        this.subscribers = subscribers;
        this.emitter = new EventEmitter();
        this.execute();
    }
    execute() {
        this.subscribers.forEach((subscriber) => {
            const events = subscriber.subscribedTo();
            events.forEach((handlerFunction, eventName) => {
                this.emitter.on(eventName, (event) => {
                    console.log(`Handling on ${event.eventName()}`);
                    handlerFunction(event);
                });
            });
        });
    }
    publish(events) {
        events.forEach((event) => {
            this.emitter.emit(event.eventName(), event);
        });
        return true;
    }
};
NodejsEventBus = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('event.bus.subscribers')),
    tslib_1.__metadata("design:paramtypes", [Array])
], NodejsEventBus);
exports.default = NodejsEventBus;
//# sourceMappingURL=nodejs-event-bus.js.map