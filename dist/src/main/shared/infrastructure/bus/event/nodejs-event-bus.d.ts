declare const EventEmitter: any;
import EventBus, { Event } from '../../../domain/bus/event/event-bus';
import EventSubscriber from '../../../domain/bus/event/event-subscriber';
export default class NodejsEventBus implements EventBus {
    emitter: typeof EventEmitter;
    subscribers: Array<EventSubscriber>;
    constructor(subscribers: Array<EventSubscriber>);
    execute(): void;
    publish(events: Array<Event>): boolean;
}
export {};
