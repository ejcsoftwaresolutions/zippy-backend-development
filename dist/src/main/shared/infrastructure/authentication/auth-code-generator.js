"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const code_generator_1 = require("../../domain/utils/code-generator");
class AuthCodeGenerator {
    generate() {
        return this.makeId(6);
    }
    makeId(length) {
        let result = '';
        const characters = '0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
}
exports.default = AuthCodeGenerator;
//# sourceMappingURL=auth-code-generator.js.map