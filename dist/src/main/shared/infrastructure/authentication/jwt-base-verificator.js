"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JWTBaseVerificator = void 0;
const jwt = require('jsonwebtoken');
const common_1 = require("@nestjs/common");
const util_1 = require("util");
const verifyAsync = (0, util_1.promisify)(jwt.verify);
class JWTBaseVerificator {
    constructor(jwtSecret) {
        this.jwtSecret = jwtSecret;
    }
    async verifyToken(token) {
        if (!token) {
            throw new common_1.UnauthorizedException('INVALID_USER');
        }
        let userProfile;
        try {
            const decodedToken = await verifyAsync(token, this.jwtSecret);
            userProfile = Object.assign({
                lastName: decodedToken.lastName,
                firstName: decodedToken.firstName,
                email: decodedToken.email,
                id: decodedToken.id,
                roles: decodedToken.roles,
                ...decodedToken,
            });
        }
        catch (error) {
            throw new common_1.UnauthorizedException('INVALID_USER');
        }
        return userProfile;
    }
}
exports.JWTBaseVerificator = JWTBaseVerificator;
//# sourceMappingURL=jwt-base-verificator.js.map