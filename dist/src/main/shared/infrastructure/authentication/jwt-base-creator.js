"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JWTBaseCreator = void 0;
const jwt = require('jsonwebtoken');
const common_1 = require("@nestjs/common");
const util_1 = require("util");
const signAsync = (0, util_1.promisify)(jwt.sign);
const verifyAsync = (0, util_1.promisify)(jwt.verify);
class JWTBaseCreator {
    constructor(jwtSecret, jwtExpiresIn) {
        this.jwtSecret = jwtSecret;
        this.jwtExpiresIn = jwtExpiresIn;
    }
    async generateToken(userProfile) {
        if (!userProfile) {
            throw new common_1.UnauthorizedException('INVALID_USER');
        }
        let token;
        try {
            token = await signAsync(userProfile, this.jwtSecret, {
                expiresIn: this.jwtExpiresIn,
            });
        }
        catch (error) {
            throw new common_1.UnauthorizedException(`Error encoding token : ${error}`);
        }
        return token;
    }
}
exports.JWTBaseCreator = JWTBaseCreator;
//# sourceMappingURL=jwt-base-creator.js.map