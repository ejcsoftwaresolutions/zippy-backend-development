import CodeGenerator from '@shared/domain/utils/code-generator';
export default class AuthCodeGenerator implements CodeGenerator {
    generate(): string;
    private makeId;
}
