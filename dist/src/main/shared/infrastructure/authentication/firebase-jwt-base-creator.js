"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const firebase_admin_1 = require("firebase-admin");
class FirebaseJWTCreator {
    constructor(jwtSecret, jwtExpiresIn) {
        this.jwtSecret = jwtSecret;
        this.jwtExpiresIn = jwtExpiresIn;
    }
    async verifyToken(token) {
        if (!token) {
            throw new common_1.UnauthorizedException(`Error verifying token : 'token' is null`);
        }
        try {
            const decodedToken = await firebase_admin_1.default.auth().verifyIdToken(token);
            return {
                id: decodedToken.uid,
            };
        }
        catch (e) {
            throw new common_1.UnauthorizedException(`Invalid firebase token`);
        }
    }
    async generateToken(data) {
        return '';
    }
}
exports.default = FirebaseJWTCreator;
//# sourceMappingURL=firebase-jwt-base-creator.js.map