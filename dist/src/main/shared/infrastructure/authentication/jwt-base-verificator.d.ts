interface Profile {
    id: string;
    firstName: string;
    [props: string]: any;
}
export declare class JWTBaseVerificator {
    private readonly jwtSecret;
    constructor(jwtSecret: string);
    verifyToken(token: string): Promise<Profile>;
}
export {};
