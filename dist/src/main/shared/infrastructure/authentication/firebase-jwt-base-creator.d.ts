interface ProfileProps {
    id: string;
}
export default class FirebaseJWTCreator {
    private readonly jwtSecret;
    private readonly jwtExpiresIn;
    constructor(jwtSecret: string, jwtExpiresIn: string);
    verifyToken(token: string): Promise<ProfileProps>;
    generateToken(data: any): Promise<string>;
}
export {};
