interface Profile {
    id: string;
    firstName: string;
    [props: string]: any;
}
export declare class JWTBaseCreator {
    private readonly jwtSecret;
    private readonly jwtExpiresIn;
    constructor(jwtSecret: string, jwtExpiresIn: string);
    generateToken(userProfile: Profile): Promise<string>;
}
export {};
