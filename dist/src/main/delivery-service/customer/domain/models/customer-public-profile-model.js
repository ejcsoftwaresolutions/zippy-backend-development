"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const utils_1 = require("../../../../shared/domain/utils");
const DefaultProps = {};
class CustomerPublicProfile extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.email;
    }
    get firstName() {
        return this.props.firstName;
    }
    get lastName() {
        return this.props.lastName;
    }
    get phone() {
        return this.props.phone;
    }
    get profilePictureUrl() {
        return this.props.profilePictureUrl;
    }
    get pushToken() {
        return this.props.pushToken;
    }
    get createdAt() {
        return this.props.createdAt;
    }
    get wantsToReceivePushNotifications() {
        return this.props.configurations.receivePushNotifications;
    }
    static create({ ...plainData }) {
        return new CustomerPublicProfile(plainData);
    }
    static fromPrimitives({ ...plainData }) {
        return new CustomerPublicProfile(plainData);
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = CustomerPublicProfile;
//# sourceMappingURL=customer-public-profile-model.js.map