import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
interface CustomerPublicProfileProps {
    id: string;
    email: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    profilePictureUrl?: string;
    createdAt?: Date;
    pushToken: string;
    configurations: {
        referralPromotions: boolean;
        receiveEmails: boolean;
        receivePushNotifications: boolean;
        receiveSMS: boolean;
    };
}
export type CustomerPublicProfilePrimitiveProps = CustomerPublicProfileProps;
export default class CustomerPublicProfile extends AggregateRoot<CustomerPublicProfileProps> {
    constructor(props: CustomerPublicProfileProps);
    get id(): string;
    get email(): string;
    get firstName(): string;
    get lastName(): string;
    get phone(): string;
    get profilePictureUrl(): string;
    get pushToken(): string;
    get createdAt(): Date;
    get wantsToReceivePushNotifications(): boolean;
    static create({ ...plainData }: CustomerPublicProfileProps): CustomerPublicProfile;
    static fromPrimitives({ ...plainData }: CustomerPublicProfilePrimitiveProps): CustomerPublicProfile;
    toPrimitives(): CustomerPublicProfilePrimitiveProps;
}
export {};
