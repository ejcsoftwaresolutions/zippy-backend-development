import CustomerPublicProfile from '../models/customer-public-profile-model';
export default interface CustomerPublicProfileQueryRepository {
    findProfileById(id: string): Promise<CustomerPublicProfile | null>;
}
