import CustomerPublicProfile from '../models/customer-public-profile-model';
export default interface CustomerPublicProfileCommandRepository {
    createProfile(publicProfile: CustomerPublicProfile): void;
    updateProfile(publicProfile: CustomerPublicProfile): void;
}
