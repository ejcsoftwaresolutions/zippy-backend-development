import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';
export default class CustomerPublicProfileMapper {
    static toDomain(plainValues: {
        id: string;
        email: string;
        lastName: string;
        firstName: string;
        phone: string;
        profilePictureURL: string;
        pushToken: string;
        configurations: {
            referralPromotions: boolean;
            receiveEmails: boolean;
            receivePushNotifications: boolean;
            receiveSMS: boolean;
        };
    }): CustomerPublicProfile;
    static toPersistence(user: CustomerPublicProfile): any;
}
