"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerPublicProfileEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let CustomerPublicProfileEntity = class CustomerPublicProfileEntity {
};
CustomerPublicProfileEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('users')
], CustomerPublicProfileEntity);
exports.CustomerPublicProfileEntity = CustomerPublicProfileEntity;
//# sourceMappingURL=customer-public-profile-entity.js.map