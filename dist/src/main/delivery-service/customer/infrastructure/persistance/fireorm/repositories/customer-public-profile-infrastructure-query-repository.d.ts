import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';
import CustomerPublicProfileQueryRepository from '@deliveryService/customer/domain/repositories/customer-public-profile-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { CustomerPublicProfileEntity } from '../entities/customer-public-profile-entity';
export default class CustomerPublicProfileQueryInfrastructureRepository extends FireOrmRepository<CustomerPublicProfileEntity> implements CustomerPublicProfileQueryRepository {
    static readonly bindingKey = "CustomerPublicProfileQueryRepository";
    getEntityClass(): EntityConstructorOrPath<CustomerPublicProfileEntity>;
    findProfileById(id: string): Promise<CustomerPublicProfile | null>;
}
