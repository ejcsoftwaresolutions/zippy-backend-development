"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const customer_public_profile_model_1 = require("../../../../domain/models/customer-public-profile-model");
const customer_public_profile_query_repository_1 = require("../../../../domain/repositories/customer-public-profile-query-repository");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const customer_public_profile_entity_1 = require("../entities/customer-public-profile-entity");
const customer_public_profile_mapper_1 = require("../mappers/customer-public-profile-mapper");
const firebase_admin_1 = require("firebase-admin");
const constants_1 = require("../../../../../delivery/infrastructure/persistance/fireorm/constants");
const constants_2 = require("../../../../../../../../functions/src/constants");
class CustomerPublicProfileQueryInfrastructureRepository extends fireorm_repository_1.default {
    getEntityClass() {
        return customer_public_profile_entity_1.CustomerPublicProfileEntity;
    }
    async findProfileById(id) {
        var _a;
        const userAccount = await this.repository()
            .whereEqualTo('id', id)
            .findOne();
        if (!userAccount)
            return null;
        const profile = (await firebase_admin_1.default.firestore().collection(constants_2.CUSTOMERS).doc(userAccount.id).get()).data();
        if (!profile)
            return null;
        const pushTokens = (await firebase_admin_1.default
            .firestore()
            .collection(constants_1.PUSH_TOKENS)
            .doc(userAccount.id)
            .get()).data();
        return customer_public_profile_mapper_1.default.toDomain({
            id: userAccount.id,
            email: userAccount.email,
            firstName: userAccount.firstName,
            lastName: userAccount.lastName,
            phone: userAccount.phone,
            profilePictureURL: profile.profilePictureURL,
            pushToken: pushTokens === null || pushTokens === void 0 ? void 0 : pushTokens.customer,
            configurations: (_a = profile.configurations) !== null && _a !== void 0 ? _a : {
                receiveEmails: false,
                receiveSMS: false,
                receivePushNotifications: false,
                referralPromotions: false,
            },
        });
    }
}
exports.default = CustomerPublicProfileQueryInfrastructureRepository;
CustomerPublicProfileQueryInfrastructureRepository.bindingKey = 'CustomerPublicProfileQueryRepository';
//# sourceMappingURL=customer-public-profile-infrastructure-query-repository.js.map