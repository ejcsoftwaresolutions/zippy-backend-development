export declare class CustomerPublicProfileEntity {
    id: string;
    email: string;
    lastName: string;
    firstName: string;
    phone: string;
}
