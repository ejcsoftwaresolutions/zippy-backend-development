"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const customer_public_profile_model_1 = require("../../../../domain/models/customer-public-profile-model");
class CustomerPublicProfileMapper {
    static toDomain(plainValues) {
        return customer_public_profile_model_1.default.fromPrimitives({
            id: plainValues.id,
            email: plainValues.email,
            firstName: plainValues.firstName,
            lastName: plainValues.lastName,
            phone: plainValues.phone,
            profilePictureUrl: plainValues.profilePictureURL,
            pushToken: plainValues.pushToken,
            configurations: plainValues.configurations,
        });
    }
    static toPersistence(user) {
        return user.toPrimitives();
    }
}
exports.default = CustomerPublicProfileMapper;
//# sourceMappingURL=customer-public-profile-mapper.js.map