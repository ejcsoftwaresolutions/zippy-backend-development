"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StorePublicProfileEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let StorePublicProfileEntity = class StorePublicProfileEntity {
};
StorePublicProfileEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('users')
], StorePublicProfileEntity);
exports.StorePublicProfileEntity = StorePublicProfileEntity;
//# sourceMappingURL=store-public-profile-entity.js.map