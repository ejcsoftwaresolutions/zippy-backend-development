import StorePublicProfile from '@deliveryService/store/domain/models/store-public-profile-model';
export default class StorePublicProfileMapper {
    static toDomain(plainValues: {
        id: string;
        title: string;
        address: any;
        location: {
            latitude: number;
            longitude: number;
        };
        deliveryRadius: string;
        phone?: string;
        pushToken?: string;
        logoImage?: string;
        configurations: {
            referralPromotions: boolean;
            receiveEmails: boolean;
            receivePushNotifications: boolean;
            receiveSMS: boolean;
        };
    }): StorePublicProfile;
    static toPersistence(user: StorePublicProfile): any;
}
