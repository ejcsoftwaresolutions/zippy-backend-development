import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import StoreOrderQueryRepository from '@deliveryService/store/domain/repositories/store-order-query-repository';
import StoreOrder from '@deliveryService/store/domain/models/store-order-model';
export default class StoreOrderInfrastructureQueryRepository extends FireOrmRepository<any> implements StoreOrderQueryRepository {
    static readonly bindingKey = "StoreOrderQueryRepository";
    getEntityClass(): EntityConstructorOrPath<any>;
    findById(id: string): Promise<StoreOrder | null>;
    private findVendorProfileById;
}
