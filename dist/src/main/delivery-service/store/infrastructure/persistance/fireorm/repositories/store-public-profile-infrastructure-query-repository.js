"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const store_public_profile_model_1 = require("../../../../domain/models/store-public-profile-model");
const store_public_profile_query_repository_1 = require("../../../../domain/repositories/store-public-profile-query-repository");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const store_public_profile_entity_1 = require("../entities/store-public-profile-entity");
const store_public_profile_mapper_1 = require("../mappers/store-public-profile-mapper");
const firebase_admin_1 = require("firebase-admin");
const constants_1 = require("../../../../../delivery/infrastructure/persistance/fireorm/constants");
class StorePublicProfileQueryInfrastructureRepository extends fireorm_repository_1.default {
    getEntityClass() {
        return store_public_profile_entity_1.StorePublicProfileEntity;
    }
    async findProfileById(id) {
        var _a;
        const userAccount = await this.repository()
            .whereEqualTo('id', id)
            .findOne();
        if (!userAccount)
            return null;
        const profile = (await firebase_admin_1.default.firestore().collection(constants_1.VENDORS).doc(userAccount.id).get()).data();
        if (!profile)
            return null;
        const pushTokens = (await firebase_admin_1.default
            .firestore()
            .collection(constants_1.PUSH_TOKENS)
            .doc(userAccount.id)
            .get()).data();
        return store_public_profile_mapper_1.default.toDomain({
            id: userAccount.id,
            address: profile.address,
            location: {
                latitude: profile.location.latitude,
                longitude: profile.location.longitude,
            },
            phone: userAccount.phone,
            deliveryRadius: profile.deliveryRadius,
            logoImage: profile.logoInage,
            title: profile.title,
            pushToken: pushTokens === null || pushTokens === void 0 ? void 0 : pushTokens.vendor,
            configurations: (_a = profile.configurations) !== null && _a !== void 0 ? _a : {
                receiveEmails: false,
                receiveSMS: false,
                receivePushNotifications: false,
                referralPromotions: false,
            },
        });
    }
}
exports.default = StorePublicProfileQueryInfrastructureRepository;
StorePublicProfileQueryInfrastructureRepository.bindingKey = 'StorePublicProfileQueryRepository';
//# sourceMappingURL=store-public-profile-infrastructure-query-repository.js.map