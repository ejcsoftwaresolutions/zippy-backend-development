export declare class StorePublicProfileEntity {
    id: string;
    firstName?: string;
    lastName?: string;
    email: string;
    phone?: string;
}
