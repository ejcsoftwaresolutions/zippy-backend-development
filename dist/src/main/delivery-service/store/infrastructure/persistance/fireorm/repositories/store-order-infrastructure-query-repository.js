"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const firebase_admin_1 = require("firebase-admin");
const constants_1 = require("../../../../../delivery/infrastructure/persistance/fireorm/constants");
const store_order_query_repository_1 = require("../../../../domain/repositories/store-order-query-repository");
const store_order_model_1 = require("../../../../domain/models/store-order-model");
const firebase_utils_1 = require("../../../../../../../../functions/src/utils/firebase-utils");
const store_public_profile_model_1 = require("../../../../domain/models/store-public-profile-model");
const store_public_profile_mapper_1 = require("../mappers/store-public-profile-mapper");
class StoreOrderInfrastructureQueryRepository extends fireorm_repository_1.default {
    getEntityClass() {
        return null;
    }
    async findById(id) {
        var _a, _b, _c;
        const vendorOrder = (await firebase_admin_1.default.firestore().collection(constants_1.RESTAURANT_ORDERS).doc(id).get()).data();
        if (!vendorOrder)
            return null;
        const profile = await this.findVendorProfileById(vendorOrder.vendorID);
        if (!profile)
            return null;
        if (vendorOrder.deliveryMethod === 'PICK_UP')
            return null;
        return store_order_model_1.default.fromPrimitives({
            id: vendorOrder.id,
            items: vendorOrder.products.map((p) => ({
                id: p.id,
                name: p.name,
                quantity: p.quantity,
                unitPrice: p.price
            })),
            paymentMethod: 'CASH',
            subtotal: vendorOrder.subtotal,
            total: vendorOrder.total,
            customerId: vendorOrder.customerID,
            customerLocation: {
                address: vendorOrder.shipAddr.formattedAddress,
                geoLocation: [vendorOrder.shipAddr.location.latitude, vendorOrder.shipAddr.location.longitude]
            },
            deliveryFee: (_b = (_a = vendorOrder === null || vendorOrder === void 0 ? void 0 : vendorOrder.fees) === null || _a === void 0 ? void 0 : _a.delivery) !== null && _b !== void 0 ? _b : vendorOrder.total - vendorOrder.subtotal,
            date: firebase_utils_1.default.getDate(vendorOrder.createdAt),
            code: (_c = vendorOrder.code) !== null && _c !== void 0 ? _c : vendorOrder.id,
            store: profile.toPrimitives()
        });
    }
    async findVendorProfileById(id) {
        var _a;
        const userAccount = (await firebase_admin_1.default.firestore().collection("users")
            .doc(id)
            .get()).data();
        if (!userAccount)
            return null;
        const profile = (await firebase_admin_1.default.firestore().collection(constants_1.VENDORS).doc(userAccount.id).get()).data();
        if (!profile)
            return null;
        const pushTokens = (await firebase_admin_1.default
            .firestore()
            .collection(constants_1.PUSH_TOKENS)
            .doc(userAccount.id)
            .get()).data();
        return store_public_profile_mapper_1.default.toDomain({
            id: userAccount.id,
            address: profile.address,
            location: {
                latitude: profile.location.latitude,
                longitude: profile.location.longitude
            },
            phone: userAccount.phone,
            deliveryRadius: profile.deliveryRadius,
            logoImage: profile.logoInage,
            title: profile.title,
            pushToken: pushTokens === null || pushTokens === void 0 ? void 0 : pushTokens.vendor,
            configurations: (_a = profile.configurations) !== null && _a !== void 0 ? _a : {
                receiveEmails: false,
                receiveSMS: false,
                receivePushNotifications: false,
                referralPromotions: false
            }
        });
    }
}
exports.default = StoreOrderInfrastructureQueryRepository;
StoreOrderInfrastructureQueryRepository.bindingKey = 'StoreOrderQueryRepository';
//# sourceMappingURL=store-order-infrastructure-query-repository.js.map