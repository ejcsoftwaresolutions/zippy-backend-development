"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const store_public_profile_model_1 = require("../../../../domain/models/store-public-profile-model");
class StorePublicProfileMapper {
    static toDomain(plainValues) {
        var _a, _b, _c;
        return store_public_profile_model_1.default.fromPrimitives({
            id: plainValues.id,
            name: plainValues.title,
            location: {
                address: `${plainValues.address.line1} ${(_a = plainValues.address.line2) !== null && _a !== void 0 ? _a : ""} ${(_b = plainValues.address.city) !== null && _b !== void 0 ? _b : ""} ${(_c = plainValues.address.state) !== null && _c !== void 0 ? _c : ""}`,
                geoLocation: [parseFloat(plainValues.location.latitude), parseFloat(plainValues.location.longitude),],
            },
            phone: plainValues.phone,
            serviceConfigurations: {
                deliveryRadius: parseFloat(plainValues.deliveryRadius),
            },
            pushToken: plainValues.pushToken,
            logoUrl: plainValues.logoImage,
            configurations: plainValues.configurations,
        });
    }
    static toPersistence(user) {
        const primitives = user.toPrimitives();
        return primitives;
    }
}
exports.default = StorePublicProfileMapper;
//# sourceMappingURL=store-public-profile-mapper.js.map