import StorePublicProfile from '@deliveryService/store/domain/models/store-public-profile-model';
import StorePublicProfileQueryRepository from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { StorePublicProfileEntity } from '../entities/store-public-profile-entity';
export default class StorePublicProfileQueryInfrastructureRepository extends FireOrmRepository<StorePublicProfileEntity> implements StorePublicProfileQueryRepository {
    static readonly bindingKey = "StorePublicProfileQueryRepository";
    getEntityClass(): EntityConstructorOrPath<StorePublicProfileEntity>;
    findProfileById(id: string): Promise<StorePublicProfile | null>;
}
