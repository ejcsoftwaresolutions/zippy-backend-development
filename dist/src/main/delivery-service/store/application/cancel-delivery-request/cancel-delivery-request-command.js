"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class CancelDeliveryRequestCommand extends command_1.default {
    constructor(requestId) {
        super();
        this.requestId = requestId;
    }
    name() {
        return CancelDeliveryRequestCommand.name;
    }
}
exports.default = CancelDeliveryRequestCommand;
//# sourceMappingURL=cancel-delivery-request-command.js.map