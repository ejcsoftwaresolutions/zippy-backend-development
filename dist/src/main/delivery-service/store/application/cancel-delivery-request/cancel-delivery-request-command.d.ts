import Command from '@shared/domain/bus/command/command';
export default class CancelDeliveryRequestCommand extends Command {
    readonly requestId: string;
    constructor(requestId: string);
    name(): string;
}
