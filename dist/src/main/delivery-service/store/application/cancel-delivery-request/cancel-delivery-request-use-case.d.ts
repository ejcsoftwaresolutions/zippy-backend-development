import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import EventBus from '@shared/domain/bus/event/event-bus';
export default class CancelDeliveryRequestCommandCase {
    private deliveryRequestRepository;
    private requestTimerService;
    private eventBus;
    constructor(deliveryRequestRepository: DeliveryRequestCommandRepository, requestTimerService: RequestAttemptCountdownTimer, eventBus: EventBus);
    execute(requestId: string): Promise<void>;
    private resetCurrentCandidate;
}
