"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const request_attempt_countdown_timer_1 = require("../../../delivery/domain/contracts/request-attempt-countdown-timer");
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const collection_1 = require("../../../../shared/domain/value-object/collection");
let CancelDeliveryRequestCommandCase = class CancelDeliveryRequestCommandCase {
    constructor(deliveryRequestRepository, requestTimerService, eventBus) {
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.requestTimerService = requestTimerService;
        this.eventBus = eventBus;
    }
    async execute(requestId) {
        const currentRequest = await this.deliveryRequestRepository.getRequest(requestId);
        try {
            this.requestTimerService.stop(currentRequest.id.value);
            this.requestTimerService.destroy(currentRequest.id.value);
        }
        catch (error) { }
        currentRequest.markAsCanceled();
        await this.resetCurrentCandidate(currentRequest);
        await this.deliveryRequestRepository.deleteRequest(currentRequest);
        this.eventBus.publish(currentRequest.pullDomainEvents());
    }
    async resetCurrentCandidate(currentRequest) {
        if (!currentRequest.currentAlertedCandidate) {
            return;
        }
        currentRequest.setCurrentAttemptAlertedCandidateNotification(null);
        await this.deliveryRequestRepository.updateDriverNotification(currentRequest.currentAlertedCandidate, currentRequest.mode);
        await this.deliveryRequestRepository.toggleLockedCandidates(new collection_1.default([currentRequest.currentAlertedCandidate]), currentRequest.mode, false);
        currentRequest.setCurrentAttemptAlertedCandidate(null);
    }
};
CancelDeliveryRequestCommandCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(1, (0, decorators_1.inject)('services.request.attempt.countdown.timer')),
    tslib_1.__param(2, (0, decorators_1.inject)('event.bus')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], CancelDeliveryRequestCommandCase);
exports.default = CancelDeliveryRequestCommandCase;
//# sourceMappingURL=cancel-delivery-request-use-case.js.map