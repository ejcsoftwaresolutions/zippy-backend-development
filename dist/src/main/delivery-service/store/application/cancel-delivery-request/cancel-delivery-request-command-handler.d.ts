import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import CancelDeliveryRequestCommand from './cancel-delivery-request-command';
import CancelDeliveryRequestCommandCase from './cancel-delivery-request-use-case';
export default class CancelDeliveryRequestCommandHandler implements CommandHandler {
    private useCase;
    private repository;
    constructor(useCase: CancelDeliveryRequestCommandCase, repository: DeliveryRequestCommandRepository);
    getCommandName(): string;
    handle(command: CancelDeliveryRequestCommand): Promise<void>;
}
