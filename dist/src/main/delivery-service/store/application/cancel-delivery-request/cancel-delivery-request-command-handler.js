"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const cancel_delivery_request_command_1 = require("./cancel-delivery-request-command");
const cancel_delivery_request_use_case_1 = require("./cancel-delivery-request-use-case");
let CancelDeliveryRequestCommandHandler = class CancelDeliveryRequestCommandHandler {
    constructor(useCase, repository) {
        this.useCase = useCase;
        this.repository = repository;
    }
    getCommandName() {
        return cancel_delivery_request_command_1.default.name;
    }
    async handle(command) {
        const currentRequest = await this.repository.getRequest(command.requestId);
        if (!currentRequest) {
            throw new Error('request_not_found');
        }
        await this.useCase.execute(command.requestId);
    }
};
CancelDeliveryRequestCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('CancelDeliveryRequestCommandCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [cancel_delivery_request_use_case_1.default, Object])
], CancelDeliveryRequestCommandHandler);
exports.default = CancelDeliveryRequestCommandHandler;
//# sourceMappingURL=cancel-delivery-request-command-handler.js.map