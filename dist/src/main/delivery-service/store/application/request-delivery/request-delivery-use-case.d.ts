import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import TripGeolocationDescriptor from '@deliveryService/delivery/domain/contracts/trip-geolocation-descriptor';
import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import StoreOrder from '@deliveryService/store/domain/models/store-order-model';
import EventBus from '@shared/domain/bus/event/event-bus';
import Id from '@shared/domain/id/id';
import TimeHumanizer from '@shared/domain/utils/time-humanizer';
import CustomerPublicProfile from '../../../customer/domain/models/customer-public-profile-model';
import DeliveryRequest, { RideGeoPoint } from '../../../delivery/domain/models/delivery-request-model';
import DeliveryRequestRiderCandidate from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';
interface Props {
    id: Id;
    customer: CustomerPublicProfile;
    order: StoreOrder;
    serviceConfigurations: ServiceConfigurations;
    mode: 'live' | 'test';
    customerLocation: RideGeoPoint;
    serviceType: string;
    preCandidates?: string[];
    candidate?: DeliveryRequestRiderCandidate;
}
export default class RequestDeliveryUseCase {
    private tripRequestRepository;
    private eventBus;
    private requestInformationBuilder;
    private startAttemptService;
    constructor(tripRequestRepository: DeliveryRequestCommandRepository, tripGeolocationDescriptor: TripGeolocationDescriptor, timeHumanizer: TimeHumanizer, requestTimerService: RequestAttemptCountdownTimer, eventBus: EventBus);
    execute(props: Props): Promise<void>;
    saveRequest(props: Props): Promise<DeliveryRequest>;
    private retryRequest;
}
export {};
