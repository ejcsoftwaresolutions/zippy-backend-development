"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const start_request_attempt_use_case_1 = require("../../../delivery/application/start-request-attempt/start-request-attempt-use-case");
const request_attempt_countdown_timer_1 = require("../../../delivery/domain/contracts/request-attempt-countdown-timer");
const trip_geolocation_descriptor_1 = require("../../../delivery/domain/contracts/trip-geolocation-descriptor");
const service_configurations_model_1 = require("../../../delivery/domain/models/service-configurations-model");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
const store_order_model_1 = require("../../domain/models/store-order-model");
const common_1 = require("@nestjs/common");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const time_humanizer_1 = require("../../../../shared/domain/utils/time-humanizer");
const collection_1 = require("../../../../shared/domain/value-object/collection");
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const request_information_builder_1 = require("../../domain/services/request-information-builder");
const delivery_request_rider_candidate_model_1 = require("../../../delivery/domain/models/delivery-request-rider-candidate-model");
let RequestDeliveryUseCase = class RequestDeliveryUseCase {
    constructor(tripRequestRepository, tripGeolocationDescriptor, timeHumanizer, requestTimerService, eventBus) {
        this.tripRequestRepository = tripRequestRepository;
        this.eventBus = eventBus;
        this.requestInformationBuilder = new request_information_builder_1.default(tripGeolocationDescriptor, timeHumanizer);
        this.startAttemptService = new start_request_attempt_use_case_1.default(requestTimerService, tripRequestRepository, this.requestInformationBuilder, this.eventBus);
    }
    async execute(props) {
        const { id, preCandidates } = props;
        let currentRequest = null;
        try {
            currentRequest = await this.tripRequestRepository.getRequest(id.value);
        }
        catch (e) {
        }
        if (currentRequest) {
            currentRequest.setPreCandidates(preCandidates);
            await this.retryRequest(currentRequest);
            return;
        }
        const newRequest = await this.saveRequest(props);
        this.startAttemptService.execute(newRequest);
    }
    async saveRequest(props) {
        const { id, customer, serviceConfigurations, mode, customerLocation, order, preCandidates, serviceType } = props;
        const route = await this.requestInformationBuilder.buildNewRoute(order.storeLocation, customerLocation);
        const tripRoutes = [
            {
                ...route,
                origin: { ...route.origin, address: order.storeLocation.address },
                destination: {
                    ...route.destination,
                    address: customerLocation.address
                }
            }
        ];
        const tripGeoInformation = await this.requestInformationBuilder.getTripGeoInformation(tripRoutes);
        const newRequest = new delivery_request_model_1.default({
            id: id,
            customer,
            requestInfo: order,
            serviceConfig: serviceConfigurations,
            preCandidates: preCandidates,
            tripDistance: {
                value: tripGeoInformation.distanceValue,
                text: tripGeoInformation.distance
            },
            tripDuration: {
                value: tripGeoInformation.durationValue,
                text: tripGeoInformation.duration
            },
            date: new Date(),
            mode,
            routes: tripRoutes ? new collection_1.default(tripRoutes) : undefined,
            serviceType: serviceType
        });
        newRequest.registerNewAttempt();
        if (props.candidate) {
            newRequest.setCurrentAttemptAlertedCandidate(props.candidate);
        }
        await this.tripRequestRepository.saveRequest(newRequest);
        return newRequest;
    }
    async retryRequest(currentRequest) {
        currentRequest.registerNewAttempt();
        await this.tripRequestRepository.saveRequest(currentRequest);
        this.startAttemptService.execute(currentRequest);
    }
};
RequestDeliveryUseCase = tslib_1.__decorate([
    (0, service_1.default)(common_1.Scope.REQUEST),
    tslib_1.__param(0, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(1, (0, decorators_1.inject)('services.request.geolocation.descriptor')),
    tslib_1.__param(2, (0, decorators_1.inject)('time.humanizer')),
    tslib_1.__param(3, (0, decorators_1.inject)('services.request.attempt.countdown.timer')),
    tslib_1.__param(4, (0, decorators_1.inject)('event.bus')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object, Object])
], RequestDeliveryUseCase);
exports.default = RequestDeliveryUseCase;
//# sourceMappingURL=request-delivery-use-case.js.map