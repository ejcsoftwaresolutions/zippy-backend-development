"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_configurations_query_repository_1 = require("../../../delivery/domain/repositories/service-configurations-query-repository");
const store_order_model_1 = require("../../domain/models/store-order-model");
const store_public_profile_query_repository_1 = require("../../domain/repositories/store-public-profile-query-repository");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const request_delivery_command_1 = require("./request-delivery-command");
const request_delivery_use_case_1 = require("./request-delivery-use-case");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
let RequestDeliveryCommandHandler = class RequestDeliveryCommandHandler {
    constructor(useCase, customerPublicProfileQueryRepository, storePublicProfileQueryRepository, serviceConfigQueryRepository, tripRequestRepository) {
        this.useCase = useCase;
        this.customerPublicProfileQueryRepository = customerPublicProfileQueryRepository;
        this.storePublicProfileQueryRepository = storePublicProfileQueryRepository;
        this.serviceConfigQueryRepository = serviceConfigQueryRepository;
        this.tripRequestRepository = tripRequestRepository;
    }
    getCommandName() {
        return request_delivery_command_1.default.name;
    }
    async handle(command) {
        const customer = await this.customerPublicProfileQueryRepository.findProfileById(command.customerId);
        const store = await this.storePublicProfileQueryRepository.findProfileById(command.order.storeId);
        if (!customer) {
            throw new Error('customer_not_found');
        }
        if (!store) {
            throw new Error('store_not_found');
        }
        const id = new id_1.default(command.order.id);
        const serviceConfigurations = await this.serviceConfigQueryRepository.getConfigurations();
        await this.useCase.execute({
            id: id,
            customer: customer,
            order: store_order_model_1.default.fromPrimitives({
                ...command.order,
                customerLocation: command.customerLocation,
                customerId: command.customerId,
                paymentMethod: command.order.paymentMethod,
                store: store.toPrimitives()
            }),
            preCandidates: command.preCandidates,
            serviceConfigurations: serviceConfigurations,
            mode: command.mode,
            customerLocation: command.customerLocation,
            serviceType: command.serviceType
        });
        return {
            id: id.value
        };
    }
};
RequestDeliveryCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('RequestDeliveryUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('CustomerPublicProfileQueryRepository')),
    tslib_1.__param(2, (0, decorators_1.inject)('StorePublicProfileQueryRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('ServiceConfigurationsQueryRepository')),
    tslib_1.__param(4, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [request_delivery_use_case_1.default, Object, Object, Object, Object])
], RequestDeliveryCommandHandler);
exports.default = RequestDeliveryCommandHandler;
//# sourceMappingURL=request-delivery-command-handler.js.map