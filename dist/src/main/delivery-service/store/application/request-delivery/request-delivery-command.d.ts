import { RideGeoPoint } from '@deliveryService/delivery/domain/models/delivery-request-model';
import Command from '@shared/domain/bus/command/command';
export interface RequestDeliveryData {
    customerId: string;
    mode: 'live' | 'test';
    order: {
        id: string;
        items: any[];
        storeId: string;
        subtotal: number;
        total: number;
        date: Date;
        paymentMethod: string;
        deliveryFee: number;
        code: string;
    };
    preCandidates?: string[];
    customerLocation: RideGeoPoint;
    serviceType: string;
}
export default class RequestDeliveryCommand extends Command {
    readonly props: RequestDeliveryData;
    constructor(props: RequestDeliveryData);
    get customerId(): string;
    get preCandidates(): string[];
    get mode(): "test" | "live";
    get order(): {
        id: string;
        items: any[];
        storeId: string;
        subtotal: number;
        total: number;
        date: Date;
        paymentMethod: string;
        deliveryFee: number;
        code: string;
    };
    get customerLocation(): RideGeoPoint;
    get serviceType(): string;
    name(): string;
}
