import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import StorePublicProfileQueryRepository from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import CustomerPublicProfileQueryRepository from '../../../customer/domain/repositories/customer-public-profile-query-repository';
import RequestDeliveryCommand from './request-delivery-command';
import RequestDeliveryUseCase from './request-delivery-use-case';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
export default class RequestDeliveryCommandHandler implements CommandHandler {
    private useCase;
    private customerPublicProfileQueryRepository;
    private storePublicProfileQueryRepository;
    private serviceConfigQueryRepository;
    private tripRequestRepository;
    constructor(useCase: RequestDeliveryUseCase, customerPublicProfileQueryRepository: CustomerPublicProfileQueryRepository, storePublicProfileQueryRepository: StorePublicProfileQueryRepository, serviceConfigQueryRepository: ServiceConfigurationsQueryRepository, tripRequestRepository: DeliveryRequestCommandRepository);
    getCommandName(): string;
    handle(command: RequestDeliveryCommand): Promise<{
        id: string;
    }>;
}
