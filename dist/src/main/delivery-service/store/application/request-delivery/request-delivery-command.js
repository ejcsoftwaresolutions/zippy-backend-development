"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const command_1 = require("../../../../shared/domain/bus/command/command");
class RequestDeliveryCommand extends command_1.default {
    constructor(props) {
        super();
        this.props = props;
    }
    get customerId() {
        return this.props.customerId;
    }
    get preCandidates() {
        return this.props.preCandidates;
    }
    get mode() {
        return this.props.mode;
    }
    get order() {
        return this.props.order;
    }
    get customerLocation() {
        return this.props.customerLocation;
    }
    get serviceType() {
        return this.props.serviceType;
    }
    name() {
        return RequestDeliveryCommand.name;
    }
}
exports.default = RequestDeliveryCommand;
//# sourceMappingURL=request-delivery-command.js.map