"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const utils_1 = require("../../../../shared/domain/utils");
const store_public_profile_model_1 = require("./store-public-profile-model");
const DefaultProps = {};
class StoreOrder extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props
        });
    }
    get paymentMethod() {
        return this.props.paymentMethod;
    }
    get customerLocation() {
        return this.props.customerLocation;
    }
    get storeLocation() {
        return this.props.store.location;
    }
    get store() {
        return this.props.store;
    }
    get storeId() {
        return this.props.store.id;
    }
    get customerId() {
        return this.props.customerId;
    }
    get deliveryFee() {
        return this.props.deliveryFee;
    }
    get id() {
        return this.props.id;
    }
    get code() {
        return this.props.code;
    }
    static create({ ...plainData }) {
        return new StoreOrder(plainData);
    }
    static fromPrimitives({ ...plainData }) {
        return new StoreOrder({
            id: plainData.id,
            date: plainData.date,
            items: plainData.items,
            store: store_public_profile_model_1.default.fromPrimitives(plainData.store),
            subtotal: plainData.subtotal,
            total: plainData.total,
            paymentMethod: plainData.paymentMethod,
            deliveryFee: plainData.deliveryFee,
            code: plainData.code,
            customerId: plainData.customerId,
            customerLocation: plainData.customerLocation
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = StoreOrder;
//# sourceMappingURL=store-order-model.js.map