import { RideGeoPoint } from '@deliveryService/delivery/domain/models/delivery-request-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
interface StorePublicProfileProps {
    id: string;
    name: string;
    location: RideGeoPoint;
    phone?: string;
    serviceConfigurations: {
        deliveryRadius: number;
    };
    pushToken?: string;
    logoUrl?: string;
    configurations: {
        referralPromotions: boolean;
        receiveEmails: boolean;
        receivePushNotifications: boolean;
        receiveSMS: boolean;
    };
}
export type StorePublicProfilePrimitiveProps = StorePublicProfileProps;
export default class StorePublicProfile extends AggregateRoot<StorePublicProfileProps> {
    constructor(props: StorePublicProfileProps);
    get id(): string;
    get location(): RideGeoPoint;
    get phone(): string;
    get pushToken(): string;
    get logoUrl(): string;
    get deliveryRadius(): number;
    get wantsToReceivePushNotifications(): boolean;
    static create(plainData: StorePublicProfileProps): StorePublicProfile;
    static fromPrimitives({ ...plainData }: StorePublicProfilePrimitiveProps): StorePublicProfile;
    toPrimitives(): StorePublicProfilePrimitiveProps;
}
export {};
