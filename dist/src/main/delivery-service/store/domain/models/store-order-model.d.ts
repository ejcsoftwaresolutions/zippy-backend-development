import { PaymentMethod, RideGeoPoint } from '@deliveryService/delivery/domain/models/delivery-request-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import StorePublicProfile, { StorePublicProfilePrimitiveProps } from './store-public-profile-model';
export type StoreOrderItem = {
    id: string;
    name: string;
    quantity: number;
    unitPrice: number;
};
interface StoreOrderProps {
    id: string;
    items: StoreOrderItem[];
    store: StorePublicProfile;
    customerId: string;
    subtotal: number;
    total: number;
    date: Date;
    paymentMethod: PaymentMethod;
    deliveryFee: number;
    code: string;
    customerLocation: RideGeoPoint;
}
export interface StoreOrderPrimitiveProps {
    id: string;
    items: StoreOrderItem[];
    store: StorePublicProfilePrimitiveProps;
    customerId: string;
    subtotal: number;
    total: number;
    date: Date;
    paymentMethod: PaymentMethod;
    deliveryFee: number;
    code: string;
    customerLocation: RideGeoPoint;
}
export default class StoreOrder extends AggregateRoot<StoreOrderProps> {
    constructor(props: StoreOrderProps);
    get paymentMethod(): PaymentMethod;
    get customerLocation(): RideGeoPoint;
    get storeLocation(): RideGeoPoint;
    get store(): StorePublicProfile;
    get storeId(): string;
    get customerId(): string;
    get deliveryFee(): number;
    get id(): string;
    get code(): string;
    static create({ ...plainData }: StoreOrderProps): StoreOrder;
    static fromPrimitives({ ...plainData }: StoreOrderPrimitiveProps): StoreOrder;
    toPrimitives(): StoreOrderPrimitiveProps;
}
export {};
