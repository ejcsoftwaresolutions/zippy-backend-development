"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const utils_1 = require("../../../../shared/domain/utils");
const DefaultProps = {};
class StorePublicProfile extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get location() {
        return this.props.location;
    }
    get phone() {
        return this.props.phone;
    }
    get pushToken() {
        return this.props.pushToken;
    }
    get logoUrl() {
        return this.props.logoUrl;
    }
    get deliveryRadius() {
        return this.props.serviceConfigurations.deliveryRadius;
    }
    get wantsToReceivePushNotifications() {
        return this.props.configurations.receivePushNotifications;
    }
    static create(plainData) {
        return new StorePublicProfile(plainData);
    }
    static fromPrimitives({ ...plainData }) {
        return new StorePublicProfile(plainData);
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = StorePublicProfile;
//# sourceMappingURL=store-public-profile-model.js.map