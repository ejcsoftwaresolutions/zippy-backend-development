"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const trip_geolocation_descriptor_1 = require("../../../delivery/domain/contracts/trip-geolocation-descriptor");
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const time_humanizer_1 = require("../../../../shared/domain/utils/time-humanizer");
class RequestInformationBuilder {
    constructor(tripGeolocationDescriptor, timeHumanizer) {
        this.tripGeolocationDescriptor = tripGeolocationDescriptor;
        this.timeHumanizer = timeHumanizer;
    }
    async getTripGeoInformation(routes) {
        const totalDistance = parseFloat(routes
            .map((route) => route.distance.value)
            .reduce((a, b) => a + b, 0)
            .toFixed(2));
        const totalDuration = parseFloat(routes
            .map((route) => route.duration.value)
            .reduce((a, b) => a + b, 0)
            .toFixed(2));
        return {
            distance: `${totalDistance} km`,
            distanceValue: totalDistance,
            duration: this.timeHumanizer.humanize(totalDuration * 60000),
            durationValue: totalDuration,
        };
    }
    async buildNewRoute(origin, destination) {
        const routeGeoInformation = await this.tripGeolocationDescriptor.basicInfo(origin.geoLocation.join(','), destination.geoLocation.join(','));
        if (!routeGeoInformation) {
            throw new Error('not_found');
        }
        return {
            distance: {
                text: routeGeoInformation.distance,
                value: routeGeoInformation.distanceValue,
            },
            duration: {
                text: routeGeoInformation.duration,
                value: routeGeoInformation.durationValue,
            },
            origin: { ...origin, address: routeGeoInformation.originAddress },
            destination: {
                ...destination,
                address: routeGeoInformation.destinationAddress,
            },
        };
    }
    async buildRoutes(stops) {
        const points = stops.filter((stop, index) => index !== stops.length - 1);
        const promises = points.map((point, index) => {
            const isLastPoint = index === points.length - 1;
            return new Promise(async (resolve) => {
                const routeOrigin = point;
                const routeDestination = isLastPoint
                    ? stops[stops.length - 1]
                    : points[index + 1];
                const routeGeoInformation = await this.buildNewRoute(routeOrigin, routeDestination);
                resolve(routeGeoInformation);
            });
        });
        return await Promise.all(promises);
    }
}
exports.default = RequestInformationBuilder;
//# sourceMappingURL=request-information-builder.js.map