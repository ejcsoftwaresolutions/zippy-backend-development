import TripGeolocationDescriptor, { TripGeoInformation } from '@deliveryService/delivery/domain/contracts/trip-geolocation-descriptor';
import { RideGeoPoint, RideRoute } from '@deliveryService/delivery/domain/models/delivery-request-model';
import TimeHumanizer from '@shared/domain/utils/time-humanizer';
export default class RequestInformationBuilder {
    private tripGeolocationDescriptor;
    private timeHumanizer;
    constructor(tripGeolocationDescriptor: TripGeolocationDescriptor, timeHumanizer: TimeHumanizer);
    getTripGeoInformation(routes: RideRoute[]): Promise<TripGeoInformation>;
    buildNewRoute(origin: RideGeoPoint, destination: RideGeoPoint): Promise<RideRoute>;
    buildRoutes(stops: RideGeoPoint[]): Promise<RideRoute[]>;
}
