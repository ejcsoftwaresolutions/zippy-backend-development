"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const request_information_builder_1 = require("../../../store/domain/services/request-information-builder");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const collection_1 = require("../../../../shared/domain/value-object/collection");
class ProcessAttemptNextCandidateUseCase {
    constructor(requestTimerService, deliveryRequestRepository, requestInformationBuilder, eventBus) {
        this.requestTimerService = requestTimerService;
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.requestInformationBuilder = requestInformationBuilder;
        this.eventBus = eventBus;
    }
    async execute(request, shouldDiscardCurrentCandidate = true) {
        this.request = request;
        if (this.request.acceptedRider) {
            throw new Error('request_already_accepted');
        }
        const currentCandidate = await this.getNextAvailableCandidate(shouldDiscardCurrentCandidate);
        if (!currentCandidate) {
            console.log(`${this.request.id.value} Se llego al final`);
            await this.attemptGiveUp();
            return;
        }
        console.log(`${this.request.id.value} Se le esta enviando la notificación al rider ${currentCandidate.id}`);
        this.request.setCurrentAttemptAlertedCandidate(currentCandidate);
        if (currentCandidate.publicProfile) {
            this.request.updateServiceType(currentCandidate.publicProfile.deliveryMethod);
        }
        try {
            this.requestTimerService.start(this.request.id.value, currentCandidate.id);
        }
        catch (error) { }
        const notificationData = {
            id: this.request.id.value,
            driverId: currentCandidate.id,
            requestInfo: this.request.requestInfo.toPrimitives(),
            customer: this.request.customer.toPrimitives(),
            tripDistance: this.request.tripDistance,
            tripDuration: this.request.tripDuration,
            finalFee: this.request.finalFee,
            distanceToClient: currentCandidate.distanceToClient,
            distanceToStore: currentCandidate.distanceToStore,
            routes: this.request.routes.toPrimitives(),
            acceptanceTimeout: this.request.acceptanceTimeout,
            timerStart: new Date().getTime(),
            pushToken: currentCandidate.publicProfile.pushToken,
        };
        this.request.setCurrentAttemptAlertedCandidateNotification(notificationData);
        await this.deliveryRequestRepository.updateDriverNotification(this.request.currentAlertedCandidate, this.request.mode);
        await this.deliveryRequestRepository.updateRequest(this.request);
        this.eventBus.publish(this.request.pullDomainEvents());
    }
    async attemptGiveUp() {
        try {
            this.requestTimerService.stop(this.request.id.value);
            this.requestTimerService.destroy(this.request.id.value);
        }
        catch (error) { }
        if (this.request.currentAlertedCandidate) {
            this.request.setCurrentAttemptAlertedCandidateNotification(null);
            await this.deliveryRequestRepository.updateDriverNotification(this.request.currentAlertedCandidate, this.request.mode);
            await this.deliveryRequestRepository.toggleLockedCandidates(new collection_1.default([this.request.currentAlertedCandidate]), this.request.mode, false);
            this.request.setCurrentAttemptAlertedCandidate(null);
        }
        this.request.isLastAttempt
            ? this.request.setMaxAttemptsReached()
            : this.request.setRidersNotFoundInAttempt();
        await this.deliveryRequestRepository.updateRequest(this.request);
        if (this.request.isLastAttempt) {
            await this.deliveryRequestRepository.deleteRequest(this.request);
        }
    }
    async discardCurrentCandidate() {
        const discardedCandidate = this.request.currentAlertedCandidate;
        if (!discardedCandidate) {
            return;
        }
        this.request.discardCandidate(discardedCandidate);
        await this.deliveryRequestRepository.updateRequest(this.request);
    }
    async cleanCurrentAlertedNotification() {
        const discardedCandidate = this.request.currentAlertedCandidate;
        if (!discardedCandidate) {
            return;
        }
        this.request.setCurrentAttemptAlertedCandidateNotification(null);
        try {
            await this.deliveryRequestRepository.updateDriverNotification(this.request.currentAlertedCandidate, this.request.mode);
            await this.deliveryRequestRepository.toggleLockedCandidates(new collection_1.default([discardedCandidate]), this.request.mode, false);
        }
        catch (error) {
            console.log('ERROR AL DISCARD DB!');
        }
    }
    async getNextAvailableCandidate(shouldDiscardCurrentCandidate) {
        var _a;
        (_a = this.request.currentAlertedCandidate) === null || _a === void 0 ? void 0 : _a.setAvailableForRetry(!shouldDiscardCurrentCandidate);
        await this.discardCurrentCandidate();
        await this.cleanCurrentAlertedNotification();
        try {
            this.requestTimerService.reset(this.request.id.value);
        }
        catch (error) {
            console.log('Error trying to stop the timer');
        }
        const nextCandidate = await this.deliveryRequestRepository.findClosestRiderCandidate(this.request);
        if (!nextCandidate) {
            return null;
        }
        nextCandidate.setAttemptNumber(this.request.attemptNumber);
        this.request.addCandidateToCurrentAttempt(nextCandidate);
        return nextCandidate;
    }
}
exports.default = ProcessAttemptNextCandidateUseCase;
//# sourceMappingURL=process-attempt-next-candidate-use-case.js.map