import RequestInformationBuilder from '@deliveryService/store/domain/services/request-information-builder';
import EventBus from '@shared/domain/bus/event/event-bus';
import RequestAttemptCountdownTimer from '../../domain/contracts/request-attempt-countdown-timer';
import DeliveryRequest from '../../domain/models/delivery-request-model';
import DeliveryRequestCommandRepository from '../../domain/repositories/delivery-request-command-repository';
export default class ProcessAttemptNextCandidateUseCase {
    private requestTimerService;
    private deliveryRequestRepository;
    private requestInformationBuilder;
    private eventBus;
    private request;
    constructor(requestTimerService: RequestAttemptCountdownTimer, deliveryRequestRepository: DeliveryRequestCommandRepository, requestInformationBuilder: RequestInformationBuilder, eventBus: EventBus);
    execute(request: DeliveryRequest, shouldDiscardCurrentCandidate?: boolean): Promise<void>;
    private attemptGiveUp;
    private discardCurrentCandidate;
    private cleanCurrentAlertedNotification;
    private getNextAvailableCandidate;
}
