import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import DeliveryRequestCommandRepository from '../../domain/repositories/delivery-request-command-repository';
import StartRequestAttemptUseCase from './start-request-attempt-use-case';
export default class StartRequestAttemptEventSubscriber implements EventSubscriber {
    private useCase;
    private deliveryRequestRepository;
    constructor(useCase: StartRequestAttemptUseCase, deliveryRequestRepository: DeliveryRequestCommandRepository);
    subscribedTo(): Map<string, Function>;
    handle(event: any): Promise<void>;
}
