"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const request_information_builder_1 = require("../../../store/domain/services/request-information-builder");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const id_1 = require("../../../../shared/domain/id/id");
const process_attempt_next_candidate_use_case_1 = require("../process-attempt-next-candidate/process-attempt-next-candidate-use-case");
class StartRequestAttemptUseCase {
    constructor(requestTimerService, deliveryRequestRepository, requestInformationBuilder, eventBus) {
        this.requestTimerService = requestTimerService;
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.eventBus = eventBus;
        this.processNextCandidateService = new process_attempt_next_candidate_use_case_1.default(requestTimerService, deliveryRequestRepository, requestInformationBuilder, this.eventBus);
    }
    async execute(request) {
        this.request = request;
        if (!this.requestHasAttemptTimer()) {
            this.createRequestTimer();
            this.setUpTimerEvents();
        }
        this.request.processCurrentAttempt();
        await this.processNextCandidateService.execute(this.request);
    }
    setUpTimerEvents() {
        const onUpdate = (seconds) => {
            try {
                if (seconds === this.request.acceptanceTimeout)
                    return;
                const timer = this.requestTimerService.get(this.request.id.value);
                if (!timer || !timer.candidateId) {
                    return;
                }
                if (timer.interrupted) {
                    this.processNextCandidateService.execute(this.request);
                    return;
                }
                this.request.setCandidateCountDown(new id_1.default(timer.candidateId), seconds);
                this.deliveryRequestRepository.updateRequest(this.request);
            }
            catch (error) {
                console.log(`Error in timer update: ${error.message}`);
            }
        };
        const onTimeout = () => {
            const timer = this.requestTimerService.get(this.request.id.value);
            if (!timer)
                return;
            this.processNextCandidateService.execute(this.request, false);
        };
        try {
            this.requestTimerService.onUpdate(this.request.id.value, onUpdate);
            this.requestTimerService.onTimeout(this.request.id.value, onTimeout);
        }
        catch (error) {
        }
    }
    createRequestTimer() {
        this.requestTimerService.initialize(this.request.id.value, this.request.acceptanceTimeout);
    }
    requestHasAttemptTimer() {
        return this.requestTimerService.exists(this.request.id.value);
    }
}
exports.default = StartRequestAttemptUseCase;
//# sourceMappingURL=start-request-attempt-use-case.js.map