"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const event_subscriber_1 = require("../../../../shared/domain/bus/event/event-subscriber");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const start_request_attempt_use_case_1 = require("./start-request-attempt-use-case");
let StartRequestAttemptEventSubscriber = class StartRequestAttemptEventSubscriber {
    constructor(useCase, deliveryRequestRepository) {
        this.useCase = useCase;
        this.deliveryRequestRepository = deliveryRequestRepository;
    }
    subscribedTo() {
        const events = new Map();
        return events;
    }
    async handle(event) {
        const request = await this.deliveryRequestRepository.getRequest(event.aggregateId);
        this.useCase.execute(request);
    }
};
StartRequestAttemptEventSubscriber = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('StartRequestAttemptUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [start_request_attempt_use_case_1.default, Object])
], StartRequestAttemptEventSubscriber);
exports.default = StartRequestAttemptEventSubscriber;
//# sourceMappingURL=start-request-attempt-event-subscriber.js.map