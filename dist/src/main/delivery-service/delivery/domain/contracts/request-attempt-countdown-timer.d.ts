export default interface RequestAttemptCountdownTimer {
    toggleInterrupted(requestId: string, interrupted: boolean): void;
    start(requestId: string, candidateId: string): void;
    stop(requestId: string): void;
    reset(requestId: string): void;
    destroy(requestId: string): void;
    initialize(requestId: string, startAt: number): void;
    onUpdate(requestId: string, callback: Function): void;
    onTimeout(requestId: string, callback: Function): void;
    exists(requestId: string): boolean;
    get(requestId: string): {
        candidateId?: string;
        timer: any;
        interrupted: boolean;
    };
}
