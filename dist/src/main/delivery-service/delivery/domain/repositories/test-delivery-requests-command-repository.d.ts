import Id from '@shared/domain/id/id';
export default interface TestDeliveryRequestsCommandRepository {
    insertMockCandidatesInRadius(center: {
        latitude: number;
        longitude: number;
    }, radius: number): Promise<any>;
    createTestRider(id: Id, serviceType: string, position: {
        latitude: number;
        longitude: number;
    }): Promise<any>;
    createTestCustomer(id: Id, position: {
        latitude: number;
        longitude: number;
    }): Promise<any>;
    removeTestCustomer(id: Id): Promise<any>;
    removeTestDriver(id: Id, serviceType: string): Promise<any>;
    removeAllCustomers(): Promise<any>;
    removeAllDrivers(): Promise<any>;
}
