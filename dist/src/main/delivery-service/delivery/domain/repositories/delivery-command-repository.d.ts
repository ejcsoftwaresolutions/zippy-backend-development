import Id from '@shared/domain/id/id';
import Delivery from '../models/delivery-model';
export default interface DeliveryCommandRepository {
    createDelivery(delivery: Delivery): Promise<void>;
    updateDelivery(delivery: Delivery): Promise<void>;
    findDeliveryById(id: Id): Promise<Delivery>;
    cancelDelivery(delivery: Delivery): Promise<void>;
    completeDelivery(delivery: Delivery): Promise<void>;
}
