"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_NEW_RIDER_ALERTED';
class DeliveryNewRiderAlertedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryNewRiderAlertedDomainEvent;
DeliveryNewRiderAlertedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-new-rider-alerted-domain-event.js.map