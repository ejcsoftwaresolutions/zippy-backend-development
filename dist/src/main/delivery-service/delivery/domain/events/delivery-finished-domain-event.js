"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_FINISHED';
class DeliveryFinishedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryFinishedDomainEvent;
DeliveryFinishedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-finished-domain-event.js.map