"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_REQUEST_ATTEMPT_CREATED';
class DeliveryRequestAttemptCreatedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryRequestAttemptCreatedDomainEvent;
DeliveryRequestAttemptCreatedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-request-attempt-created-domain-event.js.map