"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_REQUEST_ATTEMPT_STARTED';
class DeliveryRequestAttemptStartedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryRequestAttemptStartedDomainEvent;
DeliveryRequestAttemptStartedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-request-attempt-started-domain-event.js.map