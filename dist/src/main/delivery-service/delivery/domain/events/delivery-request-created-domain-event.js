"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_REQUEST_CREATED';
class DeliveryRequestCreatedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryRequestCreatedDomainEvent;
DeliveryRequestCreatedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-request-created-domain-event.js.map