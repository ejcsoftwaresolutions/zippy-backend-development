import DomainEvent, { DomainEventProps } from '@shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {
        riderId: string;
        deliveryId: string;
        storeId: string;
        vendorOrderId: string;
        driverFee: number;
    };
}
export default class DeliveryFinishedDomainEvent extends DomainEvent<EventProps> {
    static eventName: string;
    eventName(): string;
}
export {};
