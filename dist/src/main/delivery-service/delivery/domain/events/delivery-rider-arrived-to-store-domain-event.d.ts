import DomainEvent, { DomainEventProps } from '@shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {
        riderId: string;
        deliveryId: string;
        storeId: string;
    };
}
export default class DeliveryRiderArrivedToStoreDomainEvent extends DomainEvent<EventProps> {
    static eventName: string;
    eventName(): string;
}
export {};
