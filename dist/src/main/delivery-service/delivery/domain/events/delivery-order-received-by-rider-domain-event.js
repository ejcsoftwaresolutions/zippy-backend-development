"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_ORDER_RECEIVED_BY_RIDER';
class DeliveryOrderReceivedByRiderDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryOrderReceivedByRiderDomainEvent;
DeliveryOrderReceivedByRiderDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-order-received-by-rider-domain-event.js.map