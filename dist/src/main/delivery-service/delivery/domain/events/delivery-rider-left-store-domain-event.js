"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_RIDER_LEFT_STORE';
class DeliveryRiderLeftStoreDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryRiderLeftStoreDomainEvent;
DeliveryRiderLeftStoreDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-rider-left-store-domain-event.js.map