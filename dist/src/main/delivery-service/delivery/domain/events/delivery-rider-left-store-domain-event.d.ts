import DomainEvent, { DomainEventProps } from '@shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {
        riderId: string;
        deliveryId: string;
    };
}
export default class DeliveryRiderLeftStoreDomainEvent extends DomainEvent<EventProps> {
    static eventName: string;
    eventName(): string;
}
export {};
