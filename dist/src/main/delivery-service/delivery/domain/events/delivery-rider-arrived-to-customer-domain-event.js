"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'DELIVERY_RIDER_ARRIVED_TO_CUSTOMER';
class DeliveryRiderArrivedToCustomerDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = DeliveryRiderArrivedToCustomerDomainEvent;
DeliveryRiderArrivedToCustomerDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=delivery-rider-arrived-to-customer-domain-event.js.map