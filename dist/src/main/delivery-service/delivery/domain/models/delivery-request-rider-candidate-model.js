"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rider_public_profile_model_1 = require("../../../rider/domain/models/rider-public-profile-model");
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const utils_1 = require("../../../../shared/domain/utils");
class DeliveryRequestRiderCandidate extends aggregate_root_1.default {
    constructor(constructorData) {
        super({ ...constructorData });
    }
    get id() {
        return this.props.id;
    }
    get distanceToClient() {
        return this.props.distanceToClient;
    }
    get durationToClient() {
        return this.props.durationToClient;
    }
    get distanceToStore() {
        return this.props.distanceToStore;
    }
    get durationToStore() {
        return this.props.durationToStore;
    }
    get publicProfile() {
        return this.props.publicProfile;
    }
    get isBlockedToTakeRequest() {
        return this.props.isBlockedToTakeRequest;
    }
    get countDown() {
        return this.props.countDown;
    }
    get notification() {
        return this.props.notification;
    }
    get attemptNumber() {
        return this.props.attemptNumber;
    }
    get initialCoords() {
        return this.props.initialCoords;
    }
    get availableForRetry() {
        return this.props.availableForRetry;
    }
    static fromPrimitives(data) {
        const profile = new DeliveryRequestRiderCandidate({
            ...data,
            publicProfile: data.publicProfile
                ? rider_public_profile_model_1.default.fromPrimitives(data.publicProfile)
                : undefined,
        });
        return profile;
    }
    setAttemptNumber(number) {
        this.props.attemptNumber = number;
    }
    setAvailableForRetry(available) {
        this.props.availableForRetry = available;
    }
    setNotification(notification) {
        this.props.notification = notification;
    }
    setCountDown(second) {
        this.props.countDown = second;
    }
    toggleBlock(isBlocked) {
        this.props.isBlockedToTakeRequest = isBlocked;
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown({
            id: json.id,
            publicProfile: json.publicProfile,
            distanceToClient: json.distanceToClient,
            durationToClient: json.durationToClient ? json.durationToClient : 0,
            distanceToStore: json.distanceToStore,
            durationToStore: json.durationToStore ? json.durationToStore : 0,
            isBlockedToTakeRequest: json.isBlockedToTakeRequest,
            attemptNumber: json.attemptNumber,
            availableForRetry: json.availableForRetry,
            initialCoords: json.initialCoords,
        });
    }
}
exports.default = DeliveryRequestRiderCandidate;
//# sourceMappingURL=delivery-request-rider-candidate-model.js.map