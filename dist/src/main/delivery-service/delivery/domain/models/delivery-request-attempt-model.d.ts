import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Collection from '@shared/domain/value-object/collection';
import DeliveryRequestRiderCandidate from './delivery-request-rider-candidate-model';
export interface DeliveryRequestAttemptProps {
    number: number;
    radius: number;
}
export default class DeliveryRequestAttempt extends AggregateRoot<DeliveryRequestAttemptProps> {
    constructor(constructParams: DeliveryRequestAttemptProps);
    private _candidates;
    get candidates(): Collection<DeliveryRequestRiderCandidate>;
    private _currentAlertedCandidate;
    get currentAlertedCandidate(): DeliveryRequestRiderCandidate;
    get number(): number;
    get radius(): number;
    setCandidates(candidates: Collection<DeliveryRequestRiderCandidate>): void;
    addCandidate(candidate: DeliveryRequestRiderCandidate): void;
    setCurrentAlertedCandidate(candidate: DeliveryRequestRiderCandidate | null): void;
}
