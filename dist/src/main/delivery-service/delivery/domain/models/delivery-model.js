"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const customer_public_profile_model_1 = require("../../../customer/domain/models/customer-public-profile-model");
const rider_public_profile_model_1 = require("../../../rider/domain/models/rider-public-profile-model");
const store_order_model_1 = require("../../../store/domain/models/store-order-model");
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const collection_1 = require("../../../../shared/domain/value-object/collection");
class Delivery extends aggregate_root_1.default {
    constructor(constructParams) {
        super({ ...constructParams });
    }
    get id() {
        return this.props.id;
    }
    get customer() {
        return this.props.customer;
    }
    get rider() {
        return this.props.rider;
    }
    get requestedAt() {
        return this.props.requestedAt;
    }
    get restaurantOrderId() {
        return this.props.orderDetails.id;
    }
    get acceptedAt() {
        return this.props.acceptedAt;
    }
    get distance() {
        return this.props.distance;
    }
    get duration() {
        return this.props.duration;
    }
    get finalFee() {
        return this.props.finalFee;
    }
    get status() {
        return this.props.status;
    }
    get orderCode() {
        return this.props.orderDetails.code;
    }
    get mode() {
        return this.props.mode;
    }
    setAssigned() {
        this.props.assigned = true;
    }
    static fromPrimitives(props) {
        return new Delivery({
            id: new id_1.default(props.id),
            customer: customer_public_profile_model_1.default.fromPrimitives(props.customer),
            rider: rider_public_profile_model_1.default.fromPrimitives(props.rider),
            requestedAt: props.requestedAt,
            acceptedAt: props.acceptedAt,
            distance: props.distance,
            duration: props.duration,
            finalFee: props.finalFee,
            status: props.status,
            mode: props.mode,
            routes: props.routes ? new collection_1.default(props.routes) : undefined,
            serviceConfig: props.serviceConfig,
            orderDetails: store_order_model_1.default.fromPrimitives(props.orderDetails),
            leftStoreAt: props.leftStoreAt,
            deliveredAt: props.deliveredAt,
            assigned: props.assigned
        });
    }
    static fromDeliveryRequest(request) {
        var _a;
        if (!request.acceptedRider.publicProfile) {
            throw new Error('NOT_PUBLIC_PROFILE_RIDER');
        }
        return Delivery.fromPrimitives({
            id: new id_1.default(request.requestInfo.id).value,
            customer: request.customer.toPrimitives(),
            rider: request.acceptedRider.publicProfile.toPrimitives(),
            status: 'ACCEPTED',
            duration: request.tripDuration,
            distance: request.tripDistance,
            finalFee: request.finalFee,
            acceptedAt: new Date(),
            requestedAt: request.date ? request.date : new Date(),
            mode: request.mode,
            routes: (_a = request.routes) === null || _a === void 0 ? void 0 : _a.toPrimitives(),
            orderDetails: request.requestInfo.toPrimitives()
        });
    }
    toPrimitives() {
        var _a;
        return {
            id: this.id.value,
            customer: this.customer.toPrimitives(),
            rider: this.rider.toPrimitives(),
            requestedAt: this.requestedAt,
            acceptedAt: this.acceptedAt,
            distance: this.distance,
            duration: this.duration,
            finalFee: this.finalFee,
            status: this.status,
            mode: this.props.mode,
            routes: (_a = this.props.routes) === null || _a === void 0 ? void 0 : _a.toPrimitives(),
            orderDetails: this.props.orderDetails.toPrimitives(),
            leftStoreAt: this.props.leftStoreAt,
            deliveredAt: this.props.deliveredAt,
            assigned: this.props.assigned
        };
    }
}
exports.default = Delivery;
//# sourceMappingURL=delivery-model.js.map