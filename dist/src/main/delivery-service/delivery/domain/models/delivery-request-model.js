"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const store_order_model_1 = require("../../../store/domain/models/store-order-model");
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const delivery_category_1 = require("../../../../shared/domain/models/delivery-category");
const collection_1 = require("../../../../shared/domain/value-object/collection");
const customer_public_profile_model_1 = require("../../../customer/domain/models/customer-public-profile-model");
const delivery_new_rider_alerted_domain_event_1 = require("../events/delivery-new-rider-alerted-domain-event");
const delivery_request_accepted_domain_event_1 = require("../events/delivery-request-accepted-domain-event");
const delivery_request_attempt_created_domain_event_1 = require("../events/delivery-request-attempt-created-domain-event");
const delivery_request_attempt_started_domain_event_1 = require("../events/delivery-request-attempt-started-domain-event");
const delivery_request_created_domain_event_1 = require("../events/delivery-request-created-domain-event");
const delivery_request_attempt_model_1 = require("./delivery-request-attempt-model");
const delivery_request_rider_candidate_model_1 = require("./delivery-request-rider-candidate-model");
const DefaultProps = {};
class DeliveryRequest extends aggregate_root_1.default {
    constructor(props) {
        super({ ...DefaultProps, ...props });
        this._status = 'SEARCHING';
        this._discardedCandidates = new collection_1.default([]);
    }
    get status() {
        return this._status;
    }
    get discardedCandidates() {
        return this._discardedCandidates;
    }
    get acceptedRider() {
        return this._acceptedRider;
    }
    get preCandidates() {
        return this.props.preCandidates;
    }
    get preCandidate() {
        if (!this.props.preCandidates)
            return undefined;
        if (this.props.preCandidates.length == 0)
            return undefined;
        return this.props.preCandidates[0];
    }
    get id() {
        return this.props.id;
    }
    get customer() {
        return this.props.customer;
    }
    get deliveryFee() {
        return this.props.requestInfo.deliveryFee;
    }
    get searchRadius() {
        return this.props.serviceConfig.attemptSearchRadius;
    }
    get acceptanceTimeout() {
        return this.props.serviceConfig.acceptanceTimeout;
    }
    get maxAttempts() {
        return this.props.serviceConfig.maxAttempts;
    }
    get attemptMaxCandidates() {
        return this.props.serviceConfig.attemptMaxCandidates;
    }
    get tripDistance() {
        return this.props.tripDistance;
    }
    get requestInfo() {
        return this.props.requestInfo;
    }
    get storeAddress() {
        return this.props.routes.getAt(0).destination.address;
    }
    get tripDuration() {
        return this.props.tripDuration;
    }
    get date() {
        return this.props.date;
    }
    get mode() {
        return this.props.mode;
    }
    get isTest() {
        return this.props.mode === 'test';
    }
    get routes() {
        return this.props.routes;
    }
    get serviceType() {
        return this.props.serviceType;
    }
    get attemptNumber() {
        return this._currentAttempt.number;
    }
    get currentAlertedCandidate() {
        return this._currentAttempt.currentAlertedCandidate;
    }
    get currentAttemptRadius() {
        return this._currentAttempt.radius;
    }
    get finalFee() {
        return {
            amount: this.deliveryFee,
            currency: '$',
        };
    }
    get currentAttemptCandidates() {
        return this._currentAttempt.candidates;
    }
    get remainingCandidatesCount() {
        if (!this.currentAttemptCandidates) {
            return 0;
        }
        return (this.currentAttemptCandidates.count() -
            this._discardedCandidates.items.filter((candidate) => candidate.attemptNumber === this.attemptNumber).length);
    }
    get isLastAttempt() {
        return this.attemptNumber === this.maxAttempts;
    }
    static fromPrimitives(data) {
        if (!data.serviceConfig) {
            throw new Error('Please provide service configurations model');
        }
        const request = new DeliveryRequest({
            id: new id_1.default(data.id),
            customer: customer_public_profile_model_1.default.fromPrimitives({
                ...data.customer,
            }),
            requestInfo: store_order_model_1.default.fromPrimitives(data.requestInfo),
            serviceConfig: data.serviceConfig,
            tripDistance: data.tripDistance,
            tripDuration: data.tripDuration,
            date: data.date,
            mode: data.mode,
            preCandidates: data.preCandidates,
            routes: data.routes ? new collection_1.default(data.routes) : undefined,
            serviceType: data.serviceType,
        });
        if (data.discardedCandidates) {
            request._discardedCandidates =
                new collection_1.default(data.discardedCandidates.map((candidate) => new delivery_request_rider_candidate_model_1.default({
                    ...candidate,
                })));
        }
        request._currentAttempt = new delivery_request_attempt_model_1.default({
            number: data.currentAttempt.number,
            radius: data.currentAttempt.radius,
        });
        if (data.currentAttempt.currentAlertedCandidate) {
            request.setCurrentAttemptAlertedCandidate(delivery_request_rider_candidate_model_1.default.fromPrimitives({
                ...data.currentAttempt.currentAlertedCandidate,
                isBlockedToTakeRequest: true,
            }));
        }
        request._status = data.status;
        return request;
    }
    static create(deliveryRequest) {
        const newDeliveryRequest = new DeliveryRequest(deliveryRequest);
        newDeliveryRequest.record(new delivery_request_created_domain_event_1.default({
            aggregateId: newDeliveryRequest.id.value,
            eventData: {
                customerId: newDeliveryRequest.customer.id,
            },
            occurredOn: new Date(),
        }));
        return newDeliveryRequest;
    }
    setPreCandidates(preCandidates) {
        if (!preCandidates) {
            this.props.preCandidates = [];
            return;
        }
        this.props.preCandidates = preCandidates;
    }
    updateServiceType(serviceType) {
        this.props.serviceType = serviceType;
    }
    guardExistFeesForType() {
        this.props.serviceConfig.guardExistFeesForType(this.props.serviceType);
    }
    discardCandidate(candidate) {
        const candidateIndex = this._discardedCandidates.findIndex(candidate);
        this._discardedCandidates.replaceAt(candidateIndex, candidate);
    }
    addCandidateToCurrentAttempt(candidate) {
        this._currentAttempt.addCandidate(candidate);
    }
    setCurrentAttemptAlertedCandidate(candidate) {
        this._currentAttempt.setCurrentAlertedCandidate(candidate);
    }
    setCurrentAttemptAlertedCandidateNotification(notification) {
        this._currentAttempt.currentAlertedCandidate.setNotification(notification);
        if (!notification) {
            return;
        }
        this.record(new delivery_new_rider_alerted_domain_event_1.default({
            aggregateId: this.id.value,
            eventData: {
                riderId: this._currentAttempt.currentAlertedCandidate.id,
            },
            occurredOn: new Date(),
        }));
    }
    setCandidateCountDown(candidateId, seconds) {
        const candidate = this._currentAttempt.candidates.items.find((c) => c.id === candidateId.value);
        if (!candidate) {
            return;
        }
        candidate.setCountDown(seconds);
    }
    setMaxAttemptsReached() {
        this._status = 'NOT_FOUND_MAX_ATTEMPTS_REACHED';
    }
    setRidersNotFoundInAttempt() {
        this._status = 'DRIVERS_NOT_FOUND_IN_ATTEMPT';
    }
    markAsCanceled() {
        this._status = 'CANCELED';
    }
    toPrimitives() {
        var _a;
        return {
            id: this.id.value,
            preCandidates: this.props.preCandidates,
            currentAttempt: {
                number: this._currentAttempt.number,
                radius: this._currentAttempt.radius,
                candidates: this._currentAttempt.candidates
                    ? this._currentAttempt.candidates.map((candidate) => candidate.toPrimitives())
                    : [],
                currentAlertedCandidate: this._currentAttempt && this._currentAttempt.currentAlertedCandidate
                    ? this._currentAttempt.currentAlertedCandidate.toPrimitives()
                    : null,
            },
            customer: this.customer.toPrimitives(),
            discardedCandidates: this._discardedCandidates
                ? this._discardedCandidates.map((candidate) => candidate.toPrimitives())
                : [],
            tripDistance: this.tripDistance,
            tripDuration: this.tripDuration,
            finalFee: this.finalFee,
            status: this._status,
            acceptedRider: this._acceptedRider
                ? this._acceptedRider.toPrimitives()
                : null,
            requestInfo: this.props.requestInfo.toPrimitives(),
            date: this.date ? this.props.date : new Date(),
            mode: this.props.mode,
            routes: (_a = this.props.routes) === null || _a === void 0 ? void 0 : _a.toPrimitives(),
            serviceType: this.props.serviceType,
            maxAttempts: this.props.serviceConfig.maxAttempts,
        };
    }
    registerNewAttempt() {
        const number = !this._currentAttempt ? 1 : this._currentAttempt.number + 1;
        const newAttempt = new delivery_request_attempt_model_1.default({
            number: number,
            radius: this.searchRadius,
        });
        this._currentAttempt = newAttempt;
        this._status = 'SEARCHING';
        this.record(new delivery_request_attempt_created_domain_event_1.default({
            aggregateId: this.id.value,
            eventData: {
                customerId: this.id.value,
            },
            occurredOn: new Date(),
        }));
        return newAttempt;
    }
    processCurrentAttempt() {
        if (!this._currentAttempt) {
            throw new Error("There's not an active attempt registered. can't start processing");
        }
        this.record(new delivery_request_attempt_started_domain_event_1.default({
            aggregateId: this.id.value,
            eventData: {
                customerId: this.id.value,
            },
            occurredOn: new Date(),
        }));
    }
    setAcceptedRider(rider) {
        this._acceptedRider = rider;
        this._status = 'ACCEPTED';
        this.record(new delivery_request_accepted_domain_event_1.default({
            aggregateId: this.id.value,
            eventData: {
                riderId: this._acceptedRider.id,
            },
            occurredOn: new Date(),
        }));
    }
    updateRoutes(routes) {
        this.props.routes = routes;
    }
}
exports.default = DeliveryRequest;
//# sourceMappingURL=delivery-request-model.js.map