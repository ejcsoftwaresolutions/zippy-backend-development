import RiderPublicProfile, { DriverPublicProfilePrimitiveProps } from '@deliveryService/rider/domain/models/rider-public-profile-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import { RideGeoPoint } from './delivery-request-model';
export interface DeliveryRequestRiderCandidateProps {
    distanceToClient?: number;
    durationToClient?: number;
    distanceToStore?: number;
    durationToStore?: number;
    id: string;
    publicProfile?: RiderPublicProfile;
    notification?: any;
    countDown?: number;
    isBlockedToTakeRequest: boolean;
    attemptNumber: number;
    initialCoords?: RideGeoPoint;
    availableForRetry?: boolean;
}
export interface DeliveryRequestRiderCandidatePrimitiveProps {
    distanceToClient?: number;
    durationToClient?: number;
    distanceToStore?: number;
    durationToStore?: number;
    id: string;
    publicProfile?: DriverPublicProfilePrimitiveProps;
    isBlockedToTakeRequest: boolean;
    attemptNumber: number;
    notification?: any;
    countDown?: number;
    initialCoords?: RideGeoPoint;
    availableForRetry?: boolean;
}
export default class DeliveryRequestRiderCandidate extends AggregateRoot<DeliveryRequestRiderCandidateProps> {
    constructor(constructorData: DeliveryRequestRiderCandidateProps);
    get id(): string;
    get distanceToClient(): number;
    get durationToClient(): number;
    get distanceToStore(): number;
    get durationToStore(): number;
    get publicProfile(): RiderPublicProfile;
    get isBlockedToTakeRequest(): boolean;
    get countDown(): number;
    get notification(): any;
    get attemptNumber(): number;
    get initialCoords(): RideGeoPoint;
    get availableForRetry(): boolean;
    static fromPrimitives(data: DeliveryRequestRiderCandidatePrimitiveProps): DeliveryRequestRiderCandidate;
    setAttemptNumber(number: number): void;
    setAvailableForRetry(available: any): void;
    setNotification(notification: any): void;
    setCountDown(second: number): void;
    toggleBlock(isBlocked: boolean): void;
    toPrimitives(): DeliveryRequestRiderCandidatePrimitiveProps;
}
