import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';
import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';
import StoreOrder, { StoreOrderPrimitiveProps } from '@deliveryService/store/domain/models/store-order-model';
import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import Collection from '@shared/domain/value-object/collection';
import DeliveryRequest, { RideMeasurement, RideRoute } from './delivery-request-model';
import ServiceConfigurations, { Price } from './service-configurations-model';
export type ActiveRideRoute = RideRoute & {
    completed: boolean;
    completedAt?: Date;
};
export type DeliveryStatus = 'ACCEPTED' | 'ARRIVED_TO_STORE' | 'ORDER_RECEIVED' | 'ON_THE_WAY_TO_CUSTOMER' | 'ARRIVED_TO_CUSTOMER' | 'DELIVERED';
export interface DeliveryProps {
    id: Id;
    customer: CustomerPublicProfile;
    rider: RiderPublicProfile;
    serviceConfig: ServiceConfigurations;
    requestedAt: Date;
    acceptedAt: Date;
    distance: RideMeasurement;
    duration: RideMeasurement;
    finalFee: Price;
    status: DeliveryStatus;
    routes: Collection<ActiveRideRoute>;
    orderDetails: StoreOrder;
    leftStoreAt?: Date;
    deliveredAt?: Date;
    mode: 'test' | 'live';
    assigned?: boolean;
}
interface DeliveryPrimitiveProps {
    id: string;
    customer: any;
    rider: any;
    requestedAt: Date;
    acceptedAt: Date;
    distance: RideMeasurement;
    duration: RideMeasurement;
    finalFee: Price;
    status: DeliveryStatus;
    serviceConfig?: ServiceConfigurations;
    routes: ActiveRideRoute[];
    orderDetails: StoreOrderPrimitiveProps;
    leftStoreAt?: Date;
    deliveredAt?: Date;
    mode: 'test' | 'live';
    assigned?: boolean;
}
export default class Delivery extends AggregateRoot<DeliveryProps> {
    constructor(constructParams: DeliveryProps);
    get id(): Id;
    get customer(): CustomerPublicProfile;
    get rider(): RiderPublicProfile;
    get requestedAt(): Date;
    get restaurantOrderId(): string;
    get acceptedAt(): Date;
    get distance(): RideMeasurement;
    get duration(): RideMeasurement;
    get finalFee(): Price;
    get status(): DeliveryStatus;
    get orderCode(): string;
    get mode(): "test" | "live";
    setAssigned(): void;
    static fromPrimitives(props: DeliveryPrimitiveProps): Delivery;
    static fromDeliveryRequest(request: DeliveryRequest): Delivery;
    toPrimitives(): DeliveryPrimitiveProps;
}
export {};
