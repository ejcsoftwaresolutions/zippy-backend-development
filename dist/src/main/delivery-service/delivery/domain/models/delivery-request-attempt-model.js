"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const collection_1 = require("../../../../shared/domain/value-object/collection");
class DeliveryRequestAttempt extends aggregate_root_1.default {
    constructor(constructParams) {
        super({ ...constructParams });
        this._currentAlertedCandidate = null;
        this._candidates = new collection_1.default([]);
    }
    get candidates() {
        return this._candidates;
    }
    get currentAlertedCandidate() {
        return this._currentAlertedCandidate;
    }
    get number() {
        return this.props.number;
    }
    get radius() {
        return this.props.radius;
    }
    setCandidates(candidates) {
        this._candidates = candidates;
    }
    addCandidate(candidate) {
        this._candidates.add(candidate);
    }
    setCurrentAlertedCandidate(candidate) {
        this._currentAlertedCandidate = candidate;
    }
}
exports.default = DeliveryRequestAttempt;
//# sourceMappingURL=delivery-request-attempt-model.js.map