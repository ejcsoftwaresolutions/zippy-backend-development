import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
export type DeliveryAvailableTypes = 'CAR' | 'MOTOR_CYCLE' | 'BIKE';
export type Price = {
    amount: number;
    currency: string;
};
export type DistanceRange = {
    min: number;
    max?: number;
};
export type Fee = {
    distanceRange: DistanceRange;
    baseFee: Price;
    kmFee?: Price;
    durationFee?: Price;
    additionalDurationFee?: Price;
};
export type SearchConfigurations = {
    attemptSearchRadius: number;
    attemptMaxCandidates: number;
    maxAttempts: number;
    acceptanceTimeout: number;
};
export type ServiceConfigurationsProps = {
    searchConfigurations: SearchConfigurations;
    feeConfigurations: {
        [type in DeliveryAvailableTypes]: Fee[];
    };
};
export type ServiceConfigurationsPrimitiveProps = {
    searchConfigurations: SearchConfigurations;
    feeConfigurations: {
        [type in DeliveryAvailableTypes]: Fee[];
    };
};
export default class ServiceConfigurations extends AggregateRoot<ServiceConfigurationsProps> {
    constructor(props: ServiceConfigurationsProps);
    get attemptSearchRadius(): number;
    get acceptanceTimeout(): number;
    get maxAttempts(): number;
    get attemptMaxCandidates(): number;
    static fromPrimitives(plainData: ServiceConfigurationsPrimitiveProps): ServiceConfigurations;
    guardExistFeesForType(type: string): void;
    toPrimitives(): import("../../../../shared/domain/types").AnyObject;
}
