"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
class ServiceConfigurations extends aggregate_root_1.default {
    constructor(props) {
        super({ ...props });
    }
    get attemptSearchRadius() {
        return this.props.searchConfigurations.attemptSearchRadius;
    }
    get acceptanceTimeout() {
        return this.props.searchConfigurations.acceptanceTimeout;
    }
    get maxAttempts() {
        return this.props.searchConfigurations.maxAttempts;
    }
    get attemptMaxCandidates() {
        return this.props.searchConfigurations.attemptMaxCandidates;
    }
    static fromPrimitives(plainData) {
        return new ServiceConfigurations({
            ...plainData,
        });
    }
    guardExistFeesForType(type) {
        if (!this.props.feeConfigurations[type]) {
            throw new Error(`fees_not_found_for_category`);
        }
    }
    toPrimitives() {
        const json = this.toJson();
        return json;
    }
}
exports.default = ServiceConfigurations;
//# sourceMappingURL=service-configurations-model.js.map