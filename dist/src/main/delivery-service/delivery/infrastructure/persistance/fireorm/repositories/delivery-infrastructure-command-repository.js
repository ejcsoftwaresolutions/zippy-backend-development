"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const customer_public_profile_query_repository_1 = require("../../../../../customer/domain/repositories/customer-public-profile-query-repository");
const delivery_model_1 = require("../../../../domain/models/delivery-model");
const delivery_command_repository_1 = require("../../../../domain/repositories/delivery-command-repository");
const service_configurations_query_repository_1 = require("../../../../domain/repositories/service-configurations-query-repository");
const rider_public_profile_query_repository_1 = require("../../../../../rider/domain/repositories/rider-public-profile-query-repository");
const rider_public_profile_mapper_1 = require("../../../../../rider/infrastructure/persistance/fireorm/mappers/rider-public-profile-mapper");
const decorators_1 = require("../../../../../../shared/domain/decorators");
const id_1 = require("../../../../../../shared/domain/id/id");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const nestjs_firebase_admin_1 = require("@tfarras/nestjs-firebase-admin");
const constants_1 = require("../constants");
const delivery_entity_1 = require("../entities/delivery-entity");
const delivery_mapper_1 = require("../mappers/delivery-mapper");
let DeliveryCommandInfrastructureRepository = class DeliveryCommandInfrastructureRepository extends fireorm_repository_1.default {
    constructor(firebase, driverPublicProfileRepo, customerPublicProfileRepo, serviceConfigQueryRepository) {
        super();
        this.firebase = firebase;
        this.driverPublicProfileRepo = driverPublicProfileRepo;
        this.customerPublicProfileRepo = customerPublicProfileRepo;
        this.serviceConfigQueryRepository = serviceConfigQueryRepository;
    }
    getEntityClass() {
        return delivery_entity_1.DeliveryEntity;
    }
    async createDelivery(delivery) {
        const data = delivery_mapper_1.default.toPersistence(delivery);
        await this.repository().create(utils_1.ObjectUtils.omitUnknown(data));
        try {
            const rider = rider_public_profile_mapper_1.default.toPersistence(delivery.rider);
            await this.updateRestaurantOrderRider(delivery.restaurantOrderId, {
                ...rider,
            }, 'Driver Accepted');
        }
        catch (error) {
            console.log(error.message);
        }
    }
    async updateDelivery(delivery) {
        const data = delivery_mapper_1.default.toPersistence(delivery);
        await this.repository().update(utils_1.ObjectUtils.omitUnknown(data));
        try {
            await this.updateIntermediateOrderState(delivery);
        }
        catch (error) {
            console.log(error.message);
        }
    }
    async updateIntermediateOrderState(delivery) {
        switch (delivery.status) {
            case 'ORDER_RECEIVED':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Order Received');
                break;
            case 'ON_THE_WAY_TO_CUSTOMER':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'In Transit');
                break;
            case 'ARRIVED_TO_STORE':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Arrived to Store');
                break;
            case 'ARRIVED_TO_CUSTOMER':
                await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Arrived to Customer');
                break;
            default:
                break;
        }
    }
    async cancelDelivery(delivery) {
        const data = delivery_mapper_1.default.toPersistence(delivery);
        await this.repository().update(utils_1.ObjectUtils.omitUnknown(data));
        await this.toggleBlockDriver(data.driverId, data.mode, false);
    }
    async completeDelivery(delivery) {
        const data = delivery_mapper_1.default.toPersistence(delivery);
        await this.repository().update(utils_1.ObjectUtils.omitUnknown(data));
        try {
            await this.updateRestaurantOrderStatus(delivery.restaurantOrderId, 'Order Completed');
        }
        catch (error) {
            console.log(error.message);
        }
        try {
            await this.resetChat(data.customerId, data.driverId);
        }
        catch (error) {
        }
        await this.toggleBlockDriver(data.driverId, data.mode, false);
    }
    async resetChat(clientId, driverId) {
        const actions = [this.firebase
                .firestore()
                .collection('channels')
                .doc(`${clientId}${driverId}`)
                .delete(),
            this.firebase
                .firestore()
                .collection('channels')
                .doc(`${driverId}${clientId}`)
                .delete(),];
        Promise.all(actions);
    }
    async findDeliveryById(id) {
        const element = await this.repository()
            .whereEqualTo('id', id.value)
            .findOne();
        if (!element)
            return null;
        const customerPublicProfile = await this.customerPublicProfileRepo.findProfileById(element.customerId);
        const driverPublicProfile = await this.driverPublicProfileRepo.findProfileById(element.driverId);
        const serviceConfiguration = await this.serviceConfigQueryRepository.getConfigurations();
        if (!driverPublicProfile) {
            throw new Error('RIDER_PUBLIC_PROFILE_NOT_FOUND');
        }
        if (!customerPublicProfile) {
            throw new Error('CUSTOMER_PUBLIC_PROFILE_NOT_FOUND');
        }
        return delivery_mapper_1.default.toDomain({
            ...element,
            riderPublicProfile: driverPublicProfile,
            customerPublicProfile: customerPublicProfile,
            serviceConfig: serviceConfiguration,
        });
    }
    async toggleBlockDriver(driverId, mode, blocked) {
        const BLOCKED_DRIVERS_REF = this.firebase
            .database()
            .ref(`${constants_1.BUSY_RIDERS}${mode === 'test' ? '_test' : ''}`);
        return BLOCKED_DRIVERS_REF.child(driverId).transaction((blockedDriver) => {
            blockedDriver = blocked ? true : null;
            return blockedDriver;
        });
    }
    async updateRestaurantOrderRider(orderId, driver, status) {
        return this.firebase
            .firestore()
            .collection(constants_1.RESTAURANT_ORDERS)
            .doc(orderId)
            .update(utils_1.ObjectUtils.omitUnknown({
            status: status, driver: driver, driverID: driver.id, acceptedAt: new Date(),
        }));
    }
    async updateRestaurantOrderStatus(orderId, status) {
        return this.firebase
            .firestore()
            .collection(constants_1.RESTAURANT_ORDERS)
            .doc(orderId)
            .update(utils_1.ObjectUtils.omitUnknown({
            status: status,
        }));
    }
};
DeliveryCommandInfrastructureRepository.bindingKey = 'DeliveryCommandRepository';
DeliveryCommandInfrastructureRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)(nestjs_firebase_admin_1.FIREBASE_ADMIN_INJECT)),
    tslib_1.__param(1, (0, decorators_1.inject)('RiderPublicProfileQueryRepository')),
    tslib_1.__param(2, (0, decorators_1.inject)('CustomerPublicProfileQueryRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('ServiceConfigurationsQueryRepository')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object])
], DeliveryCommandInfrastructureRepository);
exports.default = DeliveryCommandInfrastructureRepository;
//# sourceMappingURL=delivery-infrastructure-command-repository.js.map