export declare class DeliveryEntity {
    id: string;
    customerId: string;
    driverId: string;
    storeId: string;
    requestedAt: Date;
    acceptedAt: Date;
    distance: any;
    duration: any;
    finalFee: any;
    status: string;
    paid: boolean;
    charged: boolean;
    rated: boolean;
    mode: string;
    routes: any[];
    customerInfo: any;
    driverInfo: any;
    orderDetails: any;
    leftStoreAt?: Date;
    deliveredAt?: Date;
    assigned?: boolean;
}
