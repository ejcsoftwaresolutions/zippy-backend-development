import CustomerPublicProfileQueryRepository from '@deliveryService/customer/domain/repositories/customer-public-profile-query-repository';
import Delivery from '@deliveryService/delivery/domain/models/delivery-model';
import DeliveryCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-command-repository';
import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import RiderPublicProfileQueryRepository from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import Id from '@shared/domain/id/id';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin';
import { EntityConstructorOrPath } from 'fireorm';
import { DeliveryEntity } from '../entities/delivery-entity';
export default class DeliveryCommandInfrastructureRepository extends FireOrmRepository<DeliveryEntity> implements DeliveryCommandRepository {
    private firebase;
    private driverPublicProfileRepo;
    private customerPublicProfileRepo;
    private serviceConfigQueryRepository;
    static readonly bindingKey = "DeliveryCommandRepository";
    constructor(firebase: FirebaseAdminSDK, driverPublicProfileRepo: RiderPublicProfileQueryRepository, customerPublicProfileRepo: CustomerPublicProfileQueryRepository, serviceConfigQueryRepository: ServiceConfigurationsQueryRepository);
    getEntityClass(): EntityConstructorOrPath<DeliveryEntity>;
    createDelivery(delivery: Delivery): Promise<void>;
    updateDelivery(delivery: Delivery): Promise<void>;
    updateIntermediateOrderState(delivery: Delivery): Promise<void>;
    cancelDelivery(delivery: Delivery): Promise<void>;
    completeDelivery(delivery: Delivery): Promise<void>;
    resetChat(clientId: string, driverId: string): Promise<void>;
    findDeliveryById(id: Id): Promise<Delivery>;
    private toggleBlockDriver;
    private updateRestaurantOrderRider;
    private updateRestaurantOrderStatus;
}
