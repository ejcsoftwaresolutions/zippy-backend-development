"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeliveryEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let DeliveryEntity = class DeliveryEntity {
};
DeliveryEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('delivery_orders')
], DeliveryEntity);
exports.DeliveryEntity = DeliveryEntity;
//# sourceMappingURL=delivery-entity.js.map