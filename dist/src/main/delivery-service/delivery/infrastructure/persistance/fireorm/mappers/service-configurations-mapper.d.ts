import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import { ServiceConfigurationsEntity } from '../entities/service-configurations-entity';
export default class ServiceConfigurationsMapper {
    static toDomain(plainValues: ServiceConfigurationsEntity): ServiceConfigurations;
}
