import { RideMeasurement } from '../../../../domain/models/delivery-request-model';
import { Price } from '../../../../domain/models/service-configurations-model';
export default interface ActiveDeliveryNotificationEntity {
    id: string;
    finalFee: Price;
    customer: any;
    acceptedDriver: any;
    tripDistance: RideMeasurement;
    tripDuration: RideMeasurement;
    status: string;
    requestInfo: any;
}
