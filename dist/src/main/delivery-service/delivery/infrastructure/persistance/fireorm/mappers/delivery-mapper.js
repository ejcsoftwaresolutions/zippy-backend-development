"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const customer_public_profile_model_1 = require("../../../../../customer/domain/models/customer-public-profile-model");
const delivery_model_1 = require("../../../../domain/models/delivery-model");
const service_configurations_model_1 = require("../../../../domain/models/service-configurations-model");
const rider_public_profile_model_1 = require("../../../../../rider/domain/models/rider-public-profile-model");
const delivery_entity_1 = require("../entities/delivery-entity");
class DeliveryMapper {
    static toDomain(plainValues) {
        return delivery_model_1.default.fromPrimitives({
            id: plainValues.id,
            customer: plainValues.customerPublicProfile.toPrimitives(),
            rider: plainValues.riderPublicProfile.toPrimitives(),
            acceptedAt: plainValues.acceptedAt,
            distance: plainValues.distance,
            duration: plainValues.duration,
            finalFee: plainValues.finalFee,
            requestedAt: plainValues.requestedAt,
            status: plainValues.status,
            mode: plainValues.mode,
            routes: plainValues.routes,
            serviceConfig: plainValues.serviceConfig,
            orderDetails: plainValues.orderDetails,
            leftStoreAt: plainValues.leftStoreAt,
            deliveredAt: plainValues.deliveredAt,
        });
    }
    static toPersistence(delivery) {
        const primitives = delivery.toPrimitives();
        const entity = new delivery_entity_1.DeliveryEntity();
        entity.id = delivery.id.value;
        entity.driverId = primitives.rider.id;
        entity.customerId = primitives.customer.id;
        entity.storeId = primitives.orderDetails.store.id;
        entity.acceptedAt = primitives.acceptedAt;
        entity.distance = primitives.distance;
        entity.duration = primitives.duration;
        entity.finalFee = primitives.finalFee;
        entity.requestedAt = new Date();
        entity.status = primitives.status;
        entity.mode = primitives.mode;
        entity.routes = primitives.routes;
        entity.orderDetails = primitives.orderDetails;
        entity.customerInfo = primitives.customer;
        entity.driverInfo = primitives.rider;
        entity.leftStoreAt = primitives.leftStoreAt;
        entity.deliveredAt = primitives.deliveredAt;
        entity.assigned = primitives.assigned;
        return entity;
    }
}
exports.default = DeliveryMapper;
//# sourceMappingURL=delivery-mapper.js.map