"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PUSH_TOKENS = exports.VENDORS = exports.RIDERS = exports.VENDOR_PRODUCTS = exports.RESTAURANT_ORDERS = exports.TEST_CUSTOMERS = exports.DRIVER_ALERTS = exports.DELIVERY_REQUESTS = exports.BUSY_RIDERS = exports.AVAILABLE_RIDERS = exports.CONNECTED_RIDERS = exports.RIDERS_LOCATIONS = void 0;
exports.RIDERS_LOCATIONS = 'rider_locations';
exports.CONNECTED_RIDERS = 'connected_riders';
exports.AVAILABLE_RIDERS = 'available_riders';
exports.BUSY_RIDERS = 'busy_riders';
exports.DELIVERY_REQUESTS = 'delivery_requests';
exports.DRIVER_ALERTS = 'driver_delivery_request_alerts';
exports.TEST_CUSTOMERS = 'test_customers';
exports.RESTAURANT_ORDERS = 'orders';
exports.VENDOR_PRODUCTS = 'vendor_products';
exports.RIDERS = 'riders';
exports.VENDORS = 'vendors';
exports.PUSH_TOKENS = 'user_push_tokens';
//# sourceMappingURL=index.js.map