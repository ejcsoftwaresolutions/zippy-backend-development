"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const delivery_request_model_1 = require("../../../../domain/models/delivery-request-model");
const delivery_request_rider_candidate_model_1 = require("../../../../domain/models/delivery-request-rider-candidate-model");
const delivery_request_command_repository_1 = require("../../../../domain/repositories/delivery-request-command-repository");
const service_configurations_query_repository_1 = require("../../../../domain/repositories/service-configurations-query-repository");
const rider_public_profile_query_repository_1 = require("../../../../../rider/domain/repositories/rider-public-profile-query-repository");
const decorators_1 = require("../../../../../../shared/domain/decorators");
const utils_1 = require("../../../../../../shared/domain/utils");
const collection_1 = require("../../../../../../shared/domain/value-object/collection");
const nestjs_firebase_admin_1 = require("@tfarras/nestjs-firebase-admin");
const constants_1 = require("../../../../../../../../functions/src/constants");
const constants_2 = require("../constants");
const geoFire = require('geofire');
let DeliveryRequestCommandInfrastructureRepository = class DeliveryRequestCommandInfrastructureRepository {
    constructor(serviceConfigQueryRepository, driverPublicProfileRepo, firebase) {
        this.serviceConfigQueryRepository = serviceConfigQueryRepository;
        this.driverPublicProfileRepo = driverPublicProfileRepo;
        this.firebase = firebase;
    }
    saveRequest(request) {
        const primitiveData = request.toPrimitives();
        this.firebase
            .database()
            .ref(this.getCollectionName(constants_2.DELIVERY_REQUESTS, primitiveData.mode))
            .child(request.id.value)
            .set(utils_1.ObjectUtils.omitUnknown(primitiveData))
            .then(() => {
        })
            .catch(() => {
            console.log('error saving request');
        });
        this.updateRestaurantOrderStatus(request.requestInfo.id, 'Driver Pending').catch((error) => {
            console.log(error.message);
        });
    }
    async getRequest(requestId) {
        const liveRequestSnap = await this.firebase
            .database()
            .ref(this.getCollectionName(constants_2.DELIVERY_REQUESTS, 'live'))
            .child(requestId)
            .once('value');
        const testRequestSnap = await this.firebase
            .database()
            .ref(this.getCollectionName(constants_2.DELIVERY_REQUESTS, 'test'))
            .child(requestId)
            .once('value');
        if (!liveRequestSnap.exists() && !testRequestSnap.exists()) {
            throw new Error('request_not_found');
        }
        const requestData = liveRequestSnap.exists()
            ? liveRequestSnap.val()
            : testRequestSnap.val();
        const serviceConfiguration = await this.serviceConfigQueryRepository.getConfigurations();
        return delivery_request_model_1.default.fromPrimitives({
            ...requestData,
            serviceConfig: serviceConfiguration,
            date: new Date(requestData.date),
            preCandidates: requestData.preCandidates,
        });
    }
    async deleteRequest(request) {
        try {
            await this.firebase
                .database()
                .ref(this.getCollectionName(constants_2.DELIVERY_REQUESTS, request.mode))
                .child(request.id.value)
                .set(null);
        }
        catch (e) {
            console.log('error deleting request');
        }
    }
    async findClosestRiderCandidate(request) {
        const { selected: nearestCandidatesInService } = await (async () => {
            const params = {
                radius: request.currentAttemptRadius,
                origin: request.requestInfo.storeLocation.geoLocation,
                mode: request.mode,
                attemptNumber: request.attemptNumber,
            };
            if (request.preCandidate) {
                return this.getPreCandidateDriver({
                    ...params,
                    id: request.preCandidate,
                }, request.discardedCandidates);
            }
            console.log('Normal mode');
            return this.searchClosestDriversInService({
                ...params,
            }, request.discardedCandidates);
        })();
        if (!nearestCandidatesInService)
            return undefined;
        const publicProfile = await this.driverPublicProfileRepo.findProfileById(nearestCandidatesInService.key);
        if (!publicProfile)
            return undefined;
        return new delivery_request_rider_candidate_model_1.default({
            id: nearestCandidatesInService.key,
            distanceToClient: nearestCandidatesInService.distance,
            publicProfile: publicProfile,
            isBlockedToTakeRequest: true,
            attemptNumber: request.attemptNumber,
            initialCoords: {
                address: '',
                geoLocation: nearestCandidatesInService.location,
            },
        });
    }
    async updateDriverNotification(candidate, mode) {
        await this.firebase
            .database()
            .ref(this.getCollectionName(constants_2.DRIVER_ALERTS, mode))
            .child(candidate.id)
            .transaction((c) => {
            c = candidate.notification
                ? {
                    ...utils_1.ObjectUtils.omitUnknown({
                        ...candidate.notification,
                        countDown: candidate.countDown,
                    }),
                }
                : null;
            return c;
        });
    }
    async updateRequest(request) {
        const changes = {};
        const requestChanges = utils_1.ObjectUtils.omitUnknown(utils_1.ObjectUtils.omitUnknown(utils_1.ObjectUtils.omit(request.toPrimitives(), ['date'])));
        changes[`${this.getCollectionName(constants_2.DELIVERY_REQUESTS, request.mode)}/${request.id.value}`] = requestChanges;
        try {
            await this.firebase.database().ref().update(changes);
        }
        catch (e) { }
        if ([
            'NOT_FOUND_MAX_ATTEMPTS_REACHED',
            'DRIVERS_NOT_FOUND_IN_ATTEMPT',
        ].includes(request.status)) {
            this.updateRestaurantOrderStatus(request.requestInfo.id, 'Driver Not Found').catch((error) => {
                console.log(error.message);
            });
        }
    }
    async toggleLockedCandidates(candidates, mode, isLocked) {
        let filteredCandidates = candidates;
        if (!isLocked) {
            const activeDeliveries = await this.getRidersActiveDeliveries(candidates.map((c) => c.id));
            filteredCandidates = filteredCandidates.filter((c, index) => !activeDeliveries[index]);
        }
        return this.toggleBlockDrivers(filteredCandidates.map((candidate) => ({
            id: candidate.id,
        })), mode, isLocked);
    }
    async getRidersActiveDeliveries(ridersIds) {
        try {
            const deliveries = await Promise.all(ridersIds.map((e) => {
                return new Promise(async (resolve) => {
                    const d = await this.firebase
                        .database()
                        .ref(constants_1.RIDERS_ACTIVE_ORDERS)
                        .get();
                    resolve(d.val());
                });
            }));
            return deliveries;
        }
        catch (e) {
            return [];
        }
    }
    async searchClosestDriversInService({ radius, origin, mode, attemptNumber, }, discardedCandidates) {
        const docKey = constants_2.AVAILABLE_RIDERS;
        const AVAILABLE_DRIVERS_IN_SERVICE_REF = this.firebase
            .database()
            .ref(`${this.getCollectionName(docKey, mode)}`);
        return this.findCandidatesInRadius({
            source: AVAILABLE_DRIVERS_IN_SERVICE_REF,
            radius: radius,
            origin: origin,
            mode: mode,
            attemptNumber: attemptNumber,
            discardedCandidates: discardedCandidates,
        });
    }
    async getPreCandidateDriver({ id, radius, origin, mode, attemptNumber, }, discardedCandidates) {
        const isAvailable = (await this.firebase
            .database()
            .ref(`${this.getCollectionName(constants_2.AVAILABLE_RIDERS, mode)}/${id}`)
            .get()).exists();
        const docKey = isAvailable ? constants_2.AVAILABLE_RIDERS : constants_2.BUSY_RIDERS;
        const AVAILABLE_DRIVERS_IN_SERVICE_REF = this.firebase
            .database()
            .ref(`${this.getCollectionName(docKey, mode)}`);
        return this.findCandidatesInRadius({
            source: AVAILABLE_DRIVERS_IN_SERVICE_REF,
            radius: radius,
            origin: origin,
            mode: mode,
            attemptNumber: attemptNumber,
            riderId: id,
            discardedCandidates: discardedCandidates,
        });
    }
    async findCandidatesInRadius({ riderId, source, origin, mode, radius, discardedCandidates, attemptNumber, }) {
        const geoFireInstance = new geoFire.GeoFire(source);
        let candidatesInRadius = [];
        return new Promise((resolve) => {
            const geoQuery = geoFireInstance.query({
                center: origin,
                radius: radius,
            });
            geoQuery.on('key_entered', async (key, location, distance) => {
                const discartedC = discardedCandidates.find({
                    id: key,
                });
                const canBeChosen = (() => {
                    console.log('Selecting mode');
                    if (riderId) {
                        console.log('Extra delivery for ' + riderId);
                        if (key !== riderId)
                            return false;
                        if (!discartedC)
                            return true;
                        return (discartedC.availableForRetry &&
                            discartedC.attemptNumber !== attemptNumber);
                    }
                    if (!discartedC)
                        return true;
                    return (discartedC.availableForRetry &&
                        discartedC.attemptNumber !== attemptNumber);
                })();
                if (canBeChosen) {
                    candidatesInRadius.push({
                        key,
                        distance: distance,
                        location: location,
                    });
                }
            });
            geoQuery.on('ready', async () => {
                geoQuery.cancel();
                candidatesInRadius = utils_1.ArrayUtils.orderBy(candidatesInRadius, ['distance'], ['asc']);
                const nearest = candidatesInRadius.slice(0, 1);
                const selected = nearest[0];
                if (selected) {
                    this.toggleBlockDrivers([selected].map((c) => ({ ...c, id: c.key })), mode, true);
                }
                resolve({
                    selected: selected ? selected : null,
                });
            });
        });
    }
    async updateRestaurantOrderStatus(orderId, status) {
        return this.firebase
            .firestore()
            .collection(constants_2.RESTAURANT_ORDERS)
            .doc(orderId)
            .update(utils_1.ObjectUtils.omitUnknown({ status: status }));
    }
    toggleBlockDrivers(drivers, mode, blocked) {
        const promises = drivers.map((driver) => {
            return new Promise(async (resolve) => {
                await this.createCandidateBlockedTransaction(driver.id, mode, (blockedDriver) => {
                    blockedDriver = blocked && driver.location ? driver.location : null;
                    return blockedDriver;
                });
                resolve({});
            });
        });
        return Promise.all(promises);
    }
    createCandidateBlockedTransaction(candidateId, mode, update) {
        const BLOCKED_DRIVERS_REF = this.firebase
            .database()
            .ref(`${this.getCollectionName(constants_2.BUSY_RIDERS, mode)}`);
        return BLOCKED_DRIVERS_REF.child(candidateId).transaction(update);
    }
    getCollectionName(name, mode) {
        return `${name}${mode == 'test' ? `_${mode}` : ''}`;
    }
};
DeliveryRequestCommandInfrastructureRepository.bindingKey = 'DeliveryRequestCommandRepository';
DeliveryRequestCommandInfrastructureRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('ServiceConfigurationsQueryRepository')),
    tslib_1.__param(1, (0, decorators_1.inject)('RiderPublicProfileQueryRepository')),
    tslib_1.__param(2, (0, decorators_1.inject)(nestjs_firebase_admin_1.FIREBASE_ADMIN_INJECT)),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], DeliveryRequestCommandInfrastructureRepository);
exports.default = DeliveryRequestCommandInfrastructureRepository;
//# sourceMappingURL=delivery-request-infrastructure-command-repository.js.map