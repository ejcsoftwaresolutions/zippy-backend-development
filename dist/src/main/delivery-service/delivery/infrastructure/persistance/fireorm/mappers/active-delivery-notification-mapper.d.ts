import Delivery from '@deliveryService/delivery/domain/models/delivery-model';
import ActiveDeliveryNotificationEntity from '../entities/active-delivery-notification-entity';
export default class ActiveDeliveryNotificationMapper {
    static toPersistence(delivery: Delivery): ActiveDeliveryNotificationEntity;
}
