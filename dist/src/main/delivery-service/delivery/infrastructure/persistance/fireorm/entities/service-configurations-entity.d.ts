export declare class ServiceConfigurationsEntity {
    id: string;
    searchConfigurations: any;
    feeConfigurations: any;
    globalFees: any;
}
