import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { ServiceConfigurationsEntity } from '../entities/service-configurations-entity';
export default class ServiceConfigurationsQueryInfrastructureRepository extends FireOrmRepository<ServiceConfigurationsEntity> implements ServiceConfigurationsQueryRepository {
    static readonly bindingKey = "ServiceConfigurationsQueryRepository";
    getEntityClass(): typeof ServiceConfigurationsEntity;
    getConfigurations(): Promise<ServiceConfigurations>;
}
