"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceConfigurationsEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let ServiceConfigurationsEntity = class ServiceConfigurationsEntity {
};
ServiceConfigurationsEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('app_configurations')
], ServiceConfigurationsEntity);
exports.ServiceConfigurationsEntity = ServiceConfigurationsEntity;
//# sourceMappingURL=service-configurations-entity.js.map