import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import TestDeliveryRequestsCommandRepository from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';
import Id from '@shared/domain/id/id';
import { FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin';
export default class TestDeliveryRequestCommandInfrastructureRepository implements TestDeliveryRequestsCommandRepository {
    private serviceConfigQueryRepository;
    private firebase;
    static readonly bindingKey = "TestDeliveryRequestsCommandRepository";
    constructor(serviceConfigQueryRepository: ServiceConfigurationsQueryRepository, firebase: FirebaseAdminSDK);
    insertMockCandidatesInRadius(center: {
        latitude: number;
        longitude: number;
    }, radius: number): Promise<any>;
    createTestRider(id: Id, serviceType: any, position: {
        latitude: number;
        longitude: number;
    }): Promise<any>;
    createTestCustomer(id: Id, position: {
        latitude: number;
        longitude: number;
    }): Promise<any>;
    removeTestCustomer(id: Id): Promise<any>;
    removeTestDriver(id: Id, serviceType: string): Promise<any>;
    removeAllCustomers(): Promise<any>;
    removeAllDrivers(): Promise<any>;
}
