import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import DeliveryRequestRiderCandidate from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import RiderPublicProfileQueryRepository from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import Collection from '@shared/domain/value-object/collection';
import { FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin';
export default class DeliveryRequestCommandInfrastructureRepository implements DeliveryRequestCommandRepository {
    private serviceConfigQueryRepository;
    private driverPublicProfileRepo;
    private firebase;
    static readonly bindingKey = "DeliveryRequestCommandRepository";
    constructor(serviceConfigQueryRepository: ServiceConfigurationsQueryRepository, driverPublicProfileRepo: RiderPublicProfileQueryRepository, firebase: FirebaseAdminSDK);
    saveRequest(request: DeliveryRequest): void;
    getRequest(requestId: string): Promise<DeliveryRequest>;
    deleteRequest(request: DeliveryRequest): Promise<void>;
    findClosestRiderCandidate(request: DeliveryRequest): Promise<DeliveryRequestRiderCandidate>;
    updateDriverNotification(candidate: DeliveryRequestRiderCandidate, mode: string): Promise<void>;
    updateRequest(request: DeliveryRequest): Promise<void>;
    toggleLockedCandidates(candidates: Collection<DeliveryRequestRiderCandidate>, mode: string, isLocked: boolean): Promise<unknown[]>;
    private getRidersActiveDeliveries;
    private searchClosestDriversInService;
    private getPreCandidateDriver;
    private findCandidatesInRadius;
    private updateRestaurantOrderStatus;
    private toggleBlockDrivers;
    private createCandidateBlockedTransaction;
    private getCollectionName;
}
