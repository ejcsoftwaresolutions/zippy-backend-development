"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const service_configurations_model_1 = require("../../../../domain/models/service-configurations-model");
const service_configurations_query_repository_1 = require("../../../../domain/repositories/service-configurations-query-repository");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const service_configurations_entity_1 = require("../entities/service-configurations-entity");
const service_configurations_mapper_1 = require("../mappers/service-configurations-mapper");
const CONFIG_ID = 'RIDER_DELIVERY_CONFIGURATIONS';
class ServiceConfigurationsQueryInfrastructureRepository extends fireorm_repository_1.default {
    getEntityClass() {
        return service_configurations_entity_1.ServiceConfigurationsEntity;
    }
    async getConfigurations() {
        const config = await this.repository().findById(CONFIG_ID);
        if (!config) {
            throw new Error('App setting not configured, run fixtures first!');
        }
        return service_configurations_mapper_1.default.toDomain(config);
    }
}
exports.default = ServiceConfigurationsQueryInfrastructureRepository;
ServiceConfigurationsQueryInfrastructureRepository.bindingKey = 'ServiceConfigurationsQueryRepository';
//# sourceMappingURL=service-configurations-infrastructure-query-repository.js.map