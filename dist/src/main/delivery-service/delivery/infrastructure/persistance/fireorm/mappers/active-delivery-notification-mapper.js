"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const delivery_model_1 = require("../../../../domain/models/delivery-model");
const utils_1 = require("../../../../../../shared/domain/utils");
class ActiveDeliveryNotificationMapper {
    static toPersistence(delivery) {
        const primitives = delivery.toPrimitives();
        return {
            id: primitives.id,
            finalFee: primitives.finalFee,
            customer: utils_1.ObjectUtils.omitUnknown(primitives.customer),
            acceptedDriver: utils_1.ObjectUtils.omitUnknown(primitives.rider),
            tripDistance: primitives.distance,
            tripDuration: primitives.duration,
            status: primitives.status,
            requestInfo: primitives.orderDetails,
        };
    }
}
exports.default = ActiveDeliveryNotificationMapper;
//# sourceMappingURL=active-delivery-notification-mapper.js.map