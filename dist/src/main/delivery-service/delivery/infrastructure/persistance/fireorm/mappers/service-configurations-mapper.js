"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const service_configurations_model_1 = require("../../../../domain/models/service-configurations-model");
class ServiceConfigurationsMapper {
    static toDomain(plainValues) {
        return service_configurations_model_1.default.fromPrimitives({
            feeConfigurations: plainValues.feeConfigurations,
            searchConfigurations: {
                acceptanceTimeout: plainValues.searchConfigurations.acceptanceTimeout,
                maxAttempts: plainValues.searchConfigurations.maxAttempts,
                attemptMaxCandidates: plainValues.searchConfigurations.attemptMaxCandidates,
                attemptSearchRadius: plainValues.searchConfigurations.attemptSearchRadius,
            },
        });
    }
}
exports.default = ServiceConfigurationsMapper;
//# sourceMappingURL=service-configurations-mapper.js.map