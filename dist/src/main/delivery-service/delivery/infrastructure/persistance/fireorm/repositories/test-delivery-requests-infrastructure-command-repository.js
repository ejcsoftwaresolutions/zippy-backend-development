"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_configurations_query_repository_1 = require("../../../../domain/repositories/service-configurations-query-repository");
const test_delivery_requests_command_repository_1 = require("../../../../domain/repositories/test-delivery-requests-command-repository");
const decorators_1 = require("../../../../../../shared/domain/decorators");
const id_1 = require("../../../../../../shared/domain/id/id");
const nestjs_firebase_admin_1 = require("@tfarras/nestjs-firebase-admin");
const constants_1 = require("../constants");
const geoFire = require('geofire');
const randomLocation = require('random-location');
let TestDeliveryRequestCommandInfrastructureRepository = class TestDeliveryRequestCommandInfrastructureRepository {
    constructor(serviceConfigQueryRepository, firebase) {
        this.serviceConfigQueryRepository = serviceConfigQueryRepository;
        this.firebase = firebase;
    }
    async insertMockCandidatesInRadius(center, radius) {
        const P = center;
        const R = radius;
        const drivers = [...Array(5)].map((mockDriver) => {
            const randomPoint = randomLocation.randomCirclePoint(P, R);
            const id = new id_1.default().value;
            return {
                location: [randomPoint.latitude, randomPoint.longitude],
                id: id,
            };
        });
        const plainCandidates = drivers;
        const promises = plainCandidates.map((candidate) => {
            const ref = this.firebase.database().ref(`${constants_1.RIDERS_LOCATIONS}_test`);
            const geoFireInstance = new geoFire.GeoFire(ref);
            return geoFireInstance.set(candidate.id, candidate.location);
        });
        await Promise.all(promises);
        return plainCandidates;
    }
    async createTestRider(id, serviceType, position) {
        const ref = this.firebase.database().ref(`${constants_1.RIDERS_LOCATIONS}_test`);
        const geoFireInstance = new geoFire.GeoFire(ref);
        await geoFireInstance.set(id.value, [
            position.latitude,
            position.longitude,
        ]);
    }
    async createTestCustomer(id, position) {
        const ref = this.firebase.database().ref(`${constants_1.TEST_CUSTOMERS}`);
        const geoFireInstance = new geoFire.GeoFire(ref);
        await geoFireInstance.set(id.value, [
            position.latitude,
            position.longitude,
        ]);
    }
    async removeTestCustomer(id) {
        await this.firebase
            .database()
            .ref(`${constants_1.TEST_CUSTOMERS}/${id.value}`)
            .remove();
    }
    async removeTestDriver(id, serviceType) {
        await this.firebase
            .database()
            .ref(`${constants_1.RIDERS_LOCATIONS}_test/${serviceType
            .replace(/\./g, '_')
            .toUpperCase()}/${id.value}`)
            .remove();
    }
    async removeAllCustomers() {
        await this.firebase.database().ref(`${constants_1.TEST_CUSTOMERS}`).remove();
    }
    async removeAllDrivers() {
        await this.firebase.database().ref(`${constants_1.RIDERS_LOCATIONS}_test`).remove();
    }
};
TestDeliveryRequestCommandInfrastructureRepository.bindingKey = 'TestDeliveryRequestsCommandRepository';
TestDeliveryRequestCommandInfrastructureRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('ServiceConfigurationsQueryRepository')),
    tslib_1.__param(1, (0, decorators_1.inject)(nestjs_firebase_admin_1.FIREBASE_ADMIN_INJECT)),
    tslib_1.__metadata("design:paramtypes", [Object, Object])
], TestDeliveryRequestCommandInfrastructureRepository);
exports.default = TestDeliveryRequestCommandInfrastructureRepository;
//# sourceMappingURL=test-delivery-requests-infrastructure-command-repository.js.map