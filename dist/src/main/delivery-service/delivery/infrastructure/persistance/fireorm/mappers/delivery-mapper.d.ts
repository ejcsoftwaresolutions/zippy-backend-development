import CustomerPublicProfile from '@deliveryService/customer/domain/models/customer-public-profile-model';
import Delivery from '@deliveryService/delivery/domain/models/delivery-model';
import ServiceConfigurations from '@deliveryService/delivery/domain/models/service-configurations-model';
import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';
import { DeliveryEntity } from '../entities/delivery-entity';
export default class DeliveryMapper {
    static toDomain(plainValues: DeliveryEntity & {
        riderPublicProfile: RiderPublicProfile;
        customerPublicProfile: CustomerPublicProfile;
        serviceConfig: ServiceConfigurations;
    }): Delivery;
    static toPersistence(delivery: Delivery): DeliveryEntity;
}
