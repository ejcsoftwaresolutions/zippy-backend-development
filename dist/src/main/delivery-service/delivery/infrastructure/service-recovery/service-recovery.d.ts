export default class ServiceRecoveryService {
    constructor();
    onModuleInit(): void;
    execute(): Promise<void>;
}
