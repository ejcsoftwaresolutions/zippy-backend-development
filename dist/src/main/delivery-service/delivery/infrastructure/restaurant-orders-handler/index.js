"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../../../shared/domain/decorators");
const moment = require('moment-timezone');
let RestaurantOrdersHandler = class RestaurantOrdersHandler {
    constructor() { }
    onModuleInit() {
        this.execute();
    }
    async execute() {
        if (process.env.NODE_ENV !== 'development')
            return;
    }
};
RestaurantOrdersHandler = tslib_1.__decorate([
    (0, decorators_1.injectable)(),
    tslib_1.__metadata("design:paramtypes", [])
], RestaurantOrdersHandler);
exports.default = RestaurantOrdersHandler;
//# sourceMappingURL=index.js.map