export default class RestaurantOrdersHandler {
    constructor();
    onModuleInit(): void;
    execute(): Promise<void>;
}
