export default class AvailableRidersHandler {
    constructor();
    onModuleInit(): void;
    execute(): Promise<void>;
}
