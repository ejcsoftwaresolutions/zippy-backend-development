"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const interval_timer_1 = require("../../../../shared/infrastructure/utils/interval-timer");
let IntervalTimerRequestAttemptCountdownTimer = class IntervalTimerRequestAttemptCountdownTimer {
    constructor(deliveryRequestTimers) {
        this.deliveryRequestTimers = deliveryRequestTimers;
    }
    initialize(requestId, startAtSecond) {
        const options = {
            startTime: startAtSecond * 1000,
            updateFrequency: 1000,
            countdown: true,
        };
        this.deliveryRequestTimers[requestId] = {
            timer: new interval_timer_1.Timer(options),
            interrupted: false,
        };
        return this.deliveryRequestTimers[requestId];
    }
    get(requestId) {
        return this.deliveryRequestTimers[requestId];
    }
    exists(requestId) {
        return !!this.deliveryRequestTimers[requestId];
    }
    destroy(requestId) {
        if (!this.exists(requestId))
            throw new Error('timer_not_initialized');
        this.deliveryRequestTimers[requestId].timer.destroy();
        delete this.deliveryRequestTimers[requestId];
    }
    toggleInterrupted(requestId, interrupted) {
        if (!this.exists(requestId))
            throw new Error('timer_not_initialized');
        this.deliveryRequestTimers[requestId].interrupted = interrupted;
    }
    start(requestId, candidateId) {
        if (!this.exists(requestId))
            throw new Error('timer_not_initialized');
        this.deliveryRequestTimers[requestId].candidateId = candidateId;
        this.deliveryRequestTimers[requestId].interrupted = false;
        this.deliveryRequestTimers[requestId].timer.start();
    }
    stop(requestId) {
        if (!this.exists(requestId))
            throw new Error('timer_not_initialized');
        this.deliveryRequestTimers[requestId].interrupted = false;
        this.deliveryRequestTimers[requestId].timer.stop();
    }
    reset(requestId) {
        if (!this.exists(requestId))
            throw new Error('timer_not_initialized');
        this.deliveryRequestTimers[requestId].interrupted = false;
        this.deliveryRequestTimers[requestId].candidateId = undefined;
        this.deliveryRequestTimers[requestId].timer.reset();
    }
    onTimeout(requestId, callback) {
        if (!this.exists(requestId))
            throw new Error('timer_not_initialized');
        this.deliveryRequestTimers[requestId].timer.addEventListener('end', () => {
            callback();
        });
    }
    onUpdate(requestId, callback) {
        if (!this.exists(requestId))
            throw new Error('timer_not_initialized');
        this.deliveryRequestTimers[requestId].timer.addEventListener('update', (rs) => {
            callback(rs.getTime.seconds);
        });
    }
};
IntervalTimerRequestAttemptCountdownTimer = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('delivery.request.timers')),
    tslib_1.__metadata("design:paramtypes", [Object])
], IntervalTimerRequestAttemptCountdownTimer);
exports.default = IntervalTimerRequestAttemptCountdownTimer;
//# sourceMappingURL=intervaltimer-request-attempt-countdown-timer.js.map