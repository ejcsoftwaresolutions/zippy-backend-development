import { Timer } from '@shared/infrastructure/utils/interval-timer';
import RequestAttemptCountdownTimer from '../../domain/contracts/request-attempt-countdown-timer';
export default class IntervalTimerRequestAttemptCountdownTimer implements RequestAttemptCountdownTimer {
    private deliveryRequestTimers;
    constructor(deliveryRequestTimers: {
        [requestId: string]: {
            candidateId?: string;
            timer: any;
            interrupted: boolean;
        };
    });
    initialize(requestId: string, startAtSecond: number): {
        candidateId?: string;
        timer: Timer;
    };
    get(requestId: string): {
        candidateId?: string;
        timer: Timer;
        interrupted: boolean;
    };
    exists(requestId: string): boolean;
    destroy(requestId: string): void;
    toggleInterrupted(requestId: string, interrupted: boolean): void;
    start(requestId: string, candidateId: string): void;
    stop(requestId: string): void;
    reset(requestId: string): void;
    onTimeout(requestId: string, callback: Function): void;
    onUpdate(requestId: string, callback: Function): void;
}
