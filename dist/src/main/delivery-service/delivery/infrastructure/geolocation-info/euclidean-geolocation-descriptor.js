"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_1 = require("../../../../shared/domain/decorators/service");
let EuclideanGeolocationDescriptor = class EuclideanGeolocationDescriptor {
    constructor() { }
    async basicInfo(origin, destination) {
        return new Promise((resolve, reject) => {
            const originCoord = origin.split(',');
            const destCoord = destination.split(',');
            const distance = distanceRadius(parseFloat(originCoord[0]), parseFloat(originCoord[1]), parseFloat(destCoord[0]), parseFloat(destCoord[1]));
            resolve({
                distance: distance + ' km',
                distanceValue: parseFloat((distance / 1000).toFixed(2)),
                duration: '0 min',
                durationValue: 0,
            });
        });
    }
};
EuclideanGeolocationDescriptor = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__metadata("design:paramtypes", [])
], EuclideanGeolocationDescriptor);
exports.default = EuclideanGeolocationDescriptor;
const distanceRadius = (lat1, lon1, lat2, lon2) => {
    if (lat1 === lat2 && lon1 === lon2) {
        return 0;
    }
    else {
        const radlat1 = (Math.PI * lat1) / 180;
        const radlat2 = (Math.PI * lat2) / 180;
        const theta = lon1 - lon2;
        const radtheta = (Math.PI * theta) / 180;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) +
            Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = (dist * 180) / Math.PI;
        dist = dist * 60 * 1.1515;
        return dist;
    }
};
//# sourceMappingURL=euclidean-geolocation-descriptor.js.map