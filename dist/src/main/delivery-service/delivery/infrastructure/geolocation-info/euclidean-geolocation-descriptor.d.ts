import TripGeolocationDescriptor, { TripGeoInformation } from '../../domain/contracts/trip-geolocation-descriptor';
export default class EuclideanGeolocationDescriptor implements TripGeolocationDescriptor {
    constructor();
    basicInfo(origin: string, destination: string): Promise<TripGeoInformation>;
}
