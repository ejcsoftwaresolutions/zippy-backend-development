"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_1 = require("../../../../shared/domain/decorators/service");
const distance = require('google-distance');
let GoogleGeolocationDescriptor = class GoogleGeolocationDescriptor {
    constructor() {
        if (!process.env.GOOGLE_API_KEY) {
            throw new Error('Not google API set in env');
        }
        distance.apiKey = process.env.GOOGLE_API_KEY;
    }
    async basicInfo(origin, destination) {
        return new Promise((resolve, reject) => {
            distance.get({
                origin: origin,
                destination: destination,
                language: 'es',
            }, (err, data) => {
                if (err) {
                    console.log(err);
                    return resolve(undefined);
                }
                return resolve({
                    originAddress: data.origin,
                    destinationAddress: data.destination,
                    distance: data.distance,
                    distanceValue: parseFloat((data.distanceValue / 1000).toFixed(2)),
                    duration: data.duration,
                    durationValue: parseFloat((data.durationValue / 60).toFixed(2)),
                });
            });
        });
    }
};
GoogleGeolocationDescriptor = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__metadata("design:paramtypes", [])
], GoogleGeolocationDescriptor);
exports.default = GoogleGeolocationDescriptor;
//# sourceMappingURL=google-geolocation-descriptor.js.map