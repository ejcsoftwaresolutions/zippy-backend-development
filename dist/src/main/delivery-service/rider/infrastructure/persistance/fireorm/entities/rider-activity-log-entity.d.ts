export declare class RiderActivityLogEntity {
    id: string;
    driverId: string;
    vendorId: string;
    vendorOrderId: string;
    date: Date;
    event: string;
    fee: number;
}
