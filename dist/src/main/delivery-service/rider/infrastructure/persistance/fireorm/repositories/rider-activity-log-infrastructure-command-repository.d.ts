import RiderActivityLogCommandRepository from '@deliveryService/rider/domain/repositories/rider-activity-log-command-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { RiderActivityLogEntity } from '../entities/rider-activity-log-entity';
export default class RiderActivityLogCommandInfrastructureRepository extends FireOrmRepository<RiderActivityLogEntity> implements RiderActivityLogCommandRepository {
    static readonly bindingKey = "RiderActivityLogCommandRepository";
    getEntityClass(): EntityConstructorOrPath<RiderActivityLogEntity>;
    log(event: {
        riderId: string;
        vendorId: string;
        vendorOrderId: string;
        date: Date;
        event: string;
        fee: number;
    }): Promise<void>;
}
