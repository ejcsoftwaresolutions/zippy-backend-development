"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rider_public_profile_model_1 = require("../../../../domain/models/rider-public-profile-model");
const rider_public_profile_query_repository_1 = require("../../../../domain/repositories/rider-public-profile-query-repository");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const rider_public_profile_entity_1 = require("../entities/rider-public-profile-entity");
const rider_public_profile_mapper_1 = require("../mappers/rider-public-profile-mapper");
const firebase_admin_1 = require("firebase-admin");
const constants_1 = require("../../../../../delivery/infrastructure/persistance/fireorm/constants");
class RiderPublicProfileQueryInfrastructureRepository extends fireorm_repository_1.default {
    getEntityClass() {
        return rider_public_profile_entity_1.RiderPublicProfileEntity;
    }
    async findProfileById(id) {
        var _a;
        const userAccount = await this.repository()
            .whereEqualTo('id', id)
            .findOne();
        if (!userAccount)
            return null;
        const profile = (await firebase_admin_1.default.firestore().collection(constants_1.RIDERS).doc(userAccount.id).get()).data();
        if (!profile)
            return null;
        const pushTokens = (await firebase_admin_1.default
            .firestore()
            .collection(constants_1.PUSH_TOKENS)
            .doc(userAccount.id)
            .get()).data();
        return rider_public_profile_mapper_1.default.toDomain({
            id: userAccount.id,
            email: userAccount.email,
            firstName: userAccount.firstName,
            lastName: userAccount.lastName,
            phone: userAccount.phone,
            identificationCard: profile.identificationCard,
            deliveryMethod: profile.deliveryMethod,
            profilePictureUrl: profile.profilePictureURL,
            reviewsCount: profile.reviewsCount,
            reviewsSum: profile.reviewsSum,
            pushToken: pushTokens === null || pushTokens === void 0 ? void 0 : pushTokens.rider,
            configurations: (_a = profile.configurations) !== null && _a !== void 0 ? _a : {
                receiveEmails: false,
                receiveSMS: false,
                receivePushNotifications: false,
                referralPromotions: false,
            },
        });
    }
}
exports.default = RiderPublicProfileQueryInfrastructureRepository;
RiderPublicProfileQueryInfrastructureRepository.bindingKey = 'RiderPublicProfileQueryRepository';
//# sourceMappingURL=rider-public-profile-infrastructure-query-repository.js.map