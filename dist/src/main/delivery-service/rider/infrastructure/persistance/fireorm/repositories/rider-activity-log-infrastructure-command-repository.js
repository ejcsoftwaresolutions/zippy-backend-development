"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rider_activity_log_command_repository_1 = require("../../../../domain/repositories/rider-activity-log-command-repository");
const id_1 = require("../../../../../../shared/domain/id/id");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const rider_activity_log_entity_1 = require("../entities/rider-activity-log-entity");
class RiderActivityLogCommandInfrastructureRepository extends fireorm_repository_1.default {
    getEntityClass() {
        return rider_activity_log_entity_1.RiderActivityLogEntity;
    }
    async log(event) {
        const entity = new rider_activity_log_entity_1.RiderActivityLogEntity();
        entity.id = new id_1.default().value;
        entity.driverId = event.riderId;
        entity.vendorId = event.vendorId;
        entity.vendorOrderId = event.vendorOrderId;
        entity.date = event.date;
        entity.event = event.event;
        entity.fee = event.fee;
        await this.repository().create(utils_1.ObjectUtils.omitUnknown(entity));
    }
}
exports.default = RiderActivityLogCommandInfrastructureRepository;
RiderActivityLogCommandInfrastructureRepository.bindingKey = 'RiderActivityLogCommandRepository';
//# sourceMappingURL=rider-activity-log-infrastructure-command-repository.js.map