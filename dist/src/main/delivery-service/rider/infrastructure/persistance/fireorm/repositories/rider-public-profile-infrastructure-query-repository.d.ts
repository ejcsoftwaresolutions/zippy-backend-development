import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';
import RiderPublicProfileQueryRepository from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { EntityConstructorOrPath } from 'fireorm';
import { RiderPublicProfileEntity } from '../entities/rider-public-profile-entity';
export default class RiderPublicProfileQueryInfrastructureRepository extends FireOrmRepository<RiderPublicProfileEntity> implements RiderPublicProfileQueryRepository {
    static readonly bindingKey = "RiderPublicProfileQueryRepository";
    getEntityClass(): EntityConstructorOrPath<RiderPublicProfileEntity>;
    findProfileById(id: string): Promise<RiderPublicProfile | null>;
}
