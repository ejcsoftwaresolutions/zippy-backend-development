export declare class RiderPublicProfileEntity {
    id: string;
    firstName?: string;
    lastName?: string;
    email: string;
    phone?: string;
}
