"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderPublicProfileEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let RiderPublicProfileEntity = class RiderPublicProfileEntity {
};
RiderPublicProfileEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('users')
], RiderPublicProfileEntity);
exports.RiderPublicProfileEntity = RiderPublicProfileEntity;
//# sourceMappingURL=rider-public-profile-entity.js.map