import RiderPublicProfile from '@deliveryService/rider/domain/models/rider-public-profile-model';
export default class RiderPublicProfileMapper {
    static toDomain(plainValues: {
        id: string;
        firstName?: string;
        lastName?: string;
        email: string;
        phone?: string;
        deliveryMethod: string;
        pushToken: string;
        identificationCard: string;
        profilePictureUrl: string;
        reviewsSum: number;
        reviewsCount: number;
        configurations: {
            referralPromotions: boolean;
            receiveEmails: boolean;
            receivePushNotifications: boolean;
            receiveSMS: boolean;
        };
    }): RiderPublicProfile;
    static getRiderRating(item: any): number;
    static toPersistence(user: RiderPublicProfile): {
        id: string;
        firstName?: string;
        lastName?: string;
        email: string;
        phone?: string;
        rating?: number;
    };
}
