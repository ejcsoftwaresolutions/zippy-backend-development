"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rider_public_profile_model_1 = require("../../../../domain/models/rider-public-profile-model");
class RiderPublicProfileMapper {
    static toDomain(plainValues) {
        return rider_public_profile_model_1.default.fromPrimitives({
            id: plainValues.id,
            email: plainValues.email,
            firstName: plainValues.firstName,
            lastName: plainValues.lastName,
            phone: plainValues.phone,
            profilePictureUrl: plainValues.profilePictureUrl,
            rating: RiderPublicProfileMapper.getRiderRating(plainValues),
            deliveryMethod: plainValues.deliveryMethod,
            pushToken: plainValues.pushToken,
            identificationCard: plainValues.identificationCard,
            configurations: plainValues.configurations,
        });
    }
    static getRiderRating(item) {
        let reviewAvg = item.reviewsCount === undefined
            ? 0
            : Math.round(item.reviewsSum / item.reviewsCount);
        reviewAvg = Number(Math.round((reviewAvg + 'e' + 2)) + 'e-' + 2);
        if (Number.isNaN(reviewAvg)) {
            reviewAvg = Number(0);
        }
        return reviewAvg;
    }
    static toPersistence(user) {
        return user.toPrimitives();
    }
}
exports.default = RiderPublicProfileMapper;
//# sourceMappingURL=rider-public-profile-mapper.js.map