"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderActivityLogEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let RiderActivityLogEntity = class RiderActivityLogEntity {
};
RiderActivityLogEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('driver_delivery_activity_logs')
], RiderActivityLogEntity);
exports.RiderActivityLogEntity = RiderActivityLogEntity;
//# sourceMappingURL=rider-activity-log-entity.js.map