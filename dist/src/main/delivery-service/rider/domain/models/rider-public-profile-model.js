"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const utils_1 = require("../../../../shared/domain/utils");
const DefaultProps = {
    rating: 5,
};
class RiderPublicProfile extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.email;
    }
    get rating() {
        return this.props.rating;
    }
    get firstName() {
        return this.props.firstName;
    }
    get lastName() {
        return this.props.lastName;
    }
    get phone() {
        return this.props.phone;
    }
    get profilePictureUrl() {
        return this.props.profilePictureUrl;
    }
    get deliveryMethod() {
        return this.props.deliveryMethod;
    }
    get pushToken() {
        return this.props.pushToken;
    }
    get createdAt() {
        return this.props.createdAt;
    }
    get identificationCard() {
        return this.props.identificationCard;
    }
    get wantsToReceivePushNotifications() {
        return this.props.configurations.receivePushNotifications;
    }
    static create(data) {
        return new RiderPublicProfile(data);
    }
    static fromPrimitives({ ...plainData }) {
        return new RiderPublicProfile(plainData);
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
    updateRating(newRate) {
        var _a;
        this.props.rating = Math.round((((_a = this.props.rating) !== null && _a !== void 0 ? _a : 0) + newRate) / 2);
    }
}
exports.default = RiderPublicProfile;
//# sourceMappingURL=rider-public-profile-model.js.map