import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
export interface RiderPublicProfileProps {
    id: string;
    email: string;
    rating?: number;
    firstName?: string;
    lastName?: string;
    phone?: string;
    profilePictureUrl?: string;
    deliveryMethod: string;
    createdAt?: Date;
    pushToken?: string;
    identificationCard?: string;
    configurations: {
        referralPromotions: boolean;
        receiveEmails: boolean;
        receivePushNotifications: boolean;
        receiveSMS: boolean;
    };
}
export type DriverPublicProfilePrimitiveProps = RiderPublicProfileProps;
export default class RiderPublicProfile extends AggregateRoot<RiderPublicProfileProps> {
    constructor(props: RiderPublicProfileProps);
    get id(): string;
    get email(): string;
    get rating(): number;
    get firstName(): string;
    get lastName(): string;
    get phone(): string;
    get profilePictureUrl(): string;
    get deliveryMethod(): string;
    get pushToken(): string;
    get createdAt(): Date;
    get identificationCard(): string;
    get wantsToReceivePushNotifications(): boolean;
    static create(data: RiderPublicProfileProps): RiderPublicProfile;
    static fromPrimitives({ ...plainData }: DriverPublicProfilePrimitiveProps): RiderPublicProfile;
    toPrimitives(): DriverPublicProfilePrimitiveProps;
    updateRating(newRate: number): void;
}
