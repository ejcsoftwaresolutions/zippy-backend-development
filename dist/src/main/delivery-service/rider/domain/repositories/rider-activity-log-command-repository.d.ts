export default interface RiderActivityLogCommandRepository {
    log(event: {
        riderId: string;
        vendorId: string;
        vendorOrderId: string;
        date: Date;
        event: string;
        fee: number;
    }): void;
}
