import RiderPublicProfile from '../models/rider-public-profile-model';
export default interface RiderPublicProfileQueryRepository {
    findProfileById(id: string): Promise<RiderPublicProfile | null>;
}
