"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'RIDER_REJECTED';
class RiderRejectedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = RiderRejectedDomainEvent;
RiderRejectedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=rider-rejected-domain-event.js.map