import DomainEvent, { DomainEventProps } from '@shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {
        riderId: string;
        vendorOrderId: string;
        vendorId: string;
        driverFee: number;
    };
}
export default class RiderAcceptedDomainEvent extends DomainEvent<EventProps> {
    static eventName: string;
    eventName(): string;
}
export {};
