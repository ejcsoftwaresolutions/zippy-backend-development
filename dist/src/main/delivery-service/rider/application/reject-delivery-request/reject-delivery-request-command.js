"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class RejectDeliveryRequestCommand extends command_1.default {
    constructor(driverId, requestId) {
        super();
        this.driverId = driverId;
        this.requestId = requestId;
    }
    name() {
        return RejectDeliveryRequestCommand.name;
    }
}
exports.default = RejectDeliveryRequestCommand;
//# sourceMappingURL=reject-delivery-request-command.js.map