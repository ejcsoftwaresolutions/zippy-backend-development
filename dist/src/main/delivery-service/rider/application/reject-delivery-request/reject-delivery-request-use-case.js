"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const request_attempt_countdown_timer_1 = require("../../../delivery/domain/contracts/request-attempt-countdown-timer");
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const rider_rejected_domain_event_1 = require("../../domain/events/rider-rejected-domain-event");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
let RejectDeliveryRequestUseCase = class RejectDeliveryRequestUseCase {
    constructor(requestTimerService, eventBus) {
        this.requestTimerService = requestTimerService;
        this.eventBus = eventBus;
    }
    async execute(request, driverId) {
        this.requestTimerService.toggleInterrupted(request.id.value, true);
        this.eventBus.publish([
            new rider_rejected_domain_event_1.default({
                occurredOn: new Date(),
                aggregateId: driverId,
                eventData: {
                    riderId: driverId,
                    vendorId: request.requestInfo.storeId,
                    vendorOrderId: request.requestInfo.id,
                    driverFee: request.finalFee.amount,
                },
            }),
        ]);
    }
};
RejectDeliveryRequestUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('services.request.attempt.countdown.timer')),
    tslib_1.__param(1, (0, decorators_1.inject)('event.bus')),
    tslib_1.__metadata("design:paramtypes", [Object, Object])
], RejectDeliveryRequestUseCase);
exports.default = RejectDeliveryRequestUseCase;
//# sourceMappingURL=reject-delivery-request-use-case.js.map