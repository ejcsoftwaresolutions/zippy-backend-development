import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import RejectDeliveryRequestCommand from './reject-delivery-request-command';
import RejectDeliveryRequestUseCase from './reject-delivery-request-use-case';
export default class RejectDeliveryRequestCommandHandler implements CommandHandler {
    private useCase;
    private deliveryRequestRepository;
    constructor(useCase: RejectDeliveryRequestUseCase, deliveryRequestRepository: DeliveryRequestCommandRepository);
    getCommandName(): string;
    handle(command: RejectDeliveryRequestCommand): Promise<void>;
}
