import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import EventBus from '@shared/domain/bus/event/event-bus';
export default class RejectDeliveryRequestUseCase {
    private requestTimerService;
    private eventBus;
    constructor(requestTimerService: RequestAttemptCountdownTimer, eventBus: EventBus);
    execute(request: DeliveryRequest, driverId: string): Promise<void>;
}
