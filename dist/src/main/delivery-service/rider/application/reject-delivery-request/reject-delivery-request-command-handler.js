"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const reject_delivery_request_command_1 = require("./reject-delivery-request-command");
const reject_delivery_request_use_case_1 = require("./reject-delivery-request-use-case");
let RejectDeliveryRequestCommandHandler = class RejectDeliveryRequestCommandHandler {
    constructor(useCase, deliveryRequestRepository) {
        this.useCase = useCase;
        this.deliveryRequestRepository = deliveryRequestRepository;
    }
    getCommandName() {
        return reject_delivery_request_command_1.default.name;
    }
    async handle(command) {
        const request = await this.deliveryRequestRepository.getRequest(command.requestId);
        if (request.currentAlertedCandidate.id !== command.driverId) {
            return;
        }
        await this.useCase.execute(request, command.driverId);
    }
};
RejectDeliveryRequestCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('RejectDeliveryRequestUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [reject_delivery_request_use_case_1.default, Object])
], RejectDeliveryRequestCommandHandler);
exports.default = RejectDeliveryRequestCommandHandler;
//# sourceMappingURL=reject-delivery-request-command-handler.js.map