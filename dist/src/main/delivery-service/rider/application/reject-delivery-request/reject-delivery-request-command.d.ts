import Command from '@shared/domain/bus/command/command';
export default class RejectDeliveryRequestCommand extends Command {
    readonly driverId: string;
    readonly requestId: string;
    constructor(driverId: string, requestId: string);
    name(): string;
}
