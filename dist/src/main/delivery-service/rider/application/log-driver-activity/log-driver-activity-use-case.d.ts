import RiderActivityLogCommandRepository from '@deliveryService/rider/domain/repositories/rider-activity-log-command-repository';
export default class LogDriverActivityUseCase {
    private driverLogRepo;
    constructor(driverLogRepo: RiderActivityLogCommandRepository);
    execute(data: {
        riderId: string;
        vendorOrderId: string;
        vendorId: string;
        date: Date;
        event: 'ORDER_REJECTED' | 'ORDER_ACCEPTED' | 'ORDER_COMPLETED';
        fee: number;
    }): Promise<void>;
}
