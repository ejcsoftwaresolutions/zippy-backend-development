import RiderAcceptedDomainEvent from '@deliveryService/rider/domain/events/rider-accepted-domain-event';
import RiderRejectedDomainEvent from '@deliveryService/rider/domain/events/rider-rejected-domain-event';
import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import LogDriverActivityUseCase from './log-driver-activity-use-case';
export default class LogDriverActivityEventSubscriber implements EventSubscriber {
    private useCase;
    constructor(useCase: LogDriverActivityUseCase);
    subscribedTo(): Map<string, Function>;
    handleAccepted(event: RiderAcceptedDomainEvent): Promise<void>;
    handleRejected(event: RiderRejectedDomainEvent): Promise<void>;
}
