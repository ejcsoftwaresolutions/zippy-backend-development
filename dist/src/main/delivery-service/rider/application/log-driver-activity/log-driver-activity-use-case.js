"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const rider_activity_log_command_repository_1 = require("../../domain/repositories/rider-activity-log-command-repository");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
let LogDriverActivityUseCase = class LogDriverActivityUseCase {
    constructor(driverLogRepo) {
        this.driverLogRepo = driverLogRepo;
    }
    async execute(data) {
        this.driverLogRepo.log({
            riderId: data.riderId,
            vendorId: data.vendorId,
            vendorOrderId: data.vendorOrderId,
            date: data.date,
            event: data.event,
            fee: data.fee,
        });
    }
};
LogDriverActivityUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('RiderActivityLogCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [Object])
], LogDriverActivityUseCase);
exports.default = LogDriverActivityUseCase;
//# sourceMappingURL=log-driver-activity-use-case.js.map