"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const rider_accepted_domain_event_1 = require("../../domain/events/rider-accepted-domain-event");
const rider_rejected_domain_event_1 = require("../../domain/events/rider-rejected-domain-event");
const event_subscriber_1 = require("../../../../shared/domain/bus/event/event-subscriber");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const log_driver_activity_use_case_1 = require("./log-driver-activity-use-case");
let LogDriverActivityEventSubscriber = class LogDriverActivityEventSubscriber {
    constructor(useCase) {
        this.useCase = useCase;
    }
    subscribedTo() {
        const events = new Map();
        events.set(rider_accepted_domain_event_1.default.eventName, this.handleAccepted.bind(this));
        events.set(rider_rejected_domain_event_1.default.eventName, this.handleRejected.bind(this));
        return events;
    }
    async handleAccepted(event) {
        this.useCase.execute({
            ...event.props.eventData,
            date: event.occurredOn,
            event: 'ORDER_ACCEPTED',
            fee: event.props.eventData.driverFee,
        });
    }
    async handleRejected(event) {
        this.useCase.execute({
            ...event.props.eventData,
            date: event.occurredOn,
            event: 'ORDER_REJECTED',
            fee: event.props.eventData.driverFee,
        });
    }
};
LogDriverActivityEventSubscriber = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('LogDriverActivityUseCase')),
    tslib_1.__metadata("design:paramtypes", [log_driver_activity_use_case_1.default])
], LogDriverActivityEventSubscriber);
exports.default = LogDriverActivityEventSubscriber;
//# sourceMappingURL=log-driver-activity-event-subscriber.js.map