import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import AssignDeliveryOrderCommand from './assign-delivery-order-command';
import RequestDeliveryUseCase from '@deliveryService/store/application/request-delivery/request-delivery-use-case';
import StoreOrderQueryRepository from '@deliveryService/store/domain/repositories/store-order-query-repository';
import CustomerPublicProfileQueryRepository from '@deliveryService/customer/domain/repositories/customer-public-profile-query-repository';
import StorePublicProfileQueryRepository from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import ServiceConfigurationsQueryRepository from '@deliveryService/delivery/domain/repositories/service-configurations-query-repository';
import RiderPublicProfileQueryRepository from '@deliveryService/rider/domain/repositories/rider-public-profile-query-repository';
import AcceptDeliveryRequestUseCase from '@deliveryService/rider/application/accept-delivery-request/accept-delivery-request-use-case';
export default class AssignDeliveryOrderCommandHandler implements CommandHandler {
    private useCase;
    private deliveryRequestRepository;
    private orderRepository;
    private customerPublicProfileQueryRepository;
    private riderPublicProfileQueryRepository;
    private storePublicProfileQueryRepository;
    private serviceConfigQueryRepository;
    private requestDeliveryUseCase;
    constructor(useCase: AcceptDeliveryRequestUseCase, deliveryRequestRepository: DeliveryRequestCommandRepository, orderRepository: StoreOrderQueryRepository, customerPublicProfileQueryRepository: CustomerPublicProfileQueryRepository, riderPublicProfileQueryRepository: RiderPublicProfileQueryRepository, storePublicProfileQueryRepository: StorePublicProfileQueryRepository, serviceConfigQueryRepository: ServiceConfigurationsQueryRepository, requestDeliveryUseCase: RequestDeliveryUseCase);
    getCommandName(): string;
    handle(command: AssignDeliveryOrderCommand): Promise<void>;
    private createDeliveryRequest;
}
