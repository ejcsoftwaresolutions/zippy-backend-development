import Command from '@shared/domain/bus/command/command';
export default class AssignDeliveryOrderCommand extends Command {
    readonly driverId: string;
    readonly orderId: string;
    constructor(driverId: string, orderId: string);
    name(): string;
}
