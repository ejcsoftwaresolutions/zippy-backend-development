"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const assign_delivery_order_command_1 = require("./assign-delivery-order-command");
const request_delivery_use_case_1 = require("../../../store/application/request-delivery/request-delivery-use-case");
const store_order_query_repository_1 = require("../../../store/domain/repositories/store-order-query-repository");
const id_1 = require("../../../../shared/domain/id/id");
const customer_public_profile_query_repository_1 = require("../../../customer/domain/repositories/customer-public-profile-query-repository");
const store_public_profile_query_repository_1 = require("../../../store/domain/repositories/store-public-profile-query-repository");
const service_configurations_query_repository_1 = require("../../../delivery/domain/repositories/service-configurations-query-repository");
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const delivery_request_rider_candidate_model_1 = require("../../../delivery/domain/models/delivery-request-rider-candidate-model");
const rider_public_profile_query_repository_1 = require("../../domain/repositories/rider-public-profile-query-repository");
const accept_delivery_request_use_case_1 = require("../accept-delivery-request/accept-delivery-request-use-case");
let AssignDeliveryOrderCommandHandler = class AssignDeliveryOrderCommandHandler {
    constructor(useCase, deliveryRequestRepository, orderRepository, customerPublicProfileQueryRepository, riderPublicProfileQueryRepository, storePublicProfileQueryRepository, serviceConfigQueryRepository, requestDeliveryUseCase) {
        this.useCase = useCase;
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.orderRepository = orderRepository;
        this.customerPublicProfileQueryRepository = customerPublicProfileQueryRepository;
        this.riderPublicProfileQueryRepository = riderPublicProfileQueryRepository;
        this.storePublicProfileQueryRepository = storePublicProfileQueryRepository;
        this.serviceConfigQueryRepository = serviceConfigQueryRepository;
        this.requestDeliveryUseCase = requestDeliveryUseCase;
    }
    getCommandName() {
        return assign_delivery_order_command_1.default.name;
    }
    async handle(command) {
        let currentRequest;
        const riderProfile = await this.riderPublicProfileQueryRepository.findProfileById(command.driverId);
        if (!riderProfile)
            throw new Error('rider_not_found');
        const candidate = delivery_request_rider_candidate_model_1.default.fromPrimitives({
            id: command.driverId,
            attemptNumber: 1,
            initialCoords: {
                address: '',
                geoLocation: [-1, -1]
            },
            isBlockedToTakeRequest: false,
            notification: null,
            publicProfile: riderProfile.toPrimitives()
        });
        try {
            currentRequest = await this.deliveryRequestRepository.getRequest(command.orderId);
        }
        catch (e) {
            if (e.message == 'request_not_found') {
                currentRequest = await this.createDeliveryRequest(command, candidate);
            }
        }
        if (!currentRequest)
            throw new Error('empty_request');
        await this.useCase.setRiderToOrder({
            request: currentRequest,
            driver: candidate,
            assigned: true
        });
    }
    async createDeliveryRequest(command, candidate) {
        const storeOrder = await this.orderRepository.findById(command.orderId);
        if (!storeOrder) {
            throw new Error('order_not_found');
        }
        const customer = await this.customerPublicProfileQueryRepository.findProfileById(storeOrder.customerId);
        if (!customer) {
            throw new Error('customer_not_found');
        }
        const id = new id_1.default(command.orderId);
        const serviceConfigurations = await this.serviceConfigQueryRepository.getConfigurations();
        const request = await this.requestDeliveryUseCase.saveRequest({
            id: id,
            customer: customer,
            order: storeOrder,
            serviceConfigurations: serviceConfigurations,
            mode: 'live',
            customerLocation: storeOrder.customerLocation,
            serviceType: 'CAR',
            candidate: candidate
        });
        return request;
    }
};
AssignDeliveryOrderCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('AcceptDeliveryRequestUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(2, (0, decorators_1.inject)('StoreOrderQueryRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('CustomerPublicProfileQueryRepository')),
    tslib_1.__param(4, (0, decorators_1.inject)('RiderPublicProfileQueryRepository')),
    tslib_1.__param(5, (0, decorators_1.inject)('StorePublicProfileQueryRepository')),
    tslib_1.__param(6, (0, decorators_1.inject)('ServiceConfigurationsQueryRepository')),
    tslib_1.__param(7, (0, decorators_1.inject)('RequestDeliveryUseCase')),
    tslib_1.__metadata("design:paramtypes", [accept_delivery_request_use_case_1.default, Object, Object, Object, Object, Object, Object, request_delivery_use_case_1.default])
], AssignDeliveryOrderCommandHandler);
exports.default = AssignDeliveryOrderCommandHandler;
//# sourceMappingURL=assign-delivery-order-command-handler.js.map