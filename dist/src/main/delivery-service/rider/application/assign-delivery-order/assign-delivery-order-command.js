"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class AssignDeliveryOrderCommand extends command_1.default {
    constructor(driverId, orderId) {
        super();
        this.driverId = driverId;
        this.orderId = orderId;
    }
    name() {
        return AssignDeliveryOrderCommand.name;
    }
}
exports.default = AssignDeliveryOrderCommand;
//# sourceMappingURL=assign-delivery-order-command.js.map