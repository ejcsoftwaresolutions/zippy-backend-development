import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import DeliveryRequestRiderCandidate from '@deliveryService/delivery/domain/models/delivery-request-rider-candidate-model';
import DeliveryRequest from '@deliveryService/delivery/domain/models/delivery-request-model';
import DeliveryCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-command-repository';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import EventBus from '@shared/domain/bus/event/event-bus';
export default class AcceptDeliveryRequestUseCase {
    private requestTimerService;
    private deliveryRequestRepository;
    private deliveryCommandRepository;
    private eventBus;
    private request;
    constructor(requestTimerService: RequestAttemptCountdownTimer, deliveryRequestRepository: DeliveryRequestCommandRepository, deliveryCommandRepository: DeliveryCommandRepository, eventBus: EventBus);
    execute(request: DeliveryRequest, driver: DeliveryRequestRiderCandidate): Promise<void>;
    setRiderToOrder({ request, driver, assigned }: {
        request: DeliveryRequest;
        driver: DeliveryRequestRiderCandidate;
        assigned?: boolean;
    }): Promise<void>;
    private afterAccepted;
}
