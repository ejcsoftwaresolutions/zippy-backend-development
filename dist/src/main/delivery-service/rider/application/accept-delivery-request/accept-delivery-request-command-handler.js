"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const request_attempt_countdown_timer_1 = require("../../../delivery/domain/contracts/request-attempt-countdown-timer");
const delivery_request_rider_candidate_model_1 = require("../../../delivery/domain/models/delivery-request-rider-candidate-model");
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const accept_delivery_request_command_1 = require("./accept-delivery-request-command");
const accept_delivery_request_use_case_1 = require("./accept-delivery-request-use-case");
let AcceptDeliveryRequestCommandHandler = class AcceptDeliveryRequestCommandHandler {
    constructor(useCase, deliveryRequestRepository, requestTimerService) {
        this.useCase = useCase;
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.requestTimerService = requestTimerService;
    }
    getCommandName() {
        return accept_delivery_request_command_1.default.name;
    }
    async handle(command) {
        const request = await this.deliveryRequestRepository.getRequest(command.requestId);
        if (!request) {
            throw new Error('request_not_found');
        }
        const currentDriver = request.currentAlertedCandidate;
        this.guardProperResponse(request, currentDriver);
        await this.useCase.execute(request, currentDriver);
    }
    guardProperResponse(request, driver) {
        if (this.requestTimerService.get(request.id.value).candidateId !== driver.id) {
            throw new Error('too_late_to_accept');
        }
    }
};
AcceptDeliveryRequestCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('AcceptDeliveryRequestUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(2, (0, decorators_1.inject)('services.request.attempt.countdown.timer')),
    tslib_1.__metadata("design:paramtypes", [accept_delivery_request_use_case_1.default, Object, Object])
], AcceptDeliveryRequestCommandHandler);
exports.default = AcceptDeliveryRequestCommandHandler;
//# sourceMappingURL=accept-delivery-request-command-handler.js.map