"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const request_attempt_countdown_timer_1 = require("../../../delivery/domain/contracts/request-attempt-countdown-timer");
const delivery_model_1 = require("../../../delivery/domain/models/delivery-model");
const delivery_request_rider_candidate_model_1 = require("../../../delivery/domain/models/delivery-request-rider-candidate-model");
const delivery_request_model_1 = require("../../../delivery/domain/models/delivery-request-model");
const delivery_command_repository_1 = require("../../../delivery/domain/repositories/delivery-command-repository");
const delivery_request_command_repository_1 = require("../../../delivery/domain/repositories/delivery-request-command-repository");
const rider_accepted_domain_event_1 = require("../../domain/events/rider-accepted-domain-event");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
let AcceptDeliveryRequestUseCase = class AcceptDeliveryRequestUseCase {
    constructor(requestTimerService, deliveryRequestRepository, deliveryCommandRepository, eventBus) {
        this.requestTimerService = requestTimerService;
        this.deliveryRequestRepository = deliveryRequestRepository;
        this.deliveryCommandRepository = deliveryCommandRepository;
        this.eventBus = eventBus;
    }
    async execute(request, driver) {
        this.request = request;
        this.requestTimerService.stop(this.request.id.value);
        this.requestTimerService.destroy(this.request.id.value);
        await this.setRiderToOrder({
            request, driver
        });
    }
    async setRiderToOrder({ request, driver, assigned }) {
        console.log(`${request.id.value}
            'El conductor  ${driver.id}  acepto la solicitud`);
        request.setAcceptedRider(driver);
        if (!!request.currentAlertedCandidate) {
            request.currentAlertedCandidate.setNotification(null);
            await this.deliveryRequestRepository.updateDriverNotification(request.currentAlertedCandidate, request.mode);
        }
        await this.deliveryRequestRepository.updateRequest(request);
        await this.deliveryRequestRepository.deleteRequest(request);
        this.afterAccepted({
            request,
            driver,
            assigned
        });
        this.eventBus.publish([
            new rider_accepted_domain_event_1.default({
                occurredOn: new Date(),
                aggregateId: driver.id,
                eventData: {
                    riderId: driver.id,
                    vendorId: request.requestInfo.storeId,
                    vendorOrderId: request.requestInfo.id,
                    driverFee: request.toPrimitives().finalFee.amount
                }
            })
        ]);
    }
    afterAccepted({ request, driver, assigned }) {
        const newDelivery = delivery_model_1.default.fromDeliveryRequest(request);
        if (assigned) {
            newDelivery.setAssigned();
        }
        this.deliveryCommandRepository
            .createDelivery(newDelivery)
            .then(() => {
            console.log('Delivery created');
        })
            .catch((e) => {
        });
    }
};
AcceptDeliveryRequestUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('services.request.attempt.countdown.timer')),
    tslib_1.__param(1, (0, decorators_1.inject)('DeliveryRequestCommandRepository')),
    tslib_1.__param(2, (0, decorators_1.inject)('DeliveryCommandRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('event.bus')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object, Object])
], AcceptDeliveryRequestUseCase);
exports.default = AcceptDeliveryRequestUseCase;
//# sourceMappingURL=accept-delivery-request-use-case.js.map