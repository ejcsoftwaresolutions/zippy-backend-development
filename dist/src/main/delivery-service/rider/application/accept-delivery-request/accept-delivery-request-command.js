"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class AcceptDeliveryRequestCommand extends command_1.default {
    constructor(driverId, requestId) {
        super();
        this.driverId = driverId;
        this.requestId = requestId;
    }
    name() {
        return AcceptDeliveryRequestCommand.name;
    }
}
exports.default = AcceptDeliveryRequestCommand;
//# sourceMappingURL=accept-delivery-request-command.js.map