import RequestAttemptCountdownTimer from '@deliveryService/delivery/domain/contracts/request-attempt-countdown-timer';
import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import AcceptDeliveryRequestCommand from './accept-delivery-request-command';
import AcceptDeliveryRequestUseCase from './accept-delivery-request-use-case';
export default class AcceptDeliveryRequestCommandHandler implements CommandHandler {
    private useCase;
    private deliveryRequestRepository;
    private requestTimerService;
    constructor(useCase: AcceptDeliveryRequestUseCase, deliveryRequestRepository: DeliveryRequestCommandRepository, requestTimerService: RequestAttemptCountdownTimer);
    getCommandName(): string;
    handle(command: AcceptDeliveryRequestCommand): Promise<void>;
    private guardProperResponse;
}
