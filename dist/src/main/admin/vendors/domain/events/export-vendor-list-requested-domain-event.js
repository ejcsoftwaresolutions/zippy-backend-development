"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'EXPORT_VENDOR_LIST_REQUESTED';
class ExportVendorListRequestedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = ExportVendorListRequestedDomainEvent;
ExportVendorListRequestedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=export-vendor-list-requested-domain-event.js.map