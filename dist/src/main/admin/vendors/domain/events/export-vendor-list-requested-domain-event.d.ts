import DomainEvent, { DomainEventProps } from '@shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {
        email: string;
        format: string;
        filters?: any;
        limit?: number;
        skip?: number;
        order?: any;
    };
}
export default class ExportVendorListRequestedDomainEvent extends DomainEvent<EventProps> {
    static eventName: string;
    eventName(): string;
}
export {};
