"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const utils_1 = require("../../../../shared/domain/utils");
class VendorExport extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.email;
    }
    get title() {
        return this.props.title;
    }
    get phone() {
        return this.props.phone;
    }
    get createdAt() {
        return this.props.createdAt;
    }
    static fromPrimitives({ ...plainData }) {
        return new VendorExport(plainData);
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = VendorExport;
//# sourceMappingURL=vendor-export.js.map