import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
export interface VendorExportProps {
    id: string;
    email: string;
    title: string;
    rif: string;
    phone: string;
    city: string;
    state: string;
    createdAt?: Date;
}
export type VendorExportPrimitiveProps = VendorExportProps;
export default class VendorExport extends AggregateRoot<VendorExportProps> {
    constructor(props: VendorExportProps);
    get id(): string;
    get email(): string;
    get title(): string;
    get phone(): string;
    get createdAt(): Date;
    static fromPrimitives({ ...plainData }: VendorExportPrimitiveProps): VendorExport;
    toPrimitives(): VendorExportPrimitiveProps;
}
