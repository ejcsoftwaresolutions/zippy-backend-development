import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import ExportVendorListRequestUseCase from './export-vendor-list-request-use-case';
import ExportVendorListRequestedDomainEvent from '@admin/vendors/domain/events/export-vendor-list-requested-domain-event';
export default class ExportVendorListRequestEventSubscriber implements EventSubscriber {
    private useCase;
    constructor(useCase: ExportVendorListRequestUseCase);
    subscribedTo(): Map<string, Function>;
    handle(event: ExportVendorListRequestedDomainEvent): Promise<void>;
}
