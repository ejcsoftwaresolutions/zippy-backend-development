"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const event_subscriber_1 = require("../../../../shared/domain/bus/event/event-subscriber");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const export_vendor_list_request_use_case_1 = require("./export-vendor-list-request-use-case");
const export_vendor_list_requested_domain_event_1 = require("../../domain/events/export-vendor-list-requested-domain-event");
let ExportVendorListRequestEventSubscriber = class ExportVendorListRequestEventSubscriber {
    constructor(useCase) {
        this.useCase = useCase;
    }
    subscribedTo() {
        const events = new Map();
        events.set(export_vendor_list_requested_domain_event_1.default.eventName, this.handle.bind(this));
        return events;
    }
    async handle(event) {
        await this.useCase.execute(event.props.eventData);
    }
};
ExportVendorListRequestEventSubscriber = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('ExportVendorListRequestUseCase')),
    tslib_1.__metadata("design:paramtypes", [export_vendor_list_request_use_case_1.default])
], ExportVendorListRequestEventSubscriber);
exports.default = ExportVendorListRequestEventSubscriber;
//# sourceMappingURL=export-vendor-list-request-event-subscriber.js.map