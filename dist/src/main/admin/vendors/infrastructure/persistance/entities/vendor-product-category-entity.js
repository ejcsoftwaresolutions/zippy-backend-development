"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorProductCategoryEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let VendorProductCategoryEntity = class VendorProductCategoryEntity {
};
VendorProductCategoryEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('vendor_categories')
], VendorProductCategoryEntity);
exports.VendorProductCategoryEntity = VendorProductCategoryEntity;
//# sourceMappingURL=vendor-product-category-entity.js.map