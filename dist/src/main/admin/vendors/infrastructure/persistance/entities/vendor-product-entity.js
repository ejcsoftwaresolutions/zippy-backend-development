"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorProductEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
let VendorProductEntity = class VendorProductEntity {
};
VendorProductEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('vendor_products')
], VendorProductEntity);
exports.VendorProductEntity = VendorProductEntity;
//# sourceMappingURL=vendor-product-entity.js.map