import PdfGenerator from '@shared/domain/pdf/pdf-generator';
import ExcelGenerator from '@shared/domain/excel/excel-generator';
export default class ExportRiderListRequestUseCase {
    private pdfGenerator;
    private excelGenerator;
    constructor(pdfGenerator: PdfGenerator, excelGenerator: ExcelGenerator);
    execute(requestData: {
        format: string;
        email: string;
        filters?: any;
        limit?: number;
        skip?: number;
        order?: any;
    }): Promise<void>;
    private getData;
    private requestPdf;
    private parseTemplate;
    private exportExcel;
    private sendEmail;
}
