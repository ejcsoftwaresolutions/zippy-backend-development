"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const service_1 = require("../../../../shared/domain/decorators/service");
const firebase_admin_1 = require("firebase-admin");
const rider_export_1 = require("../../domain/models/rider-export");
const decorators_1 = require("../../../../shared/domain/decorators");
const pdf_generator_1 = require("../../../../shared/domain/pdf/pdf-generator");
const excel_generator_1 = require("../../../../shared/domain/excel/excel-generator");
const excel_request_1 = require("../../../../shared/domain/excel/excel-request");
const utils_1 = require("../../../../shared/domain/utils");
const id_1 = require("../../../../shared/domain/id/id");
const path = require('path');
const fs = require('fs');
const ejs = require('ejs');
let ExportRiderListRequestUseCase = class ExportRiderListRequestUseCase {
    constructor(pdfGenerator, excelGenerator) {
        this.pdfGenerator = pdfGenerator;
        this.excelGenerator = excelGenerator;
    }
    async execute(requestData) {
        const data = await this.getData();
        let fileUrl;
        if (requestData.format === "PDF") {
            await this.requestPdf({
                items: data,
                email: requestData.email,
                title: 'Lista de Zippers',
            });
            return;
        }
        try {
            fileUrl = await this.exportExcel(data);
            console.log(fileUrl);
            await this.sendEmail({
                email: requestData.email,
                title: 'Lista de Zippers',
                url: fileUrl,
            });
        }
        catch (e) {
            console.log(e);
        }
    }
    async getData(filters, limit, skip, order) {
        const items = await firebase_admin_1.default
            .firestore()
            .collection('users')
            .where('role', '==', 'driver');
        const data = await items.get();
        const fItems = data.docs.map((s) => s.data());
        return fItems.map((item) => {
            return rider_export_1.default.fromPrimitives({
                id: item.id,
                email: item.email,
                firstName: item.firstName,
                lastName: item.lastName,
                identificationCard: item.identificationCard,
                phone: item.phone,
                createdAt: new Date(item.createdAt.seconds * 1000),
                deliveryMethod: item.deliveryMethod,
            });
        });
    }
    async requestPdf({ email, items, title, }) {
        const pdfTemplatesFolder = path.resolve("public/pdf-templates");
        const pdfTemplate = fs.readFileSync(pdfTemplatesFolder + "/rider-list.ejs", "utf-8");
        const pdfContent = await this.parseTemplate(pdfTemplate, {
            riders: items
                .map((i) => i.toPrimitives())
                .map((i) => ({
                firstName: i.firstName,
                lastName: i.lastName,
                email: i.email,
                identificationCard: i.identificationCard,
                createdAt: utils_1.DateTimeUtils.format(i.createdAt, 'DD/MM/YYYY'),
            })),
        });
        await firebase_admin_1.default
            .firestore()
            .collection('admin_pdf')
            .add({
            email: email,
            slug: "RIDER_LIST",
            content: pdfContent,
            title: title,
            status: "REQUESTED",
            fileName: `admin/pdf/riders-${new id_1.default().value}.pdf`,
            pdfConfig: {}
        });
    }
    async parseTemplate(template, variables) {
        return ejs.render(template, variables, {});
    }
    async exportExcel(items) {
        const styles = {
            header: {
                font: {
                    color: {
                        rgb: '000',
                    },
                    sz: 12,
                    bold: true,
                    underline: false,
                },
            },
        };
        const result = await this.excelGenerator.generate(new excel_request_1.default({
            uploadFolderUrl: 'admin/excel',
            data: items
                .map((i) => i.toPrimitives())
                .map((i) => ({
                firstName: i.firstName,
                lastName: i.lastName,
                email: i.email,
                identificationCard: i.identificationCard,
                createdAt: utils_1.DateTimeUtils.format(i.createdAt, 'DD/MM/YYYY'),
            })),
            specification: {
                firstName: {
                    displayName: 'Nombre',
                    width: 120,
                    headerStyle: styles.header,
                },
                lastName: {
                    displayName: 'Apellido',
                    width: 120,
                    headerStyle: styles.header,
                },
                email: {
                    displayName: 'Email',
                    width: 120,
                    headerStyle: styles.header,
                },
                identificationCard: {
                    displayName: 'Cédula',
                    width: 120,
                    headerStyle: styles.header,
                },
                createdAt: {
                    displayName: 'Fecha de registro',
                    width: 120,
                    headerStyle: styles.header,
                },
            },
        }));
        return result.url;
    }
    async sendEmail({ email, url, title, }) {
        await firebase_admin_1.default
            .firestore()
            .collection('mail')
            .add({
            to: email,
            message: {
                subject: `${title}`,
                html: `
                  <div>
                       <p>
                         Reporte generado, puedes descargar el archivo <a href="${url}">aquí</a> !.
                      </p>
                    <br/>
                  </div>
                `,
            },
        });
    }
};
ExportRiderListRequestUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('pdf.generator')),
    tslib_1.__param(1, (0, decorators_1.inject)('excel.generator')),
    tslib_1.__metadata("design:paramtypes", [Object, Object])
], ExportRiderListRequestUseCase);
exports.default = ExportRiderListRequestUseCase;
//# sourceMappingURL=export-rider-list-request-use-case.js.map