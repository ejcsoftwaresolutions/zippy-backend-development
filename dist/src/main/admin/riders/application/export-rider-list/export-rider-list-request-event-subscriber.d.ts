import EventSubscriber from '@shared/domain/bus/event/event-subscriber';
import ExportRiderListRequestUseCase from './export-rider-list-request-use-case';
import ExportRiderListRequestedDomainEvent from '@admin/riders/domain/events/export-rider-list-requested-domain-event';
export default class ExportRiderListRequestEventSubscriber implements EventSubscriber {
    private useCase;
    constructor(useCase: ExportRiderListRequestUseCase);
    subscribedTo(): Map<string, Function>;
    handle(event: ExportRiderListRequestedDomainEvent): Promise<void>;
}
