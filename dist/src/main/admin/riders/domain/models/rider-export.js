"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const utils_1 = require("../../../../shared/domain/utils");
class RiderExport extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.email;
    }
    get rating() {
        return this.props.rating;
    }
    get firstName() {
        return this.props.firstName;
    }
    get lastName() {
        return this.props.lastName;
    }
    get phone() {
        return this.props.phone;
    }
    get profilePictureUrl() {
        return this.props.profilePictureUrl;
    }
    get deliveryMethod() {
        return this.props.deliveryMethod;
    }
    get createdAt() {
        return this.props.createdAt;
    }
    get identificationCard() {
        return this.props.identificationCard;
    }
    static fromPrimitives({ ...plainData }) {
        return new RiderExport(plainData);
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = RiderExport;
//# sourceMappingURL=rider-export.js.map