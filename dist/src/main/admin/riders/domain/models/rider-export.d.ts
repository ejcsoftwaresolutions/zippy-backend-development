import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
export interface RiderExportProps {
    id: string;
    email: string;
    rating?: number;
    firstName?: string;
    lastName?: string;
    phone?: string;
    profilePictureUrl?: string;
    deliveryMethod: string;
    createdAt?: Date;
    identificationCard?: string;
}
export type RiderExportPrimitiveProps = RiderExportProps;
export default class RiderExport extends AggregateRoot<RiderExportProps> {
    constructor(props: RiderExportProps);
    get id(): string;
    get email(): string;
    get rating(): number;
    get firstName(): string;
    get lastName(): string;
    get phone(): string;
    get profilePictureUrl(): string;
    get deliveryMethod(): string;
    get createdAt(): Date;
    get identificationCard(): string;
    static fromPrimitives({ ...plainData }: RiderExportPrimitiveProps): RiderExport;
    toPrimitives(): RiderExportPrimitiveProps;
}
