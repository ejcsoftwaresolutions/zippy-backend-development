import DomainEvent, { DomainEventProps } from '@shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {
        format: string;
        email: string;
        filters?: any;
        limit?: number;
        skip?: number;
        order?: any;
    };
}
export default class ExportRiderListRequestedDomainEvent extends DomainEvent<EventProps> {
    static eventName: string;
    eventName(): string;
}
export {};
