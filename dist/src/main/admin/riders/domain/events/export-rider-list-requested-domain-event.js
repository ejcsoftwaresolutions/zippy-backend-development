"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'EXPORT_RIDER_LIST_REQUESTED';
class ExportRiderListRequestedDomainEvent extends domain_event_1.default {
    eventName() {
        return EVENT_NAME;
    }
}
exports.default = ExportRiderListRequestedDomainEvent;
ExportRiderListRequestedDomainEvent.eventName = EVENT_NAME;
//# sourceMappingURL=export-rider-list-requested-domain-event.js.map