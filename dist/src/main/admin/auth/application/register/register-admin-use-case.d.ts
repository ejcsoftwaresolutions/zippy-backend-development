import Id from '@shared/domain/id/id';
import PasswordHasher from '@shared/domain/utils/password-hasher';
import AuthAdminCommandRepository from '../../domain/repositories/auth-admin-command-repository';
type UserProps = {
    id: Id;
    email: string;
    password: string;
    roleId: string;
};
export default class RegisterAdminUseCase {
    private authRepository;
    private passwordHasher;
    constructor(authRepository: AuthAdminCommandRepository, passwordHasher: PasswordHasher);
    execute(newUser: UserProps): Promise<void>;
}
export {};
