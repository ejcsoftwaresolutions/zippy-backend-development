"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const create_admin_command_1 = require("./create-admin-command");
const register_admin_use_case_1 = require("./register-admin-use-case");
let CreateAdminCommandHandler = class CreateAdminCommandHandler {
    constructor(userCreator) {
        this.userCreator = userCreator;
    }
    getCommandName() {
        return create_admin_command_1.default.name;
    }
    async handle(command) {
        const id = new id_1.default();
        try {
            await this.userCreator.execute({ id: id, ...command });
        }
        catch (e) {
            throw new Error('No fue posible registrar un usuario');
        }
    }
};
CreateAdminCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('RegisterAdminUseCase')),
    tslib_1.__metadata("design:paramtypes", [register_admin_use_case_1.default])
], CreateAdminCommandHandler);
exports.default = CreateAdminCommandHandler;
//# sourceMappingURL=create-admin-command-handler.js.map