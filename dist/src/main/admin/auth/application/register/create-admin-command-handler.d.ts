import CommandHandler from '../../../../shared/domain/bus/command/command-handler';
import CreateAdminCommand from './create-admin-command';
import RegisterAdminUseCase from './register-admin-use-case';
export default class CreateAdminCommandHandler implements CommandHandler {
    private userCreator;
    constructor(userCreator: RegisterAdminUseCase);
    getCommandName(): string;
    handle(command: CreateAdminCommand): Promise<void>;
}
