import Command from '@shared/domain/bus/command/command';
export default class CreateAdminCommand extends Command {
    readonly email: string;
    readonly password: string;
    readonly roleId: string;
    constructor(email: string, password: string, roleId: string);
    name(): string;
}
