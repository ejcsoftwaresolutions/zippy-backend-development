"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const admin_auth_user_1 = require("../../domain/models/admin-auth-user");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const password_hasher_1 = require("../../../../shared/domain/utils/password-hasher");
let RegisterAdminUseCase = class RegisterAdminUseCase {
    constructor(authRepository, passwordHasher) {
        this.authRepository = authRepository;
        this.passwordHasher = passwordHasher;
    }
    async execute(newUser) {
        const hashedPassword = await this.passwordHasher.hashPassword(newUser.password);
        const newUserEntity = admin_auth_user_1.default.create({
            ...newUser,
            credentials: {
                password: hashedPassword,
                email: newUser.email,
            },
        });
        await this.authRepository.register(newUserEntity);
    }
};
RegisterAdminUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('AuthAdminCommandRepository')),
    tslib_1.__param(1, (0, decorators_1.inject)('utils.passwordHasher')),
    tslib_1.__metadata("design:paramtypes", [Object, Object])
], RegisterAdminUseCase);
exports.default = RegisterAdminUseCase;
//# sourceMappingURL=register-admin-use-case.js.map