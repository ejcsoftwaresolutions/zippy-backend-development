"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class CreateAdminCommand extends command_1.default {
    constructor(email, password, roleId) {
        super();
        this.email = email;
        this.password = password;
        this.roleId = roleId;
    }
    name() {
        return CreateAdminCommand.name;
    }
}
exports.default = CreateAdminCommand;
//# sourceMappingURL=create-admin-command.js.map