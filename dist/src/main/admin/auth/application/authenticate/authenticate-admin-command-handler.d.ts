import AuthenticateAdminCommand from '@admin/auth/application/authenticate/authenticate-admin-command';
import CommandHandler from '@shared/domain/bus/command/command-handler';
import AuthenticateAdminUseCase from './authenticate-admin-use-case';
export default class AuthenticateAdminCommandHandler implements CommandHandler {
    private authenticator;
    constructor(authenticator: AuthenticateAdminUseCase);
    getCommandName(): string;
    handle(command: AuthenticateAdminCommand): Promise<any>;
}
