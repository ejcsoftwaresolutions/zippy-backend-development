"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const admin_auth_credentials_1 = require("../../domain/models/admin-auth-credentials");
const auth_token_creator_1 = require("../../../../shared/domain/authentication/auth-token-creator");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const password_hasher_1 = require("../../../../shared/domain/utils/password-hasher");
let AuthenticateAdminUseCase = class AuthenticateAdminUseCase {
    constructor(userFinder, tokenCreator, passwordHasher) {
        this.userFinder = userFinder;
        this.tokenCreator = tokenCreator;
        this.passwordHasher = passwordHasher;
    }
    async execute(credentials) {
        const authUser = await this.ensureUserExists(credentials.email);
        await this.ensureCredentialsAreValid(authUser, credentials);
        const tokenData = {
            email: authUser.email,
            firstName: authUser.firstName,
            lastName: authUser.lastName,
            roleId: authUser.roleId,
            id: authUser.id.value,
            rolePermissions: authUser.rolePermissions,
            userType: 'ADMIN',
        };
        const token = await this.tokenCreator.generate(tokenData);
        return token;
    }
    async ensureUserExists(email) {
        const user = await this.userFinder.findUser(email);
        if (!user) {
            throw new Error('user_not_found');
        }
        return user;
    }
    async ensureCredentialsAreValid(authUser, credentials) {
        var _a, _b;
        const result = await this.passwordHasher.comparePassword((_a = credentials.password) !== null && _a !== void 0 ? _a : '', (_b = authUser.password) !== null && _b !== void 0 ? _b : '');
        if (!result) {
            throw new Error('invalid_credentials');
        }
        return result;
    }
};
AuthenticateAdminUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('AdminQueryRepository')),
    tslib_1.__param(1, (0, decorators_1.inject)('user.authentication.token.creator')),
    tslib_1.__param(2, (0, decorators_1.inject)('utils.passwordHasher')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], AuthenticateAdminUseCase);
exports.default = AuthenticateAdminUseCase;
//# sourceMappingURL=authenticate-admin-use-case.js.map