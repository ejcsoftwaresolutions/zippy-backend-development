import Command from '@shared/domain/bus/command/command';
export default class AuthenticateAdminCommand extends Command {
    readonly email: string;
    readonly password: string;
    constructor(email: string, password: string);
    name(): string;
}
