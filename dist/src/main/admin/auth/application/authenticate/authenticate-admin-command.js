"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class AuthenticateAdminCommand extends command_1.default {
    constructor(email, password) {
        super();
        this.email = email;
        this.password = password;
    }
    name() {
        return AuthenticateAdminCommand.name;
    }
}
exports.default = AuthenticateAdminCommand;
//# sourceMappingURL=authenticate-admin-command.js.map