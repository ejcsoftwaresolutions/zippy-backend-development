"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const authenticate_admin_command_1 = require("./authenticate-admin-command");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const authenticate_admin_use_case_1 = require("./authenticate-admin-use-case");
let AuthenticateAdminCommandHandler = class AuthenticateAdminCommandHandler {
    constructor(authenticator) {
        this.authenticator = authenticator;
    }
    getCommandName() {
        return authenticate_admin_command_1.default.name;
    }
    async handle(command) {
        try {
            return await this.authenticator.execute({ ...command });
        }
        catch (e) {
            throw new Error(e);
        }
    }
};
AuthenticateAdminCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('AuthenticateAdminUseCase')),
    tslib_1.__metadata("design:paramtypes", [authenticate_admin_use_case_1.default])
], AuthenticateAdminCommandHandler);
exports.default = AuthenticateAdminCommandHandler;
//# sourceMappingURL=authenticate-admin-command-handler.js.map