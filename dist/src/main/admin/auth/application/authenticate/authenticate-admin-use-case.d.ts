import { AdminAuthCredentialsProps } from '@admin/auth/domain/models/admin-auth-credentials';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import PasswordHasher from '@shared/domain/utils/password-hasher';
import AdminQueryRepository from '../../domain/repositories/admin-query-repository';
export default class AuthenticateAdminUseCase {
    private userFinder;
    private tokenCreator;
    private passwordHasher;
    constructor(userFinder: AdminQueryRepository, tokenCreator: AuthTokenCreator<any>, passwordHasher: PasswordHasher);
    execute(credentials: AdminAuthCredentialsProps): Promise<string | null>;
    private ensureUserExists;
    private ensureCredentialsAreValid;
}
