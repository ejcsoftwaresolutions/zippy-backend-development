export declare class AdminEntity {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    roleId: string;
    create(props: any): void;
}
