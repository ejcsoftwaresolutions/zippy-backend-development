import { AdminRoleEntity } from '../entities/admin-role-entity';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
export default class AdminRoleCommandInfrastructureRepository extends FireOrmRepository<AdminRoleEntity> {
    static readonly bindingKey = "AdminRoleCommandRepository";
    getEntityClass(): typeof AdminRoleEntity;
    getAll(filters?: any): Promise<AdminRoleEntity[]>;
    create(data: {
        id: string;
        name: string;
        permissions: any[];
        isSuperAdmin?: boolean;
    }): Promise<void>;
    count(filters?: any): number;
    updateRole(id: string, data: Partial<{
        name: string;
        permissions: any[];
        isSuperAdmin?: boolean;
    }>): Promise<void>;
    get(id: string): Promise<AdminRoleEntity>;
    delete(id: string): Promise<void>;
}
