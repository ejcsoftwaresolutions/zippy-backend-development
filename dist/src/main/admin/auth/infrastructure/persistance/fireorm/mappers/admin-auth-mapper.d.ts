import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import { AdminEntity } from '@admin/auth/infrastructure/persistance/fireorm/entities/admin-entity';
export default class AdminAuthMapper {
    static toDomain(plainValues: AdminEntity, rolePermissions: any[]): AdminAuthUser;
    static toPersistence(user: AdminAuthUser): AdminEntity;
}
