"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin_auth_user_1 = require("../../../../domain/models/admin-auth-user");
const admin_entity_1 = require("../entities/admin-entity");
class AdminAuthMapper {
    static toDomain(plainValues, rolePermissions) {
        return admin_auth_user_1.default.fromPrimitives({
            id: plainValues.id,
            email: plainValues.email,
            password: plainValues.password,
            roleId: plainValues.roleId,
            firstName: plainValues.firstName,
            lastName: plainValues.lastName,
            rolePermissions: rolePermissions,
        });
    }
    static toPersistence(user) {
        const primitives = user.toPrimitives();
        const entity = new admin_entity_1.AdminEntity();
        entity.create({
            id: primitives.id,
            email: primitives.email,
            firstName: primitives.firstName,
            lastName: primitives.lastName,
            password: user.password,
            roleId: primitives.roleId,
        });
        return entity;
    }
}
exports.default = AdminAuthMapper;
//# sourceMappingURL=admin-auth-mapper.js.map