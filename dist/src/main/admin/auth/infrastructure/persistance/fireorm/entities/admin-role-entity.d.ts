export declare class AdminRoleEntity {
    id: string;
    name: string;
    permissions: string[];
    isSuperAdmin?: boolean;
    create(props: any): void;
}
