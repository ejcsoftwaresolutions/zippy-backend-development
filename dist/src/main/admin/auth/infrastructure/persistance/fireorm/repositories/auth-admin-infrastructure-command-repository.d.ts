import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import AuthAdminCommandRepository from '@admin/auth/domain/repositories/auth-admin-command-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { AdminEntity } from '@admin/auth/infrastructure/persistance/fireorm/entities/admin-entity';
import AdminRoleCommandInfrastructureRepository from '@admin/auth/infrastructure/persistance/fireorm/repositories/admin-role-infrastructure-command-repository';
export default class AuthAdminInfrastructureCommandRepository extends FireOrmRepository<AdminEntity> implements AuthAdminCommandRepository {
    private roleFinder;
    static readonly bindingKey = "AuthAdminCommandRepository";
    constructor(roleFinder: AdminRoleCommandInfrastructureRepository);
    getEntityClass(): typeof AdminEntity;
    register(user: AdminAuthUser): Promise<void>;
    exists(email: string): Promise<boolean>;
    updateUser(user: AdminAuthUser): Promise<void>;
    changePassword(user: AdminAuthUser): Promise<void>;
    getAll(filters?: any): Promise<AdminEntity[]>;
    count(filters?: any): Promise<number>;
    get(id: string): Promise<AdminAuthUser>;
    delete(id: string): Promise<void>;
}
