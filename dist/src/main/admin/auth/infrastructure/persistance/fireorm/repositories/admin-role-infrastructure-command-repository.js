"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../../../../../../shared/domain/utils");
const admin_role_entity_1 = require("../entities/admin-role-entity");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
class AdminRoleCommandInfrastructureRepository extends fireorm_repository_1.default {
    getEntityClass() {
        return admin_role_entity_1.AdminRoleEntity;
    }
    async getAll(filters) {
        return this.repository().find();
    }
    async create(data) {
        const entity = new admin_role_entity_1.AdminRoleEntity();
        entity.create({
            ...utils_1.ObjectUtils.omit(data, ['id']),
            id: data.id,
        });
        await this.repository().create(entity);
    }
    count(filters) {
        return 0;
    }
    async updateRole(id, data) {
        const entity = new admin_role_entity_1.AdminRoleEntity();
        entity.create({
            ...utils_1.ObjectUtils.omit(data, ['id']),
            id: id,
        });
        await this.repository().update(entity);
    }
    async get(id) {
        const dto = await this.repository().whereEqualTo('id', id).findOne();
        return dto;
    }
    async delete(id) {
        return await this.repository().delete(id);
    }
}
exports.default = AdminRoleCommandInfrastructureRepository;
AdminRoleCommandInfrastructureRepository.bindingKey = 'AdminRoleCommandRepository';
//# sourceMappingURL=admin-role-infrastructure-command-repository.js.map