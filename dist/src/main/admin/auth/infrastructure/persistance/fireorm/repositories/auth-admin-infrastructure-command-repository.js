"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const admin_auth_user_1 = require("../../../../domain/models/admin-auth-user");
const auth_admin_command_repository_1 = require("../../../../domain/repositories/auth-admin-command-repository");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const admin_entity_1 = require("../entities/admin-entity");
const admin_auth_mapper_1 = require("../mappers/admin-auth-mapper");
const decorators_1 = require("../../../../../../shared/domain/decorators");
const admin_role_infrastructure_command_repository_1 = require("./admin-role-infrastructure-command-repository");
let AuthAdminInfrastructureCommandRepository = class AuthAdminInfrastructureCommandRepository extends fireorm_repository_1.default {
    constructor(roleFinder) {
        super();
        this.roleFinder = roleFinder;
    }
    getEntityClass() {
        return admin_entity_1.AdminEntity;
    }
    async register(user) {
        const entity = admin_auth_mapper_1.default.toPersistence(user);
        await this.repository().create(entity);
    }
    async exists(email) {
        const dto = await this.repository().whereEqualTo('email', email).findOne();
        return !!dto;
    }
    async updateUser(user) {
        const entity = admin_auth_mapper_1.default.toPersistence(user);
        await this.repository().update(entity);
    }
    async changePassword(user) {
        const entity = admin_auth_mapper_1.default.toPersistence(user);
        await this.repository().update(entity);
    }
    getAll(filters) {
        return this.repository().find();
    }
    async count(filters) {
        return (await this.repository().find()).length;
    }
    async get(id) {
        const dto = await this.repository().whereEqualTo('id', id).findOne();
        if (!dto)
            return undefined;
        return admin_auth_mapper_1.default.toDomain(dto, []);
    }
    async delete(id) {
        return await this.repository().delete(id);
    }
};
AuthAdminInfrastructureCommandRepository.bindingKey = 'AuthAdminCommandRepository';
AuthAdminInfrastructureCommandRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('AdminRoleCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [admin_role_infrastructure_command_repository_1.default])
], AuthAdminInfrastructureCommandRepository);
exports.default = AuthAdminInfrastructureCommandRepository;
//# sourceMappingURL=auth-admin-infrastructure-command-repository.js.map