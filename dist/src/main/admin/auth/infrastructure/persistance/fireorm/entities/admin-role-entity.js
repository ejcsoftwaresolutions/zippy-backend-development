"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminRoleEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let AdminRoleEntity = class AdminRoleEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
AdminRoleEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('admin_roles')
], AdminRoleEntity);
exports.AdminRoleEntity = AdminRoleEntity;
//# sourceMappingURL=admin-role-entity.js.map