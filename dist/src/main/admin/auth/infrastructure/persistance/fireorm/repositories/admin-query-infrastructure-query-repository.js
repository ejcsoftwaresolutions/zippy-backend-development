"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const admin_auth_user_1 = require("../../../../domain/models/admin-auth-user");
const admin_query_repository_1 = require("../../../../domain/repositories/admin-query-repository");
const decorators_1 = require("../../../../../../shared/domain/decorators");
const admin_role_infrastructure_command_repository_1 = require("./admin-role-infrastructure-command-repository");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const admin_entity_1 = require("../entities/admin-entity");
const admin_auth_mapper_1 = require("../mappers/admin-auth-mapper");
let AuthAdminInfrastructureQueryRepository = class AuthAdminInfrastructureQueryRepository extends fireorm_repository_1.default {
    constructor(roleFinder) {
        super();
        this.roleFinder = roleFinder;
    }
    getEntityClass() {
        return admin_entity_1.AdminEntity;
    }
    async findUser(email) {
        const dto = await this.repository().whereEqualTo('email', email).findOne();
        if (!dto)
            return undefined;
        return admin_auth_mapper_1.default.toDomain(dto, []);
    }
    async findUserById(userId) {
        const dto = await this.repository().whereEqualTo('id', userId).findOne();
        if (!dto)
            return null;
        return admin_auth_mapper_1.default.toDomain(dto, []);
    }
};
AuthAdminInfrastructureQueryRepository.bindingKey = 'AdminQueryRepository';
AuthAdminInfrastructureQueryRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)('AdminRoleCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [admin_role_infrastructure_command_repository_1.default])
], AuthAdminInfrastructureQueryRepository);
exports.default = AuthAdminInfrastructureQueryRepository;
//# sourceMappingURL=admin-query-infrastructure-query-repository.js.map