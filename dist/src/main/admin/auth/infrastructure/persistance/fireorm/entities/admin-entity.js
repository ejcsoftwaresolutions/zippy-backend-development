"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let AdminEntity = class AdminEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
AdminEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('admin')
], AdminEntity);
exports.AdminEntity = AdminEntity;
//# sourceMappingURL=admin-entity.js.map