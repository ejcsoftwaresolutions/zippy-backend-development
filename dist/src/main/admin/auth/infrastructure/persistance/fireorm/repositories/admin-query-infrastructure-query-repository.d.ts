import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import AdminQueryRepository from '@admin/auth/domain/repositories/admin-query-repository';
import AdminRoleCommandInfrastructureRepository from './admin-role-infrastructure-command-repository';
import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { AdminEntity } from '@admin/auth/infrastructure/persistance/fireorm/entities/admin-entity';
export default class AuthAdminInfrastructureQueryRepository extends FireOrmRepository<AdminEntity> implements AdminQueryRepository {
    private roleFinder;
    static readonly bindingKey = "AdminQueryRepository";
    constructor(roleFinder: AdminRoleCommandInfrastructureRepository);
    getEntityClass(): typeof AdminEntity;
    findUser(email: string): Promise<AdminAuthUser>;
    findUserById(userId: string): Promise<AdminAuthUser>;
}
