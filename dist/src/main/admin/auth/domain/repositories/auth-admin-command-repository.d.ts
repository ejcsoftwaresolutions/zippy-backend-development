import AdminAuthUser from '../models/admin-auth-user';
export default interface AuthAdminCommandRepository {
    register(user: AdminAuthUser): Promise<void>;
    updateUser(user: AdminAuthUser): Promise<void>;
    changePassword(user: AdminAuthUser): Promise<void>;
}
