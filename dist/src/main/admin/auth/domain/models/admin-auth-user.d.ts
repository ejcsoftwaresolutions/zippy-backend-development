import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { AdminAuthCredentialsProps } from './admin-auth-credentials';
export interface AdminAuthUserProps {
    id: Id;
    roleId: string;
    email: string;
    firstName?: string;
    lastName?: string;
    credentials: AdminAuthCredentialsProps;
    rolePermissions?: string[];
}
export interface AdminAuthUserPrimitiveProps {
    id: string;
    roleId: string;
    email: string;
    firstName?: string;
    lastName?: string;
    password: string;
    plainPassword?: string;
    rolePermissions?: string[];
}
export default class AdminAuthUser extends AggregateRoot<AdminAuthUserProps> {
    static create(props: AdminAuthUserProps): AdminAuthUser;
    static fromPrimitives(plainData: AdminAuthUserPrimitiveProps): AdminAuthUser;
    get id(): Id;
    get roleId(): string;
    get rolePermissions(): string[];
    get email(): string;
    get firstName(): string;
    get lastName(): string;
    get plainPassword(): string | undefined;
    get password(): string | undefined;
    changePassword(newPassword: string): void;
    toPrimitives(): AdminAuthUserPrimitiveProps;
}
