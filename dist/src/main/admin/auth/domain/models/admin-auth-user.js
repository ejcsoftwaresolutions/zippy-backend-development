"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const admin_auth_credentials_1 = require("./admin-auth-credentials");
class AdminAuthUser extends aggregate_root_1.default {
    static create(props) {
        const newUser = new AdminAuthUser(props);
        return newUser;
    }
    static fromPrimitives(plainData) {
        const credentials = admin_auth_credentials_1.default.fromPrimitives({
            email: plainData.email,
            password: plainData.password,
            plainPassword: plainData.plainPassword,
        });
        return new AdminAuthUser({
            id: new id_1.default(plainData.id),
            roleId: plainData.roleId,
            email: plainData.email,
            firstName: plainData.firstName,
            lastName: plainData.lastName,
            credentials: credentials,
            rolePermissions: plainData.rolePermissions,
        });
    }
    get id() {
        return this.props.id;
    }
    get roleId() {
        return this.props.roleId;
    }
    get rolePermissions() {
        return this.props.rolePermissions;
    }
    get email() {
        return this.props.email;
    }
    get firstName() {
        return this.props.firstName;
    }
    get lastName() {
        return this.props.lastName;
    }
    get plainPassword() {
        return this.props.credentials.plainPassword;
    }
    get password() {
        return this.props.credentials.password;
    }
    changePassword(newPassword) {
        this.props.credentials = admin_auth_credentials_1.default.fromPrimitives({
            ...this.props.credentials,
            password: newPassword,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return json;
    }
}
exports.default = AdminAuthUser;
//# sourceMappingURL=admin-auth-user.js.map