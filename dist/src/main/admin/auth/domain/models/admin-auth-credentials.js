"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const value_object_1 = require("../../../../shared/domain/value-object/value-object");
class AdminAuthCredentials extends value_object_1.default {
    get email() {
        return this.props.email;
    }
    get password() {
        return this.props.password;
    }
    get plainPassword() {
        return this.props.plainPassword;
    }
    constructor(properties) {
        super(properties);
    }
    static fromPrimitives(plainData) {
        return new AdminAuthCredentials(plainData);
    }
}
exports.default = AdminAuthCredentials;
//# sourceMappingURL=admin-auth-credentials.js.map