import ValueObject from '@shared/domain/value-object/value-object';
export interface AdminAuthCredentialsProps {
    email: string;
    password?: string;
    plainPassword?: string;
}
export default class AdminAuthCredentials extends ValueObject<AdminAuthCredentialsProps> {
    get email(): string;
    get password(): string;
    get plainPassword(): string;
    constructor(properties: AdminAuthCredentialsProps);
    static fromPrimitives(plainData: AdminAuthCredentialsProps): AdminAuthCredentials;
}
