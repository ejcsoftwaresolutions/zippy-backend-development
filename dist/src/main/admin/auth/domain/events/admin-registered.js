"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const domain_event_1 = require("../../../../shared/domain/bus/event/domain-event");
const EVENT_NAME = 'ADMIN_REGISTERED';
class AdminRegistered extends domain_event_1.default {
    constructor(props) {
        super({ ...props, occurredOn: new Date() });
    }
    eventName() {
        return EVENT_NAME;
    }
    get userEmail() {
        return this.props.eventData.userEmail;
    }
    get userId() {
        return this.props.eventData.userId;
    }
}
exports.default = AdminRegistered;
AdminRegistered.eventName = EVENT_NAME;
//# sourceMappingURL=admin-registered.js.map