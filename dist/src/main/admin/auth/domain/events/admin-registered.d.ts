import DomainEvent, { DomainEventProps } from '../../../../shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {
        userEmail: string;
        userId: string;
    };
}
export default class AdminRegistered extends DomainEvent<EventProps> {
    static eventName: string;
    constructor(props: EventProps);
    eventName(): string;
    get userEmail(): string;
    get userId(): string;
}
export {};
