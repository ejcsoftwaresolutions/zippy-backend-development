import CommandHandler from '@shared/domain/bus/command/command-handler';
import VendorCommandRepository from '../../domain/repositories/vendor-command-repository';
import ApproveVendorCommand from './approve-vendor-command';
import ApproveVendorUseCase from './approve-vendor-use-case';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import CodeGenerator from '@shared/domain/utils/code-generator';
export default class ApproveVendorCommandHandler implements CommandHandler {
    private useCase;
    private repository;
    private tokenCreator;
    private tokenVerificator;
    private linkAccountValidator;
    private uniqueCodeGenerator;
    constructor(useCase: ApproveVendorUseCase, codeGenerator: CodeGenerator, repository: VendorCommandRepository, tokenCreator: AuthTokenCreator<any>, tokenVerificator: AuthTokenVerificator<any>);
    getCommandName(): string;
    handle(command: ApproveVendorCommand): Promise<{
        id: string;
    }>;
}
