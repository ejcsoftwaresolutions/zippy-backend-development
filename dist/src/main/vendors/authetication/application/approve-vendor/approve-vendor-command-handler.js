"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const approve_vendor_command_1 = require("./approve-vendor-command");
const approve_vendor_use_case_1 = require("./approve-vendor-use-case");
const vendor_profile_1 = require("../../domain/models/vendor-profile");
const user_account_1 = require("../../../../shared/domain/models/user-account");
const auth_token_creator_1 = require("../../../../shared/domain/authentication/auth-token-creator");
const vendor_link_account_request_validator_1 = require("../../domain/services/vendor-link-account-request-validator");
const auth_token_verificator_1 = require("../../../../shared/domain/authentication/auth-token-verificator");
const code_generator_1 = require("../../../../shared/domain/utils/code-generator");
const vendor_code_generator_1 = require("../../domain/services/vendor-code-generator");
let ApproveVendorCommandHandler = class ApproveVendorCommandHandler {
    constructor(useCase, codeGenerator, repository, tokenCreator, tokenVerificator) {
        this.useCase = useCase;
        this.repository = repository;
        this.tokenCreator = tokenCreator;
        this.tokenVerificator = tokenVerificator;
        this.uniqueCodeGenerator = new vendor_code_generator_1.default(codeGenerator);
        this.linkAccountValidator = new vendor_link_account_request_validator_1.default(tokenVerificator);
    }
    getCommandName() {
        return approve_vendor_command_1.default.name;
    }
    async handle(command) {
        const { approveInfo } = command;
        const id = new id_1.default(approveInfo.id);
        const signupData = await this.repository.findVendorSignup(id);
        const foundAccount = await this.repository.findAccountByEmail(signupData.email);
        const code = this.uniqueCodeGenerator.generate({
            title: signupData.shopName,
        });
        const approvedAccount = await (async () => {
            if (foundAccount) {
                await this.linkAccountValidator.validate(signupData.linkAccountToken);
                foundAccount.addRole('VENDOR');
                return foundAccount;
            }
            const newPassToken = await this.tokenCreator.generate({
                id: id.value,
                userType: 'VENDOR',
            });
            return user_account_1.default.fromPrimitives({
                id: id.value,
                email: signupData.email,
                firstName: signupData.firstName,
                birthday: signupData.birthday,
                lastName: signupData.lastName,
                phone: signupData.phone,
                plainPassword: signupData.rif,
                roles: ['VENDOR'],
                hasDefaultPassword: true,
                createdAt: new Date(),
                newPasswordToken: newPassToken,
                status: 'ACTIVE',
            });
        })();
        const profile = vendor_profile_1.default.fromPrimitives({
            id: approvedAccount.id.value,
            identificationCard: signupData.identificationCard,
            rif: signupData.rif,
            categoryId: signupData.categoryId,
            title: signupData.shopName,
            logoImage: undefined,
            schedule: signupData.schedule,
            location: signupData.location,
            description: '',
            code: code,
            status: 'ACTIVE',
            reviews: {
                count: 0,
                sum: 0,
            },
            deliveryMethod: signupData.deliveryMethod,
            isOpen: false,
            deliveryRadius: 0,
            selectedSubcategories: signupData.selectedSubcategories,
            linePhone: signupData.linePhone,
            configurations: {
                referralPromotions: true,
                receivePushNotifications: true,
                receiveSMS: true,
                receiveEmails: true,
            },
            salesCommissionPercentage: signupData.salesCommissionPercentage,
            scheduledDeliveriesSchedule: signupData.deliveryMethod.includes('SCHEDULED') &&
                signupData.schedule &&
                signupData.schedule.MONDAY
                ? Object.values(signupData.schedule).reduce((acc, current) => {
                    return {
                        ...acc,
                        [current.day]: {
                            ...current,
                            slot: 60,
                        },
                    };
                }, {})
                : undefined,
        });
        await this.useCase.execute({
            id: approvedAccount.id,
            account: approvedAccount,
            profile: profile,
        });
        return {
            id: approvedAccount.id.value,
        };
    }
};
ApproveVendorCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('ApproveVendorUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('code.generator')),
    tslib_1.__param(2, (0, decorators_1.inject)('VendorCommandRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('user.authentication.token.creator')),
    tslib_1.__param(4, (0, decorators_1.inject)('user.authentication.token.verificator')),
    tslib_1.__metadata("design:paramtypes", [approve_vendor_use_case_1.default, Object, Object, Object, Object])
], ApproveVendorCommandHandler);
exports.default = ApproveVendorCommandHandler;
//# sourceMappingURL=approve-vendor-command-handler.js.map