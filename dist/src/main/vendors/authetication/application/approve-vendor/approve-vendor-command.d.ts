import Command from '@shared/domain/bus/command/command';
interface RegisterInfo {
    id: string;
}
export default class ApproveVendorCommand extends Command {
    readonly approveInfo: RegisterInfo;
    constructor(approveInfo: RegisterInfo);
    name(): string;
}
export {};
