import EventBus from '@shared/domain/bus/event/event-bus';
import Id from '@shared/domain/id/id';
import VendorProfile from '../../domain/models/vendor-profile';
import VendorCommandRepository from '../../domain/repositories/vendor-command-repository';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import UserAccount from '@shared/domain/models/user-account';
export default class ApproveVendorUseCase {
    private eventBus;
    private repo;
    private tokenCreator;
    constructor(eventBus: EventBus, repo: VendorCommandRepository, tokenCreator: AuthTokenCreator<any>);
    execute(data: {
        id: Id;
        account: UserAccount;
        profile: VendorProfile;
    }): Promise<void>;
}
