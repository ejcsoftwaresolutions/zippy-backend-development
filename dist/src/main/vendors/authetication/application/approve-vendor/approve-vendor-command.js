"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class ApproveVendorCommand extends command_1.default {
    constructor(approveInfo) {
        super();
        this.approveInfo = approveInfo;
    }
    name() {
        return ApproveVendorCommand.name;
    }
}
exports.default = ApproveVendorCommand;
//# sourceMappingURL=approve-vendor-command.js.map