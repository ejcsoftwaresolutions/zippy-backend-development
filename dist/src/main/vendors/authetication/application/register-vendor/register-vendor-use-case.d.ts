import EventBus from '@shared/domain/bus/event/event-bus';
import Id from '@shared/domain/id/id';
import VendorCommandRepository from '../../domain/repositories/vendor-command-repository';
export default class RegisterVendorUseCase {
    private eventBus;
    private vendorRepository;
    constructor(eventBus: EventBus, vendorRepository: VendorCommandRepository);
    execute(data: {
        id: Id;
        code: string;
        shopName: string;
        basicInfo: {
            email: string;
            firstName: string;
            lastName: string;
            phone: string;
            birthday?: Date;
            linePhone?: string;
            identificationCard?: string;
            contactCompanyCharge?: string;
        };
        social: {
            website?: string;
            instagram: string;
        };
        linkAccountToken?: string;
    }): Promise<void>;
}
