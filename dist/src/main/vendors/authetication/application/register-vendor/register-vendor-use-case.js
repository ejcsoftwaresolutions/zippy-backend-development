"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const vendor_signup_1 = require("../../domain/models/vendor-signup");
let RegisterVendorUseCase = class RegisterVendorUseCase {
    constructor(eventBus, vendorRepository) {
        this.eventBus = eventBus;
        this.vendorRepository = vendorRepository;
    }
    async execute(data) {
        const foundUser = await this.vendorRepository.existsUserByEmail(data.basicInfo.email);
        if (!data.linkAccountToken && foundUser) {
            throw new Error('user_already_exists');
        }
        const vendor = vendor_signup_1.default.create({
            id: data.id,
            basicInfo: data.basicInfo,
            social: data.social,
            createdAt: new Date(),
            shopName: data.shopName,
            state: 'NEW',
            code: data.code,
            selectedSubcategories: [],
            linkAccountToken: data.linkAccountToken,
            salesCommissionPercentage: 0,
        });
        await this.vendorRepository.register(vendor);
        this.eventBus.publish(vendor.pullDomainEvents());
    }
};
RegisterVendorUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('event.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('VendorCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [Object, Object])
], RegisterVendorUseCase);
exports.default = RegisterVendorUseCase;
//# sourceMappingURL=register-vendor-use-case.js.map