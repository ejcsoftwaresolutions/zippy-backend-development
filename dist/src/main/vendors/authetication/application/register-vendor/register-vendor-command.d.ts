import Command from '@shared/domain/bus/command/command';
interface RegisterInfo {
    id: string;
    shopName: string;
    basicInfo: {
        identificationCard: string;
        email: string;
        firstName: string;
        lastName: string;
        phone: string;
        birthday?: Date;
        linePhone?: string;
        contactCompanyCharge: string;
    };
    social: {
        website?: string;
        instagram: string;
    };
    linkAccountToken?: string;
}
export default class RegisterVendorCommand extends Command {
    readonly registerInfo: RegisterInfo;
    constructor(registerInfo: RegisterInfo);
    name(): string;
}
export {};
