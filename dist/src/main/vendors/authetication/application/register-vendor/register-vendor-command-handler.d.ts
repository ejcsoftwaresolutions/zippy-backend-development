import CommandHandler from '@shared/domain/bus/command/command-handler';
import RegisterVendorCommand from './register-vendor-command';
import RegisterVendorUseCase from './register-vendor-use-case';
import FileUploader from '@shared/domain/storage/storage-uploader';
import CodeGenerator from '@shared/domain/utils/code-generator';
export default class RegisterVendorCommandHandler implements CommandHandler {
    private useCase;
    private documentsUploader;
    private vendorCodeGenerator;
    constructor(useCase: RegisterVendorUseCase, codeGenerator: CodeGenerator, fileUploader: FileUploader);
    getCommandName(): string;
    handle(command: RegisterVendorCommand): Promise<{
        id: string;
    }>;
}
