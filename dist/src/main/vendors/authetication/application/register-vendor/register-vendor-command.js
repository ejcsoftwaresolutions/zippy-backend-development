"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class RegisterVendorCommand extends command_1.default {
    constructor(registerInfo) {
        super();
        this.registerInfo = registerInfo;
    }
    name() {
        return RegisterVendorCommand.name;
    }
}
exports.default = RegisterVendorCommand;
//# sourceMappingURL=register-vendor-command.js.map