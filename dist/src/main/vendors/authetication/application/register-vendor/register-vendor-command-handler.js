"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const register_vendor_command_1 = require("./register-vendor-command");
const register_vendor_use_case_1 = require("./register-vendor-use-case");
const vendor_documents_uploader_1 = require("../../domain/services/vendor-documents-uploader");
const storage_uploader_1 = require("../../../../shared/domain/storage/storage-uploader");
const code_generator_1 = require("../../../../shared/domain/utils/code-generator");
const vendor_code_generator_1 = require("../../domain/services/vendor-code-generator");
let RegisterVendorCommandHandler = class RegisterVendorCommandHandler {
    constructor(useCase, codeGenerator, fileUploader) {
        this.useCase = useCase;
        this.documentsUploader = new vendor_documents_uploader_1.default(fileUploader);
        this.vendorCodeGenerator = new vendor_code_generator_1.default(codeGenerator);
    }
    getCommandName() {
        return register_vendor_command_1.default.name;
    }
    async handle(command) {
        const id = new id_1.default(command.registerInfo.id);
        const { registerInfo } = command;
        const code = this.vendorCodeGenerator.generate({
            title: registerInfo.shopName,
        });
        await this.useCase.execute({
            id: id,
            code: code,
            basicInfo: registerInfo.basicInfo,
            social: registerInfo.social,
            shopName: registerInfo.shopName,
            linkAccountToken: registerInfo.linkAccountToken,
        });
        return {
            id: id.value,
        };
    }
};
RegisterVendorCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('RegisterVendorUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('code.generator')),
    tslib_1.__param(2, (0, decorators_1.inject)('file.uploader')),
    tslib_1.__metadata("design:paramtypes", [register_vendor_use_case_1.default, Object, Object])
], RegisterVendorCommandHandler);
exports.default = RegisterVendorCommandHandler;
//# sourceMappingURL=register-vendor-command-handler.js.map