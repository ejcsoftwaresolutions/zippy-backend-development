"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const storage_types_1 = require("../../../../shared/domain/storage/storage-types");
const storage_uploader_1 = require("../../../../shared/domain/storage/storage-uploader");
const collection_1 = require("../../../../shared/domain/value-object/collection");
const file_upload_response_1 = require("../../../../shared/domain/storage/file-upload-response");
class VendorDocumentsUploader {
    constructor(fileUploader) {
        this.fileUploader = fileUploader;
    }
    async uploadDocuments(vendorId, files) {
        try {
            const finalFiles = new collection_1.default(files.map((f) => ({
                file: f.file,
                undefined,
                fileKey: f.key,
                filePath: `${VendorDocumentsUploader.uploadPath}/${vendorId}/documents/${f.key
                    .replace('Url', '')
                    .replace(/[0-9]/g, '')
                    .toUpperCase()}`,
            })));
            const response = await this.fileUploader.uploadFiles(finalFiles);
            return response.toPrimitives();
        }
        catch (error) {
            throw new Error('Not possible to upload vendor documents');
        }
    }
}
exports.default = VendorDocumentsUploader;
VendorDocumentsUploader.uploadPath = 'vendors';
//# sourceMappingURL=vendor-documents-uploader.js.map