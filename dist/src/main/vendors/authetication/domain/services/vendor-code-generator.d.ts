import CodeGenerator from '@shared/domain/utils/code-generator';
export default class VendorCodeGenerator {
    private codeGenerator;
    constructor(codeGenerator: CodeGenerator);
    generate(params: {
        title: string;
    }): string;
}
