import { UploadFile } from '@shared/domain/storage/storage-types';
import FileUploader from '@shared/domain/storage/storage-uploader';
import { FileUploadResponse } from '@shared/domain/storage/file-upload-response';
export default class VendorDocumentsUploader {
    private fileUploader;
    static readonly uploadPath = "vendors";
    constructor(fileUploader: FileUploader);
    uploadDocuments(vendorId: string, files: {
        file: UploadFile;
        key: string;
    }[]): Promise<FileUploadResponse[]>;
}
