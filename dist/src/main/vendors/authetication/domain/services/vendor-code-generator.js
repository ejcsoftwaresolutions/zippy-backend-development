"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const code_generator_1 = require("../../../../shared/domain/utils/code-generator");
class VendorCodeGenerator {
    constructor(codeGenerator) {
        this.codeGenerator = codeGenerator;
    }
    generate(params) {
        const firstNamePart = params.title.substring(0, 3).toUpperCase();
        const randomNumbers = this.codeGenerator.generate(7);
        return `${firstNamePart}${randomNumbers}`;
    }
}
exports.default = VendorCodeGenerator;
//# sourceMappingURL=vendor-code-generator.js.map