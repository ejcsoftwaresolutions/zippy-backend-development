"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = require("firebase-admin");
const auth_token_verificator_1 = require("../../../../shared/domain/authentication/auth-token-verificator");
class VendorLinkAccountRequestValidator {
    constructor(tokenVerificator) {
        this.tokenVerificator = tokenVerificator;
    }
    async validate(token) {
        const res = await firebase_admin_1.default
            .firestore()
            .collection('link_account_requests')
            .where('token', '==', token)
            .get();
        if (res.empty)
            throw new Error('INVALID_LINK_TOKEN');
        const decodedToken = await this.tokenVerificator.decodeToken(token);
        if (decodedToken.newRole !== 'VENDOR') {
            throw new Error('INVALID_LINK_TOKEN');
        }
        return decodedToken;
    }
}
exports.default = VendorLinkAccountRequestValidator;
//# sourceMappingURL=vendor-link-account-request-validator.js.map