"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const utils_1 = require("../../../../shared/domain/utils");
const DefaultProps = {
    reviews: {
        count: 0,
        sum: 0,
    },
};
class VendorProfile extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get deliveryMethod() {
        return this.props.deliveryMethod;
    }
    static create(data) {
        return new VendorProfile(data);
    }
    static fromPrimitives({ ...plainData }) {
        return new VendorProfile({
            ...plainData,
            deliveryMethod: plainData.deliveryMethod,
            id: new id_1.default(plainData.id),
        });
    }
    update(updates) {
        Object.assign(this.props, {
            ...this.props,
            ...updates,
            deliveryMethod: updates.deliveryMethod
                ? updates.deliveryMethod
                : this.props.deliveryMethod,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown({
            ...json,
            deliveryMethod: this.props.deliveryMethod,
        });
    }
}
exports.default = VendorProfile;
//# sourceMappingURL=vendor-profile.js.map