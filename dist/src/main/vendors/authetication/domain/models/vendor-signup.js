"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const utils_1 = require("../../../../shared/domain/utils");
const DefaultProps = {};
class VendorSignup extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get code() {
        return this.props.code;
    }
    get rif() {
        return this.props.rif;
    }
    get salesCommissionPercentage() {
        return this.props.salesCommissionPercentage;
    }
    get linkAccountToken() {
        return this.props.linkAccountToken;
    }
    get identificationCard() {
        return this.props.basicInfo.identificationCard;
    }
    get selectedSubcategories() {
        return this.props.selectedSubcategories;
    }
    get birthday() {
        return this.props.basicInfo.birthday;
    }
    get email() {
        return this.props.basicInfo.email;
    }
    get deliveryMethod() {
        return this.props.deliveryMethod;
    }
    get categoryId() {
        return this.props.categoryId;
    }
    get firstName() {
        return this.props.basicInfo.firstName;
    }
    get lastName() {
        return this.props.basicInfo.lastName;
    }
    get schedule() {
        return this.props.schedule;
    }
    get phone() {
        return this.props.basicInfo.phone;
    }
    get linePhone() {
        return this.props.basicInfo.linePhone;
    }
    get shopName() {
        return this.props.shopName;
    }
    get createdAt() {
        return this.props.createdAt;
    }
    get location() {
        return this.props.location;
    }
    static create(data) {
        return new VendorSignup(data);
    }
    static fromPrimitives(plainData) {
        return new VendorSignup({
            ...plainData,
            deliveryMethod: plainData.deliveryMethod,
            id: new id_1.default(plainData.id),
        });
    }
    update(updates) {
        Object.assign(this, {
            ...this.props,
            ...updates,
            deliveryMethod: updates.deliveryMethod
                ? updates.deliveryMethod
                : this.props.deliveryMethod,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown({
            ...json,
            deliveryMethod: this.props.deliveryMethod,
        });
    }
}
exports.default = VendorSignup;
//# sourceMappingURL=vendor-signup.js.map