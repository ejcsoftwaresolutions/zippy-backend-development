"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const utils_1 = require("../../../../shared/domain/utils");
const user_account_1 = require("../../../../shared/domain/models/user-account");
const vendor_profile_1 = require("./vendor-profile");
const DefaultProps = {};
class VendorUser extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.account.email;
    }
    get firstName() {
        return this.props.account.firstName;
    }
    get lastName() {
        return this.props.account.lastName;
    }
    get phone() {
        return this.props.account.phone;
    }
    get plainPassword() {
        return this.props.account.plainPassword;
    }
    get createdAt() {
        return this.props.account.createdAt;
    }
    static create(data) {
        return new VendorUser(data);
    }
    static fromPrimitives({ ...plainData }) {
        return new VendorUser({
            ...plainData,
            id: new id_1.default(plainData.id),
            profile: vendor_profile_1.default.fromPrimitives(plainData.profile),
            account: user_account_1.default.fromPrimitives(plainData.account),
        });
    }
    update(updates) {
        Object.assign(this, {
            ...this.props,
            ...updates,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = VendorUser;
//# sourceMappingURL=vendor-user.js.map