import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
export interface VendorProfileProps {
    id: Id;
    title: string;
    code: string;
    logoImage?: string;
    isOpen: boolean;
    deliveryRadius?: number;
    rif: string;
    identificationCard: string;
    categoryId: string;
    deliveryMethod: string[];
    schedule: any;
    scheduledDeliveriesSchedule?: any;
    location: {
        line1: string;
        line2?: string;
        city: string;
        state: string;
        country: string;
        address: string;
        latitude?: number;
        longitude?: number;
    };
    reviews: {
        count: number;
        sum: number;
    };
    description: string;
    status: string;
    selectedSubcategories: {
        id: string;
        name: string;
    }[];
    linePhone?: string;
    configurations: {
        referralPromotions: boolean;
        receiveEmails: boolean;
        receivePushNotifications: boolean;
        receiveSMS: boolean;
    };
    salesCommissionPercentage: number;
}
export interface VendorProfilePrimitiveProps extends Omit<VendorProfileProps, 'id'> {
    id: string;
}
export default class VendorProfile extends AggregateRoot<VendorProfileProps> {
    constructor(props: VendorProfileProps);
    get id(): Id;
    get deliveryMethod(): string[];
    static create(data: VendorProfileProps): VendorProfile;
    static fromPrimitives({ ...plainData }: VendorProfilePrimitiveProps): VendorProfile;
    update(updates: Partial<VendorProfileProps>): void;
    toPrimitives(): VendorProfilePrimitiveProps;
}
