import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import UserAccount, { UserAccountPrimitiveProps } from '@shared/domain/models/user-account';
import VendorProfile, { VendorProfilePrimitiveProps } from './vendor-profile';
export interface VendorUserProps {
    id: Id;
    account: UserAccount;
    profile: VendorProfile;
}
export interface VendorUserPrimitiveProps {
    id: string;
    account: UserAccountPrimitiveProps;
    profile: VendorProfilePrimitiveProps;
}
export default class VendorUser extends AggregateRoot<VendorUserProps> {
    constructor(props: VendorUserProps);
    get id(): Id;
    get email(): string;
    get firstName(): string;
    get lastName(): string;
    get phone(): string;
    get plainPassword(): string;
    get createdAt(): Date;
    static create(data: VendorUserProps): VendorUser;
    static fromPrimitives({ ...plainData }: VendorUserPrimitiveProps): VendorUser;
    update(updates: Partial<VendorUserProps>): void;
    toPrimitives(): VendorUserPrimitiveProps;
}
