import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
export type VendorSignupLocation = {
    line1: string;
    line2?: string;
    city: string;
    state: string;
    country: string;
    address: string;
    latitude?: number;
    longitude?: number;
};
export interface VendorSignupProps {
    id: Id;
    shopName: string;
    code: string;
    location?: VendorSignupLocation;
    rif?: string;
    basicInfo: {
        identificationCard?: string;
        email: string;
        firstName: string;
        lastName: string;
        phone: string;
        birthday?: Date;
        linePhone?: string;
        contactCompanyCharge?: string;
    };
    social: {
        website?: string;
        instagram: string;
    };
    extra?: {
        productTypes: string;
    };
    schedule?: any;
    deliveryMethod?: string[];
    documents?: {
        rifUrl: string;
        dniUrl: string;
        productCatalogUrl: string;
        LIAELicenseUrl: string;
        permitMedicalUrl: string;
        foodManipulationUrl: string;
    };
    withdrawalDetails?: {
        accountType: string;
        bank: string;
        accountNumber: string;
    };
    categoryId?: string;
    selectedSubcategories: {
        id: string;
        name: string;
    }[];
    createdAt: Date;
    state: string;
    linkAccountToken?: string;
    salesCommissionPercentage: number;
}
export interface VendorSignupPrimitiveProps extends Omit<VendorSignupProps, 'id'> {
    id: string;
}
export default class VendorSignup extends AggregateRoot<VendorSignupProps> {
    constructor(props: VendorSignupProps);
    get id(): Id;
    get code(): string;
    get rif(): string;
    get salesCommissionPercentage(): number;
    get linkAccountToken(): string;
    get identificationCard(): string;
    get selectedSubcategories(): {
        id: string;
        name: string;
    }[];
    get birthday(): Date;
    get email(): string;
    get deliveryMethod(): string[];
    get categoryId(): string;
    get firstName(): string;
    get lastName(): string;
    get schedule(): any;
    get phone(): string;
    get linePhone(): string;
    get shopName(): string;
    get createdAt(): Date;
    get location(): VendorSignupLocation;
    static create(data: VendorSignupProps): VendorSignup;
    static fromPrimitives(plainData: VendorSignupPrimitiveProps): VendorSignup;
    update(updates: Partial<VendorSignupProps>): void;
    toPrimitives(): VendorSignupPrimitiveProps;
}
