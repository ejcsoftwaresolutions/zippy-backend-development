import VendorUser from '../models/vendor-user';
import Id from '@shared/domain/id/id';
import VendorSignup from '../models/vendor-signup';
import UserAccount from '@shared/domain/models/user-account';
export default interface VendorCommandRepository {
    register(publicProfile: VendorSignup): void;
    approve(publicProfile: VendorUser): void;
    findVendorSignup(id: Id): Promise<VendorSignup>;
    findVendor(id: Id): Promise<VendorUser>;
    existsUserByEmail(email: string): Promise<boolean>;
    findAccountByEmail(email: string): Promise<UserAccount>;
}
