import VendorProfile from 'src/main/vendors/authetication/domain/models/vendor-profile';
import { VendorEntity } from '../entities/vendor-entity';
export default class VendorProfileMapper {
    static toDomain(plainValues: VendorEntity): VendorProfile;
    static toPersistence(user: VendorProfile): VendorEntity;
}
