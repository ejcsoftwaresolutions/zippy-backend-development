export declare class VendorAdminEntity {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    birthday?: Date;
    state: string;
    title: string;
    contactCompanyCharge: string;
    location?: {
        latitude: number;
        longitude: number;
    };
    address: {
        country: string;
        city: string;
        state: string;
        line1: string;
        line2?: string;
    };
    logoImage?: string;
    profilePictureURL?: string;
    phone: string;
    linePhone?: string;
    rif: string;
    identificationCard: string;
    social: {
        website?: string;
        instagram: string;
    };
    deliveryMethod: string[];
    extra: {
        productTypes: string;
    };
    schedule: any;
    documents: {
        rifUrl: string;
        dniUrl: string;
        productCatalogUrl: string;
        LIAELicenseUrl: string;
        permitMedicalUrl: string;
        foodManipulationUrl: string;
    };
    withdrawalDetails?: {
        accountType: string;
        bank: string;
        accountNumber: string;
    };
    code: string;
    categoryId: string;
    createdAt: Date;
    selectedSubcategories: {
        id: string;
        name: string;
    }[];
    linkAccountToken?: string;
    salesCommissionPercentage: number;
    create(props: any): void;
}
