"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../../../../../../shared/domain/utils");
const vendor_signup_1 = require("../../../../domain/models/vendor-signup");
const vendor_admin_entity_1 = require("../entities/vendor-admin-entity");
const vendor_delivery_method_mapper_1 = require("./vendor-delivery-method-mapper");
class VendorAdminMapper {
    static toDomain(plainValues) {
        return vendor_signup_1.default.fromPrimitives({
            id: plainValues.id,
            code: plainValues.code,
            state: plainValues.state,
            basicInfo: {
                identificationCard: plainValues.identificationCard,
                email: plainValues.email,
                birthday: plainValues.birthday,
                firstName: plainValues.firstName,
                lastName: plainValues.lastName,
                phone: plainValues.phone,
                contactCompanyCharge: plainValues.contactCompanyCharge,
                linePhone: plainValues.linePhone,
            },
            deliveryMethod: vendor_delivery_method_mapper_1.default.toDomain(plainValues),
            location: plainValues.location
                ? {
                    ...plainValues.location,
                    ...utils_1.ObjectUtils.omit(plainValues.address, 'formattedAddress'),
                }
                : undefined,
            rif: plainValues.rif,
            social: {
                website: plainValues.social.website,
                instagram: plainValues.social.instagram,
            },
            categoryId: plainValues.categoryId,
            extra: plainValues.extra
                ? {
                    productTypes: plainValues.extra.productTypes,
                }
                : undefined,
            schedule: plainValues.schedule,
            shopName: plainValues.title,
            withdrawalDetails: plainValues.withdrawalDetails,
            documents: plainValues.documents,
            createdAt: plainValues.createdAt,
            selectedSubcategories: plainValues.selectedSubcategories,
            linkAccountToken: plainValues.linkAccountToken,
            salesCommissionPercentage: plainValues.salesCommissionPercentage,
        });
    }
    static toPersistence(user) {
        const primitives = user.toPrimitives();
        const entity = new vendor_admin_entity_1.VendorAdminEntity();
        entity.create(utils_1.ObjectUtils.omitUnknown({
            id: primitives.id,
            email: primitives.basicInfo.email,
            firstName: primitives.basicInfo.firstName,
            lastName: primitives.basicInfo.lastName,
            birthday: primitives.basicInfo.birthday,
            title: primitives.shopName,
            contactCompanyCharge: primitives.basicInfo.contactCompanyCharge,
            phone: primitives.basicInfo.phone,
            linePhone: primitives.basicInfo.linePhone,
            identificationCard: primitives.basicInfo.identificationCard,
            deliveryMethod: primitives.deliveryMethod
                ? vendor_delivery_method_mapper_1.default.toPersistenceFromApplication(primitives.deliveryMethod)
                : undefined,
            location: primitives.location && primitives.location.latitude
                ? {
                    ...utils_1.ObjectUtils.pick(primitives.location, [
                        'latitude',
                        'longitude',
                    ]),
                }
                : undefined,
            address: primitives.location
                ? {
                    ...utils_1.ObjectUtils.omit(primitives.location, [
                        'latitude',
                        'longitude',
                        'address',
                    ]),
                }
                : undefined,
            rif: primitives.rif,
            social: utils_1.ObjectUtils.omitUnknown(primitives.social),
            extra: primitives.extra
                ? utils_1.ObjectUtils.omitUnknown(primitives.extra)
                : undefined,
            schedule: primitives.schedule
                ? utils_1.ObjectUtils.omitUnknown(primitives.schedule)
                : undefined,
            documents: primitives.documents
                ? utils_1.ObjectUtils.omitUnknown(primitives.documents)
                : undefined,
            withdrawalDetails: primitives.withdrawalDetails
                ? utils_1.ObjectUtils.omitUnknown(primitives.withdrawalDetails)
                : undefined,
            categoryId: primitives.categoryId,
            createdAt: primitives.createdAt,
            state: primitives.state,
            code: primitives.code,
            selectedSubcategories: primitives.selectedSubcategories,
            linkAccountToken: primitives.linkAccountToken,
            salesCommissionPercentage: primitives.salesCommissionPercentage,
        }));
        return entity;
    }
}
exports.default = VendorAdminMapper;
//# sourceMappingURL=vendor-admin-mapper.js.map