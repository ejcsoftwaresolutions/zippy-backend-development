"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorAdminEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let VendorAdminEntity = class VendorAdminEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
VendorAdminEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('vendor_applications')
], VendorAdminEntity);
exports.VendorAdminEntity = VendorAdminEntity;
//# sourceMappingURL=vendor-admin-entity.js.map