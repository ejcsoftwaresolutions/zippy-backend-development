"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../../../../../shared/domain/decorators");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const nestjs_firebase_admin_1 = require("@tfarras/nestjs-firebase-admin");
const vendor_profile_1 = require("../../../../domain/models/vendor-profile");
const vendor_user_1 = require("../../../../domain/models/vendor-user");
const vendor_command_repository_1 = require("../../../../domain/repositories/vendor-command-repository");
const vendor_user_entity_1 = require("../entities/vendor-user-entity");
const vendor_admin_entity_1 = require("../entities/vendor-admin-entity");
const vendor_admin_mapper_1 = require("../mappers/vendor-admin-mapper");
const id_1 = require("../../../../../../shared/domain/id/id");
const vendor_user_mapper_1 = require("../mappers/vendor-user-mapper");
const vendor_profile_mapper_1 = require("../mappers/vendor-profile-mapper");
const vendor_entity_1 = require("../entities/vendor-entity");
const vendor_withdrawal_details_entity_1 = require("../entities/vendor-withdrawal-details-entity");
const auth_token_verificator_1 = require("../../../../../../shared/domain/authentication/auth-token-verificator");
const user_account_1 = require("../../../../../../shared/domain/models/user-account");
const user_role_1 = require("../../../../../../shared/domain/models/user-role");
let VendorCommandInfrastructureRepository = class VendorCommandInfrastructureRepository extends fireorm_repository_1.default {
    constructor(firebase, tokenVerificator) {
        super();
        this.firebase = firebase;
        this.tokenVerificator = tokenVerificator;
    }
    getEntityClass() {
        return vendor_user_entity_1.VendorUserEntity;
    }
    async register(user) {
        const entity = vendor_admin_mapper_1.default.toPersistence(user);
        await Promise.all([
            this.getEntityRepository(vendor_admin_entity_1.VendorAdminEntity).create(entity),
        ]);
    }
    async existsUserByEmail(email) {
        const data = await this.repository().whereEqualTo('email', email).findOne();
        return !!data;
    }
    async approve(publicProfile) {
        const account = vendor_user_mapper_1.default.toPersistence(publicProfile);
        const profile = vendor_profile_1.default.fromPrimitives(publicProfile.toPrimitives().profile);
        const withdrawalDetails = new vendor_withdrawal_details_entity_1.VendorWithdrawalDetailsEntity();
        const signupVendor = await this.findVendorSignupByEmail(publicProfile.email);
        if (!signupVendor) {
            throw new Error('SIGNUP_INFO_NOT_FOUND');
        }
        const signupPrimitives = signupVendor.toPrimitives();
        withdrawalDetails.create({
            id: publicProfile.id.value,
            type: 'bank',
            vendorId: publicProfile.id.value,
            details: {
                ...signupVendor.toPrimitives().withdrawalDetails,
            },
        });
        await Promise.all([
            ...(signupPrimitives.linkAccountToken
                ? [this.repository().update(account)]
                : [
                    this.firebase.auth().createUser({
                        email: publicProfile.email,
                        password: publicProfile.plainPassword,
                        uid: publicProfile.id.value,
                    }),
                    this.repository().create(account),
                ]),
            this.getEntityRepository(vendor_entity_1.VendorEntity).create(vendor_profile_mapper_1.default.toPersistence(profile)),
            this.getEntityRepository(vendor_withdrawal_details_entity_1.VendorWithdrawalDetailsEntity).create(withdrawalDetails),
        ]);
    }
    async findVendor(id) {
        const item = await this.repository().whereEqualTo('id', id.value).findOne();
        if (!item)
            return;
        const profile = await this.getEntityRepository(vendor_entity_1.VendorEntity)
            .whereEqualTo('id', id.value)
            .findOne();
        return vendor_user_mapper_1.default.toDomain({ account: item, profile: profile });
    }
    async findVendorSignup(id) {
        const item = await this.getEntityRepository(vendor_admin_entity_1.VendorAdminEntity)
            .whereEqualTo('id', id.value)
            .findOne();
        if (!item)
            return;
        return vendor_admin_mapper_1.default.toDomain(item);
    }
    async findAccountByEmail(email) {
        const data = await this.repository().whereEqualTo('email', email).findOne();
        if (!data)
            return;
        return user_account_1.default.fromPrimitives({
            id: data.id,
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName,
            phone: data.phone,
            createdAt: data.createdAt,
            hasDefaultPassword: data.hasDefaultPassword,
            newPasswordToken: data.newPasswordToken,
            roles: data.roles,
            status: data.status,
        });
    }
    async findVendorSignupByEmail(email) {
        const item = await this.getEntityRepository(vendor_admin_entity_1.VendorAdminEntity)
            .whereEqualTo('email', email)
            .findOne();
        if (!item)
            return;
        return vendor_admin_mapper_1.default.toDomain(item);
    }
};
VendorCommandInfrastructureRepository.bindingKey = 'VendorCommandRepository';
VendorCommandInfrastructureRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)(nestjs_firebase_admin_1.FIREBASE_ADMIN_INJECT)),
    tslib_1.__param(1, (0, decorators_1.inject)('user.authentication.token.verificator')),
    tslib_1.__metadata("design:paramtypes", [Object, Object])
], VendorCommandInfrastructureRepository);
exports.default = VendorCommandInfrastructureRepository;
//# sourceMappingURL=vendor-infrastructure-command-repository.js.map