"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../../../../../../shared/domain/utils");
const vendor_profile_1 = require("../../../../domain/models/vendor-profile");
const vendor_entity_1 = require("../entities/vendor-entity");
const vendor_delivery_method_mapper_1 = require("./vendor-delivery-method-mapper");
class VendorProfileMapper {
    static toDomain(plainValues) {
        return vendor_profile_1.default.fromPrimitives({
            id: plainValues.id,
            title: plainValues.title,
            isOpen: plainValues.isOpen,
            deliveryRadius: plainValues.deliveryRadius,
            rif: plainValues.rif,
            categoryId: plainValues.categoryID,
            reviews: {
                sum: plainValues.reviewsSum,
                count: plainValues.reviewsCount,
            },
            identificationCard: plainValues.identificationCard,
            location: {
                ...plainValues.location,
                ...utils_1.ObjectUtils.omit(plainValues.address, 'formattedAddress'),
            },
            logoImage: plainValues.logoImage,
            schedule: plainValues.schedule,
            status: plainValues.status,
            description: plainValues.description,
            code: plainValues.code,
            deliveryMethod: vendor_delivery_method_mapper_1.default.toDomain(plainValues),
            selectedSubcategories: plainValues.selectedSubcategories,
            linePhone: plainValues.linePhone,
            configurations: plainValues.configurations,
            salesCommissionPercentage: plainValues.salesCommissionPercentage,
            scheduledDeliveriesSchedule: plainValues.scheduledOrdersSchedule,
        });
    }
    static toPersistence(user) {
        var _a, _b, _c, _d;
        const primitives = user.toPrimitives();
        const entity = new vendor_entity_1.VendorEntity();
        entity.create(utils_1.ObjectUtils.omitUnknown({
            id: primitives.id,
            title: primitives.title,
            rif: primitives.rif,
            isOpen: primitives.isOpen,
            deliveryRadius: (_a = primitives.deliveryRadius) !== null && _a !== void 0 ? _a : 0,
            reviewsSum: (_b = primitives.reviews.sum) !== null && _b !== void 0 ? _b : 0,
            reviewsCount: (_c = primitives.reviews.count) !== null && _c !== void 0 ? _c : 0,
            identificationCard: primitives.identificationCard,
            location: primitives.location
                ? {
                    ...utils_1.ObjectUtils.pick(primitives.location, [
                        'latitude',
                        'longitude',
                    ]),
                }
                : undefined,
            address: {
                ...utils_1.ObjectUtils.omit(primitives.location, [
                    'latitude',
                    'longitude',
                    'address',
                ]),
            },
            schedule: primitives.schedule,
            categoryID: primitives.categoryId,
            description: (_d = primitives.description) !== null && _d !== void 0 ? _d : '',
            code: primitives.code,
            status: primitives.status,
            ...vendor_delivery_method_mapper_1.default.toPersistence(primitives.deliveryMethod),
            selectedSubcategories: primitives.selectedSubcategories,
            linePhone: primitives.linePhone,
            configurations: primitives.configurations,
            salesCommissionPercentage: primitives.salesCommissionPercentage,
            scheduledOrdersSchedule: primitives.scheduledDeliveriesSchedule,
        }));
        return entity;
    }
}
exports.default = VendorProfileMapper;
//# sourceMappingURL=vendor-profile-mapper.js.map