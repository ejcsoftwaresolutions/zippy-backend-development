"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("../../../../../../shared/domain/utils");
class VendorDeliveryMethodMapper {
    static toDomain(dto) {
        let methods = [];
        function toArray(item) {
            if (item == 'ALL') {
                return ['IMMEDIATELY', 'SCHEDULED', 'PICK_UP'];
            }
            return [item];
        }
        if (!!dto.deliveryMethod) {
            if (!utils_1.ArrayUtils.isArray(dto.deliveryMethod)) {
                methods = toArray(dto.deliveryMethod);
                return methods;
            }
            methods = dto.deliveryMethod;
            return methods;
        }
        if (dto.immediateDelivery) {
            methods.push('IMMEDIATELY');
        }
        if (dto.scheduleDelivery) {
            methods.push('SCHEDULED');
        }
        if (dto.pickup) {
            methods.push('PICK_UP');
        }
        return methods;
    }
    static toPersistence(types) {
        return {
            immediateDelivery: types.includes('IMMEDIATELY'),
            scheduleDelivery: types.includes('SCHEDULED'),
            pickup: types.includes('PICK_UP'),
        };
    }
    static toPersistenceFromApplication(deliveryMethod) {
        if (!utils_1.ArrayUtils.isArray(deliveryMethod)) {
            return [deliveryMethod];
        }
        return deliveryMethod;
    }
}
exports.default = VendorDeliveryMethodMapper;
//# sourceMappingURL=vendor-delivery-method-mapper.js.map