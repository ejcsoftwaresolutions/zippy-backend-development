import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin';
import { EntityConstructorOrPath } from 'fireorm';
import VendorUser from 'src/main/vendors/authetication/domain/models/vendor-user';
import VendorCommandRepository from 'src/main/vendors/authetication/domain/repositories/vendor-command-repository';
import { VendorUserEntity } from '../entities/vendor-user-entity';
import VendorSignup from '../../../../domain/models/vendor-signup';
import Id from '@shared/domain/id/id';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import UserAccount from '@shared/domain/models/user-account';
export default class VendorCommandInfrastructureRepository extends FireOrmRepository<VendorUserEntity> implements VendorCommandRepository {
    private firebase;
    private tokenVerificator;
    static readonly bindingKey = "VendorCommandRepository";
    constructor(firebase: FirebaseAdminSDK, tokenVerificator: AuthTokenVerificator<any>);
    getEntityClass(): EntityConstructorOrPath<VendorUserEntity>;
    register(user: VendorSignup): Promise<void>;
    existsUserByEmail(email: string): Promise<boolean>;
    approve(publicProfile: VendorUser): Promise<void>;
    findVendor(id: Id): Promise<VendorUser>;
    findVendorSignup(id: Id): Promise<VendorSignup>;
    findAccountByEmail(email: string): Promise<UserAccount>;
    private findVendorSignupByEmail;
}
