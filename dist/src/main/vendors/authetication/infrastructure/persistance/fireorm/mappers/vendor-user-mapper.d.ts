import VendorUser from 'src/main/vendors/authetication/domain/models/vendor-user';
import { VendorEntity } from '../entities/vendor-entity';
import { VendorUserEntity } from '../entities/vendor-user-entity';
export default class VendorUserMapper {
    static toDomain(plainValues: {
        account: VendorUserEntity;
        profile: VendorEntity;
    }): VendorUser;
    static toPersistence(user: VendorUser): VendorUserEntity;
}
