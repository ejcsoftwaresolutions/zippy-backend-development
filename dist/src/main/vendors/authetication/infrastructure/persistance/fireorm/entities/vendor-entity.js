"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let VendorEntity = class VendorEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
VendorEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('vendors')
], VendorEntity);
exports.VendorEntity = VendorEntity;
//# sourceMappingURL=vendor-entity.js.map