export declare class VendorUserEntity {
    id: string;
    firstName?: string;
    lastName?: string;
    birthday?: Date;
    email: string;
    phone?: string;
    createdAt: Date;
    status: string;
    hasDefaultPassword: boolean;
    newPasswordToken?: string;
    roles: string[];
    create(props: any): void;
}
