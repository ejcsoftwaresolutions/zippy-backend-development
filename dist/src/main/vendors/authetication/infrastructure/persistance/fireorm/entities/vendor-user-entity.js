"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorUserEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let VendorUserEntity = class VendorUserEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
VendorUserEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('users')
], VendorUserEntity);
exports.VendorUserEntity = VendorUserEntity;
//# sourceMappingURL=vendor-user-entity.js.map