export declare class VendorWithdrawalDetailsEntity {
    id: string;
    vendorId: string;
    type: string;
    details: any;
    create(props: any): void;
}
