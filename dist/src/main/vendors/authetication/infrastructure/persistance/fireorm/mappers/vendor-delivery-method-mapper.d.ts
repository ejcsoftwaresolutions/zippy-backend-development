export default class VendorDeliveryMethodMapper {
    static toDomain(dto: any): string[];
    static toPersistence(types: string[]): any;
    static toPersistenceFromApplication(deliveryMethod: any): any;
}
