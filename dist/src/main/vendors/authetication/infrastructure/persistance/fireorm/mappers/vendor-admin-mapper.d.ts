import VendorSignup from '../../../../domain/models/vendor-signup';
import { VendorAdminEntity } from '../entities/vendor-admin-entity';
export default class VendorAdminMapper {
    static toDomain(plainValues: VendorAdminEntity): VendorSignup;
    static toPersistence(user: VendorSignup): VendorAdminEntity;
}
