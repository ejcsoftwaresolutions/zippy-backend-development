"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VendorWithdrawalDetailsEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
const utils_1 = require("../../../../../../shared/domain/utils");
let VendorWithdrawalDetailsEntity = class VendorWithdrawalDetailsEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
VendorWithdrawalDetailsEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('vendor_withdrawal_details')
], VendorWithdrawalDetailsEntity);
exports.VendorWithdrawalDetailsEntity = VendorWithdrawalDetailsEntity;
//# sourceMappingURL=vendor-withdrawal-details-entity.js.map