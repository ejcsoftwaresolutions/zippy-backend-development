"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vendor_user_1 = require("../../../../domain/models/vendor-user");
const vendor_user_entity_1 = require("../entities/vendor-user-entity");
const vendor_profile_mapper_1 = require("./vendor-profile-mapper");
const user_account_1 = require("../../../../../../shared/domain/models/user-account");
const user_role_1 = require("../../../../../../shared/domain/models/user-role");
class VendorUserMapper {
    static toDomain(plainValues) {
        const { account, profile } = plainValues;
        return vendor_user_1.default.fromPrimitives({
            id: account.id,
            account: user_account_1.default.fromPrimitives({
                id: account.id,
                email: account.email,
                firstName: account.firstName,
                lastName: account.lastName,
                birthday: account.birthday,
                phone: account.phone,
                createdAt: account.createdAt,
                hasDefaultPassword: account.hasDefaultPassword,
                newPasswordToken: account.newPasswordToken,
                roles: account.roles,
                status: account.status,
            }).toPrimitives(),
            profile: vendor_profile_mapper_1.default.toDomain(profile).toPrimitives(),
        });
    }
    static toPersistence(user) {
        const primitives = user.toPrimitives();
        const entity = new vendor_user_entity_1.VendorUserEntity();
        entity.create({
            id: primitives.id,
            email: primitives.account.email,
            firstName: primitives.account.firstName,
            lastName: primitives.account.lastName,
            phone: primitives.account.phone,
            birthday: primitives.account.birthday,
            createdAt: primitives.account.createdAt,
            hasDefaultPassword: primitives.account.hasDefaultPassword,
            newPasswordToken: primitives.account.newPasswordToken,
            roles: primitives.account.roles,
            status: primitives.account.status,
        });
        return entity;
    }
}
exports.default = VendorUserMapper;
//# sourceMappingURL=vendor-user-mapper.js.map