import DomainEvent, { DomainEventProps } from '@shared/domain/bus/event/domain-event';
interface EventProps extends DomainEventProps {
    eventData: {};
}
export default class RiderCreatedDomainEvent extends DomainEvent<EventProps> {
    static eventName: string;
    eventName(): string;
}
export {};
