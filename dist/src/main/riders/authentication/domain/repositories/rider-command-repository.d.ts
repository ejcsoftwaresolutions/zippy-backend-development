import RiderSignup from '../models/rider-signup';
import Id from '@shared/domain/id/id';
import RiderUser from '../models/rider-user';
import UserAccount from '@shared/domain/models/user-account';
export default interface RiderCommandRepository {
    register(publicProfile: RiderSignup): void;
    approve(publicProfile: RiderUser): void;
    existsUserByEmail(email: string): Promise<boolean>;
    findRiderSignup(id: Id): Promise<RiderSignup>;
    findRider(id: Id): Promise<RiderUser>;
    findAccountByEmail(email: string): Promise<UserAccount>;
}
