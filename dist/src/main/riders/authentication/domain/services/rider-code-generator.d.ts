import CodeGenerator from '@shared/domain/utils/code-generator';
export default class RiderCodeGenerator {
    private codeGenerator;
    constructor(codeGenerator: CodeGenerator);
    generate(params: {
        firstName: string;
        lastName: string;
    }): string;
}
