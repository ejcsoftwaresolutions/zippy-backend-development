"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const code_generator_1 = require("../../../../shared/domain/utils/code-generator");
class RiderReferralCodeGenerator {
    constructor(codeGenerator) {
        this.codeGenerator = codeGenerator;
    }
    generate(params) {
        return `${params.firstName.substring(0, 1).toUpperCase()}${params.lastName
            .substring(0, 1)
            .toUpperCase()}${this.codeGenerator.generate(4)}`;
    }
}
exports.default = RiderReferralCodeGenerator;
//# sourceMappingURL=rider-referral-code-generator.js.map