import CodeGenerator from '@shared/domain/utils/code-generator';
export default class RiderReferralCodeGenerator {
    private codeGenerator;
    constructor(codeGenerator: CodeGenerator);
    generate(params: {
        firstName: string;
        lastName: string;
    }): string;
}
