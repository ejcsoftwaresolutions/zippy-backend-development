"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const code_generator_1 = require("../../../../shared/domain/utils/code-generator");
class RiderCodeGenerator {
    constructor(codeGenerator) {
        this.codeGenerator = codeGenerator;
    }
    generate(params) {
        const firstNamePart = params.firstName.substring(0, 3).toUpperCase();
        const lastNamePart = params.lastName.substring(0, 3).toUpperCase();
        const randomNumbers = this.codeGenerator.generate(4);
        return `${firstNamePart}${lastNamePart}${randomNumbers}`;
    }
}
exports.default = RiderCodeGenerator;
//# sourceMappingURL=rider-code-generator.js.map