import { UploadFile } from '@shared/domain/storage/storage-types';
import FileUploader from '@shared/domain/storage/storage-uploader';
import { FileUploadResponse } from '@shared/domain/storage/file-upload-response';
export default class RiderDocumentsUploader {
    private fileUploader;
    static readonly uploadPath = "riders";
    constructor(fileUploader: FileUploader);
    uploadDocuments(riderId: string, files: {
        file: UploadFile;
        key: string;
    }[]): Promise<FileUploadResponse[]>;
}
