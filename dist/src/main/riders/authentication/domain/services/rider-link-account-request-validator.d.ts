import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
export default class RiderLinkAccountRequestValidator {
    private tokenVerificator;
    constructor(tokenVerificator: AuthTokenVerificator<any>);
    validate(token: string): Promise<any>;
}
