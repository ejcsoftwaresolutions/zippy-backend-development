"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const utils_1 = require("../../../../shared/domain/utils");
const rider_created_domain_event_1 = require("../events/rider-created-domain-event");
const DefaultProps = {
    isActive: false,
    availableSchedule: 'FULL_TIME',
    hasDefaultPassword: true,
};
class RiderProfile extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get email() {
        return this.props.basicInfo.email;
    }
    get rif() {
        return this.props.basicInfo.rif;
    }
    get firstName() {
        return this.props.basicInfo.firstName;
    }
    get lastName() {
        return this.props.basicInfo.lastName;
    }
    get phone() {
        return this.props.basicInfo.phone;
    }
    get referralCode() {
        return this.props.referralCode;
    }
    get referrerCode() {
        return this.props.referrerCode;
    }
    get identificationCard() {
        return this.props.basicInfo.identificationCard;
    }
    static create(data) {
        const newItem = new RiderProfile(data);
        newItem.record(new rider_created_domain_event_1.default({
            aggregateId: data.id.value,
            occurredOn: new Date(),
            eventData: {},
        }));
        return newItem;
    }
    static fromPrimitives({ ...plainData }) {
        return new RiderProfile({
            ...plainData,
            id: new id_1.default(plainData.id),
        });
    }
    update(updates) {
        Object.assign(this, {
            ...this.props,
            ...updates,
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = RiderProfile;
//# sourceMappingURL=rider-profile.js.map