import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import { RiderProfileProps } from './rider-profile';
export type RiderSignupDocuments = {
    personalReferencesUrl: string[];
    driverLicenseFrontUrl: string;
    driverLicenseBackUrl: string;
    medicalCertificateUrl: string;
    RCVpolicyUrl: string;
    criminalRecordUrl: string;
    rifUrl?: string;
    dniUrl: string;
    drivingPermitUrl: string;
};
export type RiderSignupVehicle = {
    type: string;
    hasBag: boolean;
    year: string;
    model: string;
    comments: string;
};
export interface RiderSignupProps extends Omit<RiderProfileProps, 'rating' | 'badgeCount' | 'configurations' | 'isActive' | 'plainPassword' | 'status' | 'newPasswordToken' | 'hasDefaultPassword'> {
    documents: RiderSignupDocuments;
    vehicle: RiderSignupVehicle;
    deliveryRegion: {
        state: string;
        city: string;
    };
    availableSchedule: string;
    withdrawalDetails: {
        accountType: string;
        bank: string;
        accountNumber: string;
    };
    state: string;
    linkAccountToken?: string;
    createdAt: Date;
}
export interface RiderSignupPrimitiveProps extends Omit<RiderSignupProps, 'id'> {
    id: string;
}
export default class RiderSignup extends AggregateRoot<RiderSignupProps> {
    constructor(props: RiderSignupProps);
    get id(): Id;
    get code(): string;
    get rif(): string;
    get birthday(): Date;
    get linkAccountToken(): string;
    get email(): string;
    get firstName(): string;
    get lastName(): string;
    get phone(): string;
    get vehicleType(): string;
    get referralCode(): string;
    get referrerCode(): string;
    get deliveryRegion(): {
        state: string;
        city: string;
    };
    get homeAddress(): import("./rider-profile").RiderHomeAddress;
    get createdAt(): Date;
    get identificationCard(): string;
    static create(data: RiderSignupProps): RiderSignup;
    static fromPrimitives({ ...plainData }: RiderSignupPrimitiveProps): RiderSignup;
    toPrimitives(): RiderSignupPrimitiveProps;
}
