"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const aggregate_root_1 = require("../../../../shared/domain/aggregate/aggregate-root");
const id_1 = require("../../../../shared/domain/id/id");
const utils_1 = require("../../../../shared/domain/utils");
const rider_registered_domain_event_1 = require("../events/rider-registered-domain-event");
const DefaultProps = {
    availableSchedule: 'FULL_TIME',
};
class RiderSignup extends aggregate_root_1.default {
    constructor(props) {
        super({
            ...DefaultProps,
            ...props,
        });
    }
    get id() {
        return this.props.id;
    }
    get code() {
        return this.props.code;
    }
    get rif() {
        return this.props.basicInfo.rif;
    }
    get birthday() {
        return this.props.basicInfo.birthday;
    }
    get linkAccountToken() {
        return this.props.linkAccountToken;
    }
    get email() {
        return this.props.basicInfo.email;
    }
    get firstName() {
        return this.props.basicInfo.firstName;
    }
    get lastName() {
        return this.props.basicInfo.lastName;
    }
    get phone() {
        return this.props.basicInfo.phone;
    }
    get vehicleType() {
        return this.props.vehicle.type;
    }
    get referralCode() {
        return this.props.referralCode;
    }
    get referrerCode() {
        return this.props.referrerCode;
    }
    get deliveryRegion() {
        return this.props.deliveryRegion;
    }
    get homeAddress() {
        return this.props.basicInfo.homeAddress;
    }
    get createdAt() {
        return this.props.createdAt;
    }
    get identificationCard() {
        return this.props.basicInfo.identificationCard;
    }
    static create(data) {
        const newItem = new RiderSignup(data);
        newItem.record(new rider_registered_domain_event_1.default({
            aggregateId: data.id.value,
            occurredOn: new Date(),
            eventData: {},
        }));
        return newItem;
    }
    static fromPrimitives({ ...plainData }) {
        return new RiderSignup({
            ...plainData,
            id: new id_1.default(plainData.id),
        });
    }
    toPrimitives() {
        const json = super.toJson();
        return utils_1.ObjectUtils.omitUnknown(json);
    }
}
exports.default = RiderSignup;
//# sourceMappingURL=rider-signup.js.map