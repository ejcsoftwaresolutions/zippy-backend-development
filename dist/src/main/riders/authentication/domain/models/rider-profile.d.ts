import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
export type RiderHomeAddress = {
    line1: string;
    line2?: string;
    city: string;
    state: string;
    country: string;
    address: string;
    latitude?: number;
    longitude?: number;
};
export type RiderBasicInfo = {
    identificationCard: string;
    email: string;
    firstName?: string;
    lastName?: string;
    phone?: string;
    birthday?: Date;
    homeAddress?: RiderHomeAddress;
    rif: string;
};
export interface RiderProfileProps {
    id: Id;
    code: string;
    status: string;
    basicInfo: RiderBasicInfo;
    referralCode: string;
    referrerCode?: string;
    deliveryMethod: string;
    deliveryRegion: {
        state: string;
        city: string;
    };
    badgeCount: number;
    configurations: {
        referralPromotions: boolean;
        receiveEmails: boolean;
        receivePushNotifications: boolean;
        receiveSMS: boolean;
    };
}
export interface RiderProfilePrimitiveProps extends Omit<RiderProfileProps, 'id'> {
    id: string;
}
export default class RiderProfile extends AggregateRoot<RiderProfileProps> {
    constructor(props: RiderProfileProps);
    get id(): Id;
    get email(): string;
    get rif(): string;
    get firstName(): string;
    get lastName(): string;
    get phone(): string;
    get referralCode(): string;
    get referrerCode(): string;
    get identificationCard(): string;
    static create(data: RiderProfileProps): RiderProfile;
    static fromPrimitives({ ...plainData }: RiderProfilePrimitiveProps): RiderProfile;
    update(updates: Partial<RiderProfileProps>): void;
    toPrimitives(): RiderProfilePrimitiveProps;
}
