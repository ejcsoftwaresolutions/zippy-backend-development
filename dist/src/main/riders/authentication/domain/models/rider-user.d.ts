import AggregateRoot from '@shared/domain/aggregate/aggregate-root';
import Id from '@shared/domain/id/id';
import UserAccount, { UserAccountPrimitiveProps } from '@shared/domain/models/user-account';
import RiderProfile, { RiderProfilePrimitiveProps } from './rider-profile';
export interface RiderUserProps {
    id: Id;
    account: UserAccount;
    profile: RiderProfile;
}
export interface RiderUserPrimitiveProps {
    id: string;
    account: UserAccountPrimitiveProps;
    profile: RiderProfilePrimitiveProps;
}
export default class RiderUser extends AggregateRoot<RiderUserProps> {
    constructor(props: RiderUserProps);
    get id(): Id;
    get email(): string;
    get firstName(): string;
    get lastName(): string;
    get phone(): string;
    get plainPassword(): string;
    get createdAt(): Date;
    static create(data: RiderUserProps): RiderUser;
    static fromPrimitives({ ...plainData }: RiderUserPrimitiveProps): RiderUser;
    update(updates: Partial<RiderUserProps>): void;
    toPrimitives(): RiderUserPrimitiveProps;
}
