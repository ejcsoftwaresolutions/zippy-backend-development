export declare class RiderAdminEntity {
    id: string;
    firstName?: string;
    lastName?: string;
    state: string;
    profilePictureURL?: string;
    email: string;
    phone?: string;
    identificationCard?: string;
    referralCode?: string;
    referrerCode?: string;
    deliveryMethod: string;
    birthday?: Date;
    homeAddress: {
        country: string;
        city: string;
        state: string;
        line1: string;
        line2?: string;
    };
    availableSchedule: string;
    rif: string;
    documents: {
        personalReferencesUrl: string[];
        driverLicenseFrontUrl: string;
        driverLicenseBackUrl: string;
        medicalCertificateUrl: string;
        RCVpolicyUrl: string;
        criminalRecordUrl: string;
        rifUrl?: string;
        dniUrl: string;
        drivingPermitUrl: string;
    };
    vehicle: {
        type: string;
        hasBag: boolean;
        year: string;
        model: string;
        comments: string;
    };
    deliveryRegion: {
        state: string;
        city: string;
    };
    withdrawalDetails: {
        accountType: string;
        bank: string;
        accountNumber: string;
    };
    code: string;
    createdAt: Date;
    linkAccountToken?: string;
    create(props: any): void;
}
