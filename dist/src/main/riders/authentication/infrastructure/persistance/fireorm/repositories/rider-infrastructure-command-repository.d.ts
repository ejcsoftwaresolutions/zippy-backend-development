import FireOrmRepository from '@shared/infrastructure/persistence/fireorm/fireorm-repository';
import { FirebaseAdminSDK } from '@tfarras/nestjs-firebase-admin';
import { EntityConstructorOrPath } from 'fireorm';
import RiderCommandRepository from 'src/main/riders/authentication/domain/repositories/rider-command-repository';
import RiderSignup from '../../../../domain/models/rider-signup';
import Id from '@shared/domain/id/id';
import RiderUser from '../../../../domain/models/rider-user';
import UserAccount from '@shared/domain/models/user-account';
import { RiderUserEntity } from '../entities/rider-user-entity';
export default class RiderCommandInfrastructureRepository extends FireOrmRepository<RiderUserEntity> implements RiderCommandRepository {
    private firebase;
    static readonly bindingKey = "RiderCommandRepository";
    constructor(firebase: FirebaseAdminSDK);
    getEntityClass(): EntityConstructorOrPath<RiderUserEntity>;
    register(user: RiderSignup): Promise<void>;
    existsUserByEmail(email: string): Promise<boolean>;
    approve(publicProfile: RiderUser): Promise<void>;
    findRider(id: Id): Promise<RiderUser>;
    findRiderSignup(id: Id): Promise<RiderSignup>;
    findAccountByEmail(email: string): Promise<UserAccount>;
    private findRiderSignupByEmail;
}
