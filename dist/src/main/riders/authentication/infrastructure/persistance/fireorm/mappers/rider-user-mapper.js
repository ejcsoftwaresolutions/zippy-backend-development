"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_account_1 = require("../../../../../../shared/domain/models/user-account");
const user_role_1 = require("../../../../../../shared/domain/models/user-role");
const rider_user_entity_1 = require("../entities/rider-user-entity");
const rider_profile_mapper_1 = require("./rider-profile-mapper");
const rider_user_1 = require("../../../../domain/models/rider-user");
const utils_1 = require("../../../../../../shared/domain/utils");
class RiderUserMapper {
    static toDomain(plainValues) {
        const { account, profile } = plainValues;
        return rider_user_1.default.fromPrimitives({
            id: account.id,
            account: user_account_1.default.fromPrimitives({
                id: account.id,
                email: account.email,
                firstName: account.firstName,
                lastName: account.lastName,
                phone: account.phone,
                createdAt: account.createdAt,
                birthday: account.birthday,
                hasDefaultPassword: account.hasDefaultPassword,
                newPasswordToken: account.newPasswordToken,
                roles: account.roles,
                status: account.status,
            }).toPrimitives(),
            profile: rider_profile_mapper_1.default.toDomain(profile).toPrimitives(),
        });
    }
    static toPersistence(user) {
        const primitives = user.toPrimitives();
        const entity = new rider_user_entity_1.RiderUserEntity();
        entity.create(utils_1.ObjectUtils.omitUnknown({
            id: primitives.id,
            email: primitives.account.email,
            firstName: primitives.account.firstName,
            lastName: primitives.account.lastName,
            phone: primitives.account.phone,
            createdAt: primitives.account.createdAt,
            hasDefaultPassword: primitives.account.hasDefaultPassword,
            newPasswordToken: primitives.account.newPasswordToken,
            roles: primitives.account.roles,
            status: primitives.account.status,
            birthday: primitives.account.birthday,
        }));
        return entity;
    }
}
exports.default = RiderUserMapper;
//# sourceMappingURL=rider-user-mapper.js.map