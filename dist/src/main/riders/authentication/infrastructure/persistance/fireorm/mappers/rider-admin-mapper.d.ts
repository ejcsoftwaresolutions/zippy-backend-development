import { RiderAdminEntity } from '../entities/rider-admin-entity';
import RiderSignup from '../../../../domain/models/rider-signup';
export default class RiderAdminMapper {
    static toDomain(plainValues: RiderAdminEntity): RiderSignup;
    static toPersistence(user: RiderSignup): RiderAdminEntity;
}
