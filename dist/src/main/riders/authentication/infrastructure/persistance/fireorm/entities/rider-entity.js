"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let RiderEntity = class RiderEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
RiderEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('riders')
], RiderEntity);
exports.RiderEntity = RiderEntity;
//# sourceMappingURL=rider-entity.js.map