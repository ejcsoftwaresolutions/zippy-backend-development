import RiderProfile from 'src/main/riders/authentication/domain/models/rider-profile';
import { RiderEntity } from '../entities/rider-entity';
export default class RiderProfileMapper {
    static toDomain(plainValues: RiderEntity): RiderProfile;
    static toPersistence(user: RiderProfile): RiderEntity;
}
