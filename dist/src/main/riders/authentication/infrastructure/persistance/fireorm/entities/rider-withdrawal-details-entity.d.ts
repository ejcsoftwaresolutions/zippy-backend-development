export declare class RiderWithdrawalDetailsEntity {
    id: string;
    driverId: string;
    type: string;
    details: any;
    create(props: any): void;
}
