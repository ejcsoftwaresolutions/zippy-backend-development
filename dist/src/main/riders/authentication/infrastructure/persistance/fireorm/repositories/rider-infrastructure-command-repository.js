"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const decorators_1 = require("../../../../../../shared/domain/decorators");
const fireorm_repository_1 = require("../../../../../../shared/infrastructure/persistence/fireorm/fireorm-repository");
const nestjs_firebase_admin_1 = require("@tfarras/nestjs-firebase-admin");
const rider_command_repository_1 = require("../../../../domain/repositories/rider-command-repository");
const rider_admin_entity_1 = require("../entities/rider-admin-entity");
const rider_admin_mapper_1 = require("../mappers/rider-admin-mapper");
const id_1 = require("../../../../../../shared/domain/id/id");
const rider_withdrawal_details_entity_1 = require("../entities/rider-withdrawal-details-entity");
const rider_user_mapper_1 = require("../mappers/rider-user-mapper");
const user_account_1 = require("../../../../../../shared/domain/models/user-account");
const user_role_1 = require("../../../../../../shared/domain/models/user-role");
const rider_user_entity_1 = require("../entities/rider-user-entity");
const rider_entity_1 = require("../entities/rider-entity");
const rider_profile_1 = require("../../../../domain/models/rider-profile");
const rider_profile_mapper_1 = require("../mappers/rider-profile-mapper");
let RiderCommandInfrastructureRepository = class RiderCommandInfrastructureRepository extends fireorm_repository_1.default {
    constructor(firebase) {
        super();
        this.firebase = firebase;
    }
    getEntityClass() {
        return rider_user_entity_1.RiderUserEntity;
    }
    async register(user) {
        const entity = rider_admin_mapper_1.default.toPersistence(user);
        await Promise.all([
            this.getEntityRepository(rider_admin_entity_1.RiderAdminEntity).create(entity),
        ]);
    }
    async existsUserByEmail(email) {
        const data = await this.getEntityRepository(rider_admin_entity_1.RiderAdminEntity)
            .whereEqualTo('email', email)
            .findOne();
        return !!data;
    }
    async approve(publicProfile) {
        const account = rider_user_mapper_1.default.toPersistence(publicProfile);
        const profile = rider_profile_1.default.fromPrimitives(publicProfile.toPrimitives().profile);
        const withdrawalDetails = new rider_withdrawal_details_entity_1.RiderWithdrawalDetailsEntity();
        const signupRider = await this.findRiderSignupByEmail(publicProfile.email);
        if (!signupRider) {
            throw new Error('SIGNUP_INFO_NOT_FOUND');
        }
        const signupPrimitives = signupRider.toPrimitives();
        withdrawalDetails.create({
            id: account.id,
            type: 'bank',
            driverId: account.id,
            details: {
                ...signupPrimitives.withdrawalDetails,
            },
        });
        await Promise.all([
            ...(signupPrimitives.linkAccountToken
                ? [this.repository().update(account)]
                : [
                    this.firebase.auth().createUser({
                        email: publicProfile.email,
                        password: publicProfile.plainPassword,
                        uid: publicProfile.id.value,
                    }),
                    this.repository().create(account),
                ]),
            this.getEntityRepository(rider_entity_1.RiderEntity).create(rider_profile_mapper_1.default.toPersistence(profile)),
            this.getEntityRepository(rider_withdrawal_details_entity_1.RiderWithdrawalDetailsEntity).create(withdrawalDetails),
        ]);
    }
    async findRider(id) {
        const item = await this.repository().whereEqualTo('id', id.value).findOne();
        if (!item)
            return;
        const profile = await this.getEntityRepository(rider_entity_1.RiderEntity)
            .whereEqualTo('id', id.value)
            .findOne();
        return rider_user_mapper_1.default.toDomain({ account: item, profile: profile });
    }
    async findRiderSignup(id) {
        const item = await this.getEntityRepository(rider_admin_entity_1.RiderAdminEntity)
            .whereEqualTo('id', id.value)
            .findOne();
        if (!item)
            return;
        return rider_admin_mapper_1.default.toDomain(item);
    }
    async findAccountByEmail(email) {
        const data = await this.repository().whereEqualTo('email', email).findOne();
        if (!data)
            return;
        return user_account_1.default.fromPrimitives({
            id: data.id,
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName,
            phone: data.phone,
            createdAt: data.createdAt,
            hasDefaultPassword: data.hasDefaultPassword,
            newPasswordToken: data.newPasswordToken,
            roles: data.roles,
            status: data.status,
        });
    }
    async findRiderSignupByEmail(email) {
        const item = await this.getEntityRepository(rider_admin_entity_1.RiderAdminEntity)
            .whereEqualTo('email', email)
            .findOne();
        if (!item)
            return;
        return rider_admin_mapper_1.default.toDomain(item);
    }
};
RiderCommandInfrastructureRepository.bindingKey = 'RiderCommandRepository';
RiderCommandInfrastructureRepository = tslib_1.__decorate([
    tslib_1.__param(0, (0, decorators_1.inject)(nestjs_firebase_admin_1.FIREBASE_ADMIN_INJECT)),
    tslib_1.__metadata("design:paramtypes", [Object])
], RiderCommandInfrastructureRepository);
exports.default = RiderCommandInfrastructureRepository;
//# sourceMappingURL=rider-infrastructure-command-repository.js.map