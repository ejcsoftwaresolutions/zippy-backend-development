"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rider_profile_1 = require("../../../../domain/models/rider-profile");
const rider_entity_1 = require("../entities/rider-entity");
const utils_1 = require("../../../../../../shared/domain/utils");
class RiderProfileMapper {
    static toDomain(plainValues) {
        return rider_profile_1.default.fromPrimitives({
            id: plainValues.id,
            basicInfo: {
                rif: plainValues.rif,
                email: plainValues.email,
                firstName: plainValues.firstName,
                lastName: plainValues.lastName,
                phone: plainValues.phone,
                identificationCard: plainValues.identificationCard,
                homeAddress: {
                    ...plainValues.homeAddress,
                    ...utils_1.ObjectUtils.omit(plainValues.homeAddress, 'formattedAddress'),
                },
            },
            referralCode: plainValues.referralCode,
            referrerCode: plainValues.referrerCode,
            badgeCount: plainValues.badgeCount,
            configurations: plainValues.configurations,
            deliveryMethod: plainValues.deliveryMethod,
            status: plainValues.status,
            deliveryRegion: plainValues.deliveryRegion,
            code: plainValues.code,
        });
    }
    static toPersistence(user) {
        const primitives = user.toPrimitives();
        const entity = new rider_entity_1.RiderEntity();
        entity.create(utils_1.ObjectUtils.omitUnknown({
            id: primitives.id,
            email: primitives.basicInfo.email,
            firstName: primitives.basicInfo.firstName,
            lastName: primitives.basicInfo.lastName,
            phone: primitives.basicInfo.phone,
            identificationCard: primitives.basicInfo.identificationCard,
            referralCode: primitives.referralCode,
            referrerCode: primitives.referrerCode,
            badgeCount: primitives.badgeCount,
            configurations: primitives.configurations,
            deliveryMethod: primitives.deliveryMethod,
            homeAddress: {
                ...utils_1.ObjectUtils.omit(primitives.basicInfo.homeAddress, [
                    'latitude',
                    'longitude',
                    'address',
                ]),
            },
            rif: primitives.basicInfo.rif,
            status: primitives.status,
            deliveryRegion: primitives.deliveryRegion,
            code: primitives.code,
        }));
        return entity;
    }
}
exports.default = RiderProfileMapper;
//# sourceMappingURL=rider-profile-mapper.js.map