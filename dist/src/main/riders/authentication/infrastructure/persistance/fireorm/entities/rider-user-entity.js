"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderUserEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let RiderUserEntity = class RiderUserEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
RiderUserEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('users')
], RiderUserEntity);
exports.RiderUserEntity = RiderUserEntity;
//# sourceMappingURL=rider-user-entity.js.map