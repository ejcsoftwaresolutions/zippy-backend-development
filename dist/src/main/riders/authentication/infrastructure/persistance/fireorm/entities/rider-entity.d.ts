export declare class RiderEntity {
    id: string;
    firstName?: string;
    lastName?: string;
    email: string;
    phone?: string;
    identificationCard?: string;
    referralCode?: string;
    referrerCode?: string;
    homeAddress: {
        country: string;
        city: string;
        state: string;
        line1: string;
        line2?: string;
    };
    badgeCount: number;
    rif: string;
    configurations: {
        referralPromotions: boolean;
        receiveEmails: boolean;
        receivePushNotifications: boolean;
        receiveSMS: boolean;
    };
    deliveryRegion: {
        state: string;
        city: string;
    };
    deliveryMethod: string;
    code: string;
    status: string;
    create(props: any): void;
}
