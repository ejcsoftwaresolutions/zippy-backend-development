"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderWithdrawalDetailsEntity = void 0;
const tslib_1 = require("tslib");
const fireorm_1 = require("fireorm");
const utils_1 = require("../../../../../../shared/domain/utils");
let RiderWithdrawalDetailsEntity = class RiderWithdrawalDetailsEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
RiderWithdrawalDetailsEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('driver_withdrawal_details')
], RiderWithdrawalDetailsEntity);
exports.RiderWithdrawalDetailsEntity = RiderWithdrawalDetailsEntity;
//# sourceMappingURL=rider-withdrawal-details-entity.js.map