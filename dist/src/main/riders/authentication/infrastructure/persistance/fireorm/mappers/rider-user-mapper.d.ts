import { RiderUserEntity } from '../entities/rider-user-entity';
import { RiderEntity } from '../entities/rider-entity';
import RiderUser from '../../../../domain/models/rider-user';
export default class RiderUserMapper {
    static toDomain(plainValues: {
        account: RiderUserEntity;
        profile: RiderEntity;
    }): RiderUser;
    static toPersistence(user: RiderUser): RiderUserEntity;
}
