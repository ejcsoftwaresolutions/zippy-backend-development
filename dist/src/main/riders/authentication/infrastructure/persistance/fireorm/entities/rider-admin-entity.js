"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RiderAdminEntity = void 0;
const tslib_1 = require("tslib");
const utils_1 = require("../../../../../../shared/domain/utils");
const fireorm_1 = require("fireorm");
let RiderAdminEntity = class RiderAdminEntity {
    create(props) {
        Object.assign(this, utils_1.ObjectUtils.omitUnknown(props));
    }
};
RiderAdminEntity = tslib_1.__decorate([
    (0, fireorm_1.Collection)('rider_applications')
], RiderAdminEntity);
exports.RiderAdminEntity = RiderAdminEntity;
//# sourceMappingURL=rider-admin-entity.js.map