export declare class RiderUserEntity {
    id: string;
    firstName?: string;
    lastName?: string;
    email: string;
    phone?: string;
    createdAt: Date;
    status: string;
    hasDefaultPassword: boolean;
    newPasswordToken?: string;
    roles: string[];
    birthday: Date;
    create(props: any): void;
}
