"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rider_admin_entity_1 = require("../entities/rider-admin-entity");
const rider_signup_1 = require("../../../../domain/models/rider-signup");
const utils_1 = require("../../../../../../shared/domain/utils");
class RiderAdminMapper {
    static toDomain(plainValues) {
        return rider_signup_1.default.fromPrimitives({
            id: plainValues.id,
            state: plainValues.state,
            basicInfo: {
                rif: plainValues.rif,
                birthday: plainValues.birthday,
                email: plainValues.email,
                firstName: plainValues.firstName,
                lastName: plainValues.lastName,
                phone: plainValues.phone,
                identificationCard: plainValues.identificationCard,
                homeAddress: {
                    ...plainValues.homeAddress,
                    ...utils_1.ObjectUtils.omit(plainValues.homeAddress, 'formattedAddress'),
                },
            },
            referralCode: plainValues.referralCode,
            referrerCode: plainValues.referrerCode,
            deliveryMethod: plainValues.deliveryMethod,
            createdAt: plainValues.createdAt,
            availableSchedule: plainValues.availableSchedule,
            deliveryRegion: plainValues.deliveryRegion,
            documents: plainValues.documents,
            vehicle: plainValues.vehicle,
            withdrawalDetails: plainValues.withdrawalDetails,
            code: plainValues.code,
            linkAccountToken: plainValues.linkAccountToken,
        });
    }
    static toPersistence(user) {
        const primitives = user.toPrimitives();
        const entity = new rider_admin_entity_1.RiderAdminEntity();
        entity.create(utils_1.ObjectUtils.omitUnknown({
            id: primitives.id,
            email: primitives.basicInfo.email,
            firstName: primitives.basicInfo.firstName,
            lastName: primitives.basicInfo.lastName,
            phone: primitives.basicInfo.phone,
            birthday: primitives.basicInfo.birthday,
            identificationCard: primitives.basicInfo.identificationCard,
            homeAddress: {
                ...utils_1.ObjectUtils.omit(primitives.basicInfo.homeAddress, [
                    'latitude',
                    'longitude',
                    'address',
                ]),
            },
            referralCode: primitives.referralCode,
            referrerCode: primitives.referrerCode,
            deliveryMethod: primitives.deliveryMethod,
            availableSchedule: primitives.availableSchedule,
            deliveryRegion: primitives.deliveryRegion,
            documents: utils_1.ObjectUtils.omitUnknown(primitives.documents),
            vehicle: utils_1.ObjectUtils.omitUnknown(primitives.vehicle),
            createdAt: primitives.createdAt,
            withdrawalDetails: utils_1.ObjectUtils.omitUnknown(primitives.withdrawalDetails),
            rif: primitives.basicInfo.rif,
            state: primitives.state,
            code: primitives.code,
            linkAccountToken: primitives.linkAccountToken,
        }));
        return entity;
    }
}
exports.default = RiderAdminMapper;
//# sourceMappingURL=rider-admin-mapper.js.map