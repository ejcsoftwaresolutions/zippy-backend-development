"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const code_generator_1 = require("../../../../shared/domain/utils/code-generator");
const rider_referral_code_generator_1 = require("../../domain/services/rider-referral-code-generator");
const register_rider_command_1 = require("./register-rider-command");
const register_rider_use_case_1 = require("./register-rider-use-case");
const rider_documents_uploader_1 = require("../../domain/services/rider-documents-uploader");
const storage_uploader_1 = require("../../../../shared/domain/storage/storage-uploader");
const decorators_1 = require("../../../../shared/domain/decorators");
const rider_code_generator_1 = require("../../domain/services/rider-code-generator");
let RegisterRiderCommandHandler = class RegisterRiderCommandHandler {
    constructor(useCase, codeGenerator, fileUploader) {
        this.useCase = useCase;
        this.riderReferralCodeGenerator = new rider_referral_code_generator_1.default(codeGenerator);
        this.riderCodeGenerator = new rider_code_generator_1.default(codeGenerator);
        this.documentsUploader = new rider_documents_uploader_1.default(fileUploader);
    }
    getCommandName() {
        return register_rider_command_1.default.name;
    }
    async handle(command) {
        const id = new id_1.default(command.registerInfo.id);
        const { registerInfo } = command;
        const referralCode = this.riderReferralCodeGenerator.generate({
            firstName: registerInfo.basicInfo.firstName,
            lastName: registerInfo.basicInfo.lastName,
        });
        const code = this.riderCodeGenerator.generate({
            firstName: registerInfo.basicInfo.firstName,
            lastName: registerInfo.basicInfo.lastName,
        });
        const filesUrls = command.registerInfo.documents;
        const vehicle = {
            comments: registerInfo.vehicle.comments,
            hasBag: registerInfo.vehicle.hasBag,
            year: registerInfo.vehicle.year,
            type: registerInfo.vehicle.type,
            model: registerInfo.vehicle.model,
        };
        const documents = {
            RCVpolicyUrl: filesUrls['RCVpolicyUrl'],
            dniUrl: filesUrls['dniUrl'],
            criminalRecordUrl: filesUrls['criminalRecordUrl'],
            driverLicenseBackUrl: filesUrls['driverLicenseBackUrl'],
            driverLicenseFrontUrl: filesUrls['driverLicenseFrontUrl'],
            medicalCertificateUrl: filesUrls['medicalCertificateUrl'],
            personalReferencesUrl: filesUrls['personalReferencesUrl'],
            rifUrl: filesUrls['rifUrl'],
            drivingPermitUrl: filesUrls['drivingPermitUrl'],
        };
        const basicInfo = registerInfo.basicInfo;
        await this.useCase.execute({
            id: id,
            code: code,
            basicInfo: basicInfo,
            vehicle: vehicle,
            deliveryMethod: registerInfo.vehicle.type,
            documents: documents,
            withdrawalDetails: registerInfo.withdrawalDetails,
            deliveryRegion: registerInfo.deliveryRegion,
            referrerCode: registerInfo.referrerCode,
            referralCode: referralCode,
            availableSchedule: registerInfo.availableSchedule,
            linkAccountToken: registerInfo.linkAccountToken,
        });
        return {
            id: id.value,
        };
    }
};
RegisterRiderCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('RegisterRiderUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('code.generator')),
    tslib_1.__param(2, (0, decorators_1.inject)('file.uploader')),
    tslib_1.__metadata("design:paramtypes", [register_rider_use_case_1.default, Object, Object])
], RegisterRiderCommandHandler);
exports.default = RegisterRiderCommandHandler;
//# sourceMappingURL=register-rider-command-handler.js.map