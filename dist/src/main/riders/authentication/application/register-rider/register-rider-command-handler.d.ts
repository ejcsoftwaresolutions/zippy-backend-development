import CommandHandler from '@shared/domain/bus/command/command-handler';
import CodeGenerator from '@shared/domain/utils/code-generator';
import RegisterRiderCommand from './register-rider-command';
import RegisterRiderUseCase from './register-rider-use-case';
import FileUploader from '@shared/domain/storage/storage-uploader';
export default class RegisterRiderCommandHandler implements CommandHandler {
    private useCase;
    private riderReferralCodeGenerator;
    private riderCodeGenerator;
    private documentsUploader;
    constructor(useCase: RegisterRiderUseCase, codeGenerator: CodeGenerator, fileUploader: FileUploader);
    getCommandName(): string;
    handle(command: RegisterRiderCommand): Promise<{
        id: string;
    }>;
}
