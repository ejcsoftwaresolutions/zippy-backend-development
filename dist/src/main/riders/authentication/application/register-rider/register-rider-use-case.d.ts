import EventBus from '@shared/domain/bus/event/event-bus';
import Id from '@shared/domain/id/id';
import RiderCommandRepository from '../../domain/repositories/rider-command-repository';
import { RiderSignupDocuments, RiderSignupVehicle } from '../../domain/models/rider-signup';
import { RiderBasicInfo } from '../../domain/models/rider-profile';
interface Params {
    id: Id;
    basicInfo: RiderBasicInfo;
    referrerCode: string;
    referralCode: string;
    documents: RiderSignupDocuments;
    vehicle: RiderSignupVehicle;
    deliveryRegion: {
        state: string;
        city: string;
    };
    deliveryMethod: string;
    withdrawalDetails: {
        accountType: string;
        bank: string;
        accountNumber: string;
    };
    availableSchedule: string;
    code: string;
    linkAccountToken?: string;
}
export default class RegisterRiderUseCase {
    private eventBus;
    private riderRepository;
    constructor(eventBus: EventBus, riderRepository: RiderCommandRepository);
    execute(data: Params): Promise<void>;
}
export {};
