"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const rider_signup_1 = require("../../domain/models/rider-signup");
const decorators_1 = require("../../../../shared/domain/decorators");
let RegisterRiderUseCase = class RegisterRiderUseCase {
    constructor(eventBus, riderRepository) {
        this.eventBus = eventBus;
        this.riderRepository = riderRepository;
    }
    async execute(data) {
        const foundUser = await this.riderRepository.existsUserByEmail(data.basicInfo.email);
        if (!data.linkAccountToken && foundUser) {
            throw new Error('user_already_exists');
        }
        const item = rider_signup_1.default.create({
            id: data.id,
            code: data.code,
            basicInfo: data.basicInfo,
            createdAt: new Date(),
            referralCode: data.referralCode,
            referrerCode: data.referrerCode,
            deliveryMethod: data.deliveryMethod,
            availableSchedule: data.availableSchedule,
            deliveryRegion: {
                city: data.deliveryRegion.city,
                state: data.deliveryRegion.state,
            },
            documents: data.documents,
            vehicle: data.vehicle,
            withdrawalDetails: {
                accountNumber: data.withdrawalDetails.accountNumber,
                accountType: data.withdrawalDetails.accountType,
                bank: data.withdrawalDetails.bank,
            },
            state: 'NEW',
            linkAccountToken: data.linkAccountToken,
        });
        await this.riderRepository.register(item);
        this.eventBus.publish(item.pullDomainEvents());
    }
};
RegisterRiderUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('event.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('RiderCommandRepository')),
    tslib_1.__metadata("design:paramtypes", [Object, Object])
], RegisterRiderUseCase);
exports.default = RegisterRiderUseCase;
//# sourceMappingURL=register-rider-use-case.js.map