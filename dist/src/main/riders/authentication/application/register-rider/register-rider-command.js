"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class RegisterRiderCommand extends command_1.default {
    constructor(registerInfo) {
        super();
        this.registerInfo = registerInfo;
    }
    name() {
        return RegisterRiderCommand.name;
    }
}
exports.default = RegisterRiderCommand;
//# sourceMappingURL=register-rider-command.js.map