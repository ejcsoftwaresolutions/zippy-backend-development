"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const command_handler_1 = require("../../../../shared/domain/bus/command/command-handler");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const code_generator_1 = require("../../../../shared/domain/utils/code-generator");
const approve_rider_command_1 = require("./approve-rider-command");
const approve_rider_use_case_1 = require("./approve-rider-use-case");
const auth_token_verificator_1 = require("../../../../shared/domain/authentication/auth-token-verificator");
const rider_link_account_request_validator_1 = require("../../domain/services/rider-link-account-request-validator");
const user_account_1 = require("../../../../shared/domain/models/user-account");
const rider_profile_1 = require("../../domain/models/rider-profile");
const auth_token_creator_1 = require("../../../../shared/domain/authentication/auth-token-creator");
const rider_code_generator_1 = require("../../domain/services/rider-code-generator");
let ApproveRiderCommandHandler = class ApproveRiderCommandHandler {
    constructor(useCase, codeGenerator, repository, tokenVerificator, tokenCreator) {
        this.useCase = useCase;
        this.repository = repository;
        this.tokenVerificator = tokenVerificator;
        this.tokenCreator = tokenCreator;
        this.uniqueCodeGenerator = new rider_code_generator_1.default(codeGenerator);
        this.linkAccountValidator = new rider_link_account_request_validator_1.default(tokenVerificator);
    }
    getCommandName() {
        return approve_rider_command_1.default.name;
    }
    async handle(command) {
        const { approveInfo } = command;
        const id = new id_1.default(approveInfo.id);
        const signupData = await this.repository.findRiderSignup(id);
        const foundAccount = await this.repository.findAccountByEmail(signupData.email);
        const code = this.uniqueCodeGenerator.generate({
            firstName: signupData.firstName,
            lastName: signupData.lastName,
        });
        const approvedAccount = await (async () => {
            if (foundAccount) {
                await this.linkAccountValidator.validate(signupData.linkAccountToken);
                foundAccount.addRole('RIDER');
                return foundAccount;
            }
            const newPassToken = await this.tokenCreator.generate({
                id: id.value,
                userType: 'RIDER',
            });
            return user_account_1.default.fromPrimitives({
                id: id.value,
                email: signupData.email,
                firstName: signupData.firstName,
                lastName: signupData.lastName,
                phone: signupData.phone,
                plainPassword: signupData.identificationCard,
                roles: ['RIDER'],
                birthday: signupData.birthday,
                hasDefaultPassword: true,
                createdAt: new Date(),
                newPasswordToken: newPassToken,
                status: 'ACTIVE',
            });
        })();
        const profile = rider_profile_1.default.fromPrimitives({
            id: approvedAccount.id.value,
            code: code,
            referralCode: code,
            status: 'ACTIVE',
            badgeCount: 0,
            deliveryMethod: signupData.vehicleType,
            referrerCode: signupData.referrerCode,
            deliveryRegion: signupData.deliveryRegion,
            basicInfo: {
                rif: signupData.rif,
                email: signupData.email,
                firstName: signupData.firstName,
                lastName: signupData.lastName,
                phone: signupData.phone,
                homeAddress: signupData.homeAddress,
                identificationCard: signupData.identificationCard,
            },
            configurations: {
                referralPromotions: true,
                receivePushNotifications: true,
                receiveSMS: true,
                receiveEmails: true,
            },
        });
        await this.useCase.execute({
            id: approvedAccount.id,
            account: approvedAccount,
            profile: profile,
        });
        return {
            id: approvedAccount.id.value,
        };
    }
};
ApproveRiderCommandHandler = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('ApproveRiderUseCase')),
    tslib_1.__param(1, (0, decorators_1.inject)('code.generator')),
    tslib_1.__param(2, (0, decorators_1.inject)('RiderCommandRepository')),
    tslib_1.__param(3, (0, decorators_1.inject)('user.authentication.token.verificator')),
    tslib_1.__param(4, (0, decorators_1.inject)('user.authentication.token.creator')),
    tslib_1.__metadata("design:paramtypes", [approve_rider_use_case_1.default, Object, Object, Object, Object])
], ApproveRiderCommandHandler);
exports.default = ApproveRiderCommandHandler;
//# sourceMappingURL=approve-rider-command-handler.js.map