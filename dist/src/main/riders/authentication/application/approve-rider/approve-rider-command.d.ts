import Command from '@shared/domain/bus/command/command';
interface ApproveInfo {
    id: string;
}
export default class ApproveRiderCommand extends Command {
    readonly approveInfo: ApproveInfo;
    constructor(approveInfo: ApproveInfo);
    name(): string;
}
export {};
