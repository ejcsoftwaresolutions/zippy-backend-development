import CommandHandler from '@shared/domain/bus/command/command-handler';
import CodeGenerator from '@shared/domain/utils/code-generator';
import ApproveRiderCommand from './approve-rider-command';
import ApproveRiderUseCase from './approve-rider-use-case';
import RiderCommandRepository from '../../domain/repositories/rider-command-repository';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
export default class ApproveRiderCommandHandler implements CommandHandler {
    private useCase;
    private repository;
    private tokenVerificator;
    private tokenCreator;
    private uniqueCodeGenerator;
    private linkAccountValidator;
    constructor(useCase: ApproveRiderUseCase, codeGenerator: CodeGenerator, repository: RiderCommandRepository, tokenVerificator: AuthTokenVerificator<any>, tokenCreator: AuthTokenCreator<any>);
    getCommandName(): string;
    handle(command: ApproveRiderCommand): Promise<{
        id: string;
    }>;
}
