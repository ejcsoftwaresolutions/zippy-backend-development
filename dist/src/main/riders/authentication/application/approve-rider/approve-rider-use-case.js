"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const event_bus_1 = require("../../../../shared/domain/bus/event/event-bus");
const decorators_1 = require("../../../../shared/domain/decorators");
const service_1 = require("../../../../shared/domain/decorators/service");
const id_1 = require("../../../../shared/domain/id/id");
const auth_token_creator_1 = require("../../../../shared/domain/authentication/auth-token-creator");
const user_account_1 = require("../../../../shared/domain/models/user-account");
const rider_user_1 = require("../../domain/models/rider-user");
let ApproveRiderUseCase = class ApproveRiderUseCase {
    constructor(eventBus, repo, tokenCreator) {
        this.eventBus = eventBus;
        this.repo = repo;
        this.tokenCreator = tokenCreator;
    }
    async execute(data) {
        const user = rider_user_1.default.create({
            id: data.id,
            account: data.account,
            profile: data.profile,
        });
        await this.repo.approve(user);
        this.eventBus.publish(user.pullDomainEvents());
    }
};
ApproveRiderUseCase = tslib_1.__decorate([
    (0, service_1.default)(),
    tslib_1.__param(0, (0, decorators_1.inject)('event.bus')),
    tslib_1.__param(1, (0, decorators_1.inject)('RiderCommandRepository')),
    tslib_1.__param(2, (0, decorators_1.inject)('user.authentication.token.creator')),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Object])
], ApproveRiderUseCase);
exports.default = ApproveRiderUseCase;
//# sourceMappingURL=approve-rider-use-case.js.map