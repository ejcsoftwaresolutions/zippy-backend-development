"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const command_1 = require("../../../../shared/domain/bus/command/command");
class ApproveRiderCommand extends command_1.default {
    constructor(approveInfo) {
        super();
        this.approveInfo = approveInfo;
    }
    name() {
        return ApproveRiderCommand.name;
    }
}
exports.default = ApproveRiderCommand;
//# sourceMappingURL=approve-rider-command.js.map