import EventBus from '@shared/domain/bus/event/event-bus';
import Id from '@shared/domain/id/id';
import RiderCommandRepository from '../../domain/repositories/rider-command-repository';
import RiderProfile from '../../domain/models/rider-profile';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import UserAccount from '@shared/domain/models/user-account';
export default class ApproveRiderUseCase {
    private eventBus;
    private repo;
    private tokenCreator;
    constructor(eventBus: EventBus, repo: RiderCommandRepository, tokenCreator: AuthTokenCreator<any>);
    execute(data: {
        id: Id;
        account: UserAccount;
        profile: RiderProfile;
    }): Promise<void>;
}
