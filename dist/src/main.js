"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const platform_fastify_1 = require("@nestjs/platform-fastify");
const swagger_1 = require("@nestjs/swagger");
const fastify_multer_1 = require("fastify-multer");
require("module-alias/register");
const app_module_1 = require("./main/shared/infrastructure/nestjs/modules/app-module");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule, new platform_fastify_1.FastifyAdapter({
        logger: false,
        bodyLimit: 1000000000,
    }), {
        abortOnError: false,
        logger: ['error'],
        cors: true,
    });
    app.register(fastify_multer_1.contentParser);
    if (process.env.NODE_ENV !== 'production') {
        const config = new swagger_1.DocumentBuilder()
            .setTitle('Zippi Market API')
            .setDescription('Zippi Market API Documentation')
            .setVersion('1.0')
            .addBearerAuth()
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, config);
        swagger_1.SwaggerModule.setup('api', app, document);
    }
    await app.listen(process.env.PORT || 3000, '0.0.0.0');
    process.on('uncaughtException', function (err) {
        console.log(err);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map