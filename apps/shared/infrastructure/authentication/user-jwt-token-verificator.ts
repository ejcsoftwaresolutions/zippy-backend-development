import { UnauthorizedException } from '@nestjs/common';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import { inject } from '@shared/domain/decorators';
import { JWTBaseVerificator } from '@shared/infrastructure/authentication/jwt-base-verificator';
import AuthenticatedUser, {
  AuthenticatedUserPrimitiveProps,
} from '../../dto/authenticated-user';
import jwt_decode from 'jwt-decode';

export default class UserJWTVerificator
  extends JWTBaseVerificator
  implements AuthTokenVerificator<AuthenticatedUser>
{
  constructor(
    @inject('authentication.token.secret')
    jwtSecret: string,
  ) {
    super(jwtSecret);
  }

  public async verify(params: any): Promise<AuthenticatedUser> {
    if (!params.userType) {
      throw new UnauthorizedException('INVALID_TOKEN');
    }

    const user = await this.searchUser(params);

    if (!user) {
      throw new UnauthorizedException('INVALID_USER');
    }

    return AuthenticatedUser.fromPrimitives(user);
  }

  decodeToken(token: string): Promise<any> {
    return jwt_decode(token);
  }

  private async searchUser(
    params: any,
  ): Promise<AuthenticatedUserPrimitiveProps> {
    throw new Error('Not implemented');
  }
}
