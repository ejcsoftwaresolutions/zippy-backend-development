import SocialMediaProfile from '@shared/infrastructure/authentication/social-media-profile';
import axios from 'axios';

export default async function getFacebookProfile(
  accessToken: string,
): Promise<SocialMediaProfile | null> {
  const response = await axios.get('https://graph.facebook.com/v9.0/me/', {
    params: {
      access_token: accessToken,
      fields: 'id,first_name,last_name,email,picture.width(640)',
      redirect: false,
    },
  });

  const profile = response.data;

  return {
    id: profile.id,
    firstName: profile.first_name,
    lastName: profile.last_name,
    fullName: undefined,
    pictureUrl: profile.picture.data.url,
    email: profile.email,
  };
}
