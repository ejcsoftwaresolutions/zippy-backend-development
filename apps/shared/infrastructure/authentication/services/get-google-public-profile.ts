import SocialMediaProfile from '@shared/infrastructure/authentication/social-media-profile';
import axios from 'axios';

export default async function getGooglePublicProfile(
  accessToken: string,
): Promise<SocialMediaProfile | null> {
  const response = await axios.get(
    'https://www.googleapis.com/oauth2/v1/userinfo',
    {
      /* @ts-ignore */
      params: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        // eslint-disable-next-line @typescript-eslint/camelcase
        alt: 'json',
        access_token: accessToken,
      },
    },
  );

  const profile = response.data;

  return {
    id: profile.id,
    email: profile.email,
    firstName: profile.given_name,
    lastName: profile.family_name,
    fullName: undefined,
    pictureUrl: profile.picture + '-s640', // size 640*640
  };
}
