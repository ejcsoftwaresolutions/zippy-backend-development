import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import TestDeliveryRequestsCommandRepository from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';
import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { inject } from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import PdfGenerator from '@shared/domain/pdf/pdf-generator';
import ExcelGenerator from '@shared/domain/excel/excel-generator';
import { ConfigService } from '@nestjs/config';

import firebase from 'firebase-admin';
import { PushNotification } from '../../../../functions/src/utils/models/push-notification';
import Id from '@shared/domain/id/id';
import { createBulkNotificationService } from '../../../../functions/src/utils/bulk-notifications/bulk-notification-service';
import { createNotificationService } from '../../../../functions/src/utils/notification-service/notification-service';
import PushNotificator from '../../../../functions/src/utils/notification-service/push-notificator';
import BulkNotificator from '../../../../functions/src/utils/bulk-notifications/bulk-notificator';

class NotificationDto {
  @ApiProperty()
  userId: string;

  @ApiProperty()
  role: string;

  @ApiProperty()
  notificationData: any;
}

@ApiTags('Test')
@Controller({
  path: 'test',
})
export class TestController extends ApiController {
  private notificationService: PushNotificator;
  private bulkNotificationService: BulkNotificator;

  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
    @inject('DeliveryRequestCommandRepository')
    private deliveryRequestRepository: DeliveryRequestCommandRepository,
    @inject('TestDeliveryRequestsCommandRepository')
    private testDeliveryRequestsRepository: TestDeliveryRequestsCommandRepository,
    @inject('pdf.generator')
    private pdfGenerator: PdfGenerator,
    @inject('excel.generator')
    private excelGenerator: ExcelGenerator,
    private configService: ConfigService,
  ) {
    super(commandBus, queryBus, multipartHandler);

    this.notificationService = createNotificationService();

    this.bulkNotificationService = createBulkNotificationService(
      this.notificationService,
    );
  }

  @Get('ping')
  async ping() {
    try {
      return {
        ok: true,
        x: true,
      };
    } catch (error) {
      return {
        ok: false,
        error: error.message,
      };
    }
  }

  @Post('test-notifications')
  @ApiOperation({
    summary: 'test',
    description:
      '{\n' +
      '        "userId": "string",\n' +
      '        "role": "VENDOR or RIDER or CUSTOMER",\n' +
      '        "notificationData": {\n' +
      '          "title": "Hola",\n' +
      '          "content": "Hola"\n' +
      '        }\n' +
      '      }',
  })
  async test(@Body() data: NotificationDto) {
    const appId = (() => {
      if (data.role.toUpperCase() === 'CUSTOMER') {
        return this.configService.get<string>('CUSTOMER_APP_EXPERIENCE_ID');
      }

      if (data.role.toUpperCase() === 'VENDOR') {
        return this.configService.get<string>('STORE_APP_EXPERIENCE_ID');
      }

      return this.configService.get<string>('RIDER_APP_EXPERIENCE_ID');
    })();

    const userToken = await (async () => {
      const tokens = (
        await firebase
          .firestore()
          .collection('user_push_tokens')
          .doc(data.userId)
          .get()
      ).data();

      if (data.role.toUpperCase() === 'CUSTOMER') {
        return tokens?.customer;
      }

      if (data.role.toUpperCase() === 'VENDOR') {
        return tokens?.vendor;
      }

      return tokens?.rider;
    })();

    if (!userToken)
      return {
        ok: false,
        message: 'Token not found',
      };

    const channel = (appId as string).split('/')[1];

    try {
      const res = await this.notificationService.send(
        new PushNotification({
          id: new Id().value,
          to: userToken,
          /*   metadata: {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             isExternalId: true,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           },*/
          title: 'Tap the notification, Mike :)',
          content: 'This is a product link now :)',
          channelId: channel,
          appId: appId,
          linkUrl: 'zippi-market://v/LAL043974',
        }),
      );

      console.log(res);
      return {
        res: res,
        ok: true,
      };
    } catch (e) {
      return {
        res: e.message,
        ok: false,
      };
    }
  }

  @Post('test-bulk-notifications')
  async testBulk() {
    try {
      const promises = [
        {
          id: 'message-id',
          content: {
            title: 'Hola [[FIRST_NAME]]',
            description: 'Hola desc',
          },
          type: 'CAMPAIGN',
          targetGroup: 'CUSTOMER',
          /* targetRecipients: [
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         id: 'tyEZ8f0FVFWquKx0WCrc1xxFyFk1',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         firstName: 'Ed',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         lastName: 'Car',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         email: 'edu@gm.com',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         token: '09c58ef4-7316-44d0-ac72-cd8369441b4c',
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       },
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ],*/
        },
      ].map((reqData: any) => {
        return new Promise(async (resolve) => {
          const message = {
            id: reqData.id,
            title: reqData.content.title,
            description: reqData.content.description,
            imageUrl:
              reqData.content.attachments &&
              reqData.content.attachments.length > 0
                ? reqData.content.attachments[0].url
                : undefined,
          };

          const results =
            reqData.type === 'INDIVIDUAL'
              ? await this.bulkNotificationService.sendBulkIndividualNotification(
                  reqData.targetGroup,
                  message,
                  reqData.targetRecipients,
                )
              : await this.bulkNotificationService.sendCampaignNotification(
                  reqData.targetGroup,
                  message,
                );

          resolve({
            id: reqData.id,
            results: results,
          });
        });
      });
      const resultsPerMessage: any = await Promise.all(promises);

      console.log(resultsPerMessage[0].results);

      return {
        ok: true,
      };
    } catch (e) {
      return {
        ok: false,
        message: e.message,
      };
    }
  }
}
