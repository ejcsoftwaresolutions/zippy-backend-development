import DeliveryRequestCommandRepository from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import TestDeliveryRequestsCommandRepository from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';

import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { inject } from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import AcceptDeliveryRequestCommand from '@deliveryService/rider/application/accept-delivery-request/accept-delivery-request-command';
import RejectDeliveryRequestCommand from '@deliveryService/rider/application/reject-delivery-request/reject-delivery-request-command';
import AssignDeliveryOrderCommand from '@deliveryService/rider/application/assign-delivery-order/assign-delivery-order-command';

class DeliveryRequestDriverResponseDto {
  @ApiProperty()
  driverId: string;
  @ApiProperty()
  requestId: string;
}

@ApiTags('Rider')
@Controller({
  path: 'rider/delivery',
})
export class RiderServiceController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
    @inject('DeliveryRequestCommandRepository')
    private deliveryRequestRepository: DeliveryRequestCommandRepository,
    @inject('TestDeliveryRequestsCommandRepository')
    private testDeliveryRequestsRepository: TestDeliveryRequestsCommandRepository,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('request/accept')
  @ApiOperation({
    summary: 'Accept a delivery order request',
  })
  async acceptRequest(@Body() body: DeliveryRequestDriverResponseDto) {
    try {
      await this.dispatch(
        new AcceptDeliveryRequestCommand(body.driverId, body.requestId),
      );

      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }

  @Post('request/assign')
  @ApiOperation({
    summary: 'Assign a delivery order request to a rider',
  })
  async assignRequest(@Body() body: DeliveryRequestDriverResponseDto) {
    try {
      await this.dispatch(
        new AssignDeliveryOrderCommand(body.driverId, body.requestId),
      );

      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }

  @Post('request/reject')
  @ApiOperation({
    summary: 'Reject a delivery order request (Discard current candidate)',
  })
  async rejectRequest(@Body() body: DeliveryRequestDriverResponseDto) {
    try {
      await this.dispatch(
        new RejectDeliveryRequestCommand(body.driverId, body.requestId),
      );

      const res = new Promise((resolve) => {
        setTimeout(() => {
          resolve({});
        }, 2000);
      });

      await res;

      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }
}
