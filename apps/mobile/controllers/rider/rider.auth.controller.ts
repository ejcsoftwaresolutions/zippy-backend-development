import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import { Body, Controller, Post } from '@nestjs/common';
import RegisterRiderCommand from '../../../../src/main/riders/authentication/application/register-rider/register-rider-command';
import { inject } from '@shared/domain/decorators';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';

class RiderRegisterDto {
  id: string;
  @ApiProperty()
  basicInfo: {
    identificationCard: string;
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    birthday?: string;
    rif: string;
    homeAddress: {
      formattedAddress: string;
      line1: string;
      line2: string;
      city: string;
      address: string;
      state: string;
      country: string;
      latitude: number;
      longitude: number;
    };
  };

  @ApiProperty()
  referrerCode?: string;

  @ApiProperty()
  documents: {
    personalReferencesUrl: string[];
    driverLicenseFrontUrl: string;
    driverLicenseBackUrl: string;
    medicalCertificateUrl: string;
    RCVpolicyUrl: string;
    criminalRecordUrl: string;
    rifUrl?: string;
    dniUrl: string;
    drivingPermitUrl: string;
  };

  @ApiProperty()
  vehicle: {
    type: string;
    hasBag: boolean;
    year: string;
    model: string;
    comments: string;
  };

  @ApiProperty()
  deliveryRegion: {
    state: string;
    city: string;
  };

  @ApiProperty()
  withdrawalDetails: {
    accountType: string;
    bank: string;
    accountNumber: string;
  };

  @ApiProperty()
  availableSchedule: string;

  @ApiProperty()
  linkAccountToken?: string;
}

@ApiTags('Rider')
@Controller({
  path: 'rider/auth',
})
export class RiderAuthController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('register')
  @ApiOperation({
    summary: 'Register a new rider',
  })
  async register(@Body() body: RiderRegisterDto) {
    try {
      const files = {
        drivingPermitUrl: body.documents.drivingPermitUrl,
        personalReferencesUrl: body.documents.personalReferencesUrl,
        driverLicenseFrontUrl: body.documents.driverLicenseFrontUrl,
        driverLicenseBackUrl: body.documents.driverLicenseBackUrl,
        medicalCertificateUrl: body.documents.medicalCertificateUrl,
        RCVpolicyUrl: body.documents.RCVpolicyUrl,
        criminalRecordUrl: body.documents.criminalRecordUrl,
        rifUrl: body.documents.rifUrl,
        dniUrl: body.documents.dniUrl,
      };

      const res = await this.dispatch(
        new RegisterRiderCommand({
          id: body.id,
          basicInfo: {
            ...body.basicInfo,
            email: body.basicInfo.email.toLowerCase(),
            birthday: body.basicInfo.birthday
              ? new Date(body.basicInfo.birthday)
              : undefined,
            homeAddress: {
              longitude: body.basicInfo.homeAddress.longitude,
              latitude: body.basicInfo.homeAddress.latitude,
              line1: body.basicInfo.homeAddress.line1,
              line2: body.basicInfo.homeAddress.line2,
              address: body.basicInfo.homeAddress.formattedAddress,
              state: body.basicInfo.homeAddress.state,
              city: body.basicInfo.homeAddress.city,
              country: body.basicInfo.homeAddress.country,
            },
          },
          referrerCode: body.referrerCode,
          availableSchedule: body.availableSchedule,
          withdrawalDetails: body.withdrawalDetails,
          deliveryRegion: body.deliveryRegion,
          vehicle: {
            type: body.vehicle.type,
            model: body.vehicle.model,
            comments: body.vehicle.comments,
            year: body.vehicle.year,
            hasBag: body.vehicle.hasBag,
          },
          documents: {
            ...files,
          },
          linkAccountToken: body.linkAccountToken,
        }),
      );

      return {
        id: res.id,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }
}
