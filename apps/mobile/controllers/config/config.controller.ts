import ServiceConfigurations, {
    ServiceConfigurationsPrimitiveProps,
} from '@deliveryService/delivery/domain/models/service-configurations-model';
import DeliveryRequestCommandRepository
    from '@deliveryService/delivery/domain/repositories/delivery-request-command-repository';
import TestDeliveryRequestsCommandRepository
    from '@deliveryService/delivery/domain/repositories/test-delivery-requests-command-repository';
import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {ApiOperation, ApiProperty, ApiTags} from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import {inject} from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import {ObjectUtils} from '@shared/domain/utils';
import ApiController from '@shared/infrastructure/controller/api-controller';
import firebase from 'firebase-admin';
import AuthTokenCreator from '@shared/domain/authentication/auth-token-creator';
import {ConfigService} from '@nestjs/config';
import axios from 'axios';
import AuthTokenVerificator from '@shared/domain/authentication/auth-token-verificator';
import MobilePushNotificationService from '@shared/infrastructure/push-notifications';

const FeeSchema = {
    type: 'object', properties: {
        baseFee: {
            type: 'object', properties: {
                amount: {
                    type: 'number',
                }, currency: {
                    type: 'string',
                },
            },
        }, durationFee: {
            type: 'object', properties: {
                amount: {
                    type: 'number',
                }, currency: {
                    type: 'string',
                },
            },
        }, kmFee: {
            type: 'object', properties: {
                amount: {
                    type: 'number',
                }, currency: {
                    type: 'string',
                },
            },
        }, distanceRange: {
            type: 'object', properties: {
                min: {
                    type: 'number',
                },
            },
        },
    },
};
const FeeConfigurationsSchema = {
    type: 'object', properties: {
        CAR: {
            type: 'array', items: FeeSchema,
        }, MOTOR_CYCLE: {
            type: 'array', items: FeeSchema,
        }, BIKE: {
            type: 'array', items: FeeSchema,
        },
    },
};

const SearchConfigSchema = {
    type: 'object', properties: {
        acceptanceTimeout: {
            type: 'number',
        }, attemptMaxCandidates: {
            type: 'number',
        }, attemptSearchRadius: {
            type: 'number',
        }, maxAttempts: {
            type: 'number',
        },
    },
};

const ConfigSchema = {
    type: 'object', properties: {
        feeConfigurations: FeeConfigurationsSchema, searchConfigurations: SearchConfigSchema,
    },
};

class ConfigurationsDto {
    @ApiProperty({...ConfigSchema}) config: ServiceConfigurationsPrimitiveProps;
}

class CheckNewPassTokenDto {
    @ApiProperty() token: string;
}

class CreateNewPassDto {
    @ApiProperty() token: string;

    @ApiProperty() password: string;
}

class AccountLinkVerifyDto {
    @ApiProperty() email: string;

    @ApiProperty() password: string;

    @ApiProperty() newRole: string;
}

class TransformApnsTokenDto {
    @ApiProperty() token: string;

    @ApiProperty() application: string;
}

@ApiTags('Configurations') @Controller({
    path: 'configurations',
})
export class ConfigController extends ApiController {
    constructor(@inject('command.bus') commandBus: CommandBus, @inject('query.bus') queryBus: QueryBus, @inject('multipart.handler') multipartHandler: MultipartHandler<Request, Response>, @inject('DeliveryRequestCommandRepository') private deliveryRequestRepository: DeliveryRequestCommandRepository, @inject('TestDeliveryRequestsCommandRepository') private testDeliveryRequestsRepository: TestDeliveryRequestsCommandRepository, @inject('user.authentication.token.creator') private tokenCreator: AuthTokenCreator<any>, @inject('user.authentication.token.verificator') private tokenVerificator: AuthTokenVerificator<any>, private configService: ConfigService, @inject('push.notification.service') private pushNotificationService: MobilePushNotificationService,) {
        super(commandBus, queryBus, multipartHandler);
    }

    @Post('fixture') @ApiOperation({
        summary: 'Saves default delivery service configurations', description: `{
    EXAMPLE:
      feeConfigurations: {
        CAR: [
          {
            baseFee: {
              amount: 5,
              currency: '$',
            },
            distanceRange: {
              min: 0,
            },
            durationFee: {
              amount: 0.5,
              currency: '$',
            },
            kmFee: {
              amount: 0.5,
              currency: '$',
            },
          },
        ],
        MOTOR_CYCLE: [
          {
            baseFee: {
              amount: 2,
              currency: '$',
            },
            distanceRange: {
              min: 0,
            },
            durationFee: {
              amount: 0.2,
              currency: '$',
            },
            kmFee: {
              amount: 0.2,
              currency: '$',
            },
          },
        ],
        BIKE: [
          {
            baseFee: {
              amount: 1,
              currency: '$',
            },
            distanceRange: {
              min: 0,
            },
            durationFee: {
              amount: 0.1,
              currency: '$',
            },
            kmFee: {
              amount: 0.1,
              currency: '$',
            },
          },
        ],
      },
      searchConfigurations: {
        acceptanceTimeout: 60,
        attemptMaxCandidates: 5,
        attemptSearchRadius: 1,
        maxAttempts: 3,
      },
    }`,
    })
    async saveServiceConfig(): Promise<any> {
        const defaultServiceConfig = ServiceConfigurations.fromPrimitives({
            feeConfigurations: {
                CAR: [{
                    baseFee: {
                        amount: 1, currency: '$',
                    }, distanceRange: {
                        min: 0,
                    }, durationFee: {
                        amount: 0, currency: '$',
                    }, kmFee: {
                        amount: 0.48, currency: '$',
                    },
                },], MOTOR_CYCLE: [{
                    baseFee: {
                        amount: 1, currency: '$',
                    }, distanceRange: {
                        min: 0,
                    }, durationFee: {
                        amount: 0, currency: '$',
                    }, kmFee: {
                        amount: 0.48, currency: '$',
                    },
                },], BIKE: [{
                    baseFee: {
                        amount: 1, currency: '$',
                    }, distanceRange: {
                        min: 0,
                    }, durationFee: {
                        amount: 0, currency: '$',
                    }, kmFee: {
                        amount: 0.48, currency: '$',
                    },
                },],
            }, searchConfigurations: {
                acceptanceTimeout: 60, attemptMaxCandidates: 5, attemptSearchRadius: 10, maxAttempts: 3,
            },
        });

        try {
            await firebase
            .firestore()
            .doc('app_configurations/RIDER_DELIVERY_CONFIGURATIONS')
            .set(ObjectUtils.omitUnknown(defaultServiceConfig.toPrimitives()));
        } catch (error) {
            console.log(error);
            return {
                ok: false,
            };
        }

        return {
            ok: true,
        };
    }

    @Post('save') @ApiOperation({
        summary: 'Updates delivery service configurations', description: `Send part of the updates. For example: {
      "config": {
        "searchConfigurations": {
          "attemptSearchRadius": 2
        }
      }
    } to increase attempt search radius`,
    })
    async saveConfig(@Body() body: ConfigurationsDto): Promise<any> {
        const defaultServiceConfig = ServiceConfigurations.fromPrimitives(body.config,);

        const current = await firebase
        .firestore()
        .doc('app_configurations/RIDER_DELIVERY_CONFIGURATIONS')
        .get();

        if (!current.exists) {
            return {
                ok: false,
            };
        }

        await firebase
        .firestore()
        .doc('app_configurations/RIDER_DELIVERY_CONFIGURATIONS')
        .update(ObjectUtils.omitUnknown(ObjectUtils.merge(current.data(), defaultServiceConfig.toPrimitives(),),),);

        await firebase
        .firestore()
        .doc('app_configurations/GLOBAL_FEES')
        .set(ObjectUtils.omitUnknown({
            exchangeRates: {
                bolivar: 8.02,
            },
        }),);

        await firebase
        .firestore()
        .doc('app_configurations/PAYMENT_METHODS')
        .set(ObjectUtils.omitUnknown({
            BINANCE_PAY: {
                slug: "BINANCE_PAY", available: true,
            }, PAYPAL: {
                slug: "PAYPAL", available: true,
            }, CARD: {
                slug: "CARD", available: true,
            },
        }),);

        await firebase
        .firestore()
        .doc('app_configurations/GLOBAL_STATE')
        .set(ObjectUtils.omitUnknown({
            available: true
        }));

        await firebase
        .firestore()
        .doc('app_configurations/REFERRAL_SYSTEM')
        .set(ObjectUtils.omitUnknown({
            available: true
        }));

        return {
            ok: true,
        };
    }

    @Get('check-env')
    async checkEnv() {
        try {
            return {
                projectId: this.configService.get<string>('FB_PROJECT_ID'), env: process.env.NODE_ENV,
            };
        } catch (e) {
            return {
                result: false,
            };
        }
    }

    @Get('registration/user/:email')
    async validateRegistration(@Param('email') email: string) {
        try {
            const result = await firebase.auth().getUserByEmail(email);

            return {
                result: true,
            };
        } catch (e) {
            return {
                result: false,
            };
        }
    }

    @Post('registration/create-password/check-token')
    async checkNewPasswordToken(@Body() body: CheckNewPassTokenDto) {
        if (body.token === '') return {
            isValid: false,
        };

        try {
            const result = await firebase
            .firestore()
            .collection('users')
            .where('newPasswordToken', '==', body.token)
            .get();
            const user = result.docs[0].data();
            return {
                isValid: !!!result.empty, email: user.email,
            };
        } catch (e) {
            return {
                isValid: false,
            };
        }
    }

    @Post('registration/create-password')
    async createNewPassword(@Body() body: CreateNewPassDto) {
        try {
            const result = await firebase
            .firestore()
            .collection('users')
            .where('newPasswordToken', '==', body.token)
            .get();

            if (result.empty) throw new Error('INVALID_TOKEN');

            const user = result.docs[0].data();

            await firebase.auth().updateUser(user.id, {
                password: body.password,
            });

            await firebase.firestore().collection('users').doc(user.id).update({
                newPasswordToken: '', hasDefaultPassword: false, customPasswordCreatedAt: new Date(),
            });

            return {
                result: true,
            };
        } catch (e) {
            return {
                result: false, error: e.message,
            };
        }
    }

    @Post('registration/validate-account-linking')
    async accountLinkVerification(@Body() body: AccountLinkVerifyDto) {
        const result = await firebase
        .firestore()
        .collection('users')
        .where('email', '==', body.email)
        .get();
        if (result.empty) throw new Error('USER_NOT_FOUND');

        try {
            const res = await axios.post(`https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${this.configService.get<string>('FB_API_KEY',)}`, {
                email: body.email, password: body.password, returnSecureToken: true,
            },);

            const info = await res.data;

            const token = await this.tokenCreator.generate({
                userType: '', userId: info.localId, newRole: body.newRole, email: body.email,
            });

            await firebase.firestore().collection('link_account_requests').add({
                token: token, userId: info.localId, date: new Date(),
            });

            return {
                ok: true, token: token,
            };
        } catch (e) {
            return {
                ok: false,
            };
        }
    }

    @Post('transform-apns-token')
    async transformApnsToken(@Body() body: TransformApnsTokenDto) {
        try {
            const token = await this.pushNotificationService.transformApnsToFcm(body.token, body.application,);

            return {
                ok: true, token: token,
            };
        } catch (e) {
            console.log(e.message);
            return {
                ok: false,
            };
        }
    }
}
