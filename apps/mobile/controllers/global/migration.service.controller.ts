import {Body, Controller, Post} from '@nestjs/common';
import {ApiOperation, ApiTags} from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import {inject} from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import MobilePushNotificationService from '@shared/infrastructure/push-notifications';
import firebase from 'firebase-admin';
import admin from 'firebase-admin';
import recalculateProduct from "../../../../functions/src/utils/vendor-products/recalculate-product";


@ApiTags('Migration') @Controller({
    path: 'migration',
})
export class MigrationServiceController extends ApiController {

    constructor(@inject('command.bus') commandBus: CommandBus, @inject('query.bus') queryBus: QueryBus, @inject('multipart.handler') multipartHandler: MultipartHandler<Request, Response>, @inject('push.notification.service') notificationService: MobilePushNotificationService,) {
        super(commandBus, queryBus, multipartHandler);
    }

    @Post('recalculate-products') @ApiOperation({
        summary: 'migration', description: '',
    })
    async recalculateProducts(@Body() data: any) {
        let query: any = await firebase.firestore()
        .collection("vendor_products")
        // .where("id", "==", "k4WIZzOuVYZB8Si2gfnY")
        .get();

        const docs = await query.docs;

        const finalDocs = docs.map(e => e.data())

        const updates = finalDocs.map((doc) => {
            return {
                id: doc.id, updates: recalculateProduct(doc)
            }
        }).filter(f => !!f.updates)


        try {
            await this.update(updates)
        } catch (e) {
            console.log(e)
        }


        try {
            return {
                ok: true,
            };
        } catch (e) {
            return {
                message: e.message, ok: false,
            };
        }
    }

    private async update(docs) {
        const ref = firebase.firestore().collection("vendor_products");

        const chunks = this.getChunks(docs, 500);

        for (const chunk of chunks) {
            const batch = admin.firestore().batch();

            chunk.forEach((v) => {
                const sfRef = ref.doc(v.id);
                batch.update(sfRef, v.updates);
            });

            await batch.commit();
        }
    }

    private getChunks(inputArray: any[], perChunk: number) {
        const result = inputArray.reduce((resultArray, item, index) => {
            const chunkIndex = Math.floor(index / perChunk);

            if (!resultArray[chunkIndex]) {
                resultArray[chunkIndex] = []; // start a new chunk
            }

            resultArray[chunkIndex].push(item);

            return resultArray;
        }, []);

        return result;
    }
}
