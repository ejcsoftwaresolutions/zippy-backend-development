import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { inject } from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';

import admin from 'firebase-admin';
import firebase from 'firebase-admin';
import ExternalVendorProductCatalogImporter from '../../../../functions/src/utils/external-vendor-product-syncronizer/external-vendor-product-catalog-importer';
import SucasaProductFetcher from '../../../../functions/src/utils/external-vendor-product-syncronizer/fetchers/sucasa-product-fetcher';
import SucasaMapper from '../../../../functions/src/utils/external-vendor-product-syncronizer/mappers/sucasa-mapper';
import SucasaProductBatcher from '../../../../functions/src/utils/external-vendor-product-syncronizer/batchers/sucasa-product-batcher';

class RemoveProductsDto {
  @ApiProperty()
  vendorId: string;
}

class SyncSucasaDto {
  @ApiProperty()
  mode?: string;
}

@ApiTags('General')
@Controller({
  path: 'general',
})
export class ExternalVendorProductSyncronizerController extends ApiController {
  private productImporter: ExternalVendorProductCatalogImporter;

  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
  ) {
    super(commandBus, queryBus, multipartHandler);
    this.productImporter = new ExternalVendorProductCatalogImporter();
  }

  @Post('sucasa-syncronize')
  @ApiOperation({
    summary: 'test',
    description: '',
  })
  async sucasaProductSync(@Body() data: SyncSucasaDto) {
    try {
      const isDev = process.env.NODE_ENV != 'production';
      const sellerId = isDev ? '2HvDlZbhgSSKAefzQv32' : 'y7EHAW9EDROgcRSSrKYk';
      const vendor = (
        await firebase.firestore().collection('vendors').doc(sellerId).get()
      ).data();
      if (!vendor) return;
      const sellerCategoryId = vendor.categoryID;
      const extraCommissionPercentage = vendor.salesCommissionPercentage;

      const fetcher = new SucasaProductFetcher();
      const categories = await fetcher.getCategories();

      await this.productImporter.syncSubcategories(
        sellerId,
        sellerCategoryId,
        categories,
        SucasaMapper,
      );

      const start = new Date();
      start.setUTCHours(0, 0, 0, 0);
      const end = new Date();
      end.setUTCHours(23, 59, 59, 999);

      const batcher = new SucasaProductBatcher(this.productImporter, {
        sellerId: sellerId,
        sellerCategoryId: sellerCategoryId,
        extraCommissionPercentage,
      });

      const hasMappings = await this.productImporter.hasProductsMappings(
        sellerId,
      );

      const mode: any = (() => {
        if (!data.mode) return !hasMappings ? 'FULL-SYNC' : 'PARTIAL-SYNC';
        if (['FULL-SYNC', 'PARTIAL-SYNC'].includes(data.mode)) return data.mode;
        return 'PARTIAL-SYNC';
      })();

      await batcher.process(categories, {
        mode: mode,
      });
      /* .then(() => {
                                 /!*silent*!/
                                 console.log('product sync success');
                               })
                               .catch((e) => {
                                 console.log('product sync error: ' + e.message);
                               });*/
      return {
        ok: true,
      };
    } catch (e) {
      return {
        message: e.message,
        ok: false,
      };
    }
  }

  @Post('remove-external-vendor-products')
  @ApiOperation({
    summary: 'test',
    description: '',
  })
  async removeExternalVendorProducts(@Body() data: RemoveProductsDto) {
    try {
      const ref = await admin.firestore().collection('vendor_products');
      const products = await ref.where('vendorID', '==', data.vendorId).get();

      const chunks = this.getChunks(products.docs, 500);

      for (const chunk of chunks) {
        const batch = admin.firestore().batch();

        chunk.forEach((d) => {
          batch.delete(d.ref);
        });

        await batch.commit();
      }

      await admin
        .database()
        .ref(`external_vendor_product_mappings/${data.vendorId}`)
        .set(null);
      return {
        ok: true,
      };
    } catch (e) {
      return {
        message: e.message,
        ok: false,
      };
    }
  }

  private getChunks(inputArray: any[], perChunk: number) {
    const result = inputArray.reduce((resultArray, item, index) => {
      const chunkIndex = Math.floor(index / perChunk);

      if (!resultArray[chunkIndex]) {
        resultArray[chunkIndex] = []; // start a new chunk
      }

      resultArray[chunkIndex].push(item);

      return resultArray;
    }, []);

    return result;
  }
}
