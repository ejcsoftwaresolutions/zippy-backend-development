import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { inject } from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import firebase from 'firebase-admin';

class CountDocsDto {
  @ApiProperty() collection: string;

  @ApiProperty() filters: any;
}

@ApiTags('General')
@Controller({
  path: 'general',
})
export class GeneralServiceController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('doc-counter')
  @ApiOperation({
    summary: 'test',
    description: '',
  })
  async docCounter(@Body() data: CountDocsDto) {
    try {
      let query: any = firebase.firestore().collection(data.collection);

      if (data.filters.length === 0) {
        const res = await query.count().get();
        return {
          count: res.data().count,
          ok: true,
        };
      }

      data.filters.forEach((filter) => {
        query = query.where(filter.field, filter.operator, filter.value);
      });

      const res = await query.count().get();

      return {
        count: res.data().count,
        ok: true,
      };
    } catch (e) {
      return {
        count: 0,
        message: e.message,
        ok: false,
      };
    }
  }
}
