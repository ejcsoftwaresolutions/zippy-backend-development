import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import { Body, Controller, Post } from '@nestjs/common';
import { inject } from '@shared/domain/decorators';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import firebase from 'firebase-admin';

class CustomerLinkEmailDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  credentials: {
    email: string;
    password: string;
  };
}

class DeletePhoneUserDto {
  @ApiProperty()
  id: string;
}

@ApiTags('Customer')
@Controller({
  path: 'customer/auth',
})
export class CustomerAuthController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('link-email')
  @ApiOperation({
    summary: 'Link email and password to customer auth',
  })
  async linkEmail(@Body() body: CustomerLinkEmailDto) {
    try {
      await firebase.auth().updateUser(body.id, {
        email: body.credentials.email,
        password: body.credentials.password,
      });

      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }
}
