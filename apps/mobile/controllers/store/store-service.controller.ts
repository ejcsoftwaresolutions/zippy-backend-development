import CancelDeliveryRequestCommand from '@deliveryService/store/application/cancel-delivery-request/cancel-delivery-request-command';
import RequestDeliveryCommand from '@deliveryService/store/application/request-delivery/request-delivery-command';
import StorePublicProfileQueryRepository from '@deliveryService/store/domain/repositories/store-public-profile-query-repository';
import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { inject } from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import firebase, { firestore } from 'firebase-admin';
import FieldValue = firestore.FieldValue;

const ORDER_COLLECTION = 'orders';
const DELIVERY_COLLECTION = 'delivery_orders';
const DRIVER_LOGS_COLLECTION = 'driver_delivery_activity_logs';
const BUSY_RIDERS = 'busy_riders';
const RIDERS_ACTIVE_ORDERS = 'riders_active_orders';

class SearchOrderRiderDto {
  @ApiProperty() id: string;
  @ApiProperty() preCandidates?: string[];
}

class CancelPassengerRequestDto {
  @ApiProperty() requestId: string;
}

class ResetDeliveryDto {
  @ApiProperty() orderId: string;
}

@ApiTags('Store')
@Controller({
  path: 'store/delivery',
})
export class StoreServiceController extends ApiController {
  constructor(
    @inject('command.bus') commandBus: CommandBus,
    @inject('query.bus') queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
    @inject('StorePublicProfileQueryRepository')
    private storePublicProfileRepo: StorePublicProfileQueryRepository,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('search-order-rider')
  @ApiOperation({
    summary: 'Creates an attempt delivery order to find a near rider',
  })
  async searchOrderRider(@Body() data: SearchOrderRiderDto) {
    try {
      const orderRef = firebase
        .firestore()
        .collection(ORDER_COLLECTION)
        .doc(data.id);
      const vendorOrderQ = await orderRef.get();
      const vendorOrder = vendorOrderQ.data();

      if (vendorOrder.deliveryMethod === 'PICK_UP') {
        return {
          ok: true,
          message: 'pickup request',
        };
      }
      const deliveryOrder = {
        id: vendorOrder.id,
        items: vendorOrder.products.map((p: any) => ({
          id: p.id,
          name: p.name,
          quantity: p.quantity,
          unitPrice: p.price,
        })),
        paymentMethod: 'CASH',
        storeId: vendorOrder.vendorID,
        subtotal: vendorOrder.subtotal,
        total: vendorOrder.total,
        deliveryFee:
          vendorOrder?.fees?.delivery ??
          vendorOrder.total - vendorOrder.subtotal,
        date: vendorOrder.createdAt,
        code: vendorOrder.code ?? vendorOrder.id,
      };

      const originPoint = {
        geoLocation: [
          parseFloat(vendorOrder.shipAddr.location.latitude),
          parseFloat(vendorOrder.shipAddr.location.longitude),
        ],

        address: vendorOrder.shipAddr.formattedAddress,
        referencePoint: vendorOrder.shipAddr.referencePoint,
      };

      const { id } = await this.dispatch(
        new RequestDeliveryCommand({
          preCandidates: data.preCandidates,
          customerId: vendorOrder.customerID,
          customerLocation: originPoint,
          order: {
            ...deliveryOrder,
            date: deliveryOrder.date
              ? new Date(deliveryOrder.date.seconds * 1000)
              : new Date(),
          },
          mode: 'live',
          serviceType: 'CAR',
        }),
      );

      return {
        ok: true,
        id: id,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);

      return {
        ok: false,
        error: error.message,
      };
    }
  }

  @Post('cancel-request')
  @ApiOperation({
    summary: 'Cancels an active delivery request (Rider search)',
  })
  async cancelRequest(@Body() body: CancelPassengerRequestDto) {
    try {
      await this.dispatch(new CancelDeliveryRequestCommand(body.requestId));
      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }

  @Post('reset-delivery')
  @ApiOperation({
    summary:
      'Resets active order delivery taken by a rider (Puts order back to Order Accepted)',
  })
  async resetDelivery(@Body() body: ResetDeliveryDto) {
    try {
      await firebase.firestore().runTransaction(async (transaction) => {
        const driverLogsRef = firebase
          .firestore()
          .collection(DRIVER_LOGS_COLLECTION)
          .where('vendorOrderId', '==', body.orderId);

        const driverLogRefs = (await transaction.get(driverLogsRef)).docs.map(
          (d) => {
            return d.ref;
          },
        );

        const orderRef = firebase
          .firestore()
          .collection(ORDER_COLLECTION)
          .doc(body.orderId);

        const order = (await transaction.get(orderRef)).data();

        if (!order) return;

        const driverId = order.driverID;

        await transaction.update(orderRef, {
          status: 'Order Accepted',
          driver: FieldValue.delete(),
          driverID: FieldValue.delete(),
        });

        const deliveryRef = firebase
          .firestore()
          .collection(DELIVERY_COLLECTION)
          .doc(body.orderId);

        await transaction.delete(deliveryRef);

        driverLogRefs.forEach((ref) => {
          transaction.delete(ref);
        });

        await firebase
          .database()
          .ref(`${RIDERS_ACTIVE_ORDERS}/${driverId}/${body.orderId}`)
          .remove();

        await firebase.database().ref(`${BUSY_RIDERS}/${driverId}`).remove();
      });
      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }
}
