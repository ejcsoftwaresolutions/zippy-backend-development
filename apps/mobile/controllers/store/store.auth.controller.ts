import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { inject } from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import firebase from 'firebase-admin';
import RegisterVendorCommand from '../../../../src/main/vendors/authetication/application/register-vendor/register-vendor-command';

class StoreRegisterDto {
  id: string;

  @ApiProperty({ required: true }) shopName: string;
  /* @ApiProperty({ required: true }) rif: string;*/
  /* @ApiProperty({ required: true }) deliveryMethod: string[];*/
  @ApiProperty({ required: true }) basicInfo: {
    email: string;
    firstName: string;
    lastName: string;
    phone: string;
    birthday?: string;
    linePhone?: string;
    contactCompanyCharge?: string;
    identificationCard?: string;
  };
  @ApiProperty({ required: true }) social: {
    website?: string;
    instagram: string;
  };
  /* @ApiProperty({ required: true }) extra: {
                   productTypes: string;
                 };
                 @ApiProperty({ required: true }) schedule: {
                   range: string;
                   startHour: string;
                   endHour: string;
                   comment?: string;
                 };
                 @ApiProperty({ required: true }) documents: {
                   dniUrl: string;
                   rifUrl: string;
                   productCatalogUrl: string;
                   LIAELicenseUrl: string;
                   permitMedicalUrl: string;
                   foodManipulationUrl: string;
                 };
                 @ApiProperty({ required: true }) withdrawalDetails: {
                   accountType: string;
                   bank: string;
                   accountNumber: string;
                 };
                 @ApiProperty({ required: true }) categoryId: string;
                 @ApiProperty({ required: false }) location?: {
                   latitude: number;
                   longitude: number;
                 };
                 @ApiProperty({ required: true }) address: {
                   formattedAddress: string;
                   line1: string;
                   line2: string;
                   city: string;
                   address: string;
                   state: string;
                   country: string;
                 };*/
  @ApiProperty() linkAccountToken?: string;
}

@ApiTags('Store')
@Controller({
  path: 'store/auth',
})
export class StoreAuthController extends ApiController {
  constructor(
    @inject('command.bus') commandBus: CommandBus,
    @inject('query.bus') queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('register')
  @ApiOperation({
    summary: 'Register a store',
  })
  async register(@Body() body: StoreRegisterDto) {
    try {
      /*  const files = {
                            dniUrl: body.documents.dniUrl,
                            rifUrl: body.documents.rifUrl,
                            productCatalogUrl: body.documents.productCatalogUrl,
                            LIAELicenseUrl: body.documents.LIAELicenseUrl,
                            permitMedicalUrl: body.documents.permitMedicalUrl,
                            foodManipulationUrl: body.documents.foodManipulationUrl,
                          };
                    */
      const res = await this.dispatch(
        new RegisterVendorCommand({
          id: body.id,
          /* extra: body.extra,*/
          social: body.social,
          /* withdrawalDetails: body.withdrawalDetails,
                               rif: body.rif,*/
          basicInfo: {
            linePhone: body.basicInfo.linePhone,
            contactCompanyCharge: body.basicInfo.contactCompanyCharge,
            lastName: body.basicInfo.lastName,
            firstName: body.basicInfo.firstName,
            phone: body.basicInfo.phone,
            identificationCard: body.basicInfo.identificationCard,
            birthday: body.basicInfo.birthday
              ? new Date(body.basicInfo.birthday)
              : undefined,
            email: body.basicInfo.email.toLowerCase(),
          },
          /*  schedule: body.schedule,
                                categoryId: body.categoryId,*/
          shopName: body.shopName,
          /* location: {
                       longitude: body.location?.longitude,
                       latitude: body.location?.latitude,
                       line1: body.address.line1,
                       line2: body.address.line2,
                       address: body.address.formattedAddress,
                       state: body.address.state,
                       city: body.address.city,
                       country: body.address.country,
                     },
                     documents: {
                       dniUrl: files.dniUrl,
                       rifUrl: files.rifUrl,
                       productCatalogUrl: files.productCatalogUrl,
                       permitMedicalUrl: files.permitMedicalUrl,
                       foodManipulationUrl: files.foodManipulationUrl,
                       LIAELicenseUrl: files.LIAELicenseUrl,
                     },
                     deliveryMethod: body.deliveryMethod,*/
          linkAccountToken: body.linkAccountToken,
        }),
      );

      return {
        id: res.id,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }

  @Get('categories')
  @ApiOperation({
    summary: 'store categories',
  })
  async categories() {
    try {
      const db = firebase.firestore();

      const query = db
        .collection(`vendor_categories`)
        .where('parentID', '==', '');

      const result = await query.get();

      const categoriesDto = (await result.docs).map((d) => d.data());

      return {
        categories: categoriesDto.map((item) => {
          return {
            id: item.id,
            name: item.title,
            imageUrl: item.photo,
          };
        }),
        ok: true,
      };
    } catch (error) {
      return {
        ok: false,
        error: error.message,
      };
    }
  }
}
