import { Body, Controller, Post } from '@nestjs/common';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import ApiController from '@shared/infrastructure/controller/api-controller';
import firebase from 'firebase-admin';
import { ObjectUtils } from '@shared/domain/utils';

class UpdateFBUser {
  @ApiProperty() firebaseId: string;

  @ApiProperty({
    type: 'object',
    properties: {
      email: {
        type: 'string',
      },
      password: {
        type: 'string',
      },
      phone: {
        type: 'string',
      },
    },
  })
  changes: {
    email?: string;
    password?: string;
    phone?: string;
  };

  @ApiProperty() secret: string;
}

const SECRET = 'P6fllo65l@nS';

@ApiTags('Admin')
@Controller({
  path: 'admin',
})
export class CreateDefaultAdminController extends ApiController {
  @Post('/auth/default')
  async execute(@Body() credentials: { id: string }): Promise<any> {
    try {
      await firebase.auth().createUser({
        uid: credentials.id,
        email: 'superadmin@admin.com',
        password: 'zippi*2022',
      });
    } catch (error) {
      return {
        ok: false,
      };
    }
  }

  @Post('/update-firebase-user')
  async updateUser(@Body() updateUserRequest: UpdateFBUser): Promise<any> {
    if (updateUserRequest.secret !== SECRET) {
      return {
        ok: false,
        msg: "That's not the secret folk! -,-",
      };
    }

    try {
      const changes = updateUserRequest.changes;

      if (!changes.email && !changes.password && !changes.phone) {
        return {
          ok: false,
          msg: 'Send email or password or phone number',
        };
      }

      await firebase.auth().updateUser(updateUserRequest.firebaseId, {
        email: changes.email,
        password: changes.password,
        phoneNumber: changes.phone,
      });

      const userRef = firebase
        .firestore()
        .collection('users')
        .doc(updateUserRequest.firebaseId);

      const user = (await userRef.get())?.data();

      const finalChanges = ObjectUtils.omitUnknown(
        ObjectUtils.pick(changes, ['email', 'phone']),
      );

      await userRef.update({
        ...finalChanges,
        hasDefaultPassword: false,
        newPasswordToken: '',
      });

      if (user?.roles?.includes('RIDER')) {
        await firebase
          .firestore()
          .collection('riders')
          .doc(user.id)
          .update(finalChanges);
      }

      return {
        ok: true,
      };
    } catch (error) {
      return {
        ok: false,
        msg: error.message,
      };
    }
  }

  @Post('/sync-vendor-total-earnings')
  async syncVendorTotalEarnings(): Promise<any> {
    try {
      return {
        ok: true,
      };
    } catch (error) {
      return {
        ok: false,
        msg: error.message,
      };
    }
  }
}
