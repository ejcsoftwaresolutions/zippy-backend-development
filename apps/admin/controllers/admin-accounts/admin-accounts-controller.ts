import AdminAuthUser from '@admin/auth/domain/models/admin-auth-user';
import { User } from '@apps/shared/decorators/user-decorator';
import AuthenticatedUser from '@apps/shared/dto/authenticated-user';
import {
  Body,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiProperty,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import Criteria from '@shared/domain/criteria/criteria';
import Filters from '@shared/domain/criteria/filters';
import Order from '@shared/domain/criteria/order';
import { inject } from '@shared/domain/decorators';
import Id from '@shared/domain/id/id';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import PasswordHasher from '@shared/domain/utils/password-hasher';
import Collection from '@shared/domain/value-object/collection';
import ApiController from '@shared/infrastructure/controller/api-controller';
import AuthAdminInfrastructureCommandRepository from '@admin/auth/infrastructure/persistance/fireorm/repositories/auth-admin-infrastructure-command-repository';

class ObjectDto {
  @ApiProperty()
  firstName: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  roleId: string;

  @ApiProperty()
  password: string;
}

@ApiTags('Admin')
/*@ApiBearerAuth()
@UseGuards(JwtUserGuard)*/
@Controller({
  path: 'admin/accounts',
})
export class AdminAccountsController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('AuthAdminCommandRepository')
    private repo: AuthAdminInfrastructureCommandRepository,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
    @inject('utils.passwordHasher')
    private passwordHasher: PasswordHasher,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Get()
  @ApiResponse({
    status: 200,
    description: 'List',
  })
  @ApiOperation({
    summary: 'List',
  })
  async list(@User() user: AuthenticatedUser, @Query() params): Promise<any> {
    try {
      const order = params['filter[order]'];
      const limit = params['filter[limit]'];
      const skip = params['filter[skip]'];
      const where = params['filter[where]'];

      const criteria = params
        ? new Criteria({
            order: new Collection(
              order
                ? [Order.fromValues(order.split(' ')[0], order.split(' ')[1])]
                : [],
            ),
            limit: limit ? parseInt(limit) : undefined,
            offset: skip ? parseInt(skip) : undefined,
            filters: Filters.fromValues({
              ...(where ?? {}),
              id: { $ne: user.id },
            }),
          })
        : undefined;

      return await this.repo.getAll(criteria);
    } catch (error) {
      console.log(error);
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.message,
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get('/count')
  @ApiResponse({
    status: 200,
    description: 'Get count',
  })
  @ApiOperation({
    summary: 'Total',
  })
  async getTotal(
    @User() user: AuthenticatedUser,
    @Query() data: any,
  ): Promise<any> {
    try {
      return {
        count: await this.repo.count({
          ...(data ?? {}),
          id: { $ne: user.id },
        }),
      };
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.message,
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'Get',
  })
  @ApiOperation({
    summary: 'Get',
  })
  async get(@Param('id') id: string): Promise<any> {
    try {
      return await this.repo.get(id);
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.message,
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Create',
  })
  @ApiOperation({
    summary: 'Create',
  })
  async create(@Body() data: ObjectDto): Promise<any> {
    try {
      const id = new Id();

      const newEntity = { ...data, id: id };

      const hashedPassword = await this.passwordHasher.hashPassword(
        newEntity.password,
      );

      await this.repo.register(
        AdminAuthUser.create({
          ...newEntity,
          credentials: {
            password: hashedPassword,
            email: newEntity.email,
          },
        }),
      );

      return { ...newEntity, id: id.value };
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.message,
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Patch(':id')
  @ApiResponse({
    status: 200,
    description: 'Update',
  })
  @ApiOperation({
    summary: 'Update',
  })
  async edit(@Param('id') id: string, @Body() data: ObjectDto): Promise<any> {
    try {
      const admin = await this.repo.get(id);

      if (!admin) return;
      return await this.repo.updateUser(
        AdminAuthUser.fromPrimitives({
          id: admin.id.value,
          ...data,
        }),
      );
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.message,
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  @Delete(':id')
  @ApiResponse({
    status: 200,
    description: 'delete',
  })
  @ApiOperation({
    summary: 'delete',
  })
  async delete(@Param('id') id: string): Promise<any> {
    try {
      return await this.repo.delete(id);
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: error.message,
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
