import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import { Controller, Post, Query } from '@nestjs/common';
import ApiController from '@shared/infrastructure/controller/api-controller';
import { inject } from '@shared/domain/decorators';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import EventBus from '@shared/domain/bus/event/event-bus';
import ExportRiderListRequestedDomainEvent from '@admin/riders/domain/events/export-rider-list-requested-domain-event';

class RiderListExportDto {
  @ApiProperty({ required: false })
  filters?: string;

  @ApiProperty({ required: false, type: 'number' })
  limit?: number;

  @ApiProperty({ required: false, type: 'number' })
  skip?: number;

  @ApiProperty({ required: false })
  order?: string;

  @ApiProperty({ required: true })
  format: string;

  @ApiProperty({ required: true })
  email: string;
}

@ApiTags('Admin')
@Controller({
  path: 'admin/riders',
})
export class AdminRiderListExportController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('event.bus')
    private eventBus: EventBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('export')
  @ApiOperation({
    summary: 'Export riders in pdf/excel',
  })
  async execute(@Query() query: RiderListExportDto) {
    this.eventBus.publish([
      new ExportRiderListRequestedDomainEvent({
        eventData: {
          format: query.format,
          email: query.email,
        },
        aggregateId: new Date().toUTCString(),
        occurredOn: new Date(),
      }),
    ]);

    try {
      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }
}
