import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Post } from '@nestjs/common';
import ApiController from '@shared/infrastructure/controller/api-controller';
import { inject } from '@shared/domain/decorators';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApproveRiderCommand from '../../../../src/main/riders/authentication/application/approve-rider/approve-rider-command';
import firebase from 'firebase-admin';
import { ConfigService } from '@nestjs/config';

const USERS = 'users';

class RiderApproveDto {
  @ApiProperty()
  userId: string;
}

@ApiTags('Admin')
@Controller({
  path: 'admin/riders',
})
export class AdminRiderApproveController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
    private configService: ConfigService,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('approve')
  @ApiOperation({
    summary: 'Approve a new rider',
  })
  async approve(@Body() body: RiderApproveDto) {
    try {
      const res = await this.dispatch(
        new ApproveRiderCommand({
          id: body.userId,
        }),
      );

      await firebase
        .firestore()
        .collection('rider_applications')
        .doc(body.userId)
        .update({
          state: 'APPROVED',
          rejectedAt: null,
          approvedAt: new Date(),
        });

      const userData = await this.findUser(body.userId);

      if (!userData)
        return {
          ok: true,
        };
      try {
        await this.sendEmail({
          user: userData as any,
        });
      } catch (e) {
        console.log('No se pudo enviar el correo!');
      }

      return {
        ok: true,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }

  private async findUser(id) {
    const userS = await firebase
      .firestore()
      .collection(USERS)
      .where('id', '==', id)
      .get();

    const user = !userS.empty ? userS.docs.shift()?.data() : {};

    const riderS = await firebase
      .firestore()
      .collection(USERS)
      .where('id', '==', id)
      .get();

    const rider = !riderS.empty ? riderS.docs.shift()?.data() : {};

    return { ...user, identificationCard: rider.identificationCard };
  }

  private async sendEmail({
    user,
  }: {
    user: {
      firstName;
      lastName;
      email;
      newPasswordToken;
      hasDefaultPassword;
      identificationCard;
    };
  }) {
    const resetPasswordUrl = `${this.configService.get<string>(
      'WEBSITE_URL',
    )}/new-password/${user.newPasswordToken}`;

    await firebase
      .firestore()
      .collection('mail')
      .add({
        to: user.email,
        message: {
          subject: `Zippi - Cuenta aprobada`,
          text: `Buenas noticias!. Tu solicitud ha sido aprobada. ¡Ya puedes comenzar a utilizar Zippi!.`,
          html: `
                  <div>
                       <p>
                         Buenas noticias, ${user.firstName?.trim()}!. Tu cuenta ha sido aprobada. ¡Ya puedes comenzar a utilizar Zippi!.
                         Tu clave inicial es  <strong>${
                           user.identificationCard
                         }</strong> para cambiarla ingresa <a href="${resetPasswordUrl}">aquí</a>
                      </p>
                    <br/>
                  </div>
                `,
        },
      });
  }
}
