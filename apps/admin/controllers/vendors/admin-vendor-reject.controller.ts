import {ApiOperation, ApiProperty, ApiTags} from '@nestjs/swagger';
import {Body, Controller, Post} from '@nestjs/common';
import ApiController from '@shared/infrastructure/controller/api-controller';
import {inject} from '@shared/domain/decorators';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApproveVendorCommand
    from '../../../../src/main/vendors/authetication/application/approve-vendor/approve-vendor-command';
import firebase from 'firebase-admin';

class VendorRejectDto {
    @ApiProperty()
    userId: string;
}

@ApiTags('Admin')
@Controller({
    path: 'admin/vendors',
})
export class AdminVendorRejectController extends ApiController {
    constructor(
        @inject('command.bus')
            commandBus: CommandBus,
        @inject('query.bus')
            queryBus: QueryBus,
        @inject('multipart.handler')
            multipartHandler: MultipartHandler<Request, Response>,
    ) {
        super(commandBus, queryBus, multipartHandler);
    }

    @Post('reject')
    @ApiOperation({
        summary: 'Reject a store',
    })
    async reject(@Body() body: VendorRejectDto) {
        try {
            const res = await this.dispatch(
                new ApproveVendorCommand({
                    id: body.userId,
                }),
            );

            await firebase
            .firestore()
            .collection('vendor_applications')
            .doc(body.userId)
            .update({state: 'REJECTED', rejectedAt: new Date()});

            const userData = await this.findUser(body.userId)

            if (!userData) return {
                ok: true,
            };

            try {
                await this.sendEmail({
                    user: userData as any
                })
            } catch (e) {
                console.log("No se pudo enviar el correo!")
            }

            return {
                id: res.id,
            };
        } catch (error) {
            console.log(`Error: ${error.message}`);
            return {
                ok: false,
                error: error.message,
            };
        }
    }


    private async findUser(id) {
        const userS = await firebase
        .firestore()
        .collection('users')
        .where('id', '==', id)
        .get();

        const user = !userS.empty ? userS.docs.shift()?.data() : undefined;

        return user
    }

    private async sendEmail({user}: { user: { firstName, lastName, email } }) {
        await firebase
        .firestore()
        .collection('mail')
        .add({
            to: user.email,
            message: {
                subject: `Zippi - Cuenta rechazada`,
                text: `Tu solicitud ha sido rechazada.`,
                html: `
                  <div>
                       <p>
                         Malas noticias ${user.firstName?.trim()}, tu cuenta no pudo ser aprobada.
                      </p>
                    <br/>
                  </div>
                `,
            },
        });
    }

}
