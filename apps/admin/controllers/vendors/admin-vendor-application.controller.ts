import {ApiTags} from '@nestjs/swagger';
import {Controller, Get, Param, Res} from '@nestjs/common';
import ApiController from '@shared/infrastructure/controller/api-controller';
import {inject} from '@shared/domain/decorators';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import firebase from 'firebase-admin';
import axios from "axios";


@ApiTags('Admin')
@Controller({
    path: 'admin/vendors',
})
export class AdminVendorApplicationController extends ApiController {
    constructor(
        @inject('command.bus')
            commandBus: CommandBus,
        @inject('query.bus')
            queryBus: QueryBus,
        @inject('multipart.handler')
            multipartHandler: MultipartHandler<Request, Response>,
    ) {
        super(commandBus, queryBus, multipartHandler);
    }


    @Get('application/:id/document/:key')
    async downloadDocument(
        @Param('id') id: string,
        @Param('key') key: string,
        @Res() res,
    ) {

        const applicationRef = await firebase
        .firestore()
        .doc(`vendor_applications/${id}`)
        .get();

        if (!applicationRef.exists) {
            res.status(404).send("User application not found")
            return
        }

        const application = applicationRef.data()

        const documentUrl = application.documents[key]

        if (!documentUrl) {
            res.status(404).send("Document doesn't exist")
            return
        }

        const ext = documentUrl.split(".").pop().split("?")[0]

        const response = await axios({
            url: documentUrl,
            method: 'GET',
            responseType: 'stream',
        });

        const fullName = `${application.firstName}_${application.lastName}`;

        res.header('Content-disposition', `attachment; filename=${fullName}-${key.replace("Url", "")}.${ext}`)
        res.send(response.data)
    }


}
