import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import { Body, Controller, Post } from '@nestjs/common';
import ApiController from '@shared/infrastructure/controller/api-controller';
import { inject } from '@shared/domain/decorators';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApproveVendorCommand from '../../../../src/main/vendors/authetication/application/approve-vendor/approve-vendor-command';
import firebase from 'firebase-admin';
import { ConfigService } from '@nestjs/config';
import { VENDORS } from '@deliveryService/delivery/infrastructure/persistance/fireorm/constants';

const USERS = 'users';

class VendorApproveDto {
  @ApiProperty()
  userId: string;
}

@ApiTags('Admin')
@Controller({
  path: 'admin/vendors',
})
export class AdminVendorApproveController extends ApiController {
  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
    private configService: ConfigService,
  ) {
    super(commandBus, queryBus, multipartHandler);
  }

  @Post('approve')
  @ApiOperation({
    summary: 'Approve a store',
  })
  async approve(@Body() body: VendorApproveDto) {
    try {
      const res = await this.dispatch(
        new ApproveVendorCommand({
          id: body.userId,
        }),
      );
      await firebase
        .firestore()
        .collection('vendor_applications')
        .doc(body.userId)
        .update({
          state: 'APPROVED',
          rejectedAt: null,
          approvedAt: new Date(),
        });

      const userData = await this.findUser(body.userId);

      if (!userData)
        return {
          ok: true,
        };

      try {
        await this.sendEmail({
          user: userData as any,
        });
      } catch (e) {
        console.log('No se pudo enviar el correo!');
      }

      return {
        id: res.id,
      };
    } catch (error) {
      console.log(`Error: ${error.message}`);
      return {
        ok: false,
        error: error.message,
      };
    }
  }

  private async findUser(id) {
    const userS = await firebase
      .firestore()
      .collection(USERS)
      .where('id', '==', id)
      .get();

    const user = !userS.empty ? userS.docs.shift()?.data() : {};

    const vendorS = await firebase
      .firestore()
      .collection(VENDORS)
      .where('id', '==', id)
      .get();

    const vendor = !vendorS.empty ? vendorS.docs.shift()?.data() : {};

    return { ...user, rif: vendor.rif };
  }

  private async sendEmail({
    user,
  }: {
    user: {
      firstName;
      lastName;
      email;
      newPasswordToken;
      hasDefaultPassword;
      rif;
    };
  }) {
    const resetPasswordUrl = `${this.configService.get<string>(
      'WEBSITE_URL',
    )}/new-password/${user.newPasswordToken}`;

    await firebase
      .firestore()
      .collection('mail')
      .add({
        to: user.email,
        message: {
          subject: `Zippi - Cuenta aprobada`,
          text: `Buenas noticias!. Tu solicitud ha sido aprobada. ¡Ya puedes comenzar a utilizar Zippi!.`,
          html: `
                  <div>
                       <p>
                         Buenas noticias, ${user.firstName?.trim()}!. Tu cuenta ha sido aprobada. ¡Ya puedes comenzar a utilizar Zippi!.
                          Tu clave inicial es <strong>${
                            user.rif
                          }</strong> para cambiarla ingresa <a href="${resetPasswordUrl}">aquí</a>
                      </p>
                    <br/>
                  </div>
                `,
        },
      });
  }
}
