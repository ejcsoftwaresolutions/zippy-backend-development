import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiProperty, ApiTags } from '@nestjs/swagger';
import CommandBus from '@shared/domain/bus/command/command-bus';
import QueryBus from '@shared/domain/bus/query/query-bus';
import { inject } from '@shared/domain/decorators';
import MultipartHandler from '@shared/domain/storage/multipart-handler';
import ApiController from '@shared/infrastructure/controller/api-controller';
import { createNotificationService } from '../../../../functions/src/utils/notification-service/notification-service';
import PushNotificator from '../../../../functions/src/utils/notification-service/push-notificator';

class NotificationStatsDto {
  @ApiProperty() id: string;

  @ApiProperty() targetApp: any;
}

@ApiTags('Admin')
@Controller({
  path: 'admin',
})
export class AdminNotificationsController extends ApiController {
  private notificationService: PushNotificator;

  constructor(
    @inject('command.bus')
    commandBus: CommandBus,
    @inject('query.bus')
    queryBus: QueryBus,
    @inject('multipart.handler')
    multipartHandler: MultipartHandler<Request, Response>,
  ) {
    super(commandBus, queryBus, multipartHandler);
    this.notificationService = createNotificationService();
  }

  @Post('notifications/stats')
  @ApiOperation({
    summary: 'test',
    description: '',
  })
  async notificationStats(@Body() data: NotificationStatsDto) {
    try {
      const res = await this.notificationService.getNotificationStats(
        data.id,
        data.targetApp,
      );

      return {
        ...res,
      };
    } catch (e) {
      return {
        res: 0,
        message: e.message,
      };
    }
  }
}
